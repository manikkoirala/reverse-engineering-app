.class public Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;
.super Ljava/lang/Object;
.source "LaterArrowPathBuilder.java"


# static fields
.field private static final TODEGREE:F = 1.6666666f

.field private static path:Landroid/graphics/Path;

.field private static s_rect:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 7
    .line 8
    new-instance v0, Landroid/graphics/Path;

    .line 9
    .line 10
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static getAngle(DD)D
    .locals 4

    .line 1
    mul-double v0, p0, p0

    .line 2
    .line 3
    mul-double v2, p2, p2

    .line 4
    .line 5
    add-double/2addr v0, v2

    .line 6
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    .line 7
    .line 8
    .line 9
    move-result-wide v0

    .line 10
    div-double/2addr p0, v0

    .line 11
    invoke-static {p0, p1}, Ljava/lang/Math;->acos(D)D

    .line 12
    .line 13
    .line 14
    move-result-wide p0

    .line 15
    const-wide v0, 0x4066800000000000L    # 180.0

    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    mul-double p0, p0, v0

    .line 21
    .line 22
    const-wide v0, 0x400921fb54442d18L    # Math.PI

    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    div-double/2addr p0, v0

    .line 28
    const-wide/16 v0, 0x0

    .line 29
    .line 30
    cmpg-double v2, p2, v0

    .line 31
    .line 32
    if-gez v2, :cond_0

    .line 33
    .line 34
    const-wide p2, 0x4076800000000000L    # 360.0

    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    sub-double p0, p2, p0

    .line 40
    .line 41
    :cond_0
    return-wide p0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static getArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/lang/Object;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/16 v1, 0xd

    .line 11
    .line 12
    if-eq v0, v1, :cond_7

    .line 13
    .line 14
    const/16 v1, 0xf

    .line 15
    .line 16
    if-eq v0, v1, :cond_6

    .line 17
    .line 18
    const/16 v1, 0x37

    .line 19
    .line 20
    if-eq v0, v1, :cond_5

    .line 21
    .line 22
    const/16 v1, 0x53

    .line 23
    .line 24
    if-eq v0, v1, :cond_4

    .line 25
    .line 26
    const/16 v1, 0x63

    .line 27
    .line 28
    if-eq v0, v1, :cond_3

    .line 29
    .line 30
    const/16 v1, 0xb6

    .line 31
    .line 32
    if-eq v0, v1, :cond_2

    .line 33
    .line 34
    const/16 v1, 0x5d

    .line 35
    .line 36
    if-eq v0, v1, :cond_1

    .line 37
    .line 38
    const/16 v1, 0x5e

    .line 39
    .line 40
    if-eq v0, v1, :cond_0

    .line 41
    .line 42
    packed-switch v0, :pswitch_data_0

    .line 43
    .line 44
    .line 45
    packed-switch v0, :pswitch_data_1

    .line 46
    .line 47
    .line 48
    packed-switch v0, :pswitch_data_2

    .line 49
    .line 50
    .line 51
    packed-switch v0, :pswitch_data_3

    .line 52
    .line 53
    .line 54
    new-instance p0, Landroid/graphics/Path;

    .line 55
    .line 56
    invoke-direct {p0}, Landroid/graphics/Path;-><init>()V

    .line 57
    .line 58
    .line 59
    return-object p0

    .line 60
    :pswitch_0
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getCurvedDownArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 61
    .line 62
    .line 63
    move-result-object p0

    .line 64
    return-object p0

    .line 65
    :pswitch_1
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getCurvedUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 66
    .line 67
    .line 68
    move-result-object p0

    .line 69
    return-object p0

    .line 70
    :pswitch_2
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getCurvedLeftArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 71
    .line 72
    .line 73
    move-result-object p0

    .line 74
    return-object p0

    .line 75
    :pswitch_3
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getCurvedRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 76
    .line 77
    .line 78
    move-result-object p0

    .line 79
    return-object p0

    .line 80
    :pswitch_4
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getUturnArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 81
    .line 82
    .line 83
    move-result-object p0

    .line 84
    return-object p0

    .line 85
    :pswitch_5
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getBentArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 86
    .line 87
    .line 88
    move-result-object p0

    .line 89
    return-object p0

    .line 90
    :pswitch_6
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getBentUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 91
    .line 92
    .line 93
    move-result-object p0

    .line 94
    return-object p0

    .line 95
    :pswitch_7
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getLeftUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 96
    .line 97
    .line 98
    move-result-object p0

    .line 99
    return-object p0

    .line 100
    :pswitch_8
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getLeftRightArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 101
    .line 102
    .line 103
    move-result-object p0

    .line 104
    return-object p0

    .line 105
    :pswitch_9
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getDownArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 106
    .line 107
    .line 108
    move-result-object p0

    .line 109
    return-object p0

    .line 110
    :pswitch_a
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getUpArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 111
    .line 112
    .line 113
    move-result-object p0

    .line 114
    return-object p0

    .line 115
    :pswitch_b
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getRightArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 116
    .line 117
    .line 118
    move-result-object p0

    .line 119
    return-object p0

    .line 120
    :pswitch_c
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getLeftArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 121
    .line 122
    .line 123
    move-result-object p0

    .line 124
    return-object p0

    .line 125
    :pswitch_d
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getQuadArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 126
    .line 127
    .line 128
    move-result-object p0

    .line 129
    return-object p0

    .line 130
    :pswitch_e
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getUpDownArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 131
    .line 132
    .line 133
    move-result-object p0

    .line 134
    return-object p0

    .line 135
    :pswitch_f
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getLeftRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 136
    .line 137
    .line 138
    move-result-object p0

    .line 139
    return-object p0

    .line 140
    :pswitch_10
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 141
    .line 142
    .line 143
    move-result-object p0

    .line 144
    return-object p0

    .line 145
    :pswitch_11
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getDownArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 146
    .line 147
    .line 148
    move-result-object p0

    .line 149
    return-object p0

    .line 150
    :pswitch_12
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getLeftArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 151
    .line 152
    .line 153
    move-result-object p0

    .line 154
    return-object p0

    .line 155
    :cond_0
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getNotchedRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 156
    .line 157
    .line 158
    move-result-object p0

    .line 159
    return-object p0

    .line 160
    :cond_1
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getStripedRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 161
    .line 162
    .line 163
    move-result-object p0

    .line 164
    return-object p0

    .line 165
    :cond_2
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getLeftRightUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 166
    .line 167
    .line 168
    move-result-object p0

    .line 169
    return-object p0

    .line 170
    :cond_3
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getCircularArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 171
    .line 172
    .line 173
    move-result-object p0

    .line 174
    return-object p0

    .line 175
    :cond_4
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getQuadArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 176
    .line 177
    .line 178
    move-result-object p0

    .line 179
    return-object p0

    .line 180
    :cond_5
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getChevronPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 181
    .line 182
    .line 183
    move-result-object p0

    .line 184
    return-object p0

    .line 185
    :cond_6
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getHomePlatePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 186
    .line 187
    .line 188
    move-result-object p0

    .line 189
    return-object p0

    .line 190
    :cond_7
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 191
    .line 192
    .line 193
    move-result-object p0

    .line 194
    return-object p0

    .line 195
    :pswitch_data_0
    .packed-switch 0x42
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
    .end packed-switch

    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    :pswitch_data_1
    .packed-switch 0x4c
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
    .end packed-switch

    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    :pswitch_data_2
    .packed-switch 0x59
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch

    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    :pswitch_data_3
    .packed-switch 0x65
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getBentArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 11

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, 0x2

    .line 18
    if-eqz p0, :cond_2

    .line 19
    .line 20
    array-length v2, p0

    .line 21
    const/4 v3, 0x4

    .line 22
    if-ne v2, v3, :cond_2

    .line 23
    .line 24
    int-to-float v0, v0

    .line 25
    const/4 v2, 0x0

    .line 26
    aget-object v2, p0, v2

    .line 27
    .line 28
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    mul-float v2, v2, v0

    .line 33
    .line 34
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    const/4 v3, 0x1

    .line 39
    aget-object v3, p0, v3

    .line 40
    .line 41
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    mul-float v3, v3, v0

    .line 46
    .line 47
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 48
    .line 49
    .line 50
    move-result v3

    .line 51
    aget-object v4, p0, v1

    .line 52
    .line 53
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 54
    .line 55
    .line 56
    move-result v4

    .line 57
    mul-float v4, v4, v0

    .line 58
    .line 59
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 60
    .line 61
    .line 62
    move-result v4

    .line 63
    const/4 v5, 0x3

    .line 64
    aget-object p0, p0, v5

    .line 65
    .line 66
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 67
    .line 68
    .line 69
    move-result p0

    .line 70
    mul-float v0, v0, p0

    .line 71
    .line 72
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 73
    .line 74
    .line 75
    move-result p0

    .line 76
    mul-int/lit8 v0, v3, 0x2

    .line 77
    .line 78
    if-le v2, v0, :cond_0

    .line 79
    .line 80
    move v2, v0

    .line 81
    :cond_0
    add-int v0, v4, p0

    .line 82
    .line 83
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 84
    .line 85
    .line 86
    move-result v5

    .line 87
    if-le v0, v5, :cond_1

    .line 88
    .line 89
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 90
    .line 91
    .line 92
    move-result p0

    .line 93
    sub-int/2addr p0, v4

    .line 94
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    if-le p0, v0, :cond_3

    .line 99
    .line 100
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 101
    .line 102
    .line 103
    move-result p0

    .line 104
    goto :goto_0

    .line 105
    :cond_2
    int-to-float p0, v0

    .line 106
    const/high16 v0, 0x3e800000    # 0.25f

    .line 107
    .line 108
    mul-float v0, v0, p0

    .line 109
    .line 110
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 111
    .line 112
    .line 113
    move-result v2

    .line 114
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 115
    .line 116
    .line 117
    move-result v3

    .line 118
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 119
    .line 120
    .line 121
    move-result v4

    .line 122
    const/high16 v0, 0x3ee00000    # 0.4375f

    .line 123
    .line 124
    mul-float p0, p0, v0

    .line 125
    .line 126
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 127
    .line 128
    .line 129
    move-result p0

    .line 130
    :cond_3
    :goto_0
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 131
    .line 132
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 133
    .line 134
    int-to-float v5, v5

    .line 135
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 136
    .line 137
    int-to-float v6, v6

    .line 138
    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 139
    .line 140
    .line 141
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 142
    .line 143
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 144
    .line 145
    int-to-float v5, v5

    .line 146
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 147
    .line 148
    add-int/2addr v6, v3

    .line 149
    div-int/lit8 v7, v2, 0x2

    .line 150
    .line 151
    sub-int/2addr v6, v7

    .line 152
    add-int/2addr v6, p0

    .line 153
    int-to-float v6, v6

    .line 154
    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 155
    .line 156
    .line 157
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 158
    .line 159
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 160
    .line 161
    int-to-float v6, v5

    .line 162
    iget v8, p1, Landroid/graphics/Rect;->top:I

    .line 163
    .line 164
    add-int v9, v8, v3

    .line 165
    .line 166
    sub-int/2addr v9, v7

    .line 167
    int-to-float v9, v9

    .line 168
    mul-int/lit8 v10, p0, 0x2

    .line 169
    .line 170
    add-int/2addr v5, v10

    .line 171
    int-to-float v5, v5

    .line 172
    add-int/2addr v8, v3

    .line 173
    sub-int/2addr v8, v7

    .line 174
    add-int/2addr v8, v10

    .line 175
    int-to-float v8, v8

    .line 176
    invoke-virtual {v0, v6, v9, v5, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 177
    .line 178
    .line 179
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 180
    .line 181
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 182
    .line 183
    const/high16 v6, 0x43340000    # 180.0f

    .line 184
    .line 185
    const/high16 v8, 0x42b40000    # 90.0f

    .line 186
    .line 187
    invoke-virtual {v0, v5, v6, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 188
    .line 189
    .line 190
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 191
    .line 192
    iget v5, p1, Landroid/graphics/Rect;->right:I

    .line 193
    .line 194
    sub-int/2addr v5, v4

    .line 195
    int-to-float v5, v5

    .line 196
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 197
    .line 198
    add-int/2addr v6, v3

    .line 199
    sub-int/2addr v6, v7

    .line 200
    int-to-float v6, v6

    .line 201
    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 202
    .line 203
    .line 204
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 205
    .line 206
    iget v5, p1, Landroid/graphics/Rect;->right:I

    .line 207
    .line 208
    sub-int/2addr v5, v4

    .line 209
    int-to-float v5, v5

    .line 210
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 211
    .line 212
    int-to-float v6, v6

    .line 213
    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 214
    .line 215
    .line 216
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 217
    .line 218
    iget v5, p1, Landroid/graphics/Rect;->right:I

    .line 219
    .line 220
    int-to-float v5, v5

    .line 221
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 222
    .line 223
    add-int/2addr v6, v3

    .line 224
    int-to-float v6, v6

    .line 225
    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 226
    .line 227
    .line 228
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 229
    .line 230
    iget v5, p1, Landroid/graphics/Rect;->right:I

    .line 231
    .line 232
    sub-int/2addr v5, v4

    .line 233
    int-to-float v5, v5

    .line 234
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 235
    .line 236
    mul-int/lit8 v8, v3, 0x2

    .line 237
    .line 238
    add-int/2addr v6, v8

    .line 239
    int-to-float v6, v6

    .line 240
    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 241
    .line 242
    .line 243
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 244
    .line 245
    iget v5, p1, Landroid/graphics/Rect;->right:I

    .line 246
    .line 247
    sub-int/2addr v5, v4

    .line 248
    int-to-float v4, v5

    .line 249
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 250
    .line 251
    add-int/2addr v5, v3

    .line 252
    add-int/2addr v5, v7

    .line 253
    int-to-float v5, v5

    .line 254
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 255
    .line 256
    .line 257
    if-lt p0, v2, :cond_4

    .line 258
    .line 259
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 260
    .line 261
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 262
    .line 263
    add-int/2addr v4, p0

    .line 264
    int-to-float v4, v4

    .line 265
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 266
    .line 267
    add-int/2addr v5, v3

    .line 268
    add-int/2addr v5, v7

    .line 269
    int-to-float v5, v5

    .line 270
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 271
    .line 272
    .line 273
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 274
    .line 275
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 276
    .line 277
    add-int v5, v4, v2

    .line 278
    .line 279
    int-to-float v5, v5

    .line 280
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 281
    .line 282
    add-int v8, v6, v3

    .line 283
    .line 284
    add-int/2addr v8, v7

    .line 285
    int-to-float v8, v8

    .line 286
    sub-int v9, p0, v2

    .line 287
    .line 288
    mul-int/lit8 v9, v9, 0x2

    .line 289
    .line 290
    add-int/2addr v4, v9

    .line 291
    int-to-float v1, v4

    .line 292
    add-int/2addr v6, v3

    .line 293
    add-int/2addr v6, v7

    .line 294
    add-int/2addr v6, v9

    .line 295
    int-to-float v4, v6

    .line 296
    invoke-virtual {v0, v5, v8, v1, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 297
    .line 298
    .line 299
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 300
    .line 301
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 302
    .line 303
    const/high16 v4, 0x43870000    # 270.0f

    .line 304
    .line 305
    const/high16 v5, -0x3d4c0000    # -90.0f

    .line 306
    .line 307
    invoke-virtual {v0, v1, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 308
    .line 309
    .line 310
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 311
    .line 312
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 313
    .line 314
    add-int/2addr v1, v2

    .line 315
    int-to-float v1, v1

    .line 316
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 317
    .line 318
    add-int/2addr v4, v3

    .line 319
    sub-int/2addr v4, v7

    .line 320
    add-int/2addr v4, p0

    .line 321
    int-to-float p0, v4

    .line 322
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 323
    .line 324
    .line 325
    goto :goto_1

    .line 326
    :cond_4
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 327
    .line 328
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 329
    .line 330
    add-int/2addr v0, v2

    .line 331
    int-to-float v0, v0

    .line 332
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 333
    .line 334
    add-int/2addr v1, v3

    .line 335
    add-int/2addr v1, v7

    .line 336
    int-to-float v1, v1

    .line 337
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 338
    .line 339
    .line 340
    :goto_1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 341
    .line 342
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 343
    .line 344
    add-int/2addr v0, v2

    .line 345
    int-to-float v0, v0

    .line 346
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 347
    .line 348
    int-to-float p1, p1

    .line 349
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 350
    .line 351
    .line 352
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 353
    .line 354
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 355
    .line 356
    .line 357
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 358
    .line 359
    return-object p0
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getBentUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    array-length v1, p0

    .line 20
    const/4 v2, 0x3

    .line 21
    if-ne v1, v2, :cond_0

    .line 22
    .line 23
    int-to-float v0, v0

    .line 24
    const/4 v1, 0x0

    .line 25
    aget-object v1, p0, v1

    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    mul-float v1, v1, v0

    .line 32
    .line 33
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    const/4 v2, 0x1

    .line 38
    aget-object v2, p0, v2

    .line 39
    .line 40
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    mul-float v2, v2, v0

    .line 45
    .line 46
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    const/4 v3, 0x2

    .line 51
    aget-object p0, p0, v3

    .line 52
    .line 53
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 54
    .line 55
    .line 56
    move-result p0

    .line 57
    mul-float v0, v0, p0

    .line 58
    .line 59
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 60
    .line 61
    .line 62
    move-result p0

    .line 63
    goto :goto_0

    .line 64
    :cond_0
    int-to-float p0, v0

    .line 65
    const/high16 v0, 0x3e800000    # 0.25f

    .line 66
    .line 67
    mul-float p0, p0, v0

    .line 68
    .line 69
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 70
    .line 71
    .line 72
    move-result v1

    .line 73
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 74
    .line 75
    .line 76
    move-result v2

    .line 77
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 78
    .line 79
    .line 80
    move-result p0

    .line 81
    :goto_0
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 82
    .line 83
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 84
    .line 85
    int-to-float v3, v3

    .line 86
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 87
    .line 88
    int-to-float v4, v4

    .line 89
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 90
    .line 91
    .line 92
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 93
    .line 94
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 95
    .line 96
    int-to-float v3, v3

    .line 97
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 98
    .line 99
    sub-int/2addr v4, v1

    .line 100
    int-to-float v4, v4

    .line 101
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 102
    .line 103
    .line 104
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 105
    .line 106
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 107
    .line 108
    sub-int/2addr v3, v2

    .line 109
    div-int/lit8 v4, v1, 0x2

    .line 110
    .line 111
    sub-int/2addr v3, v4

    .line 112
    int-to-float v3, v3

    .line 113
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 114
    .line 115
    sub-int/2addr v5, v1

    .line 116
    int-to-float v1, v5

    .line 117
    invoke-virtual {v0, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 118
    .line 119
    .line 120
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 121
    .line 122
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 123
    .line 124
    sub-int/2addr v1, v2

    .line 125
    sub-int/2addr v1, v4

    .line 126
    int-to-float v1, v1

    .line 127
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 128
    .line 129
    add-int/2addr v3, p0

    .line 130
    int-to-float v3, v3

    .line 131
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 132
    .line 133
    .line 134
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 135
    .line 136
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 137
    .line 138
    mul-int/lit8 v3, v2, 0x2

    .line 139
    .line 140
    sub-int/2addr v1, v3

    .line 141
    int-to-float v1, v1

    .line 142
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 143
    .line 144
    add-int/2addr v3, p0

    .line 145
    int-to-float v3, v3

    .line 146
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 147
    .line 148
    .line 149
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 150
    .line 151
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 152
    .line 153
    sub-int/2addr v1, v2

    .line 154
    int-to-float v1, v1

    .line 155
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 156
    .line 157
    int-to-float v3, v3

    .line 158
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 159
    .line 160
    .line 161
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 162
    .line 163
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 164
    .line 165
    int-to-float v1, v1

    .line 166
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 167
    .line 168
    add-int/2addr v3, p0

    .line 169
    int-to-float v3, v3

    .line 170
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 171
    .line 172
    .line 173
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 174
    .line 175
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 176
    .line 177
    sub-int/2addr v1, v2

    .line 178
    add-int/2addr v1, v4

    .line 179
    int-to-float v1, v1

    .line 180
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 181
    .line 182
    add-int/2addr v3, p0

    .line 183
    int-to-float p0, v3

    .line 184
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 185
    .line 186
    .line 187
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 188
    .line 189
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 190
    .line 191
    sub-int/2addr v0, v2

    .line 192
    add-int/2addr v0, v4

    .line 193
    int-to-float v0, v0

    .line 194
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 195
    .line 196
    int-to-float p1, p1

    .line 197
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 198
    .line 199
    .line 200
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 201
    .line 202
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 203
    .line 204
    .line 205
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 206
    .line 207
    return-object p0
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getChevronPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    array-length v1, p0

    .line 20
    const/4 v2, 0x1

    .line 21
    if-ne v1, v2, :cond_0

    .line 22
    .line 23
    int-to-float v0, v0

    .line 24
    const/4 v1, 0x0

    .line 25
    aget-object p0, p0, v1

    .line 26
    .line 27
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result p0

    .line 31
    mul-float v0, v0, p0

    .line 32
    .line 33
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 34
    .line 35
    .line 36
    move-result p0

    .line 37
    goto :goto_0

    .line 38
    :cond_0
    int-to-float p0, v0

    .line 39
    const/high16 v0, 0x3f000000    # 0.5f

    .line 40
    .line 41
    mul-float p0, p0, v0

    .line 42
    .line 43
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 44
    .line 45
    .line 46
    move-result p0

    .line 47
    :goto_0
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 48
    .line 49
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 50
    .line 51
    int-to-float v1, v1

    .line 52
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 53
    .line 54
    int-to-float v2, v2

    .line 55
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 56
    .line 57
    .line 58
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 59
    .line 60
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 61
    .line 62
    sub-int/2addr v1, p0

    .line 63
    int-to-float v1, v1

    .line 64
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 65
    .line 66
    int-to-float v2, v2

    .line 67
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 68
    .line 69
    .line 70
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 71
    .line 72
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 73
    .line 74
    int-to-float v1, v1

    .line 75
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 76
    .line 77
    .line 78
    move-result v2

    .line 79
    int-to-float v2, v2

    .line 80
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 81
    .line 82
    .line 83
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 84
    .line 85
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 86
    .line 87
    sub-int/2addr v1, p0

    .line 88
    int-to-float v1, v1

    .line 89
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 90
    .line 91
    int-to-float v2, v2

    .line 92
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 93
    .line 94
    .line 95
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 96
    .line 97
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 98
    .line 99
    int-to-float v1, v1

    .line 100
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 101
    .line 102
    int-to-float v2, v2

    .line 103
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 104
    .line 105
    .line 106
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 107
    .line 108
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 109
    .line 110
    add-int/2addr v1, p0

    .line 111
    int-to-float p0, v1

    .line 112
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 113
    .line 114
    .line 115
    move-result p1

    .line 116
    int-to-float p1, p1

    .line 117
    invoke-virtual {v0, p0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 118
    .line 119
    .line 120
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 121
    .line 122
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 123
    .line 124
    .line 125
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 126
    .line 127
    return-object p0
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getCircularArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 28

    .line 1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x2

    .line 6
    const/16 v2, 0x64

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    array-length v3, v0

    .line 11
    const/4 v4, 0x5

    .line 12
    if-ne v3, v4, :cond_0

    .line 13
    .line 14
    int-to-float v2, v2

    .line 15
    const/4 v3, 0x0

    .line 16
    aget-object v3, v0, v3

    .line 17
    .line 18
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    mul-float v3, v3, v2

    .line 23
    .line 24
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    const/4 v4, 0x1

    .line 29
    aget-object v4, v0, v4

    .line 30
    .line 31
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 32
    .line 33
    .line 34
    move-result v4

    .line 35
    const v5, 0x3fd55555

    .line 36
    .line 37
    .line 38
    mul-float v4, v4, v5

    .line 39
    .line 40
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 41
    .line 42
    .line 43
    move-result v4

    .line 44
    aget-object v6, v0, v1

    .line 45
    .line 46
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 47
    .line 48
    .line 49
    move-result v6

    .line 50
    mul-float v6, v6, v5

    .line 51
    .line 52
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 53
    .line 54
    .line 55
    move-result v6

    .line 56
    const/4 v7, 0x3

    .line 57
    aget-object v7, v0, v7

    .line 58
    .line 59
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 60
    .line 61
    .line 62
    move-result v7

    .line 63
    mul-float v7, v7, v5

    .line 64
    .line 65
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 66
    .line 67
    .line 68
    move-result v5

    .line 69
    const/4 v7, 0x4

    .line 70
    aget-object v0, v0, v7

    .line 71
    .line 72
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    mul-float v2, v2, v0

    .line 77
    .line 78
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 79
    .line 80
    .line 81
    move-result v0

    .line 82
    goto :goto_0

    .line 83
    :cond_0
    int-to-float v0, v2

    .line 84
    const/high16 v2, 0x3e000000    # 0.125f

    .line 85
    .line 86
    mul-float v0, v0, v2

    .line 87
    .line 88
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 89
    .line 90
    .line 91
    move-result v3

    .line 92
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 93
    .line 94
    .line 95
    move-result v0

    .line 96
    const/16 v4, 0x14

    .line 97
    .line 98
    const/16 v6, 0x154

    .line 99
    .line 100
    const/16 v5, 0xb4

    .line 101
    .line 102
    :goto_0
    rsub-int/lit8 v2, v0, 0x32

    .line 103
    .line 104
    int-to-double v7, v2

    .line 105
    int-to-double v9, v6

    .line 106
    const-wide v11, 0x400921fb54442d18L    # Math.PI

    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    mul-double v9, v9, v11

    .line 112
    .line 113
    const-wide v13, 0x4066800000000000L    # 180.0

    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    div-double/2addr v9, v13

    .line 119
    invoke-static {v9, v10}, Ljava/lang/Math;->sin(D)D

    .line 120
    .line 121
    .line 122
    move-result-wide v15

    .line 123
    mul-double v15, v15, v7

    .line 124
    .line 125
    invoke-static {v9, v10}, Ljava/lang/Math;->cos(D)D

    .line 126
    .line 127
    .line 128
    move-result-wide v9

    .line 129
    mul-double v9, v9, v7

    .line 130
    .line 131
    add-int/2addr v4, v6

    .line 132
    move/from16 v17, v2

    .line 133
    .line 134
    int-to-double v1, v4

    .line 135
    mul-double v1, v1, v11

    .line 136
    .line 137
    div-double/2addr v1, v13

    .line 138
    invoke-static {v1, v2}, Ljava/lang/Math;->tan(D)D

    .line 139
    .line 140
    .line 141
    move-result-wide v11

    .line 142
    mul-double v13, v11, v9

    .line 143
    .line 144
    sub-double/2addr v15, v13

    .line 145
    int-to-double v13, v0

    .line 146
    move-wide/from16 v18, v7

    .line 147
    .line 148
    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    .line 149
    .line 150
    invoke-static {v13, v14, v7, v8}, Ljava/lang/Math;->pow(DD)D

    .line 151
    .line 152
    .line 153
    move-result-wide v13

    .line 154
    invoke-static {v11, v12, v7, v8}, Ljava/lang/Math;->pow(DD)D

    .line 155
    .line 156
    .line 157
    move-result-wide v20

    .line 158
    const-wide/high16 v22, 0x3ff0000000000000L    # 1.0

    .line 159
    .line 160
    add-double v20, v20, v22

    .line 161
    .line 162
    div-double v13, v13, v20

    .line 163
    .line 164
    invoke-static {v13, v14}, Ljava/lang/Math;->sqrt(D)D

    .line 165
    .line 166
    .line 167
    move-result-wide v13

    .line 168
    const/4 v4, 0x2

    .line 169
    div-int/2addr v3, v4

    .line 170
    move-wide/from16 v20, v1

    .line 171
    .line 172
    int-to-double v1, v3

    .line 173
    invoke-static {v1, v2, v7, v8}, Ljava/lang/Math;->pow(DD)D

    .line 174
    .line 175
    .line 176
    move-result-wide v1

    .line 177
    invoke-static {v11, v12, v7, v8}, Ljava/lang/Math;->pow(DD)D

    .line 178
    .line 179
    .line 180
    move-result-wide v7

    .line 181
    add-double v7, v7, v22

    .line 182
    .line 183
    div-double/2addr v1, v7

    .line 184
    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    .line 185
    .line 186
    .line 187
    move-result-wide v1

    .line 188
    const/16 v4, 0x5a

    .line 189
    .line 190
    if-le v6, v4, :cond_1

    .line 191
    .line 192
    const/16 v4, 0x10e

    .line 193
    .line 194
    if-ge v6, v4, :cond_1

    .line 195
    .line 196
    neg-double v13, v13

    .line 197
    neg-double v1, v1

    .line 198
    :cond_1
    add-double v6, v9, v1

    .line 199
    .line 200
    mul-double v22, v11, v6

    .line 201
    .line 202
    move-wide/from16 v24, v13

    .line 203
    .line 204
    add-double v13, v22, v15

    .line 205
    .line 206
    invoke-static {v6, v7, v13, v14}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getAngle(DD)D

    .line 207
    .line 208
    .line 209
    move-result-wide v6

    .line 210
    sub-double v1, v9, v1

    .line 211
    .line 212
    mul-double v13, v11, v1

    .line 213
    .line 214
    add-double/2addr v13, v15

    .line 215
    invoke-static {v1, v2, v13, v14}, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->getAngle(DD)D

    .line 216
    .line 217
    .line 218
    move-result-wide v1

    .line 219
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 220
    .line 221
    sub-int v8, v0, v3

    .line 222
    .line 223
    add-int/lit8 v8, v8, -0x32

    .line 224
    .line 225
    int-to-float v8, v8

    .line 226
    add-int v13, v17, v3

    .line 227
    .line 228
    int-to-float v13, v13

    .line 229
    invoke-virtual {v4, v8, v8, v13, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 230
    .line 231
    .line 232
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 233
    .line 234
    sget-object v8, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 235
    .line 236
    int-to-float v13, v5

    .line 237
    move-wide/from16 v22, v1

    .line 238
    .line 239
    int-to-double v1, v5

    .line 240
    sub-double/2addr v6, v1

    .line 241
    const-wide v26, 0x4076800000000000L    # 360.0

    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    add-double v6, v6, v26

    .line 247
    .line 248
    double-to-float v5, v6

    .line 249
    const/high16 v6, 0x43b40000    # 360.0f

    .line 250
    .line 251
    rem-float/2addr v5, v6

    .line 252
    invoke-virtual {v4, v8, v13, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 253
    .line 254
    .line 255
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 256
    .line 257
    add-double v13, v9, v24

    .line 258
    .line 259
    double-to-float v5, v13

    .line 260
    mul-double v13, v13, v11

    .line 261
    .line 262
    add-double/2addr v13, v15

    .line 263
    double-to-float v7, v13

    .line 264
    invoke-virtual {v4, v5, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 265
    .line 266
    .line 267
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 268
    .line 269
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->cos(D)D

    .line 270
    .line 271
    .line 272
    move-result-wide v7

    .line 273
    mul-double v7, v7, v18

    .line 274
    .line 275
    double-to-float v5, v7

    .line 276
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->sin(D)D

    .line 277
    .line 278
    .line 279
    move-result-wide v7

    .line 280
    mul-double v7, v7, v18

    .line 281
    .line 282
    double-to-float v7, v7

    .line 283
    invoke-virtual {v4, v5, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 284
    .line 285
    .line 286
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 287
    .line 288
    sub-double v9, v9, v24

    .line 289
    .line 290
    double-to-float v5, v9

    .line 291
    mul-double v11, v11, v9

    .line 292
    .line 293
    add-double/2addr v11, v15

    .line 294
    double-to-float v7, v11

    .line 295
    invoke-virtual {v4, v5, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 296
    .line 297
    .line 298
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 299
    .line 300
    add-int/2addr v0, v3

    .line 301
    add-int/lit8 v0, v0, -0x32

    .line 302
    .line 303
    int-to-float v0, v0

    .line 304
    sub-int v3, v17, v3

    .line 305
    .line 306
    int-to-float v3, v3

    .line 307
    invoke-virtual {v4, v0, v0, v3, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 308
    .line 309
    .line 310
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 311
    .line 312
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 313
    .line 314
    move-wide/from16 v4, v22

    .line 315
    .line 316
    double-to-float v7, v4

    .line 317
    sub-double/2addr v1, v4

    .line 318
    sub-double v1, v1, v26

    .line 319
    .line 320
    double-to-float v1, v1

    .line 321
    rem-float/2addr v1, v6

    .line 322
    invoke-virtual {v0, v3, v7, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 323
    .line 324
    .line 325
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 326
    .line 327
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 328
    .line 329
    .line 330
    new-instance v0, Landroid/graphics/Matrix;

    .line 331
    .line 332
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 333
    .line 334
    .line 335
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 336
    .line 337
    .line 338
    move-result v1

    .line 339
    int-to-float v1, v1

    .line 340
    const/high16 v2, 0x42c80000    # 100.0f

    .line 341
    .line 342
    div-float/2addr v1, v2

    .line 343
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 344
    .line 345
    .line 346
    move-result v3

    .line 347
    int-to-float v3, v3

    .line 348
    div-float/2addr v3, v2

    .line 349
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 350
    .line 351
    .line 352
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 353
    .line 354
    invoke-virtual {v1, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 355
    .line 356
    .line 357
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 358
    .line 359
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 360
    .line 361
    .line 362
    move-result v1

    .line 363
    int-to-float v1, v1

    .line 364
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 365
    .line 366
    .line 367
    move-result v2

    .line 368
    int-to-float v2, v2

    .line 369
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->offset(FF)V

    .line 370
    .line 371
    .line 372
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 373
    .line 374
    return-object v0
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getCurvedDownArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    new-instance v1, Ljava/util/ArrayList;

    .line 4
    .line 5
    const/4 v2, 0x2

    .line 6
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 7
    .line 8
    .line 9
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    new-instance v4, Landroid/graphics/Path;

    .line 14
    .line 15
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 16
    .line 17
    .line 18
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 19
    .line 20
    .line 21
    move-result v5

    .line 22
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 23
    .line 24
    .line 25
    move-result v6

    .line 26
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    .line 27
    .line 28
    .line 29
    move-result v5

    .line 30
    if-eqz v3, :cond_0

    .line 31
    .line 32
    array-length v6, v3

    .line 33
    const/4 v7, 0x3

    .line 34
    if-ne v6, v7, :cond_0

    .line 35
    .line 36
    int-to-float v5, v5

    .line 37
    const/4 v6, 0x0

    .line 38
    aget-object v6, v3, v6

    .line 39
    .line 40
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 41
    .line 42
    .line 43
    move-result v6

    .line 44
    mul-float v6, v6, v5

    .line 45
    .line 46
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 47
    .line 48
    .line 49
    move-result v6

    .line 50
    const/4 v7, 0x1

    .line 51
    aget-object v7, v3, v7

    .line 52
    .line 53
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 54
    .line 55
    .line 56
    move-result v7

    .line 57
    mul-float v7, v7, v5

    .line 58
    .line 59
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 60
    .line 61
    .line 62
    move-result v7

    .line 63
    aget-object v3, v3, v2

    .line 64
    .line 65
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 66
    .line 67
    .line 68
    move-result v3

    .line 69
    mul-float v5, v5, v3

    .line 70
    .line 71
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 72
    .line 73
    .line 74
    move-result v3

    .line 75
    goto :goto_0

    .line 76
    :cond_0
    int-to-float v3, v5

    .line 77
    const/high16 v5, 0x3e800000    # 0.25f

    .line 78
    .line 79
    mul-float v5, v5, v3

    .line 80
    .line 81
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 82
    .line 83
    .line 84
    move-result v6

    .line 85
    const/high16 v7, 0x3f000000    # 0.5f

    .line 86
    .line 87
    mul-float v3, v3, v7

    .line 88
    .line 89
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 90
    .line 91
    .line 92
    move-result v7

    .line 93
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 94
    .line 95
    .line 96
    move-result v3

    .line 97
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 98
    .line 99
    .line 100
    move-result v5

    .line 101
    div-int/2addr v7, v2

    .line 102
    sub-int/2addr v5, v7

    .line 103
    div-int/lit8 v8, v6, 0x2

    .line 104
    .line 105
    sub-int/2addr v5, v8

    .line 106
    div-int/2addr v5, v2

    .line 107
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 108
    .line 109
    .line 110
    move-result v2

    .line 111
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 112
    .line 113
    int-to-float v9, v9

    .line 114
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    .line 115
    .line 116
    int-to-float v10, v10

    .line 117
    invoke-virtual {v4, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 118
    .line 119
    .line 120
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 121
    .line 122
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 123
    .line 124
    int-to-float v11, v10

    .line 125
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 126
    .line 127
    int-to-float v13, v12

    .line 128
    mul-int/lit8 v14, v5, 0x2

    .line 129
    .line 130
    add-int/2addr v10, v14

    .line 131
    int-to-float v10, v10

    .line 132
    mul-int/lit8 v15, v2, 0x2

    .line 133
    .line 134
    add-int/2addr v12, v15

    .line 135
    int-to-float v12, v12

    .line 136
    invoke-virtual {v9, v11, v13, v10, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 137
    .line 138
    .line 139
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 140
    .line 141
    const/high16 v10, 0x43340000    # 180.0f

    .line 142
    .line 143
    const/high16 v11, 0x42b40000    # 90.0f

    .line 144
    .line 145
    invoke-virtual {v4, v9, v10, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 146
    .line 147
    .line 148
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 149
    .line 150
    add-int/2addr v9, v5

    .line 151
    add-int/2addr v9, v6

    .line 152
    int-to-float v9, v9

    .line 153
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 154
    .line 155
    int-to-float v10, v10

    .line 156
    invoke-virtual {v4, v9, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 157
    .line 158
    .line 159
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 160
    .line 161
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 162
    .line 163
    add-int v11, v10, v6

    .line 164
    .line 165
    int-to-float v11, v11

    .line 166
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 167
    .line 168
    int-to-float v13, v12

    .line 169
    add-int/2addr v10, v14

    .line 170
    add-int/2addr v10, v6

    .line 171
    int-to-float v10, v10

    .line 172
    add-int/2addr v12, v15

    .line 173
    int-to-float v12, v12

    .line 174
    invoke-virtual {v9, v11, v13, v10, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 175
    .line 176
    .line 177
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 178
    .line 179
    const/high16 v10, -0x3d4c0000    # -90.0f

    .line 180
    .line 181
    const/high16 v11, 0x43870000    # 270.0f

    .line 182
    .line 183
    invoke-virtual {v4, v9, v11, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 184
    .line 185
    .line 186
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 187
    .line 188
    .line 189
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    .line 191
    .line 192
    new-instance v4, Landroid/graphics/Path;

    .line 193
    .line 194
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 195
    .line 196
    .line 197
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 198
    .line 199
    add-int/2addr v9, v5

    .line 200
    int-to-float v9, v9

    .line 201
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 202
    .line 203
    int-to-float v10, v10

    .line 204
    invoke-virtual {v4, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 205
    .line 206
    .line 207
    int-to-double v9, v5

    .line 208
    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    .line 209
    .line 210
    invoke-static {v9, v10, v12, v13}, Ljava/lang/Math;->pow(DD)D

    .line 211
    .line 212
    .line 213
    move-result-wide v9

    .line 214
    move-object/from16 v16, v1

    .line 215
    .line 216
    int-to-double v1, v2

    .line 217
    invoke-static {v1, v2, v12, v13}, Ljava/lang/Math;->pow(DD)D

    .line 218
    .line 219
    .line 220
    move-result-wide v17

    .line 221
    move/from16 p0, v6

    .line 222
    .line 223
    move/from16 v19, v7

    .line 224
    .line 225
    int-to-double v6, v3

    .line 226
    invoke-static {v6, v7, v12, v13}, Ljava/lang/Math;->pow(DD)D

    .line 227
    .line 228
    .line 229
    move-result-wide v20

    .line 230
    sub-double v17, v17, v20

    .line 231
    .line 232
    mul-double v9, v9, v17

    .line 233
    .line 234
    invoke-static {v1, v2, v12, v13}, Ljava/lang/Math;->pow(DD)D

    .line 235
    .line 236
    .line 237
    move-result-wide v1

    .line 238
    div-double/2addr v9, v1

    .line 239
    invoke-static {v9, v10}, Ljava/lang/Math;->sqrt(D)D

    .line 240
    .line 241
    .line 242
    move-result-wide v1

    .line 243
    div-double v6, v1, v6

    .line 244
    .line 245
    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    .line 246
    .line 247
    .line 248
    move-result-wide v6

    .line 249
    const-wide v9, 0x4066800000000000L    # 180.0

    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    mul-double v6, v6, v9

    .line 255
    .line 256
    const-wide v9, 0x400921fb54442d18L    # Math.PI

    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    div-double/2addr v6, v9

    .line 262
    double-to-int v6, v6

    .line 263
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 264
    .line 265
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 266
    .line 267
    int-to-float v10, v9

    .line 268
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 269
    .line 270
    int-to-float v13, v12

    .line 271
    add-int/2addr v9, v14

    .line 272
    int-to-float v9, v9

    .line 273
    add-int/2addr v12, v15

    .line 274
    int-to-float v12, v12

    .line 275
    invoke-virtual {v7, v10, v13, v9, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 276
    .line 277
    .line 278
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 279
    .line 280
    int-to-float v9, v6

    .line 281
    invoke-virtual {v4, v7, v11, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 282
    .line 283
    .line 284
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 285
    .line 286
    add-int/2addr v7, v5

    .line 287
    int-to-float v7, v7

    .line 288
    double-to-float v1, v1

    .line 289
    add-float/2addr v7, v1

    .line 290
    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 291
    .line 292
    sub-int/2addr v2, v3

    .line 293
    int-to-float v2, v2

    .line 294
    invoke-virtual {v4, v7, v2}, Landroid/graphics/Path;->setLastPoint(FF)V

    .line 295
    .line 296
    .line 297
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 298
    .line 299
    add-int/2addr v2, v5

    .line 300
    int-to-float v2, v2

    .line 301
    add-float/2addr v2, v1

    .line 302
    int-to-float v7, v8

    .line 303
    add-float/2addr v2, v7

    .line 304
    move/from16 v8, v19

    .line 305
    .line 306
    int-to-float v9, v8

    .line 307
    sub-float/2addr v2, v9

    .line 308
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    .line 309
    .line 310
    sub-int/2addr v10, v3

    .line 311
    int-to-float v10, v10

    .line 312
    invoke-virtual {v4, v2, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 313
    .line 314
    .line 315
    iget v2, v0, Landroid/graphics/Rect;->right:I

    .line 316
    .line 317
    sub-int/2addr v2, v8

    .line 318
    int-to-float v2, v2

    .line 319
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    .line 320
    .line 321
    int-to-float v8, v8

    .line 322
    invoke-virtual {v4, v2, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 323
    .line 324
    .line 325
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 326
    .line 327
    add-int/2addr v2, v5

    .line 328
    int-to-float v2, v2

    .line 329
    add-float/2addr v2, v1

    .line 330
    add-float/2addr v2, v7

    .line 331
    add-float/2addr v2, v9

    .line 332
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    .line 333
    .line 334
    sub-int/2addr v7, v3

    .line 335
    int-to-float v7, v7

    .line 336
    invoke-virtual {v4, v2, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 337
    .line 338
    .line 339
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 340
    .line 341
    add-int/2addr v2, v5

    .line 342
    int-to-float v2, v2

    .line 343
    add-float/2addr v2, v1

    .line 344
    move/from16 v1, p0

    .line 345
    .line 346
    int-to-float v5, v1

    .line 347
    add-float/2addr v2, v5

    .line 348
    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 349
    .line 350
    sub-int/2addr v5, v3

    .line 351
    int-to-float v3, v5

    .line 352
    invoke-virtual {v4, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 353
    .line 354
    .line 355
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 356
    .line 357
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 358
    .line 359
    add-int v5, v3, v1

    .line 360
    .line 361
    int-to-float v5, v5

    .line 362
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 363
    .line 364
    int-to-float v7, v0

    .line 365
    add-int/2addr v3, v14

    .line 366
    add-int/2addr v3, v1

    .line 367
    int-to-float v1, v3

    .line 368
    add-int/2addr v0, v15

    .line 369
    int-to-float v0, v0

    .line 370
    invoke-virtual {v2, v5, v7, v1, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 371
    .line 372
    .line 373
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 374
    .line 375
    add-int/lit16 v1, v6, 0x10e

    .line 376
    .line 377
    int-to-float v1, v1

    .line 378
    neg-int v2, v6

    .line 379
    int-to-float v2, v2

    .line 380
    invoke-virtual {v4, v0, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 381
    .line 382
    .line 383
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 384
    .line 385
    .line 386
    move-object/from16 v0, v16

    .line 387
    .line 388
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389
    .line 390
    .line 391
    return-object v0
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getCurvedLeftArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    new-instance v1, Ljava/util/ArrayList;

    .line 4
    .line 5
    const/4 v2, 0x2

    .line 6
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 7
    .line 8
    .line 9
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    new-instance v4, Landroid/graphics/Path;

    .line 14
    .line 15
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 16
    .line 17
    .line 18
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 19
    .line 20
    .line 21
    move-result v5

    .line 22
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 23
    .line 24
    .line 25
    move-result v6

    .line 26
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    .line 27
    .line 28
    .line 29
    move-result v5

    .line 30
    if-eqz v3, :cond_0

    .line 31
    .line 32
    array-length v6, v3

    .line 33
    const/4 v7, 0x3

    .line 34
    if-ne v6, v7, :cond_0

    .line 35
    .line 36
    int-to-float v5, v5

    .line 37
    const/4 v6, 0x0

    .line 38
    aget-object v6, v3, v6

    .line 39
    .line 40
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 41
    .line 42
    .line 43
    move-result v6

    .line 44
    mul-float v6, v6, v5

    .line 45
    .line 46
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 47
    .line 48
    .line 49
    move-result v6

    .line 50
    const/4 v7, 0x1

    .line 51
    aget-object v7, v3, v7

    .line 52
    .line 53
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 54
    .line 55
    .line 56
    move-result v7

    .line 57
    mul-float v7, v7, v5

    .line 58
    .line 59
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 60
    .line 61
    .line 62
    move-result v7

    .line 63
    aget-object v3, v3, v2

    .line 64
    .line 65
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 66
    .line 67
    .line 68
    move-result v3

    .line 69
    mul-float v5, v5, v3

    .line 70
    .line 71
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 72
    .line 73
    .line 74
    move-result v3

    .line 75
    goto :goto_0

    .line 76
    :cond_0
    int-to-float v3, v5

    .line 77
    const/high16 v5, 0x3e800000    # 0.25f

    .line 78
    .line 79
    mul-float v5, v5, v3

    .line 80
    .line 81
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 82
    .line 83
    .line 84
    move-result v6

    .line 85
    const/high16 v7, 0x3f000000    # 0.5f

    .line 86
    .line 87
    mul-float v3, v3, v7

    .line 88
    .line 89
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 90
    .line 91
    .line 92
    move-result v7

    .line 93
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 94
    .line 95
    .line 96
    move-result v3

    .line 97
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 98
    .line 99
    .line 100
    move-result v5

    .line 101
    mul-int/lit8 v5, v5, 0x2

    .line 102
    .line 103
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    .line 104
    .line 105
    div-int/2addr v7, v2

    .line 106
    sub-int/2addr v8, v7

    .line 107
    div-int/lit8 v2, v6, 0x2

    .line 108
    .line 109
    sub-int/2addr v8, v2

    .line 110
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 111
    .line 112
    sub-int/2addr v8, v9

    .line 113
    iget v10, v0, Landroid/graphics/Rect;->right:I

    .line 114
    .line 115
    int-to-float v10, v10

    .line 116
    div-int/lit8 v11, v8, 0x2

    .line 117
    .line 118
    add-int/2addr v9, v11

    .line 119
    int-to-float v9, v9

    .line 120
    invoke-virtual {v4, v10, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 121
    .line 122
    .line 123
    int-to-double v9, v11

    .line 124
    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    .line 125
    .line 126
    invoke-static {v9, v10, v12, v13}, Ljava/lang/Math;->pow(DD)D

    .line 127
    .line 128
    .line 129
    move-result-wide v9

    .line 130
    div-int/lit8 v14, v5, 0x2

    .line 131
    .line 132
    int-to-double v14, v14

    .line 133
    invoke-static {v14, v15, v12, v13}, Ljava/lang/Math;->pow(DD)D

    .line 134
    .line 135
    .line 136
    move-result-wide v16

    .line 137
    move/from16 p0, v6

    .line 138
    .line 139
    move/from16 v18, v7

    .line 140
    .line 141
    int-to-double v6, v3

    .line 142
    invoke-static {v6, v7, v12, v13}, Ljava/lang/Math;->pow(DD)D

    .line 143
    .line 144
    .line 145
    move-result-wide v19

    .line 146
    sub-double v16, v16, v19

    .line 147
    .line 148
    mul-double v9, v9, v16

    .line 149
    .line 150
    invoke-static {v14, v15, v12, v13}, Ljava/lang/Math;->pow(DD)D

    .line 151
    .line 152
    .line 153
    move-result-wide v12

    .line 154
    div-double/2addr v9, v12

    .line 155
    invoke-static {v9, v10}, Ljava/lang/Math;->sqrt(D)D

    .line 156
    .line 157
    .line 158
    move-result-wide v9

    .line 159
    div-double v6, v9, v6

    .line 160
    .line 161
    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    .line 162
    .line 163
    .line 164
    move-result-wide v6

    .line 165
    const-wide v12, 0x4066800000000000L    # 180.0

    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    mul-double v6, v6, v12

    .line 171
    .line 172
    const-wide v12, 0x400921fb54442d18L    # Math.PI

    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    div-double/2addr v6, v12

    .line 178
    double-to-int v6, v6

    .line 179
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 180
    .line 181
    iget v12, v0, Landroid/graphics/Rect;->right:I

    .line 182
    .line 183
    sub-int v13, v12, v5

    .line 184
    .line 185
    int-to-float v13, v13

    .line 186
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 187
    .line 188
    int-to-float v15, v14

    .line 189
    int-to-float v12, v12

    .line 190
    add-int/2addr v14, v8

    .line 191
    int-to-float v14, v14

    .line 192
    invoke-virtual {v7, v13, v15, v12, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 193
    .line 194
    .line 195
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 196
    .line 197
    int-to-float v12, v6

    .line 198
    const/4 v13, 0x0

    .line 199
    invoke-virtual {v4, v7, v13, v12}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 200
    .line 201
    .line 202
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 203
    .line 204
    add-int/2addr v7, v3

    .line 205
    int-to-float v7, v7

    .line 206
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 207
    .line 208
    add-int/2addr v14, v11

    .line 209
    double-to-int v9, v9

    .line 210
    add-int/2addr v14, v9

    .line 211
    int-to-float v10, v14

    .line 212
    invoke-virtual {v4, v7, v10}, Landroid/graphics/Path;->setLastPoint(FF)V

    .line 213
    .line 214
    .line 215
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 216
    .line 217
    add-int/2addr v7, v3

    .line 218
    int-to-float v7, v7

    .line 219
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 220
    .line 221
    add-int/2addr v10, v11

    .line 222
    add-int/2addr v10, v9

    .line 223
    add-int/2addr v10, v2

    .line 224
    sub-int v10, v10, v18

    .line 225
    .line 226
    int-to-float v10, v10

    .line 227
    invoke-virtual {v4, v7, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 228
    .line 229
    .line 230
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 231
    .line 232
    int-to-float v7, v7

    .line 233
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    .line 234
    .line 235
    sub-int v10, v10, v18

    .line 236
    .line 237
    int-to-float v10, v10

    .line 238
    invoke-virtual {v4, v7, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 239
    .line 240
    .line 241
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 242
    .line 243
    add-int/2addr v7, v3

    .line 244
    int-to-float v7, v7

    .line 245
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 246
    .line 247
    add-int/2addr v10, v11

    .line 248
    add-int/2addr v10, v9

    .line 249
    add-int/2addr v10, v2

    .line 250
    add-int v10, v10, v18

    .line 251
    .line 252
    int-to-float v2, v10

    .line 253
    invoke-virtual {v4, v7, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 254
    .line 255
    .line 256
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 257
    .line 258
    add-int/2addr v2, v3

    .line 259
    int-to-float v2, v2

    .line 260
    iget v3, v0, Landroid/graphics/Rect;->top:I

    .line 261
    .line 262
    add-int/2addr v3, v11

    .line 263
    add-int/2addr v3, v9

    .line 264
    add-int v3, v3, p0

    .line 265
    .line 266
    int-to-float v3, v3

    .line 267
    invoke-virtual {v4, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 268
    .line 269
    .line 270
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 271
    .line 272
    iget v3, v0, Landroid/graphics/Rect;->right:I

    .line 273
    .line 274
    sub-int v7, v3, v5

    .line 275
    .line 276
    int-to-float v7, v7

    .line 277
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 278
    .line 279
    add-int v10, v9, p0

    .line 280
    .line 281
    int-to-float v10, v10

    .line 282
    int-to-float v3, v3

    .line 283
    add-int/2addr v9, v8

    .line 284
    add-int v9, v9, p0

    .line 285
    .line 286
    int-to-float v9, v9

    .line 287
    invoke-virtual {v2, v7, v10, v3, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 288
    .line 289
    .line 290
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 291
    .line 292
    neg-int v3, v6

    .line 293
    int-to-float v3, v3

    .line 294
    invoke-virtual {v4, v2, v12, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 295
    .line 296
    .line 297
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 298
    .line 299
    .line 300
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    .line 302
    .line 303
    new-instance v2, Landroid/graphics/Path;

    .line 304
    .line 305
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 306
    .line 307
    .line 308
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 309
    .line 310
    int-to-float v3, v3

    .line 311
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 312
    .line 313
    int-to-float v4, v4

    .line 314
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 315
    .line 316
    .line 317
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 318
    .line 319
    iget v4, v0, Landroid/graphics/Rect;->right:I

    .line 320
    .line 321
    sub-int v6, v4, v5

    .line 322
    .line 323
    int-to-float v6, v6

    .line 324
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 325
    .line 326
    int-to-float v9, v7

    .line 327
    int-to-float v4, v4

    .line 328
    add-int/2addr v7, v8

    .line 329
    int-to-float v7, v7

    .line 330
    invoke-virtual {v3, v6, v9, v4, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 331
    .line 332
    .line 333
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 334
    .line 335
    const/high16 v4, 0x43870000    # 270.0f

    .line 336
    .line 337
    const/high16 v6, 0x42b40000    # 90.0f

    .line 338
    .line 339
    invoke-virtual {v2, v3, v4, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 340
    .line 341
    .line 342
    iget v3, v0, Landroid/graphics/Rect;->right:I

    .line 343
    .line 344
    int-to-float v3, v3

    .line 345
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 346
    .line 347
    add-int/2addr v4, v11

    .line 348
    add-int v4, v4, p0

    .line 349
    .line 350
    int-to-float v4, v4

    .line 351
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 352
    .line 353
    .line 354
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 355
    .line 356
    iget v4, v0, Landroid/graphics/Rect;->right:I

    .line 357
    .line 358
    sub-int v5, v4, v5

    .line 359
    .line 360
    int-to-float v5, v5

    .line 361
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 362
    .line 363
    add-int v6, v0, p0

    .line 364
    .line 365
    int-to-float v6, v6

    .line 366
    int-to-float v4, v4

    .line 367
    add-int/2addr v0, v8

    .line 368
    add-int v0, v0, p0

    .line 369
    .line 370
    int-to-float v0, v0

    .line 371
    invoke-virtual {v3, v5, v6, v4, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 372
    .line 373
    .line 374
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 375
    .line 376
    const/high16 v3, -0x3d4c0000    # -90.0f

    .line 377
    .line 378
    invoke-virtual {v2, v0, v13, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 379
    .line 380
    .line 381
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 382
    .line 383
    .line 384
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 385
    .line 386
    .line 387
    return-object v1
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getCurvedRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    new-instance v1, Ljava/util/ArrayList;

    .line 4
    .line 5
    const/4 v2, 0x2

    .line 6
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 7
    .line 8
    .line 9
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    new-instance v4, Landroid/graphics/Path;

    .line 14
    .line 15
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 16
    .line 17
    .line 18
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 19
    .line 20
    .line 21
    move-result v5

    .line 22
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 23
    .line 24
    .line 25
    move-result v6

    .line 26
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    .line 27
    .line 28
    .line 29
    move-result v5

    .line 30
    if-eqz v3, :cond_0

    .line 31
    .line 32
    array-length v6, v3

    .line 33
    const/4 v7, 0x3

    .line 34
    if-ne v6, v7, :cond_0

    .line 35
    .line 36
    int-to-float v5, v5

    .line 37
    const/4 v6, 0x0

    .line 38
    aget-object v6, v3, v6

    .line 39
    .line 40
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 41
    .line 42
    .line 43
    move-result v6

    .line 44
    mul-float v6, v6, v5

    .line 45
    .line 46
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 47
    .line 48
    .line 49
    move-result v6

    .line 50
    const/4 v7, 0x1

    .line 51
    aget-object v7, v3, v7

    .line 52
    .line 53
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 54
    .line 55
    .line 56
    move-result v7

    .line 57
    mul-float v7, v7, v5

    .line 58
    .line 59
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 60
    .line 61
    .line 62
    move-result v7

    .line 63
    aget-object v3, v3, v2

    .line 64
    .line 65
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 66
    .line 67
    .line 68
    move-result v3

    .line 69
    mul-float v5, v5, v3

    .line 70
    .line 71
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 72
    .line 73
    .line 74
    move-result v3

    .line 75
    goto :goto_0

    .line 76
    :cond_0
    int-to-float v3, v5

    .line 77
    const/high16 v5, 0x3e800000    # 0.25f

    .line 78
    .line 79
    mul-float v5, v5, v3

    .line 80
    .line 81
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 82
    .line 83
    .line 84
    move-result v6

    .line 85
    const/high16 v7, 0x3f000000    # 0.5f

    .line 86
    .line 87
    mul-float v3, v3, v7

    .line 88
    .line 89
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 90
    .line 91
    .line 92
    move-result v7

    .line 93
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 94
    .line 95
    .line 96
    move-result v3

    .line 97
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 98
    .line 99
    .line 100
    move-result v5

    .line 101
    mul-int/lit8 v5, v5, 0x2

    .line 102
    .line 103
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    .line 104
    .line 105
    div-int/2addr v7, v2

    .line 106
    sub-int/2addr v8, v7

    .line 107
    div-int/lit8 v2, v6, 0x2

    .line 108
    .line 109
    sub-int/2addr v8, v2

    .line 110
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 111
    .line 112
    sub-int/2addr v8, v9

    .line 113
    iget v10, v0, Landroid/graphics/Rect;->right:I

    .line 114
    .line 115
    int-to-float v10, v10

    .line 116
    int-to-float v9, v9

    .line 117
    invoke-virtual {v4, v10, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 118
    .line 119
    .line 120
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 121
    .line 122
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 123
    .line 124
    int-to-float v11, v10

    .line 125
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 126
    .line 127
    int-to-float v13, v12

    .line 128
    add-int/2addr v10, v5

    .line 129
    int-to-float v10, v10

    .line 130
    add-int/2addr v12, v8

    .line 131
    int-to-float v12, v12

    .line 132
    invoke-virtual {v9, v11, v13, v10, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 133
    .line 134
    .line 135
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 136
    .line 137
    const/high16 v10, 0x43870000    # 270.0f

    .line 138
    .line 139
    const/high16 v11, -0x3d4c0000    # -90.0f

    .line 140
    .line 141
    invoke-virtual {v4, v9, v10, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 142
    .line 143
    .line 144
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 145
    .line 146
    int-to-float v9, v9

    .line 147
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 148
    .line 149
    div-int/lit8 v11, v8, 0x2

    .line 150
    .line 151
    add-int/2addr v10, v11

    .line 152
    add-int/2addr v10, v6

    .line 153
    int-to-float v10, v10

    .line 154
    invoke-virtual {v4, v9, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 155
    .line 156
    .line 157
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 158
    .line 159
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 160
    .line 161
    int-to-float v12, v10

    .line 162
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 163
    .line 164
    add-int v14, v13, v6

    .line 165
    .line 166
    int-to-float v14, v14

    .line 167
    add-int/2addr v10, v5

    .line 168
    int-to-float v10, v10

    .line 169
    add-int/2addr v13, v8

    .line 170
    add-int/2addr v13, v6

    .line 171
    int-to-float v13, v13

    .line 172
    invoke-virtual {v9, v12, v14, v10, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 173
    .line 174
    .line 175
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 176
    .line 177
    const/high16 v10, 0x42b40000    # 90.0f

    .line 178
    .line 179
    const/high16 v12, 0x43340000    # 180.0f

    .line 180
    .line 181
    invoke-virtual {v4, v9, v12, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 182
    .line 183
    .line 184
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 185
    .line 186
    .line 187
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    .line 189
    .line 190
    new-instance v4, Landroid/graphics/Path;

    .line 191
    .line 192
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 193
    .line 194
    .line 195
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 196
    .line 197
    int-to-float v9, v9

    .line 198
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 199
    .line 200
    add-int/2addr v10, v11

    .line 201
    int-to-float v10, v10

    .line 202
    invoke-virtual {v4, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 203
    .line 204
    .line 205
    int-to-double v9, v11

    .line 206
    const-wide/high16 v13, 0x4000000000000000L    # 2.0

    .line 207
    .line 208
    invoke-static {v9, v10, v13, v14}, Ljava/lang/Math;->pow(DD)D

    .line 209
    .line 210
    .line 211
    move-result-wide v9

    .line 212
    div-int/lit8 v15, v5, 0x2

    .line 213
    .line 214
    move/from16 p0, v6

    .line 215
    .line 216
    move/from16 v16, v7

    .line 217
    .line 218
    int-to-double v6, v15

    .line 219
    invoke-static {v6, v7, v13, v14}, Ljava/lang/Math;->pow(DD)D

    .line 220
    .line 221
    .line 222
    move-result-wide v17

    .line 223
    move-object v15, v1

    .line 224
    move/from16 v19, v2

    .line 225
    .line 226
    int-to-double v1, v3

    .line 227
    invoke-static {v1, v2, v13, v14}, Ljava/lang/Math;->pow(DD)D

    .line 228
    .line 229
    .line 230
    move-result-wide v20

    .line 231
    sub-double v17, v17, v20

    .line 232
    .line 233
    mul-double v9, v9, v17

    .line 234
    .line 235
    invoke-static {v6, v7, v13, v14}, Ljava/lang/Math;->pow(DD)D

    .line 236
    .line 237
    .line 238
    move-result-wide v6

    .line 239
    div-double/2addr v9, v6

    .line 240
    invoke-static {v9, v10}, Ljava/lang/Math;->sqrt(D)D

    .line 241
    .line 242
    .line 243
    move-result-wide v6

    .line 244
    div-double v1, v6, v1

    .line 245
    .line 246
    invoke-static {v1, v2}, Ljava/lang/Math;->atan(D)D

    .line 247
    .line 248
    .line 249
    move-result-wide v1

    .line 250
    const-wide v9, 0x4066800000000000L    # 180.0

    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    mul-double v1, v1, v9

    .line 256
    .line 257
    const-wide v9, 0x400921fb54442d18L    # Math.PI

    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    div-double/2addr v1, v9

    .line 263
    double-to-int v1, v1

    .line 264
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 265
    .line 266
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 267
    .line 268
    int-to-float v10, v9

    .line 269
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 270
    .line 271
    int-to-float v14, v13

    .line 272
    add-int/2addr v9, v5

    .line 273
    int-to-float v9, v9

    .line 274
    add-int/2addr v13, v8

    .line 275
    int-to-float v13, v13

    .line 276
    invoke-virtual {v2, v10, v14, v9, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 277
    .line 278
    .line 279
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 280
    .line 281
    neg-int v9, v1

    .line 282
    int-to-float v9, v9

    .line 283
    invoke-virtual {v4, v2, v12, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 284
    .line 285
    .line 286
    iget v2, v0, Landroid/graphics/Rect;->right:I

    .line 287
    .line 288
    sub-int/2addr v2, v3

    .line 289
    int-to-float v2, v2

    .line 290
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 291
    .line 292
    add-int/2addr v9, v11

    .line 293
    double-to-int v6, v6

    .line 294
    add-int/2addr v9, v6

    .line 295
    int-to-float v7, v9

    .line 296
    invoke-virtual {v4, v2, v7}, Landroid/graphics/Path;->setLastPoint(FF)V

    .line 297
    .line 298
    .line 299
    iget v2, v0, Landroid/graphics/Rect;->right:I

    .line 300
    .line 301
    sub-int/2addr v2, v3

    .line 302
    int-to-float v2, v2

    .line 303
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 304
    .line 305
    add-int/2addr v7, v11

    .line 306
    add-int/2addr v7, v6

    .line 307
    add-int v7, v7, v19

    .line 308
    .line 309
    sub-int v7, v7, v16

    .line 310
    .line 311
    int-to-float v7, v7

    .line 312
    invoke-virtual {v4, v2, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 313
    .line 314
    .line 315
    iget v2, v0, Landroid/graphics/Rect;->right:I

    .line 316
    .line 317
    int-to-float v2, v2

    .line 318
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    .line 319
    .line 320
    sub-int v7, v7, v16

    .line 321
    .line 322
    int-to-float v7, v7

    .line 323
    invoke-virtual {v4, v2, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 324
    .line 325
    .line 326
    iget v2, v0, Landroid/graphics/Rect;->right:I

    .line 327
    .line 328
    sub-int/2addr v2, v3

    .line 329
    int-to-float v2, v2

    .line 330
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 331
    .line 332
    add-int/2addr v7, v11

    .line 333
    add-int/2addr v7, v6

    .line 334
    add-int v7, v7, v19

    .line 335
    .line 336
    add-int v7, v7, v16

    .line 337
    .line 338
    int-to-float v7, v7

    .line 339
    invoke-virtual {v4, v2, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 340
    .line 341
    .line 342
    iget v2, v0, Landroid/graphics/Rect;->right:I

    .line 343
    .line 344
    sub-int/2addr v2, v3

    .line 345
    int-to-float v2, v2

    .line 346
    iget v3, v0, Landroid/graphics/Rect;->top:I

    .line 347
    .line 348
    add-int/2addr v3, v11

    .line 349
    add-int/2addr v3, v6

    .line 350
    add-int v3, v3, p0

    .line 351
    .line 352
    int-to-float v3, v3

    .line 353
    invoke-virtual {v4, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 354
    .line 355
    .line 356
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 357
    .line 358
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 359
    .line 360
    int-to-float v6, v3

    .line 361
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 362
    .line 363
    add-int v7, v0, p0

    .line 364
    .line 365
    int-to-float v7, v7

    .line 366
    add-int/2addr v3, v5

    .line 367
    int-to-float v3, v3

    .line 368
    add-int/2addr v0, v8

    .line 369
    add-int v0, v0, p0

    .line 370
    .line 371
    int-to-float v0, v0

    .line 372
    invoke-virtual {v2, v6, v7, v3, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 373
    .line 374
    .line 375
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 376
    .line 377
    rsub-int v2, v1, 0xb4

    .line 378
    .line 379
    int-to-float v2, v2

    .line 380
    int-to-float v1, v1

    .line 381
    invoke-virtual {v4, v0, v2, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 382
    .line 383
    .line 384
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 385
    .line 386
    .line 387
    move-object v0, v15

    .line 388
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389
    .line 390
    .line 391
    return-object v0
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getCurvedUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    new-instance v1, Ljava/util/ArrayList;

    .line 4
    .line 5
    const/4 v2, 0x2

    .line 6
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 7
    .line 8
    .line 9
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    new-instance v4, Landroid/graphics/Path;

    .line 14
    .line 15
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 16
    .line 17
    .line 18
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 19
    .line 20
    .line 21
    move-result v5

    .line 22
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 23
    .line 24
    .line 25
    move-result v6

    .line 26
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    .line 27
    .line 28
    .line 29
    move-result v5

    .line 30
    if-eqz v3, :cond_0

    .line 31
    .line 32
    array-length v6, v3

    .line 33
    const/4 v7, 0x3

    .line 34
    if-ne v6, v7, :cond_0

    .line 35
    .line 36
    int-to-float v5, v5

    .line 37
    const/4 v6, 0x0

    .line 38
    aget-object v6, v3, v6

    .line 39
    .line 40
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 41
    .line 42
    .line 43
    move-result v6

    .line 44
    mul-float v6, v6, v5

    .line 45
    .line 46
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 47
    .line 48
    .line 49
    move-result v6

    .line 50
    const/4 v7, 0x1

    .line 51
    aget-object v7, v3, v7

    .line 52
    .line 53
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 54
    .line 55
    .line 56
    move-result v7

    .line 57
    mul-float v7, v7, v5

    .line 58
    .line 59
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 60
    .line 61
    .line 62
    move-result v7

    .line 63
    aget-object v3, v3, v2

    .line 64
    .line 65
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 66
    .line 67
    .line 68
    move-result v3

    .line 69
    mul-float v5, v5, v3

    .line 70
    .line 71
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 72
    .line 73
    .line 74
    move-result v3

    .line 75
    goto :goto_0

    .line 76
    :cond_0
    int-to-float v3, v5

    .line 77
    const/high16 v5, 0x3e800000    # 0.25f

    .line 78
    .line 79
    mul-float v5, v5, v3

    .line 80
    .line 81
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 82
    .line 83
    .line 84
    move-result v6

    .line 85
    const/high16 v7, 0x3f000000    # 0.5f

    .line 86
    .line 87
    mul-float v3, v3, v7

    .line 88
    .line 89
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 90
    .line 91
    .line 92
    move-result v7

    .line 93
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 94
    .line 95
    .line 96
    move-result v3

    .line 97
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 98
    .line 99
    .line 100
    move-result v5

    .line 101
    div-int/2addr v7, v2

    .line 102
    sub-int/2addr v5, v7

    .line 103
    div-int/lit8 v8, v6, 0x2

    .line 104
    .line 105
    sub-int/2addr v5, v8

    .line 106
    div-int/2addr v5, v2

    .line 107
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 108
    .line 109
    .line 110
    move-result v2

    .line 111
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 112
    .line 113
    add-int/2addr v9, v5

    .line 114
    int-to-float v9, v9

    .line 115
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    .line 116
    .line 117
    int-to-float v10, v10

    .line 118
    invoke-virtual {v4, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 119
    .line 120
    .line 121
    int-to-double v9, v5

    .line 122
    const-wide/high16 v11, 0x4000000000000000L    # 2.0

    .line 123
    .line 124
    invoke-static {v9, v10, v11, v12}, Ljava/lang/Math;->pow(DD)D

    .line 125
    .line 126
    .line 127
    move-result-wide v9

    .line 128
    int-to-double v13, v2

    .line 129
    invoke-static {v13, v14, v11, v12}, Ljava/lang/Math;->pow(DD)D

    .line 130
    .line 131
    .line 132
    move-result-wide v15

    .line 133
    move/from16 p0, v6

    .line 134
    .line 135
    move/from16 v17, v7

    .line 136
    .line 137
    int-to-double v6, v3

    .line 138
    invoke-static {v6, v7, v11, v12}, Ljava/lang/Math;->pow(DD)D

    .line 139
    .line 140
    .line 141
    move-result-wide v18

    .line 142
    sub-double v15, v15, v18

    .line 143
    .line 144
    mul-double v9, v9, v15

    .line 145
    .line 146
    invoke-static {v13, v14, v11, v12}, Ljava/lang/Math;->pow(DD)D

    .line 147
    .line 148
    .line 149
    move-result-wide v11

    .line 150
    div-double/2addr v9, v11

    .line 151
    invoke-static {v9, v10}, Ljava/lang/Math;->sqrt(D)D

    .line 152
    .line 153
    .line 154
    move-result-wide v9

    .line 155
    div-double v6, v9, v6

    .line 156
    .line 157
    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    .line 158
    .line 159
    .line 160
    move-result-wide v6

    .line 161
    const-wide v11, 0x4066800000000000L    # 180.0

    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    mul-double v6, v6, v11

    .line 167
    .line 168
    const-wide v11, 0x400921fb54442d18L    # Math.PI

    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    div-double/2addr v6, v11

    .line 174
    double-to-int v6, v6

    .line 175
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 176
    .line 177
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 178
    .line 179
    int-to-float v12, v11

    .line 180
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 181
    .line 182
    sub-int v14, v13, v2

    .line 183
    .line 184
    int-to-float v14, v14

    .line 185
    mul-int/lit8 v15, v5, 0x2

    .line 186
    .line 187
    add-int/2addr v11, v15

    .line 188
    int-to-float v11, v11

    .line 189
    add-int/2addr v13, v2

    .line 190
    int-to-float v13, v13

    .line 191
    invoke-virtual {v7, v12, v14, v11, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 192
    .line 193
    .line 194
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 195
    .line 196
    neg-int v11, v6

    .line 197
    int-to-float v11, v11

    .line 198
    const/high16 v12, 0x42b40000    # 90.0f

    .line 199
    .line 200
    invoke-virtual {v4, v7, v12, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 201
    .line 202
    .line 203
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 204
    .line 205
    add-int/2addr v7, v5

    .line 206
    int-to-float v7, v7

    .line 207
    double-to-float v9, v9

    .line 208
    add-float/2addr v7, v9

    .line 209
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 210
    .line 211
    add-int/2addr v10, v3

    .line 212
    int-to-float v10, v10

    .line 213
    invoke-virtual {v4, v7, v10}, Landroid/graphics/Path;->setLastPoint(FF)V

    .line 214
    .line 215
    .line 216
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 217
    .line 218
    add-int/2addr v7, v5

    .line 219
    int-to-float v7, v7

    .line 220
    add-float/2addr v7, v9

    .line 221
    int-to-float v8, v8

    .line 222
    add-float/2addr v7, v8

    .line 223
    move/from16 v10, v17

    .line 224
    .line 225
    int-to-float v11, v10

    .line 226
    sub-float/2addr v7, v11

    .line 227
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 228
    .line 229
    add-int/2addr v13, v3

    .line 230
    int-to-float v13, v13

    .line 231
    invoke-virtual {v4, v7, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 232
    .line 233
    .line 234
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 235
    .line 236
    sub-int/2addr v7, v10

    .line 237
    int-to-float v7, v7

    .line 238
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 239
    .line 240
    int-to-float v10, v10

    .line 241
    invoke-virtual {v4, v7, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 242
    .line 243
    .line 244
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 245
    .line 246
    add-int/2addr v7, v5

    .line 247
    int-to-float v7, v7

    .line 248
    add-float/2addr v7, v9

    .line 249
    add-float/2addr v7, v8

    .line 250
    add-float/2addr v7, v11

    .line 251
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 252
    .line 253
    add-int/2addr v8, v3

    .line 254
    int-to-float v8, v8

    .line 255
    invoke-virtual {v4, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 256
    .line 257
    .line 258
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 259
    .line 260
    add-int/2addr v7, v5

    .line 261
    int-to-float v7, v7

    .line 262
    add-float/2addr v7, v9

    .line 263
    move/from16 v8, p0

    .line 264
    .line 265
    int-to-float v9, v8

    .line 266
    add-float/2addr v7, v9

    .line 267
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 268
    .line 269
    add-int/2addr v9, v3

    .line 270
    int-to-float v3, v9

    .line 271
    invoke-virtual {v4, v7, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 272
    .line 273
    .line 274
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 275
    .line 276
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 277
    .line 278
    add-int v9, v7, v8

    .line 279
    .line 280
    int-to-float v9, v9

    .line 281
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 282
    .line 283
    sub-int v11, v10, v2

    .line 284
    .line 285
    int-to-float v11, v11

    .line 286
    add-int/2addr v7, v15

    .line 287
    add-int/2addr v7, v8

    .line 288
    int-to-float v7, v7

    .line 289
    add-int/2addr v10, v2

    .line 290
    int-to-float v10, v10

    .line 291
    invoke-virtual {v3, v9, v11, v7, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 292
    .line 293
    .line 294
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 295
    .line 296
    rsub-int/lit8 v7, v6, 0x5a

    .line 297
    .line 298
    int-to-float v7, v7

    .line 299
    int-to-float v6, v6

    .line 300
    invoke-virtual {v4, v3, v7, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 301
    .line 302
    .line 303
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 304
    .line 305
    .line 306
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    .line 308
    .line 309
    new-instance v3, Landroid/graphics/Path;

    .line 310
    .line 311
    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 312
    .line 313
    .line 314
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 315
    .line 316
    int-to-float v4, v4

    .line 317
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 318
    .line 319
    int-to-float v6, v6

    .line 320
    invoke-virtual {v3, v4, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 321
    .line 322
    .line 323
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 324
    .line 325
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 326
    .line 327
    int-to-float v7, v6

    .line 328
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 329
    .line 330
    sub-int v10, v9, v2

    .line 331
    .line 332
    int-to-float v10, v10

    .line 333
    add-int/2addr v6, v15

    .line 334
    int-to-float v6, v6

    .line 335
    add-int/2addr v9, v2

    .line 336
    int-to-float v9, v9

    .line 337
    invoke-virtual {v4, v7, v10, v6, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 338
    .line 339
    .line 340
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 341
    .line 342
    const/high16 v6, 0x43340000    # 180.0f

    .line 343
    .line 344
    const/high16 v7, -0x3d4c0000    # -90.0f

    .line 345
    .line 346
    invoke-virtual {v3, v4, v6, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 347
    .line 348
    .line 349
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 350
    .line 351
    add-int/2addr v4, v5

    .line 352
    add-int/2addr v4, v8

    .line 353
    int-to-float v4, v4

    .line 354
    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 355
    .line 356
    int-to-float v5, v5

    .line 357
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 358
    .line 359
    .line 360
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 361
    .line 362
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 363
    .line 364
    add-int v6, v5, v8

    .line 365
    .line 366
    int-to-float v6, v6

    .line 367
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 368
    .line 369
    sub-int v7, v0, v2

    .line 370
    .line 371
    int-to-float v7, v7

    .line 372
    add-int/2addr v5, v15

    .line 373
    add-int/2addr v5, v8

    .line 374
    int-to-float v5, v5

    .line 375
    add-int/2addr v0, v2

    .line 376
    int-to-float v0, v0

    .line 377
    invoke-virtual {v4, v6, v7, v5, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 378
    .line 379
    .line 380
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 381
    .line 382
    invoke-virtual {v3, v0, v12, v12}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 383
    .line 384
    .line 385
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    .line 386
    .line 387
    .line 388
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389
    .line 390
    .line 391
    return-object v1
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getDownArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, 0x2

    .line 18
    if-eqz p0, :cond_4

    .line 19
    .line 20
    array-length v2, p0

    .line 21
    const/4 v3, 0x4

    .line 22
    if-ne v2, v3, :cond_4

    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    const/4 v4, 0x0

    .line 26
    :goto_0
    if-ge v4, v3, :cond_1

    .line 27
    .line 28
    aget-object v5, p0, v4

    .line 29
    .line 30
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 31
    .line 32
    .line 33
    move-result v5

    .line 34
    const/high16 v6, 0x3f800000    # 1.0f

    .line 35
    .line 36
    cmpl-float v5, v5, v6

    .line 37
    .line 38
    if-lez v5, :cond_0

    .line 39
    .line 40
    if-eq v4, v1, :cond_0

    .line 41
    .line 42
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 43
    .line 44
    .line 45
    move-result-object v5

    .line 46
    aput-object v5, p0, v4

    .line 47
    .line 48
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    int-to-float v0, v0

    .line 52
    aget-object v2, p0, v2

    .line 53
    .line 54
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    mul-float v2, v2, v0

    .line 59
    .line 60
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    const/4 v3, 0x1

    .line 65
    aget-object v3, p0, v3

    .line 66
    .line 67
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 68
    .line 69
    .line 70
    move-result v3

    .line 71
    mul-float v3, v3, v0

    .line 72
    .line 73
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 74
    .line 75
    .line 76
    move-result v3

    .line 77
    aget-object v4, p0, v1

    .line 78
    .line 79
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 80
    .line 81
    .line 82
    move-result v4

    .line 83
    mul-float v0, v0, v4

    .line 84
    .line 85
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 90
    .line 91
    .line 92
    move-result v4

    .line 93
    int-to-float v4, v4

    .line 94
    const/4 v5, 0x3

    .line 95
    aget-object p0, p0, v5

    .line 96
    .line 97
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 98
    .line 99
    .line 100
    move-result p0

    .line 101
    mul-float v4, v4, p0

    .line 102
    .line 103
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 104
    .line 105
    .line 106
    move-result p0

    .line 107
    mul-int/lit8 v4, v3, 0x2

    .line 108
    .line 109
    if-le v2, v4, :cond_2

    .line 110
    .line 111
    move v2, v4

    .line 112
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 113
    .line 114
    .line 115
    move-result v4

    .line 116
    if-le v0, v4, :cond_3

    .line 117
    .line 118
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    :cond_3
    add-int v4, p0, v0

    .line 123
    .line 124
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 125
    .line 126
    .line 127
    move-result v5

    .line 128
    if-le v4, v5, :cond_5

    .line 129
    .line 130
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 131
    .line 132
    .line 133
    move-result p0

    .line 134
    sub-int/2addr p0, v0

    .line 135
    goto :goto_1

    .line 136
    :cond_4
    int-to-float p0, v0

    .line 137
    const/high16 v0, 0x3e800000    # 0.25f

    .line 138
    .line 139
    mul-float p0, p0, v0

    .line 140
    .line 141
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 142
    .line 143
    .line 144
    move-result v2

    .line 145
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 146
    .line 147
    .line 148
    move-result v3

    .line 149
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 150
    .line 151
    .line 152
    move-result v0

    .line 153
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 154
    .line 155
    .line 156
    move-result p0

    .line 157
    int-to-float p0, p0

    .line 158
    const v4, 0x3f266666    # 0.65f

    .line 159
    .line 160
    .line 161
    mul-float p0, p0, v4

    .line 162
    .line 163
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 164
    .line 165
    .line 166
    move-result p0

    .line 167
    :cond_5
    :goto_1
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 168
    .line 169
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 170
    .line 171
    .line 172
    move-result v5

    .line 173
    int-to-float v5, v5

    .line 174
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 175
    .line 176
    int-to-float v6, v6

    .line 177
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 178
    .line 179
    .line 180
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 181
    .line 182
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 183
    .line 184
    .line 185
    move-result v5

    .line 186
    sub-int/2addr v5, v3

    .line 187
    int-to-float v5, v5

    .line 188
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 189
    .line 190
    sub-int/2addr v6, v0

    .line 191
    int-to-float v6, v6

    .line 192
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 193
    .line 194
    .line 195
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 196
    .line 197
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 198
    .line 199
    .line 200
    move-result v5

    .line 201
    div-int/2addr v2, v1

    .line 202
    sub-int/2addr v5, v2

    .line 203
    int-to-float v1, v5

    .line 204
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 205
    .line 206
    sub-int/2addr v5, v0

    .line 207
    int-to-float v5, v5

    .line 208
    invoke-virtual {v4, v1, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 209
    .line 210
    .line 211
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 212
    .line 213
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 214
    .line 215
    .line 216
    move-result v4

    .line 217
    sub-int/2addr v4, v2

    .line 218
    int-to-float v4, v4

    .line 219
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 220
    .line 221
    add-int/2addr v5, p0

    .line 222
    int-to-float v5, v5

    .line 223
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 224
    .line 225
    .line 226
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 227
    .line 228
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 229
    .line 230
    int-to-float v4, v4

    .line 231
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 232
    .line 233
    add-int/2addr v5, p0

    .line 234
    int-to-float v5, v5

    .line 235
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 236
    .line 237
    .line 238
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 239
    .line 240
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 241
    .line 242
    int-to-float v4, v4

    .line 243
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 244
    .line 245
    int-to-float v5, v5

    .line 246
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 247
    .line 248
    .line 249
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 250
    .line 251
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 252
    .line 253
    int-to-float v4, v4

    .line 254
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 255
    .line 256
    int-to-float v5, v5

    .line 257
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 258
    .line 259
    .line 260
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 261
    .line 262
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 263
    .line 264
    int-to-float v4, v4

    .line 265
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 266
    .line 267
    add-int/2addr v5, p0

    .line 268
    int-to-float v5, v5

    .line 269
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 270
    .line 271
    .line 272
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 273
    .line 274
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 275
    .line 276
    .line 277
    move-result v4

    .line 278
    add-int/2addr v4, v2

    .line 279
    int-to-float v4, v4

    .line 280
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 281
    .line 282
    add-int/2addr v5, p0

    .line 283
    int-to-float p0, v5

    .line 284
    invoke-virtual {v1, v4, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 285
    .line 286
    .line 287
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 288
    .line 289
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 290
    .line 291
    .line 292
    move-result v1

    .line 293
    add-int/2addr v1, v2

    .line 294
    int-to-float v1, v1

    .line 295
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 296
    .line 297
    sub-int/2addr v2, v0

    .line 298
    int-to-float v2, v2

    .line 299
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 300
    .line 301
    .line 302
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 303
    .line 304
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 305
    .line 306
    .line 307
    move-result v1

    .line 308
    add-int/2addr v1, v3

    .line 309
    int-to-float v1, v1

    .line 310
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 311
    .line 312
    sub-int/2addr p1, v0

    .line 313
    int-to-float p1, p1

    .line 314
    invoke-virtual {p0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 315
    .line 316
    .line 317
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 318
    .line 319
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 320
    .line 321
    .line 322
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 323
    .line 324
    return-object p0
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getDownArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x2

    .line 10
    div-int/2addr v0, v1

    .line 11
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz p0, :cond_0

    .line 24
    .line 25
    array-length v3, p0

    .line 26
    if-ne v3, v1, :cond_0

    .line 27
    .line 28
    int-to-float v0, v0

    .line 29
    const/4 v1, 0x0

    .line 30
    aget-object v1, p0, v1

    .line 31
    .line 32
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    mul-float v0, v0, v1

    .line 37
    .line 38
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    int-to-float v1, v2

    .line 43
    const/4 v2, 0x1

    .line 44
    aget-object p0, p0, v2

    .line 45
    .line 46
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 47
    .line 48
    .line 49
    move-result p0

    .line 50
    mul-float v1, v1, p0

    .line 51
    .line 52
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 53
    .line 54
    .line 55
    move-result p0

    .line 56
    goto :goto_0

    .line 57
    :cond_0
    int-to-float p0, v0

    .line 58
    const/high16 v0, 0x3f000000    # 0.5f

    .line 59
    .line 60
    mul-float p0, p0, v0

    .line 61
    .line 62
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 63
    .line 64
    .line 65
    move-result p0

    .line 66
    int-to-float v1, v2

    .line 67
    mul-float v1, v1, v0

    .line 68
    .line 69
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    move v4, v0

    .line 74
    move v0, p0

    .line 75
    move p0, v4

    .line 76
    :goto_0
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 77
    .line 78
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 79
    .line 80
    .line 81
    move-result v2

    .line 82
    sub-int/2addr v2, v0

    .line 83
    int-to-float v2, v2

    .line 84
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 85
    .line 86
    int-to-float v3, v3

    .line 87
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 88
    .line 89
    .line 90
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 91
    .line 92
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 93
    .line 94
    .line 95
    move-result v2

    .line 96
    add-int/2addr v2, v0

    .line 97
    int-to-float v2, v2

    .line 98
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 99
    .line 100
    int-to-float v3, v3

    .line 101
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 102
    .line 103
    .line 104
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 105
    .line 106
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 107
    .line 108
    .line 109
    move-result v2

    .line 110
    add-int/2addr v2, v0

    .line 111
    int-to-float v2, v2

    .line 112
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 113
    .line 114
    sub-int/2addr v3, p0

    .line 115
    int-to-float v3, v3

    .line 116
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 117
    .line 118
    .line 119
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 120
    .line 121
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 122
    .line 123
    int-to-float v2, v2

    .line 124
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 125
    .line 126
    sub-int/2addr v3, p0

    .line 127
    int-to-float v3, v3

    .line 128
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 129
    .line 130
    .line 131
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 132
    .line 133
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 134
    .line 135
    .line 136
    move-result v2

    .line 137
    int-to-float v2, v2

    .line 138
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 139
    .line 140
    int-to-float v3, v3

    .line 141
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 142
    .line 143
    .line 144
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 145
    .line 146
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 147
    .line 148
    int-to-float v2, v2

    .line 149
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 150
    .line 151
    sub-int/2addr v3, p0

    .line 152
    int-to-float v3, v3

    .line 153
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 154
    .line 155
    .line 156
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 157
    .line 158
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 159
    .line 160
    .line 161
    move-result v2

    .line 162
    sub-int/2addr v2, v0

    .line 163
    int-to-float v0, v2

    .line 164
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 165
    .line 166
    sub-int/2addr p1, p0

    .line 167
    int-to-float p0, p1

    .line 168
    invoke-virtual {v1, v0, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    .line 170
    .line 171
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 172
    .line 173
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 174
    .line 175
    .line 176
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 177
    .line 178
    return-object p0
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getHomePlatePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    array-length v1, p0

    .line 20
    const/4 v2, 0x1

    .line 21
    if-ne v1, v2, :cond_0

    .line 22
    .line 23
    int-to-float v0, v0

    .line 24
    const/4 v1, 0x0

    .line 25
    aget-object p0, p0, v1

    .line 26
    .line 27
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result p0

    .line 31
    mul-float v0, v0, p0

    .line 32
    .line 33
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 34
    .line 35
    .line 36
    move-result p0

    .line 37
    goto :goto_0

    .line 38
    :cond_0
    int-to-float p0, v0

    .line 39
    const/high16 v0, 0x3f000000    # 0.5f

    .line 40
    .line 41
    mul-float p0, p0, v0

    .line 42
    .line 43
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 44
    .line 45
    .line 46
    move-result p0

    .line 47
    :goto_0
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 48
    .line 49
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 50
    .line 51
    int-to-float v1, v1

    .line 52
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 53
    .line 54
    int-to-float v2, v2

    .line 55
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 56
    .line 57
    .line 58
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 59
    .line 60
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 61
    .line 62
    sub-int/2addr v1, p0

    .line 63
    int-to-float v1, v1

    .line 64
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 65
    .line 66
    int-to-float v2, v2

    .line 67
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 68
    .line 69
    .line 70
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 71
    .line 72
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 73
    .line 74
    int-to-float v1, v1

    .line 75
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 76
    .line 77
    .line 78
    move-result v2

    .line 79
    int-to-float v2, v2

    .line 80
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 81
    .line 82
    .line 83
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 84
    .line 85
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 86
    .line 87
    sub-int/2addr v1, p0

    .line 88
    int-to-float p0, v1

    .line 89
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 90
    .line 91
    int-to-float v1, v1

    .line 92
    invoke-virtual {v0, p0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 93
    .line 94
    .line 95
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 96
    .line 97
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 98
    .line 99
    int-to-float v0, v0

    .line 100
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 101
    .line 102
    int-to-float p1, p1

    .line 103
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 104
    .line 105
    .line 106
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 107
    .line 108
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 109
    .line 110
    .line 111
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 112
    .line 113
    return-object p0
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getLeftArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, 0x2

    .line 18
    if-eqz p0, :cond_4

    .line 19
    .line 20
    array-length v2, p0

    .line 21
    const/4 v3, 0x4

    .line 22
    if-ne v2, v3, :cond_4

    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    const/4 v4, 0x0

    .line 26
    :goto_0
    if-ge v4, v3, :cond_1

    .line 27
    .line 28
    aget-object v5, p0, v4

    .line 29
    .line 30
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 31
    .line 32
    .line 33
    move-result v5

    .line 34
    const/high16 v6, 0x3f800000    # 1.0f

    .line 35
    .line 36
    cmpl-float v5, v5, v6

    .line 37
    .line 38
    if-lez v5, :cond_0

    .line 39
    .line 40
    if-eq v4, v1, :cond_0

    .line 41
    .line 42
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 43
    .line 44
    .line 45
    move-result-object v5

    .line 46
    aput-object v5, p0, v4

    .line 47
    .line 48
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    int-to-float v0, v0

    .line 52
    aget-object v2, p0, v2

    .line 53
    .line 54
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    mul-float v2, v2, v0

    .line 59
    .line 60
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    const/4 v3, 0x1

    .line 65
    aget-object v3, p0, v3

    .line 66
    .line 67
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 68
    .line 69
    .line 70
    move-result v3

    .line 71
    mul-float v3, v3, v0

    .line 72
    .line 73
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 74
    .line 75
    .line 76
    move-result v3

    .line 77
    aget-object v4, p0, v1

    .line 78
    .line 79
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 80
    .line 81
    .line 82
    move-result v4

    .line 83
    mul-float v0, v0, v4

    .line 84
    .line 85
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 90
    .line 91
    .line 92
    move-result v4

    .line 93
    int-to-float v4, v4

    .line 94
    const/4 v5, 0x3

    .line 95
    aget-object p0, p0, v5

    .line 96
    .line 97
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 98
    .line 99
    .line 100
    move-result p0

    .line 101
    mul-float v4, v4, p0

    .line 102
    .line 103
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 104
    .line 105
    .line 106
    move-result p0

    .line 107
    mul-int/lit8 v4, v3, 0x2

    .line 108
    .line 109
    if-le v2, v4, :cond_2

    .line 110
    .line 111
    move v2, v4

    .line 112
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 113
    .line 114
    .line 115
    move-result v4

    .line 116
    if-le v0, v4, :cond_3

    .line 117
    .line 118
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    :cond_3
    add-int v4, p0, v0

    .line 123
    .line 124
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 125
    .line 126
    .line 127
    move-result v5

    .line 128
    if-le v4, v5, :cond_5

    .line 129
    .line 130
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 131
    .line 132
    .line 133
    move-result p0

    .line 134
    sub-int/2addr p0, v0

    .line 135
    goto :goto_1

    .line 136
    :cond_4
    int-to-float p0, v0

    .line 137
    const/high16 v0, 0x3e800000    # 0.25f

    .line 138
    .line 139
    mul-float p0, p0, v0

    .line 140
    .line 141
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 142
    .line 143
    .line 144
    move-result v2

    .line 145
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 146
    .line 147
    .line 148
    move-result v3

    .line 149
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 150
    .line 151
    .line 152
    move-result v0

    .line 153
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 154
    .line 155
    .line 156
    move-result p0

    .line 157
    int-to-float p0, p0

    .line 158
    const v4, 0x3f266666    # 0.65f

    .line 159
    .line 160
    .line 161
    mul-float p0, p0, v4

    .line 162
    .line 163
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 164
    .line 165
    .line 166
    move-result p0

    .line 167
    :cond_5
    :goto_1
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 168
    .line 169
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 170
    .line 171
    int-to-float v5, v5

    .line 172
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 173
    .line 174
    .line 175
    move-result v6

    .line 176
    int-to-float v6, v6

    .line 177
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 178
    .line 179
    .line 180
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 181
    .line 182
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 183
    .line 184
    add-int/2addr v5, v0

    .line 185
    int-to-float v5, v5

    .line 186
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 187
    .line 188
    .line 189
    move-result v6

    .line 190
    sub-int/2addr v6, v3

    .line 191
    int-to-float v6, v6

    .line 192
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 193
    .line 194
    .line 195
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 196
    .line 197
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 198
    .line 199
    add-int/2addr v5, v0

    .line 200
    int-to-float v5, v5

    .line 201
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 202
    .line 203
    .line 204
    move-result v6

    .line 205
    div-int/2addr v2, v1

    .line 206
    sub-int/2addr v6, v2

    .line 207
    int-to-float v1, v6

    .line 208
    invoke-virtual {v4, v5, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 209
    .line 210
    .line 211
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 212
    .line 213
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 214
    .line 215
    sub-int/2addr v4, p0

    .line 216
    int-to-float v4, v4

    .line 217
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 218
    .line 219
    .line 220
    move-result v5

    .line 221
    sub-int/2addr v5, v2

    .line 222
    int-to-float v5, v5

    .line 223
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 224
    .line 225
    .line 226
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 227
    .line 228
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 229
    .line 230
    sub-int/2addr v4, p0

    .line 231
    int-to-float v4, v4

    .line 232
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 233
    .line 234
    int-to-float v5, v5

    .line 235
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 236
    .line 237
    .line 238
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 239
    .line 240
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 241
    .line 242
    int-to-float v4, v4

    .line 243
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 244
    .line 245
    int-to-float v5, v5

    .line 246
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 247
    .line 248
    .line 249
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 250
    .line 251
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 252
    .line 253
    int-to-float v4, v4

    .line 254
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 255
    .line 256
    int-to-float v5, v5

    .line 257
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 258
    .line 259
    .line 260
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 261
    .line 262
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 263
    .line 264
    sub-int/2addr v4, p0

    .line 265
    int-to-float v4, v4

    .line 266
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 267
    .line 268
    int-to-float v5, v5

    .line 269
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 270
    .line 271
    .line 272
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 273
    .line 274
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 275
    .line 276
    sub-int/2addr v4, p0

    .line 277
    int-to-float p0, v4

    .line 278
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 279
    .line 280
    .line 281
    move-result v4

    .line 282
    add-int/2addr v4, v2

    .line 283
    int-to-float v4, v4

    .line 284
    invoke-virtual {v1, p0, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 285
    .line 286
    .line 287
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 288
    .line 289
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 290
    .line 291
    add-int/2addr v1, v0

    .line 292
    int-to-float v1, v1

    .line 293
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 294
    .line 295
    .line 296
    move-result v4

    .line 297
    add-int/2addr v4, v2

    .line 298
    int-to-float v2, v4

    .line 299
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 300
    .line 301
    .line 302
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 303
    .line 304
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 305
    .line 306
    add-int/2addr v1, v0

    .line 307
    int-to-float v0, v1

    .line 308
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 309
    .line 310
    .line 311
    move-result p1

    .line 312
    add-int/2addr p1, v3

    .line 313
    int-to-float p1, p1

    .line 314
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 315
    .line 316
    .line 317
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 318
    .line 319
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 320
    .line 321
    .line 322
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 323
    .line 324
    return-object p0
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getLeftArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x2

    .line 10
    div-int/2addr v0, v1

    .line 11
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz p0, :cond_0

    .line 24
    .line 25
    array-length v3, p0

    .line 26
    if-ne v3, v1, :cond_0

    .line 27
    .line 28
    int-to-float v0, v0

    .line 29
    const/4 v1, 0x0

    .line 30
    aget-object v1, p0, v1

    .line 31
    .line 32
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    mul-float v0, v0, v1

    .line 37
    .line 38
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    int-to-float v1, v2

    .line 43
    const/4 v2, 0x1

    .line 44
    aget-object p0, p0, v2

    .line 45
    .line 46
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 47
    .line 48
    .line 49
    move-result p0

    .line 50
    mul-float v1, v1, p0

    .line 51
    .line 52
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 53
    .line 54
    .line 55
    move-result p0

    .line 56
    goto :goto_0

    .line 57
    :cond_0
    int-to-float p0, v0

    .line 58
    const/high16 v0, 0x3f000000    # 0.5f

    .line 59
    .line 60
    mul-float p0, p0, v0

    .line 61
    .line 62
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 63
    .line 64
    .line 65
    move-result p0

    .line 66
    int-to-float v1, v2

    .line 67
    mul-float v1, v1, v0

    .line 68
    .line 69
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    move v4, v0

    .line 74
    move v0, p0

    .line 75
    move p0, v4

    .line 76
    :goto_0
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 77
    .line 78
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 79
    .line 80
    int-to-float v2, v2

    .line 81
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 82
    .line 83
    .line 84
    move-result v3

    .line 85
    int-to-float v3, v3

    .line 86
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 87
    .line 88
    .line 89
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 90
    .line 91
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 92
    .line 93
    add-int/2addr v2, p0

    .line 94
    int-to-float v2, v2

    .line 95
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 96
    .line 97
    int-to-float v3, v3

    .line 98
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 99
    .line 100
    .line 101
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 102
    .line 103
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 104
    .line 105
    add-int/2addr v2, p0

    .line 106
    int-to-float v2, v2

    .line 107
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 108
    .line 109
    .line 110
    move-result v3

    .line 111
    sub-int/2addr v3, v0

    .line 112
    int-to-float v3, v3

    .line 113
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 114
    .line 115
    .line 116
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 117
    .line 118
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 119
    .line 120
    int-to-float v2, v2

    .line 121
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 122
    .line 123
    .line 124
    move-result v3

    .line 125
    sub-int/2addr v3, v0

    .line 126
    int-to-float v3, v3

    .line 127
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 128
    .line 129
    .line 130
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 131
    .line 132
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 133
    .line 134
    int-to-float v2, v2

    .line 135
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 136
    .line 137
    .line 138
    move-result v3

    .line 139
    add-int/2addr v3, v0

    .line 140
    int-to-float v3, v3

    .line 141
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 142
    .line 143
    .line 144
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 145
    .line 146
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 147
    .line 148
    add-int/2addr v2, p0

    .line 149
    int-to-float v2, v2

    .line 150
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 151
    .line 152
    .line 153
    move-result v3

    .line 154
    add-int/2addr v3, v0

    .line 155
    int-to-float v0, v3

    .line 156
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 157
    .line 158
    .line 159
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 160
    .line 161
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 162
    .line 163
    add-int/2addr v1, p0

    .line 164
    int-to-float p0, v1

    .line 165
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 166
    .line 167
    int-to-float p1, p1

    .line 168
    invoke-virtual {v0, p0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    .line 170
    .line 171
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 172
    .line 173
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 174
    .line 175
    .line 176
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 177
    .line 178
    return-object p0
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getLeftRightArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, 0x2

    .line 18
    if-eqz p0, :cond_3

    .line 19
    .line 20
    array-length v2, p0

    .line 21
    const/4 v3, 0x4

    .line 22
    if-ne v2, v3, :cond_3

    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    const/4 v4, 0x0

    .line 26
    :goto_0
    if-ge v4, v3, :cond_1

    .line 27
    .line 28
    aget-object v5, p0, v4

    .line 29
    .line 30
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 31
    .line 32
    .line 33
    move-result v5

    .line 34
    const/high16 v6, 0x3f800000    # 1.0f

    .line 35
    .line 36
    cmpl-float v5, v5, v6

    .line 37
    .line 38
    if-lez v5, :cond_0

    .line 39
    .line 40
    if-eq v4, v1, :cond_0

    .line 41
    .line 42
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 43
    .line 44
    .line 45
    move-result-object v5

    .line 46
    aput-object v5, p0, v4

    .line 47
    .line 48
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    int-to-float v0, v0

    .line 52
    aget-object v2, p0, v2

    .line 53
    .line 54
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    mul-float v2, v2, v0

    .line 59
    .line 60
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    const/4 v3, 0x1

    .line 65
    aget-object v3, p0, v3

    .line 66
    .line 67
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 68
    .line 69
    .line 70
    move-result v3

    .line 71
    mul-float v3, v3, v0

    .line 72
    .line 73
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 74
    .line 75
    .line 76
    move-result v3

    .line 77
    aget-object v4, p0, v1

    .line 78
    .line 79
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 80
    .line 81
    .line 82
    move-result v4

    .line 83
    mul-float v0, v0, v4

    .line 84
    .line 85
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 90
    .line 91
    .line 92
    move-result v4

    .line 93
    int-to-float v4, v4

    .line 94
    const/4 v5, 0x3

    .line 95
    aget-object p0, p0, v5

    .line 96
    .line 97
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 98
    .line 99
    .line 100
    move-result p0

    .line 101
    mul-float v4, v4, p0

    .line 102
    .line 103
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 104
    .line 105
    .line 106
    move-result p0

    .line 107
    mul-int/lit8 v4, v0, 0x2

    .line 108
    .line 109
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 110
    .line 111
    .line 112
    move-result v5

    .line 113
    if-lt v4, v5, :cond_2

    .line 114
    .line 115
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 116
    .line 117
    .line 118
    move-result v0

    .line 119
    div-int/2addr v0, v1

    .line 120
    :cond_2
    mul-int/lit8 v4, v0, 0x2

    .line 121
    .line 122
    add-int v5, v4, p0

    .line 123
    .line 124
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 125
    .line 126
    .line 127
    move-result v6

    .line 128
    if-lt v5, v6, :cond_4

    .line 129
    .line 130
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 131
    .line 132
    .line 133
    move-result p0

    .line 134
    sub-int/2addr p0, v4

    .line 135
    goto :goto_1

    .line 136
    :cond_3
    int-to-float p0, v0

    .line 137
    const/high16 v0, 0x3e800000    # 0.25f

    .line 138
    .line 139
    mul-float p0, p0, v0

    .line 140
    .line 141
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 142
    .line 143
    .line 144
    move-result v2

    .line 145
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 146
    .line 147
    .line 148
    move-result v3

    .line 149
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 150
    .line 151
    .line 152
    move-result v0

    .line 153
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 154
    .line 155
    .line 156
    move-result p0

    .line 157
    int-to-float p0, p0

    .line 158
    const/high16 v4, 0x3f000000    # 0.5f

    .line 159
    .line 160
    mul-float p0, p0, v4

    .line 161
    .line 162
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 163
    .line 164
    .line 165
    move-result p0

    .line 166
    :cond_4
    :goto_1
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 167
    .line 168
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 169
    .line 170
    int-to-float v5, v5

    .line 171
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 172
    .line 173
    .line 174
    move-result v6

    .line 175
    int-to-float v6, v6

    .line 176
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 177
    .line 178
    .line 179
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 180
    .line 181
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 182
    .line 183
    add-int/2addr v5, v0

    .line 184
    int-to-float v5, v5

    .line 185
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 186
    .line 187
    .line 188
    move-result v6

    .line 189
    sub-int/2addr v6, v3

    .line 190
    int-to-float v6, v6

    .line 191
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 192
    .line 193
    .line 194
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 195
    .line 196
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 197
    .line 198
    add-int/2addr v5, v0

    .line 199
    int-to-float v5, v5

    .line 200
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 201
    .line 202
    .line 203
    move-result v6

    .line 204
    div-int/2addr v2, v1

    .line 205
    sub-int/2addr v6, v2

    .line 206
    int-to-float v6, v6

    .line 207
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 208
    .line 209
    .line 210
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 211
    .line 212
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 213
    .line 214
    .line 215
    move-result v5

    .line 216
    div-int/2addr p0, v1

    .line 217
    sub-int/2addr v5, p0

    .line 218
    int-to-float v1, v5

    .line 219
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 220
    .line 221
    .line 222
    move-result v5

    .line 223
    sub-int/2addr v5, v2

    .line 224
    int-to-float v5, v5

    .line 225
    invoke-virtual {v4, v1, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 226
    .line 227
    .line 228
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 229
    .line 230
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 231
    .line 232
    .line 233
    move-result v4

    .line 234
    sub-int/2addr v4, p0

    .line 235
    int-to-float v4, v4

    .line 236
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 237
    .line 238
    int-to-float v5, v5

    .line 239
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 240
    .line 241
    .line 242
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 243
    .line 244
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 245
    .line 246
    .line 247
    move-result v4

    .line 248
    add-int/2addr v4, p0

    .line 249
    int-to-float v4, v4

    .line 250
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 251
    .line 252
    int-to-float v5, v5

    .line 253
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 254
    .line 255
    .line 256
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 257
    .line 258
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 259
    .line 260
    .line 261
    move-result v4

    .line 262
    add-int/2addr v4, p0

    .line 263
    int-to-float v4, v4

    .line 264
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 265
    .line 266
    .line 267
    move-result v5

    .line 268
    sub-int/2addr v5, v2

    .line 269
    int-to-float v5, v5

    .line 270
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 271
    .line 272
    .line 273
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 274
    .line 275
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 276
    .line 277
    sub-int/2addr v4, v0

    .line 278
    int-to-float v4, v4

    .line 279
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 280
    .line 281
    .line 282
    move-result v5

    .line 283
    sub-int/2addr v5, v2

    .line 284
    int-to-float v5, v5

    .line 285
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 286
    .line 287
    .line 288
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 289
    .line 290
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 291
    .line 292
    sub-int/2addr v4, v0

    .line 293
    int-to-float v4, v4

    .line 294
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 295
    .line 296
    .line 297
    move-result v5

    .line 298
    sub-int/2addr v5, v3

    .line 299
    int-to-float v5, v5

    .line 300
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 301
    .line 302
    .line 303
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 304
    .line 305
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 306
    .line 307
    int-to-float v4, v4

    .line 308
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 309
    .line 310
    .line 311
    move-result v5

    .line 312
    int-to-float v5, v5

    .line 313
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 314
    .line 315
    .line 316
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 317
    .line 318
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 319
    .line 320
    sub-int/2addr v4, v0

    .line 321
    int-to-float v4, v4

    .line 322
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 323
    .line 324
    .line 325
    move-result v5

    .line 326
    add-int/2addr v5, v3

    .line 327
    int-to-float v5, v5

    .line 328
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 329
    .line 330
    .line 331
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 332
    .line 333
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 334
    .line 335
    sub-int/2addr v4, v0

    .line 336
    int-to-float v4, v4

    .line 337
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 338
    .line 339
    .line 340
    move-result v5

    .line 341
    add-int/2addr v5, v2

    .line 342
    int-to-float v5, v5

    .line 343
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 344
    .line 345
    .line 346
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 347
    .line 348
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 349
    .line 350
    .line 351
    move-result v4

    .line 352
    add-int/2addr v4, p0

    .line 353
    int-to-float v4, v4

    .line 354
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 355
    .line 356
    .line 357
    move-result v5

    .line 358
    add-int/2addr v5, v2

    .line 359
    int-to-float v5, v5

    .line 360
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 361
    .line 362
    .line 363
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 364
    .line 365
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 366
    .line 367
    .line 368
    move-result v4

    .line 369
    add-int/2addr v4, p0

    .line 370
    int-to-float v4, v4

    .line 371
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 372
    .line 373
    int-to-float v5, v5

    .line 374
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 375
    .line 376
    .line 377
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 378
    .line 379
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 380
    .line 381
    .line 382
    move-result v4

    .line 383
    sub-int/2addr v4, p0

    .line 384
    int-to-float v4, v4

    .line 385
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 386
    .line 387
    int-to-float v5, v5

    .line 388
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 389
    .line 390
    .line 391
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 392
    .line 393
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 394
    .line 395
    .line 396
    move-result v4

    .line 397
    sub-int/2addr v4, p0

    .line 398
    int-to-float p0, v4

    .line 399
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 400
    .line 401
    .line 402
    move-result v4

    .line 403
    add-int/2addr v4, v2

    .line 404
    int-to-float v4, v4

    .line 405
    invoke-virtual {v1, p0, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 406
    .line 407
    .line 408
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 409
    .line 410
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 411
    .line 412
    add-int/2addr v1, v0

    .line 413
    int-to-float v1, v1

    .line 414
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 415
    .line 416
    .line 417
    move-result v4

    .line 418
    add-int/2addr v4, v2

    .line 419
    int-to-float v2, v4

    .line 420
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 421
    .line 422
    .line 423
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 424
    .line 425
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 426
    .line 427
    add-int/2addr v1, v0

    .line 428
    int-to-float v0, v1

    .line 429
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 430
    .line 431
    .line 432
    move-result p1

    .line 433
    add-int/2addr p1, v3

    .line 434
    int-to-float p1, p1

    .line 435
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 436
    .line 437
    .line 438
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 439
    .line 440
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 441
    .line 442
    .line 443
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 444
    .line 445
    return-object p0
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getLeftRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x2

    .line 10
    div-int/2addr v0, v1

    .line 11
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz p0, :cond_0

    .line 24
    .line 25
    array-length v3, p0

    .line 26
    if-ne v3, v1, :cond_0

    .line 27
    .line 28
    int-to-float v0, v0

    .line 29
    const/4 v3, 0x0

    .line 30
    aget-object v3, p0, v3

    .line 31
    .line 32
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    mul-float v0, v0, v3

    .line 37
    .line 38
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    int-to-float v3, v2

    .line 43
    const/4 v4, 0x1

    .line 44
    aget-object p0, p0, v4

    .line 45
    .line 46
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 47
    .line 48
    .line 49
    move-result p0

    .line 50
    mul-float v3, v3, p0

    .line 51
    .line 52
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 53
    .line 54
    .line 55
    move-result p0

    .line 56
    mul-int/lit8 v3, p0, 0x2

    .line 57
    .line 58
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 59
    .line 60
    .line 61
    move-result v4

    .line 62
    if-le v3, v4, :cond_1

    .line 63
    .line 64
    mul-int/lit8 p0, v2, 0x2

    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_0
    int-to-float p0, v0

    .line 68
    const/high16 v0, 0x3f000000    # 0.5f

    .line 69
    .line 70
    mul-float p0, p0, v0

    .line 71
    .line 72
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 73
    .line 74
    .line 75
    move-result p0

    .line 76
    int-to-float v1, v2

    .line 77
    mul-float v1, v1, v0

    .line 78
    .line 79
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    move v5, v0

    .line 84
    move v0, p0

    .line 85
    move p0, v5

    .line 86
    :cond_1
    :goto_0
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 87
    .line 88
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 89
    .line 90
    int-to-float v2, v2

    .line 91
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 92
    .line 93
    .line 94
    move-result v3

    .line 95
    int-to-float v3, v3

    .line 96
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 97
    .line 98
    .line 99
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 100
    .line 101
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 102
    .line 103
    add-int/2addr v2, p0

    .line 104
    int-to-float v2, v2

    .line 105
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 106
    .line 107
    int-to-float v3, v3

    .line 108
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 109
    .line 110
    .line 111
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 112
    .line 113
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 114
    .line 115
    add-int/2addr v2, p0

    .line 116
    int-to-float v2, v2

    .line 117
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 118
    .line 119
    .line 120
    move-result v3

    .line 121
    sub-int/2addr v3, v0

    .line 122
    int-to-float v3, v3

    .line 123
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 124
    .line 125
    .line 126
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 127
    .line 128
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 129
    .line 130
    sub-int/2addr v2, p0

    .line 131
    int-to-float v2, v2

    .line 132
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 133
    .line 134
    .line 135
    move-result v3

    .line 136
    sub-int/2addr v3, v0

    .line 137
    int-to-float v3, v3

    .line 138
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 139
    .line 140
    .line 141
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 142
    .line 143
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 144
    .line 145
    sub-int/2addr v2, p0

    .line 146
    int-to-float v2, v2

    .line 147
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 148
    .line 149
    int-to-float v3, v3

    .line 150
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 151
    .line 152
    .line 153
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 154
    .line 155
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 156
    .line 157
    int-to-float v2, v2

    .line 158
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 159
    .line 160
    .line 161
    move-result v3

    .line 162
    int-to-float v3, v3

    .line 163
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 164
    .line 165
    .line 166
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 167
    .line 168
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 169
    .line 170
    sub-int/2addr v2, p0

    .line 171
    int-to-float v2, v2

    .line 172
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 173
    .line 174
    int-to-float v3, v3

    .line 175
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 176
    .line 177
    .line 178
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 179
    .line 180
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 181
    .line 182
    sub-int/2addr v2, p0

    .line 183
    int-to-float v2, v2

    .line 184
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 185
    .line 186
    .line 187
    move-result v3

    .line 188
    add-int/2addr v3, v0

    .line 189
    int-to-float v3, v3

    .line 190
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 191
    .line 192
    .line 193
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 194
    .line 195
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 196
    .line 197
    add-int/2addr v2, p0

    .line 198
    int-to-float v2, v2

    .line 199
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 200
    .line 201
    .line 202
    move-result v3

    .line 203
    add-int/2addr v3, v0

    .line 204
    int-to-float v0, v3

    .line 205
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 206
    .line 207
    .line 208
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 209
    .line 210
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 211
    .line 212
    add-int/2addr v1, p0

    .line 213
    int-to-float p0, v1

    .line 214
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 215
    .line 216
    int-to-float p1, p1

    .line 217
    invoke-virtual {v0, p0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 218
    .line 219
    .line 220
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 221
    .line 222
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 223
    .line 224
    .line 225
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 226
    .line 227
    return-object p0
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getLeftRightUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/high16 v1, 0x40000000    # 2.0f

    .line 18
    .line 19
    if-eqz p0, :cond_2

    .line 20
    .line 21
    array-length v2, p0

    .line 22
    const/4 v3, 0x3

    .line 23
    if-ne v2, v3, :cond_2

    .line 24
    .line 25
    int-to-float v2, v0

    .line 26
    const/4 v3, 0x0

    .line 27
    aget-object v3, p0, v3

    .line 28
    .line 29
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 30
    .line 31
    .line 32
    move-result v3

    .line 33
    mul-float v3, v3, v2

    .line 34
    .line 35
    div-float/2addr v3, v1

    .line 36
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    const/4 v3, 0x1

    .line 41
    aget-object v3, p0, v3

    .line 42
    .line 43
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 44
    .line 45
    .line 46
    move-result v3

    .line 47
    mul-float v3, v3, v2

    .line 48
    .line 49
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 50
    .line 51
    .line 52
    move-result v3

    .line 53
    const/4 v4, 0x2

    .line 54
    aget-object p0, p0, v4

    .line 55
    .line 56
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 57
    .line 58
    .line 59
    move-result p0

    .line 60
    mul-float v2, v2, p0

    .line 61
    .line 62
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 63
    .line 64
    .line 65
    move-result p0

    .line 66
    if-le v1, v3, :cond_0

    .line 67
    .line 68
    move v1, v3

    .line 69
    :cond_0
    add-int v2, v3, p0

    .line 70
    .line 71
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 72
    .line 73
    .line 74
    move-result v5

    .line 75
    if-le v2, v5, :cond_1

    .line 76
    .line 77
    div-int/2addr v0, v4

    .line 78
    sub-int p0, v0, v3

    .line 79
    .line 80
    :cond_1
    mul-int/lit8 v0, v3, 0x2

    .line 81
    .line 82
    add-int v2, v0, p0

    .line 83
    .line 84
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 85
    .line 86
    .line 87
    move-result v4

    .line 88
    if-le v2, v4, :cond_3

    .line 89
    .line 90
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 91
    .line 92
    .line 93
    move-result p0

    .line 94
    sub-int/2addr p0, v0

    .line 95
    goto :goto_0

    .line 96
    :cond_2
    int-to-float p0, v0

    .line 97
    const v0, 0x3e666666    # 0.225f

    .line 98
    .line 99
    .line 100
    mul-float p0, p0, v0

    .line 101
    .line 102
    div-float v0, p0, v1

    .line 103
    .line 104
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 105
    .line 106
    .line 107
    move-result v1

    .line 108
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 109
    .line 110
    .line 111
    move-result v3

    .line 112
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 113
    .line 114
    .line 115
    move-result p0

    .line 116
    :cond_3
    :goto_0
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 117
    .line 118
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 119
    .line 120
    add-int/2addr v2, p0

    .line 121
    int-to-float v2, v2

    .line 122
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 123
    .line 124
    sub-int/2addr v4, v3

    .line 125
    add-int/2addr v4, v1

    .line 126
    int-to-float v4, v4

    .line 127
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 128
    .line 129
    .line 130
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 131
    .line 132
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 133
    .line 134
    add-int/2addr v2, p0

    .line 135
    int-to-float v2, v2

    .line 136
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 137
    .line 138
    int-to-float v4, v4

    .line 139
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 140
    .line 141
    .line 142
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 143
    .line 144
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 145
    .line 146
    int-to-float v2, v2

    .line 147
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 148
    .line 149
    sub-int/2addr v4, v3

    .line 150
    int-to-float v4, v4

    .line 151
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 152
    .line 153
    .line 154
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 155
    .line 156
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 157
    .line 158
    add-int/2addr v2, p0

    .line 159
    int-to-float v2, v2

    .line 160
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 161
    .line 162
    mul-int/lit8 v5, v3, 0x2

    .line 163
    .line 164
    sub-int/2addr v4, v5

    .line 165
    int-to-float v4, v4

    .line 166
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 167
    .line 168
    .line 169
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 170
    .line 171
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 172
    .line 173
    add-int/2addr v2, p0

    .line 174
    int-to-float v2, v2

    .line 175
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 176
    .line 177
    sub-int/2addr v4, v3

    .line 178
    sub-int/2addr v4, v1

    .line 179
    int-to-float v4, v4

    .line 180
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 181
    .line 182
    .line 183
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 184
    .line 185
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 186
    .line 187
    .line 188
    move-result v2

    .line 189
    sub-int/2addr v2, v1

    .line 190
    int-to-float v2, v2

    .line 191
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 192
    .line 193
    sub-int/2addr v4, v3

    .line 194
    sub-int/2addr v4, v1

    .line 195
    int-to-float v4, v4

    .line 196
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 197
    .line 198
    .line 199
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 200
    .line 201
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 202
    .line 203
    .line 204
    move-result v2

    .line 205
    sub-int/2addr v2, v1

    .line 206
    int-to-float v2, v2

    .line 207
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 208
    .line 209
    add-int/2addr v4, p0

    .line 210
    int-to-float v4, v4

    .line 211
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 212
    .line 213
    .line 214
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 215
    .line 216
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 217
    .line 218
    .line 219
    move-result v2

    .line 220
    sub-int/2addr v2, v3

    .line 221
    int-to-float v2, v2

    .line 222
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 223
    .line 224
    add-int/2addr v4, p0

    .line 225
    int-to-float v4, v4

    .line 226
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 227
    .line 228
    .line 229
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 230
    .line 231
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 232
    .line 233
    .line 234
    move-result v2

    .line 235
    int-to-float v2, v2

    .line 236
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 237
    .line 238
    int-to-float v4, v4

    .line 239
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 240
    .line 241
    .line 242
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 243
    .line 244
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 245
    .line 246
    .line 247
    move-result v2

    .line 248
    add-int/2addr v2, v3

    .line 249
    int-to-float v2, v2

    .line 250
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 251
    .line 252
    add-int/2addr v4, p0

    .line 253
    int-to-float v4, v4

    .line 254
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 255
    .line 256
    .line 257
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 258
    .line 259
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 260
    .line 261
    .line 262
    move-result v2

    .line 263
    add-int/2addr v2, v1

    .line 264
    int-to-float v2, v2

    .line 265
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 266
    .line 267
    add-int/2addr v4, p0

    .line 268
    int-to-float v4, v4

    .line 269
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 270
    .line 271
    .line 272
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 273
    .line 274
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 275
    .line 276
    .line 277
    move-result v2

    .line 278
    add-int/2addr v2, v1

    .line 279
    int-to-float v2, v2

    .line 280
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 281
    .line 282
    sub-int/2addr v4, v3

    .line 283
    sub-int/2addr v4, v1

    .line 284
    int-to-float v4, v4

    .line 285
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 286
    .line 287
    .line 288
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 289
    .line 290
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 291
    .line 292
    sub-int/2addr v2, p0

    .line 293
    int-to-float v2, v2

    .line 294
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 295
    .line 296
    sub-int/2addr v4, v3

    .line 297
    sub-int/2addr v4, v1

    .line 298
    int-to-float v4, v4

    .line 299
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 300
    .line 301
    .line 302
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 303
    .line 304
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 305
    .line 306
    sub-int/2addr v2, p0

    .line 307
    int-to-float v2, v2

    .line 308
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 309
    .line 310
    sub-int/2addr v4, v5

    .line 311
    int-to-float v4, v4

    .line 312
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 313
    .line 314
    .line 315
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 316
    .line 317
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 318
    .line 319
    int-to-float v2, v2

    .line 320
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 321
    .line 322
    sub-int/2addr v4, v3

    .line 323
    int-to-float v4, v4

    .line 324
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 325
    .line 326
    .line 327
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 328
    .line 329
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 330
    .line 331
    sub-int/2addr v2, p0

    .line 332
    int-to-float v2, v2

    .line 333
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 334
    .line 335
    int-to-float v4, v4

    .line 336
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 337
    .line 338
    .line 339
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 340
    .line 341
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 342
    .line 343
    sub-int/2addr v2, p0

    .line 344
    int-to-float p0, v2

    .line 345
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 346
    .line 347
    sub-int/2addr p1, v3

    .line 348
    add-int/2addr p1, v1

    .line 349
    int-to-float p1, p1

    .line 350
    invoke-virtual {v0, p0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 351
    .line 352
    .line 353
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 354
    .line 355
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 356
    .line 357
    .line 358
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 359
    .line 360
    return-object p0
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getLeftUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, 0x2

    .line 18
    if-eqz p0, :cond_1

    .line 19
    .line 20
    array-length v2, p0

    .line 21
    const/4 v3, 0x3

    .line 22
    if-ne v2, v3, :cond_1

    .line 23
    .line 24
    int-to-float v2, v0

    .line 25
    const/4 v3, 0x0

    .line 26
    aget-object v3, p0, v3

    .line 27
    .line 28
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    mul-float v3, v3, v2

    .line 33
    .line 34
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    const/4 v4, 0x1

    .line 39
    aget-object v4, p0, v4

    .line 40
    .line 41
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 42
    .line 43
    .line 44
    move-result v4

    .line 45
    mul-float v4, v4, v2

    .line 46
    .line 47
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 48
    .line 49
    .line 50
    move-result v4

    .line 51
    aget-object p0, p0, v1

    .line 52
    .line 53
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 54
    .line 55
    .line 56
    move-result p0

    .line 57
    mul-float v2, v2, p0

    .line 58
    .line 59
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 60
    .line 61
    .line 62
    move-result p0

    .line 63
    mul-int/lit8 v2, v4, 0x2

    .line 64
    .line 65
    if-le v3, v2, :cond_0

    .line 66
    .line 67
    move v3, v2

    .line 68
    :cond_0
    add-int v5, v2, p0

    .line 69
    .line 70
    if-le v5, v0, :cond_2

    .line 71
    .line 72
    sub-int p0, v0, v2

    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_1
    int-to-float p0, v0

    .line 76
    const/high16 v0, 0x3e800000    # 0.25f

    .line 77
    .line 78
    mul-float p0, p0, v0

    .line 79
    .line 80
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 81
    .line 82
    .line 83
    move-result v3

    .line 84
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 85
    .line 86
    .line 87
    move-result v4

    .line 88
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 89
    .line 90
    .line 91
    move-result p0

    .line 92
    :cond_2
    :goto_0
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 93
    .line 94
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 95
    .line 96
    add-int/2addr v2, p0

    .line 97
    int-to-float v2, v2

    .line 98
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 99
    .line 100
    sub-int/2addr v5, v4

    .line 101
    div-int/2addr v3, v1

    .line 102
    add-int/2addr v5, v3

    .line 103
    int-to-float v1, v5

    .line 104
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 105
    .line 106
    .line 107
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 108
    .line 109
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 110
    .line 111
    add-int/2addr v1, p0

    .line 112
    int-to-float v1, v1

    .line 113
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 114
    .line 115
    int-to-float v2, v2

    .line 116
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 117
    .line 118
    .line 119
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 120
    .line 121
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 122
    .line 123
    int-to-float v1, v1

    .line 124
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 125
    .line 126
    sub-int/2addr v2, v4

    .line 127
    int-to-float v2, v2

    .line 128
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 129
    .line 130
    .line 131
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 132
    .line 133
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 134
    .line 135
    add-int/2addr v1, p0

    .line 136
    int-to-float v1, v1

    .line 137
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 138
    .line 139
    mul-int/lit8 v5, v4, 0x2

    .line 140
    .line 141
    sub-int/2addr v2, v5

    .line 142
    int-to-float v2, v2

    .line 143
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 144
    .line 145
    .line 146
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 147
    .line 148
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 149
    .line 150
    add-int/2addr v1, p0

    .line 151
    int-to-float v1, v1

    .line 152
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 153
    .line 154
    sub-int/2addr v2, v4

    .line 155
    sub-int/2addr v2, v3

    .line 156
    int-to-float v2, v2

    .line 157
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 158
    .line 159
    .line 160
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 161
    .line 162
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 163
    .line 164
    sub-int/2addr v1, v4

    .line 165
    sub-int/2addr v1, v3

    .line 166
    int-to-float v1, v1

    .line 167
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 168
    .line 169
    sub-int/2addr v2, v4

    .line 170
    sub-int/2addr v2, v3

    .line 171
    int-to-float v2, v2

    .line 172
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 173
    .line 174
    .line 175
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 176
    .line 177
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 178
    .line 179
    sub-int/2addr v1, v4

    .line 180
    sub-int/2addr v1, v3

    .line 181
    int-to-float v1, v1

    .line 182
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 183
    .line 184
    add-int/2addr v2, p0

    .line 185
    int-to-float v2, v2

    .line 186
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 187
    .line 188
    .line 189
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 190
    .line 191
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 192
    .line 193
    sub-int/2addr v1, v5

    .line 194
    int-to-float v1, v1

    .line 195
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 196
    .line 197
    add-int/2addr v2, p0

    .line 198
    int-to-float v2, v2

    .line 199
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 200
    .line 201
    .line 202
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 203
    .line 204
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 205
    .line 206
    sub-int/2addr v1, v4

    .line 207
    int-to-float v1, v1

    .line 208
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 209
    .line 210
    int-to-float v2, v2

    .line 211
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 212
    .line 213
    .line 214
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 215
    .line 216
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 217
    .line 218
    int-to-float v1, v1

    .line 219
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 220
    .line 221
    add-int/2addr v2, p0

    .line 222
    int-to-float v2, v2

    .line 223
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 224
    .line 225
    .line 226
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 227
    .line 228
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 229
    .line 230
    sub-int/2addr v1, v4

    .line 231
    add-int/2addr v1, v3

    .line 232
    int-to-float v1, v1

    .line 233
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 234
    .line 235
    add-int/2addr v2, p0

    .line 236
    int-to-float p0, v2

    .line 237
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 238
    .line 239
    .line 240
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 241
    .line 242
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 243
    .line 244
    sub-int/2addr v0, v4

    .line 245
    add-int/2addr v0, v3

    .line 246
    int-to-float v0, v0

    .line 247
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 248
    .line 249
    sub-int/2addr p1, v4

    .line 250
    add-int/2addr p1, v3

    .line 251
    int-to-float p1, p1

    .line 252
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 253
    .line 254
    .line 255
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 256
    .line 257
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 258
    .line 259
    .line 260
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 261
    .line 262
    return-object p0
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getNotchedRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x2

    .line 10
    div-int/2addr v0, v1

    .line 11
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz p0, :cond_0

    .line 24
    .line 25
    array-length v3, p0

    .line 26
    if-ne v3, v1, :cond_0

    .line 27
    .line 28
    int-to-float v0, v0

    .line 29
    const/4 v1, 0x0

    .line 30
    aget-object v1, p0, v1

    .line 31
    .line 32
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    mul-float v0, v0, v1

    .line 37
    .line 38
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    int-to-float v1, v2

    .line 43
    const/4 v2, 0x1

    .line 44
    aget-object p0, p0, v2

    .line 45
    .line 46
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 47
    .line 48
    .line 49
    move-result p0

    .line 50
    mul-float v1, v1, p0

    .line 51
    .line 52
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 53
    .line 54
    .line 55
    move-result p0

    .line 56
    goto :goto_0

    .line 57
    :cond_0
    int-to-float p0, v0

    .line 58
    const/high16 v0, 0x3f000000    # 0.5f

    .line 59
    .line 60
    mul-float p0, p0, v0

    .line 61
    .line 62
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 63
    .line 64
    .line 65
    move-result p0

    .line 66
    int-to-float v1, v2

    .line 67
    mul-float v1, v1, v0

    .line 68
    .line 69
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    move v5, v0

    .line 74
    move v0, p0

    .line 75
    move p0, v5

    .line 76
    :goto_0
    mul-int/lit8 v1, v0, 0x2

    .line 77
    .line 78
    mul-int v1, v1, p0

    .line 79
    .line 80
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 81
    .line 82
    .line 83
    move-result v2

    .line 84
    div-int/2addr v1, v2

    .line 85
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 86
    .line 87
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 88
    .line 89
    int-to-float v3, v3

    .line 90
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 91
    .line 92
    .line 93
    move-result v4

    .line 94
    sub-int/2addr v4, v0

    .line 95
    int-to-float v4, v4

    .line 96
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 97
    .line 98
    .line 99
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 100
    .line 101
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 102
    .line 103
    sub-int/2addr v3, p0

    .line 104
    int-to-float v3, v3

    .line 105
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 106
    .line 107
    .line 108
    move-result v4

    .line 109
    sub-int/2addr v4, v0

    .line 110
    int-to-float v4, v4

    .line 111
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 112
    .line 113
    .line 114
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 115
    .line 116
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 117
    .line 118
    sub-int/2addr v3, p0

    .line 119
    int-to-float v3, v3

    .line 120
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 121
    .line 122
    int-to-float v4, v4

    .line 123
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 124
    .line 125
    .line 126
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 127
    .line 128
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 129
    .line 130
    int-to-float v3, v3

    .line 131
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 132
    .line 133
    .line 134
    move-result v4

    .line 135
    int-to-float v4, v4

    .line 136
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 137
    .line 138
    .line 139
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 140
    .line 141
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 142
    .line 143
    sub-int/2addr v3, p0

    .line 144
    int-to-float v3, v3

    .line 145
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 146
    .line 147
    int-to-float v4, v4

    .line 148
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 149
    .line 150
    .line 151
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 152
    .line 153
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 154
    .line 155
    sub-int/2addr v3, p0

    .line 156
    int-to-float p0, v3

    .line 157
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 158
    .line 159
    .line 160
    move-result v3

    .line 161
    add-int/2addr v3, v0

    .line 162
    int-to-float v3, v3

    .line 163
    invoke-virtual {v2, p0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 164
    .line 165
    .line 166
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 167
    .line 168
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 169
    .line 170
    int-to-float v2, v2

    .line 171
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 172
    .line 173
    .line 174
    move-result v3

    .line 175
    add-int/2addr v3, v0

    .line 176
    int-to-float v0, v3

    .line 177
    invoke-virtual {p0, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 178
    .line 179
    .line 180
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 181
    .line 182
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 183
    .line 184
    add-int/2addr v0, v1

    .line 185
    int-to-float v0, v0

    .line 186
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 187
    .line 188
    .line 189
    move-result p1

    .line 190
    int-to-float p1, p1

    .line 191
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 192
    .line 193
    .line 194
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 195
    .line 196
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 197
    .line 198
    .line 199
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 200
    .line 201
    return-object p0
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getQuadArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, 0x2

    .line 18
    if-eqz p0, :cond_6

    .line 19
    .line 20
    array-length v2, p0

    .line 21
    const/4 v3, 0x4

    .line 22
    if-ne v2, v3, :cond_6

    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    const/4 v4, 0x0

    .line 26
    :goto_0
    if-ge v4, v3, :cond_1

    .line 27
    .line 28
    aget-object v5, p0, v4

    .line 29
    .line 30
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 31
    .line 32
    .line 33
    move-result v5

    .line 34
    const/high16 v6, 0x3f800000    # 1.0f

    .line 35
    .line 36
    cmpl-float v5, v5, v6

    .line 37
    .line 38
    if-lez v5, :cond_0

    .line 39
    .line 40
    if-eq v4, v1, :cond_0

    .line 41
    .line 42
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 43
    .line 44
    .line 45
    move-result-object v5

    .line 46
    aput-object v5, p0, v4

    .line 47
    .line 48
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    int-to-float v3, v0

    .line 52
    aget-object v4, p0, v2

    .line 53
    .line 54
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 55
    .line 56
    .line 57
    move-result v4

    .line 58
    mul-float v4, v4, v3

    .line 59
    .line 60
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 61
    .line 62
    .line 63
    move-result v4

    .line 64
    const/4 v5, 0x1

    .line 65
    aget-object v5, p0, v5

    .line 66
    .line 67
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 68
    .line 69
    .line 70
    move-result v5

    .line 71
    mul-float v5, v5, v3

    .line 72
    .line 73
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 74
    .line 75
    .line 76
    move-result v5

    .line 77
    aget-object v6, p0, v1

    .line 78
    .line 79
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 80
    .line 81
    .line 82
    move-result v6

    .line 83
    mul-float v6, v6, v3

    .line 84
    .line 85
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 86
    .line 87
    .line 88
    move-result v6

    .line 89
    const/4 v7, 0x3

    .line 90
    aget-object p0, p0, v7

    .line 91
    .line 92
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 93
    .line 94
    .line 95
    move-result p0

    .line 96
    mul-float v3, v3, p0

    .line 97
    .line 98
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 99
    .line 100
    .line 101
    move-result p0

    .line 102
    mul-int/lit8 v3, v5, 0x2

    .line 103
    .line 104
    if-le v4, v3, :cond_2

    .line 105
    .line 106
    move v4, v3

    .line 107
    :cond_2
    if-le p0, v3, :cond_3

    .line 108
    .line 109
    move p0, v3

    .line 110
    :cond_3
    if-lt v3, v0, :cond_4

    .line 111
    .line 112
    div-int/lit8 v5, v0, 0x2

    .line 113
    .line 114
    goto :goto_1

    .line 115
    :cond_4
    move v2, v6

    .line 116
    :goto_1
    mul-int/lit8 v3, v2, 0x2

    .line 117
    .line 118
    if-lt v3, v0, :cond_5

    .line 119
    .line 120
    div-int/lit8 v2, v0, 0x2

    .line 121
    .line 122
    :cond_5
    add-int v3, v5, v2

    .line 123
    .line 124
    div-int/2addr v0, v1

    .line 125
    if-le v3, v0, :cond_7

    .line 126
    .line 127
    sub-int v2, v0, v5

    .line 128
    .line 129
    goto :goto_2

    .line 130
    :cond_6
    int-to-float p0, v0

    .line 131
    const v0, 0x3e3d97f6    # 0.18515f

    .line 132
    .line 133
    .line 134
    mul-float v0, v0, p0

    .line 135
    .line 136
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 137
    .line 138
    .line 139
    move-result v4

    .line 140
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 141
    .line 142
    .line 143
    move-result v5

    .line 144
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 145
    .line 146
    .line 147
    move-result v2

    .line 148
    const v0, 0x3ef5c28f    # 0.48f

    .line 149
    .line 150
    .line 151
    mul-float p0, p0, v0

    .line 152
    .line 153
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 154
    .line 155
    .line 156
    move-result p0

    .line 157
    :cond_7
    :goto_2
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 158
    .line 159
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 160
    .line 161
    int-to-float v3, v3

    .line 162
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 163
    .line 164
    .line 165
    move-result v6

    .line 166
    int-to-float v6, v6

    .line 167
    invoke-virtual {v0, v3, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 168
    .line 169
    .line 170
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 171
    .line 172
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 173
    .line 174
    add-int/2addr v3, v2

    .line 175
    int-to-float v3, v3

    .line 176
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 177
    .line 178
    .line 179
    move-result v6

    .line 180
    sub-int/2addr v6, v5

    .line 181
    int-to-float v6, v6

    .line 182
    invoke-virtual {v0, v3, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 183
    .line 184
    .line 185
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 186
    .line 187
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 188
    .line 189
    add-int/2addr v3, v2

    .line 190
    int-to-float v3, v3

    .line 191
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 192
    .line 193
    .line 194
    move-result v6

    .line 195
    div-int/2addr v4, v1

    .line 196
    sub-int/2addr v6, v4

    .line 197
    int-to-float v6, v6

    .line 198
    invoke-virtual {v0, v3, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 199
    .line 200
    .line 201
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 202
    .line 203
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 204
    .line 205
    .line 206
    move-result v3

    .line 207
    div-int/2addr p0, v1

    .line 208
    sub-int/2addr v3, p0

    .line 209
    int-to-float v1, v3

    .line 210
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 211
    .line 212
    .line 213
    move-result v3

    .line 214
    sub-int/2addr v3, v4

    .line 215
    int-to-float v3, v3

    .line 216
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 217
    .line 218
    .line 219
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 220
    .line 221
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 222
    .line 223
    .line 224
    move-result v1

    .line 225
    sub-int/2addr v1, p0

    .line 226
    int-to-float v1, v1

    .line 227
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 228
    .line 229
    .line 230
    move-result v3

    .line 231
    sub-int/2addr v3, p0

    .line 232
    int-to-float v3, v3

    .line 233
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 234
    .line 235
    .line 236
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 237
    .line 238
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 239
    .line 240
    .line 241
    move-result v1

    .line 242
    sub-int/2addr v1, v4

    .line 243
    int-to-float v1, v1

    .line 244
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 245
    .line 246
    .line 247
    move-result v3

    .line 248
    sub-int/2addr v3, p0

    .line 249
    int-to-float v3, v3

    .line 250
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 251
    .line 252
    .line 253
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 254
    .line 255
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 256
    .line 257
    .line 258
    move-result v1

    .line 259
    sub-int/2addr v1, v4

    .line 260
    int-to-float v1, v1

    .line 261
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 262
    .line 263
    add-int/2addr v3, v2

    .line 264
    int-to-float v3, v3

    .line 265
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 266
    .line 267
    .line 268
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 269
    .line 270
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 271
    .line 272
    .line 273
    move-result v1

    .line 274
    sub-int/2addr v1, v5

    .line 275
    int-to-float v1, v1

    .line 276
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 277
    .line 278
    add-int/2addr v3, v2

    .line 279
    int-to-float v3, v3

    .line 280
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 281
    .line 282
    .line 283
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 284
    .line 285
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 286
    .line 287
    .line 288
    move-result v1

    .line 289
    int-to-float v1, v1

    .line 290
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 291
    .line 292
    int-to-float v3, v3

    .line 293
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 294
    .line 295
    .line 296
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 297
    .line 298
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 299
    .line 300
    .line 301
    move-result v1

    .line 302
    add-int/2addr v1, v5

    .line 303
    int-to-float v1, v1

    .line 304
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 305
    .line 306
    add-int/2addr v3, v2

    .line 307
    int-to-float v3, v3

    .line 308
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 309
    .line 310
    .line 311
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 312
    .line 313
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 314
    .line 315
    .line 316
    move-result v1

    .line 317
    add-int/2addr v1, v4

    .line 318
    int-to-float v1, v1

    .line 319
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 320
    .line 321
    add-int/2addr v3, v2

    .line 322
    int-to-float v3, v3

    .line 323
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 324
    .line 325
    .line 326
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 327
    .line 328
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 329
    .line 330
    .line 331
    move-result v1

    .line 332
    add-int/2addr v1, v4

    .line 333
    int-to-float v1, v1

    .line 334
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 335
    .line 336
    .line 337
    move-result v3

    .line 338
    sub-int/2addr v3, p0

    .line 339
    int-to-float v3, v3

    .line 340
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 341
    .line 342
    .line 343
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 344
    .line 345
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 346
    .line 347
    .line 348
    move-result v1

    .line 349
    add-int/2addr v1, p0

    .line 350
    int-to-float v1, v1

    .line 351
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 352
    .line 353
    .line 354
    move-result v3

    .line 355
    sub-int/2addr v3, p0

    .line 356
    int-to-float v3, v3

    .line 357
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 358
    .line 359
    .line 360
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 361
    .line 362
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 363
    .line 364
    .line 365
    move-result v1

    .line 366
    add-int/2addr v1, p0

    .line 367
    int-to-float v1, v1

    .line 368
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 369
    .line 370
    .line 371
    move-result v3

    .line 372
    sub-int/2addr v3, v4

    .line 373
    int-to-float v3, v3

    .line 374
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 375
    .line 376
    .line 377
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 378
    .line 379
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 380
    .line 381
    sub-int/2addr v1, v2

    .line 382
    int-to-float v1, v1

    .line 383
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 384
    .line 385
    .line 386
    move-result v3

    .line 387
    sub-int/2addr v3, v4

    .line 388
    int-to-float v3, v3

    .line 389
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 390
    .line 391
    .line 392
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 393
    .line 394
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 395
    .line 396
    sub-int/2addr v1, v2

    .line 397
    int-to-float v1, v1

    .line 398
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 399
    .line 400
    .line 401
    move-result v3

    .line 402
    sub-int/2addr v3, v5

    .line 403
    int-to-float v3, v3

    .line 404
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 405
    .line 406
    .line 407
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 408
    .line 409
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 410
    .line 411
    int-to-float v1, v1

    .line 412
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 413
    .line 414
    .line 415
    move-result v3

    .line 416
    int-to-float v3, v3

    .line 417
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 418
    .line 419
    .line 420
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 421
    .line 422
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 423
    .line 424
    sub-int/2addr v1, v2

    .line 425
    int-to-float v1, v1

    .line 426
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 427
    .line 428
    .line 429
    move-result v3

    .line 430
    add-int/2addr v3, v5

    .line 431
    int-to-float v3, v3

    .line 432
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 433
    .line 434
    .line 435
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 436
    .line 437
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 438
    .line 439
    sub-int/2addr v1, v2

    .line 440
    int-to-float v1, v1

    .line 441
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 442
    .line 443
    .line 444
    move-result v3

    .line 445
    add-int/2addr v3, v4

    .line 446
    int-to-float v3, v3

    .line 447
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 448
    .line 449
    .line 450
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 451
    .line 452
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 453
    .line 454
    .line 455
    move-result v1

    .line 456
    add-int/2addr v1, p0

    .line 457
    int-to-float v1, v1

    .line 458
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 459
    .line 460
    .line 461
    move-result v3

    .line 462
    add-int/2addr v3, v4

    .line 463
    int-to-float v3, v3

    .line 464
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 465
    .line 466
    .line 467
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 468
    .line 469
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 470
    .line 471
    .line 472
    move-result v1

    .line 473
    add-int/2addr v1, p0

    .line 474
    int-to-float v1, v1

    .line 475
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 476
    .line 477
    .line 478
    move-result v3

    .line 479
    add-int/2addr v3, p0

    .line 480
    int-to-float v3, v3

    .line 481
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 482
    .line 483
    .line 484
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 485
    .line 486
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 487
    .line 488
    .line 489
    move-result v1

    .line 490
    add-int/2addr v1, v4

    .line 491
    int-to-float v1, v1

    .line 492
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 493
    .line 494
    .line 495
    move-result v3

    .line 496
    add-int/2addr v3, p0

    .line 497
    int-to-float v3, v3

    .line 498
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 499
    .line 500
    .line 501
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 502
    .line 503
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 504
    .line 505
    .line 506
    move-result v1

    .line 507
    add-int/2addr v1, v4

    .line 508
    int-to-float v1, v1

    .line 509
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 510
    .line 511
    sub-int/2addr v3, v2

    .line 512
    int-to-float v3, v3

    .line 513
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 514
    .line 515
    .line 516
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 517
    .line 518
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 519
    .line 520
    .line 521
    move-result v1

    .line 522
    add-int/2addr v1, v5

    .line 523
    int-to-float v1, v1

    .line 524
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 525
    .line 526
    sub-int/2addr v3, v2

    .line 527
    int-to-float v3, v3

    .line 528
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 529
    .line 530
    .line 531
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 532
    .line 533
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 534
    .line 535
    .line 536
    move-result v1

    .line 537
    int-to-float v1, v1

    .line 538
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 539
    .line 540
    int-to-float v3, v3

    .line 541
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 542
    .line 543
    .line 544
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 545
    .line 546
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 547
    .line 548
    .line 549
    move-result v1

    .line 550
    sub-int/2addr v1, v5

    .line 551
    int-to-float v1, v1

    .line 552
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 553
    .line 554
    sub-int/2addr v3, v2

    .line 555
    int-to-float v3, v3

    .line 556
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 557
    .line 558
    .line 559
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 560
    .line 561
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 562
    .line 563
    .line 564
    move-result v1

    .line 565
    sub-int/2addr v1, v4

    .line 566
    int-to-float v1, v1

    .line 567
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 568
    .line 569
    sub-int/2addr v3, v2

    .line 570
    int-to-float v3, v3

    .line 571
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 572
    .line 573
    .line 574
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 575
    .line 576
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 577
    .line 578
    .line 579
    move-result v1

    .line 580
    sub-int/2addr v1, v4

    .line 581
    int-to-float v1, v1

    .line 582
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 583
    .line 584
    .line 585
    move-result v3

    .line 586
    add-int/2addr v3, p0

    .line 587
    int-to-float v3, v3

    .line 588
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 589
    .line 590
    .line 591
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 592
    .line 593
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 594
    .line 595
    .line 596
    move-result v1

    .line 597
    sub-int/2addr v1, p0

    .line 598
    int-to-float v1, v1

    .line 599
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 600
    .line 601
    .line 602
    move-result v3

    .line 603
    add-int/2addr v3, p0

    .line 604
    int-to-float v3, v3

    .line 605
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 606
    .line 607
    .line 608
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 609
    .line 610
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 611
    .line 612
    .line 613
    move-result v1

    .line 614
    sub-int/2addr v1, p0

    .line 615
    int-to-float p0, v1

    .line 616
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 617
    .line 618
    .line 619
    move-result v1

    .line 620
    add-int/2addr v1, v4

    .line 621
    int-to-float v1, v1

    .line 622
    invoke-virtual {v0, p0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 623
    .line 624
    .line 625
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 626
    .line 627
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 628
    .line 629
    add-int/2addr v0, v2

    .line 630
    int-to-float v0, v0

    .line 631
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 632
    .line 633
    .line 634
    move-result v1

    .line 635
    add-int/2addr v1, v4

    .line 636
    int-to-float v1, v1

    .line 637
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 638
    .line 639
    .line 640
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 641
    .line 642
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 643
    .line 644
    add-int/2addr v0, v2

    .line 645
    int-to-float v0, v0

    .line 646
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 647
    .line 648
    .line 649
    move-result p1

    .line 650
    add-int/2addr p1, v5

    .line 651
    int-to-float p1, p1

    .line 652
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 653
    .line 654
    .line 655
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 656
    .line 657
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 658
    .line 659
    .line 660
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 661
    .line 662
    return-object p0
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getQuadArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, 0x2

    .line 18
    div-int/2addr v0, v1

    .line 19
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 24
    .line 25
    .line 26
    move-result v3

    .line 27
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    if-eqz p0, :cond_1

    .line 32
    .line 33
    array-length v3, p0

    .line 34
    const/4 v4, 0x3

    .line 35
    if-ne v3, v4, :cond_1

    .line 36
    .line 37
    int-to-float v0, v0

    .line 38
    const/4 v3, 0x0

    .line 39
    aget-object v3, p0, v3

    .line 40
    .line 41
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    mul-float v0, v0, v3

    .line 46
    .line 47
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    int-to-float v3, v2

    .line 52
    const/4 v4, 0x1

    .line 53
    aget-object v4, p0, v4

    .line 54
    .line 55
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 56
    .line 57
    .line 58
    move-result v4

    .line 59
    mul-float v4, v4, v3

    .line 60
    .line 61
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 62
    .line 63
    .line 64
    move-result v4

    .line 65
    aget-object p0, p0, v1

    .line 66
    .line 67
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 68
    .line 69
    .line 70
    move-result p0

    .line 71
    mul-float v3, v3, p0

    .line 72
    .line 73
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 74
    .line 75
    .line 76
    move-result p0

    .line 77
    if-le v0, v4, :cond_0

    .line 78
    .line 79
    move v0, v4

    .line 80
    :cond_0
    add-int v3, v4, p0

    .line 81
    .line 82
    div-int/2addr v2, v1

    .line 83
    if-le v3, v2, :cond_2

    .line 84
    .line 85
    sub-int p0, v2, v4

    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_1
    int-to-float p0, v0

    .line 89
    const v0, 0x3e666666    # 0.225f

    .line 90
    .line 91
    .line 92
    mul-float p0, p0, v0

    .line 93
    .line 94
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 95
    .line 96
    .line 97
    move-result p0

    .line 98
    int-to-float v1, v2

    .line 99
    mul-float v1, v1, v0

    .line 100
    .line 101
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 102
    .line 103
    .line 104
    move-result v4

    .line 105
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 106
    .line 107
    .line 108
    move-result v0

    .line 109
    move v5, v0

    .line 110
    move v0, p0

    .line 111
    move p0, v5

    .line 112
    :cond_2
    :goto_0
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 113
    .line 114
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 115
    .line 116
    int-to-float v2, v2

    .line 117
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 118
    .line 119
    .line 120
    move-result v3

    .line 121
    int-to-float v3, v3

    .line 122
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 123
    .line 124
    .line 125
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 126
    .line 127
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 128
    .line 129
    add-int/2addr v2, p0

    .line 130
    int-to-float v2, v2

    .line 131
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 132
    .line 133
    .line 134
    move-result v3

    .line 135
    sub-int/2addr v3, v4

    .line 136
    int-to-float v3, v3

    .line 137
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 138
    .line 139
    .line 140
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 141
    .line 142
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 143
    .line 144
    add-int/2addr v2, p0

    .line 145
    int-to-float v2, v2

    .line 146
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 147
    .line 148
    .line 149
    move-result v3

    .line 150
    sub-int/2addr v3, v0

    .line 151
    int-to-float v3, v3

    .line 152
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 153
    .line 154
    .line 155
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 156
    .line 157
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 158
    .line 159
    .line 160
    move-result v2

    .line 161
    sub-int/2addr v2, v0

    .line 162
    int-to-float v2, v2

    .line 163
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 164
    .line 165
    .line 166
    move-result v3

    .line 167
    sub-int/2addr v3, v0

    .line 168
    int-to-float v3, v3

    .line 169
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 170
    .line 171
    .line 172
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 173
    .line 174
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 175
    .line 176
    .line 177
    move-result v2

    .line 178
    sub-int/2addr v2, v0

    .line 179
    int-to-float v2, v2

    .line 180
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 181
    .line 182
    add-int/2addr v3, p0

    .line 183
    int-to-float v3, v3

    .line 184
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 185
    .line 186
    .line 187
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 188
    .line 189
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 190
    .line 191
    .line 192
    move-result v2

    .line 193
    sub-int/2addr v2, v4

    .line 194
    int-to-float v2, v2

    .line 195
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 196
    .line 197
    add-int/2addr v3, p0

    .line 198
    int-to-float v3, v3

    .line 199
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 200
    .line 201
    .line 202
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 203
    .line 204
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 205
    .line 206
    .line 207
    move-result v2

    .line 208
    int-to-float v2, v2

    .line 209
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 210
    .line 211
    int-to-float v3, v3

    .line 212
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 213
    .line 214
    .line 215
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 216
    .line 217
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 218
    .line 219
    .line 220
    move-result v2

    .line 221
    add-int/2addr v2, v4

    .line 222
    int-to-float v2, v2

    .line 223
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 224
    .line 225
    add-int/2addr v3, p0

    .line 226
    int-to-float v3, v3

    .line 227
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 228
    .line 229
    .line 230
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 231
    .line 232
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 233
    .line 234
    .line 235
    move-result v2

    .line 236
    add-int/2addr v2, v0

    .line 237
    int-to-float v2, v2

    .line 238
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 239
    .line 240
    add-int/2addr v3, p0

    .line 241
    int-to-float v3, v3

    .line 242
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 243
    .line 244
    .line 245
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 246
    .line 247
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 248
    .line 249
    .line 250
    move-result v2

    .line 251
    add-int/2addr v2, v0

    .line 252
    int-to-float v2, v2

    .line 253
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 254
    .line 255
    .line 256
    move-result v3

    .line 257
    sub-int/2addr v3, v0

    .line 258
    int-to-float v3, v3

    .line 259
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 260
    .line 261
    .line 262
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 263
    .line 264
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 265
    .line 266
    sub-int/2addr v2, p0

    .line 267
    int-to-float v2, v2

    .line 268
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 269
    .line 270
    .line 271
    move-result v3

    .line 272
    sub-int/2addr v3, v0

    .line 273
    int-to-float v3, v3

    .line 274
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 275
    .line 276
    .line 277
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 278
    .line 279
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 280
    .line 281
    sub-int/2addr v2, p0

    .line 282
    int-to-float v2, v2

    .line 283
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 284
    .line 285
    .line 286
    move-result v3

    .line 287
    sub-int/2addr v3, v4

    .line 288
    int-to-float v3, v3

    .line 289
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 290
    .line 291
    .line 292
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 293
    .line 294
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 295
    .line 296
    int-to-float v2, v2

    .line 297
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 298
    .line 299
    .line 300
    move-result v3

    .line 301
    int-to-float v3, v3

    .line 302
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 303
    .line 304
    .line 305
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 306
    .line 307
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 308
    .line 309
    sub-int/2addr v2, p0

    .line 310
    int-to-float v2, v2

    .line 311
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 312
    .line 313
    .line 314
    move-result v3

    .line 315
    add-int/2addr v3, v4

    .line 316
    int-to-float v3, v3

    .line 317
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 318
    .line 319
    .line 320
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 321
    .line 322
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 323
    .line 324
    sub-int/2addr v2, p0

    .line 325
    int-to-float v2, v2

    .line 326
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 327
    .line 328
    .line 329
    move-result v3

    .line 330
    add-int/2addr v3, v0

    .line 331
    int-to-float v3, v3

    .line 332
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 333
    .line 334
    .line 335
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 336
    .line 337
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 338
    .line 339
    .line 340
    move-result v2

    .line 341
    add-int/2addr v2, v0

    .line 342
    int-to-float v2, v2

    .line 343
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 344
    .line 345
    .line 346
    move-result v3

    .line 347
    add-int/2addr v3, v0

    .line 348
    int-to-float v3, v3

    .line 349
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 350
    .line 351
    .line 352
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 353
    .line 354
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 355
    .line 356
    .line 357
    move-result v2

    .line 358
    add-int/2addr v2, v0

    .line 359
    int-to-float v2, v2

    .line 360
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 361
    .line 362
    sub-int/2addr v3, p0

    .line 363
    int-to-float v3, v3

    .line 364
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 365
    .line 366
    .line 367
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 368
    .line 369
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 370
    .line 371
    .line 372
    move-result v2

    .line 373
    add-int/2addr v2, v4

    .line 374
    int-to-float v2, v2

    .line 375
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 376
    .line 377
    sub-int/2addr v3, p0

    .line 378
    int-to-float v3, v3

    .line 379
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 380
    .line 381
    .line 382
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 383
    .line 384
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 385
    .line 386
    .line 387
    move-result v2

    .line 388
    int-to-float v2, v2

    .line 389
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 390
    .line 391
    int-to-float v3, v3

    .line 392
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 393
    .line 394
    .line 395
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 396
    .line 397
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 398
    .line 399
    .line 400
    move-result v2

    .line 401
    sub-int/2addr v2, v4

    .line 402
    int-to-float v2, v2

    .line 403
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 404
    .line 405
    sub-int/2addr v3, p0

    .line 406
    int-to-float v3, v3

    .line 407
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 408
    .line 409
    .line 410
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 411
    .line 412
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 413
    .line 414
    .line 415
    move-result v2

    .line 416
    sub-int/2addr v2, v0

    .line 417
    int-to-float v2, v2

    .line 418
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 419
    .line 420
    sub-int/2addr v3, p0

    .line 421
    int-to-float v3, v3

    .line 422
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 423
    .line 424
    .line 425
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 426
    .line 427
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 428
    .line 429
    .line 430
    move-result v2

    .line 431
    sub-int/2addr v2, v0

    .line 432
    int-to-float v2, v2

    .line 433
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 434
    .line 435
    .line 436
    move-result v3

    .line 437
    add-int/2addr v3, v0

    .line 438
    int-to-float v3, v3

    .line 439
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 440
    .line 441
    .line 442
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 443
    .line 444
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 445
    .line 446
    add-int/2addr v2, p0

    .line 447
    int-to-float v2, v2

    .line 448
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 449
    .line 450
    .line 451
    move-result v3

    .line 452
    add-int/2addr v3, v0

    .line 453
    int-to-float v0, v3

    .line 454
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 455
    .line 456
    .line 457
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 458
    .line 459
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 460
    .line 461
    add-int/2addr v1, p0

    .line 462
    int-to-float p0, v1

    .line 463
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 464
    .line 465
    .line 466
    move-result p1

    .line 467
    add-int/2addr p1, v4

    .line 468
    int-to-float p1, p1

    .line 469
    invoke-virtual {v0, p0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 470
    .line 471
    .line 472
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 473
    .line 474
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 475
    .line 476
    .line 477
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 478
    .line 479
    return-object p0
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getRightArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, 0x2

    .line 18
    if-eqz p0, :cond_4

    .line 19
    .line 20
    array-length v2, p0

    .line 21
    const/4 v3, 0x4

    .line 22
    if-ne v2, v3, :cond_4

    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    const/4 v4, 0x0

    .line 26
    :goto_0
    if-ge v4, v3, :cond_1

    .line 27
    .line 28
    aget-object v5, p0, v4

    .line 29
    .line 30
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 31
    .line 32
    .line 33
    move-result v5

    .line 34
    const/high16 v6, 0x3f800000    # 1.0f

    .line 35
    .line 36
    cmpl-float v5, v5, v6

    .line 37
    .line 38
    if-lez v5, :cond_0

    .line 39
    .line 40
    if-eq v4, v1, :cond_0

    .line 41
    .line 42
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 43
    .line 44
    .line 45
    move-result-object v5

    .line 46
    aput-object v5, p0, v4

    .line 47
    .line 48
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    int-to-float v0, v0

    .line 52
    aget-object v2, p0, v2

    .line 53
    .line 54
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    mul-float v2, v2, v0

    .line 59
    .line 60
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    const/4 v3, 0x1

    .line 65
    aget-object v3, p0, v3

    .line 66
    .line 67
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 68
    .line 69
    .line 70
    move-result v3

    .line 71
    mul-float v3, v3, v0

    .line 72
    .line 73
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 74
    .line 75
    .line 76
    move-result v3

    .line 77
    aget-object v4, p0, v1

    .line 78
    .line 79
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 80
    .line 81
    .line 82
    move-result v4

    .line 83
    mul-float v0, v0, v4

    .line 84
    .line 85
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 90
    .line 91
    .line 92
    move-result v4

    .line 93
    int-to-float v4, v4

    .line 94
    const/4 v5, 0x3

    .line 95
    aget-object p0, p0, v5

    .line 96
    .line 97
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 98
    .line 99
    .line 100
    move-result p0

    .line 101
    mul-float v4, v4, p0

    .line 102
    .line 103
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 104
    .line 105
    .line 106
    move-result p0

    .line 107
    mul-int/lit8 v4, v3, 0x2

    .line 108
    .line 109
    if-le v2, v4, :cond_2

    .line 110
    .line 111
    move v2, v4

    .line 112
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 113
    .line 114
    .line 115
    move-result v4

    .line 116
    if-le v0, v4, :cond_3

    .line 117
    .line 118
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    :cond_3
    add-int v4, p0, v0

    .line 123
    .line 124
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 125
    .line 126
    .line 127
    move-result v5

    .line 128
    if-le v4, v5, :cond_5

    .line 129
    .line 130
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 131
    .line 132
    .line 133
    move-result p0

    .line 134
    sub-int/2addr p0, v0

    .line 135
    goto :goto_1

    .line 136
    :cond_4
    int-to-float p0, v0

    .line 137
    const/high16 v0, 0x3e800000    # 0.25f

    .line 138
    .line 139
    mul-float p0, p0, v0

    .line 140
    .line 141
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 142
    .line 143
    .line 144
    move-result v2

    .line 145
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 146
    .line 147
    .line 148
    move-result v3

    .line 149
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 150
    .line 151
    .line 152
    move-result v0

    .line 153
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 154
    .line 155
    .line 156
    move-result p0

    .line 157
    int-to-float p0, p0

    .line 158
    const v4, 0x3f266666    # 0.65f

    .line 159
    .line 160
    .line 161
    mul-float p0, p0, v4

    .line 162
    .line 163
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 164
    .line 165
    .line 166
    move-result p0

    .line 167
    :cond_5
    :goto_1
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 168
    .line 169
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 170
    .line 171
    int-to-float v5, v5

    .line 172
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 173
    .line 174
    int-to-float v6, v6

    .line 175
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 176
    .line 177
    .line 178
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 179
    .line 180
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 181
    .line 182
    add-int/2addr v5, p0

    .line 183
    int-to-float v5, v5

    .line 184
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 185
    .line 186
    int-to-float v6, v6

    .line 187
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 188
    .line 189
    .line 190
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 191
    .line 192
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 193
    .line 194
    add-int/2addr v5, p0

    .line 195
    int-to-float v5, v5

    .line 196
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 197
    .line 198
    .line 199
    move-result v6

    .line 200
    div-int/2addr v2, v1

    .line 201
    sub-int/2addr v6, v2

    .line 202
    int-to-float v1, v6

    .line 203
    invoke-virtual {v4, v5, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 204
    .line 205
    .line 206
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 207
    .line 208
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 209
    .line 210
    sub-int/2addr v4, v0

    .line 211
    int-to-float v4, v4

    .line 212
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 213
    .line 214
    .line 215
    move-result v5

    .line 216
    sub-int/2addr v5, v2

    .line 217
    int-to-float v5, v5

    .line 218
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 219
    .line 220
    .line 221
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 222
    .line 223
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 224
    .line 225
    sub-int/2addr v4, v0

    .line 226
    int-to-float v4, v4

    .line 227
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 228
    .line 229
    .line 230
    move-result v5

    .line 231
    sub-int/2addr v5, v3

    .line 232
    int-to-float v5, v5

    .line 233
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 234
    .line 235
    .line 236
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 237
    .line 238
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 239
    .line 240
    int-to-float v4, v4

    .line 241
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 242
    .line 243
    .line 244
    move-result v5

    .line 245
    int-to-float v5, v5

    .line 246
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 247
    .line 248
    .line 249
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 250
    .line 251
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 252
    .line 253
    sub-int/2addr v4, v0

    .line 254
    int-to-float v4, v4

    .line 255
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 256
    .line 257
    .line 258
    move-result v5

    .line 259
    add-int/2addr v5, v3

    .line 260
    int-to-float v3, v5

    .line 261
    invoke-virtual {v1, v4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 262
    .line 263
    .line 264
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 265
    .line 266
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 267
    .line 268
    sub-int/2addr v3, v0

    .line 269
    int-to-float v0, v3

    .line 270
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 271
    .line 272
    .line 273
    move-result v3

    .line 274
    add-int/2addr v3, v2

    .line 275
    int-to-float v3, v3

    .line 276
    invoke-virtual {v1, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 277
    .line 278
    .line 279
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 280
    .line 281
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 282
    .line 283
    add-int/2addr v1, p0

    .line 284
    int-to-float v1, v1

    .line 285
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 286
    .line 287
    .line 288
    move-result v3

    .line 289
    add-int/2addr v3, v2

    .line 290
    int-to-float v2, v3

    .line 291
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 292
    .line 293
    .line 294
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 295
    .line 296
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 297
    .line 298
    add-int/2addr v1, p0

    .line 299
    int-to-float p0, v1

    .line 300
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 301
    .line 302
    int-to-float v1, v1

    .line 303
    invoke-virtual {v0, p0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 304
    .line 305
    .line 306
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 307
    .line 308
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 309
    .line 310
    int-to-float v0, v0

    .line 311
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 312
    .line 313
    int-to-float p1, p1

    .line 314
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 315
    .line 316
    .line 317
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 318
    .line 319
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 320
    .line 321
    .line 322
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 323
    .line 324
    return-object p0
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x2

    .line 10
    div-int/2addr v0, v1

    .line 11
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz p0, :cond_0

    .line 24
    .line 25
    array-length v3, p0

    .line 26
    if-ne v3, v1, :cond_0

    .line 27
    .line 28
    int-to-float v0, v0

    .line 29
    const/4 v1, 0x0

    .line 30
    aget-object v1, p0, v1

    .line 31
    .line 32
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    mul-float v0, v0, v1

    .line 37
    .line 38
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    int-to-float v1, v2

    .line 43
    const/4 v2, 0x1

    .line 44
    aget-object p0, p0, v2

    .line 45
    .line 46
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 47
    .line 48
    .line 49
    move-result p0

    .line 50
    mul-float v1, v1, p0

    .line 51
    .line 52
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 53
    .line 54
    .line 55
    move-result p0

    .line 56
    goto :goto_0

    .line 57
    :cond_0
    int-to-float p0, v0

    .line 58
    const/high16 v0, 0x3f000000    # 0.5f

    .line 59
    .line 60
    mul-float p0, p0, v0

    .line 61
    .line 62
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 63
    .line 64
    .line 65
    move-result p0

    .line 66
    int-to-float v1, v2

    .line 67
    mul-float v1, v1, v0

    .line 68
    .line 69
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    move v4, v0

    .line 74
    move v0, p0

    .line 75
    move p0, v4

    .line 76
    :goto_0
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 77
    .line 78
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 79
    .line 80
    int-to-float v2, v2

    .line 81
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 82
    .line 83
    .line 84
    move-result v3

    .line 85
    sub-int/2addr v3, v0

    .line 86
    int-to-float v3, v3

    .line 87
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 88
    .line 89
    .line 90
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 91
    .line 92
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 93
    .line 94
    sub-int/2addr v2, p0

    .line 95
    int-to-float v2, v2

    .line 96
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 97
    .line 98
    .line 99
    move-result v3

    .line 100
    sub-int/2addr v3, v0

    .line 101
    int-to-float v3, v3

    .line 102
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 103
    .line 104
    .line 105
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 106
    .line 107
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 108
    .line 109
    sub-int/2addr v2, p0

    .line 110
    int-to-float v2, v2

    .line 111
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 112
    .line 113
    int-to-float v3, v3

    .line 114
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 115
    .line 116
    .line 117
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 118
    .line 119
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 120
    .line 121
    int-to-float v2, v2

    .line 122
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 123
    .line 124
    .line 125
    move-result v3

    .line 126
    int-to-float v3, v3

    .line 127
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 128
    .line 129
    .line 130
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 131
    .line 132
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 133
    .line 134
    sub-int/2addr v2, p0

    .line 135
    int-to-float v2, v2

    .line 136
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 137
    .line 138
    int-to-float v3, v3

    .line 139
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 140
    .line 141
    .line 142
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 143
    .line 144
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 145
    .line 146
    sub-int/2addr v2, p0

    .line 147
    int-to-float p0, v2

    .line 148
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 149
    .line 150
    .line 151
    move-result v2

    .line 152
    add-int/2addr v2, v0

    .line 153
    int-to-float v2, v2

    .line 154
    invoke-virtual {v1, p0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 155
    .line 156
    .line 157
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 158
    .line 159
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 160
    .line 161
    int-to-float v1, v1

    .line 162
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 163
    .line 164
    .line 165
    move-result p1

    .line 166
    add-int/2addr p1, v0

    .line 167
    int-to-float p1, p1

    .line 168
    invoke-virtual {p0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    .line 170
    .line 171
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 172
    .line 173
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 174
    .line 175
    .line 176
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 177
    .line 178
    return-object p0
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getStripedRightArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 18

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    const/4 v3, 0x2

    .line 12
    div-int/2addr v2, v3

    .line 13
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 14
    .line 15
    .line 16
    move-result v4

    .line 17
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 18
    .line 19
    .line 20
    move-result v5

    .line 21
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    .line 22
    .line 23
    .line 24
    move-result v4

    .line 25
    if-eqz v1, :cond_0

    .line 26
    .line 27
    array-length v5, v1

    .line 28
    if-ne v5, v3, :cond_0

    .line 29
    .line 30
    int-to-float v2, v2

    .line 31
    const/4 v3, 0x0

    .line 32
    aget-object v3, v1, v3

    .line 33
    .line 34
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    mul-float v2, v2, v3

    .line 39
    .line 40
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    int-to-float v3, v4

    .line 45
    const/4 v5, 0x1

    .line 46
    aget-object v1, v1, v5

    .line 47
    .line 48
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    mul-float v3, v3, v1

    .line 53
    .line 54
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 55
    .line 56
    .line 57
    move-result v1

    .line 58
    goto :goto_0

    .line 59
    :cond_0
    int-to-float v1, v2

    .line 60
    const/high16 v2, 0x3f000000    # 0.5f

    .line 61
    .line 62
    mul-float v1, v1, v2

    .line 63
    .line 64
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 65
    .line 66
    .line 67
    move-result v1

    .line 68
    int-to-float v3, v4

    .line 69
    mul-float v3, v3, v2

    .line 70
    .line 71
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 72
    .line 73
    .line 74
    move-result v2

    .line 75
    move/from16 v17, v2

    .line 76
    .line 77
    move v2, v1

    .line 78
    move/from16 v1, v17

    .line 79
    .line 80
    :goto_0
    div-int/lit8 v4, v4, 0x20

    .line 81
    .line 82
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 83
    .line 84
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 85
    .line 86
    int-to-float v6, v3

    .line 87
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 88
    .line 89
    .line 90
    move-result v3

    .line 91
    sub-int/2addr v3, v2

    .line 92
    int-to-float v7, v3

    .line 93
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 94
    .line 95
    add-int/2addr v3, v4

    .line 96
    int-to-float v8, v3

    .line 97
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 98
    .line 99
    .line 100
    move-result v3

    .line 101
    add-int/2addr v3, v2

    .line 102
    int-to-float v9, v3

    .line 103
    sget-object v10, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 104
    .line 105
    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 106
    .line 107
    .line 108
    sget-object v11, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 109
    .line 110
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 111
    .line 112
    mul-int/lit8 v5, v4, 0x2

    .line 113
    .line 114
    add-int/2addr v3, v5

    .line 115
    int-to-float v12, v3

    .line 116
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 117
    .line 118
    .line 119
    move-result v3

    .line 120
    sub-int/2addr v3, v2

    .line 121
    int-to-float v13, v3

    .line 122
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 123
    .line 124
    mul-int/lit8 v5, v4, 0x4

    .line 125
    .line 126
    add-int/2addr v3, v5

    .line 127
    int-to-float v14, v3

    .line 128
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 129
    .line 130
    .line 131
    move-result v3

    .line 132
    add-int/2addr v3, v2

    .line 133
    int-to-float v15, v3

    .line 134
    sget-object v16, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 135
    .line 136
    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 137
    .line 138
    .line 139
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 140
    .line 141
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 142
    .line 143
    mul-int/lit8 v4, v4, 0x5

    .line 144
    .line 145
    add-int/2addr v5, v4

    .line 146
    int-to-float v5, v5

    .line 147
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 148
    .line 149
    .line 150
    move-result v6

    .line 151
    sub-int/2addr v6, v2

    .line 152
    int-to-float v6, v6

    .line 153
    invoke-virtual {v3, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 154
    .line 155
    .line 156
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 157
    .line 158
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 159
    .line 160
    sub-int/2addr v5, v1

    .line 161
    int-to-float v5, v5

    .line 162
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 163
    .line 164
    .line 165
    move-result v6

    .line 166
    sub-int/2addr v6, v2

    .line 167
    int-to-float v6, v6

    .line 168
    invoke-virtual {v3, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    .line 170
    .line 171
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 172
    .line 173
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 174
    .line 175
    sub-int/2addr v5, v1

    .line 176
    int-to-float v5, v5

    .line 177
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 178
    .line 179
    int-to-float v6, v6

    .line 180
    invoke-virtual {v3, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 181
    .line 182
    .line 183
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 184
    .line 185
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 186
    .line 187
    int-to-float v5, v5

    .line 188
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 189
    .line 190
    .line 191
    move-result v6

    .line 192
    int-to-float v6, v6

    .line 193
    invoke-virtual {v3, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 194
    .line 195
    .line 196
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 197
    .line 198
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 199
    .line 200
    sub-int/2addr v5, v1

    .line 201
    int-to-float v5, v5

    .line 202
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    .line 203
    .line 204
    int-to-float v6, v6

    .line 205
    invoke-virtual {v3, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 206
    .line 207
    .line 208
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 209
    .line 210
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 211
    .line 212
    sub-int/2addr v5, v1

    .line 213
    int-to-float v1, v5

    .line 214
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 215
    .line 216
    .line 217
    move-result v5

    .line 218
    add-int/2addr v5, v2

    .line 219
    int-to-float v5, v5

    .line 220
    invoke-virtual {v3, v1, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 221
    .line 222
    .line 223
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 224
    .line 225
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 226
    .line 227
    add-int/2addr v3, v4

    .line 228
    int-to-float v3, v3

    .line 229
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 230
    .line 231
    .line 232
    move-result v0

    .line 233
    add-int/2addr v0, v2

    .line 234
    int-to-float v0, v0

    .line 235
    invoke-virtual {v1, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 236
    .line 237
    .line 238
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 239
    .line 240
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 241
    .line 242
    .line 243
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 244
    .line 245
    return-object v0
.end method

.method private static getUpArrowCalloutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, 0x2

    .line 18
    if-eqz p0, :cond_4

    .line 19
    .line 20
    array-length v2, p0

    .line 21
    const/4 v3, 0x4

    .line 22
    if-ne v2, v3, :cond_4

    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    const/4 v4, 0x0

    .line 26
    :goto_0
    if-ge v4, v3, :cond_1

    .line 27
    .line 28
    aget-object v5, p0, v4

    .line 29
    .line 30
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 31
    .line 32
    .line 33
    move-result v5

    .line 34
    const/high16 v6, 0x3f800000    # 1.0f

    .line 35
    .line 36
    cmpl-float v5, v5, v6

    .line 37
    .line 38
    if-lez v5, :cond_0

    .line 39
    .line 40
    if-eq v4, v1, :cond_0

    .line 41
    .line 42
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 43
    .line 44
    .line 45
    move-result-object v5

    .line 46
    aput-object v5, p0, v4

    .line 47
    .line 48
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    int-to-float v0, v0

    .line 52
    aget-object v2, p0, v2

    .line 53
    .line 54
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    mul-float v2, v2, v0

    .line 59
    .line 60
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    const/4 v3, 0x1

    .line 65
    aget-object v3, p0, v3

    .line 66
    .line 67
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 68
    .line 69
    .line 70
    move-result v3

    .line 71
    mul-float v3, v3, v0

    .line 72
    .line 73
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 74
    .line 75
    .line 76
    move-result v3

    .line 77
    aget-object v4, p0, v1

    .line 78
    .line 79
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 80
    .line 81
    .line 82
    move-result v4

    .line 83
    mul-float v0, v0, v4

    .line 84
    .line 85
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 90
    .line 91
    .line 92
    move-result v4

    .line 93
    int-to-float v4, v4

    .line 94
    const/4 v5, 0x3

    .line 95
    aget-object p0, p0, v5

    .line 96
    .line 97
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 98
    .line 99
    .line 100
    move-result p0

    .line 101
    mul-float v4, v4, p0

    .line 102
    .line 103
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 104
    .line 105
    .line 106
    move-result p0

    .line 107
    mul-int/lit8 v4, v3, 0x2

    .line 108
    .line 109
    if-le v2, v4, :cond_2

    .line 110
    .line 111
    move v2, v4

    .line 112
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 113
    .line 114
    .line 115
    move-result v4

    .line 116
    if-le v0, v4, :cond_3

    .line 117
    .line 118
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    :cond_3
    add-int v4, p0, v0

    .line 123
    .line 124
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 125
    .line 126
    .line 127
    move-result v5

    .line 128
    if-le v4, v5, :cond_5

    .line 129
    .line 130
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 131
    .line 132
    .line 133
    move-result p0

    .line 134
    sub-int/2addr p0, v0

    .line 135
    goto :goto_1

    .line 136
    :cond_4
    int-to-float p0, v0

    .line 137
    const/high16 v0, 0x3e800000    # 0.25f

    .line 138
    .line 139
    mul-float p0, p0, v0

    .line 140
    .line 141
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 142
    .line 143
    .line 144
    move-result v2

    .line 145
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 146
    .line 147
    .line 148
    move-result v3

    .line 149
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 150
    .line 151
    .line 152
    move-result v0

    .line 153
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 154
    .line 155
    .line 156
    move-result p0

    .line 157
    int-to-float p0, p0

    .line 158
    const v4, 0x3f266666    # 0.65f

    .line 159
    .line 160
    .line 161
    mul-float p0, p0, v4

    .line 162
    .line 163
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 164
    .line 165
    .line 166
    move-result p0

    .line 167
    :cond_5
    :goto_1
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 168
    .line 169
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 170
    .line 171
    .line 172
    move-result v5

    .line 173
    int-to-float v5, v5

    .line 174
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 175
    .line 176
    int-to-float v6, v6

    .line 177
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 178
    .line 179
    .line 180
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 181
    .line 182
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 183
    .line 184
    .line 185
    move-result v5

    .line 186
    add-int/2addr v5, v3

    .line 187
    int-to-float v5, v5

    .line 188
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 189
    .line 190
    add-int/2addr v6, v0

    .line 191
    int-to-float v6, v6

    .line 192
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 193
    .line 194
    .line 195
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 196
    .line 197
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 198
    .line 199
    .line 200
    move-result v5

    .line 201
    div-int/2addr v2, v1

    .line 202
    add-int/2addr v5, v2

    .line 203
    int-to-float v1, v5

    .line 204
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 205
    .line 206
    add-int/2addr v5, v0

    .line 207
    int-to-float v5, v5

    .line 208
    invoke-virtual {v4, v1, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 209
    .line 210
    .line 211
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 212
    .line 213
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 214
    .line 215
    .line 216
    move-result v4

    .line 217
    add-int/2addr v4, v2

    .line 218
    int-to-float v4, v4

    .line 219
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 220
    .line 221
    sub-int/2addr v5, p0

    .line 222
    int-to-float v5, v5

    .line 223
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 224
    .line 225
    .line 226
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 227
    .line 228
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 229
    .line 230
    int-to-float v4, v4

    .line 231
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 232
    .line 233
    sub-int/2addr v5, p0

    .line 234
    int-to-float v5, v5

    .line 235
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 236
    .line 237
    .line 238
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 239
    .line 240
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 241
    .line 242
    int-to-float v4, v4

    .line 243
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 244
    .line 245
    int-to-float v5, v5

    .line 246
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 247
    .line 248
    .line 249
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 250
    .line 251
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 252
    .line 253
    int-to-float v4, v4

    .line 254
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 255
    .line 256
    int-to-float v5, v5

    .line 257
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 258
    .line 259
    .line 260
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 261
    .line 262
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 263
    .line 264
    int-to-float v4, v4

    .line 265
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 266
    .line 267
    sub-int/2addr v5, p0

    .line 268
    int-to-float v5, v5

    .line 269
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 270
    .line 271
    .line 272
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 273
    .line 274
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 275
    .line 276
    .line 277
    move-result v4

    .line 278
    sub-int/2addr v4, v2

    .line 279
    int-to-float v4, v4

    .line 280
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 281
    .line 282
    sub-int/2addr v5, p0

    .line 283
    int-to-float p0, v5

    .line 284
    invoke-virtual {v1, v4, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 285
    .line 286
    .line 287
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 288
    .line 289
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 290
    .line 291
    .line 292
    move-result v1

    .line 293
    sub-int/2addr v1, v2

    .line 294
    int-to-float v1, v1

    .line 295
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 296
    .line 297
    add-int/2addr v2, v0

    .line 298
    int-to-float v2, v2

    .line 299
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 300
    .line 301
    .line 302
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 303
    .line 304
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 305
    .line 306
    .line 307
    move-result v1

    .line 308
    sub-int/2addr v1, v3

    .line 309
    int-to-float v1, v1

    .line 310
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 311
    .line 312
    add-int/2addr p1, v0

    .line 313
    int-to-float p1, p1

    .line 314
    invoke-virtual {p0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 315
    .line 316
    .line 317
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 318
    .line 319
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 320
    .line 321
    .line 322
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 323
    .line 324
    return-object p0
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getUpArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x2

    .line 10
    div-int/2addr v0, v1

    .line 11
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz p0, :cond_0

    .line 24
    .line 25
    array-length v3, p0

    .line 26
    if-ne v3, v1, :cond_0

    .line 27
    .line 28
    int-to-float v0, v0

    .line 29
    const/4 v1, 0x0

    .line 30
    aget-object v1, p0, v1

    .line 31
    .line 32
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    mul-float v0, v0, v1

    .line 37
    .line 38
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    int-to-float v1, v2

    .line 43
    const/4 v2, 0x1

    .line 44
    aget-object p0, p0, v2

    .line 45
    .line 46
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 47
    .line 48
    .line 49
    move-result p0

    .line 50
    mul-float v1, v1, p0

    .line 51
    .line 52
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 53
    .line 54
    .line 55
    move-result p0

    .line 56
    goto :goto_0

    .line 57
    :cond_0
    int-to-float p0, v0

    .line 58
    const/high16 v0, 0x3f000000    # 0.5f

    .line 59
    .line 60
    mul-float p0, p0, v0

    .line 61
    .line 62
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 63
    .line 64
    .line 65
    move-result p0

    .line 66
    int-to-float v1, v2

    .line 67
    mul-float v1, v1, v0

    .line 68
    .line 69
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    move v4, v0

    .line 74
    move v0, p0

    .line 75
    move p0, v4

    .line 76
    :goto_0
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 77
    .line 78
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 79
    .line 80
    .line 81
    move-result v2

    .line 82
    int-to-float v2, v2

    .line 83
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 84
    .line 85
    int-to-float v3, v3

    .line 86
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 87
    .line 88
    .line 89
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 90
    .line 91
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 92
    .line 93
    int-to-float v2, v2

    .line 94
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 95
    .line 96
    add-int/2addr v3, p0

    .line 97
    int-to-float v3, v3

    .line 98
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 99
    .line 100
    .line 101
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 102
    .line 103
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 104
    .line 105
    .line 106
    move-result v2

    .line 107
    add-int/2addr v2, v0

    .line 108
    int-to-float v2, v2

    .line 109
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 110
    .line 111
    add-int/2addr v3, p0

    .line 112
    int-to-float v3, v3

    .line 113
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 114
    .line 115
    .line 116
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 117
    .line 118
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 119
    .line 120
    .line 121
    move-result v2

    .line 122
    add-int/2addr v2, v0

    .line 123
    int-to-float v2, v2

    .line 124
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 125
    .line 126
    int-to-float v3, v3

    .line 127
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 128
    .line 129
    .line 130
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 131
    .line 132
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 133
    .line 134
    .line 135
    move-result v2

    .line 136
    sub-int/2addr v2, v0

    .line 137
    int-to-float v2, v2

    .line 138
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 139
    .line 140
    int-to-float v3, v3

    .line 141
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 142
    .line 143
    .line 144
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 145
    .line 146
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 147
    .line 148
    .line 149
    move-result v2

    .line 150
    sub-int/2addr v2, v0

    .line 151
    int-to-float v0, v2

    .line 152
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 153
    .line 154
    add-int/2addr v2, p0

    .line 155
    int-to-float v2, v2

    .line 156
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 157
    .line 158
    .line 159
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 160
    .line 161
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 162
    .line 163
    int-to-float v1, v1

    .line 164
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 165
    .line 166
    add-int/2addr p1, p0

    .line 167
    int-to-float p0, p1

    .line 168
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    .line 170
    .line 171
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 172
    .line 173
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 174
    .line 175
    .line 176
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 177
    .line 178
    return-object p0
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getUpDownArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x2

    .line 10
    div-int/2addr v0, v1

    .line 11
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz p0, :cond_0

    .line 24
    .line 25
    array-length v3, p0

    .line 26
    if-ne v3, v1, :cond_0

    .line 27
    .line 28
    int-to-float v0, v0

    .line 29
    const/4 v3, 0x0

    .line 30
    aget-object v3, p0, v3

    .line 31
    .line 32
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    mul-float v0, v0, v3

    .line 37
    .line 38
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    int-to-float v3, v2

    .line 43
    const/4 v4, 0x1

    .line 44
    aget-object p0, p0, v4

    .line 45
    .line 46
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 47
    .line 48
    .line 49
    move-result p0

    .line 50
    mul-float v3, v3, p0

    .line 51
    .line 52
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 53
    .line 54
    .line 55
    move-result p0

    .line 56
    mul-int/lit8 v3, p0, 0x2

    .line 57
    .line 58
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 59
    .line 60
    .line 61
    move-result v4

    .line 62
    if-le v3, v4, :cond_1

    .line 63
    .line 64
    mul-int/lit8 p0, v2, 0x2

    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_0
    int-to-float p0, v0

    .line 68
    const/high16 v0, 0x3f000000    # 0.5f

    .line 69
    .line 70
    mul-float p0, p0, v0

    .line 71
    .line 72
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 73
    .line 74
    .line 75
    move-result p0

    .line 76
    int-to-float v1, v2

    .line 77
    mul-float v1, v1, v0

    .line 78
    .line 79
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    move v5, v0

    .line 84
    move v0, p0

    .line 85
    move p0, v5

    .line 86
    :cond_1
    :goto_0
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 87
    .line 88
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 89
    .line 90
    .line 91
    move-result v2

    .line 92
    int-to-float v2, v2

    .line 93
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 94
    .line 95
    int-to-float v3, v3

    .line 96
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 97
    .line 98
    .line 99
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 100
    .line 101
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 102
    .line 103
    int-to-float v2, v2

    .line 104
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 105
    .line 106
    add-int/2addr v3, p0

    .line 107
    int-to-float v3, v3

    .line 108
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 109
    .line 110
    .line 111
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 112
    .line 113
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 114
    .line 115
    .line 116
    move-result v2

    .line 117
    add-int/2addr v2, v0

    .line 118
    int-to-float v2, v2

    .line 119
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 120
    .line 121
    add-int/2addr v3, p0

    .line 122
    int-to-float v3, v3

    .line 123
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 124
    .line 125
    .line 126
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 127
    .line 128
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 129
    .line 130
    .line 131
    move-result v2

    .line 132
    add-int/2addr v2, v0

    .line 133
    int-to-float v2, v2

    .line 134
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 135
    .line 136
    sub-int/2addr v3, p0

    .line 137
    int-to-float v3, v3

    .line 138
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 139
    .line 140
    .line 141
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 142
    .line 143
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 144
    .line 145
    int-to-float v2, v2

    .line 146
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 147
    .line 148
    sub-int/2addr v3, p0

    .line 149
    int-to-float v3, v3

    .line 150
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 151
    .line 152
    .line 153
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 154
    .line 155
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 156
    .line 157
    .line 158
    move-result v2

    .line 159
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 160
    .line 161
    int-to-float v3, v3

    .line 162
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 163
    .line 164
    .line 165
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 166
    .line 167
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 168
    .line 169
    int-to-float v2, v2

    .line 170
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 171
    .line 172
    sub-int/2addr v3, p0

    .line 173
    int-to-float v3, v3

    .line 174
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 175
    .line 176
    .line 177
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 178
    .line 179
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 180
    .line 181
    .line 182
    move-result v2

    .line 183
    sub-int/2addr v2, v0

    .line 184
    int-to-float v2, v2

    .line 185
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 186
    .line 187
    sub-int/2addr v3, p0

    .line 188
    int-to-float v3, v3

    .line 189
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 190
    .line 191
    .line 192
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 193
    .line 194
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 195
    .line 196
    .line 197
    move-result v2

    .line 198
    sub-int/2addr v2, v0

    .line 199
    int-to-float v0, v2

    .line 200
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 201
    .line 202
    add-int/2addr v2, p0

    .line 203
    int-to-float v2, v2

    .line 204
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 205
    .line 206
    .line 207
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 208
    .line 209
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 210
    .line 211
    int-to-float v1, v1

    .line 212
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 213
    .line 214
    add-int/2addr p1, p0

    .line 215
    int-to-float p0, p1

    .line 216
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 217
    .line 218
    .line 219
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 220
    .line 221
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 222
    .line 223
    .line 224
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 225
    .line 226
    return-object p0
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getUturnArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 13

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, 0x2

    .line 18
    if-eqz p0, :cond_3

    .line 19
    .line 20
    array-length v2, p0

    .line 21
    const/4 v3, 0x5

    .line 22
    if-ne v2, v3, :cond_3

    .line 23
    .line 24
    int-to-float v0, v0

    .line 25
    const/4 v2, 0x0

    .line 26
    aget-object v2, p0, v2

    .line 27
    .line 28
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    mul-float v2, v2, v0

    .line 33
    .line 34
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    const/4 v3, 0x1

    .line 39
    aget-object v3, p0, v3

    .line 40
    .line 41
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    mul-float v3, v3, v0

    .line 46
    .line 47
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 48
    .line 49
    .line 50
    move-result v3

    .line 51
    aget-object v4, p0, v1

    .line 52
    .line 53
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 54
    .line 55
    .line 56
    move-result v4

    .line 57
    mul-float v4, v4, v0

    .line 58
    .line 59
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 60
    .line 61
    .line 62
    move-result v4

    .line 63
    const/4 v5, 0x3

    .line 64
    aget-object v5, p0, v5

    .line 65
    .line 66
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 67
    .line 68
    .line 69
    move-result v5

    .line 70
    mul-float v0, v0, v5

    .line 71
    .line 72
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 77
    .line 78
    .line 79
    move-result v5

    .line 80
    int-to-float v5, v5

    .line 81
    const/4 v6, 0x4

    .line 82
    aget-object p0, p0, v6

    .line 83
    .line 84
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 85
    .line 86
    .line 87
    move-result p0

    .line 88
    mul-float v5, v5, p0

    .line 89
    .line 90
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 91
    .line 92
    .line 93
    move-result p0

    .line 94
    mul-int/lit8 v5, v3, 0x2

    .line 95
    .line 96
    if-le v2, v5, :cond_0

    .line 97
    .line 98
    move v2, v5

    .line 99
    :cond_0
    add-int v5, v0, v4

    .line 100
    .line 101
    if-lt v5, p0, :cond_1

    .line 102
    .line 103
    move p0, v5

    .line 104
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 105
    .line 106
    .line 107
    move-result v5

    .line 108
    if-le p0, v5, :cond_2

    .line 109
    .line 110
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 111
    .line 112
    .line 113
    move-result p0

    .line 114
    sub-int v4, p0, v0

    .line 115
    .line 116
    :cond_2
    sub-int v5, p0, v4

    .line 117
    .line 118
    if-ge v5, v2, :cond_4

    .line 119
    .line 120
    sub-int v4, p0, v2

    .line 121
    .line 122
    goto :goto_0

    .line 123
    :cond_3
    int-to-float p0, v0

    .line 124
    const/high16 v0, 0x3e800000    # 0.25f

    .line 125
    .line 126
    mul-float v0, v0, p0

    .line 127
    .line 128
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 129
    .line 130
    .line 131
    move-result v2

    .line 132
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 133
    .line 134
    .line 135
    move-result v3

    .line 136
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 137
    .line 138
    .line 139
    move-result v4

    .line 140
    const/high16 v0, 0x3ee00000    # 0.4375f

    .line 141
    .line 142
    mul-float p0, p0, v0

    .line 143
    .line 144
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 145
    .line 146
    .line 147
    move-result v0

    .line 148
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 149
    .line 150
    .line 151
    move-result p0

    .line 152
    int-to-float p0, p0

    .line 153
    const/high16 v5, 0x3f400000    # 0.75f

    .line 154
    .line 155
    mul-float p0, p0, v5

    .line 156
    .line 157
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    .line 158
    .line 159
    .line 160
    move-result p0

    .line 161
    :cond_4
    :goto_0
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 162
    .line 163
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 164
    .line 165
    int-to-float v6, v6

    .line 166
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 167
    .line 168
    int-to-float v7, v7

    .line 169
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 170
    .line 171
    .line 172
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 173
    .line 174
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 175
    .line 176
    int-to-float v6, v6

    .line 177
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 178
    .line 179
    add-int/2addr v7, v0

    .line 180
    int-to-float v7, v7

    .line 181
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 182
    .line 183
    .line 184
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 185
    .line 186
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 187
    .line 188
    int-to-float v7, v6

    .line 189
    iget v8, p1, Landroid/graphics/Rect;->top:I

    .line 190
    .line 191
    int-to-float v9, v8

    .line 192
    mul-int/lit8 v10, v0, 0x2

    .line 193
    .line 194
    add-int/2addr v6, v10

    .line 195
    int-to-float v6, v6

    .line 196
    add-int/2addr v8, v10

    .line 197
    int-to-float v8, v8

    .line 198
    invoke-virtual {v5, v7, v9, v6, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 199
    .line 200
    .line 201
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 202
    .line 203
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 204
    .line 205
    const/high16 v7, 0x43340000    # 180.0f

    .line 206
    .line 207
    const/high16 v8, 0x42b40000    # 90.0f

    .line 208
    .line 209
    invoke-virtual {v5, v6, v7, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 210
    .line 211
    .line 212
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 213
    .line 214
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 215
    .line 216
    sub-int/2addr v6, v3

    .line 217
    div-int/lit8 v7, v2, 0x2

    .line 218
    .line 219
    add-int/2addr v6, v7

    .line 220
    sub-int/2addr v6, v0

    .line 221
    int-to-float v6, v6

    .line 222
    iget v9, p1, Landroid/graphics/Rect;->top:I

    .line 223
    .line 224
    int-to-float v9, v9

    .line 225
    invoke-virtual {v5, v6, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 226
    .line 227
    .line 228
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 229
    .line 230
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 231
    .line 232
    sub-int v9, v6, v3

    .line 233
    .line 234
    add-int/2addr v9, v7

    .line 235
    sub-int/2addr v9, v10

    .line 236
    int-to-float v9, v9

    .line 237
    iget v11, p1, Landroid/graphics/Rect;->top:I

    .line 238
    .line 239
    int-to-float v12, v11

    .line 240
    sub-int/2addr v6, v3

    .line 241
    add-int/2addr v6, v7

    .line 242
    int-to-float v6, v6

    .line 243
    add-int/2addr v11, v10

    .line 244
    int-to-float v10, v11

    .line 245
    invoke-virtual {v5, v9, v12, v6, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 246
    .line 247
    .line 248
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 249
    .line 250
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 251
    .line 252
    const/high16 v9, 0x43870000    # 270.0f

    .line 253
    .line 254
    invoke-virtual {v5, v6, v9, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 255
    .line 256
    .line 257
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 258
    .line 259
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 260
    .line 261
    sub-int/2addr v6, v3

    .line 262
    add-int/2addr v6, v7

    .line 263
    int-to-float v6, v6

    .line 264
    iget v8, p1, Landroid/graphics/Rect;->top:I

    .line 265
    .line 266
    add-int/2addr v8, p0

    .line 267
    sub-int/2addr v8, v4

    .line 268
    int-to-float v8, v8

    .line 269
    invoke-virtual {v5, v6, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 270
    .line 271
    .line 272
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 273
    .line 274
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 275
    .line 276
    int-to-float v6, v6

    .line 277
    iget v8, p1, Landroid/graphics/Rect;->top:I

    .line 278
    .line 279
    add-int/2addr v8, p0

    .line 280
    sub-int/2addr v8, v4

    .line 281
    int-to-float v8, v8

    .line 282
    invoke-virtual {v5, v6, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 283
    .line 284
    .line 285
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 286
    .line 287
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 288
    .line 289
    sub-int/2addr v6, v3

    .line 290
    int-to-float v6, v6

    .line 291
    iget v8, p1, Landroid/graphics/Rect;->top:I

    .line 292
    .line 293
    add-int/2addr v8, p0

    .line 294
    int-to-float v8, v8

    .line 295
    invoke-virtual {v5, v6, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 296
    .line 297
    .line 298
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 299
    .line 300
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 301
    .line 302
    mul-int/lit8 v8, v3, 0x2

    .line 303
    .line 304
    sub-int/2addr v6, v8

    .line 305
    int-to-float v6, v6

    .line 306
    iget v8, p1, Landroid/graphics/Rect;->top:I

    .line 307
    .line 308
    add-int/2addr v8, p0

    .line 309
    sub-int/2addr v8, v4

    .line 310
    int-to-float v8, v8

    .line 311
    invoke-virtual {v5, v6, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 312
    .line 313
    .line 314
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 315
    .line 316
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 317
    .line 318
    sub-int/2addr v6, v3

    .line 319
    sub-int/2addr v6, v7

    .line 320
    int-to-float v6, v6

    .line 321
    iget v8, p1, Landroid/graphics/Rect;->top:I

    .line 322
    .line 323
    add-int/2addr v8, p0

    .line 324
    sub-int/2addr v8, v4

    .line 325
    int-to-float p0, v8

    .line 326
    invoke-virtual {v5, v6, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 327
    .line 328
    .line 329
    if-lt v0, v2, :cond_5

    .line 330
    .line 331
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 332
    .line 333
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 334
    .line 335
    sub-int/2addr v4, v3

    .line 336
    sub-int/2addr v4, v7

    .line 337
    int-to-float v4, v4

    .line 338
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 339
    .line 340
    add-int/2addr v5, v0

    .line 341
    int-to-float v5, v5

    .line 342
    invoke-virtual {p0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 343
    .line 344
    .line 345
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 346
    .line 347
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 348
    .line 349
    sub-int v5, v4, v3

    .line 350
    .line 351
    sub-int/2addr v5, v7

    .line 352
    sub-int v6, v0, v2

    .line 353
    .line 354
    mul-int/lit8 v6, v6, 0x2

    .line 355
    .line 356
    sub-int/2addr v5, v6

    .line 357
    int-to-float v1, v5

    .line 358
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 359
    .line 360
    add-int v8, v5, v2

    .line 361
    .line 362
    int-to-float v8, v8

    .line 363
    sub-int/2addr v4, v3

    .line 364
    sub-int/2addr v4, v7

    .line 365
    int-to-float v4, v4

    .line 366
    add-int/2addr v5, v2

    .line 367
    add-int/2addr v5, v6

    .line 368
    int-to-float v5, v5

    .line 369
    invoke-virtual {p0, v1, v8, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 370
    .line 371
    .line 372
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 373
    .line 374
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 375
    .line 376
    const/4 v4, 0x0

    .line 377
    const/high16 v5, -0x3d4c0000    # -90.0f

    .line 378
    .line 379
    invoke-virtual {p0, v1, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 380
    .line 381
    .line 382
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 383
    .line 384
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 385
    .line 386
    sub-int/2addr v1, v3

    .line 387
    add-int/2addr v1, v7

    .line 388
    sub-int/2addr v1, v0

    .line 389
    int-to-float v1, v1

    .line 390
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 391
    .line 392
    add-int/2addr v3, v2

    .line 393
    int-to-float v3, v3

    .line 394
    invoke-virtual {p0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 395
    .line 396
    .line 397
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 398
    .line 399
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 400
    .line 401
    add-int/2addr v1, v0

    .line 402
    int-to-float v1, v1

    .line 403
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 404
    .line 405
    add-int/2addr v3, v2

    .line 406
    int-to-float v3, v3

    .line 407
    invoke-virtual {p0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 408
    .line 409
    .line 410
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 411
    .line 412
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 413
    .line 414
    add-int v3, v1, v2

    .line 415
    .line 416
    int-to-float v3, v3

    .line 417
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 418
    .line 419
    add-int v7, v4, v2

    .line 420
    .line 421
    int-to-float v7, v7

    .line 422
    add-int/2addr v1, v2

    .line 423
    add-int/2addr v1, v6

    .line 424
    int-to-float v1, v1

    .line 425
    add-int/2addr v4, v2

    .line 426
    add-int/2addr v4, v6

    .line 427
    int-to-float v4, v4

    .line 428
    invoke-virtual {p0, v3, v7, v1, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 429
    .line 430
    .line 431
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 432
    .line 433
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 434
    .line 435
    invoke-virtual {p0, v1, v9, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 436
    .line 437
    .line 438
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 439
    .line 440
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 441
    .line 442
    add-int/2addr v1, v2

    .line 443
    int-to-float v1, v1

    .line 444
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 445
    .line 446
    add-int/2addr v3, v0

    .line 447
    int-to-float v0, v3

    .line 448
    invoke-virtual {p0, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 449
    .line 450
    .line 451
    goto :goto_1

    .line 452
    :cond_5
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 453
    .line 454
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 455
    .line 456
    sub-int/2addr v0, v3

    .line 457
    sub-int/2addr v0, v7

    .line 458
    int-to-float v0, v0

    .line 459
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 460
    .line 461
    add-int/2addr v1, v2

    .line 462
    int-to-float v1, v1

    .line 463
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 464
    .line 465
    .line 466
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 467
    .line 468
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 469
    .line 470
    add-int/2addr v0, v2

    .line 471
    int-to-float v0, v0

    .line 472
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 473
    .line 474
    add-int/2addr v1, v2

    .line 475
    int-to-float v1, v1

    .line 476
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 477
    .line 478
    .line 479
    :goto_1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 480
    .line 481
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 482
    .line 483
    add-int/2addr v0, v2

    .line 484
    int-to-float v0, v0

    .line 485
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 486
    .line 487
    int-to-float p1, p1

    .line 488
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 489
    .line 490
    .line 491
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 492
    .line 493
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 494
    .line 495
    .line 496
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/arrow/LaterArrowPathBuilder;->path:Landroid/graphics/Path;

    .line 497
    .line 498
    return-object p0
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

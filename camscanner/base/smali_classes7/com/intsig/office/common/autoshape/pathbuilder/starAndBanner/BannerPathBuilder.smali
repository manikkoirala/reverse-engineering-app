.class public Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;
.super Ljava/lang/Object;
.source "BannerPathBuilder.java"


# static fields
.field private static final PICTURECOLOR:I = -0x70aaaaab

.field private static final TINT:F = -0.3f

.field private static pathExList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation
.end field

.field private static sm:Landroid/graphics/Matrix;

.field private static tempRect:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Matrix;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 7
    .line 8
    new-instance v0, Landroid/graphics/RectF;

    .line 9
    .line 10
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 14
    .line 15
    new-instance v0, Ljava/util/ArrayList;

    .line 16
    .line 17
    const/4 v1, 0x2

    .line 18
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 19
    .line 20
    .line 21
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static BezierComputePoint(FFFFFFFFF)Landroid/graphics/PointF;
    .locals 4

    .line 1
    new-instance v0, Landroid/graphics/PointF;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 4
    .line 5
    .line 6
    const/high16 v1, 0x3f800000    # 1.0f

    .line 7
    .line 8
    sub-float/2addr v1, p8

    .line 9
    mul-float v2, v1, v1

    .line 10
    .line 11
    mul-float v2, v2, v1

    .line 12
    .line 13
    mul-float p0, p0, v2

    .line 14
    .line 15
    mul-float v2, v2, p1

    .line 16
    .line 17
    const/high16 p1, 0x40400000    # 3.0f

    .line 18
    .line 19
    mul-float p1, p1, p8

    .line 20
    .line 21
    mul-float v3, p1, v1

    .line 22
    .line 23
    mul-float v3, v3, v1

    .line 24
    .line 25
    mul-float p2, p2, v3

    .line 26
    .line 27
    add-float/2addr p0, p2

    .line 28
    mul-float v3, v3, p3

    .line 29
    .line 30
    add-float/2addr v2, v3

    .line 31
    mul-float p1, p1, p8

    .line 32
    .line 33
    mul-float p1, p1, v1

    .line 34
    .line 35
    mul-float p4, p4, p1

    .line 36
    .line 37
    add-float/2addr p0, p4

    .line 38
    mul-float p1, p1, p5

    .line 39
    .line 40
    add-float/2addr v2, p1

    .line 41
    mul-float p1, p8, p8

    .line 42
    .line 43
    mul-float p1, p1, p8

    .line 44
    .line 45
    mul-float p6, p6, p1

    .line 46
    .line 47
    add-float/2addr p0, p6

    .line 48
    iput p0, v0, Landroid/graphics/PointF;->x:F

    .line 49
    .line 50
    mul-float p1, p1, p7

    .line 51
    .line 52
    add-float/2addr v2, p1

    .line 53
    iput v2, v0, Landroid/graphics/PointF;->y:F

    .line 54
    .line 55
    return-object v0
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
.end method

.method private static computeBezierCtrPoint(FFFFFFF)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FFFFFFF)",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    move/from16 v0, p6

    const/4 v1, 0x0

    const v2, 0x3727c5ac    # 1.0E-5f

    cmpg-float v3, v0, v2

    if-gez v3, :cond_0

    float-to-double v3, v0

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v3, v5

    const-wide v5, 0x3ee4f8b580000000L    # 9.999999747378752E-6

    cmpl-double v7, v3, v5

    if-lez v7, :cond_0

    return-object v1

    .line 10
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 11
    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4}, Landroid/graphics/PointF;-><init>()V

    .line 12
    new-instance v5, Landroid/graphics/PointF;

    invoke-direct {v5}, Landroid/graphics/PointF;-><init>()V

    const/4 v6, 0x0

    .line 13
    invoke-interface {v3, v6, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    const/4 v6, 0x1

    .line 14
    invoke-interface {v3, v6, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    const/high16 v6, 0x3f800000    # 1.0f

    sub-float/2addr v6, v0

    const/high16 v7, 0x40400000    # 3.0f

    mul-float v8, v0, v7

    mul-float v9, v8, v6

    mul-float v9, v9, v6

    mul-float v8, v8, v0

    mul-float v8, v8, v6

    mul-float v10, v6, v6

    mul-float v10, v10, v6

    mul-float v11, v0, v0

    mul-float v11, v11, v0

    div-float/2addr v0, v6

    mul-float v7, v7, v11

    add-float/2addr v7, v9

    mul-float v6, v10, p0

    sub-float v6, p4, v6

    mul-float v12, v9, p0

    sub-float/2addr v6, v12

    mul-float v12, v8, p2

    sub-float/2addr v6, v12

    mul-float v12, v11, p2

    sub-float/2addr v6, v12

    cmpg-float v2, v7, v2

    if-gez v2, :cond_1

    return-object v1

    :cond_1
    div-float v12, v6, v7

    add-float/2addr v12, p0

    .line 15
    iput v12, v4, Landroid/graphics/PointF;->x:F

    mul-float v6, v6, v0

    div-float/2addr v6, v7

    add-float/2addr v6, p2

    .line 16
    iput v6, v5, Landroid/graphics/PointF;->x:F

    mul-float v10, v10, p1

    sub-float v6, p5, v10

    mul-float v9, v9, p1

    sub-float/2addr v6, v9

    mul-float v8, v8, p3

    sub-float/2addr v6, v8

    mul-float v11, v11, p3

    sub-float/2addr v6, v11

    if-gez v2, :cond_2

    return-object v1

    :cond_2
    div-float v1, v6, v7

    add-float/2addr v1, p1

    .line 17
    iput v1, v4, Landroid/graphics/PointF;->y:F

    mul-float v0, v0, v6

    div-float/2addr v0, v7

    add-float v0, v0, p3

    .line 18
    iput v0, v5, Landroid/graphics/PointF;->y:F

    return-object v3
.end method

.method private static computeBezierCtrPoint(FFFFFFFF)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FFFFFFFF)",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    .line 3
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    const/4 v3, 0x0

    .line 4
    invoke-interface {v0, v3, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    const/4 v3, 0x1

    .line 5
    invoke-interface {v0, v3, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    const/high16 v3, -0x3f600000    # -5.0f

    mul-float v4, p0, v3

    const/high16 v5, 0x41900000    # 18.0f

    mul-float v6, p2, v5

    add-float/2addr v4, v6

    const/high16 v6, 0x41100000    # 9.0f

    mul-float v7, p4, v6

    sub-float/2addr v4, v7

    const/high16 v7, 0x40000000    # 2.0f

    mul-float v8, p6, v7

    add-float/2addr v4, v8

    const/high16 v8, 0x40c00000    # 6.0f

    div-float/2addr v4, v8

    .line 6
    iput v4, v1, Landroid/graphics/PointF;->x:F

    mul-float v3, v3, p1

    mul-float v4, p3, v5

    add-float/2addr v3, v4

    mul-float v4, p5, v6

    sub-float/2addr v3, v4

    mul-float v4, p7, v7

    add-float/2addr v3, v4

    div-float/2addr v3, v8

    .line 7
    iput v3, v1, Landroid/graphics/PointF;->y:F

    mul-float v1, p0, v7

    mul-float v3, p2, v6

    sub-float/2addr v1, v3

    mul-float v3, p4, v5

    add-float/2addr v1, v3

    const/high16 v3, 0x40a00000    # 5.0f

    mul-float v4, p6, v3

    sub-float/2addr v1, v4

    div-float/2addr v1, v8

    .line 8
    iput v1, v2, Landroid/graphics/PointF;->x:F

    mul-float v1, p1, v7

    mul-float v4, p3, v6

    sub-float/2addr v1, v4

    mul-float v4, p5, v5

    add-float/2addr v1, v4

    mul-float v3, v3, p7

    sub-float/2addr v1, v3

    div-float/2addr v1, v8

    .line 9
    iput v1, v2, Landroid/graphics/PointF;->y:F

    return-object v0
.end method

.method private static getDoubleWavePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 16
    .line 17
    .line 18
    move-result v4

    .line 19
    const/4 v5, 0x1

    .line 20
    const/high16 v6, 0x3e000000    # 0.125f

    .line 21
    .line 22
    const/4 v7, 0x0

    .line 23
    const/4 v8, 0x2

    .line 24
    if-eqz v4, :cond_1

    .line 25
    .line 26
    if-eqz v1, :cond_0

    .line 27
    .line 28
    array-length v4, v1

    .line 29
    if-ne v4, v8, :cond_0

    .line 30
    .line 31
    int-to-float v3, v3

    .line 32
    aget-object v4, v1, v7

    .line 33
    .line 34
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 35
    .line 36
    .line 37
    move-result v4

    .line 38
    mul-float v3, v3, v4

    .line 39
    .line 40
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    int-to-float v4, v2

    .line 45
    aget-object v1, v1, v5

    .line 46
    .line 47
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    mul-float v4, v4, v1

    .line 52
    .line 53
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 54
    .line 55
    .line 56
    move-result v7

    .line 57
    goto :goto_1

    .line 58
    :cond_0
    int-to-float v1, v3

    .line 59
    mul-float v1, v1, v6

    .line 60
    .line 61
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 62
    .line 63
    .line 64
    move-result v3

    .line 65
    goto :goto_1

    .line 66
    :cond_1
    if-eqz v1, :cond_3

    .line 67
    .line 68
    array-length v4, v1

    .line 69
    if-lt v4, v5, :cond_3

    .line 70
    .line 71
    aget-object v4, v1, v7

    .line 72
    .line 73
    if-eqz v4, :cond_2

    .line 74
    .line 75
    int-to-float v3, v3

    .line 76
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 77
    .line 78
    .line 79
    move-result v4

    .line 80
    mul-float v3, v3, v4

    .line 81
    .line 82
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 83
    .line 84
    .line 85
    move-result v3

    .line 86
    goto :goto_0

    .line 87
    :cond_2
    int-to-float v3, v3

    .line 88
    mul-float v3, v3, v6

    .line 89
    .line 90
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 91
    .line 92
    .line 93
    move-result v3

    .line 94
    :goto_0
    array-length v4, v1

    .line 95
    if-lt v4, v8, :cond_4

    .line 96
    .line 97
    aget-object v1, v1, v5

    .line 98
    .line 99
    if-eqz v1, :cond_4

    .line 100
    .line 101
    int-to-float v4, v2

    .line 102
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 103
    .line 104
    .line 105
    move-result v1

    .line 106
    const/high16 v5, 0x3f000000    # 0.5f

    .line 107
    .line 108
    sub-float/2addr v1, v5

    .line 109
    mul-float v4, v4, v1

    .line 110
    .line 111
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 112
    .line 113
    .line 114
    move-result v7

    .line 115
    goto :goto_1

    .line 116
    :cond_3
    int-to-float v1, v3

    .line 117
    mul-float v1, v1, v6

    .line 118
    .line 119
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 120
    .line 121
    .line 122
    move-result v3

    .line 123
    :cond_4
    :goto_1
    mul-int/lit8 v1, v7, 0x2

    .line 124
    .line 125
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    .line 126
    .line 127
    .line 128
    move-result v1

    .line 129
    sub-int/2addr v2, v1

    .line 130
    div-int/2addr v2, v8

    .line 131
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 132
    .line 133
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 134
    .line 135
    .line 136
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 137
    .line 138
    .line 139
    move-result v4

    .line 140
    if-eqz v4, :cond_5

    .line 141
    .line 142
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 143
    .line 144
    .line 145
    move-result-object v4

    .line 146
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 147
    .line 148
    .line 149
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 150
    .line 151
    .line 152
    move-result-object v4

    .line 153
    invoke-virtual {v4}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 154
    .line 155
    .line 156
    move-result-object v4

    .line 157
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 158
    .line 159
    .line 160
    :cond_5
    new-instance v4, Landroid/graphics/Path;

    .line 161
    .line 162
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 163
    .line 164
    .line 165
    const v5, 0x3eaaa64c    # 0.3333f

    .line 166
    .line 167
    .line 168
    const v6, 0x3f2aacda    # 0.6667f

    .line 169
    .line 170
    .line 171
    const v15, 0x3faaa993    # 1.3333f

    .line 172
    .line 173
    .line 174
    const v8, 0x40554fdf    # 3.333f

    .line 175
    .line 176
    .line 177
    const v16, 0x3fd5566d    # 1.6667f

    .line 178
    .line 179
    .line 180
    if-lez v7, :cond_6

    .line 181
    .line 182
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 183
    .line 184
    int-to-float v7, v7

    .line 185
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 186
    .line 187
    add-int/2addr v9, v3

    .line 188
    int-to-float v9, v9

    .line 189
    invoke-virtual {v4, v7, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 190
    .line 191
    .line 192
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 193
    .line 194
    int-to-float v9, v7

    .line 195
    int-to-float v14, v2

    .line 196
    mul-float v5, v5, v14

    .line 197
    .line 198
    add-float/2addr v9, v5

    .line 199
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 200
    .line 201
    add-int v11, v10, v3

    .line 202
    .line 203
    int-to-float v11, v11

    .line 204
    int-to-float v12, v3

    .line 205
    mul-float v17, v12, v8

    .line 206
    .line 207
    sub-float v11, v11, v17

    .line 208
    .line 209
    int-to-float v8, v7

    .line 210
    mul-float v6, v6, v14

    .line 211
    .line 212
    add-float v12, v8, v6

    .line 213
    .line 214
    add-int v8, v10, v3

    .line 215
    .line 216
    int-to-float v8, v8

    .line 217
    add-float v13, v8, v17

    .line 218
    .line 219
    add-int/2addr v7, v2

    .line 220
    int-to-float v7, v7

    .line 221
    add-int/2addr v10, v3

    .line 222
    int-to-float v10, v10

    .line 223
    move-object v8, v4

    .line 224
    move/from16 v18, v10

    .line 225
    .line 226
    move v10, v11

    .line 227
    move v11, v12

    .line 228
    move v12, v13

    .line 229
    move v13, v7

    .line 230
    move v7, v14

    .line 231
    move/from16 v14, v18

    .line 232
    .line 233
    invoke-virtual/range {v8 .. v14}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 234
    .line 235
    .line 236
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 237
    .line 238
    int-to-float v9, v8

    .line 239
    mul-float v15, v15, v7

    .line 240
    .line 241
    add-float/2addr v9, v15

    .line 242
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 243
    .line 244
    add-int v11, v10, v3

    .line 245
    .line 246
    int-to-float v11, v11

    .line 247
    sub-float v11, v11, v17

    .line 248
    .line 249
    int-to-float v12, v8

    .line 250
    mul-float v7, v7, v16

    .line 251
    .line 252
    add-float/2addr v12, v7

    .line 253
    add-int v13, v10, v3

    .line 254
    .line 255
    int-to-float v13, v13

    .line 256
    add-float v13, v13, v17

    .line 257
    .line 258
    mul-int/lit8 v16, v2, 0x2

    .line 259
    .line 260
    add-int v8, v8, v16

    .line 261
    .line 262
    int-to-float v14, v8

    .line 263
    add-int/2addr v10, v3

    .line 264
    int-to-float v10, v10

    .line 265
    move-object v8, v4

    .line 266
    move/from16 v18, v10

    .line 267
    .line 268
    move v10, v11

    .line 269
    move v11, v12

    .line 270
    move v12, v13

    .line 271
    move v13, v14

    .line 272
    move/from16 v14, v18

    .line 273
    .line 274
    invoke-virtual/range {v8 .. v14}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 275
    .line 276
    .line 277
    iget v8, v0, Landroid/graphics/Rect;->right:I

    .line 278
    .line 279
    int-to-float v8, v8

    .line 280
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 281
    .line 282
    sub-int/2addr v9, v3

    .line 283
    int-to-float v9, v9

    .line 284
    invoke-virtual {v4, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 285
    .line 286
    .line 287
    iget v8, v0, Landroid/graphics/Rect;->right:I

    .line 288
    .line 289
    int-to-float v9, v8

    .line 290
    sub-float/2addr v9, v5

    .line 291
    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 292
    .line 293
    sub-int v10, v5, v3

    .line 294
    .line 295
    int-to-float v10, v10

    .line 296
    add-float v10, v10, v17

    .line 297
    .line 298
    int-to-float v11, v8

    .line 299
    sub-float/2addr v11, v6

    .line 300
    sub-int v6, v5, v3

    .line 301
    .line 302
    int-to-float v6, v6

    .line 303
    sub-float v12, v6, v17

    .line 304
    .line 305
    sub-int/2addr v8, v2

    .line 306
    int-to-float v13, v8

    .line 307
    sub-int/2addr v5, v3

    .line 308
    int-to-float v14, v5

    .line 309
    move-object v8, v4

    .line 310
    invoke-virtual/range {v8 .. v14}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 311
    .line 312
    .line 313
    iget v2, v0, Landroid/graphics/Rect;->right:I

    .line 314
    .line 315
    int-to-float v5, v2

    .line 316
    sub-float v9, v5, v15

    .line 317
    .line 318
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 319
    .line 320
    sub-int v5, v0, v3

    .line 321
    .line 322
    int-to-float v5, v5

    .line 323
    add-float v10, v5, v17

    .line 324
    .line 325
    int-to-float v5, v2

    .line 326
    sub-float v11, v5, v7

    .line 327
    .line 328
    sub-int v5, v0, v3

    .line 329
    .line 330
    int-to-float v5, v5

    .line 331
    sub-float v12, v5, v17

    .line 332
    .line 333
    sub-int v2, v2, v16

    .line 334
    .line 335
    int-to-float v13, v2

    .line 336
    sub-int/2addr v0, v3

    .line 337
    int-to-float v14, v0

    .line 338
    invoke-virtual/range {v8 .. v14}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 339
    .line 340
    .line 341
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 342
    .line 343
    .line 344
    goto/16 :goto_2

    .line 345
    .line 346
    :cond_6
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 347
    .line 348
    mul-int/lit8 v17, v2, 0x2

    .line 349
    .line 350
    sub-int v7, v7, v17

    .line 351
    .line 352
    int-to-float v7, v7

    .line 353
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 354
    .line 355
    add-int/2addr v9, v3

    .line 356
    int-to-float v9, v9

    .line 357
    invoke-virtual {v4, v7, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 358
    .line 359
    .line 360
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 361
    .line 362
    int-to-float v9, v7

    .line 363
    int-to-float v14, v2

    .line 364
    mul-float v16, v16, v14

    .line 365
    .line 366
    sub-float v9, v9, v16

    .line 367
    .line 368
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 369
    .line 370
    add-int v11, v10, v3

    .line 371
    .line 372
    int-to-float v11, v11

    .line 373
    int-to-float v12, v3

    .line 374
    mul-float v18, v12, v8

    .line 375
    .line 376
    sub-float v11, v11, v18

    .line 377
    .line 378
    int-to-float v8, v7

    .line 379
    mul-float v15, v15, v14

    .line 380
    .line 381
    sub-float v12, v8, v15

    .line 382
    .line 383
    add-int v8, v10, v3

    .line 384
    .line 385
    int-to-float v8, v8

    .line 386
    add-float v13, v8, v18

    .line 387
    .line 388
    sub-int/2addr v7, v2

    .line 389
    int-to-float v7, v7

    .line 390
    add-int/2addr v10, v3

    .line 391
    int-to-float v10, v10

    .line 392
    move-object v8, v4

    .line 393
    move/from16 v19, v10

    .line 394
    .line 395
    move v10, v11

    .line 396
    move v11, v12

    .line 397
    move v12, v13

    .line 398
    move v13, v7

    .line 399
    move v7, v14

    .line 400
    move/from16 v14, v19

    .line 401
    .line 402
    invoke-virtual/range {v8 .. v14}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 403
    .line 404
    .line 405
    iget v8, v0, Landroid/graphics/Rect;->right:I

    .line 406
    .line 407
    int-to-float v9, v8

    .line 408
    mul-float v6, v6, v7

    .line 409
    .line 410
    sub-float/2addr v9, v6

    .line 411
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 412
    .line 413
    add-int v11, v10, v3

    .line 414
    .line 415
    int-to-float v11, v11

    .line 416
    sub-float v11, v11, v18

    .line 417
    .line 418
    int-to-float v12, v8

    .line 419
    mul-float v5, v5, v7

    .line 420
    .line 421
    sub-float v7, v12, v5

    .line 422
    .line 423
    add-int v12, v10, v3

    .line 424
    .line 425
    int-to-float v12, v12

    .line 426
    add-float v12, v12, v18

    .line 427
    .line 428
    int-to-float v13, v8

    .line 429
    add-int/2addr v10, v3

    .line 430
    int-to-float v14, v10

    .line 431
    move-object v8, v4

    .line 432
    move v10, v11

    .line 433
    move v11, v7

    .line 434
    invoke-virtual/range {v8 .. v14}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 435
    .line 436
    .line 437
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 438
    .line 439
    add-int v7, v7, v17

    .line 440
    .line 441
    int-to-float v7, v7

    .line 442
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    .line 443
    .line 444
    sub-int/2addr v8, v3

    .line 445
    int-to-float v8, v8

    .line 446
    invoke-virtual {v4, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 447
    .line 448
    .line 449
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 450
    .line 451
    int-to-float v8, v7

    .line 452
    add-float v9, v8, v16

    .line 453
    .line 454
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    .line 455
    .line 456
    sub-int v10, v8, v3

    .line 457
    .line 458
    int-to-float v10, v10

    .line 459
    add-float v10, v10, v18

    .line 460
    .line 461
    int-to-float v11, v7

    .line 462
    add-float/2addr v11, v15

    .line 463
    sub-int v12, v8, v3

    .line 464
    .line 465
    int-to-float v12, v12

    .line 466
    sub-float v12, v12, v18

    .line 467
    .line 468
    add-int/2addr v7, v2

    .line 469
    int-to-float v13, v7

    .line 470
    sub-int/2addr v8, v3

    .line 471
    int-to-float v14, v8

    .line 472
    move-object v8, v4

    .line 473
    invoke-virtual/range {v8 .. v14}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 474
    .line 475
    .line 476
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 477
    .line 478
    int-to-float v7, v2

    .line 479
    add-float v9, v7, v6

    .line 480
    .line 481
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 482
    .line 483
    sub-int v6, v0, v3

    .line 484
    .line 485
    int-to-float v6, v6

    .line 486
    add-float v10, v6, v18

    .line 487
    .line 488
    int-to-float v6, v2

    .line 489
    add-float v11, v6, v5

    .line 490
    .line 491
    sub-int v5, v0, v3

    .line 492
    .line 493
    int-to-float v5, v5

    .line 494
    sub-float v12, v5, v18

    .line 495
    .line 496
    int-to-float v13, v2

    .line 497
    sub-int/2addr v0, v3

    .line 498
    int-to-float v14, v0

    .line 499
    invoke-virtual/range {v8 .. v14}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 500
    .line 501
    .line 502
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 503
    .line 504
    .line 505
    :goto_2
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 506
    .line 507
    .line 508
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 509
    .line 510
    .line 511
    move-result-object v0

    .line 512
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 513
    .line 514
    .line 515
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 516
    .line 517
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 518
    .line 519
    .line 520
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 521
    .line 522
    return-object v0
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getEllipseRibbon2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    .line 1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    move-result-object v1

    .line 2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    move-result v3

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v4, 0x3

    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v6, 0x2

    const/high16 v13, 0x3e000000    # 0.125f

    const/high16 v7, 0x3e800000    # 0.25f

    const/high16 v14, 0x40000000    # 2.0f

    const/4 v15, 0x0

    const/4 v11, 0x1

    if-eqz v3, :cond_3

    if-eqz v1, :cond_2

    .line 4
    array-length v3, v1

    if-ne v3, v4, :cond_2

    .line 5
    aget-object v3, v1, v15

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    aget-object v4, v1, v6

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    sub-float/2addr v3, v4

    const v4, 0x3e4ccccd    # 0.2f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 6
    aget-object v3, v1, v15

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v6

    .line 7
    :cond_0
    aget-object v3, v1, v11

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/high16 v4, 0x3f400000    # 0.75f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 8
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v11

    .line 9
    :cond_1
    aget-object v3, v1, v11

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    div-float/2addr v3, v14

    sub-float/2addr v5, v3

    .line 10
    aget-object v3, v1, v15

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    mul-float v3, v3, v2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    div-float v4, v2, v14

    .line 11
    aget-object v7, v1, v11

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    mul-float v4, v4, v7

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 12
    aget-object v1, v1, v6

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float v1, v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    move/from16 v16, v5

    goto/16 :goto_4

    :cond_2
    mul-float v1, v2, v7

    .line 13
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v3

    div-float v1, v2, v14

    mul-float v1, v1, v5

    .line 14
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    mul-float v1, v2, v13

    .line 15
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    goto :goto_3

    :cond_3
    if-eqz v1, :cond_7

    .line 16
    array-length v3, v1

    if-lt v3, v11, :cond_7

    .line 17
    aget-object v3, v1, v15

    if-eqz v3, :cond_4

    .line 18
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 19
    aget-object v8, v1, v15

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    sub-float/2addr v5, v8

    mul-float v5, v5, v2

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    goto :goto_0

    :cond_4
    mul-float v3, v2, v7

    .line 20
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    const/high16 v3, 0x3e800000    # 0.25f

    .line 21
    :goto_0
    array-length v5, v1

    if-lt v5, v6, :cond_5

    aget-object v5, v1, v11

    if-eqz v5, :cond_5

    .line 22
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    sub-float v5, v12, v5

    mul-float v5, v5, v2

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    goto :goto_1

    :cond_5
    mul-float v7, v7, v2

    .line 23
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 24
    :goto_1
    array-length v7, v1

    if-lt v7, v4, :cond_6

    aget-object v1, v1, v6

    if-eqz v1, :cond_6

    .line 25
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float v1, v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    goto :goto_2

    :cond_6
    mul-float v1, v2, v13

    .line 26
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    :goto_2
    move/from16 v16, v3

    move v3, v5

    goto :goto_4

    :cond_7
    mul-float v1, v2, v7

    .line 27
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v3

    div-float v1, v2, v14

    mul-float v1, v1, v5

    .line 28
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    mul-float v1, v2, v13

    .line 29
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    :goto_3
    const/high16 v16, 0x3e800000    # 0.25f

    .line 30
    :goto_4
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->sm:Landroid/graphics/Matrix;

    invoke-virtual {v4}, Landroid/graphics/Matrix;->reset()V

    .line 31
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->sm:Landroid/graphics/Matrix;

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v2

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->postScale(FF)Z

    const/4 v10, 0x0

    if-lt v1, v3, :cond_9

    const/4 v1, 0x0

    int-to-float v12, v3

    div-float v9, v2, v14

    const/4 v8, 0x0

    const/high16 v16, 0x3f000000    # 0.5f

    move v3, v1

    move v4, v12

    move v5, v2

    move v6, v12

    move v7, v9

    move v1, v9

    move/from16 v9, v16

    .line 32
    invoke-static/range {v3 .. v9}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->computeBezierCtrPoint(FFFFFFF)Ljava/util/List;

    move-result-object v9

    .line 33
    new-instance v8, Lcom/intsig/office/common/autoshape/ExtendPath;

    invoke-direct {v8}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 34
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 35
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    move-result-object v3

    invoke-virtual {v8, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 36
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    move-result-object v3

    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v3

    invoke-virtual {v8, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 37
    :cond_8
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 38
    invoke-virtual {v7, v10, v12}, Landroid/graphics/Path;->moveTo(FF)V

    .line 39
    invoke-interface {v9, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v1

    div-float v4, v3, v14

    invoke-interface {v9, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v5, v3, Landroid/graphics/PointF;->y:F

    .line 40
    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v1

    div-float v6, v3, v14

    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v10, v3, Landroid/graphics/PointF;->y:F

    move-object v3, v7

    move-object v15, v7

    move v7, v10

    move-object v10, v8

    move v8, v2

    move-object v14, v9

    move v9, v12

    .line 41
    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    mul-float v13, v13, v2

    sub-float v3, v2, v13

    .line 42
    invoke-virtual {v15, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 43
    invoke-virtual {v15, v2, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 44
    invoke-interface {v14, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v1

    const/high16 v4, 0x40000000    # 2.0f

    div-float v5, v3, v4

    invoke-interface {v14, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v2

    sub-float v6, v3, v12

    const/4 v3, 0x0

    .line 45
    invoke-interface {v14, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    add-float/2addr v7, v1

    div-float/2addr v7, v4

    invoke-interface {v14, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v2

    sub-float v8, v3, v12

    const/4 v9, 0x0

    move-object v3, v15

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v2

    .line 46
    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 47
    invoke-virtual {v15, v13, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 48
    invoke-virtual {v15}, Landroid/graphics/Path;->close()V

    .line 49
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->sm:Landroid/graphics/Matrix;

    invoke-virtual {v15, v1}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 50
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {v15, v1, v0}, Landroid/graphics/Path;->offset(FF)V

    .line 51
    invoke-virtual {v10, v15}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 52
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 53
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    :cond_9
    const/4 v4, 0x0

    int-to-float v14, v3

    const/high16 v5, 0x40000000    # 2.0f

    div-float v15, v2, v5

    sub-int/2addr v3, v1

    int-to-float v8, v3

    const/high16 v9, 0x3f000000    # 0.5f

    move v3, v4

    move v4, v14

    move v5, v2

    move v6, v14

    move v7, v15

    .line 54
    invoke-static/range {v3 .. v9}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->computeBezierCtrPoint(FFFFFFF)Ljava/util/List;

    move-result-object v9

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 55
    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, v15

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    .line 56
    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v4, v15

    div-float v8, v4, v6

    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v6, v4, Landroid/graphics/PointF;->y:F

    const/high16 v17, 0x3e000000    # 0.125f

    move v4, v14

    move/from16 v18, v6

    move v6, v7

    move v7, v8

    move/from16 v8, v18

    move-object v12, v9

    move v9, v2

    move v10, v14

    const/4 v13, 0x1

    move/from16 v11, v17

    .line 57
    invoke-static/range {v3 .. v11}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->BezierComputePoint(FFFFFFFFF)Landroid/graphics/PointF;

    move-result-object v11

    const/4 v4, 0x0

    .line 58
    invoke-interface {v12, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, v15

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-interface {v12, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    .line 59
    invoke-interface {v12, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v4, v15

    div-float v8, v4, v6

    invoke-interface {v12, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v9, v4, Landroid/graphics/PointF;->y:F

    move v4, v14

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v2

    move-object/from16 v24, v11

    move/from16 v11, v16

    .line 60
    invoke-static/range {v3 .. v11}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->BezierComputePoint(FFFFFFFFF)Landroid/graphics/PointF;

    const/4 v4, 0x0

    .line 61
    invoke-interface {v12, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, v15

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-interface {v12, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    .line 62
    invoke-interface {v12, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v4, v15

    div-float v8, v4, v6

    invoke-interface {v12, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v9, v4, Landroid/graphics/PointF;->y:F

    const/high16 v4, 0x3e000000    # 0.125f

    add-float v12, v16, v4

    move v4, v14

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v2

    move v11, v12

    .line 63
    invoke-static/range {v3 .. v11}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->BezierComputePoint(FFFFFFFFF)Landroid/graphics/PointF;

    move-result-object v11

    const/16 v17, 0x0

    .line 64
    iget v3, v11, Landroid/graphics/PointF;->x:F

    iget v4, v11, Landroid/graphics/PointF;->y:F

    move-object/from16 v10, v24

    iget v5, v10, Landroid/graphics/PointF;->x:F

    iget v6, v10, Landroid/graphics/PointF;->y:F

    const/high16 v7, 0x3e000000    # 0.125f

    div-float v12, v7, v12

    move/from16 v18, v14

    move/from16 v19, v3

    move/from16 v20, v4

    move/from16 v21, v5

    move/from16 v22, v6

    move/from16 v23, v12

    invoke-static/range {v17 .. v23}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->computeBezierCtrPoint(FFFFFFF)Ljava/util/List;

    move-result-object v3

    .line 65
    new-instance v9, Lcom/intsig/office/common/autoshape/ExtendPath;

    invoke-direct {v9}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 66
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 67
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    move-result-object v4

    invoke-virtual {v9, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 68
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    move-result-object v4

    invoke-virtual {v4}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v4

    invoke-virtual {v9, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 69
    :cond_a
    new-instance v8, Landroid/graphics/Path;

    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    const/4 v4, 0x0

    .line 70
    invoke-virtual {v8, v4, v14}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v4, 0x0

    .line 71
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v4, v6, Landroid/graphics/PointF;->y:F

    .line 72
    invoke-interface {v3, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    invoke-interface {v3, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    iget v13, v11, Landroid/graphics/PointF;->x:F

    move-object/from16 v24, v9

    iget v9, v11, Landroid/graphics/PointF;->y:F

    move-object/from16 v17, v8

    move/from16 v18, v5

    move/from16 v19, v4

    move/from16 v20, v6

    move/from16 v21, v7

    move/from16 v22, v13

    move/from16 v23, v9

    .line 73
    invoke-virtual/range {v17 .. v23}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 74
    iget v4, v11, Landroid/graphics/PointF;->x:F

    iget v5, v11, Landroid/graphics/PointF;->y:F

    add-float/2addr v5, v2

    sub-float/2addr v5, v14

    invoke-virtual {v8, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    const/4 v4, 0x1

    .line 75
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v4, v6, Landroid/graphics/PointF;->y:F

    add-float/2addr v4, v2

    sub-float v6, v4, v14

    const/4 v4, 0x0

    .line 76
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v2

    sub-float v9, v3, v14

    const/4 v13, 0x0

    move-object v3, v8

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v9

    move-object v9, v8

    move v8, v13

    move-object/from16 v25, v9

    move-object/from16 v13, v24

    move v9, v2

    .line 77
    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    const/4 v3, 0x0

    add-float v4, v2, v14

    const/high16 v5, 0x40000000    # 2.0f

    div-float v17, v4, v5

    int-to-float v1, v1

    sub-float v8, v17, v1

    const/high16 v9, 0x3f000000    # 0.5f

    move/from16 v4, v17

    move v5, v2

    move/from16 v6, v17

    move v7, v15

    .line 78
    invoke-static/range {v3 .. v9}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->computeBezierCtrPoint(FFFFFFF)Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 79
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    add-float/2addr v6, v15

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    const/4 v5, 0x1

    .line 80
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    add-float/2addr v9, v15

    div-float/2addr v9, v7

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v7, v3, Landroid/graphics/PointF;->y:F

    const/high16 v18, 0x3e000000    # 0.125f

    move v3, v4

    move/from16 v4, v17

    move v5, v6

    move v6, v8

    move v8, v7

    move v7, v9

    move v9, v2

    move-object/from16 v26, v10

    move/from16 v10, v17

    move/from16 v24, v15

    move-object v15, v11

    move/from16 v11, v18

    .line 81
    invoke-static/range {v3 .. v11}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->BezierComputePoint(FFFFFFFFF)Landroid/graphics/PointF;

    move-result-object v10

    .line 82
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v11, v25

    invoke-virtual {v11, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 83
    invoke-virtual {v11}, Landroid/graphics/Path;->close()V

    .line 84
    iget v3, v15, Landroid/graphics/PointF;->x:F

    sub-float v3, v2, v3

    iget v4, v15, Landroid/graphics/PointF;->y:F

    move-object/from16 v5, v26

    iget v6, v5, Landroid/graphics/PointF;->x:F

    sub-float v7, v2, v6

    iget v8, v5, Landroid/graphics/PointF;->y:F

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float v9, v5, v12

    move v5, v2

    move v6, v14

    invoke-static/range {v3 .. v9}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->computeBezierCtrPoint(FFFFFFF)Ljava/util/List;

    move-result-object v12

    .line 85
    iget v3, v15, Landroid/graphics/PointF;->x:F

    sub-float v3, v2, v3

    iget v4, v15, Landroid/graphics/PointF;->y:F

    invoke-virtual {v11, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v3, 0x0

    .line 86
    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    const/4 v3, 0x1

    .line 87
    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    move-object v3, v11

    move v8, v2

    move v9, v14

    .line 88
    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 89
    iget v3, v10, Landroid/graphics/PointF;->x:F

    sub-float v3, v2, v3

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v11, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 90
    invoke-virtual {v11, v2, v2}, Landroid/graphics/Path;->lineTo(FF)V

    const/4 v3, 0x1

    .line 91
    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v3, v5, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v2

    sub-float v19, v3, v14

    const/4 v3, 0x0

    .line 92
    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v3, v6, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v2

    sub-float v21, v3, v14

    iget v3, v15, Landroid/graphics/PointF;->x:F

    sub-float v22, v2, v3

    iget v3, v15, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v2

    sub-float v23, v3, v14

    move-object/from16 v17, v11

    move/from16 v18, v4

    move/from16 v20, v5

    .line 93
    invoke-virtual/range {v17 .. v23}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 94
    invoke-virtual {v11}, Landroid/graphics/Path;->close()V

    .line 95
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->sm:Landroid/graphics/Matrix;

    invoke-virtual {v11, v3}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 96
    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget v4, v0, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    invoke-virtual {v11, v3, v4}, Landroid/graphics/Path;->offset(FF)V

    .line 97
    invoke-virtual {v13, v11}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 98
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v3

    invoke-virtual {v13, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 99
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    invoke-interface {v3, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x0

    const/4 v8, 0x0

    const/high16 v9, 0x3f000000    # 0.5f

    move v4, v1

    move v5, v2

    move v6, v1

    move/from16 v7, v24

    .line 100
    invoke-static/range {v3 .. v9}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->computeBezierCtrPoint(FFFFFFF)Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 101
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    add-float v6, v6, v24

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    const/4 v5, 0x1

    .line 102
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    add-float v9, v9, v24

    div-float/2addr v9, v7

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v10, v3, Landroid/graphics/PointF;->y:F

    move v3, v4

    move v4, v1

    move v5, v6

    move v6, v8

    move v7, v9

    move v8, v10

    move v9, v2

    move v10, v1

    move/from16 v11, v16

    .line 103
    invoke-static/range {v3 .. v11}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->BezierComputePoint(FFFFFFFFF)Landroid/graphics/PointF;

    move-result-object v1

    .line 104
    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v4, v1, Landroid/graphics/PointF;->y:F

    sub-float v19, v2, v3

    const/16 v22, 0x0

    const/high16 v23, 0x3f000000    # 0.5f

    move/from16 v17, v3

    move/from16 v18, v4

    move/from16 v20, v4

    move/from16 v21, v24

    invoke-static/range {v17 .. v23}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->computeBezierCtrPoint(FFFFFFF)Ljava/util/List;

    move-result-object v3

    .line 105
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 106
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 107
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 108
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    move-result-object v5

    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 109
    :cond_b
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 110
    iget v6, v1, Landroid/graphics/PointF;->x:F

    iget v7, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v6, 0x0

    .line 111
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    add-float v7, v7, v24

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    const/4 v6, 0x1

    .line 112
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    add-float v10, v10, v24

    div-float/2addr v10, v8

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    iget v11, v8, Landroid/graphics/PointF;->y:F

    iget v6, v1, Landroid/graphics/PointF;->x:F

    sub-float v12, v2, v6

    iget v13, v1, Landroid/graphics/PointF;->y:F

    move-object v6, v5

    move v8, v9

    move v9, v10

    move v10, v11

    move v11, v12

    move v12, v13

    .line 113
    invoke-virtual/range {v6 .. v12}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 114
    iget v6, v1, Landroid/graphics/PointF;->x:F

    sub-float v6, v2, v6

    iget v7, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v7, v2

    sub-float/2addr v7, v14

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    const/4 v6, 0x1

    .line 115
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    add-float v7, v7, v24

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    add-float/2addr v6, v2

    sub-float v9, v6, v14

    const/4 v6, 0x0

    .line 116
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    add-float v10, v10, v24

    div-float/2addr v10, v8

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v2

    sub-float/2addr v3, v14

    iget v11, v1, Landroid/graphics/PointF;->x:F

    iget v6, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v6, v2

    sub-float v12, v6, v14

    move-object v6, v5

    move v8, v9

    move v9, v10

    move v10, v3

    .line 117
    invoke-virtual/range {v6 .. v12}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 118
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 119
    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v6, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v6, v2

    sub-float/2addr v6, v14

    invoke-virtual {v5, v3, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 120
    iget v3, v15, Landroid/graphics/PointF;->x:F

    iget v6, v15, Landroid/graphics/PointF;->y:F

    add-float/2addr v6, v2

    sub-float/2addr v6, v14

    invoke-virtual {v5, v3, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 121
    iget v3, v1, Landroid/graphics/PointF;->x:F

    sub-float v3, v2, v3

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    sub-float/2addr v1, v14

    invoke-virtual {v5, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 122
    iget v1, v15, Landroid/graphics/PointF;->x:F

    sub-float v1, v2, v1

    iget v3, v15, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v2

    sub-float/2addr v3, v14

    invoke-virtual {v5, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 123
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 124
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->sm:Landroid/graphics/Matrix;

    invoke-virtual {v5, v1}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 125
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {v5, v1, v0}, Landroid/graphics/Path;->offset(FF)V

    .line 126
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 127
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 128
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    :goto_5
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    return-object v0
.end method

.method private static getEllipseRibbonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 36
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    .line 1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    move-result-object v1

    .line 2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    move-result v3

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v4, 0x3

    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v6, 0x2

    const/high16 v13, 0x3e000000    # 0.125f

    const/high16 v7, 0x3e800000    # 0.25f

    const/high16 v14, 0x40000000    # 2.0f

    const/4 v15, 0x0

    const/4 v11, 0x1

    if-eqz v3, :cond_3

    if-eqz v1, :cond_2

    .line 4
    array-length v3, v1

    if-ne v3, v4, :cond_2

    .line 5
    aget-object v3, v1, v15

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    aget-object v4, v1, v6

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    sub-float/2addr v3, v4

    const v4, 0x3e4ccccd    # 0.2f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 6
    aget-object v3, v1, v15

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v6

    .line 7
    :cond_0
    aget-object v3, v1, v11

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/high16 v4, 0x3f400000    # 0.75f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 8
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v11

    .line 9
    :cond_1
    aget-object v3, v1, v11

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    div-float/2addr v3, v14

    sub-float/2addr v5, v3

    .line 10
    aget-object v3, v1, v15

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    mul-float v3, v3, v2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    div-float v4, v2, v14

    .line 11
    aget-object v7, v1, v11

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    mul-float v4, v4, v7

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 12
    aget-object v1, v1, v6

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float v1, v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    move v10, v3

    move/from16 v16, v5

    goto/16 :goto_4

    :cond_2
    mul-float v1, v2, v7

    .line 13
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v3

    div-float v1, v2, v14

    mul-float v1, v1, v5

    .line 14
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    mul-float v1, v2, v13

    .line 15
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    goto :goto_3

    :cond_3
    if-eqz v1, :cond_7

    .line 16
    array-length v3, v1

    if-lt v3, v11, :cond_7

    .line 17
    aget-object v3, v1, v15

    if-eqz v3, :cond_4

    .line 18
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 19
    aget-object v8, v1, v15

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    sub-float/2addr v5, v8

    mul-float v5, v5, v2

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    goto :goto_0

    :cond_4
    mul-float v3, v2, v7

    .line 20
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    const/high16 v3, 0x3e800000    # 0.25f

    .line 21
    :goto_0
    array-length v5, v1

    if-lt v5, v6, :cond_5

    aget-object v5, v1, v11

    if-eqz v5, :cond_5

    .line 22
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    mul-float v5, v5, v2

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    goto :goto_1

    :cond_5
    mul-float v7, v7, v2

    .line 23
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 24
    :goto_1
    array-length v7, v1

    if-lt v7, v4, :cond_6

    aget-object v1, v1, v6

    if-eqz v1, :cond_6

    .line 25
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    sub-float v1, v12, v1

    mul-float v1, v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    goto :goto_2

    :cond_6
    mul-float v1, v2, v13

    .line 26
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    :goto_2
    move/from16 v16, v3

    move v10, v5

    goto :goto_4

    :cond_7
    mul-float v1, v2, v7

    .line 27
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v3

    div-float v1, v2, v14

    mul-float v1, v1, v5

    .line 28
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    mul-float v1, v2, v13

    .line 29
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    :goto_3
    move v10, v3

    const/high16 v16, 0x3e800000    # 0.25f

    .line 30
    :goto_4
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->sm:Landroid/graphics/Matrix;

    invoke-virtual {v3}, Landroid/graphics/Matrix;->reset()V

    .line 31
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->sm:Landroid/graphics/Matrix;

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v2

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    const/4 v9, 0x0

    if-lt v1, v10, :cond_9

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    div-float v1, v2, v14

    int-to-float v10, v10

    const/high16 v12, 0x3f000000    # 0.5f

    move v5, v2

    move v7, v1

    move v8, v10

    const/4 v13, 0x0

    move v9, v12

    .line 32
    invoke-static/range {v3 .. v9}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->computeBezierCtrPoint(FFFFFFF)Ljava/util/List;

    move-result-object v12

    .line 33
    new-instance v9, Lcom/intsig/office/common/autoshape/ExtendPath;

    invoke-direct {v9}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 34
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 35
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    move-result-object v3

    invoke-virtual {v9, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 36
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    move-result-object v3

    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v3

    invoke-virtual {v9, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 37
    :cond_8
    new-instance v8, Landroid/graphics/Path;

    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    .line 38
    invoke-virtual {v8, v13, v13}, Landroid/graphics/Path;->moveTo(FF)V

    .line 39
    invoke-interface {v12, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v1

    div-float v4, v3, v14

    invoke-interface {v12, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v5, v3, Landroid/graphics/PointF;->y:F

    .line 40
    invoke-interface {v12, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v1

    div-float v6, v3, v14

    invoke-interface {v12, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v7, v3, Landroid/graphics/PointF;->y:F

    const/4 v13, 0x0

    move-object v3, v8

    move-object v15, v8

    move v8, v2

    move-object/from16 v24, v9

    move v9, v13

    .line 41
    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    const/high16 v3, 0x3e000000    # 0.125f

    mul-float v13, v2, v3

    sub-float v3, v2, v13

    .line 42
    invoke-virtual {v15, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    sub-float v3, v2, v10

    .line 43
    invoke-virtual {v15, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 44
    invoke-interface {v12, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v4, v1

    div-float v18, v4, v14

    invoke-interface {v12, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    add-float/2addr v4, v2

    sub-float v19, v4, v10

    const/4 v4, 0x0

    .line 45
    invoke-interface {v12, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, v1

    div-float v20, v5, v14

    invoke-interface {v12, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    add-float/2addr v4, v2

    sub-float v21, v4, v10

    const/16 v22, 0x0

    move-object/from16 v17, v15

    move/from16 v23, v3

    .line 46
    invoke-virtual/range {v17 .. v23}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 47
    invoke-virtual {v15, v13, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 48
    invoke-virtual {v15}, Landroid/graphics/Path;->close()V

    .line 49
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->sm:Landroid/graphics/Matrix;

    invoke-virtual {v15, v1}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 50
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {v15, v1, v0}, Landroid/graphics/Path;->offset(FF)V

    move-object/from16 v0, v24

    .line 51
    invoke-virtual {v0, v15}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 52
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 53
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    :cond_9
    const/4 v13, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    div-float v15, v2, v14

    int-to-float v1, v1

    const/high16 v9, 0x3f000000    # 0.5f

    move v5, v2

    move v7, v15

    move v8, v1

    .line 54
    invoke-static/range {v3 .. v9}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->computeBezierCtrPoint(FFFFFFF)Ljava/util/List;

    move-result-object v9

    const/4 v5, 0x0

    .line 55
    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    add-float/2addr v6, v15

    div-float/2addr v6, v14

    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    .line 56
    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, v15

    div-float v8, v5, v14

    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    const/16 v18, 0x0

    const/high16 v19, 0x3e000000    # 0.125f

    move/from16 v20, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move/from16 v8, v20

    move-object v12, v9

    move v9, v2

    move/from16 v25, v10

    move/from16 v10, v18

    const/4 v13, 0x1

    move/from16 v11, v19

    .line 57
    invoke-static/range {v3 .. v11}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->BezierComputePoint(FFFFFFFFF)Landroid/graphics/PointF;

    move-result-object v11

    const/4 v5, 0x0

    .line 58
    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    add-float/2addr v6, v15

    div-float/2addr v6, v14

    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    .line 59
    invoke-interface {v12, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, v15

    div-float v8, v5, v14

    invoke-interface {v12, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v9, v5, Landroid/graphics/PointF;->y:F

    const/4 v10, 0x0

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v2

    move-object/from16 v26, v11

    move/from16 v11, v16

    .line 60
    invoke-static/range {v3 .. v11}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->BezierComputePoint(FFFFFFFFF)Landroid/graphics/PointF;

    const/4 v5, 0x0

    .line 61
    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    add-float/2addr v6, v15

    div-float/2addr v6, v14

    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    .line 62
    invoke-interface {v12, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, v15

    div-float v8, v5, v14

    invoke-interface {v12, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v9, v5, Landroid/graphics/PointF;->y:F

    const/high16 v5, 0x3e000000    # 0.125f

    add-float v12, v16, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v2

    move v11, v12

    .line 63
    invoke-static/range {v3 .. v11}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->BezierComputePoint(FFFFFFFFF)Landroid/graphics/PointF;

    move-result-object v11

    .line 64
    iget v5, v11, Landroid/graphics/PointF;->x:F

    iget v6, v11, Landroid/graphics/PointF;->y:F

    move-object/from16 v10, v26

    iget v7, v10, Landroid/graphics/PointF;->x:F

    iget v8, v10, Landroid/graphics/PointF;->y:F

    const/high16 v9, 0x3e000000    # 0.125f

    div-float v12, v9, v12

    move v9, v12

    invoke-static/range {v3 .. v9}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->computeBezierCtrPoint(FFFFFFF)Ljava/util/List;

    move-result-object v3

    .line 65
    new-instance v9, Lcom/intsig/office/common/autoshape/ExtendPath;

    invoke-direct {v9}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 66
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 67
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    move-result-object v4

    invoke-virtual {v9, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 68
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    move-result-object v4

    invoke-virtual {v4}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v4

    invoke-virtual {v9, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 69
    :cond_a
    new-instance v8, Landroid/graphics/Path;

    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    const/4 v4, 0x0

    .line 70
    invoke-virtual {v8, v4, v4}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v4, 0x0

    .line 71
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v4, v6, Landroid/graphics/PointF;->y:F

    .line 72
    invoke-interface {v3, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    invoke-interface {v3, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    iget v14, v11, Landroid/graphics/PointF;->x:F

    iget v13, v11, Landroid/graphics/PointF;->y:F

    move-object/from16 v26, v8

    move/from16 v27, v5

    move/from16 v28, v4

    move/from16 v29, v6

    move/from16 v30, v7

    move/from16 v31, v14

    move/from16 v32, v13

    .line 73
    invoke-virtual/range {v26 .. v32}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 74
    iget v4, v11, Landroid/graphics/PointF;->x:F

    iget v5, v11, Landroid/graphics/PointF;->y:F

    add-float/2addr v5, v2

    move/from16 v6, v25

    int-to-float v13, v6

    sub-float/2addr v5, v13

    invoke-virtual {v8, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    const/4 v4, 0x1

    .line 75
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v4, v6, Landroid/graphics/PointF;->y:F

    add-float/2addr v4, v2

    sub-float v28, v4, v13

    const/4 v4, 0x0

    .line 76
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v2

    sub-float v30, v3, v13

    const/16 v31, 0x0

    sub-float v14, v2, v13

    move/from16 v27, v5

    move/from16 v29, v6

    move/from16 v32, v14

    .line 77
    invoke-virtual/range {v26 .. v32}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    const/4 v3, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    div-float v19, v14, v4

    add-float v21, v19, v1

    const/high16 v22, 0x3f000000    # 0.5f

    move/from16 v4, v19

    move v5, v2

    move/from16 v6, v19

    move v7, v15

    move-object/from16 v33, v8

    move/from16 v8, v21

    move-object/from16 v34, v9

    move/from16 v9, v22

    .line 78
    invoke-static/range {v3 .. v9}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->computeBezierCtrPoint(FFFFFFF)Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 79
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    add-float/2addr v6, v15

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    const/4 v5, 0x1

    .line 80
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    add-float/2addr v9, v15

    div-float/2addr v9, v7

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v7, v3, Landroid/graphics/PointF;->y:F

    const/high16 v21, 0x3e000000    # 0.125f

    move v3, v4

    move/from16 v4, v19

    move v5, v6

    move v6, v8

    move v8, v7

    move v7, v9

    move v9, v2

    move-object/from16 v35, v10

    move/from16 v10, v19

    move/from16 v19, v15

    move-object v15, v11

    move/from16 v11, v21

    .line 81
    invoke-static/range {v3 .. v11}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->BezierComputePoint(FFFFFFFFF)Landroid/graphics/PointF;

    move-result-object v10

    .line 82
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v11, v33

    invoke-virtual {v11, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 83
    invoke-virtual {v11}, Landroid/graphics/Path;->close()V

    .line 84
    iget v3, v15, Landroid/graphics/PointF;->x:F

    sub-float v3, v2, v3

    iget v4, v15, Landroid/graphics/PointF;->y:F

    const/4 v6, 0x0

    move-object/from16 v5, v35

    iget v7, v5, Landroid/graphics/PointF;->x:F

    sub-float v7, v2, v7

    iget v8, v5, Landroid/graphics/PointF;->y:F

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float v9, v5, v12

    move v5, v2

    invoke-static/range {v3 .. v9}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->computeBezierCtrPoint(FFFFFFF)Ljava/util/List;

    move-result-object v12

    .line 85
    iget v3, v15, Landroid/graphics/PointF;->x:F

    sub-float v3, v2, v3

    iget v4, v15, Landroid/graphics/PointF;->y:F

    invoke-virtual {v11, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v3, 0x0

    .line 86
    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    const/4 v3, 0x1

    .line 87
    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    const/4 v9, 0x0

    move-object v3, v11

    move v8, v2

    .line 88
    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 89
    iget v3, v10, Landroid/graphics/PointF;->x:F

    sub-float v3, v2, v3

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v11, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 90
    invoke-virtual {v11, v2, v14}, Landroid/graphics/Path;->lineTo(FF)V

    const/4 v3, 0x1

    .line 91
    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v3, v5, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v2

    sub-float v28, v3, v13

    const/4 v3, 0x0

    .line 92
    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v3, v6, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v2

    sub-float v30, v3, v13

    iget v3, v15, Landroid/graphics/PointF;->x:F

    sub-float v31, v2, v3

    iget v3, v15, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v2

    sub-float v32, v3, v13

    move-object/from16 v26, v11

    move/from16 v27, v4

    move/from16 v29, v5

    .line 93
    invoke-virtual/range {v26 .. v32}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 94
    invoke-virtual {v11}, Landroid/graphics/Path;->close()V

    .line 95
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->sm:Landroid/graphics/Matrix;

    invoke-virtual {v11, v3}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 96
    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget v4, v0, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    invoke-virtual {v11, v3, v4}, Landroid/graphics/Path;->offset(FF)V

    move-object/from16 v3, v34

    .line 97
    invoke-virtual {v3, v11}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 98
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 99
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x0

    sub-float v10, v2, v1

    const/high16 v9, 0x3f000000    # 0.5f

    move v4, v10

    move v5, v2

    move v6, v10

    move/from16 v7, v19

    .line 100
    invoke-static/range {v3 .. v9}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->computeBezierCtrPoint(FFFFFFF)Ljava/util/List;

    move-result-object v1

    const/4 v4, 0x0

    .line 101
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    add-float v5, v5, v19

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    const/4 v4, 0x1

    .line 102
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    add-float v8, v8, v19

    div-float/2addr v8, v6

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    move v4, v10

    move v6, v7

    move v7, v8

    move v8, v1

    move v9, v2

    move/from16 v11, v16

    .line 103
    invoke-static/range {v3 .. v11}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->BezierComputePoint(FFFFFFFFF)Landroid/graphics/PointF;

    move-result-object v1

    .line 104
    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v6, v1, Landroid/graphics/PointF;->y:F

    sub-float v5, v2, v3

    const/high16 v9, 0x3f000000    # 0.5f

    move v4, v6

    move/from16 v7, v19

    move v8, v2

    invoke-static/range {v3 .. v9}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->computeBezierCtrPoint(FFFFFFF)Ljava/util/List;

    move-result-object v3

    .line 105
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 106
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 107
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 108
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    move-result-object v5

    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 109
    :cond_b
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 110
    iget v6, v1, Landroid/graphics/PointF;->x:F

    iget v7, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v6, 0x0

    .line 111
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    add-float v7, v7, v19

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    const/4 v6, 0x1

    .line 112
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    add-float v10, v10, v19

    div-float/2addr v10, v8

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    iget v11, v8, Landroid/graphics/PointF;->y:F

    iget v6, v1, Landroid/graphics/PointF;->x:F

    sub-float v12, v2, v6

    iget v13, v1, Landroid/graphics/PointF;->y:F

    move-object v6, v5

    move v8, v9

    move v9, v10

    move v10, v11

    move v11, v12

    move v12, v13

    .line 113
    invoke-virtual/range {v6 .. v12}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 114
    iget v6, v1, Landroid/graphics/PointF;->x:F

    sub-float v6, v2, v6

    iget v7, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v7, v14

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    const/4 v6, 0x1

    .line 115
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    add-float v7, v7, v19

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    sub-float v9, v6, v14

    const/4 v6, 0x0

    .line 116
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    add-float v10, v10, v19

    div-float/2addr v10, v8

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v14

    iget v11, v1, Landroid/graphics/PointF;->x:F

    iget v6, v1, Landroid/graphics/PointF;->y:F

    sub-float v12, v6, v14

    move-object v6, v5

    move v8, v9

    move v9, v10

    move v10, v3

    .line 117
    invoke-virtual/range {v6 .. v12}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 118
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 119
    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v6, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v6, v14

    invoke-virtual {v5, v3, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 120
    iget v3, v15, Landroid/graphics/PointF;->x:F

    iget v6, v15, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v3, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 121
    iget v3, v1, Landroid/graphics/PointF;->x:F

    sub-float v3, v2, v3

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v14

    invoke-virtual {v5, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 122
    iget v1, v15, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v1

    iget v1, v15, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 123
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 124
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->sm:Landroid/graphics/Matrix;

    invoke-virtual {v5, v1}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 125
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {v5, v1, v0}, Landroid/graphics/Path;->offset(FF)V

    .line 126
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 127
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 128
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    :goto_5
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    return-object v0
.end method

.method public static getFlagExtendPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/16 v1, 0x35

    .line 11
    .line 12
    if-eq v0, v1, :cond_8

    .line 13
    .line 14
    const/16 v1, 0x36

    .line 15
    .line 16
    if-eq v0, v1, :cond_7

    .line 17
    .line 18
    const/16 v1, 0x40

    .line 19
    .line 20
    if-eq v0, v1, :cond_6

    .line 21
    .line 22
    const/16 v1, 0xbc

    .line 23
    .line 24
    if-eq v0, v1, :cond_5

    .line 25
    .line 26
    const/16 v1, 0xf4

    .line 27
    .line 28
    if-eq v0, v1, :cond_4

    .line 29
    .line 30
    const/16 v1, 0x61

    .line 31
    .line 32
    if-eq v0, v1, :cond_3

    .line 33
    .line 34
    const/16 v1, 0x62

    .line 35
    .line 36
    if-eq v0, v1, :cond_2

    .line 37
    .line 38
    const/16 v1, 0x6b

    .line 39
    .line 40
    if-eq v0, v1, :cond_1

    .line 41
    .line 42
    const/16 v1, 0x6c

    .line 43
    .line 44
    if-eq v0, v1, :cond_0

    .line 45
    .line 46
    const/4 p0, 0x0

    .line 47
    return-object p0

    .line 48
    :cond_0
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->getEllipseRibbon2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 49
    .line 50
    .line 51
    move-result-object p0

    .line 52
    return-object p0

    .line 53
    :cond_1
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->getEllipseRibbonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 54
    .line 55
    .line 56
    move-result-object p0

    .line 57
    return-object p0

    .line 58
    :cond_2
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->getHorizontalScrollPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 59
    .line 60
    .line 61
    move-result-object p0

    .line 62
    return-object p0

    .line 63
    :cond_3
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->getVerticalScrollPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 64
    .line 65
    .line 66
    move-result-object p0

    .line 67
    return-object p0

    .line 68
    :cond_4
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->getLeftRightRibbon(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 69
    .line 70
    .line 71
    move-result-object p0

    .line 72
    return-object p0

    .line 73
    :cond_5
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->getDoubleWavePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 74
    .line 75
    .line 76
    move-result-object p0

    .line 77
    return-object p0

    .line 78
    :cond_6
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->getWavePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 79
    .line 80
    .line 81
    move-result-object p0

    .line 82
    return-object p0

    .line 83
    :cond_7
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->getRibbon2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 84
    .line 85
    .line 86
    move-result-object p0

    .line 87
    return-object p0

    .line 88
    :cond_8
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->getRibbonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 89
    .line 90
    .line 91
    move-result-object p0

    .line 92
    return-object p0
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getHorizontalScrollPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    const/4 v3, 0x0

    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    array-length v4, v1

    .line 23
    const/4 v5, 0x1

    .line 24
    if-ne v4, v5, :cond_0

    .line 25
    .line 26
    int-to-float v2, v2

    .line 27
    aget-object v1, v1, v3

    .line 28
    .line 29
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    mul-float v2, v2, v1

    .line 34
    .line 35
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    goto :goto_0

    .line 40
    :cond_0
    int-to-float v1, v2

    .line 41
    const/high16 v2, 0x3e000000    # 0.125f

    .line 42
    .line 43
    mul-float v1, v1, v2

    .line 44
    .line 45
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    :goto_0
    int-to-float v2, v1

    .line 50
    const/high16 v4, 0x40000000    # 2.0f

    .line 51
    .line 52
    div-float/2addr v2, v4

    .line 53
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 54
    .line 55
    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 56
    .line 57
    .line 58
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 59
    .line 60
    .line 61
    move-result v5

    .line 62
    if-eqz v5, :cond_1

    .line 63
    .line 64
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 65
    .line 66
    .line 67
    move-result-object v5

    .line 68
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 69
    .line 70
    .line 71
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 72
    .line 73
    .line 74
    move-result-object v5

    .line 75
    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 76
    .line 77
    .line 78
    move-result-object v5

    .line 79
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 80
    .line 81
    .line 82
    :cond_1
    new-instance v5, Landroid/graphics/Path;

    .line 83
    .line 84
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 85
    .line 86
    .line 87
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 88
    .line 89
    int-to-float v6, v6

    .line 90
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 91
    .line 92
    int-to-float v7, v7

    .line 93
    const/high16 v8, 0x40400000    # 3.0f

    .line 94
    .line 95
    mul-float v8, v8, v2

    .line 96
    .line 97
    add-float/2addr v7, v8

    .line 98
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 99
    .line 100
    .line 101
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 102
    .line 103
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 104
    .line 105
    int-to-float v9, v7

    .line 106
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 107
    .line 108
    add-int v11, v10, v1

    .line 109
    .line 110
    int-to-float v11, v11

    .line 111
    add-int/2addr v7, v1

    .line 112
    int-to-float v7, v7

    .line 113
    mul-int/lit8 v12, v1, 0x2

    .line 114
    .line 115
    add-int/2addr v10, v12

    .line 116
    int-to-float v10, v10

    .line 117
    invoke-virtual {v6, v9, v11, v7, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 118
    .line 119
    .line 120
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 121
    .line 122
    const/high16 v7, 0x43340000    # 180.0f

    .line 123
    .line 124
    const/high16 v9, -0x3ccc0000    # -180.0f

    .line 125
    .line 126
    invoke-virtual {v5, v6, v7, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 127
    .line 128
    .line 129
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 130
    .line 131
    add-int/2addr v6, v1

    .line 132
    int-to-float v6, v6

    .line 133
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    .line 134
    .line 135
    int-to-float v10, v10

    .line 136
    sub-float/2addr v10, v2

    .line 137
    invoke-virtual {v5, v6, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 138
    .line 139
    .line 140
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 141
    .line 142
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 143
    .line 144
    int-to-float v11, v10

    .line 145
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 146
    .line 147
    sub-int v14, v13, v1

    .line 148
    .line 149
    int-to-float v14, v14

    .line 150
    add-int/2addr v10, v1

    .line 151
    int-to-float v10, v10

    .line 152
    int-to-float v13, v13

    .line 153
    invoke-virtual {v6, v11, v14, v10, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 154
    .line 155
    .line 156
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 157
    .line 158
    const/4 v10, 0x0

    .line 159
    invoke-virtual {v5, v6, v10, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 160
    .line 161
    .line 162
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 163
    .line 164
    .line 165
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 166
    .line 167
    .line 168
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 169
    .line 170
    .line 171
    move-result-object v5

    .line 172
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 173
    .line 174
    .line 175
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 176
    .line 177
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    .line 179
    .line 180
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 181
    .line 182
    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 183
    .line 184
    .line 185
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 186
    .line 187
    .line 188
    move-result v5

    .line 189
    if-eqz v5, :cond_2

    .line 190
    .line 191
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 192
    .line 193
    .line 194
    move-result-object v5

    .line 195
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 196
    .line 197
    .line 198
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 199
    .line 200
    .line 201
    move-result-object v5

    .line 202
    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 203
    .line 204
    .line 205
    move-result-object v5

    .line 206
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 207
    .line 208
    .line 209
    :cond_2
    new-instance v5, Landroid/graphics/Path;

    .line 210
    .line 211
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 212
    .line 213
    .line 214
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 215
    .line 216
    add-int/2addr v6, v1

    .line 217
    int-to-float v6, v6

    .line 218
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 219
    .line 220
    int-to-float v11, v11

    .line 221
    add-float/2addr v11, v8

    .line 222
    invoke-virtual {v5, v6, v11}, Landroid/graphics/Path;->moveTo(FF)V

    .line 223
    .line 224
    .line 225
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 226
    .line 227
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 228
    .line 229
    int-to-float v11, v8

    .line 230
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 231
    .line 232
    add-int v14, v13, v1

    .line 233
    .line 234
    int-to-float v14, v14

    .line 235
    add-int/2addr v8, v1

    .line 236
    int-to-float v8, v8

    .line 237
    add-int/2addr v13, v12

    .line 238
    int-to-float v13, v13

    .line 239
    invoke-virtual {v6, v11, v14, v8, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 240
    .line 241
    .line 242
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 243
    .line 244
    const/high16 v8, 0x43870000    # 270.0f

    .line 245
    .line 246
    invoke-virtual {v5, v6, v10, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 247
    .line 248
    .line 249
    iget v6, v0, Landroid/graphics/Rect;->right:I

    .line 250
    .line 251
    int-to-float v6, v6

    .line 252
    sub-float/2addr v6, v2

    .line 253
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 254
    .line 255
    add-int/2addr v11, v1

    .line 256
    int-to-float v11, v11

    .line 257
    invoke-virtual {v5, v6, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 258
    .line 259
    .line 260
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 261
    .line 262
    iget v11, v0, Landroid/graphics/Rect;->right:I

    .line 263
    .line 264
    sub-int v13, v11, v1

    .line 265
    .line 266
    int-to-float v13, v13

    .line 267
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 268
    .line 269
    int-to-float v15, v14

    .line 270
    int-to-float v11, v11

    .line 271
    add-int/2addr v14, v1

    .line 272
    int-to-float v14, v14

    .line 273
    invoke-virtual {v6, v13, v15, v11, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 274
    .line 275
    .line 276
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 277
    .line 278
    const/high16 v11, 0x42b40000    # 90.0f

    .line 279
    .line 280
    const/high16 v13, -0x3d4c0000    # -90.0f

    .line 281
    .line 282
    invoke-virtual {v5, v6, v11, v13}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 283
    .line 284
    .line 285
    iget v6, v0, Landroid/graphics/Rect;->right:I

    .line 286
    .line 287
    int-to-float v6, v6

    .line 288
    iget v14, v0, Landroid/graphics/Rect;->bottom:I

    .line 289
    .line 290
    sub-int/2addr v14, v1

    .line 291
    int-to-float v14, v14

    .line 292
    sub-float/2addr v14, v2

    .line 293
    invoke-virtual {v5, v6, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 294
    .line 295
    .line 296
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 297
    .line 298
    iget v14, v0, Landroid/graphics/Rect;->right:I

    .line 299
    .line 300
    sub-int v15, v14, v1

    .line 301
    .line 302
    int-to-float v15, v15

    .line 303
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    .line 304
    .line 305
    sub-int v3, v8, v12

    .line 306
    .line 307
    int-to-float v3, v3

    .line 308
    int-to-float v14, v14

    .line 309
    sub-int/2addr v8, v1

    .line 310
    int-to-float v8, v8

    .line 311
    invoke-virtual {v6, v15, v3, v14, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 312
    .line 313
    .line 314
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 315
    .line 316
    invoke-virtual {v5, v3, v10, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 317
    .line 318
    .line 319
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 320
    .line 321
    add-int/2addr v3, v1

    .line 322
    int-to-float v3, v3

    .line 323
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    .line 324
    .line 325
    sub-int/2addr v6, v1

    .line 326
    int-to-float v6, v6

    .line 327
    invoke-virtual {v5, v3, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 328
    .line 329
    .line 330
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 331
    .line 332
    .line 333
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 334
    .line 335
    .line 336
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 337
    .line 338
    .line 339
    move-result-object v3

    .line 340
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 341
    .line 342
    .line 343
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 344
    .line 345
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346
    .line 347
    .line 348
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 349
    .line 350
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 351
    .line 352
    .line 353
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 354
    .line 355
    .line 356
    move-result v4

    .line 357
    if-eqz v4, :cond_3

    .line 358
    .line 359
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 360
    .line 361
    .line 362
    move-result-object v4

    .line 363
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 364
    .line 365
    .line 366
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 367
    .line 368
    .line 369
    move-result-object v4

    .line 370
    invoke-virtual {v4}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 371
    .line 372
    .line 373
    move-result-object v4

    .line 374
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 375
    .line 376
    .line 377
    :cond_3
    new-instance v4, Landroid/graphics/Path;

    .line 378
    .line 379
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 380
    .line 381
    .line 382
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 383
    .line 384
    sub-int/2addr v5, v1

    .line 385
    int-to-float v5, v5

    .line 386
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 387
    .line 388
    int-to-float v6, v6

    .line 389
    add-float/2addr v6, v2

    .line 390
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 391
    .line 392
    .line 393
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 394
    .line 395
    iget v6, v0, Landroid/graphics/Rect;->right:I

    .line 396
    .line 397
    sub-int v8, v6, v1

    .line 398
    .line 399
    int-to-float v8, v8

    .line 400
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 401
    .line 402
    int-to-float v15, v14

    .line 403
    const/high16 v16, 0x3f000000    # 0.5f

    .line 404
    .line 405
    mul-float v16, v16, v2

    .line 406
    .line 407
    add-float v15, v15, v16

    .line 408
    .line 409
    int-to-float v6, v6

    .line 410
    sub-float/2addr v6, v2

    .line 411
    int-to-float v14, v14

    .line 412
    const/high16 v17, 0x3fc00000    # 1.5f

    .line 413
    .line 414
    mul-float v17, v17, v2

    .line 415
    .line 416
    add-float v14, v14, v17

    .line 417
    .line 418
    invoke-virtual {v5, v8, v15, v6, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 419
    .line 420
    .line 421
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 422
    .line 423
    invoke-virtual {v4, v5, v7, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 424
    .line 425
    .line 426
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 427
    .line 428
    int-to-float v5, v5

    .line 429
    sub-float/2addr v5, v2

    .line 430
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 431
    .line 432
    add-int/2addr v6, v1

    .line 433
    int-to-float v6, v6

    .line 434
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 435
    .line 436
    .line 437
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 438
    .line 439
    sub-int/2addr v5, v1

    .line 440
    int-to-float v5, v5

    .line 441
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 442
    .line 443
    add-int/2addr v6, v1

    .line 444
    int-to-float v6, v6

    .line 445
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 446
    .line 447
    .line 448
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 449
    .line 450
    .line 451
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 452
    .line 453
    .line 454
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 455
    .line 456
    .line 457
    move-result-object v4

    .line 458
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 459
    .line 460
    .line 461
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 462
    .line 463
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 464
    .line 465
    .line 466
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 467
    .line 468
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 469
    .line 470
    .line 471
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 472
    .line 473
    .line 474
    move-result v4

    .line 475
    if-eqz v4, :cond_4

    .line 476
    .line 477
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 478
    .line 479
    .line 480
    move-result-object v4

    .line 481
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 482
    .line 483
    .line 484
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 485
    .line 486
    .line 487
    move-result-object v4

    .line 488
    invoke-virtual {v4}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 489
    .line 490
    .line 491
    move-result-object v4

    .line 492
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 493
    .line 494
    .line 495
    :cond_4
    new-instance v4, Landroid/graphics/Path;

    .line 496
    .line 497
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 498
    .line 499
    .line 500
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 501
    .line 502
    int-to-float v5, v5

    .line 503
    add-float/2addr v5, v2

    .line 504
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 505
    .line 506
    add-int/2addr v6, v12

    .line 507
    int-to-float v6, v6

    .line 508
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 509
    .line 510
    .line 511
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 512
    .line 513
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 514
    .line 515
    int-to-float v8, v6

    .line 516
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 517
    .line 518
    add-int v15, v14, v1

    .line 519
    .line 520
    int-to-float v15, v15

    .line 521
    add-int/2addr v6, v1

    .line 522
    int-to-float v6, v6

    .line 523
    add-int/2addr v14, v12

    .line 524
    int-to-float v12, v14

    .line 525
    invoke-virtual {v5, v8, v15, v6, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 526
    .line 527
    .line 528
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 529
    .line 530
    invoke-virtual {v4, v5, v11, v13}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 531
    .line 532
    .line 533
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 534
    .line 535
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 536
    .line 537
    int-to-float v8, v6

    .line 538
    add-float/2addr v8, v2

    .line 539
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 540
    .line 541
    add-int v12, v11, v1

    .line 542
    .line 543
    int-to-float v12, v12

    .line 544
    add-float v12, v12, v16

    .line 545
    .line 546
    add-int/2addr v6, v1

    .line 547
    int-to-float v6, v6

    .line 548
    add-int/2addr v11, v1

    .line 549
    int-to-float v11, v11

    .line 550
    add-float v11, v11, v17

    .line 551
    .line 552
    invoke-virtual {v5, v8, v12, v6, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 553
    .line 554
    .line 555
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 556
    .line 557
    invoke-virtual {v4, v5, v10, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 558
    .line 559
    .line 560
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 561
    .line 562
    .line 563
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 564
    .line 565
    .line 566
    new-instance v4, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 567
    .line 568
    invoke-direct {v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 569
    .line 570
    .line 571
    const/4 v5, 0x0

    .line 572
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 573
    .line 574
    .line 575
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 576
    .line 577
    .line 578
    move-result-object v5

    .line 579
    const-wide v8, -0x402cccccc0000000L    # -0.30000001192092896

    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    const v6, -0x70aaaaab

    .line 585
    .line 586
    .line 587
    if-eqz v5, :cond_5

    .line 588
    .line 589
    invoke-virtual {v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 590
    .line 591
    .line 592
    move-result v11

    .line 593
    if-nez v11, :cond_5

    .line 594
    .line 595
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 596
    .line 597
    .line 598
    move-result-object v11

    .line 599
    invoke-virtual {v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 600
    .line 601
    .line 602
    move-result v12

    .line 603
    invoke-virtual {v11, v12, v8, v9}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 604
    .line 605
    .line 606
    move-result v11

    .line 607
    invoke-virtual {v4, v11}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 608
    .line 609
    .line 610
    goto :goto_1

    .line 611
    :cond_5
    invoke-virtual {v4, v6}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 612
    .line 613
    .line 614
    :goto_1
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 615
    .line 616
    .line 617
    sget-object v11, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 618
    .line 619
    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 620
    .line 621
    .line 622
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 623
    .line 624
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 625
    .line 626
    .line 627
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 628
    .line 629
    .line 630
    move-result v11

    .line 631
    if-eqz v11, :cond_6

    .line 632
    .line 633
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 634
    .line 635
    .line 636
    move-result-object v11

    .line 637
    invoke-virtual {v3, v11}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 638
    .line 639
    .line 640
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 641
    .line 642
    .line 643
    move-result-object v11

    .line 644
    invoke-virtual {v11}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 645
    .line 646
    .line 647
    move-result-object v11

    .line 648
    invoke-virtual {v3, v11}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 649
    .line 650
    .line 651
    :cond_6
    new-instance v11, Landroid/graphics/Path;

    .line 652
    .line 653
    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 654
    .line 655
    .line 656
    iget v12, v0, Landroid/graphics/Rect;->right:I

    .line 657
    .line 658
    int-to-float v12, v12

    .line 659
    sub-float/2addr v12, v2

    .line 660
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 661
    .line 662
    int-to-float v13, v13

    .line 663
    add-float/2addr v13, v2

    .line 664
    invoke-virtual {v11, v12, v13}, Landroid/graphics/Path;->moveTo(FF)V

    .line 665
    .line 666
    .line 667
    sget-object v12, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 668
    .line 669
    iget v13, v0, Landroid/graphics/Rect;->right:I

    .line 670
    .line 671
    sub-int v14, v13, v1

    .line 672
    .line 673
    int-to-float v14, v14

    .line 674
    iget v15, v0, Landroid/graphics/Rect;->top:I

    .line 675
    .line 676
    int-to-float v6, v15

    .line 677
    add-float v6, v6, v16

    .line 678
    .line 679
    int-to-float v13, v13

    .line 680
    sub-float/2addr v13, v2

    .line 681
    int-to-float v2, v15

    .line 682
    add-float v2, v2, v17

    .line 683
    .line 684
    invoke-virtual {v12, v14, v6, v13, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 685
    .line 686
    .line 687
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 688
    .line 689
    invoke-virtual {v11, v2, v10, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 690
    .line 691
    .line 692
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 693
    .line 694
    iget v6, v0, Landroid/graphics/Rect;->right:I

    .line 695
    .line 696
    sub-int v10, v6, v1

    .line 697
    .line 698
    int-to-float v10, v10

    .line 699
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 700
    .line 701
    int-to-float v12, v0

    .line 702
    int-to-float v6, v6

    .line 703
    add-int/2addr v0, v1

    .line 704
    int-to-float v0, v0

    .line 705
    invoke-virtual {v2, v10, v12, v6, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 706
    .line 707
    .line 708
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 709
    .line 710
    const/high16 v1, 0x43870000    # 270.0f

    .line 711
    .line 712
    invoke-virtual {v11, v0, v7, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 713
    .line 714
    .line 715
    invoke-virtual {v11}, Landroid/graphics/Path;->close()V

    .line 716
    .line 717
    .line 718
    invoke-virtual {v3, v11}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 719
    .line 720
    .line 721
    if-eqz v5, :cond_7

    .line 722
    .line 723
    invoke-virtual {v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 724
    .line 725
    .line 726
    move-result v0

    .line 727
    if-nez v0, :cond_7

    .line 728
    .line 729
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 730
    .line 731
    .line 732
    move-result-object v0

    .line 733
    invoke-virtual {v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 734
    .line 735
    .line 736
    move-result v1

    .line 737
    invoke-virtual {v0, v1, v8, v9}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 738
    .line 739
    .line 740
    move-result v0

    .line 741
    invoke-virtual {v4, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 742
    .line 743
    .line 744
    goto :goto_2

    .line 745
    :cond_7
    const v0, -0x70aaaaab

    .line 746
    .line 747
    .line 748
    invoke-virtual {v4, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 749
    .line 750
    .line 751
    :goto_2
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 752
    .line 753
    .line 754
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 755
    .line 756
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 757
    .line 758
    .line 759
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 760
    .line 761
    return-object v0
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getLeftRightRibbon(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 20
    .line 21
    .line 22
    move-result v3

    .line 23
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    if-eqz v4, :cond_4

    .line 28
    .line 29
    const/4 v4, 0x0

    .line 30
    const/4 v5, 0x2

    .line 31
    if-eqz v1, :cond_0

    .line 32
    .line 33
    array-length v6, v1

    .line 34
    const/4 v7, 0x3

    .line 35
    if-ne v6, v7, :cond_0

    .line 36
    .line 37
    int-to-float v6, v3

    .line 38
    aget-object v7, v1, v4

    .line 39
    .line 40
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 41
    .line 42
    .line 43
    move-result v7

    .line 44
    mul-float v7, v7, v6

    .line 45
    .line 46
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 47
    .line 48
    .line 49
    move-result v7

    .line 50
    int-to-float v2, v2

    .line 51
    const/4 v8, 0x1

    .line 52
    aget-object v8, v1, v8

    .line 53
    .line 54
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    .line 55
    .line 56
    .line 57
    move-result v8

    .line 58
    mul-float v2, v2, v8

    .line 59
    .line 60
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 65
    .line 66
    .line 67
    move-result v8

    .line 68
    int-to-float v8, v8

    .line 69
    aget-object v9, v1, v5

    .line 70
    .line 71
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    .line 72
    .line 73
    .line 74
    move-result v9

    .line 75
    mul-float v8, v8, v9

    .line 76
    .line 77
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    .line 78
    .line 79
    .line 80
    move-result v8

    .line 81
    aget-object v1, v1, v5

    .line 82
    .line 83
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 84
    .line 85
    .line 86
    move-result v1

    .line 87
    mul-float v6, v6, v1

    .line 88
    .line 89
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 90
    .line 91
    .line 92
    move-result v1

    .line 93
    goto :goto_0

    .line 94
    :cond_0
    int-to-float v1, v3

    .line 95
    const/high16 v6, 0x3f000000    # 0.5f

    .line 96
    .line 97
    mul-float v7, v1, v6

    .line 98
    .line 99
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 100
    .line 101
    .line 102
    move-result v7

    .line 103
    int-to-float v2, v2

    .line 104
    mul-float v2, v2, v6

    .line 105
    .line 106
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 107
    .line 108
    .line 109
    move-result v2

    .line 110
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 111
    .line 112
    .line 113
    move-result v6

    .line 114
    int-to-float v6, v6

    .line 115
    const v8, 0x3e2aab8a    # 0.16667f

    .line 116
    .line 117
    .line 118
    mul-float v6, v6, v8

    .line 119
    .line 120
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 121
    .line 122
    .line 123
    move-result v6

    .line 124
    mul-float v1, v1, v8

    .line 125
    .line 126
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 127
    .line 128
    .line 129
    move-result v1

    .line 130
    move v8, v6

    .line 131
    :goto_0
    sub-int/2addr v3, v1

    .line 132
    new-instance v6, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 133
    .line 134
    invoke-direct {v6}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 135
    .line 136
    .line 137
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 138
    .line 139
    .line 140
    move-result v9

    .line 141
    if-eqz v9, :cond_1

    .line 142
    .line 143
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 144
    .line 145
    .line 146
    move-result-object v9

    .line 147
    invoke-virtual {v6, v9}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 148
    .line 149
    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 151
    .line 152
    .line 153
    move-result-object v9

    .line 154
    invoke-virtual {v9}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 155
    .line 156
    .line 157
    move-result-object v9

    .line 158
    invoke-virtual {v6, v9}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 159
    .line 160
    .line 161
    :cond_1
    new-instance v9, Landroid/graphics/Path;

    .line 162
    .line 163
    invoke-direct {v9}, Landroid/graphics/Path;-><init>()V

    .line 164
    .line 165
    .line 166
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 167
    .line 168
    int-to-float v10, v10

    .line 169
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 170
    .line 171
    div-int/lit8 v12, v3, 0x2

    .line 172
    .line 173
    add-int/2addr v11, v12

    .line 174
    int-to-float v11, v11

    .line 175
    invoke-virtual {v9, v10, v11}, Landroid/graphics/Path;->moveTo(FF)V

    .line 176
    .line 177
    .line 178
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 179
    .line 180
    add-int/2addr v10, v2

    .line 181
    int-to-float v10, v10

    .line 182
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 183
    .line 184
    int-to-float v11, v11

    .line 185
    invoke-virtual {v9, v10, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 186
    .line 187
    .line 188
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 189
    .line 190
    add-int/2addr v10, v2

    .line 191
    int-to-float v10, v10

    .line 192
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 193
    .line 194
    sub-int v13, v3, v7

    .line 195
    .line 196
    div-int/2addr v13, v5

    .line 197
    add-int/2addr v11, v13

    .line 198
    int-to-float v5, v11

    .line 199
    invoke-virtual {v9, v10, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 200
    .line 201
    .line 202
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 203
    .line 204
    .line 205
    move-result v5

    .line 206
    int-to-float v5, v5

    .line 207
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 208
    .line 209
    add-int/2addr v10, v13

    .line 210
    int-to-float v10, v10

    .line 211
    invoke-virtual {v9, v5, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 212
    .line 213
    .line 214
    new-instance v5, Landroid/graphics/RectF;

    .line 215
    .line 216
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 217
    .line 218
    .line 219
    move-result v10

    .line 220
    div-int/lit8 v8, v8, 0x4

    .line 221
    .line 222
    sub-int/2addr v10, v8

    .line 223
    int-to-float v10, v10

    .line 224
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 225
    .line 226
    add-int/2addr v11, v13

    .line 227
    int-to-float v11, v11

    .line 228
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 229
    .line 230
    .line 231
    move-result v14

    .line 232
    add-int/2addr v14, v8

    .line 233
    int-to-float v14, v14

    .line 234
    iget v15, v0, Landroid/graphics/Rect;->top:I

    .line 235
    .line 236
    add-int/2addr v15, v13

    .line 237
    div-int/lit8 v16, v1, 0x2

    .line 238
    .line 239
    add-int v15, v15, v16

    .line 240
    .line 241
    int-to-float v15, v15

    .line 242
    invoke-direct {v5, v10, v11, v14, v15}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 243
    .line 244
    .line 245
    const/high16 v10, 0x43340000    # 180.0f

    .line 246
    .line 247
    const/high16 v11, 0x43870000    # 270.0f

    .line 248
    .line 249
    invoke-virtual {v9, v5, v11, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 250
    .line 251
    .line 252
    new-instance v5, Landroid/graphics/RectF;

    .line 253
    .line 254
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 255
    .line 256
    .line 257
    move-result v10

    .line 258
    sub-int/2addr v10, v8

    .line 259
    int-to-float v10, v10

    .line 260
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 261
    .line 262
    add-int/2addr v14, v13

    .line 263
    add-int v14, v14, v16

    .line 264
    .line 265
    int-to-float v14, v14

    .line 266
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 267
    .line 268
    .line 269
    move-result v15

    .line 270
    add-int/2addr v15, v8

    .line 271
    int-to-float v15, v15

    .line 272
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 273
    .line 274
    add-int/2addr v4, v13

    .line 275
    add-int/2addr v4, v1

    .line 276
    int-to-float v4, v4

    .line 277
    invoke-direct {v5, v10, v14, v15, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 278
    .line 279
    .line 280
    const/high16 v4, -0x3ccc0000    # -180.0f

    .line 281
    .line 282
    invoke-virtual {v9, v5, v11, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 283
    .line 284
    .line 285
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 286
    .line 287
    sub-int/2addr v5, v2

    .line 288
    int-to-float v5, v5

    .line 289
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    .line 290
    .line 291
    sub-int/2addr v10, v13

    .line 292
    sub-int/2addr v10, v7

    .line 293
    int-to-float v10, v10

    .line 294
    invoke-virtual {v9, v5, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 295
    .line 296
    .line 297
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 298
    .line 299
    sub-int/2addr v5, v2

    .line 300
    int-to-float v5, v5

    .line 301
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    .line 302
    .line 303
    sub-int/2addr v10, v3

    .line 304
    int-to-float v10, v10

    .line 305
    invoke-virtual {v9, v5, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 306
    .line 307
    .line 308
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 309
    .line 310
    int-to-float v5, v5

    .line 311
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    .line 312
    .line 313
    sub-int/2addr v10, v12

    .line 314
    int-to-float v10, v10

    .line 315
    invoke-virtual {v9, v5, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 316
    .line 317
    .line 318
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 319
    .line 320
    sub-int/2addr v5, v2

    .line 321
    int-to-float v5, v5

    .line 322
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    .line 323
    .line 324
    int-to-float v10, v10

    .line 325
    invoke-virtual {v9, v5, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 326
    .line 327
    .line 328
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 329
    .line 330
    sub-int/2addr v5, v2

    .line 331
    int-to-float v5, v5

    .line 332
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    .line 333
    .line 334
    sub-int/2addr v10, v13

    .line 335
    int-to-float v10, v10

    .line 336
    invoke-virtual {v9, v5, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 337
    .line 338
    .line 339
    new-instance v5, Landroid/graphics/RectF;

    .line 340
    .line 341
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 342
    .line 343
    .line 344
    move-result v10

    .line 345
    sub-int/2addr v10, v8

    .line 346
    int-to-float v10, v10

    .line 347
    iget v12, v0, Landroid/graphics/Rect;->bottom:I

    .line 348
    .line 349
    sub-int/2addr v12, v13

    .line 350
    sub-int v12, v12, v16

    .line 351
    .line 352
    int-to-float v12, v12

    .line 353
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 354
    .line 355
    .line 356
    move-result v14

    .line 357
    add-int/2addr v14, v8

    .line 358
    int-to-float v14, v14

    .line 359
    iget v15, v0, Landroid/graphics/Rect;->bottom:I

    .line 360
    .line 361
    sub-int/2addr v15, v13

    .line 362
    int-to-float v15, v15

    .line 363
    invoke-direct {v5, v10, v12, v14, v15}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 364
    .line 365
    .line 366
    const/high16 v10, 0x42b40000    # 90.0f

    .line 367
    .line 368
    invoke-virtual {v9, v5, v10, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 369
    .line 370
    .line 371
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 372
    .line 373
    .line 374
    move-result v5

    .line 375
    sub-int/2addr v5, v8

    .line 376
    int-to-float v5, v5

    .line 377
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 378
    .line 379
    add-int/2addr v10, v13

    .line 380
    add-int/2addr v10, v7

    .line 381
    int-to-float v10, v10

    .line 382
    invoke-virtual {v9, v5, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 383
    .line 384
    .line 385
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 386
    .line 387
    add-int/2addr v5, v2

    .line 388
    int-to-float v5, v5

    .line 389
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 390
    .line 391
    add-int/2addr v10, v13

    .line 392
    add-int/2addr v10, v7

    .line 393
    int-to-float v7, v10

    .line 394
    invoke-virtual {v9, v5, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 395
    .line 396
    .line 397
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 398
    .line 399
    add-int/2addr v5, v2

    .line 400
    int-to-float v2, v5

    .line 401
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 402
    .line 403
    add-int/2addr v5, v3

    .line 404
    int-to-float v3, v5

    .line 405
    invoke-virtual {v9, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 406
    .line 407
    .line 408
    invoke-virtual {v9}, Landroid/graphics/Path;->close()V

    .line 409
    .line 410
    .line 411
    invoke-virtual {v6, v9}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 412
    .line 413
    .line 414
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 415
    .line 416
    .line 417
    move-result-object v2

    .line 418
    invoke-virtual {v6, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 419
    .line 420
    .line 421
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 422
    .line 423
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 424
    .line 425
    .line 426
    new-instance v2, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 427
    .line 428
    invoke-direct {v2}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 429
    .line 430
    .line 431
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 432
    .line 433
    .line 434
    move-result v3

    .line 435
    if-eqz v3, :cond_2

    .line 436
    .line 437
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 438
    .line 439
    .line 440
    move-result-object v3

    .line 441
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 442
    .line 443
    .line 444
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 445
    .line 446
    .line 447
    move-result-object v3

    .line 448
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 449
    .line 450
    .line 451
    move-result-object v3

    .line 452
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 453
    .line 454
    .line 455
    :cond_2
    new-instance v3, Landroid/graphics/Path;

    .line 456
    .line 457
    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 458
    .line 459
    .line 460
    new-instance v5, Landroid/graphics/RectF;

    .line 461
    .line 462
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 463
    .line 464
    .line 465
    move-result v6

    .line 466
    sub-int/2addr v6, v8

    .line 467
    int-to-float v6, v6

    .line 468
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 469
    .line 470
    add-int/2addr v7, v13

    .line 471
    add-int v7, v7, v16

    .line 472
    .line 473
    int-to-float v7, v7

    .line 474
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 475
    .line 476
    .line 477
    move-result v9

    .line 478
    add-int/2addr v9, v8

    .line 479
    int-to-float v8, v9

    .line 480
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 481
    .line 482
    add-int/2addr v0, v13

    .line 483
    add-int/2addr v0, v1

    .line 484
    int-to-float v0, v0

    .line 485
    invoke-direct {v5, v6, v7, v8, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 486
    .line 487
    .line 488
    invoke-virtual {v3, v5, v11, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 489
    .line 490
    .line 491
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    .line 492
    .line 493
    .line 494
    new-instance v0, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 495
    .line 496
    invoke-direct {v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 497
    .line 498
    .line 499
    const/4 v1, 0x0

    .line 500
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 501
    .line 502
    .line 503
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 504
    .line 505
    .line 506
    move-result-object v1

    .line 507
    if-eqz v1, :cond_3

    .line 508
    .line 509
    invoke-virtual {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 510
    .line 511
    .line 512
    move-result v4

    .line 513
    if-nez v4, :cond_3

    .line 514
    .line 515
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 516
    .line 517
    .line 518
    move-result-object v4

    .line 519
    invoke-virtual {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 520
    .line 521
    .line 522
    move-result v1

    .line 523
    const-wide v5, -0x402cccccc0000000L    # -0.30000001192092896

    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    invoke-virtual {v4, v1, v5, v6}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 529
    .line 530
    .line 531
    move-result v1

    .line 532
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 533
    .line 534
    .line 535
    goto :goto_1

    .line 536
    :cond_3
    const v1, -0x70aaaaab

    .line 537
    .line 538
    .line 539
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 540
    .line 541
    .line 542
    :goto_1
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 543
    .line 544
    .line 545
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 546
    .line 547
    .line 548
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 549
    .line 550
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 551
    .line 552
    .line 553
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 554
    .line 555
    return-object v0

    .line 556
    :cond_4
    const/4 v0, 0x0

    .line 557
    return-object v0
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getRibbon2Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    div-int/lit8 v4, v2, 0x8

    .line 16
    .line 17
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 18
    .line 19
    .line 20
    move-result v5

    .line 21
    const/high16 v6, 0x3f000000    # 0.5f

    .line 22
    .line 23
    const/4 v7, 0x0

    .line 24
    const/4 v8, 0x1

    .line 25
    const/4 v9, 0x2

    .line 26
    if-eqz v5, :cond_1

    .line 27
    .line 28
    if-eqz v1, :cond_0

    .line 29
    .line 30
    array-length v5, v1

    .line 31
    if-ne v5, v9, :cond_0

    .line 32
    .line 33
    int-to-float v5, v3

    .line 34
    aget-object v6, v1, v7

    .line 35
    .line 36
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 37
    .line 38
    .line 39
    move-result v6

    .line 40
    mul-float v5, v5, v6

    .line 41
    .line 42
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 43
    .line 44
    .line 45
    move-result v5

    .line 46
    div-int/2addr v2, v9

    .line 47
    int-to-float v2, v2

    .line 48
    aget-object v1, v1, v8

    .line 49
    .line 50
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    mul-float v2, v2, v1

    .line 55
    .line 56
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    goto :goto_2

    .line 61
    :cond_0
    int-to-float v1, v3

    .line 62
    const v5, 0x3e2aab8a    # 0.16667f

    .line 63
    .line 64
    .line 65
    mul-float v1, v1, v5

    .line 66
    .line 67
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 68
    .line 69
    .line 70
    move-result v5

    .line 71
    div-int/2addr v2, v9

    .line 72
    int-to-float v1, v2

    .line 73
    mul-float v1, v1, v6

    .line 74
    .line 75
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    goto :goto_2

    .line 80
    :cond_1
    const/high16 v5, 0x3e800000    # 0.25f

    .line 81
    .line 82
    const/high16 v10, 0x3e000000    # 0.125f

    .line 83
    .line 84
    if-eqz v1, :cond_4

    .line 85
    .line 86
    array-length v11, v1

    .line 87
    if-lt v11, v8, :cond_4

    .line 88
    .line 89
    aget-object v11, v1, v7

    .line 90
    .line 91
    if-eqz v11, :cond_2

    .line 92
    .line 93
    int-to-float v2, v2

    .line 94
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    .line 95
    .line 96
    .line 97
    move-result v5

    .line 98
    sub-float/2addr v6, v5

    .line 99
    mul-float v2, v2, v6

    .line 100
    .line 101
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 102
    .line 103
    .line 104
    move-result v2

    .line 105
    goto :goto_0

    .line 106
    :cond_2
    int-to-float v2, v2

    .line 107
    mul-float v2, v2, v5

    .line 108
    .line 109
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 110
    .line 111
    .line 112
    move-result v2

    .line 113
    :goto_0
    array-length v5, v1

    .line 114
    if-lt v5, v9, :cond_3

    .line 115
    .line 116
    aget-object v1, v1, v8

    .line 117
    .line 118
    if-eqz v1, :cond_3

    .line 119
    .line 120
    int-to-float v5, v3

    .line 121
    const/high16 v6, 0x3f800000    # 1.0f

    .line 122
    .line 123
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 124
    .line 125
    .line 126
    move-result v1

    .line 127
    sub-float/2addr v6, v1

    .line 128
    mul-float v5, v5, v6

    .line 129
    .line 130
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 131
    .line 132
    .line 133
    move-result v5

    .line 134
    goto :goto_1

    .line 135
    :cond_3
    int-to-float v1, v3

    .line 136
    mul-float v1, v1, v10

    .line 137
    .line 138
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 139
    .line 140
    .line 141
    move-result v5

    .line 142
    goto :goto_1

    .line 143
    :cond_4
    int-to-float v1, v3

    .line 144
    mul-float v1, v1, v10

    .line 145
    .line 146
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 147
    .line 148
    .line 149
    move-result v1

    .line 150
    int-to-float v2, v2

    .line 151
    mul-float v2, v2, v5

    .line 152
    .line 153
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 154
    .line 155
    .line 156
    move-result v2

    .line 157
    move v5, v1

    .line 158
    :goto_1
    move v1, v2

    .line 159
    :goto_2
    div-int/lit8 v2, v4, 0x4

    .line 160
    .line 161
    int-to-float v2, v2

    .line 162
    div-int/lit8 v6, v5, 0x4

    .line 163
    .line 164
    int-to-float v6, v6

    .line 165
    new-instance v8, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 166
    .line 167
    invoke-direct {v8}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 168
    .line 169
    .line 170
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 171
    .line 172
    .line 173
    move-result v10

    .line 174
    if-eqz v10, :cond_5

    .line 175
    .line 176
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 177
    .line 178
    .line 179
    move-result-object v10

    .line 180
    invoke-virtual {v8, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 181
    .line 182
    .line 183
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 184
    .line 185
    .line 186
    move-result-object v10

    .line 187
    invoke-virtual {v10}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 188
    .line 189
    .line 190
    move-result-object v10

    .line 191
    invoke-virtual {v8, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 192
    .line 193
    .line 194
    :cond_5
    new-instance v10, Landroid/graphics/Path;

    .line 195
    .line 196
    invoke-direct {v10}, Landroid/graphics/Path;-><init>()V

    .line 197
    .line 198
    .line 199
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 200
    .line 201
    int-to-float v11, v11

    .line 202
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 203
    .line 204
    add-int/2addr v12, v5

    .line 205
    int-to-float v12, v12

    .line 206
    invoke-virtual {v10, v11, v12}, Landroid/graphics/Path;->moveTo(FF)V

    .line 207
    .line 208
    .line 209
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 210
    .line 211
    add-int/2addr v11, v4

    .line 212
    int-to-float v11, v11

    .line 213
    iget v12, v0, Landroid/graphics/Rect;->bottom:I

    .line 214
    .line 215
    sub-int/2addr v3, v5

    .line 216
    div-int/2addr v3, v9

    .line 217
    sub-int/2addr v12, v3

    .line 218
    int-to-float v9, v12

    .line 219
    invoke-virtual {v10, v11, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 220
    .line 221
    .line 222
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 223
    .line 224
    int-to-float v9, v9

    .line 225
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 226
    .line 227
    int-to-float v11, v11

    .line 228
    invoke-virtual {v10, v9, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 229
    .line 230
    .line 231
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 232
    .line 233
    .line 234
    move-result v9

    .line 235
    sub-int/2addr v9, v1

    .line 236
    int-to-float v9, v9

    .line 237
    const/high16 v11, 0x40400000    # 3.0f

    .line 238
    .line 239
    mul-float v12, v2, v11

    .line 240
    .line 241
    add-float/2addr v9, v12

    .line 242
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 243
    .line 244
    int-to-float v13, v13

    .line 245
    invoke-virtual {v10, v9, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 246
    .line 247
    .line 248
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 249
    .line 250
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 251
    .line 252
    .line 253
    move-result v13

    .line 254
    sub-int/2addr v13, v1

    .line 255
    int-to-float v13, v13

    .line 256
    const/high16 v14, 0x40000000    # 2.0f

    .line 257
    .line 258
    mul-float v15, v2, v14

    .line 259
    .line 260
    add-float/2addr v13, v15

    .line 261
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    .line 262
    .line 263
    int-to-float v7, v7

    .line 264
    mul-float v14, v14, v6

    .line 265
    .line 266
    sub-float/2addr v7, v14

    .line 267
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 268
    .line 269
    .line 270
    move-result v17

    .line 271
    sub-int v17, v17, v1

    .line 272
    .line 273
    add-int v11, v17, v4

    .line 274
    .line 275
    int-to-float v11, v11

    .line 276
    move/from16 v17, v12

    .line 277
    .line 278
    iget v12, v0, Landroid/graphics/Rect;->bottom:I

    .line 279
    .line 280
    int-to-float v12, v12

    .line 281
    invoke-virtual {v9, v13, v7, v11, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 282
    .line 283
    .line 284
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 285
    .line 286
    const/high16 v9, 0x42b40000    # 90.0f

    .line 287
    .line 288
    const/high16 v11, -0x3ccc0000    # -180.0f

    .line 289
    .line 290
    invoke-virtual {v10, v7, v9, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 291
    .line 292
    .line 293
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 294
    .line 295
    .line 296
    move-result v7

    .line 297
    sub-int/2addr v7, v1

    .line 298
    int-to-float v7, v7

    .line 299
    add-float/2addr v7, v2

    .line 300
    iget v12, v0, Landroid/graphics/Rect;->bottom:I

    .line 301
    .line 302
    int-to-float v12, v12

    .line 303
    sub-float/2addr v12, v14

    .line 304
    invoke-virtual {v10, v7, v12}, Landroid/graphics/Path;->lineTo(FF)V

    .line 305
    .line 306
    .line 307
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 308
    .line 309
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 310
    .line 311
    .line 312
    move-result v12

    .line 313
    sub-int/2addr v12, v1

    .line 314
    int-to-float v12, v12

    .line 315
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 316
    .line 317
    int-to-float v13, v13

    .line 318
    const/high16 v18, 0x40800000    # 4.0f

    .line 319
    .line 320
    mul-float v19, v6, v18

    .line 321
    .line 322
    sub-float v13, v13, v19

    .line 323
    .line 324
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 325
    .line 326
    .line 327
    move-result v20

    .line 328
    sub-int v11, v20, v1

    .line 329
    .line 330
    int-to-float v11, v11

    .line 331
    add-float/2addr v11, v15

    .line 332
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 333
    .line 334
    int-to-float v9, v9

    .line 335
    sub-float/2addr v9, v14

    .line 336
    invoke-virtual {v7, v12, v13, v11, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 337
    .line 338
    .line 339
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 340
    .line 341
    const/high16 v9, 0x42b40000    # 90.0f

    .line 342
    .line 343
    invoke-virtual {v10, v7, v9, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 344
    .line 345
    .line 346
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 347
    .line 348
    .line 349
    move-result v7

    .line 350
    sub-int/2addr v7, v1

    .line 351
    int-to-float v7, v7

    .line 352
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 353
    .line 354
    add-int/2addr v9, v5

    .line 355
    int-to-float v9, v9

    .line 356
    invoke-virtual {v10, v7, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 357
    .line 358
    .line 359
    invoke-virtual {v10}, Landroid/graphics/Path;->close()V

    .line 360
    .line 361
    .line 362
    invoke-virtual {v8, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 363
    .line 364
    .line 365
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 366
    .line 367
    .line 368
    move-result-object v7

    .line 369
    invoke-virtual {v8, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 370
    .line 371
    .line 372
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 373
    .line 374
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 375
    .line 376
    .line 377
    new-instance v7, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 378
    .line 379
    invoke-direct {v7}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 380
    .line 381
    .line 382
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 383
    .line 384
    .line 385
    move-result v8

    .line 386
    if-eqz v8, :cond_6

    .line 387
    .line 388
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 389
    .line 390
    .line 391
    move-result-object v8

    .line 392
    invoke-virtual {v7, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 393
    .line 394
    .line 395
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 396
    .line 397
    .line 398
    move-result-object v8

    .line 399
    invoke-virtual {v8}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 400
    .line 401
    .line 402
    move-result-object v8

    .line 403
    invoke-virtual {v7, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 404
    .line 405
    .line 406
    :cond_6
    new-instance v8, Landroid/graphics/Path;

    .line 407
    .line 408
    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    .line 409
    .line 410
    .line 411
    iget v9, v0, Landroid/graphics/Rect;->right:I

    .line 412
    .line 413
    int-to-float v9, v9

    .line 414
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 415
    .line 416
    add-int/2addr v10, v5

    .line 417
    int-to-float v10, v10

    .line 418
    invoke-virtual {v8, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 419
    .line 420
    .line 421
    iget v9, v0, Landroid/graphics/Rect;->right:I

    .line 422
    .line 423
    sub-int/2addr v9, v4

    .line 424
    int-to-float v9, v9

    .line 425
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    .line 426
    .line 427
    sub-int/2addr v10, v3

    .line 428
    int-to-float v3, v10

    .line 429
    invoke-virtual {v8, v9, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 430
    .line 431
    .line 432
    iget v3, v0, Landroid/graphics/Rect;->right:I

    .line 433
    .line 434
    int-to-float v3, v3

    .line 435
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 436
    .line 437
    int-to-float v9, v9

    .line 438
    invoke-virtual {v8, v3, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 439
    .line 440
    .line 441
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 442
    .line 443
    .line 444
    move-result v3

    .line 445
    add-int/2addr v3, v1

    .line 446
    int-to-float v3, v3

    .line 447
    sub-float v3, v3, v17

    .line 448
    .line 449
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 450
    .line 451
    int-to-float v9, v9

    .line 452
    invoke-virtual {v8, v3, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 453
    .line 454
    .line 455
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 456
    .line 457
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 458
    .line 459
    .line 460
    move-result v9

    .line 461
    add-int/2addr v9, v1

    .line 462
    int-to-float v9, v9

    .line 463
    mul-float v18, v18, v2

    .line 464
    .line 465
    sub-float v9, v9, v18

    .line 466
    .line 467
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    .line 468
    .line 469
    int-to-float v10, v10

    .line 470
    sub-float/2addr v10, v14

    .line 471
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 472
    .line 473
    .line 474
    move-result v11

    .line 475
    add-int/2addr v11, v1

    .line 476
    int-to-float v11, v11

    .line 477
    sub-float/2addr v11, v15

    .line 478
    iget v12, v0, Landroid/graphics/Rect;->bottom:I

    .line 479
    .line 480
    int-to-float v12, v12

    .line 481
    invoke-virtual {v3, v9, v10, v11, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 482
    .line 483
    .line 484
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 485
    .line 486
    const/high16 v9, 0x43340000    # 180.0f

    .line 487
    .line 488
    const/high16 v10, 0x42b40000    # 90.0f

    .line 489
    .line 490
    invoke-virtual {v8, v3, v10, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 491
    .line 492
    .line 493
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 494
    .line 495
    .line 496
    move-result v3

    .line 497
    add-int/2addr v3, v1

    .line 498
    int-to-float v3, v3

    .line 499
    sub-float/2addr v3, v2

    .line 500
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    .line 501
    .line 502
    int-to-float v10, v10

    .line 503
    sub-float/2addr v10, v14

    .line 504
    invoke-virtual {v8, v3, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 505
    .line 506
    .line 507
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 508
    .line 509
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 510
    .line 511
    .line 512
    move-result v10

    .line 513
    add-int/2addr v10, v1

    .line 514
    int-to-float v10, v10

    .line 515
    sub-float/2addr v10, v15

    .line 516
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 517
    .line 518
    int-to-float v11, v11

    .line 519
    sub-float v11, v11, v19

    .line 520
    .line 521
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 522
    .line 523
    .line 524
    move-result v12

    .line 525
    add-int/2addr v12, v1

    .line 526
    int-to-float v12, v12

    .line 527
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 528
    .line 529
    int-to-float v13, v13

    .line 530
    sub-float/2addr v13, v14

    .line 531
    invoke-virtual {v3, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 532
    .line 533
    .line 534
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 535
    .line 536
    const/high16 v10, -0x3d4c0000    # -90.0f

    .line 537
    .line 538
    const/high16 v11, 0x42b40000    # 90.0f

    .line 539
    .line 540
    invoke-virtual {v8, v3, v11, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 541
    .line 542
    .line 543
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 544
    .line 545
    .line 546
    move-result v3

    .line 547
    add-int/2addr v3, v1

    .line 548
    int-to-float v3, v3

    .line 549
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 550
    .line 551
    add-int/2addr v11, v5

    .line 552
    int-to-float v5, v11

    .line 553
    invoke-virtual {v8, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 554
    .line 555
    .line 556
    invoke-virtual {v8}, Landroid/graphics/Path;->close()V

    .line 557
    .line 558
    .line 559
    invoke-virtual {v7, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 560
    .line 561
    .line 562
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 563
    .line 564
    .line 565
    move-result-object v3

    .line 566
    invoke-virtual {v7, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 567
    .line 568
    .line 569
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 570
    .line 571
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 572
    .line 573
    .line 574
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 575
    .line 576
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 577
    .line 578
    .line 579
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 580
    .line 581
    .line 582
    move-result v5

    .line 583
    if-eqz v5, :cond_7

    .line 584
    .line 585
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 586
    .line 587
    .line 588
    move-result-object v5

    .line 589
    invoke-virtual {v3, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 590
    .line 591
    .line 592
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 593
    .line 594
    .line 595
    move-result-object v5

    .line 596
    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 597
    .line 598
    .line 599
    move-result-object v5

    .line 600
    invoke-virtual {v3, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 601
    .line 602
    .line 603
    :cond_7
    new-instance v5, Landroid/graphics/Path;

    .line 604
    .line 605
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 606
    .line 607
    .line 608
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 609
    .line 610
    .line 611
    move-result v7

    .line 612
    sub-int/2addr v7, v1

    .line 613
    int-to-float v7, v7

    .line 614
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 615
    .line 616
    int-to-float v8, v8

    .line 617
    add-float/2addr v8, v6

    .line 618
    invoke-virtual {v5, v7, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 619
    .line 620
    .line 621
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 622
    .line 623
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 624
    .line 625
    .line 626
    move-result v8

    .line 627
    sub-int/2addr v8, v1

    .line 628
    int-to-float v8, v8

    .line 629
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 630
    .line 631
    int-to-float v11, v11

    .line 632
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 633
    .line 634
    .line 635
    move-result v12

    .line 636
    sub-int/2addr v12, v1

    .line 637
    int-to-float v12, v12

    .line 638
    add-float/2addr v12, v15

    .line 639
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 640
    .line 641
    int-to-float v13, v13

    .line 642
    add-float/2addr v13, v14

    .line 643
    invoke-virtual {v7, v8, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 644
    .line 645
    .line 646
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 647
    .line 648
    const/high16 v8, 0x42b40000    # 90.0f

    .line 649
    .line 650
    invoke-virtual {v5, v7, v9, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 651
    .line 652
    .line 653
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 654
    .line 655
    .line 656
    move-result v7

    .line 657
    add-int/2addr v7, v1

    .line 658
    int-to-float v7, v7

    .line 659
    sub-float/2addr v7, v2

    .line 660
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 661
    .line 662
    int-to-float v8, v8

    .line 663
    invoke-virtual {v5, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 664
    .line 665
    .line 666
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 667
    .line 668
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 669
    .line 670
    .line 671
    move-result v8

    .line 672
    add-int/2addr v8, v1

    .line 673
    int-to-float v8, v8

    .line 674
    sub-float/2addr v8, v15

    .line 675
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 676
    .line 677
    int-to-float v11, v11

    .line 678
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 679
    .line 680
    .line 681
    move-result v12

    .line 682
    add-int/2addr v12, v1

    .line 683
    int-to-float v12, v12

    .line 684
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 685
    .line 686
    int-to-float v13, v13

    .line 687
    add-float/2addr v13, v14

    .line 688
    invoke-virtual {v7, v8, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 689
    .line 690
    .line 691
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 692
    .line 693
    const/high16 v8, 0x43870000    # 270.0f

    .line 694
    .line 695
    const/high16 v11, 0x42b40000    # 90.0f

    .line 696
    .line 697
    invoke-virtual {v5, v7, v8, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 698
    .line 699
    .line 700
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 701
    .line 702
    .line 703
    move-result v7

    .line 704
    add-int/2addr v7, v1

    .line 705
    int-to-float v7, v7

    .line 706
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 707
    .line 708
    int-to-float v11, v11

    .line 709
    const/high16 v12, 0x40400000    # 3.0f

    .line 710
    .line 711
    mul-float v6, v6, v12

    .line 712
    .line 713
    sub-float/2addr v11, v6

    .line 714
    invoke-virtual {v5, v7, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 715
    .line 716
    .line 717
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 718
    .line 719
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 720
    .line 721
    .line 722
    move-result v7

    .line 723
    add-int/2addr v7, v1

    .line 724
    int-to-float v7, v7

    .line 725
    sub-float/2addr v7, v15

    .line 726
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 727
    .line 728
    int-to-float v11, v11

    .line 729
    sub-float v11, v11, v19

    .line 730
    .line 731
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 732
    .line 733
    .line 734
    move-result v12

    .line 735
    add-int/2addr v12, v1

    .line 736
    int-to-float v12, v12

    .line 737
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 738
    .line 739
    int-to-float v13, v13

    .line 740
    sub-float/2addr v13, v14

    .line 741
    invoke-virtual {v6, v7, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 742
    .line 743
    .line 744
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 745
    .line 746
    const/4 v7, 0x0

    .line 747
    invoke-virtual {v5, v6, v7, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 748
    .line 749
    .line 750
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 751
    .line 752
    .line 753
    move-result v6

    .line 754
    sub-int/2addr v6, v1

    .line 755
    int-to-float v6, v6

    .line 756
    add-float/2addr v6, v2

    .line 757
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    .line 758
    .line 759
    int-to-float v7, v7

    .line 760
    sub-float v7, v7, v19

    .line 761
    .line 762
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 763
    .line 764
    .line 765
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 766
    .line 767
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 768
    .line 769
    .line 770
    move-result v7

    .line 771
    sub-int/2addr v7, v1

    .line 772
    int-to-float v7, v7

    .line 773
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 774
    .line 775
    int-to-float v11, v11

    .line 776
    sub-float v11, v11, v19

    .line 777
    .line 778
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 779
    .line 780
    .line 781
    move-result v12

    .line 782
    sub-int/2addr v12, v1

    .line 783
    int-to-float v12, v12

    .line 784
    add-float/2addr v12, v15

    .line 785
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 786
    .line 787
    int-to-float v13, v13

    .line 788
    sub-float/2addr v13, v14

    .line 789
    invoke-virtual {v6, v7, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 790
    .line 791
    .line 792
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 793
    .line 794
    invoke-virtual {v5, v6, v8, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 795
    .line 796
    .line 797
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 798
    .line 799
    .line 800
    invoke-virtual {v3, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 801
    .line 802
    .line 803
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 804
    .line 805
    .line 806
    move-result-object v5

    .line 807
    invoke-virtual {v3, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 808
    .line 809
    .line 810
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 811
    .line 812
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 813
    .line 814
    .line 815
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 816
    .line 817
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 818
    .line 819
    .line 820
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 821
    .line 822
    .line 823
    move-result v5

    .line 824
    if-eqz v5, :cond_8

    .line 825
    .line 826
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 827
    .line 828
    .line 829
    move-result-object v5

    .line 830
    invoke-virtual {v3, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 831
    .line 832
    .line 833
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 834
    .line 835
    .line 836
    move-result-object v5

    .line 837
    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 838
    .line 839
    .line 840
    move-result-object v5

    .line 841
    invoke-virtual {v3, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 842
    .line 843
    .line 844
    :cond_8
    new-instance v5, Landroid/graphics/Path;

    .line 845
    .line 846
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 847
    .line 848
    .line 849
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 850
    .line 851
    .line 852
    move-result v6

    .line 853
    sub-int/2addr v6, v1

    .line 854
    add-int/2addr v6, v4

    .line 855
    int-to-float v6, v6

    .line 856
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    .line 857
    .line 858
    int-to-float v7, v7

    .line 859
    sub-float v7, v7, v19

    .line 860
    .line 861
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 862
    .line 863
    .line 864
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 865
    .line 866
    .line 867
    move-result v6

    .line 868
    sub-int/2addr v6, v1

    .line 869
    int-to-float v6, v6

    .line 870
    add-float/2addr v6, v2

    .line 871
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    .line 872
    .line 873
    int-to-float v7, v7

    .line 874
    sub-float v7, v7, v19

    .line 875
    .line 876
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 877
    .line 878
    .line 879
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 880
    .line 881
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 882
    .line 883
    .line 884
    move-result v7

    .line 885
    sub-int/2addr v7, v1

    .line 886
    int-to-float v7, v7

    .line 887
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 888
    .line 889
    int-to-float v11, v11

    .line 890
    sub-float v11, v11, v19

    .line 891
    .line 892
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 893
    .line 894
    .line 895
    move-result v12

    .line 896
    sub-int/2addr v12, v1

    .line 897
    int-to-float v12, v12

    .line 898
    add-float/2addr v12, v15

    .line 899
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 900
    .line 901
    int-to-float v13, v13

    .line 902
    sub-float/2addr v13, v14

    .line 903
    invoke-virtual {v6, v7, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 904
    .line 905
    .line 906
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 907
    .line 908
    const/high16 v7, -0x3ccc0000    # -180.0f

    .line 909
    .line 910
    invoke-virtual {v5, v6, v8, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 911
    .line 912
    .line 913
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 914
    .line 915
    .line 916
    move-result v6

    .line 917
    sub-int/2addr v6, v1

    .line 918
    int-to-float v6, v6

    .line 919
    add-float v6, v6, v17

    .line 920
    .line 921
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    .line 922
    .line 923
    int-to-float v7, v7

    .line 924
    sub-float/2addr v7, v14

    .line 925
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 926
    .line 927
    .line 928
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 929
    .line 930
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 931
    .line 932
    .line 933
    move-result v7

    .line 934
    sub-int/2addr v7, v1

    .line 935
    int-to-float v7, v7

    .line 936
    add-float/2addr v7, v15

    .line 937
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 938
    .line 939
    int-to-float v11, v11

    .line 940
    sub-float/2addr v11, v14

    .line 941
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 942
    .line 943
    .line 944
    move-result v12

    .line 945
    sub-int/2addr v12, v1

    .line 946
    int-to-float v12, v12

    .line 947
    add-float v12, v12, v18

    .line 948
    .line 949
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 950
    .line 951
    int-to-float v13, v13

    .line 952
    invoke-virtual {v6, v7, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 953
    .line 954
    .line 955
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 956
    .line 957
    const/high16 v7, 0x42b40000    # 90.0f

    .line 958
    .line 959
    invoke-virtual {v5, v6, v8, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 960
    .line 961
    .line 962
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 963
    .line 964
    .line 965
    new-instance v6, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 966
    .line 967
    invoke-direct {v6}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 968
    .line 969
    .line 970
    const/4 v7, 0x0

    .line 971
    invoke-virtual {v6, v7}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 972
    .line 973
    .line 974
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 975
    .line 976
    .line 977
    move-result-object v7

    .line 978
    const-wide v11, -0x402cccccc0000000L    # -0.30000001192092896

    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    const v13, -0x70aaaaab

    .line 984
    .line 985
    .line 986
    if-eqz v7, :cond_9

    .line 987
    .line 988
    invoke-virtual {v7}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 989
    .line 990
    .line 991
    move-result v16

    .line 992
    if-nez v16, :cond_9

    .line 993
    .line 994
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 995
    .line 996
    .line 997
    move-result-object v10

    .line 998
    invoke-virtual {v7}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 999
    .line 1000
    .line 1001
    move-result v8

    .line 1002
    invoke-virtual {v10, v8, v11, v12}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 1003
    .line 1004
    .line 1005
    move-result v8

    .line 1006
    invoke-virtual {v6, v8}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 1007
    .line 1008
    .line 1009
    goto :goto_3

    .line 1010
    :cond_9
    invoke-virtual {v6, v13}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 1011
    .line 1012
    .line 1013
    :goto_3
    invoke-virtual {v3, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 1014
    .line 1015
    .line 1016
    invoke-virtual {v3, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 1017
    .line 1018
    .line 1019
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 1020
    .line 1021
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1022
    .line 1023
    .line 1024
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 1025
    .line 1026
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 1027
    .line 1028
    .line 1029
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 1030
    .line 1031
    .line 1032
    move-result v5

    .line 1033
    if-eqz v5, :cond_a

    .line 1034
    .line 1035
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 1036
    .line 1037
    .line 1038
    move-result-object v5

    .line 1039
    invoke-virtual {v3, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 1040
    .line 1041
    .line 1042
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 1043
    .line 1044
    .line 1045
    move-result-object v5

    .line 1046
    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 1047
    .line 1048
    .line 1049
    move-result-object v5

    .line 1050
    invoke-virtual {v3, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 1051
    .line 1052
    .line 1053
    :cond_a
    new-instance v5, Landroid/graphics/Path;

    .line 1054
    .line 1055
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 1056
    .line 1057
    .line 1058
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 1059
    .line 1060
    .line 1061
    move-result v8

    .line 1062
    add-int/2addr v8, v1

    .line 1063
    sub-int/2addr v8, v4

    .line 1064
    int-to-float v4, v8

    .line 1065
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    .line 1066
    .line 1067
    int-to-float v8, v8

    .line 1068
    sub-float v8, v8, v19

    .line 1069
    .line 1070
    invoke-virtual {v5, v4, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1071
    .line 1072
    .line 1073
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 1074
    .line 1075
    .line 1076
    move-result v4

    .line 1077
    add-int/2addr v4, v1

    .line 1078
    int-to-float v4, v4

    .line 1079
    sub-float/2addr v4, v2

    .line 1080
    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 1081
    .line 1082
    int-to-float v2, v2

    .line 1083
    sub-float v2, v2, v19

    .line 1084
    .line 1085
    invoke-virtual {v5, v4, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1086
    .line 1087
    .line 1088
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 1089
    .line 1090
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 1091
    .line 1092
    .line 1093
    move-result v4

    .line 1094
    add-int/2addr v4, v1

    .line 1095
    int-to-float v4, v4

    .line 1096
    sub-float/2addr v4, v15

    .line 1097
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    .line 1098
    .line 1099
    int-to-float v8, v8

    .line 1100
    sub-float v8, v8, v19

    .line 1101
    .line 1102
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 1103
    .line 1104
    .line 1105
    move-result v10

    .line 1106
    add-int/2addr v10, v1

    .line 1107
    int-to-float v10, v10

    .line 1108
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 1109
    .line 1110
    int-to-float v13, v13

    .line 1111
    sub-float/2addr v13, v14

    .line 1112
    invoke-virtual {v2, v4, v8, v10, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1113
    .line 1114
    .line 1115
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 1116
    .line 1117
    const/high16 v4, 0x43870000    # 270.0f

    .line 1118
    .line 1119
    invoke-virtual {v5, v2, v4, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1120
    .line 1121
    .line 1122
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 1123
    .line 1124
    .line 1125
    move-result v2

    .line 1126
    add-int/2addr v2, v1

    .line 1127
    int-to-float v2, v2

    .line 1128
    sub-float v2, v2, v17

    .line 1129
    .line 1130
    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    .line 1131
    .line 1132
    int-to-float v4, v4

    .line 1133
    sub-float/2addr v4, v14

    .line 1134
    invoke-virtual {v5, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1135
    .line 1136
    .line 1137
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 1138
    .line 1139
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 1140
    .line 1141
    .line 1142
    move-result v4

    .line 1143
    add-int/2addr v4, v1

    .line 1144
    int-to-float v4, v4

    .line 1145
    sub-float v4, v4, v18

    .line 1146
    .line 1147
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    .line 1148
    .line 1149
    int-to-float v8, v8

    .line 1150
    sub-float/2addr v8, v14

    .line 1151
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 1152
    .line 1153
    .line 1154
    move-result v9

    .line 1155
    add-int/2addr v9, v1

    .line 1156
    int-to-float v1, v9

    .line 1157
    sub-float/2addr v1, v15

    .line 1158
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 1159
    .line 1160
    int-to-float v0, v0

    .line 1161
    invoke-virtual {v2, v4, v8, v1, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1162
    .line 1163
    .line 1164
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 1165
    .line 1166
    const/high16 v1, -0x3d4c0000    # -90.0f

    .line 1167
    .line 1168
    const/high16 v2, 0x43870000    # 270.0f

    .line 1169
    .line 1170
    invoke-virtual {v5, v0, v2, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1171
    .line 1172
    .line 1173
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 1174
    .line 1175
    .line 1176
    if-eqz v7, :cond_b

    .line 1177
    .line 1178
    invoke-virtual {v7}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 1179
    .line 1180
    .line 1181
    move-result v0

    .line 1182
    if-nez v0, :cond_b

    .line 1183
    .line 1184
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 1185
    .line 1186
    .line 1187
    move-result-object v0

    .line 1188
    invoke-virtual {v7}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 1189
    .line 1190
    .line 1191
    move-result v1

    .line 1192
    invoke-virtual {v0, v1, v11, v12}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 1193
    .line 1194
    .line 1195
    move-result v0

    .line 1196
    invoke-virtual {v6, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 1197
    .line 1198
    .line 1199
    goto :goto_4

    .line 1200
    :cond_b
    const v0, -0x70aaaaab

    .line 1201
    .line 1202
    .line 1203
    invoke-virtual {v6, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 1204
    .line 1205
    .line 1206
    :goto_4
    invoke-virtual {v3, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 1207
    .line 1208
    .line 1209
    invoke-virtual {v3, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 1210
    .line 1211
    .line 1212
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 1213
    .line 1214
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1215
    .line 1216
    .line 1217
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 1218
    .line 1219
    return-object v0
.end method

.method private static getRibbonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    div-int/lit8 v4, v2, 0x8

    .line 16
    .line 17
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 18
    .line 19
    .line 20
    move-result v5

    .line 21
    const/high16 v6, 0x3f000000    # 0.5f

    .line 22
    .line 23
    const/4 v7, 0x0

    .line 24
    const/4 v8, 0x1

    .line 25
    const/4 v9, 0x2

    .line 26
    if-eqz v5, :cond_1

    .line 27
    .line 28
    if-eqz v1, :cond_0

    .line 29
    .line 30
    array-length v5, v1

    .line 31
    if-ne v5, v9, :cond_0

    .line 32
    .line 33
    int-to-float v5, v3

    .line 34
    aget-object v6, v1, v7

    .line 35
    .line 36
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 37
    .line 38
    .line 39
    move-result v6

    .line 40
    mul-float v5, v5, v6

    .line 41
    .line 42
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 43
    .line 44
    .line 45
    move-result v5

    .line 46
    int-to-float v5, v5

    .line 47
    div-int/2addr v2, v9

    .line 48
    int-to-float v2, v2

    .line 49
    aget-object v1, v1, v8

    .line 50
    .line 51
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    mul-float v2, v2, v1

    .line 56
    .line 57
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    goto :goto_0

    .line 62
    :cond_0
    int-to-float v1, v3

    .line 63
    const v5, 0x3e2aab8a    # 0.16667f

    .line 64
    .line 65
    .line 66
    mul-float v1, v1, v5

    .line 67
    .line 68
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    int-to-float v5, v1

    .line 73
    div-int/2addr v2, v9

    .line 74
    int-to-float v1, v2

    .line 75
    mul-float v1, v1, v6

    .line 76
    .line 77
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    :goto_0
    int-to-float v1, v1

    .line 82
    goto :goto_4

    .line 83
    :cond_1
    const/high16 v5, 0x3e800000    # 0.25f

    .line 84
    .line 85
    const/high16 v10, 0x3e000000    # 0.125f

    .line 86
    .line 87
    if-eqz v1, :cond_4

    .line 88
    .line 89
    array-length v11, v1

    .line 90
    if-lt v11, v8, :cond_4

    .line 91
    .line 92
    aget-object v11, v1, v7

    .line 93
    .line 94
    if-eqz v11, :cond_2

    .line 95
    .line 96
    int-to-float v2, v2

    .line 97
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    .line 98
    .line 99
    .line 100
    move-result v5

    .line 101
    sub-float/2addr v6, v5

    .line 102
    mul-float v2, v2, v6

    .line 103
    .line 104
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 105
    .line 106
    .line 107
    move-result v2

    .line 108
    goto :goto_1

    .line 109
    :cond_2
    int-to-float v2, v2

    .line 110
    mul-float v2, v2, v5

    .line 111
    .line 112
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 113
    .line 114
    .line 115
    move-result v2

    .line 116
    :goto_1
    int-to-float v2, v2

    .line 117
    array-length v5, v1

    .line 118
    if-lt v5, v9, :cond_3

    .line 119
    .line 120
    aget-object v1, v1, v8

    .line 121
    .line 122
    if-eqz v1, :cond_3

    .line 123
    .line 124
    int-to-float v5, v3

    .line 125
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 126
    .line 127
    .line 128
    move-result v1

    .line 129
    mul-float v5, v5, v1

    .line 130
    .line 131
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 132
    .line 133
    .line 134
    move-result v1

    .line 135
    goto :goto_2

    .line 136
    :cond_3
    int-to-float v1, v3

    .line 137
    mul-float v1, v1, v10

    .line 138
    .line 139
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 140
    .line 141
    .line 142
    move-result v1

    .line 143
    :goto_2
    int-to-float v5, v1

    .line 144
    goto :goto_3

    .line 145
    :cond_4
    int-to-float v1, v3

    .line 146
    mul-float v1, v1, v10

    .line 147
    .line 148
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 149
    .line 150
    .line 151
    move-result v1

    .line 152
    int-to-float v1, v1

    .line 153
    int-to-float v2, v2

    .line 154
    mul-float v2, v2, v5

    .line 155
    .line 156
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 157
    .line 158
    .line 159
    move-result v2

    .line 160
    int-to-float v2, v2

    .line 161
    move v5, v1

    .line 162
    :goto_3
    move v1, v2

    .line 163
    :goto_4
    div-int/lit8 v2, v4, 0x4

    .line 164
    .line 165
    int-to-float v2, v2

    .line 166
    const/high16 v6, 0x40800000    # 4.0f

    .line 167
    .line 168
    div-float v8, v5, v6

    .line 169
    .line 170
    new-instance v9, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 171
    .line 172
    invoke-direct {v9}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 173
    .line 174
    .line 175
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 176
    .line 177
    .line 178
    move-result v10

    .line 179
    if-eqz v10, :cond_5

    .line 180
    .line 181
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 182
    .line 183
    .line 184
    move-result-object v10

    .line 185
    invoke-virtual {v9, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 186
    .line 187
    .line 188
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 189
    .line 190
    .line 191
    move-result-object v10

    .line 192
    invoke-virtual {v10}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 193
    .line 194
    .line 195
    move-result-object v10

    .line 196
    invoke-virtual {v9, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 197
    .line 198
    .line 199
    :cond_5
    new-instance v10, Landroid/graphics/Path;

    .line 200
    .line 201
    invoke-direct {v10}, Landroid/graphics/Path;-><init>()V

    .line 202
    .line 203
    .line 204
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 205
    .line 206
    int-to-float v11, v11

    .line 207
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 208
    .line 209
    int-to-float v12, v12

    .line 210
    invoke-virtual {v10, v11, v12}, Landroid/graphics/Path;->moveTo(FF)V

    .line 211
    .line 212
    .line 213
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 214
    .line 215
    add-int/2addr v11, v4

    .line 216
    int-to-float v11, v11

    .line 217
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 218
    .line 219
    int-to-float v12, v12

    .line 220
    int-to-float v3, v3

    .line 221
    sub-float/2addr v3, v5

    .line 222
    const/high16 v5, 0x40000000    # 2.0f

    .line 223
    .line 224
    div-float v13, v3, v5

    .line 225
    .line 226
    add-float/2addr v12, v13

    .line 227
    invoke-virtual {v10, v11, v12}, Landroid/graphics/Path;->lineTo(FF)V

    .line 228
    .line 229
    .line 230
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 231
    .line 232
    int-to-float v11, v11

    .line 233
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 234
    .line 235
    int-to-float v12, v12

    .line 236
    add-float/2addr v12, v3

    .line 237
    invoke-virtual {v10, v11, v12}, Landroid/graphics/Path;->lineTo(FF)V

    .line 238
    .line 239
    .line 240
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 241
    .line 242
    .line 243
    move-result v11

    .line 244
    int-to-float v11, v11

    .line 245
    sub-float/2addr v11, v1

    .line 246
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 247
    .line 248
    int-to-float v12, v12

    .line 249
    add-float/2addr v12, v3

    .line 250
    invoke-virtual {v10, v11, v12}, Landroid/graphics/Path;->lineTo(FF)V

    .line 251
    .line 252
    .line 253
    sget-object v11, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 254
    .line 255
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 256
    .line 257
    .line 258
    move-result v12

    .line 259
    int-to-float v12, v12

    .line 260
    sub-float/2addr v12, v1

    .line 261
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 262
    .line 263
    int-to-float v14, v14

    .line 264
    mul-float v15, v8, v5

    .line 265
    .line 266
    add-float/2addr v14, v15

    .line 267
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 268
    .line 269
    .line 270
    move-result v7

    .line 271
    int-to-float v7, v7

    .line 272
    sub-float/2addr v7, v1

    .line 273
    mul-float v5, v5, v2

    .line 274
    .line 275
    add-float/2addr v7, v5

    .line 276
    move/from16 v16, v3

    .line 277
    .line 278
    iget v3, v0, Landroid/graphics/Rect;->top:I

    .line 279
    .line 280
    int-to-float v3, v3

    .line 281
    mul-float v17, v8, v6

    .line 282
    .line 283
    add-float v3, v3, v17

    .line 284
    .line 285
    invoke-virtual {v11, v12, v14, v7, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 286
    .line 287
    .line 288
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 289
    .line 290
    const/high16 v7, 0x43340000    # 180.0f

    .line 291
    .line 292
    const/high16 v11, 0x42b40000    # 90.0f

    .line 293
    .line 294
    invoke-virtual {v10, v3, v7, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 295
    .line 296
    .line 297
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 298
    .line 299
    .line 300
    move-result v3

    .line 301
    int-to-float v3, v3

    .line 302
    sub-float/2addr v3, v1

    .line 303
    const/high16 v12, 0x40400000    # 3.0f

    .line 304
    .line 305
    mul-float v14, v2, v12

    .line 306
    .line 307
    add-float/2addr v3, v14

    .line 308
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 309
    .line 310
    int-to-float v7, v7

    .line 311
    add-float/2addr v7, v15

    .line 312
    invoke-virtual {v10, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 313
    .line 314
    .line 315
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 316
    .line 317
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 318
    .line 319
    .line 320
    move-result v7

    .line 321
    int-to-float v7, v7

    .line 322
    sub-float/2addr v7, v1

    .line 323
    add-float/2addr v7, v5

    .line 324
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 325
    .line 326
    int-to-float v12, v12

    .line 327
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 328
    .line 329
    .line 330
    move-result v11

    .line 331
    int-to-float v11, v11

    .line 332
    sub-float/2addr v11, v1

    .line 333
    mul-float v6, v6, v2

    .line 334
    .line 335
    add-float/2addr v11, v6

    .line 336
    move/from16 v18, v6

    .line 337
    .line 338
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 339
    .line 340
    int-to-float v6, v6

    .line 341
    add-float/2addr v6, v15

    .line 342
    invoke-virtual {v3, v7, v12, v11, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 343
    .line 344
    .line 345
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 346
    .line 347
    const/high16 v6, -0x3ccc0000    # -180.0f

    .line 348
    .line 349
    const/high16 v7, 0x42b40000    # 90.0f

    .line 350
    .line 351
    invoke-virtual {v10, v3, v7, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 352
    .line 353
    .line 354
    invoke-virtual {v10}, Landroid/graphics/Path;->close()V

    .line 355
    .line 356
    .line 357
    invoke-virtual {v9, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 358
    .line 359
    .line 360
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 361
    .line 362
    .line 363
    move-result-object v3

    .line 364
    invoke-virtual {v9, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 365
    .line 366
    .line 367
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 368
    .line 369
    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 370
    .line 371
    .line 372
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 373
    .line 374
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 375
    .line 376
    .line 377
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 378
    .line 379
    .line 380
    move-result v7

    .line 381
    if-eqz v7, :cond_6

    .line 382
    .line 383
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 384
    .line 385
    .line 386
    move-result-object v7

    .line 387
    invoke-virtual {v3, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 388
    .line 389
    .line 390
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 391
    .line 392
    .line 393
    move-result-object v7

    .line 394
    invoke-virtual {v7}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 395
    .line 396
    .line 397
    move-result-object v7

    .line 398
    invoke-virtual {v3, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 399
    .line 400
    .line 401
    :cond_6
    new-instance v7, Landroid/graphics/Path;

    .line 402
    .line 403
    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 404
    .line 405
    .line 406
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 407
    .line 408
    .line 409
    move-result v9

    .line 410
    int-to-float v9, v9

    .line 411
    sub-float/2addr v9, v1

    .line 412
    add-float/2addr v9, v2

    .line 413
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    .line 414
    .line 415
    int-to-float v10, v10

    .line 416
    invoke-virtual {v7, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 417
    .line 418
    .line 419
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 420
    .line 421
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 422
    .line 423
    .line 424
    move-result v10

    .line 425
    int-to-float v10, v10

    .line 426
    sub-float/2addr v10, v1

    .line 427
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 428
    .line 429
    int-to-float v11, v11

    .line 430
    sub-float/2addr v11, v15

    .line 431
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 432
    .line 433
    .line 434
    move-result v12

    .line 435
    int-to-float v12, v12

    .line 436
    sub-float/2addr v12, v1

    .line 437
    add-float/2addr v12, v5

    .line 438
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    .line 439
    .line 440
    int-to-float v6, v6

    .line 441
    invoke-virtual {v9, v10, v11, v12, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 442
    .line 443
    .line 444
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 445
    .line 446
    const/high16 v9, 0x42b40000    # 90.0f

    .line 447
    .line 448
    invoke-virtual {v7, v6, v9, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 449
    .line 450
    .line 451
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 452
    .line 453
    .line 454
    move-result v6

    .line 455
    int-to-float v6, v6

    .line 456
    sub-float/2addr v6, v1

    .line 457
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 458
    .line 459
    int-to-float v9, v9

    .line 460
    const/high16 v10, 0x40400000    # 3.0f

    .line 461
    .line 462
    mul-float v12, v8, v10

    .line 463
    .line 464
    add-float/2addr v9, v12

    .line 465
    invoke-virtual {v7, v6, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 466
    .line 467
    .line 468
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 469
    .line 470
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 471
    .line 472
    .line 473
    move-result v9

    .line 474
    int-to-float v9, v9

    .line 475
    sub-float/2addr v9, v1

    .line 476
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 477
    .line 478
    int-to-float v10, v10

    .line 479
    add-float/2addr v10, v15

    .line 480
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 481
    .line 482
    .line 483
    move-result v11

    .line 484
    int-to-float v11, v11

    .line 485
    sub-float/2addr v11, v1

    .line 486
    add-float/2addr v11, v5

    .line 487
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 488
    .line 489
    int-to-float v12, v12

    .line 490
    add-float v12, v12, v17

    .line 491
    .line 492
    invoke-virtual {v6, v9, v10, v11, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 493
    .line 494
    .line 495
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 496
    .line 497
    const/high16 v9, -0x3d4c0000    # -90.0f

    .line 498
    .line 499
    const/high16 v10, 0x43340000    # 180.0f

    .line 500
    .line 501
    invoke-virtual {v7, v6, v10, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 502
    .line 503
    .line 504
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 505
    .line 506
    .line 507
    move-result v6

    .line 508
    int-to-float v6, v6

    .line 509
    add-float/2addr v6, v1

    .line 510
    sub-float/2addr v6, v2

    .line 511
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 512
    .line 513
    int-to-float v10, v10

    .line 514
    add-float v10, v10, v17

    .line 515
    .line 516
    invoke-virtual {v7, v6, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 517
    .line 518
    .line 519
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 520
    .line 521
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 522
    .line 523
    .line 524
    move-result v10

    .line 525
    int-to-float v10, v10

    .line 526
    add-float/2addr v10, v1

    .line 527
    sub-float/2addr v10, v5

    .line 528
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 529
    .line 530
    int-to-float v11, v11

    .line 531
    add-float/2addr v11, v15

    .line 532
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 533
    .line 534
    .line 535
    move-result v12

    .line 536
    int-to-float v12, v12

    .line 537
    add-float/2addr v12, v1

    .line 538
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 539
    .line 540
    int-to-float v9, v9

    .line 541
    add-float v9, v9, v17

    .line 542
    .line 543
    invoke-virtual {v6, v10, v11, v12, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 544
    .line 545
    .line 546
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 547
    .line 548
    const/high16 v9, -0x3d4c0000    # -90.0f

    .line 549
    .line 550
    const/high16 v10, 0x42b40000    # 90.0f

    .line 551
    .line 552
    invoke-virtual {v7, v6, v10, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 553
    .line 554
    .line 555
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 556
    .line 557
    .line 558
    move-result v6

    .line 559
    int-to-float v6, v6

    .line 560
    add-float/2addr v6, v1

    .line 561
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 562
    .line 563
    int-to-float v9, v9

    .line 564
    sub-float/2addr v9, v8

    .line 565
    invoke-virtual {v7, v6, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 566
    .line 567
    .line 568
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 569
    .line 570
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 571
    .line 572
    .line 573
    move-result v8

    .line 574
    int-to-float v8, v8

    .line 575
    add-float/2addr v8, v1

    .line 576
    sub-float/2addr v8, v5

    .line 577
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 578
    .line 579
    int-to-float v9, v9

    .line 580
    sub-float/2addr v9, v15

    .line 581
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 582
    .line 583
    .line 584
    move-result v10

    .line 585
    int-to-float v10, v10

    .line 586
    add-float/2addr v10, v1

    .line 587
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 588
    .line 589
    int-to-float v11, v11

    .line 590
    invoke-virtual {v6, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 591
    .line 592
    .line 593
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 594
    .line 595
    const/4 v8, 0x0

    .line 596
    const/high16 v9, 0x42b40000    # 90.0f

    .line 597
    .line 598
    invoke-virtual {v7, v6, v8, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 599
    .line 600
    .line 601
    invoke-virtual {v7}, Landroid/graphics/Path;->close()V

    .line 602
    .line 603
    .line 604
    invoke-virtual {v3, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 605
    .line 606
    .line 607
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 608
    .line 609
    .line 610
    move-result-object v6

    .line 611
    invoke-virtual {v3, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 612
    .line 613
    .line 614
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 615
    .line 616
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 617
    .line 618
    .line 619
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 620
    .line 621
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 622
    .line 623
    .line 624
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 625
    .line 626
    .line 627
    move-result v6

    .line 628
    if-eqz v6, :cond_7

    .line 629
    .line 630
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 631
    .line 632
    .line 633
    move-result-object v6

    .line 634
    invoke-virtual {v3, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 635
    .line 636
    .line 637
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 638
    .line 639
    .line 640
    move-result-object v6

    .line 641
    invoke-virtual {v6}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 642
    .line 643
    .line 644
    move-result-object v6

    .line 645
    invoke-virtual {v3, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 646
    .line 647
    .line 648
    :cond_7
    new-instance v6, Landroid/graphics/Path;

    .line 649
    .line 650
    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    .line 651
    .line 652
    .line 653
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 654
    .line 655
    int-to-float v7, v7

    .line 656
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 657
    .line 658
    int-to-float v9, v9

    .line 659
    invoke-virtual {v6, v7, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 660
    .line 661
    .line 662
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 663
    .line 664
    sub-int/2addr v7, v4

    .line 665
    int-to-float v7, v7

    .line 666
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 667
    .line 668
    int-to-float v9, v9

    .line 669
    add-float/2addr v9, v13

    .line 670
    invoke-virtual {v6, v7, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 671
    .line 672
    .line 673
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 674
    .line 675
    int-to-float v7, v7

    .line 676
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 677
    .line 678
    int-to-float v9, v9

    .line 679
    add-float v9, v9, v16

    .line 680
    .line 681
    invoke-virtual {v6, v7, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 682
    .line 683
    .line 684
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 685
    .line 686
    .line 687
    move-result v7

    .line 688
    int-to-float v7, v7

    .line 689
    add-float/2addr v7, v1

    .line 690
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 691
    .line 692
    int-to-float v9, v9

    .line 693
    add-float v9, v9, v16

    .line 694
    .line 695
    invoke-virtual {v6, v7, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 696
    .line 697
    .line 698
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 699
    .line 700
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 701
    .line 702
    .line 703
    move-result v9

    .line 704
    int-to-float v9, v9

    .line 705
    add-float/2addr v9, v1

    .line 706
    sub-float/2addr v9, v5

    .line 707
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 708
    .line 709
    int-to-float v10, v10

    .line 710
    add-float/2addr v10, v15

    .line 711
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 712
    .line 713
    .line 714
    move-result v11

    .line 715
    int-to-float v11, v11

    .line 716
    add-float/2addr v11, v1

    .line 717
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 718
    .line 719
    int-to-float v12, v12

    .line 720
    add-float v12, v12, v17

    .line 721
    .line 722
    invoke-virtual {v7, v9, v10, v11, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 723
    .line 724
    .line 725
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 726
    .line 727
    const/high16 v9, -0x3d4c0000    # -90.0f

    .line 728
    .line 729
    invoke-virtual {v6, v7, v8, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 730
    .line 731
    .line 732
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 733
    .line 734
    .line 735
    move-result v7

    .line 736
    int-to-float v7, v7

    .line 737
    add-float/2addr v7, v1

    .line 738
    sub-float/2addr v7, v14

    .line 739
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 740
    .line 741
    int-to-float v8, v8

    .line 742
    add-float/2addr v8, v15

    .line 743
    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 744
    .line 745
    .line 746
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 747
    .line 748
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 749
    .line 750
    .line 751
    move-result v8

    .line 752
    int-to-float v8, v8

    .line 753
    add-float/2addr v8, v1

    .line 754
    sub-float v8, v8, v18

    .line 755
    .line 756
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 757
    .line 758
    int-to-float v9, v9

    .line 759
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 760
    .line 761
    .line 762
    move-result v10

    .line 763
    int-to-float v10, v10

    .line 764
    add-float/2addr v10, v1

    .line 765
    sub-float/2addr v10, v5

    .line 766
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 767
    .line 768
    int-to-float v11, v11

    .line 769
    add-float/2addr v11, v15

    .line 770
    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 771
    .line 772
    .line 773
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 774
    .line 775
    const/high16 v8, 0x43340000    # 180.0f

    .line 776
    .line 777
    const/high16 v9, 0x42b40000    # 90.0f

    .line 778
    .line 779
    invoke-virtual {v6, v7, v9, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 780
    .line 781
    .line 782
    invoke-virtual {v6}, Landroid/graphics/Path;->close()V

    .line 783
    .line 784
    .line 785
    invoke-virtual {v3, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 786
    .line 787
    .line 788
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 789
    .line 790
    .line 791
    move-result-object v6

    .line 792
    invoke-virtual {v3, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 793
    .line 794
    .line 795
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 796
    .line 797
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 798
    .line 799
    .line 800
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 801
    .line 802
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 803
    .line 804
    .line 805
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 806
    .line 807
    .line 808
    move-result v6

    .line 809
    if-eqz v6, :cond_8

    .line 810
    .line 811
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 812
    .line 813
    .line 814
    move-result-object v6

    .line 815
    invoke-virtual {v3, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 816
    .line 817
    .line 818
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 819
    .line 820
    .line 821
    move-result-object v6

    .line 822
    invoke-virtual {v6}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 823
    .line 824
    .line 825
    move-result-object v6

    .line 826
    invoke-virtual {v3, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 827
    .line 828
    .line 829
    :cond_8
    new-instance v6, Landroid/graphics/Path;

    .line 830
    .line 831
    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    .line 832
    .line 833
    .line 834
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 835
    .line 836
    .line 837
    move-result v7

    .line 838
    int-to-float v7, v7

    .line 839
    sub-float/2addr v7, v1

    .line 840
    add-float/2addr v7, v2

    .line 841
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 842
    .line 843
    int-to-float v8, v8

    .line 844
    add-float v8, v8, v17

    .line 845
    .line 846
    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 847
    .line 848
    .line 849
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 850
    .line 851
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 852
    .line 853
    .line 854
    move-result v8

    .line 855
    int-to-float v8, v8

    .line 856
    sub-float/2addr v8, v1

    .line 857
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 858
    .line 859
    int-to-float v9, v9

    .line 860
    add-float/2addr v9, v15

    .line 861
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 862
    .line 863
    .line 864
    move-result v10

    .line 865
    int-to-float v10, v10

    .line 866
    sub-float/2addr v10, v1

    .line 867
    add-float/2addr v10, v5

    .line 868
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 869
    .line 870
    int-to-float v11, v11

    .line 871
    add-float v11, v11, v17

    .line 872
    .line 873
    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 874
    .line 875
    .line 876
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 877
    .line 878
    const/high16 v8, 0x43340000    # 180.0f

    .line 879
    .line 880
    const/high16 v9, 0x42b40000    # 90.0f

    .line 881
    .line 882
    invoke-virtual {v6, v7, v9, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 883
    .line 884
    .line 885
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 886
    .line 887
    .line 888
    move-result v7

    .line 889
    int-to-float v7, v7

    .line 890
    sub-float/2addr v7, v1

    .line 891
    add-float/2addr v7, v14

    .line 892
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 893
    .line 894
    int-to-float v8, v8

    .line 895
    add-float/2addr v8, v15

    .line 896
    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 897
    .line 898
    .line 899
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 900
    .line 901
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 902
    .line 903
    .line 904
    move-result v8

    .line 905
    int-to-float v8, v8

    .line 906
    sub-float/2addr v8, v1

    .line 907
    add-float/2addr v8, v5

    .line 908
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 909
    .line 910
    int-to-float v9, v9

    .line 911
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 912
    .line 913
    .line 914
    move-result v10

    .line 915
    int-to-float v10, v10

    .line 916
    sub-float/2addr v10, v1

    .line 917
    add-float v10, v10, v18

    .line 918
    .line 919
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 920
    .line 921
    int-to-float v11, v11

    .line 922
    add-float/2addr v11, v15

    .line 923
    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 924
    .line 925
    .line 926
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 927
    .line 928
    const/high16 v8, -0x3d4c0000    # -90.0f

    .line 929
    .line 930
    const/high16 v9, 0x42b40000    # 90.0f

    .line 931
    .line 932
    invoke-virtual {v6, v7, v9, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 933
    .line 934
    .line 935
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 936
    .line 937
    .line 938
    move-result v7

    .line 939
    int-to-float v7, v7

    .line 940
    sub-float/2addr v7, v1

    .line 941
    int-to-float v4, v4

    .line 942
    add-float/2addr v7, v4

    .line 943
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 944
    .line 945
    int-to-float v8, v8

    .line 946
    add-float v8, v8, v17

    .line 947
    .line 948
    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 949
    .line 950
    .line 951
    invoke-virtual {v6}, Landroid/graphics/Path;->close()V

    .line 952
    .line 953
    .line 954
    new-instance v7, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 955
    .line 956
    invoke-direct {v7}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 957
    .line 958
    .line 959
    const/4 v8, 0x0

    .line 960
    invoke-virtual {v7, v8}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 961
    .line 962
    .line 963
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 964
    .line 965
    .line 966
    move-result-object v8

    .line 967
    const-wide v9, -0x402cccccc0000000L    # -0.30000001192092896

    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    const v11, -0x70aaaaab

    .line 973
    .line 974
    .line 975
    if-eqz v8, :cond_9

    .line 976
    .line 977
    invoke-virtual {v8}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 978
    .line 979
    .line 980
    move-result v12

    .line 981
    if-nez v12, :cond_9

    .line 982
    .line 983
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 984
    .line 985
    .line 986
    move-result-object v12

    .line 987
    invoke-virtual {v8}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 988
    .line 989
    .line 990
    move-result v13

    .line 991
    invoke-virtual {v12, v13, v9, v10}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 992
    .line 993
    .line 994
    move-result v12

    .line 995
    invoke-virtual {v7, v12}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 996
    .line 997
    .line 998
    goto :goto_5

    .line 999
    :cond_9
    invoke-virtual {v7, v11}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 1000
    .line 1001
    .line 1002
    :goto_5
    invoke-virtual {v3, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 1003
    .line 1004
    .line 1005
    invoke-virtual {v3, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 1006
    .line 1007
    .line 1008
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 1009
    .line 1010
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1011
    .line 1012
    .line 1013
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 1014
    .line 1015
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 1016
    .line 1017
    .line 1018
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 1019
    .line 1020
    .line 1021
    move-result v6

    .line 1022
    if-eqz v6, :cond_a

    .line 1023
    .line 1024
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 1025
    .line 1026
    .line 1027
    move-result-object v6

    .line 1028
    invoke-virtual {v3, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 1029
    .line 1030
    .line 1031
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 1032
    .line 1033
    .line 1034
    move-result-object v6

    .line 1035
    invoke-virtual {v6}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 1036
    .line 1037
    .line 1038
    move-result-object v6

    .line 1039
    invoke-virtual {v3, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 1040
    .line 1041
    .line 1042
    :cond_a
    new-instance v6, Landroid/graphics/Path;

    .line 1043
    .line 1044
    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    .line 1045
    .line 1046
    .line 1047
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 1048
    .line 1049
    .line 1050
    move-result v12

    .line 1051
    int-to-float v12, v12

    .line 1052
    add-float/2addr v12, v1

    .line 1053
    sub-float/2addr v12, v2

    .line 1054
    iget v2, v0, Landroid/graphics/Rect;->top:I

    .line 1055
    .line 1056
    int-to-float v2, v2

    .line 1057
    add-float v2, v2, v17

    .line 1058
    .line 1059
    invoke-virtual {v6, v12, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1060
    .line 1061
    .line 1062
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 1063
    .line 1064
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 1065
    .line 1066
    .line 1067
    move-result v12

    .line 1068
    int-to-float v12, v12

    .line 1069
    add-float/2addr v12, v1

    .line 1070
    sub-float/2addr v12, v5

    .line 1071
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 1072
    .line 1073
    int-to-float v13, v13

    .line 1074
    add-float/2addr v13, v15

    .line 1075
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 1076
    .line 1077
    .line 1078
    move-result v11

    .line 1079
    int-to-float v11, v11

    .line 1080
    add-float/2addr v11, v1

    .line 1081
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 1082
    .line 1083
    int-to-float v9, v9

    .line 1084
    add-float v9, v9, v17

    .line 1085
    .line 1086
    invoke-virtual {v2, v12, v13, v11, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1087
    .line 1088
    .line 1089
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 1090
    .line 1091
    const/high16 v9, -0x3ccc0000    # -180.0f

    .line 1092
    .line 1093
    const/high16 v10, 0x42b40000    # 90.0f

    .line 1094
    .line 1095
    invoke-virtual {v6, v2, v10, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1096
    .line 1097
    .line 1098
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 1099
    .line 1100
    .line 1101
    move-result v2

    .line 1102
    int-to-float v2, v2

    .line 1103
    add-float/2addr v2, v1

    .line 1104
    sub-float/2addr v2, v14

    .line 1105
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 1106
    .line 1107
    int-to-float v9, v9

    .line 1108
    add-float/2addr v9, v15

    .line 1109
    invoke-virtual {v6, v2, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1110
    .line 1111
    .line 1112
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 1113
    .line 1114
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 1115
    .line 1116
    .line 1117
    move-result v9

    .line 1118
    int-to-float v9, v9

    .line 1119
    add-float/2addr v9, v1

    .line 1120
    sub-float v9, v9, v18

    .line 1121
    .line 1122
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 1123
    .line 1124
    int-to-float v10, v10

    .line 1125
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 1126
    .line 1127
    .line 1128
    move-result v11

    .line 1129
    int-to-float v11, v11

    .line 1130
    add-float/2addr v11, v1

    .line 1131
    sub-float/2addr v11, v5

    .line 1132
    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 1133
    .line 1134
    int-to-float v5, v5

    .line 1135
    add-float/2addr v5, v15

    .line 1136
    invoke-virtual {v2, v9, v10, v11, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1137
    .line 1138
    .line 1139
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 1140
    .line 1141
    const/high16 v5, 0x42b40000    # 90.0f

    .line 1142
    .line 1143
    invoke-virtual {v6, v2, v5, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1144
    .line 1145
    .line 1146
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 1147
    .line 1148
    .line 1149
    move-result v2

    .line 1150
    int-to-float v2, v2

    .line 1151
    add-float/2addr v2, v1

    .line 1152
    sub-float/2addr v2, v4

    .line 1153
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 1154
    .line 1155
    int-to-float v0, v0

    .line 1156
    add-float v0, v0, v17

    .line 1157
    .line 1158
    invoke-virtual {v6, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1159
    .line 1160
    .line 1161
    invoke-virtual {v6}, Landroid/graphics/Path;->close()V

    .line 1162
    .line 1163
    .line 1164
    if-eqz v8, :cond_b

    .line 1165
    .line 1166
    invoke-virtual {v8}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 1167
    .line 1168
    .line 1169
    move-result v0

    .line 1170
    if-nez v0, :cond_b

    .line 1171
    .line 1172
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 1173
    .line 1174
    .line 1175
    move-result-object v0

    .line 1176
    invoke-virtual {v8}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 1177
    .line 1178
    .line 1179
    move-result v1

    .line 1180
    const-wide v4, -0x402cccccc0000000L    # -0.30000001192092896

    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    invoke-virtual {v0, v1, v4, v5}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 1186
    .line 1187
    .line 1188
    move-result v0

    .line 1189
    invoke-virtual {v7, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 1190
    .line 1191
    .line 1192
    goto :goto_6

    .line 1193
    :cond_b
    const v0, -0x70aaaaab

    .line 1194
    .line 1195
    .line 1196
    invoke-virtual {v7, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 1197
    .line 1198
    .line 1199
    :goto_6
    invoke-virtual {v3, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 1200
    .line 1201
    .line 1202
    invoke-virtual {v3, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 1203
    .line 1204
    .line 1205
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 1206
    .line 1207
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1208
    .line 1209
    .line 1210
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 1211
    .line 1212
    return-object v0
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getVerticalScrollPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    const/4 v3, 0x0

    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    array-length v4, v1

    .line 23
    const/4 v5, 0x1

    .line 24
    if-ne v4, v5, :cond_0

    .line 25
    .line 26
    int-to-float v2, v2

    .line 27
    aget-object v1, v1, v3

    .line 28
    .line 29
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    mul-float v2, v2, v1

    .line 34
    .line 35
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    goto :goto_0

    .line 40
    :cond_0
    int-to-float v1, v2

    .line 41
    const/high16 v2, 0x3e000000    # 0.125f

    .line 42
    .line 43
    mul-float v1, v1, v2

    .line 44
    .line 45
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    :goto_0
    int-to-float v2, v1

    .line 50
    const/high16 v4, 0x40000000    # 2.0f

    .line 51
    .line 52
    div-float/2addr v2, v4

    .line 53
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 54
    .line 55
    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 56
    .line 57
    .line 58
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 59
    .line 60
    .line 61
    move-result v5

    .line 62
    if-eqz v5, :cond_1

    .line 63
    .line 64
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 65
    .line 66
    .line 67
    move-result-object v5

    .line 68
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 69
    .line 70
    .line 71
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 72
    .line 73
    .line 74
    move-result-object v5

    .line 75
    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 76
    .line 77
    .line 78
    move-result-object v5

    .line 79
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 80
    .line 81
    .line 82
    :cond_1
    new-instance v5, Landroid/graphics/Path;

    .line 83
    .line 84
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 85
    .line 86
    .line 87
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 88
    .line 89
    int-to-float v6, v6

    .line 90
    add-float/2addr v6, v2

    .line 91
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    .line 92
    .line 93
    int-to-float v7, v7

    .line 94
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 95
    .line 96
    .line 97
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 98
    .line 99
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 100
    .line 101
    int-to-float v8, v7

    .line 102
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 103
    .line 104
    sub-int v10, v9, v1

    .line 105
    .line 106
    int-to-float v10, v10

    .line 107
    add-int/2addr v7, v1

    .line 108
    int-to-float v7, v7

    .line 109
    int-to-float v9, v9

    .line 110
    invoke-virtual {v6, v8, v10, v7, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 111
    .line 112
    .line 113
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 114
    .line 115
    const/high16 v7, -0x3d4c0000    # -90.0f

    .line 116
    .line 117
    const/high16 v8, 0x42b40000    # 90.0f

    .line 118
    .line 119
    invoke-virtual {v5, v6, v8, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 120
    .line 121
    .line 122
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 123
    .line 124
    add-int/2addr v6, v1

    .line 125
    int-to-float v6, v6

    .line 126
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 127
    .line 128
    int-to-float v7, v7

    .line 129
    add-float/2addr v7, v2

    .line 130
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 131
    .line 132
    .line 133
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 134
    .line 135
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 136
    .line 137
    add-int v9, v7, v1

    .line 138
    .line 139
    int-to-float v9, v9

    .line 140
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 141
    .line 142
    int-to-float v11, v10

    .line 143
    mul-int/lit8 v12, v1, 0x2

    .line 144
    .line 145
    add-int/2addr v7, v12

    .line 146
    int-to-float v7, v7

    .line 147
    add-int/2addr v10, v1

    .line 148
    int-to-float v10, v10

    .line 149
    invoke-virtual {v6, v9, v11, v7, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 150
    .line 151
    .line 152
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 153
    .line 154
    const/high16 v7, 0x43340000    # 180.0f

    .line 155
    .line 156
    const/high16 v9, 0x43870000    # 270.0f

    .line 157
    .line 158
    invoke-virtual {v5, v6, v7, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 159
    .line 160
    .line 161
    iget v6, v0, Landroid/graphics/Rect;->right:I

    .line 162
    .line 163
    sub-int/2addr v6, v1

    .line 164
    int-to-float v6, v6

    .line 165
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 166
    .line 167
    add-int/2addr v10, v1

    .line 168
    int-to-float v10, v10

    .line 169
    invoke-virtual {v5, v6, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 170
    .line 171
    .line 172
    iget v6, v0, Landroid/graphics/Rect;->right:I

    .line 173
    .line 174
    sub-int/2addr v6, v1

    .line 175
    int-to-float v6, v6

    .line 176
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    .line 177
    .line 178
    int-to-float v10, v10

    .line 179
    sub-float/2addr v10, v2

    .line 180
    invoke-virtual {v5, v6, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 181
    .line 182
    .line 183
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 184
    .line 185
    iget v10, v0, Landroid/graphics/Rect;->right:I

    .line 186
    .line 187
    sub-int v11, v10, v12

    .line 188
    .line 189
    int-to-float v11, v11

    .line 190
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 191
    .line 192
    sub-int v14, v13, v1

    .line 193
    .line 194
    int-to-float v14, v14

    .line 195
    sub-int/2addr v10, v1

    .line 196
    int-to-float v10, v10

    .line 197
    int-to-float v13, v13

    .line 198
    invoke-virtual {v6, v11, v14, v10, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 199
    .line 200
    .line 201
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 202
    .line 203
    const/4 v10, 0x0

    .line 204
    invoke-virtual {v5, v6, v10, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 205
    .line 206
    .line 207
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 208
    .line 209
    .line 210
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 211
    .line 212
    .line 213
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 214
    .line 215
    .line 216
    move-result-object v5

    .line 217
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 218
    .line 219
    .line 220
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 221
    .line 222
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    .line 224
    .line 225
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 226
    .line 227
    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 228
    .line 229
    .line 230
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 231
    .line 232
    .line 233
    move-result v5

    .line 234
    if-eqz v5, :cond_2

    .line 235
    .line 236
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 237
    .line 238
    .line 239
    move-result-object v5

    .line 240
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 241
    .line 242
    .line 243
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 244
    .line 245
    .line 246
    move-result-object v5

    .line 247
    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 248
    .line 249
    .line 250
    move-result-object v5

    .line 251
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 252
    .line 253
    .line 254
    :cond_2
    new-instance v5, Landroid/graphics/Path;

    .line 255
    .line 256
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 257
    .line 258
    .line 259
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 260
    .line 261
    int-to-float v6, v6

    .line 262
    const/high16 v11, 0x40400000    # 3.0f

    .line 263
    .line 264
    mul-float v11, v11, v2

    .line 265
    .line 266
    add-float/2addr v6, v11

    .line 267
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 268
    .line 269
    int-to-float v11, v11

    .line 270
    invoke-virtual {v5, v6, v11}, Landroid/graphics/Path;->moveTo(FF)V

    .line 271
    .line 272
    .line 273
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 274
    .line 275
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 276
    .line 277
    add-int v13, v11, v1

    .line 278
    .line 279
    int-to-float v13, v13

    .line 280
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 281
    .line 282
    int-to-float v15, v14

    .line 283
    add-int/2addr v11, v12

    .line 284
    int-to-float v11, v11

    .line 285
    add-int/2addr v14, v1

    .line 286
    int-to-float v14, v14

    .line 287
    invoke-virtual {v6, v13, v15, v11, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 288
    .line 289
    .line 290
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 291
    .line 292
    invoke-virtual {v5, v6, v9, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 293
    .line 294
    .line 295
    iget v6, v0, Landroid/graphics/Rect;->right:I

    .line 296
    .line 297
    int-to-float v6, v6

    .line 298
    sub-float/2addr v6, v2

    .line 299
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 300
    .line 301
    add-int/2addr v11, v1

    .line 302
    int-to-float v11, v11

    .line 303
    invoke-virtual {v5, v6, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 304
    .line 305
    .line 306
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 307
    .line 308
    iget v11, v0, Landroid/graphics/Rect;->right:I

    .line 309
    .line 310
    sub-int v13, v11, v1

    .line 311
    .line 312
    int-to-float v13, v13

    .line 313
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 314
    .line 315
    int-to-float v15, v14

    .line 316
    int-to-float v11, v11

    .line 317
    add-int/2addr v14, v1

    .line 318
    int-to-float v14, v14

    .line 319
    invoke-virtual {v6, v13, v15, v11, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 320
    .line 321
    .line 322
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 323
    .line 324
    const/high16 v11, -0x3ccc0000    # -180.0f

    .line 325
    .line 326
    invoke-virtual {v5, v6, v8, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 327
    .line 328
    .line 329
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 330
    .line 331
    .line 332
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 333
    .line 334
    .line 335
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 336
    .line 337
    .line 338
    move-result-object v5

    .line 339
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 340
    .line 341
    .line 342
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 343
    .line 344
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 345
    .line 346
    .line 347
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 348
    .line 349
    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 350
    .line 351
    .line 352
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 353
    .line 354
    .line 355
    move-result v5

    .line 356
    if-eqz v5, :cond_3

    .line 357
    .line 358
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 359
    .line 360
    .line 361
    move-result-object v5

    .line 362
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 363
    .line 364
    .line 365
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 366
    .line 367
    .line 368
    move-result-object v5

    .line 369
    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 370
    .line 371
    .line 372
    move-result-object v5

    .line 373
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 374
    .line 375
    .line 376
    :cond_3
    new-instance v5, Landroid/graphics/Path;

    .line 377
    .line 378
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 379
    .line 380
    .line 381
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 382
    .line 383
    add-int/2addr v6, v1

    .line 384
    int-to-float v6, v6

    .line 385
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 386
    .line 387
    sub-int/2addr v13, v1

    .line 388
    int-to-float v13, v13

    .line 389
    invoke-virtual {v5, v6, v13}, Landroid/graphics/Path;->moveTo(FF)V

    .line 390
    .line 391
    .line 392
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 393
    .line 394
    add-int/2addr v6, v1

    .line 395
    int-to-float v6, v6

    .line 396
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 397
    .line 398
    int-to-float v13, v13

    .line 399
    sub-float/2addr v13, v2

    .line 400
    invoke-virtual {v5, v6, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 401
    .line 402
    .line 403
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 404
    .line 405
    int-to-float v6, v6

    .line 406
    add-float/2addr v6, v2

    .line 407
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 408
    .line 409
    int-to-float v13, v13

    .line 410
    sub-float/2addr v13, v2

    .line 411
    invoke-virtual {v5, v6, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 412
    .line 413
    .line 414
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 415
    .line 416
    iget v13, v0, Landroid/graphics/Rect;->left:I

    .line 417
    .line 418
    int-to-float v14, v13

    .line 419
    const/high16 v15, 0x3f000000    # 0.5f

    .line 420
    .line 421
    mul-float v15, v15, v2

    .line 422
    .line 423
    add-float/2addr v14, v15

    .line 424
    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 425
    .line 426
    sub-int v7, v3, v1

    .line 427
    .line 428
    int-to-float v7, v7

    .line 429
    int-to-float v13, v13

    .line 430
    const/high16 v16, 0x3fc00000    # 1.5f

    .line 431
    .line 432
    mul-float v16, v16, v2

    .line 433
    .line 434
    add-float v13, v13, v16

    .line 435
    .line 436
    int-to-float v3, v3

    .line 437
    sub-float/2addr v3, v2

    .line 438
    invoke-virtual {v6, v14, v7, v13, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 439
    .line 440
    .line 441
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 442
    .line 443
    invoke-virtual {v5, v3, v8, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 444
    .line 445
    .line 446
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 447
    .line 448
    .line 449
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 450
    .line 451
    .line 452
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 453
    .line 454
    .line 455
    move-result-object v3

    .line 456
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 457
    .line 458
    .line 459
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 460
    .line 461
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 462
    .line 463
    .line 464
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 465
    .line 466
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 467
    .line 468
    .line 469
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 470
    .line 471
    .line 472
    move-result v4

    .line 473
    if-eqz v4, :cond_4

    .line 474
    .line 475
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 476
    .line 477
    .line 478
    move-result-object v4

    .line 479
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 480
    .line 481
    .line 482
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 483
    .line 484
    .line 485
    move-result-object v4

    .line 486
    invoke-virtual {v4}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 487
    .line 488
    .line 489
    move-result-object v4

    .line 490
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 491
    .line 492
    .line 493
    :cond_4
    new-instance v4, Landroid/graphics/Path;

    .line 494
    .line 495
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 496
    .line 497
    .line 498
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 499
    .line 500
    add-int/2addr v5, v1

    .line 501
    int-to-float v5, v5

    .line 502
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    .line 503
    .line 504
    int-to-float v6, v6

    .line 505
    sub-float/2addr v6, v2

    .line 506
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 507
    .line 508
    .line 509
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 510
    .line 511
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 512
    .line 513
    int-to-float v7, v6

    .line 514
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 515
    .line 516
    sub-int v13, v11, v1

    .line 517
    .line 518
    int-to-float v13, v13

    .line 519
    add-int/2addr v6, v1

    .line 520
    int-to-float v6, v6

    .line 521
    int-to-float v11, v11

    .line 522
    invoke-virtual {v5, v7, v13, v6, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 523
    .line 524
    .line 525
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 526
    .line 527
    invoke-virtual {v4, v5, v10, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 528
    .line 529
    .line 530
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 531
    .line 532
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 533
    .line 534
    int-to-float v7, v6

    .line 535
    add-float/2addr v7, v15

    .line 536
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 537
    .line 538
    sub-int v13, v11, v1

    .line 539
    .line 540
    int-to-float v13, v13

    .line 541
    int-to-float v6, v6

    .line 542
    add-float v6, v6, v16

    .line 543
    .line 544
    int-to-float v11, v11

    .line 545
    sub-float/2addr v11, v2

    .line 546
    invoke-virtual {v5, v7, v13, v6, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 547
    .line 548
    .line 549
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 550
    .line 551
    const/high16 v6, 0x43340000    # 180.0f

    .line 552
    .line 553
    invoke-virtual {v4, v5, v9, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 554
    .line 555
    .line 556
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 557
    .line 558
    .line 559
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 560
    .line 561
    .line 562
    new-instance v4, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 563
    .line 564
    invoke-direct {v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 565
    .line 566
    .line 567
    const/4 v5, 0x0

    .line 568
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 569
    .line 570
    .line 571
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 572
    .line 573
    .line 574
    move-result-object v5

    .line 575
    const-wide v6, -0x402cccccc0000000L    # -0.30000001192092896

    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    const v9, -0x70aaaaab

    .line 581
    .line 582
    .line 583
    if-eqz v5, :cond_5

    .line 584
    .line 585
    invoke-virtual {v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 586
    .line 587
    .line 588
    move-result v11

    .line 589
    if-nez v11, :cond_5

    .line 590
    .line 591
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 592
    .line 593
    .line 594
    move-result-object v11

    .line 595
    invoke-virtual {v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 596
    .line 597
    .line 598
    move-result v13

    .line 599
    invoke-virtual {v11, v13, v6, v7}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 600
    .line 601
    .line 602
    move-result v11

    .line 603
    invoke-virtual {v4, v11}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 604
    .line 605
    .line 606
    goto :goto_1

    .line 607
    :cond_5
    invoke-virtual {v4, v9}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 608
    .line 609
    .line 610
    :goto_1
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 611
    .line 612
    .line 613
    sget-object v11, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 614
    .line 615
    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 616
    .line 617
    .line 618
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 619
    .line 620
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 621
    .line 622
    .line 623
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 624
    .line 625
    .line 626
    move-result v11

    .line 627
    if-eqz v11, :cond_6

    .line 628
    .line 629
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 630
    .line 631
    .line 632
    move-result-object v11

    .line 633
    invoke-virtual {v3, v11}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 634
    .line 635
    .line 636
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 637
    .line 638
    .line 639
    move-result-object v11

    .line 640
    invoke-virtual {v11}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 641
    .line 642
    .line 643
    move-result-object v11

    .line 644
    invoke-virtual {v3, v11}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 645
    .line 646
    .line 647
    :cond_6
    new-instance v11, Landroid/graphics/Path;

    .line 648
    .line 649
    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 650
    .line 651
    .line 652
    iget v13, v0, Landroid/graphics/Rect;->left:I

    .line 653
    .line 654
    add-int/2addr v13, v12

    .line 655
    int-to-float v13, v13

    .line 656
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 657
    .line 658
    int-to-float v14, v14

    .line 659
    add-float/2addr v14, v2

    .line 660
    invoke-virtual {v11, v13, v14}, Landroid/graphics/Path;->moveTo(FF)V

    .line 661
    .line 662
    .line 663
    sget-object v13, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 664
    .line 665
    iget v14, v0, Landroid/graphics/Rect;->left:I

    .line 666
    .line 667
    add-int v9, v14, v1

    .line 668
    .line 669
    int-to-float v9, v9

    .line 670
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 671
    .line 672
    int-to-float v7, v6

    .line 673
    add-int/2addr v14, v12

    .line 674
    int-to-float v12, v14

    .line 675
    add-int/2addr v6, v1

    .line 676
    int-to-float v6, v6

    .line 677
    invoke-virtual {v13, v9, v7, v12, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 678
    .line 679
    .line 680
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 681
    .line 682
    invoke-virtual {v11, v6, v10, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 683
    .line 684
    .line 685
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 686
    .line 687
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 688
    .line 689
    add-int v9, v7, v1

    .line 690
    .line 691
    int-to-float v9, v9

    .line 692
    add-float/2addr v9, v15

    .line 693
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 694
    .line 695
    int-to-float v10, v0

    .line 696
    add-float/2addr v10, v2

    .line 697
    add-int/2addr v7, v1

    .line 698
    int-to-float v2, v7

    .line 699
    add-float v2, v2, v16

    .line 700
    .line 701
    add-int/2addr v0, v1

    .line 702
    int-to-float v0, v0

    .line 703
    invoke-virtual {v6, v9, v10, v2, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 704
    .line 705
    .line 706
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->tempRect:Landroid/graphics/RectF;

    .line 707
    .line 708
    const/high16 v1, 0x43340000    # 180.0f

    .line 709
    .line 710
    invoke-virtual {v11, v0, v8, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 711
    .line 712
    .line 713
    invoke-virtual {v11}, Landroid/graphics/Path;->close()V

    .line 714
    .line 715
    .line 716
    invoke-virtual {v3, v11}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 717
    .line 718
    .line 719
    if-eqz v5, :cond_7

    .line 720
    .line 721
    invoke-virtual {v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 722
    .line 723
    .line 724
    move-result v0

    .line 725
    if-nez v0, :cond_7

    .line 726
    .line 727
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 728
    .line 729
    .line 730
    move-result-object v0

    .line 731
    invoke-virtual {v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 732
    .line 733
    .line 734
    move-result v1

    .line 735
    const-wide v5, -0x402cccccc0000000L    # -0.30000001192092896

    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    invoke-virtual {v0, v1, v5, v6}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 741
    .line 742
    .line 743
    move-result v0

    .line 744
    invoke-virtual {v4, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 745
    .line 746
    .line 747
    goto :goto_2

    .line 748
    :cond_7
    const v0, -0x70aaaaab

    .line 749
    .line 750
    .line 751
    invoke-virtual {v4, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 752
    .line 753
    .line 754
    :goto_2
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 755
    .line 756
    .line 757
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 758
    .line 759
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 760
    .line 761
    .line 762
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 763
    .line 764
    return-object v0
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getWavePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 16
    .line 17
    .line 18
    move-result v4

    .line 19
    const/4 v5, 0x2

    .line 20
    const/4 v6, 0x1

    .line 21
    const/high16 v7, 0x3e000000    # 0.125f

    .line 22
    .line 23
    const/4 v8, 0x0

    .line 24
    if-eqz v4, :cond_1

    .line 25
    .line 26
    if-eqz v1, :cond_0

    .line 27
    .line 28
    array-length v4, v1

    .line 29
    if-ne v4, v5, :cond_0

    .line 30
    .line 31
    int-to-float v3, v3

    .line 32
    aget-object v4, v1, v8

    .line 33
    .line 34
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 35
    .line 36
    .line 37
    move-result v4

    .line 38
    mul-float v3, v3, v4

    .line 39
    .line 40
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    int-to-float v4, v2

    .line 45
    aget-object v1, v1, v6

    .line 46
    .line 47
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    mul-float v4, v4, v1

    .line 52
    .line 53
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 54
    .line 55
    .line 56
    move-result v8

    .line 57
    goto :goto_1

    .line 58
    :cond_0
    int-to-float v1, v3

    .line 59
    mul-float v1, v1, v7

    .line 60
    .line 61
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 62
    .line 63
    .line 64
    move-result v3

    .line 65
    goto :goto_1

    .line 66
    :cond_1
    if-eqz v1, :cond_3

    .line 67
    .line 68
    array-length v4, v1

    .line 69
    if-lt v4, v6, :cond_3

    .line 70
    .line 71
    aget-object v4, v1, v8

    .line 72
    .line 73
    if-eqz v4, :cond_2

    .line 74
    .line 75
    int-to-float v3, v3

    .line 76
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 77
    .line 78
    .line 79
    move-result v4

    .line 80
    mul-float v3, v3, v4

    .line 81
    .line 82
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 83
    .line 84
    .line 85
    move-result v3

    .line 86
    goto :goto_0

    .line 87
    :cond_2
    int-to-float v3, v3

    .line 88
    mul-float v3, v3, v7

    .line 89
    .line 90
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 91
    .line 92
    .line 93
    move-result v3

    .line 94
    :goto_0
    array-length v4, v1

    .line 95
    if-lt v4, v5, :cond_4

    .line 96
    .line 97
    aget-object v1, v1, v6

    .line 98
    .line 99
    if-eqz v1, :cond_4

    .line 100
    .line 101
    int-to-float v4, v2

    .line 102
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 103
    .line 104
    .line 105
    move-result v1

    .line 106
    const/high16 v5, 0x3f000000    # 0.5f

    .line 107
    .line 108
    sub-float/2addr v1, v5

    .line 109
    mul-float v4, v4, v1

    .line 110
    .line 111
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 112
    .line 113
    .line 114
    move-result v8

    .line 115
    goto :goto_1

    .line 116
    :cond_3
    int-to-float v1, v3

    .line 117
    mul-float v1, v1, v7

    .line 118
    .line 119
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 120
    .line 121
    .line 122
    move-result v3

    .line 123
    :cond_4
    :goto_1
    mul-int/lit8 v1, v8, 0x2

    .line 124
    .line 125
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    .line 126
    .line 127
    .line 128
    move-result v1

    .line 129
    sub-int/2addr v2, v1

    .line 130
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 131
    .line 132
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 133
    .line 134
    .line 135
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 136
    .line 137
    .line 138
    move-result v4

    .line 139
    if-eqz v4, :cond_5

    .line 140
    .line 141
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 142
    .line 143
    .line 144
    move-result-object v4

    .line 145
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 146
    .line 147
    .line 148
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 149
    .line 150
    .line 151
    move-result-object v4

    .line 152
    invoke-virtual {v4}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 153
    .line 154
    .line 155
    move-result-object v4

    .line 156
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 157
    .line 158
    .line 159
    :cond_5
    new-instance v4, Landroid/graphics/Path;

    .line 160
    .line 161
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 162
    .line 163
    .line 164
    const v5, 0x3eaa7efa    # 0.333f

    .line 165
    .line 166
    .line 167
    const v6, 0x3eaaa64c    # 0.3333f

    .line 168
    .line 169
    .line 170
    const v7, 0x40554fdf    # 3.333f

    .line 171
    .line 172
    .line 173
    const v9, 0x3f2aacda    # 0.6667f

    .line 174
    .line 175
    .line 176
    if-lez v8, :cond_6

    .line 177
    .line 178
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 179
    .line 180
    int-to-float v8, v8

    .line 181
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 182
    .line 183
    add-int/2addr v10, v3

    .line 184
    int-to-float v10, v10

    .line 185
    invoke-virtual {v4, v8, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 186
    .line 187
    .line 188
    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 189
    .line 190
    int-to-float v10, v8

    .line 191
    int-to-float v15, v2

    .line 192
    mul-float v6, v6, v15

    .line 193
    .line 194
    add-float/2addr v10, v6

    .line 195
    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 196
    .line 197
    add-int v11, v6, v3

    .line 198
    .line 199
    int-to-float v11, v11

    .line 200
    int-to-float v14, v3

    .line 201
    const v12, 0x405554ca

    .line 202
    .line 203
    .line 204
    mul-float v16, v14, v12

    .line 205
    .line 206
    sub-float v11, v11, v16

    .line 207
    .line 208
    int-to-float v12, v8

    .line 209
    mul-float v17, v15, v9

    .line 210
    .line 211
    add-float v12, v12, v17

    .line 212
    .line 213
    add-int v9, v6, v3

    .line 214
    .line 215
    int-to-float v9, v9

    .line 216
    add-float v13, v9, v16

    .line 217
    .line 218
    add-int/2addr v8, v2

    .line 219
    int-to-float v8, v8

    .line 220
    add-int/2addr v6, v3

    .line 221
    int-to-float v6, v6

    .line 222
    move-object v9, v4

    .line 223
    move/from16 v18, v14

    .line 224
    .line 225
    move v14, v8

    .line 226
    move v8, v15

    .line 227
    move v15, v6

    .line 228
    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 229
    .line 230
    .line 231
    iget v6, v0, Landroid/graphics/Rect;->right:I

    .line 232
    .line 233
    int-to-float v6, v6

    .line 234
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 235
    .line 236
    sub-int/2addr v9, v3

    .line 237
    int-to-float v9, v9

    .line 238
    invoke-virtual {v4, v6, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 239
    .line 240
    .line 241
    iget v6, v0, Landroid/graphics/Rect;->right:I

    .line 242
    .line 243
    int-to-float v9, v6

    .line 244
    mul-float v15, v8, v5

    .line 245
    .line 246
    sub-float v10, v9, v15

    .line 247
    .line 248
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 249
    .line 250
    sub-int v5, v0, v3

    .line 251
    .line 252
    int-to-float v5, v5

    .line 253
    add-float v11, v5, v16

    .line 254
    .line 255
    int-to-float v5, v6

    .line 256
    sub-float v12, v5, v17

    .line 257
    .line 258
    sub-int v5, v0, v3

    .line 259
    .line 260
    int-to-float v5, v5

    .line 261
    mul-float v14, v18, v7

    .line 262
    .line 263
    sub-float v13, v5, v14

    .line 264
    .line 265
    sub-int/2addr v6, v2

    .line 266
    int-to-float v14, v6

    .line 267
    sub-int/2addr v0, v3

    .line 268
    int-to-float v15, v0

    .line 269
    move-object v9, v4

    .line 270
    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 271
    .line 272
    .line 273
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 274
    .line 275
    .line 276
    goto :goto_2

    .line 277
    :cond_6
    iget v8, v0, Landroid/graphics/Rect;->right:I

    .line 278
    .line 279
    sub-int/2addr v8, v2

    .line 280
    int-to-float v8, v8

    .line 281
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 282
    .line 283
    add-int/2addr v10, v3

    .line 284
    int-to-float v10, v10

    .line 285
    invoke-virtual {v4, v8, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 286
    .line 287
    .line 288
    iget v8, v0, Landroid/graphics/Rect;->right:I

    .line 289
    .line 290
    int-to-float v10, v8

    .line 291
    int-to-float v15, v2

    .line 292
    mul-float v16, v15, v9

    .line 293
    .line 294
    sub-float v10, v10, v16

    .line 295
    .line 296
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 297
    .line 298
    add-int v11, v9, v3

    .line 299
    .line 300
    int-to-float v11, v11

    .line 301
    int-to-float v12, v3

    .line 302
    mul-float v7, v7, v12

    .line 303
    .line 304
    sub-float/2addr v11, v7

    .line 305
    int-to-float v12, v8

    .line 306
    mul-float v6, v6, v15

    .line 307
    .line 308
    sub-float/2addr v12, v6

    .line 309
    add-int v6, v9, v3

    .line 310
    .line 311
    int-to-float v6, v6

    .line 312
    add-float v13, v6, v7

    .line 313
    .line 314
    int-to-float v14, v8

    .line 315
    add-int/2addr v9, v3

    .line 316
    int-to-float v6, v9

    .line 317
    move-object v9, v4

    .line 318
    move v8, v15

    .line 319
    move v15, v6

    .line 320
    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 321
    .line 322
    .line 323
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 324
    .line 325
    add-int/2addr v6, v2

    .line 326
    int-to-float v2, v6

    .line 327
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    .line 328
    .line 329
    sub-int/2addr v6, v3

    .line 330
    int-to-float v6, v6

    .line 331
    invoke-virtual {v4, v2, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 332
    .line 333
    .line 334
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 335
    .line 336
    int-to-float v6, v2

    .line 337
    add-float v10, v6, v16

    .line 338
    .line 339
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 340
    .line 341
    sub-int v6, v0, v3

    .line 342
    .line 343
    int-to-float v6, v6

    .line 344
    add-float v11, v6, v7

    .line 345
    .line 346
    int-to-float v6, v2

    .line 347
    mul-float v15, v8, v5

    .line 348
    .line 349
    add-float v12, v6, v15

    .line 350
    .line 351
    sub-int v5, v0, v3

    .line 352
    .line 353
    int-to-float v5, v5

    .line 354
    sub-float v13, v5, v7

    .line 355
    .line 356
    int-to-float v14, v2

    .line 357
    sub-int/2addr v0, v3

    .line 358
    int-to-float v15, v0

    .line 359
    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 360
    .line 361
    .line 362
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 363
    .line 364
    .line 365
    :goto_2
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 366
    .line 367
    .line 368
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 369
    .line 370
    .line 371
    move-result-object v0

    .line 372
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 373
    .line 374
    .line 375
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 376
    .line 377
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 378
    .line 379
    .line 380
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/BannerPathBuilder;->pathExList:Ljava/util/List;

    .line 381
    .line 382
    return-object v0
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

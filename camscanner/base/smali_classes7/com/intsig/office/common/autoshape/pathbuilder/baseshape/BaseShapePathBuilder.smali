.class public Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;
.super Ljava/lang/Object;
.source "BaseShapePathBuilder.java"


# static fields
.field private static final TODEGREE_03:F = 0.3295496f

.field private static final TODEGREE_07:F = 1.6666666f

.field private static m:Landroid/graphics/Matrix;

.field private static path:Landroid/graphics/Path;

.field private static paths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation
.end field

.field private static rectF:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 7
    .line 8
    new-instance v0, Landroid/graphics/Path;

    .line 9
    .line 10
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 14
    .line 15
    new-instance v0, Ljava/util/ArrayList;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 21
    .line 22
    new-instance v0, Landroid/graphics/Matrix;

    .line 23
    .line 24
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 25
    .line 26
    .line 27
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->m:Landroid/graphics/Matrix;

    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static getArcPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x0

    .line 10
    const/4 v3, 0x2

    .line 11
    const/high16 v4, 0x43870000    # 270.0f

    .line 12
    .line 13
    const/4 v5, 0x1

    .line 14
    const/high16 v6, 0x43b40000    # 360.0f

    .line 15
    .line 16
    const/4 v7, 0x0

    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    if-eqz v0, :cond_5

    .line 20
    .line 21
    array-length v1, v0

    .line 22
    if-lt v1, v3, :cond_5

    .line 23
    .line 24
    aget-object v1, v0, v2

    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    const v2, 0x3fd55555

    .line 31
    .line 32
    .line 33
    mul-float v4, v1, v2

    .line 34
    .line 35
    aget-object v0, v0, v5

    .line 36
    .line 37
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    mul-float v7, v0, v2

    .line 42
    .line 43
    goto :goto_2

    .line 44
    :cond_0
    if-eqz v0, :cond_2

    .line 45
    .line 46
    array-length v1, v0

    .line 47
    if-lt v1, v5, :cond_2

    .line 48
    .line 49
    aget-object v1, v0, v2

    .line 50
    .line 51
    const/high16 v2, 0x40400000    # 3.0f

    .line 52
    .line 53
    if-eqz v1, :cond_1

    .line 54
    .line 55
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    div-float/2addr v1, v2

    .line 60
    move v4, v1

    .line 61
    goto :goto_0

    .line 62
    :cond_1
    const/4 v4, 0x0

    .line 63
    :goto_0
    array-length v1, v0

    .line 64
    if-lt v1, v3, :cond_2

    .line 65
    .line 66
    aget-object v0, v0, v5

    .line 67
    .line 68
    if-eqz v0, :cond_2

    .line 69
    .line 70
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    div-float/2addr v0, v2

    .line 75
    goto :goto_1

    .line 76
    :cond_2
    const/4 v0, 0x0

    .line 77
    :goto_1
    cmpg-float v1, v4, v7

    .line 78
    .line 79
    if-gez v1, :cond_3

    .line 80
    .line 81
    add-float/2addr v4, v6

    .line 82
    :cond_3
    cmpg-float v1, v0, v7

    .line 83
    .line 84
    if-gez v1, :cond_4

    .line 85
    .line 86
    add-float v7, v0, v6

    .line 87
    .line 88
    goto :goto_2

    .line 89
    :cond_4
    move v7, v0

    .line 90
    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 95
    .line 96
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 97
    .line 98
    .line 99
    new-instance v1, Landroid/graphics/Path;

    .line 100
    .line 101
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 102
    .line 103
    .line 104
    if-eqz v0, :cond_6

    .line 105
    .line 106
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 107
    .line 108
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 109
    .line 110
    .line 111
    new-instance v2, Landroid/graphics/Path;

    .line 112
    .line 113
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 114
    .line 115
    .line 116
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 117
    .line 118
    .line 119
    move-result v3

    .line 120
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 121
    .line 122
    .line 123
    move-result v5

    .line 124
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 125
    .line 126
    .line 127
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 128
    .line 129
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 130
    .line 131
    int-to-float v5, v5

    .line 132
    iget v8, p1, Landroid/graphics/Rect;->top:I

    .line 133
    .line 134
    int-to-float v8, v8

    .line 135
    iget v9, p1, Landroid/graphics/Rect;->right:I

    .line 136
    .line 137
    int-to-float v9, v9

    .line 138
    iget v10, p1, Landroid/graphics/Rect;->bottom:I

    .line 139
    .line 140
    int-to-float v10, v10

    .line 141
    invoke-virtual {v3, v5, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 142
    .line 143
    .line 144
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 145
    .line 146
    sub-float v5, v7, v4

    .line 147
    .line 148
    add-float/2addr v5, v6

    .line 149
    rem-float/2addr v5, v6

    .line 150
    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 151
    .line 152
    .line 153
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 154
    .line 155
    .line 156
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 157
    .line 158
    .line 159
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 160
    .line 161
    .line 162
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 163
    .line 164
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    .line 166
    .line 167
    :cond_6
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 168
    .line 169
    .line 170
    move-result v0

    .line 171
    if-eqz v0, :cond_7

    .line 172
    .line 173
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 174
    .line 175
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 176
    .line 177
    .line 178
    new-instance v1, Landroid/graphics/Path;

    .line 179
    .line 180
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 181
    .line 182
    .line 183
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 184
    .line 185
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 186
    .line 187
    int-to-float v3, v3

    .line 188
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 189
    .line 190
    int-to-float v5, v5

    .line 191
    iget v8, p1, Landroid/graphics/Rect;->right:I

    .line 192
    .line 193
    int-to-float v8, v8

    .line 194
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 195
    .line 196
    int-to-float p1, p1

    .line 197
    invoke-virtual {v2, v3, v5, v8, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 198
    .line 199
    .line 200
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 201
    .line 202
    sub-float/2addr v7, v4

    .line 203
    add-float/2addr v7, v6

    .line 204
    rem-float/2addr v7, v6

    .line 205
    invoke-virtual {v1, p1, v4, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 206
    .line 207
    .line 208
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 209
    .line 210
    .line 211
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 212
    .line 213
    .line 214
    move-result-object p0

    .line 215
    invoke-virtual {v0, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 216
    .line 217
    .line 218
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 219
    .line 220
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    .line 222
    .line 223
    :cond_7
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 224
    .line 225
    return-object p0
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static getBaseShapePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/lang/Object;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    const/16 v1, 0x10

    .line 16
    .line 17
    if-eq v0, v1, :cond_9

    .line 18
    .line 19
    const/16 v1, 0x13

    .line 20
    .line 21
    if-eq v0, v1, :cond_8

    .line 22
    .line 23
    const/16 v1, 0x41

    .line 24
    .line 25
    if-eq v0, v1, :cond_7

    .line 26
    .line 27
    const/16 v1, 0xea

    .line 28
    .line 29
    if-eq v0, v1, :cond_6

    .line 30
    .line 31
    const/16 v1, 0x38

    .line 32
    .line 33
    if-eq v0, v1, :cond_5

    .line 34
    .line 35
    const/16 v1, 0x39

    .line 36
    .line 37
    if-eq v0, v1, :cond_4

    .line 38
    .line 39
    const/16 v1, 0x49

    .line 40
    .line 41
    if-eq v0, v1, :cond_3

    .line 42
    .line 43
    const/16 v1, 0x4a

    .line 44
    .line 45
    if-eq v0, v1, :cond_2

    .line 46
    .line 47
    const/16 v1, 0x5f

    .line 48
    .line 49
    if-eq v0, v1, :cond_1

    .line 50
    .line 51
    const/16 v1, 0x60

    .line 52
    .line 53
    if-eq v0, v1, :cond_0

    .line 54
    .line 55
    packed-switch v0, :pswitch_data_0

    .line 56
    .line 57
    .line 58
    packed-switch v0, :pswitch_data_1

    .line 59
    .line 60
    .line 61
    packed-switch v0, :pswitch_data_2

    .line 62
    .line 63
    .line 64
    packed-switch v0, :pswitch_data_3

    .line 65
    .line 66
    .line 67
    packed-switch v0, :pswitch_data_4

    .line 68
    .line 69
    .line 70
    const/4 p0, 0x0

    .line 71
    return-object p0

    .line 72
    :pswitch_0
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getDiagStripePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 73
    .line 74
    .line 75
    move-result-object p0

    .line 76
    return-object p0

    .line 77
    :pswitch_1
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getCornerPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 78
    .line 79
    .line 80
    move-result-object p0

    .line 81
    return-object p0

    .line 82
    :pswitch_2
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getHalfFramePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 83
    .line 84
    .line 85
    move-result-object p0

    .line 86
    return-object p0

    .line 87
    :pswitch_3
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getFramePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 88
    .line 89
    .line 90
    move-result-object p0

    .line 91
    return-object p0

    .line 92
    :pswitch_4
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getTeardropPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 93
    .line 94
    .line 95
    move-result-object p0

    .line 96
    return-object p0

    .line 97
    :pswitch_5
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getChordPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 98
    .line 99
    .line 100
    move-result-object p0

    .line 101
    return-object p0

    .line 102
    :pswitch_6
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getPiePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 103
    .line 104
    .line 105
    move-result-object p0

    .line 106
    return-object p0

    .line 107
    :pswitch_7
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getDodecagonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 108
    .line 109
    .line 110
    move-result-object p0

    .line 111
    return-object p0

    .line 112
    :pswitch_8
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getDecagonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 113
    .line 114
    .line 115
    move-result-object p0

    .line 116
    return-object p0

    .line 117
    :pswitch_9
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getHeptagonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 118
    .line 119
    .line 120
    move-result-object p0

    .line 121
    return-object p0

    .line 122
    :pswitch_a
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getBracePairPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 123
    .line 124
    .line 125
    move-result-object p0

    .line 126
    return-object p0

    .line 127
    :pswitch_b
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getBracketPairPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 128
    .line 129
    .line 130
    move-result-object p0

    .line 131
    return-object p0

    .line 132
    :pswitch_c
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getMoonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 133
    .line 134
    .line 135
    move-result-object p0

    .line 136
    return-object p0

    .line 137
    :pswitch_d
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getSunPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 138
    .line 139
    .line 140
    move-result-object p0

    .line 141
    return-object p0

    .line 142
    :pswitch_e
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getRightBracePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 143
    .line 144
    .line 145
    move-result-object p0

    .line 146
    return-object p0

    .line 147
    :pswitch_f
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getLeftBracePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 148
    .line 149
    .line 150
    move-result-object p0

    .line 151
    return-object p0

    .line 152
    :pswitch_10
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getRightBracketPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 153
    .line 154
    .line 155
    move-result-object p0

    .line 156
    return-object p0

    .line 157
    :pswitch_11
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getLeftBracketPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 158
    .line 159
    .line 160
    move-result-object p0

    .line 161
    return-object p0

    .line 162
    :pswitch_12
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getBevelPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 163
    .line 164
    .line 165
    move-result-object p0

    .line 166
    return-object p0

    .line 167
    :pswitch_13
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getDonutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 168
    .line 169
    .line 170
    move-result-object p0

    .line 171
    return-object p0

    .line 172
    :pswitch_14
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getCanPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 173
    .line 174
    .line 175
    move-result-object p0

    .line 176
    return-object p0

    .line 177
    :pswitch_15
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getPlaquePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 178
    .line 179
    .line 180
    move-result-object p0

    .line 181
    return-object p0

    .line 182
    :pswitch_16
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getPlusPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 183
    .line 184
    .line 185
    move-result-object p0

    .line 186
    return-object p0

    .line 187
    :pswitch_17
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getOctagonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 188
    .line 189
    .line 190
    move-result-object p0

    .line 191
    return-object p0

    .line 192
    :pswitch_18
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getHexagonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 193
    .line 194
    .line 195
    move-result-object p0

    .line 196
    return-object p0

    .line 197
    :pswitch_19
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getTrapezoidPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 198
    .line 199
    .line 200
    move-result-object p0

    .line 201
    return-object p0

    .line 202
    :pswitch_1a
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getParallelogramPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 203
    .line 204
    .line 205
    move-result-object p0

    .line 206
    return-object p0

    .line 207
    :pswitch_1b
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getRtTrianglePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 208
    .line 209
    .line 210
    move-result-object p0

    .line 211
    return-object p0

    .line 212
    :pswitch_1c
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getTrianglePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 213
    .line 214
    .line 215
    move-result-object p0

    .line 216
    return-object p0

    .line 217
    :pswitch_1d
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getDiamondPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 218
    .line 219
    .line 220
    move-result-object p0

    .line 221
    return-object p0

    .line 222
    :pswitch_1e
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getEllipsePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 223
    .line 224
    .line 225
    move-result-object p0

    .line 226
    return-object p0

    .line 227
    :cond_0
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getSmileyFacePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 228
    .line 229
    .line 230
    move-result-object p0

    .line 231
    return-object p0

    .line 232
    :cond_1
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getBlockArcPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 233
    .line 234
    .line 235
    move-result-object p0

    .line 236
    return-object p0

    .line 237
    :cond_2
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getHeartPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 238
    .line 239
    .line 240
    move-result-object p0

    .line 241
    return-object p0

    .line 242
    :cond_3
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getLightningBoltPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 243
    .line 244
    .line 245
    move-result-object p0

    .line 246
    return-object p0

    .line 247
    :cond_4
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getNoSmokingPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 248
    .line 249
    .line 250
    move-result-object p0

    .line 251
    return-object p0

    .line 252
    :cond_5
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getPentagonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 253
    .line 254
    .line 255
    move-result-object p0

    .line 256
    return-object p0

    .line 257
    :cond_6
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getCloudPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 258
    .line 259
    .line 260
    move-result-object p0

    .line 261
    return-object p0

    .line 262
    :cond_7
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getFoldedCornerPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 263
    .line 264
    .line 265
    move-result-object p0

    .line 266
    return-object p0

    .line 267
    :cond_8
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getArcPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 268
    .line 269
    .line 270
    move-result-object p0

    .line 271
    return-object p0

    .line 272
    :cond_9
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->getCubePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;

    .line 273
    .line 274
    .line 275
    move-result-object p0

    .line 276
    return-object p0

    .line 277
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
    .end packed-switch

    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_15
        :pswitch_14
        :pswitch_13
    .end packed-switch

    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    :pswitch_data_2
    .packed-switch 0x54
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
    .end packed-switch

    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    :pswitch_data_3
    .packed-switch 0xb7
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
    .end packed-switch

    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    :pswitch_data_4
    .packed-switch 0xd9
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getBevelPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const/high16 v1, 0x3e000000    # 0.125f

    .line 15
    .line 16
    mul-float v0, v0, v1

    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const/4 v2, 0x0

    .line 23
    if-eqz v1, :cond_0

    .line 24
    .line 25
    array-length v3, v1

    .line 26
    const/4 v4, 0x1

    .line 27
    if-lt v3, v4, :cond_0

    .line 28
    .line 29
    aget-object v3, v1, v2

    .line 30
    .line 31
    if-eqz v3, :cond_0

    .line 32
    .line 33
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    int-to-float v0, v0

    .line 46
    aget-object v1, v1, v2

    .line 47
    .line 48
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    mul-float v0, v0, v1

    .line 53
    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    if-eqz v1, :cond_1

    .line 59
    .line 60
    new-instance v3, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 61
    .line 62
    invoke-direct {v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v3, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 66
    .line 67
    .line 68
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 69
    .line 70
    .line 71
    move-result-object v4

    .line 72
    invoke-virtual {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 73
    .line 74
    .line 75
    move-result v5

    .line 76
    const-wide v6, 0x3fc999999999999aL    # 0.2

    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    invoke-virtual {v4, v5, v6, v7}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 82
    .line 83
    .line 84
    move-result v4

    .line 85
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 86
    .line 87
    .line 88
    goto :goto_0

    .line 89
    :cond_1
    move-object v3, v1

    .line 90
    :goto_0
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 91
    .line 92
    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 93
    .line 94
    .line 95
    new-instance v5, Landroid/graphics/Path;

    .line 96
    .line 97
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 98
    .line 99
    .line 100
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 101
    .line 102
    int-to-float v6, v6

    .line 103
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 104
    .line 105
    int-to-float v7, v7

    .line 106
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 107
    .line 108
    .line 109
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 110
    .line 111
    int-to-float v6, v6

    .line 112
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 113
    .line 114
    int-to-float v7, v7

    .line 115
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 116
    .line 117
    .line 118
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 119
    .line 120
    int-to-float v6, v6

    .line 121
    sub-float/2addr v6, v0

    .line 122
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 123
    .line 124
    int-to-float v7, v7

    .line 125
    add-float/2addr v7, v0

    .line 126
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 127
    .line 128
    .line 129
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 130
    .line 131
    int-to-float v6, v6

    .line 132
    add-float/2addr v6, v0

    .line 133
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 134
    .line 135
    int-to-float v7, v7

    .line 136
    add-float/2addr v7, v0

    .line 137
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 141
    .line 142
    .line 143
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 144
    .line 145
    .line 146
    move-result-object v6

    .line 147
    invoke-virtual {v4, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 148
    .line 149
    .line 150
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 151
    .line 152
    .line 153
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 154
    .line 155
    .line 156
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 157
    .line 158
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    .line 160
    .line 161
    if-eqz v1, :cond_2

    .line 162
    .line 163
    new-instance v3, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 164
    .line 165
    invoke-direct {v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 166
    .line 167
    .line 168
    invoke-virtual {v3, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 169
    .line 170
    .line 171
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 172
    .line 173
    .line 174
    move-result-object v4

    .line 175
    invoke-virtual {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 176
    .line 177
    .line 178
    move-result v5

    .line 179
    const-wide v6, -0x4026666666666666L    # -0.4

    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    invoke-virtual {v4, v5, v6, v7}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 185
    .line 186
    .line 187
    move-result v4

    .line 188
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 189
    .line 190
    .line 191
    :cond_2
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 192
    .line 193
    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 194
    .line 195
    .line 196
    new-instance v5, Landroid/graphics/Path;

    .line 197
    .line 198
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 199
    .line 200
    .line 201
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 202
    .line 203
    int-to-float v6, v6

    .line 204
    sub-float/2addr v6, v0

    .line 205
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 206
    .line 207
    int-to-float v7, v7

    .line 208
    add-float/2addr v7, v0

    .line 209
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 210
    .line 211
    .line 212
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 213
    .line 214
    int-to-float v6, v6

    .line 215
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 216
    .line 217
    int-to-float v7, v7

    .line 218
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 219
    .line 220
    .line 221
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 222
    .line 223
    int-to-float v6, v6

    .line 224
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 225
    .line 226
    int-to-float v7, v7

    .line 227
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 228
    .line 229
    .line 230
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 231
    .line 232
    int-to-float v6, v6

    .line 233
    sub-float/2addr v6, v0

    .line 234
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 235
    .line 236
    int-to-float v7, v7

    .line 237
    sub-float/2addr v7, v0

    .line 238
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 239
    .line 240
    .line 241
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 242
    .line 243
    .line 244
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 245
    .line 246
    .line 247
    move-result-object v6

    .line 248
    invoke-virtual {v4, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 249
    .line 250
    .line 251
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 252
    .line 253
    .line 254
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 255
    .line 256
    .line 257
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 258
    .line 259
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 260
    .line 261
    .line 262
    if-eqz v1, :cond_3

    .line 263
    .line 264
    new-instance v3, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 265
    .line 266
    invoke-direct {v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 267
    .line 268
    .line 269
    invoke-virtual {v3, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 270
    .line 271
    .line 272
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 273
    .line 274
    .line 275
    move-result-object v4

    .line 276
    invoke-virtual {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 277
    .line 278
    .line 279
    move-result v5

    .line 280
    const-wide v6, -0x4036666666666666L    # -0.2

    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    invoke-virtual {v4, v5, v6, v7}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 286
    .line 287
    .line 288
    move-result v4

    .line 289
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 290
    .line 291
    .line 292
    :cond_3
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 293
    .line 294
    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 295
    .line 296
    .line 297
    new-instance v5, Landroid/graphics/Path;

    .line 298
    .line 299
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 300
    .line 301
    .line 302
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 303
    .line 304
    int-to-float v6, v6

    .line 305
    add-float/2addr v6, v0

    .line 306
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 307
    .line 308
    int-to-float v7, v7

    .line 309
    sub-float/2addr v7, v0

    .line 310
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 311
    .line 312
    .line 313
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 314
    .line 315
    int-to-float v6, v6

    .line 316
    sub-float/2addr v6, v0

    .line 317
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 318
    .line 319
    int-to-float v7, v7

    .line 320
    sub-float/2addr v7, v0

    .line 321
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 322
    .line 323
    .line 324
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 325
    .line 326
    int-to-float v6, v6

    .line 327
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 328
    .line 329
    int-to-float v7, v7

    .line 330
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 331
    .line 332
    .line 333
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 334
    .line 335
    int-to-float v6, v6

    .line 336
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 337
    .line 338
    int-to-float v7, v7

    .line 339
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 340
    .line 341
    .line 342
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 343
    .line 344
    .line 345
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 346
    .line 347
    .line 348
    move-result-object v6

    .line 349
    invoke-virtual {v4, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 350
    .line 351
    .line 352
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 353
    .line 354
    .line 355
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 356
    .line 357
    .line 358
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 359
    .line 360
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 361
    .line 362
    .line 363
    if-eqz v1, :cond_4

    .line 364
    .line 365
    new-instance v3, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 366
    .line 367
    invoke-direct {v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 368
    .line 369
    .line 370
    invoke-virtual {v3, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 371
    .line 372
    .line 373
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 374
    .line 375
    .line 376
    move-result-object v2

    .line 377
    invoke-virtual {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 378
    .line 379
    .line 380
    move-result v4

    .line 381
    const-wide v5, 0x3fd999999999999aL    # 0.4

    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    invoke-virtual {v2, v4, v5, v6}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 387
    .line 388
    .line 389
    move-result v2

    .line 390
    invoke-virtual {v3, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 391
    .line 392
    .line 393
    :cond_4
    new-instance v2, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 394
    .line 395
    invoke-direct {v2}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 396
    .line 397
    .line 398
    new-instance v4, Landroid/graphics/Path;

    .line 399
    .line 400
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 401
    .line 402
    .line 403
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 404
    .line 405
    int-to-float v5, v5

    .line 406
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 407
    .line 408
    int-to-float v6, v6

    .line 409
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 410
    .line 411
    .line 412
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 413
    .line 414
    int-to-float v5, v5

    .line 415
    add-float/2addr v5, v0

    .line 416
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 417
    .line 418
    int-to-float v6, v6

    .line 419
    add-float/2addr v6, v0

    .line 420
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 421
    .line 422
    .line 423
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 424
    .line 425
    int-to-float v5, v5

    .line 426
    add-float/2addr v5, v0

    .line 427
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 428
    .line 429
    int-to-float v6, v6

    .line 430
    sub-float/2addr v6, v0

    .line 431
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 432
    .line 433
    .line 434
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 435
    .line 436
    int-to-float v5, v5

    .line 437
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 438
    .line 439
    int-to-float v6, v6

    .line 440
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 441
    .line 442
    .line 443
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 444
    .line 445
    .line 446
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 447
    .line 448
    .line 449
    move-result-object v5

    .line 450
    invoke-virtual {v2, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 451
    .line 452
    .line 453
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 454
    .line 455
    .line 456
    invoke-virtual {v2, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 457
    .line 458
    .line 459
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 460
    .line 461
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 462
    .line 463
    .line 464
    new-instance v2, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 465
    .line 466
    invoke-direct {v2}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 467
    .line 468
    .line 469
    new-instance v9, Landroid/graphics/Path;

    .line 470
    .line 471
    invoke-direct {v9}, Landroid/graphics/Path;-><init>()V

    .line 472
    .line 473
    .line 474
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 475
    .line 476
    int-to-float v3, v3

    .line 477
    add-float v4, v3, v0

    .line 478
    .line 479
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 480
    .line 481
    int-to-float v3, v3

    .line 482
    add-float v5, v3, v0

    .line 483
    .line 484
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 485
    .line 486
    int-to-float v3, v3

    .line 487
    sub-float v6, v3, v0

    .line 488
    .line 489
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 490
    .line 491
    int-to-float p1, p1

    .line 492
    sub-float v7, p1, v0

    .line 493
    .line 494
    sget-object v8, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 495
    .line 496
    move-object v3, v9

    .line 497
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 498
    .line 499
    .line 500
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 501
    .line 502
    .line 503
    move-result-object p0

    .line 504
    invoke-virtual {v2, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 505
    .line 506
    .line 507
    invoke-virtual {v2, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 508
    .line 509
    .line 510
    invoke-virtual {v2, v9}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 511
    .line 512
    .line 513
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 514
    .line 515
    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 516
    .line 517
    .line 518
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 519
    .line 520
    return-object p0
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getBlockArcPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 9

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const/high16 v1, 0x3e800000    # 0.25f

    .line 15
    .line 16
    mul-float v0, v0, v1

    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 23
    .line 24
    .line 25
    move-result p0

    .line 26
    const/4 v3, 0x0

    .line 27
    const/4 v4, 0x2

    .line 28
    const/high16 v5, 0x43b40000    # 360.0f

    .line 29
    .line 30
    const/4 v6, 0x1

    .line 31
    const/high16 v7, 0x43340000    # 180.0f

    .line 32
    .line 33
    const/4 v8, 0x0

    .line 34
    if-eqz p0, :cond_2

    .line 35
    .line 36
    if-eqz v2, :cond_7

    .line 37
    .line 38
    array-length p0, v2

    .line 39
    const/4 v1, 0x3

    .line 40
    if-lt p0, v1, :cond_7

    .line 41
    .line 42
    aget-object p0, v2, v3

    .line 43
    .line 44
    const/high16 v1, 0x40c00000    # 6.0f

    .line 45
    .line 46
    const/high16 v3, 0x41200000    # 10.0f

    .line 47
    .line 48
    if-eqz p0, :cond_0

    .line 49
    .line 50
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 51
    .line 52
    .line 53
    move-result p0

    .line 54
    mul-float p0, p0, v3

    .line 55
    .line 56
    div-float v7, p0, v1

    .line 57
    .line 58
    :cond_0
    aget-object p0, v2, v6

    .line 59
    .line 60
    if-eqz p0, :cond_1

    .line 61
    .line 62
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 63
    .line 64
    .line 65
    move-result p0

    .line 66
    mul-float p0, p0, v3

    .line 67
    .line 68
    div-float v8, p0, v1

    .line 69
    .line 70
    :cond_1
    aget-object p0, v2, v4

    .line 71
    .line 72
    if-eqz p0, :cond_7

    .line 73
    .line 74
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 75
    .line 76
    .line 77
    move-result p0

    .line 78
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 79
    .line 80
    .line 81
    move-result v0

    .line 82
    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    .line 83
    .line 84
    .line 85
    move-result p0

    .line 86
    int-to-float p0, p0

    .line 87
    aget-object v0, v2, v4

    .line 88
    .line 89
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    mul-float v0, v0, p0

    .line 94
    .line 95
    goto :goto_4

    .line 96
    :cond_2
    if-eqz v2, :cond_5

    .line 97
    .line 98
    array-length p0, v2

    .line 99
    if-lt p0, v6, :cond_5

    .line 100
    .line 101
    aget-object p0, v2, v3

    .line 102
    .line 103
    if-eqz p0, :cond_3

    .line 104
    .line 105
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 106
    .line 107
    .line 108
    move-result p0

    .line 109
    const v0, 0x3ea8baba

    .line 110
    .line 111
    .line 112
    mul-float p0, p0, v0

    .line 113
    .line 114
    goto :goto_0

    .line 115
    :cond_3
    const/4 p0, 0x0

    .line 116
    :goto_0
    array-length v0, v2

    .line 117
    if-lt v0, v4, :cond_4

    .line 118
    .line 119
    aget-object v0, v2, v6

    .line 120
    .line 121
    if-eqz v0, :cond_4

    .line 122
    .line 123
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 124
    .line 125
    .line 126
    move-result v0

    .line 127
    int-to-float v0, v0

    .line 128
    aget-object v1, v2, v6

    .line 129
    .line 130
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 131
    .line 132
    .line 133
    move-result v1

    .line 134
    goto :goto_1

    .line 135
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 136
    .line 137
    .line 138
    move-result v0

    .line 139
    int-to-float v0, v0

    .line 140
    :goto_1
    mul-float v0, v0, v1

    .line 141
    .line 142
    goto :goto_2

    .line 143
    :cond_5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 144
    .line 145
    .line 146
    move-result p0

    .line 147
    int-to-float p0, p0

    .line 148
    mul-float p0, p0, v1

    .line 149
    .line 150
    move v0, p0

    .line 151
    const/high16 p0, 0x43340000    # 180.0f

    .line 152
    .line 153
    :goto_2
    cmpl-float v1, p0, v8

    .line 154
    .line 155
    if-ltz v1, :cond_6

    .line 156
    .line 157
    const/high16 v1, 0x42b40000    # 90.0f

    .line 158
    .line 159
    sub-float v2, v1, p0

    .line 160
    .line 161
    add-float v8, v2, v1

    .line 162
    .line 163
    goto :goto_3

    .line 164
    :cond_6
    add-float/2addr p0, v5

    .line 165
    sub-float v1, p0, v7

    .line 166
    .line 167
    sub-float v8, v5, v1

    .line 168
    .line 169
    :goto_3
    move v7, p0

    .line 170
    :cond_7
    :goto_4
    cmpl-float p0, v8, v7

    .line 171
    .line 172
    if-ltz p0, :cond_8

    .line 173
    .line 174
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 175
    .line 176
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 177
    .line 178
    int-to-float v1, v1

    .line 179
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 180
    .line 181
    int-to-float v2, v2

    .line 182
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 183
    .line 184
    int-to-float v3, v3

    .line 185
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 186
    .line 187
    int-to-float v4, v4

    .line 188
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 189
    .line 190
    .line 191
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 192
    .line 193
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 194
    .line 195
    sub-float v2, v8, v7

    .line 196
    .line 197
    invoke-virtual {p0, v1, v7, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 198
    .line 199
    .line 200
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 201
    .line 202
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 203
    .line 204
    int-to-float v1, v1

    .line 205
    add-float/2addr v1, v0

    .line 206
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 207
    .line 208
    int-to-float v2, v2

    .line 209
    add-float/2addr v2, v0

    .line 210
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 211
    .line 212
    int-to-float v3, v3

    .line 213
    sub-float/2addr v3, v0

    .line 214
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 215
    .line 216
    int-to-float p1, p1

    .line 217
    sub-float/2addr p1, v0

    .line 218
    invoke-virtual {p0, v1, v2, v3, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 219
    .line 220
    .line 221
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 222
    .line 223
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 224
    .line 225
    sub-float/2addr v7, v8

    .line 226
    invoke-virtual {p0, p1, v8, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 227
    .line 228
    .line 229
    goto :goto_5

    .line 230
    :cond_8
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 231
    .line 232
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 233
    .line 234
    int-to-float v1, v1

    .line 235
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 236
    .line 237
    int-to-float v2, v2

    .line 238
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 239
    .line 240
    int-to-float v3, v3

    .line 241
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 242
    .line 243
    int-to-float v4, v4

    .line 244
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 245
    .line 246
    .line 247
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 248
    .line 249
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 250
    .line 251
    add-float v2, v8, v5

    .line 252
    .line 253
    sub-float/2addr v2, v7

    .line 254
    invoke-virtual {p0, v1, v7, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 255
    .line 256
    .line 257
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 258
    .line 259
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 260
    .line 261
    int-to-float v1, v1

    .line 262
    add-float/2addr v1, v0

    .line 263
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 264
    .line 265
    int-to-float v2, v2

    .line 266
    add-float/2addr v2, v0

    .line 267
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 268
    .line 269
    int-to-float v3, v3

    .line 270
    sub-float/2addr v3, v0

    .line 271
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 272
    .line 273
    int-to-float p1, p1

    .line 274
    sub-float/2addr p1, v0

    .line 275
    invoke-virtual {p0, v1, v2, v3, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 276
    .line 277
    .line 278
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 279
    .line 280
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 281
    .line 282
    sub-float/2addr v7, v8

    .line 283
    sub-float/2addr v7, v5

    .line 284
    invoke-virtual {p0, p1, v8, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 285
    .line 286
    .line 287
    :goto_5
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 288
    .line 289
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 290
    .line 291
    .line 292
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 293
    .line 294
    return-object p0
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getBracePairPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    int-to-float v1, v1

    .line 16
    const v2, 0x3da3d70a    # 0.08f

    .line 17
    .line 18
    .line 19
    mul-float v1, v1, v2

    .line 20
    .line 21
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    if-eqz v2, :cond_0

    .line 26
    .line 27
    array-length v3, v2

    .line 28
    const/4 v4, 0x1

    .line 29
    if-lt v3, v4, :cond_0

    .line 30
    .line 31
    const/4 v3, 0x0

    .line 32
    aget-object v4, v2, v3

    .line 33
    .line 34
    if-eqz v4, :cond_0

    .line 35
    .line 36
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 41
    .line 42
    .line 43
    move-result v4

    .line 44
    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    int-to-float v1, v1

    .line 49
    aget-object v2, v2, v3

    .line 50
    .line 51
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    mul-float v1, v1, v2

    .line 56
    .line 57
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    const/high16 v3, 0x40000000    # 2.0f

    .line 62
    .line 63
    const/high16 v4, 0x40400000    # 3.0f

    .line 64
    .line 65
    const/high16 v6, 0x43340000    # 180.0f

    .line 66
    .line 67
    const/high16 v7, 0x43870000    # 270.0f

    .line 68
    .line 69
    const/high16 v8, -0x3d4c0000    # -90.0f

    .line 70
    .line 71
    const/high16 v9, 0x42b40000    # 90.0f

    .line 72
    .line 73
    if-eqz v2, :cond_1

    .line 74
    .line 75
    new-instance v10, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 76
    .line 77
    invoke-direct {v10}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 78
    .line 79
    .line 80
    new-instance v11, Landroid/graphics/Path;

    .line 81
    .line 82
    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 83
    .line 84
    .line 85
    sget-object v12, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 86
    .line 87
    iget v13, v0, Landroid/graphics/Rect;->right:I

    .line 88
    .line 89
    int-to-float v14, v13

    .line 90
    mul-float v15, v1, v4

    .line 91
    .line 92
    sub-float/2addr v14, v15

    .line 93
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 94
    .line 95
    int-to-float v5, v4

    .line 96
    int-to-float v13, v13

    .line 97
    sub-float/2addr v13, v1

    .line 98
    int-to-float v4, v4

    .line 99
    mul-float v16, v1, v3

    .line 100
    .line 101
    add-float v4, v4, v16

    .line 102
    .line 103
    invoke-virtual {v12, v14, v5, v13, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 104
    .line 105
    .line 106
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 107
    .line 108
    invoke-virtual {v11, v4, v7, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 109
    .line 110
    .line 111
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 112
    .line 113
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 114
    .line 115
    int-to-float v5, v5

    .line 116
    sub-float/2addr v5, v1

    .line 117
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 118
    .line 119
    .line 120
    move-result v12

    .line 121
    sub-float v12, v12, v16

    .line 122
    .line 123
    iget v13, v0, Landroid/graphics/Rect;->right:I

    .line 124
    .line 125
    int-to-float v13, v13

    .line 126
    add-float/2addr v13, v1

    .line 127
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 128
    .line 129
    .line 130
    move-result v14

    .line 131
    invoke-virtual {v4, v5, v12, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 132
    .line 133
    .line 134
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 135
    .line 136
    invoke-virtual {v11, v4, v6, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 137
    .line 138
    .line 139
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 140
    .line 141
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 142
    .line 143
    int-to-float v5, v5

    .line 144
    sub-float/2addr v5, v1

    .line 145
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 146
    .line 147
    .line 148
    move-result v12

    .line 149
    iget v13, v0, Landroid/graphics/Rect;->right:I

    .line 150
    .line 151
    int-to-float v13, v13

    .line 152
    add-float/2addr v13, v1

    .line 153
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 154
    .line 155
    .line 156
    move-result v14

    .line 157
    add-float v14, v14, v16

    .line 158
    .line 159
    invoke-virtual {v4, v5, v12, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 160
    .line 161
    .line 162
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 163
    .line 164
    invoke-virtual {v11, v4, v7, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 165
    .line 166
    .line 167
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 168
    .line 169
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 170
    .line 171
    int-to-float v12, v5

    .line 172
    sub-float/2addr v12, v15

    .line 173
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 174
    .line 175
    int-to-float v14, v13

    .line 176
    sub-float v14, v14, v16

    .line 177
    .line 178
    int-to-float v5, v5

    .line 179
    sub-float/2addr v5, v1

    .line 180
    int-to-float v13, v13

    .line 181
    invoke-virtual {v4, v12, v14, v5, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 182
    .line 183
    .line 184
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 185
    .line 186
    const/4 v5, 0x0

    .line 187
    invoke-virtual {v11, v4, v5, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 188
    .line 189
    .line 190
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 191
    .line 192
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 193
    .line 194
    int-to-float v12, v5

    .line 195
    add-float/2addr v12, v1

    .line 196
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 197
    .line 198
    int-to-float v14, v13

    .line 199
    sub-float v14, v14, v16

    .line 200
    .line 201
    int-to-float v5, v5

    .line 202
    add-float/2addr v5, v15

    .line 203
    int-to-float v13, v13

    .line 204
    invoke-virtual {v4, v12, v14, v5, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 205
    .line 206
    .line 207
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 208
    .line 209
    invoke-virtual {v11, v4, v9, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 210
    .line 211
    .line 212
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 213
    .line 214
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 215
    .line 216
    int-to-float v5, v5

    .line 217
    sub-float/2addr v5, v1

    .line 218
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 219
    .line 220
    .line 221
    move-result v12

    .line 222
    iget v13, v0, Landroid/graphics/Rect;->left:I

    .line 223
    .line 224
    int-to-float v13, v13

    .line 225
    add-float/2addr v13, v1

    .line 226
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 227
    .line 228
    .line 229
    move-result v14

    .line 230
    add-float v14, v14, v16

    .line 231
    .line 232
    invoke-virtual {v4, v5, v12, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 233
    .line 234
    .line 235
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 236
    .line 237
    const/4 v5, 0x0

    .line 238
    invoke-virtual {v11, v4, v5, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 239
    .line 240
    .line 241
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 242
    .line 243
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 244
    .line 245
    int-to-float v5, v5

    .line 246
    sub-float/2addr v5, v1

    .line 247
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 248
    .line 249
    .line 250
    move-result v12

    .line 251
    sub-float v12, v12, v16

    .line 252
    .line 253
    iget v13, v0, Landroid/graphics/Rect;->left:I

    .line 254
    .line 255
    int-to-float v13, v13

    .line 256
    add-float/2addr v13, v1

    .line 257
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 258
    .line 259
    .line 260
    move-result v14

    .line 261
    invoke-virtual {v4, v5, v12, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 262
    .line 263
    .line 264
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 265
    .line 266
    invoke-virtual {v11, v4, v9, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 267
    .line 268
    .line 269
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 270
    .line 271
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 272
    .line 273
    int-to-float v12, v5

    .line 274
    add-float/2addr v12, v1

    .line 275
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 276
    .line 277
    int-to-float v14, v13

    .line 278
    int-to-float v5, v5

    .line 279
    add-float/2addr v5, v15

    .line 280
    int-to-float v13, v13

    .line 281
    add-float v13, v13, v16

    .line 282
    .line 283
    invoke-virtual {v4, v12, v14, v5, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 284
    .line 285
    .line 286
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 287
    .line 288
    invoke-virtual {v11, v4, v6, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 289
    .line 290
    .line 291
    invoke-virtual {v11}, Landroid/graphics/Path;->close()V

    .line 292
    .line 293
    .line 294
    invoke-virtual {v10, v11}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 295
    .line 296
    .line 297
    invoke-virtual {v10, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 298
    .line 299
    .line 300
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 301
    .line 302
    invoke-interface {v2, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 303
    .line 304
    .line 305
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 306
    .line 307
    .line 308
    move-result v2

    .line 309
    if-eqz v2, :cond_2

    .line 310
    .line 311
    new-instance v2, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 312
    .line 313
    invoke-direct {v2}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 314
    .line 315
    .line 316
    new-instance v4, Landroid/graphics/Path;

    .line 317
    .line 318
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 319
    .line 320
    .line 321
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 322
    .line 323
    int-to-float v5, v5

    .line 324
    mul-float v3, v3, v1

    .line 325
    .line 326
    sub-float/2addr v5, v3

    .line 327
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 328
    .line 329
    int-to-float v10, v10

    .line 330
    invoke-virtual {v4, v5, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 331
    .line 332
    .line 333
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 334
    .line 335
    iget v10, v0, Landroid/graphics/Rect;->right:I

    .line 336
    .line 337
    int-to-float v11, v10

    .line 338
    const/high16 v12, 0x40400000    # 3.0f

    .line 339
    .line 340
    mul-float v12, v12, v1

    .line 341
    .line 342
    sub-float/2addr v11, v12

    .line 343
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 344
    .line 345
    int-to-float v14, v13

    .line 346
    int-to-float v10, v10

    .line 347
    sub-float/2addr v10, v1

    .line 348
    int-to-float v13, v13

    .line 349
    add-float/2addr v13, v3

    .line 350
    invoke-virtual {v5, v11, v14, v10, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 351
    .line 352
    .line 353
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 354
    .line 355
    invoke-virtual {v4, v5, v7, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 356
    .line 357
    .line 358
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 359
    .line 360
    iget v10, v0, Landroid/graphics/Rect;->right:I

    .line 361
    .line 362
    int-to-float v10, v10

    .line 363
    sub-float/2addr v10, v1

    .line 364
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 365
    .line 366
    .line 367
    move-result v11

    .line 368
    sub-float/2addr v11, v3

    .line 369
    iget v13, v0, Landroid/graphics/Rect;->right:I

    .line 370
    .line 371
    int-to-float v13, v13

    .line 372
    add-float/2addr v13, v1

    .line 373
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 374
    .line 375
    .line 376
    move-result v14

    .line 377
    invoke-virtual {v5, v10, v11, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 378
    .line 379
    .line 380
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 381
    .line 382
    invoke-virtual {v4, v5, v6, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 383
    .line 384
    .line 385
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 386
    .line 387
    iget v10, v0, Landroid/graphics/Rect;->right:I

    .line 388
    .line 389
    int-to-float v10, v10

    .line 390
    sub-float/2addr v10, v1

    .line 391
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 392
    .line 393
    .line 394
    move-result v11

    .line 395
    iget v13, v0, Landroid/graphics/Rect;->right:I

    .line 396
    .line 397
    int-to-float v13, v13

    .line 398
    add-float/2addr v13, v1

    .line 399
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 400
    .line 401
    .line 402
    move-result v14

    .line 403
    add-float/2addr v14, v3

    .line 404
    invoke-virtual {v5, v10, v11, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 405
    .line 406
    .line 407
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 408
    .line 409
    invoke-virtual {v4, v5, v7, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 410
    .line 411
    .line 412
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 413
    .line 414
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 415
    .line 416
    int-to-float v10, v7

    .line 417
    sub-float/2addr v10, v12

    .line 418
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 419
    .line 420
    int-to-float v13, v11

    .line 421
    sub-float/2addr v13, v3

    .line 422
    int-to-float v7, v7

    .line 423
    sub-float/2addr v7, v1

    .line 424
    int-to-float v11, v11

    .line 425
    invoke-virtual {v5, v10, v13, v7, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 426
    .line 427
    .line 428
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 429
    .line 430
    const/4 v7, 0x0

    .line 431
    invoke-virtual {v4, v5, v7, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 432
    .line 433
    .line 434
    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 435
    .line 436
    int-to-float v5, v5

    .line 437
    add-float/2addr v5, v3

    .line 438
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    .line 439
    .line 440
    int-to-float v7, v7

    .line 441
    invoke-virtual {v4, v5, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 442
    .line 443
    .line 444
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 445
    .line 446
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 447
    .line 448
    int-to-float v10, v7

    .line 449
    add-float/2addr v10, v1

    .line 450
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 451
    .line 452
    int-to-float v13, v11

    .line 453
    sub-float/2addr v13, v3

    .line 454
    int-to-float v7, v7

    .line 455
    add-float/2addr v7, v12

    .line 456
    int-to-float v11, v11

    .line 457
    invoke-virtual {v5, v10, v13, v7, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 458
    .line 459
    .line 460
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 461
    .line 462
    invoke-virtual {v4, v5, v9, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 463
    .line 464
    .line 465
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 466
    .line 467
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 468
    .line 469
    int-to-float v7, v7

    .line 470
    sub-float/2addr v7, v1

    .line 471
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 472
    .line 473
    .line 474
    move-result v10

    .line 475
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 476
    .line 477
    int-to-float v11, v11

    .line 478
    add-float/2addr v11, v1

    .line 479
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 480
    .line 481
    .line 482
    move-result v13

    .line 483
    add-float/2addr v13, v3

    .line 484
    invoke-virtual {v5, v7, v10, v11, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 485
    .line 486
    .line 487
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 488
    .line 489
    const/4 v7, 0x0

    .line 490
    invoke-virtual {v4, v5, v7, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 491
    .line 492
    .line 493
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 494
    .line 495
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 496
    .line 497
    int-to-float v7, v7

    .line 498
    sub-float/2addr v7, v1

    .line 499
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 500
    .line 501
    .line 502
    move-result v10

    .line 503
    sub-float/2addr v10, v3

    .line 504
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 505
    .line 506
    int-to-float v11, v11

    .line 507
    add-float/2addr v11, v1

    .line 508
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 509
    .line 510
    .line 511
    move-result v13

    .line 512
    invoke-virtual {v5, v7, v10, v11, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 513
    .line 514
    .line 515
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 516
    .line 517
    invoke-virtual {v4, v5, v9, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 518
    .line 519
    .line 520
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 521
    .line 522
    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 523
    .line 524
    int-to-float v8, v7

    .line 525
    add-float/2addr v8, v1

    .line 526
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 527
    .line 528
    int-to-float v1, v0

    .line 529
    int-to-float v7, v7

    .line 530
    add-float/2addr v7, v12

    .line 531
    int-to-float v0, v0

    .line 532
    add-float/2addr v0, v3

    .line 533
    invoke-virtual {v5, v8, v1, v7, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 534
    .line 535
    .line 536
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 537
    .line 538
    invoke-virtual {v4, v0, v6, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 539
    .line 540
    .line 541
    invoke-virtual {v2, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 542
    .line 543
    .line 544
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 545
    .line 546
    .line 547
    move-result-object v0

    .line 548
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 549
    .line 550
    .line 551
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 552
    .line 553
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 554
    .line 555
    .line 556
    :cond_2
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 557
    .line 558
    return-object v0
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getBracketPairPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x1

    .line 6
    const/4 v2, 0x0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    array-length v3, v0

    .line 10
    if-lt v3, v1, :cond_0

    .line 11
    .line 12
    aget-object v3, v0, v2

    .line 13
    .line 14
    if-eqz v3, :cond_0

    .line 15
    .line 16
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 21
    .line 22
    .line 23
    move-result v4

    .line 24
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    int-to-float v3, v3

    .line 29
    aget-object v0, v0, v2

    .line 30
    .line 31
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    mul-float v3, v3, v0

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 43
    .line 44
    .line 45
    move-result v3

    .line 46
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    int-to-float v0, v0

    .line 51
    const v3, 0x3e3851ec    # 0.18f

    .line 52
    .line 53
    .line 54
    mul-float v3, v3, v0

    .line 55
    .line 56
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    if-eqz v0, :cond_1

    .line 61
    .line 62
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 63
    .line 64
    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 65
    .line 66
    .line 67
    new-instance v5, Landroid/graphics/Path;

    .line 68
    .line 69
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 70
    .line 71
    .line 72
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 73
    .line 74
    iget v7, p1, Landroid/graphics/Rect;->left:I

    .line 75
    .line 76
    int-to-float v7, v7

    .line 77
    iget v8, p1, Landroid/graphics/Rect;->top:I

    .line 78
    .line 79
    int-to-float v8, v8

    .line 80
    iget v9, p1, Landroid/graphics/Rect;->right:I

    .line 81
    .line 82
    int-to-float v9, v9

    .line 83
    iget v10, p1, Landroid/graphics/Rect;->bottom:I

    .line 84
    .line 85
    int-to-float v10, v10

    .line 86
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 87
    .line 88
    .line 89
    const/16 v6, 0x8

    .line 90
    .line 91
    new-array v6, v6, [F

    .line 92
    .line 93
    aput v3, v6, v2

    .line 94
    .line 95
    aput v3, v6, v1

    .line 96
    .line 97
    const/4 v1, 0x2

    .line 98
    aput v3, v6, v1

    .line 99
    .line 100
    const/4 v1, 0x3

    .line 101
    aput v3, v6, v1

    .line 102
    .line 103
    const/4 v1, 0x4

    .line 104
    aput v3, v6, v1

    .line 105
    .line 106
    const/4 v1, 0x5

    .line 107
    aput v3, v6, v1

    .line 108
    .line 109
    const/4 v1, 0x6

    .line 110
    aput v3, v6, v1

    .line 111
    .line 112
    const/4 v1, 0x7

    .line 113
    aput v3, v6, v1

    .line 114
    .line 115
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 116
    .line 117
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 118
    .line 119
    invoke-virtual {v5, v1, v6, v2}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 123
    .line 124
    .line 125
    invoke-virtual {v4, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 126
    .line 127
    .line 128
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 129
    .line 130
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    .line 132
    .line 133
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 134
    .line 135
    .line 136
    move-result v0

    .line 137
    if-eqz v0, :cond_2

    .line 138
    .line 139
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 140
    .line 141
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 142
    .line 143
    .line 144
    new-instance v1, Landroid/graphics/Path;

    .line 145
    .line 146
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 147
    .line 148
    .line 149
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 150
    .line 151
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 152
    .line 153
    int-to-float v5, v4

    .line 154
    const/high16 v6, 0x40000000    # 2.0f

    .line 155
    .line 156
    mul-float v6, v6, v3

    .line 157
    .line 158
    sub-float/2addr v5, v6

    .line 159
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 160
    .line 161
    int-to-float v8, v7

    .line 162
    int-to-float v4, v4

    .line 163
    int-to-float v7, v7

    .line 164
    add-float/2addr v7, v6

    .line 165
    invoke-virtual {v2, v5, v8, v4, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 166
    .line 167
    .line 168
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 169
    .line 170
    const/high16 v4, 0x43870000    # 270.0f

    .line 171
    .line 172
    const/high16 v5, 0x42b40000    # 90.0f

    .line 173
    .line 174
    invoke-virtual {v1, v2, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 175
    .line 176
    .line 177
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 178
    .line 179
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 180
    .line 181
    int-to-float v7, v4

    .line 182
    sub-float/2addr v7, v6

    .line 183
    iget v8, p1, Landroid/graphics/Rect;->bottom:I

    .line 184
    .line 185
    int-to-float v9, v8

    .line 186
    sub-float/2addr v9, v6

    .line 187
    int-to-float v4, v4

    .line 188
    int-to-float v8, v8

    .line 189
    invoke-virtual {v2, v7, v9, v4, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 190
    .line 191
    .line 192
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 193
    .line 194
    const/4 v4, 0x0

    .line 195
    invoke-virtual {v1, v2, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 196
    .line 197
    .line 198
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 199
    .line 200
    int-to-float v2, v2

    .line 201
    add-float/2addr v2, v3

    .line 202
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 203
    .line 204
    int-to-float v3, v3

    .line 205
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 206
    .line 207
    .line 208
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 209
    .line 210
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 211
    .line 212
    int-to-float v4, v3

    .line 213
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 214
    .line 215
    int-to-float v8, v7

    .line 216
    sub-float/2addr v8, v6

    .line 217
    int-to-float v3, v3

    .line 218
    add-float/2addr v3, v6

    .line 219
    int-to-float v7, v7

    .line 220
    invoke-virtual {v2, v4, v8, v3, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 221
    .line 222
    .line 223
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 224
    .line 225
    invoke-virtual {v1, v2, v5, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 226
    .line 227
    .line 228
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 229
    .line 230
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 231
    .line 232
    int-to-float v4, v3

    .line 233
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 234
    .line 235
    int-to-float v7, p1

    .line 236
    int-to-float v3, v3

    .line 237
    add-float/2addr v3, v6

    .line 238
    int-to-float p1, p1

    .line 239
    add-float/2addr p1, v6

    .line 240
    invoke-virtual {v2, v4, v7, v3, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 241
    .line 242
    .line 243
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 244
    .line 245
    const/high16 v2, 0x43340000    # 180.0f

    .line 246
    .line 247
    invoke-virtual {v1, p1, v2, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 248
    .line 249
    .line 250
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 251
    .line 252
    .line 253
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 254
    .line 255
    .line 256
    move-result-object p0

    .line 257
    invoke-virtual {v0, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 258
    .line 259
    .line 260
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 261
    .line 262
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    .line 264
    .line 265
    :cond_2
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 266
    .line 267
    return-object p0
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getCanPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x0

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    int-to-float v1, v1

    .line 25
    const v3, 0x3e333333    # 0.175f

    .line 26
    .line 27
    .line 28
    mul-float v1, v1, v3

    .line 29
    .line 30
    if-eqz v0, :cond_1

    .line 31
    .line 32
    array-length v3, v0

    .line 33
    const/4 v4, 0x1

    .line 34
    if-lt v3, v4, :cond_1

    .line 35
    .line 36
    aget-object v3, v0, v2

    .line 37
    .line 38
    if-eqz v3, :cond_1

    .line 39
    .line 40
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v3

    .line 48
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    int-to-float v1, v1

    .line 53
    aget-object v0, v0, v2

    .line 54
    .line 55
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    goto :goto_0

    .line 60
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    int-to-float v1, v1

    .line 65
    const/high16 v3, 0x3e800000    # 0.25f

    .line 66
    .line 67
    mul-float v1, v1, v3

    .line 68
    .line 69
    if-eqz v0, :cond_1

    .line 70
    .line 71
    array-length v3, v0

    .line 72
    if-lez v3, :cond_1

    .line 73
    .line 74
    aget-object v3, v0, v2

    .line 75
    .line 76
    if-eqz v3, :cond_1

    .line 77
    .line 78
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 79
    .line 80
    .line 81
    move-result v1

    .line 82
    int-to-float v1, v1

    .line 83
    aget-object v0, v0, v2

    .line 84
    .line 85
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    :goto_0
    mul-float v1, v1, v0

    .line 90
    .line 91
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    if-eqz v0, :cond_2

    .line 96
    .line 97
    new-instance v3, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 98
    .line 99
    invoke-direct {v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 100
    .line 101
    .line 102
    invoke-virtual {v3, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 103
    .line 104
    .line 105
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 106
    .line 107
    .line 108
    move-result-object v2

    .line 109
    invoke-virtual {v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 110
    .line 111
    .line 112
    move-result v4

    .line 113
    const-wide v5, 0x3fd999999999999aL    # 0.4

    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    invoke-virtual {v2, v4, v5, v6}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 119
    .line 120
    .line 121
    move-result v2

    .line 122
    invoke-virtual {v3, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 123
    .line 124
    .line 125
    goto :goto_1

    .line 126
    :cond_2
    move-object v3, v0

    .line 127
    :goto_1
    new-instance v2, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 128
    .line 129
    invoke-direct {v2}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 130
    .line 131
    .line 132
    new-instance v4, Landroid/graphics/Path;

    .line 133
    .line 134
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 135
    .line 136
    .line 137
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 138
    .line 139
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 140
    .line 141
    int-to-float v6, v6

    .line 142
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 143
    .line 144
    int-to-float v8, v7

    .line 145
    iget v9, p1, Landroid/graphics/Rect;->right:I

    .line 146
    .line 147
    int-to-float v9, v9

    .line 148
    int-to-float v7, v7

    .line 149
    add-float/2addr v7, v1

    .line 150
    invoke-virtual {v5, v6, v8, v9, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 151
    .line 152
    .line 153
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 154
    .line 155
    sget-object v6, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 156
    .line 157
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 158
    .line 159
    .line 160
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 161
    .line 162
    .line 163
    invoke-virtual {v2, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 167
    .line 168
    .line 169
    move-result-object v3

    .line 170
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 171
    .line 172
    .line 173
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 174
    .line 175
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    .line 177
    .line 178
    new-instance v2, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 179
    .line 180
    invoke-direct {v2}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 181
    .line 182
    .line 183
    new-instance v3, Landroid/graphics/Path;

    .line 184
    .line 185
    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 186
    .line 187
    .line 188
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 189
    .line 190
    const/high16 v5, -0x3ccc0000    # -180.0f

    .line 191
    .line 192
    const/high16 v6, 0x43340000    # 180.0f

    .line 193
    .line 194
    invoke-virtual {v3, v4, v6, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 195
    .line 196
    .line 197
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 198
    .line 199
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 200
    .line 201
    int-to-float v5, v5

    .line 202
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 203
    .line 204
    int-to-float v8, v7

    .line 205
    sub-float/2addr v8, v1

    .line 206
    iget p1, p1, Landroid/graphics/Rect;->right:I

    .line 207
    .line 208
    int-to-float p1, p1

    .line 209
    int-to-float v1, v7

    .line 210
    invoke-virtual {v4, v5, v8, p1, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 211
    .line 212
    .line 213
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 214
    .line 215
    const/4 v1, 0x0

    .line 216
    invoke-virtual {v3, p1, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 217
    .line 218
    .line 219
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    .line 220
    .line 221
    .line 222
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 223
    .line 224
    .line 225
    move-result-object p0

    .line 226
    invoke-virtual {v2, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 227
    .line 228
    .line 229
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 230
    .line 231
    .line 232
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 233
    .line 234
    .line 235
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 236
    .line 237
    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    .line 239
    .line 240
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 241
    .line 242
    return-object p0
    .line 243
    .line 244
    .line 245
.end method

.method private static getChordPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/high16 v0, 0x42340000    # 45.0f

    .line 6
    .line 7
    const/high16 v1, 0x43870000    # 270.0f

    .line 8
    .line 9
    if-eqz p0, :cond_1

    .line 10
    .line 11
    array-length v2, p0

    .line 12
    const/4 v3, 0x2

    .line 13
    if-lt v2, v3, :cond_1

    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    aget-object v2, p0, v2

    .line 17
    .line 18
    const/high16 v3, 0x40c00000    # 6.0f

    .line 19
    .line 20
    const/high16 v4, 0x41200000    # 10.0f

    .line 21
    .line 22
    if-eqz v2, :cond_0

    .line 23
    .line 24
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    mul-float v0, v0, v4

    .line 29
    .line 30
    div-float/2addr v0, v3

    .line 31
    :cond_0
    const/4 v2, 0x1

    .line 32
    aget-object p0, p0, v2

    .line 33
    .line 34
    if-eqz p0, :cond_1

    .line 35
    .line 36
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 37
    .line 38
    .line 39
    move-result p0

    .line 40
    mul-float p0, p0, v4

    .line 41
    .line 42
    div-float v1, p0, v3

    .line 43
    .line 44
    :cond_1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 45
    .line 46
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 47
    .line 48
    int-to-float v2, v2

    .line 49
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 50
    .line 51
    int-to-float v3, v3

    .line 52
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 53
    .line 54
    int-to-float v4, v4

    .line 55
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 56
    .line 57
    int-to-float p1, p1

    .line 58
    invoke-virtual {p0, v2, v3, v4, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 59
    .line 60
    .line 61
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 62
    .line 63
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 64
    .line 65
    sub-float/2addr v1, v0

    .line 66
    invoke-virtual {p0, p1, v0, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 67
    .line 68
    .line 69
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 70
    .line 71
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 72
    .line 73
    .line 74
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 75
    .line 76
    return-object p0
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getCloudPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/graphics/Path;->reset()V

    .line 4
    .line 5
    .line 6
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 7
    .line 8
    const v0, 0x438e8000    # 285.0f

    .line 9
    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    const/high16 v2, 0x43200000    # 160.0f

    .line 13
    .line 14
    const/high16 v3, 0x42b40000    # 90.0f

    .line 15
    .line 16
    invoke-virtual {p0, v1, v2, v3, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 17
    .line 18
    .line 19
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 20
    .line 21
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 22
    .line 23
    const/high16 v2, 0x42f00000    # 120.0f

    .line 24
    .line 25
    const/high16 v4, 0x43140000    # 148.0f

    .line 26
    .line 27
    invoke-virtual {p0, v0, v2, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 28
    .line 29
    .line 30
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 31
    .line 32
    const/high16 v0, 0x433c0000    # 188.0f

    .line 33
    .line 34
    const/high16 v2, 0x437a0000    # 250.0f

    .line 35
    .line 36
    const/high16 v4, 0x42240000    # 41.0f

    .line 37
    .line 38
    const/high16 v5, 0x42300000    # 44.0f

    .line 39
    .line 40
    invoke-virtual {p0, v4, v5, v0, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 41
    .line 42
    .line 43
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 44
    .line 45
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 46
    .line 47
    const v2, 0x432c8000    # 172.5f

    .line 48
    .line 49
    .line 50
    const/high16 v4, 0x42ff0000    # 127.5f

    .line 51
    .line 52
    invoke-virtual {p0, v0, v2, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 53
    .line 54
    .line 55
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 56
    .line 57
    const/high16 v0, 0x43840000    # 264.0f

    .line 58
    .line 59
    const/high16 v2, 0x435c0000    # 220.0f

    .line 60
    .line 61
    const/high16 v4, 0x430c0000    # 140.0f

    .line 62
    .line 63
    const/high16 v5, 0x41600000    # 14.0f

    .line 64
    .line 65
    invoke-virtual {p0, v4, v5, v0, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 66
    .line 67
    .line 68
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 69
    .line 70
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 71
    .line 72
    const/high16 v2, 0x435a0000    # 218.0f

    .line 73
    .line 74
    invoke-virtual {p0, v0, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 75
    .line 76
    .line 77
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 78
    .line 79
    const/high16 v0, 0x43aa0000    # 340.0f

    .line 80
    .line 81
    const/high16 v2, 0x43520000    # 210.0f

    .line 82
    .line 83
    const/high16 v3, 0x43660000    # 230.0f

    .line 84
    .line 85
    invoke-virtual {p0, v3, v1, v0, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 86
    .line 87
    .line 88
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 89
    .line 90
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 91
    .line 92
    const/high16 v2, 0x435b0000    # 219.0f

    .line 93
    .line 94
    const/high16 v3, 0x42b80000    # 92.0f

    .line 95
    .line 96
    invoke-virtual {p0, v0, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 97
    .line 98
    .line 99
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 100
    .line 101
    const/high16 v0, 0x43d60000    # 428.0f

    .line 102
    .line 103
    const/high16 v2, 0x43760000    # 246.0f

    .line 104
    .line 105
    const/high16 v3, 0x43940000    # 296.0f

    .line 106
    .line 107
    invoke-virtual {p0, v3, v1, v0, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 108
    .line 109
    .line 110
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 111
    .line 112
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 113
    .line 114
    const/high16 v1, 0x43680000    # 232.0f

    .line 115
    .line 116
    const/high16 v2, 0x42ca0000    # 101.0f

    .line 117
    .line 118
    invoke-virtual {p0, v0, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 119
    .line 120
    .line 121
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 122
    .line 123
    const/high16 v0, 0x43e30000    # 454.0f

    .line 124
    .line 125
    const/high16 v1, 0x43560000    # 214.0f

    .line 126
    .line 127
    const/high16 v2, 0x43ab0000    # 342.0f

    .line 128
    .line 129
    const/high16 v3, 0x42700000    # 60.0f

    .line 130
    .line 131
    invoke-virtual {p0, v2, v3, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 132
    .line 133
    .line 134
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 135
    .line 136
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 137
    .line 138
    const v1, 0x43928000    # 293.0f

    .line 139
    .line 140
    .line 141
    const/high16 v2, 0x42b20000    # 89.0f

    .line 142
    .line 143
    invoke-virtual {p0, v0, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 144
    .line 145
    .line 146
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 147
    .line 148
    const v0, 0x43a38000    # 327.0f

    .line 149
    .line 150
    .line 151
    const/high16 v1, 0x43a20000    # 324.0f

    .line 152
    .line 153
    const/high16 v2, 0x43020000    # 130.0f

    .line 154
    .line 155
    const/high16 v3, 0x43ea0000    # 468.0f

    .line 156
    .line 157
    invoke-virtual {p0, v1, v2, v3, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 158
    .line 159
    .line 160
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 161
    .line 162
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 163
    .line 164
    const v1, 0x439f8000    # 319.0f

    .line 165
    .line 166
    .line 167
    const/high16 v5, 0x42ee0000    # 119.0f

    .line 168
    .line 169
    invoke-virtual {p0, v0, v1, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 170
    .line 171
    .line 172
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 173
    .line 174
    const v0, 0x43ca8000    # 405.0f

    .line 175
    .line 176
    .line 177
    const/high16 v1, 0x43ce0000    # 412.0f

    .line 178
    .line 179
    const/high16 v5, 0x438c0000    # 280.0f

    .line 180
    .line 181
    const/high16 v6, 0x43700000    # 240.0f

    .line 182
    .line 183
    invoke-virtual {p0, v5, v6, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 184
    .line 185
    .line 186
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 187
    .line 188
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 189
    .line 190
    const/high16 v1, 0x3f800000    # 1.0f

    .line 191
    .line 192
    const/high16 v5, 0x42f40000    # 122.0f

    .line 193
    .line 194
    invoke-virtual {p0, v0, v1, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 195
    .line 196
    .line 197
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 198
    .line 199
    const/high16 v0, 0x43890000    # 274.0f

    .line 200
    .line 201
    const/high16 v1, 0x439c0000    # 312.0f

    .line 202
    .line 203
    const/high16 v5, 0x43280000    # 168.0f

    .line 204
    .line 205
    invoke-virtual {p0, v5, v0, v1, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 206
    .line 207
    .line 208
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 209
    .line 210
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 211
    .line 212
    const/high16 v1, 0x41800000    # 16.0f

    .line 213
    .line 214
    invoke-virtual {p0, v0, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 215
    .line 216
    .line 217
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 218
    .line 219
    const/high16 v0, 0x43550000    # 213.0f

    .line 220
    .line 221
    const v1, 0x43dc8000    # 441.0f

    .line 222
    .line 223
    .line 224
    const/high16 v2, 0x42640000    # 57.0f

    .line 225
    .line 226
    const/high16 v5, 0x43790000    # 249.0f

    .line 227
    .line 228
    invoke-virtual {p0, v2, v5, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 229
    .line 230
    .line 231
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 232
    .line 233
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 234
    .line 235
    const/high16 v1, 0x42600000    # 56.0f

    .line 236
    .line 237
    const/high16 v2, 0x42940000    # 74.0f

    .line 238
    .line 239
    invoke-virtual {p0, v0, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 240
    .line 241
    .line 242
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 243
    .line 244
    const/high16 v0, 0x42c60000    # 99.0f

    .line 245
    .line 246
    const/high16 v1, 0x43c10000    # 386.0f

    .line 247
    .line 248
    const/high16 v2, 0x41300000    # 11.0f

    .line 249
    .line 250
    const v5, 0x43818000    # 259.0f

    .line 251
    .line 252
    .line 253
    invoke-virtual {p0, v2, v5, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 254
    .line 255
    .line 256
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 257
    .line 258
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 259
    .line 260
    const/high16 v1, 0x42a80000    # 84.0f

    .line 261
    .line 262
    invoke-virtual {p0, v0, v1, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 263
    .line 264
    .line 265
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 266
    .line 267
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 268
    .line 269
    .line 270
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->m:Landroid/graphics/Matrix;

    .line 271
    .line 272
    invoke-virtual {p0}, Landroid/graphics/Matrix;->reset()V

    .line 273
    .line 274
    .line 275
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->m:Landroid/graphics/Matrix;

    .line 276
    .line 277
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 278
    .line 279
    .line 280
    move-result v0

    .line 281
    int-to-float v0, v0

    .line 282
    div-float/2addr v0, v3

    .line 283
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 284
    .line 285
    .line 286
    move-result v1

    .line 287
    int-to-float v1, v1

    .line 288
    div-float/2addr v1, v3

    .line 289
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 290
    .line 291
    .line 292
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 293
    .line 294
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->m:Landroid/graphics/Matrix;

    .line 295
    .line 296
    invoke-virtual {p0, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 297
    .line 298
    .line 299
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 300
    .line 301
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 302
    .line 303
    int-to-float v0, v0

    .line 304
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 305
    .line 306
    int-to-float p1, p1

    .line 307
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->offset(FF)V

    .line 308
    .line 309
    .line 310
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 311
    .line 312
    return-object p0
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getCornerPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const/high16 v1, 0x3f000000    # 0.5f

    .line 15
    .line 16
    mul-float v0, v0, v1

    .line 17
    .line 18
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 23
    .line 24
    .line 25
    move-result v3

    .line 26
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    int-to-float v2, v2

    .line 31
    mul-float v2, v2, v1

    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 34
    .line 35
    .line 36
    move-result-object p0

    .line 37
    if-eqz p0, :cond_1

    .line 38
    .line 39
    array-length v1, p0

    .line 40
    const/4 v3, 0x2

    .line 41
    if-lt v1, v3, :cond_1

    .line 42
    .line 43
    const/4 v1, 0x0

    .line 44
    aget-object v3, p0, v1

    .line 45
    .line 46
    if-eqz v3, :cond_0

    .line 47
    .line 48
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    int-to-float v2, v2

    .line 61
    aget-object v1, p0, v1

    .line 62
    .line 63
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 64
    .line 65
    .line 66
    move-result v1

    .line 67
    mul-float v2, v2, v1

    .line 68
    .line 69
    :cond_0
    const/4 v1, 0x1

    .line 70
    aget-object v3, p0, v1

    .line 71
    .line 72
    if-eqz v3, :cond_1

    .line 73
    .line 74
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 75
    .line 76
    .line 77
    move-result v0

    .line 78
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 79
    .line 80
    .line 81
    move-result v3

    .line 82
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    int-to-float v0, v0

    .line 87
    aget-object p0, p0, v1

    .line 88
    .line 89
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 90
    .line 91
    .line 92
    move-result p0

    .line 93
    mul-float v0, v0, p0

    .line 94
    .line 95
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 96
    .line 97
    .line 98
    move-result p0

    .line 99
    int-to-float p0, p0

    .line 100
    sub-float/2addr p0, v2

    .line 101
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 102
    .line 103
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 104
    .line 105
    int-to-float v2, v2

    .line 106
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 107
    .line 108
    int-to-float v3, v3

    .line 109
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 110
    .line 111
    .line 112
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 113
    .line 114
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 115
    .line 116
    int-to-float v2, v2

    .line 117
    add-float/2addr v2, v0

    .line 118
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 119
    .line 120
    int-to-float v3, v3

    .line 121
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 122
    .line 123
    .line 124
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 125
    .line 126
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 127
    .line 128
    int-to-float v2, v2

    .line 129
    add-float/2addr v2, v0

    .line 130
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 131
    .line 132
    int-to-float v0, v0

    .line 133
    add-float/2addr v0, p0

    .line 134
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 135
    .line 136
    .line 137
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 138
    .line 139
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 140
    .line 141
    int-to-float v1, v1

    .line 142
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 143
    .line 144
    int-to-float v2, v2

    .line 145
    add-float/2addr v2, p0

    .line 146
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 147
    .line 148
    .line 149
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 150
    .line 151
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 152
    .line 153
    int-to-float v0, v0

    .line 154
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 155
    .line 156
    int-to-float v1, v1

    .line 157
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 158
    .line 159
    .line 160
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 161
    .line 162
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 163
    .line 164
    int-to-float v0, v0

    .line 165
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 166
    .line 167
    int-to-float p1, p1

    .line 168
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    .line 170
    .line 171
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 172
    .line 173
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 174
    .line 175
    .line 176
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 177
    .line 178
    return-object p0
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getCubePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const/high16 v1, 0x3e800000    # 0.25f

    .line 15
    .line 16
    mul-float v0, v0, v1

    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const/4 v2, 0x0

    .line 23
    if-eqz v1, :cond_0

    .line 24
    .line 25
    array-length v3, v1

    .line 26
    const/4 v4, 0x1

    .line 27
    if-lt v3, v4, :cond_0

    .line 28
    .line 29
    aget-object v3, v1, v2

    .line 30
    .line 31
    if-eqz v3, :cond_0

    .line 32
    .line 33
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    int-to-float v0, v0

    .line 46
    aget-object v1, v1, v2

    .line 47
    .line 48
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    mul-float v0, v0, v1

    .line 53
    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 59
    .line 60
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 61
    .line 62
    .line 63
    new-instance v10, Landroid/graphics/Path;

    .line 64
    .line 65
    invoke-direct {v10}, Landroid/graphics/Path;-><init>()V

    .line 66
    .line 67
    .line 68
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 69
    .line 70
    int-to-float v5, v4

    .line 71
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 72
    .line 73
    int-to-float v4, v4

    .line 74
    add-float v6, v4, v0

    .line 75
    .line 76
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 77
    .line 78
    int-to-float v4, v4

    .line 79
    sub-float v7, v4, v0

    .line 80
    .line 81
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 82
    .line 83
    int-to-float v8, v4

    .line 84
    sget-object v9, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 85
    .line 86
    move-object v4, v10

    .line 87
    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {v3, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 91
    .line 92
    .line 93
    invoke-virtual {v3, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 94
    .line 95
    .line 96
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 97
    .line 98
    .line 99
    move-result-object v4

    .line 100
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 101
    .line 102
    .line 103
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 104
    .line 105
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    .line 107
    .line 108
    if-eqz v1, :cond_1

    .line 109
    .line 110
    new-instance v3, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 111
    .line 112
    invoke-direct {v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 113
    .line 114
    .line 115
    invoke-virtual {v3, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 116
    .line 117
    .line 118
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 119
    .line 120
    .line 121
    move-result-object v4

    .line 122
    invoke-virtual {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 123
    .line 124
    .line 125
    move-result v5

    .line 126
    const-wide v6, 0x3fc999999999999aL    # 0.2

    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    invoke-virtual {v4, v5, v6, v7}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 132
    .line 133
    .line 134
    move-result v4

    .line 135
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 136
    .line 137
    .line 138
    goto :goto_0

    .line 139
    :cond_1
    move-object v3, v1

    .line 140
    :goto_0
    new-instance v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 141
    .line 142
    invoke-direct {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 143
    .line 144
    .line 145
    new-instance v5, Landroid/graphics/Path;

    .line 146
    .line 147
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 148
    .line 149
    .line 150
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 151
    .line 152
    int-to-float v6, v6

    .line 153
    add-float/2addr v6, v0

    .line 154
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 155
    .line 156
    int-to-float v7, v7

    .line 157
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 158
    .line 159
    .line 160
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 161
    .line 162
    int-to-float v6, v6

    .line 163
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 164
    .line 165
    int-to-float v7, v7

    .line 166
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 167
    .line 168
    .line 169
    iget v6, p1, Landroid/graphics/Rect;->right:I

    .line 170
    .line 171
    int-to-float v6, v6

    .line 172
    sub-float/2addr v6, v0

    .line 173
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 174
    .line 175
    int-to-float v7, v7

    .line 176
    add-float/2addr v7, v0

    .line 177
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 178
    .line 179
    .line 180
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 181
    .line 182
    int-to-float v6, v6

    .line 183
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 184
    .line 185
    int-to-float v7, v7

    .line 186
    add-float/2addr v7, v0

    .line 187
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 188
    .line 189
    .line 190
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 191
    .line 192
    .line 193
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 194
    .line 195
    .line 196
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 197
    .line 198
    .line 199
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 200
    .line 201
    .line 202
    move-result-object v5

    .line 203
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 204
    .line 205
    .line 206
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 207
    .line 208
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    .line 210
    .line 211
    if-eqz v1, :cond_2

    .line 212
    .line 213
    new-instance v3, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 214
    .line 215
    invoke-direct {v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 216
    .line 217
    .line 218
    invoke-virtual {v3, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 219
    .line 220
    .line 221
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 222
    .line 223
    .line 224
    move-result-object v2

    .line 225
    invoke-virtual {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 226
    .line 227
    .line 228
    move-result v1

    .line 229
    const-wide v4, -0x4036666666666666L    # -0.2

    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    invoke-virtual {v2, v1, v4, v5}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 235
    .line 236
    .line 237
    move-result v1

    .line 238
    invoke-virtual {v3, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 239
    .line 240
    .line 241
    :cond_2
    new-instance v1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 242
    .line 243
    invoke-direct {v1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 244
    .line 245
    .line 246
    new-instance v2, Landroid/graphics/Path;

    .line 247
    .line 248
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 249
    .line 250
    .line 251
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 252
    .line 253
    int-to-float v4, v4

    .line 254
    sub-float/2addr v4, v0

    .line 255
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 256
    .line 257
    int-to-float v5, v5

    .line 258
    add-float/2addr v5, v0

    .line 259
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 260
    .line 261
    .line 262
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 263
    .line 264
    int-to-float v4, v4

    .line 265
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 266
    .line 267
    int-to-float v5, v5

    .line 268
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 269
    .line 270
    .line 271
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 272
    .line 273
    int-to-float v4, v4

    .line 274
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 275
    .line 276
    int-to-float v5, v5

    .line 277
    sub-float/2addr v5, v0

    .line 278
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 279
    .line 280
    .line 281
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 282
    .line 283
    int-to-float v4, v4

    .line 284
    sub-float/2addr v4, v0

    .line 285
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 286
    .line 287
    int-to-float p1, p1

    .line 288
    invoke-virtual {v2, v4, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 289
    .line 290
    .line 291
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 292
    .line 293
    .line 294
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 295
    .line 296
    .line 297
    move-result-object p0

    .line 298
    invoke-virtual {v1, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 299
    .line 300
    .line 301
    invoke-virtual {v1, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 302
    .line 303
    .line 304
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 305
    .line 306
    .line 307
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 308
    .line 309
    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 310
    .line 311
    .line 312
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 313
    .line 314
    return-object p0
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getDecagonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    int-to-float p0, p0

    .line 6
    const v0, 0x3dcccccd    # 0.1f

    .line 7
    .line 8
    .line 9
    mul-float p0, p0, v0

    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    int-to-float v0, v0

    .line 16
    const v1, 0x3eb33333    # 0.35f

    .line 17
    .line 18
    .line 19
    mul-float v0, v0, v1

    .line 20
    .line 21
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    int-to-float v1, v1

    .line 26
    const v2, 0x3e4ccccd    # 0.2f

    .line 27
    .line 28
    .line 29
    mul-float v1, v1, v2

    .line 30
    .line 31
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 32
    .line 33
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 34
    .line 35
    int-to-float v3, v3

    .line 36
    add-float/2addr v3, v0

    .line 37
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 38
    .line 39
    int-to-float v4, v4

    .line 40
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 41
    .line 42
    .line 43
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 44
    .line 45
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 46
    .line 47
    int-to-float v3, v3

    .line 48
    sub-float/2addr v3, v0

    .line 49
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 50
    .line 51
    int-to-float v4, v4

    .line 52
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 53
    .line 54
    .line 55
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 56
    .line 57
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 58
    .line 59
    int-to-float v3, v3

    .line 60
    sub-float/2addr v3, p0

    .line 61
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 62
    .line 63
    int-to-float v4, v4

    .line 64
    add-float/2addr v4, v1

    .line 65
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 66
    .line 67
    .line 68
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 69
    .line 70
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 71
    .line 72
    int-to-float v3, v3

    .line 73
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 74
    .line 75
    .line 76
    move-result v4

    .line 77
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 78
    .line 79
    .line 80
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 81
    .line 82
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 83
    .line 84
    int-to-float v3, v3

    .line 85
    sub-float/2addr v3, p0

    .line 86
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 87
    .line 88
    int-to-float v4, v4

    .line 89
    sub-float/2addr v4, v1

    .line 90
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 91
    .line 92
    .line 93
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 94
    .line 95
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 96
    .line 97
    int-to-float v3, v3

    .line 98
    sub-float/2addr v3, v0

    .line 99
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 100
    .line 101
    int-to-float v4, v4

    .line 102
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 103
    .line 104
    .line 105
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 106
    .line 107
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 108
    .line 109
    int-to-float v3, v3

    .line 110
    add-float/2addr v3, v0

    .line 111
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 112
    .line 113
    int-to-float v0, v0

    .line 114
    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 115
    .line 116
    .line 117
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 118
    .line 119
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 120
    .line 121
    int-to-float v2, v2

    .line 122
    add-float/2addr v2, p0

    .line 123
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 124
    .line 125
    int-to-float v3, v3

    .line 126
    sub-float/2addr v3, v1

    .line 127
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 128
    .line 129
    .line 130
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 131
    .line 132
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 133
    .line 134
    int-to-float v2, v2

    .line 135
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 136
    .line 137
    .line 138
    move-result v3

    .line 139
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 140
    .line 141
    .line 142
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 143
    .line 144
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 145
    .line 146
    int-to-float v2, v2

    .line 147
    add-float/2addr v2, p0

    .line 148
    iget p0, p1, Landroid/graphics/Rect;->top:I

    .line 149
    .line 150
    int-to-float p0, p0

    .line 151
    add-float/2addr p0, v1

    .line 152
    invoke-virtual {v0, v2, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 153
    .line 154
    .line 155
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 156
    .line 157
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 158
    .line 159
    .line 160
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 161
    .line 162
    return-object p0
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getDiagStripePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    const/high16 v1, 0x3f000000    # 0.5f

    .line 7
    .line 8
    mul-float v0, v0, v1

    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 11
    .line 12
    .line 13
    move-result-object p0

    .line 14
    if-eqz p0, :cond_0

    .line 15
    .line 16
    array-length v1, p0

    .line 17
    const/4 v2, 0x1

    .line 18
    if-lt v1, v2, :cond_0

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    aget-object v2, p0, v1

    .line 22
    .line 23
    if-eqz v2, :cond_0

    .line 24
    .line 25
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    int-to-float v0, v0

    .line 30
    aget-object p0, p0, v1

    .line 31
    .line 32
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 33
    .line 34
    .line 35
    move-result p0

    .line 36
    mul-float v0, v0, p0

    .line 37
    .line 38
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 39
    .line 40
    .line 41
    move-result p0

    .line 42
    int-to-float p0, p0

    .line 43
    mul-float p0, p0, v0

    .line 44
    .line 45
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    int-to-float v1, v1

    .line 50
    div-float/2addr p0, v1

    .line 51
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 52
    .line 53
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 54
    .line 55
    int-to-float v2, v2

    .line 56
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 57
    .line 58
    int-to-float v3, v3

    .line 59
    add-float/2addr v3, v0

    .line 60
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 61
    .line 62
    .line 63
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 64
    .line 65
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 66
    .line 67
    int-to-float v1, v1

    .line 68
    add-float/2addr v1, p0

    .line 69
    iget p0, p1, Landroid/graphics/Rect;->top:I

    .line 70
    .line 71
    int-to-float p0, p0

    .line 72
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 73
    .line 74
    .line 75
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 76
    .line 77
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 78
    .line 79
    int-to-float v0, v0

    .line 80
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 81
    .line 82
    int-to-float v1, v1

    .line 83
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 84
    .line 85
    .line 86
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 87
    .line 88
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 89
    .line 90
    int-to-float v0, v0

    .line 91
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 92
    .line 93
    int-to-float p1, p1

    .line 94
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 95
    .line 96
    .line 97
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 98
    .line 99
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 100
    .line 101
    .line 102
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 103
    .line 104
    return-object p0
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getDiamondPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 2

    .line 1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 8
    .line 9
    int-to-float v1, v1

    .line 10
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 11
    .line 12
    .line 13
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 14
    .line 15
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 16
    .line 17
    int-to-float v0, v0

    .line 18
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 23
    .line 24
    .line 25
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 26
    .line 27
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 32
    .line 33
    int-to-float v1, v1

    .line 34
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 35
    .line 36
    .line 37
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 38
    .line 39
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 40
    .line 41
    int-to-float v0, v0

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 47
    .line 48
    .line 49
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 50
    .line 51
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 52
    .line 53
    .line 54
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 55
    .line 56
    return-object p0
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getDodecagonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    int-to-float p0, p0

    .line 6
    const v0, 0x3e083127    # 0.133f

    .line 7
    .line 8
    .line 9
    mul-float p0, p0, v0

    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    int-to-float v1, v1

    .line 16
    const v2, 0x3eb33333    # 0.35f

    .line 17
    .line 18
    .line 19
    mul-float v1, v1, v2

    .line 20
    .line 21
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    int-to-float v3, v3

    .line 26
    mul-float v3, v3, v0

    .line 27
    .line 28
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    int-to-float v0, v0

    .line 33
    mul-float v0, v0, v2

    .line 34
    .line 35
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 36
    .line 37
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 38
    .line 39
    int-to-float v4, v4

    .line 40
    add-float/2addr v4, v1

    .line 41
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 42
    .line 43
    int-to-float v5, v5

    .line 44
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 45
    .line 46
    .line 47
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 48
    .line 49
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 50
    .line 51
    int-to-float v4, v4

    .line 52
    sub-float/2addr v4, v1

    .line 53
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 54
    .line 55
    int-to-float v5, v5

    .line 56
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 57
    .line 58
    .line 59
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 60
    .line 61
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 62
    .line 63
    int-to-float v4, v4

    .line 64
    sub-float/2addr v4, p0

    .line 65
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 66
    .line 67
    int-to-float v5, v5

    .line 68
    add-float/2addr v5, v3

    .line 69
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 70
    .line 71
    .line 72
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 73
    .line 74
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 75
    .line 76
    int-to-float v4, v4

    .line 77
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 78
    .line 79
    int-to-float v5, v5

    .line 80
    add-float/2addr v5, v0

    .line 81
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 82
    .line 83
    .line 84
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 85
    .line 86
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 87
    .line 88
    int-to-float v4, v4

    .line 89
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 90
    .line 91
    int-to-float v5, v5

    .line 92
    sub-float/2addr v5, v0

    .line 93
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 94
    .line 95
    .line 96
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 97
    .line 98
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 99
    .line 100
    int-to-float v4, v4

    .line 101
    sub-float/2addr v4, p0

    .line 102
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 103
    .line 104
    int-to-float v5, v5

    .line 105
    sub-float/2addr v5, v3

    .line 106
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 107
    .line 108
    .line 109
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 110
    .line 111
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 112
    .line 113
    int-to-float v4, v4

    .line 114
    sub-float/2addr v4, v1

    .line 115
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 116
    .line 117
    int-to-float v5, v5

    .line 118
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 119
    .line 120
    .line 121
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 122
    .line 123
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 124
    .line 125
    int-to-float v4, v4

    .line 126
    add-float/2addr v4, v1

    .line 127
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 128
    .line 129
    int-to-float v1, v1

    .line 130
    invoke-virtual {v2, v4, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 131
    .line 132
    .line 133
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 134
    .line 135
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 136
    .line 137
    int-to-float v2, v2

    .line 138
    add-float/2addr v2, p0

    .line 139
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 140
    .line 141
    int-to-float v4, v4

    .line 142
    sub-float/2addr v4, v3

    .line 143
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 144
    .line 145
    .line 146
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 147
    .line 148
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 149
    .line 150
    int-to-float v2, v2

    .line 151
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 152
    .line 153
    int-to-float v4, v4

    .line 154
    sub-float/2addr v4, v0

    .line 155
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 156
    .line 157
    .line 158
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 159
    .line 160
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 161
    .line 162
    int-to-float v2, v2

    .line 163
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 164
    .line 165
    int-to-float v4, v4

    .line 166
    add-float/2addr v4, v0

    .line 167
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 168
    .line 169
    .line 170
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 171
    .line 172
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 173
    .line 174
    int-to-float v1, v1

    .line 175
    add-float/2addr v1, p0

    .line 176
    iget p0, p1, Landroid/graphics/Rect;->top:I

    .line 177
    .line 178
    int-to-float p0, p0

    .line 179
    add-float/2addr p0, v3

    .line 180
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 181
    .line 182
    .line 183
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 184
    .line 185
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 186
    .line 187
    .line 188
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 189
    .line 190
    return-object p0
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getDonutPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 6
    .line 7
    .line 8
    move-result p0

    .line 9
    const/4 v1, 0x1

    .line 10
    const/high16 v2, 0x3e800000    # 0.25f

    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    if-eqz p0, :cond_1

    .line 14
    .line 15
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 16
    .line 17
    .line 18
    move-result p0

    .line 19
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    invoke-static {p0, v4}, Ljava/lang/Math;->min(II)I

    .line 24
    .line 25
    .line 26
    move-result p0

    .line 27
    int-to-float p0, p0

    .line 28
    mul-float p0, p0, v2

    .line 29
    .line 30
    if-eqz v0, :cond_0

    .line 31
    .line 32
    array-length v2, v0

    .line 33
    if-lt v2, v1, :cond_0

    .line 34
    .line 35
    aget-object v1, v0, v3

    .line 36
    .line 37
    if-eqz v1, :cond_0

    .line 38
    .line 39
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 40
    .line 41
    .line 42
    move-result p0

    .line 43
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    invoke-static {p0, v1}, Ljava/lang/Math;->min(II)I

    .line 48
    .line 49
    .line 50
    move-result p0

    .line 51
    int-to-float p0, p0

    .line 52
    aget-object v0, v0, v3

    .line 53
    .line 54
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    mul-float p0, p0, v0

    .line 59
    .line 60
    :cond_0
    move v0, p0

    .line 61
    goto :goto_0

    .line 62
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 63
    .line 64
    .line 65
    move-result p0

    .line 66
    int-to-float p0, p0

    .line 67
    mul-float p0, p0, v2

    .line 68
    .line 69
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 70
    .line 71
    .line 72
    move-result v4

    .line 73
    int-to-float v4, v4

    .line 74
    mul-float v2, v2, v4

    .line 75
    .line 76
    if-eqz v0, :cond_2

    .line 77
    .line 78
    array-length v4, v0

    .line 79
    if-lt v4, v1, :cond_2

    .line 80
    .line 81
    aget-object v1, v0, v3

    .line 82
    .line 83
    if-eqz v1, :cond_2

    .line 84
    .line 85
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 86
    .line 87
    .line 88
    move-result p0

    .line 89
    int-to-float p0, p0

    .line 90
    aget-object v1, v0, v3

    .line 91
    .line 92
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    mul-float p0, p0, v1

    .line 97
    .line 98
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 99
    .line 100
    .line 101
    move-result v1

    .line 102
    int-to-float v1, v1

    .line 103
    aget-object v0, v0, v3

    .line 104
    .line 105
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 106
    .line 107
    .line 108
    move-result v0

    .line 109
    mul-float v0, v0, v1

    .line 110
    .line 111
    goto :goto_0

    .line 112
    :cond_2
    move v0, v2

    .line 113
    :goto_0
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 114
    .line 115
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 116
    .line 117
    int-to-float v2, v2

    .line 118
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 119
    .line 120
    int-to-float v3, v3

    .line 121
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 122
    .line 123
    int-to-float v4, v4

    .line 124
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 125
    .line 126
    int-to-float v5, v5

    .line 127
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 128
    .line 129
    .line 130
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 131
    .line 132
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 133
    .line 134
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 135
    .line 136
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 137
    .line 138
    .line 139
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 140
    .line 141
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 142
    .line 143
    int-to-float v2, v2

    .line 144
    add-float/2addr v2, p0

    .line 145
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 146
    .line 147
    int-to-float v3, v3

    .line 148
    add-float/2addr v3, v0

    .line 149
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 150
    .line 151
    int-to-float v4, v4

    .line 152
    sub-float/2addr v4, p0

    .line 153
    iget p0, p1, Landroid/graphics/Rect;->bottom:I

    .line 154
    .line 155
    int-to-float p0, p0

    .line 156
    sub-float/2addr p0, v0

    .line 157
    invoke-virtual {v1, v2, v3, v4, p0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 158
    .line 159
    .line 160
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 161
    .line 162
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 163
    .line 164
    sget-object v0, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 165
    .line 166
    invoke-virtual {p0, p1, v0}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 167
    .line 168
    .line 169
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 170
    .line 171
    return-object p0
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getEllipsePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 3

    .line 1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 2
    .line 3
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 4
    .line 5
    int-to-float v0, v0

    .line 6
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 7
    .line 8
    int-to-float v1, v1

    .line 9
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 10
    .line 11
    int-to-float v2, v2

    .line 12
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 13
    .line 14
    int-to-float p1, p1

    .line 15
    invoke-virtual {p0, v0, v1, v2, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 16
    .line 17
    .line 18
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 19
    .line 20
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 21
    .line 22
    sget-object v0, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 23
    .line 24
    invoke-virtual {p0, p1, v0}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 25
    .line 26
    .line 27
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 28
    .line 29
    return-object p0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static getFoldedCornerPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    const-wide v3, -0x4036666666666666L    # -0.2

    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    const/4 v5, 0x1

    .line 17
    const/high16 v6, 0x40400000    # 3.0f

    .line 18
    .line 19
    const-wide/high16 v7, 0x4018000000000000L    # 6.0

    .line 20
    .line 21
    const-wide v9, 0x4052c00000000000L    # 75.0

    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    const/4 v11, 0x0

    .line 27
    if-eqz v2, :cond_2

    .line 28
    .line 29
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v12

    .line 37
    invoke-static {v2, v12}, Ljava/lang/Math;->min(II)I

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    int-to-float v2, v2

    .line 42
    const/high16 v12, 0x3e800000    # 0.25f

    .line 43
    .line 44
    mul-float v2, v2, v12

    .line 45
    .line 46
    if-eqz v1, :cond_0

    .line 47
    .line 48
    array-length v12, v1

    .line 49
    if-lt v12, v5, :cond_0

    .line 50
    .line 51
    aget-object v5, v1, v11

    .line 52
    .line 53
    if-eqz v5, :cond_0

    .line 54
    .line 55
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 56
    .line 57
    .line 58
    move-result v2

    .line 59
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 60
    .line 61
    .line 62
    move-result v5

    .line 63
    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    int-to-float v2, v2

    .line 68
    aget-object v1, v1, v11

    .line 69
    .line 70
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 71
    .line 72
    .line 73
    move-result v1

    .line 74
    mul-float v2, v2, v1

    .line 75
    .line 76
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 81
    .line 82
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 83
    .line 84
    .line 85
    new-instance v12, Landroid/graphics/Path;

    .line 86
    .line 87
    invoke-direct {v12}, Landroid/graphics/Path;-><init>()V

    .line 88
    .line 89
    .line 90
    iget v13, v0, Landroid/graphics/Rect;->left:I

    .line 91
    .line 92
    int-to-float v13, v13

    .line 93
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 94
    .line 95
    int-to-float v14, v14

    .line 96
    invoke-virtual {v12, v13, v14}, Landroid/graphics/Path;->moveTo(FF)V

    .line 97
    .line 98
    .line 99
    iget v13, v0, Landroid/graphics/Rect;->right:I

    .line 100
    .line 101
    int-to-float v13, v13

    .line 102
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 103
    .line 104
    int-to-float v14, v14

    .line 105
    invoke-virtual {v12, v13, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 106
    .line 107
    .line 108
    iget v13, v0, Landroid/graphics/Rect;->right:I

    .line 109
    .line 110
    int-to-float v13, v13

    .line 111
    iget v14, v0, Landroid/graphics/Rect;->bottom:I

    .line 112
    .line 113
    int-to-float v14, v14

    .line 114
    sub-float/2addr v14, v2

    .line 115
    invoke-virtual {v12, v13, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 116
    .line 117
    .line 118
    iget v13, v0, Landroid/graphics/Rect;->right:I

    .line 119
    .line 120
    int-to-float v13, v13

    .line 121
    sub-float/2addr v13, v2

    .line 122
    iget v14, v0, Landroid/graphics/Rect;->bottom:I

    .line 123
    .line 124
    int-to-float v14, v14

    .line 125
    invoke-virtual {v12, v13, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 126
    .line 127
    .line 128
    iget v13, v0, Landroid/graphics/Rect;->left:I

    .line 129
    .line 130
    int-to-float v13, v13

    .line 131
    iget v14, v0, Landroid/graphics/Rect;->bottom:I

    .line 132
    .line 133
    int-to-float v14, v14

    .line 134
    invoke-virtual {v12, v13, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 135
    .line 136
    .line 137
    invoke-virtual {v12}, Landroid/graphics/Path;->close()V

    .line 138
    .line 139
    .line 140
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 141
    .line 142
    .line 143
    move-result-object v13

    .line 144
    invoke-virtual {v5, v13}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 145
    .line 146
    .line 147
    invoke-virtual {v5, v12}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 148
    .line 149
    .line 150
    invoke-virtual {v5, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 151
    .line 152
    .line 153
    sget-object v12, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 154
    .line 155
    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    .line 157
    .line 158
    if-eqz v1, :cond_1

    .line 159
    .line 160
    new-instance v5, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 161
    .line 162
    invoke-direct {v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 163
    .line 164
    .line 165
    invoke-virtual {v5, v11}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 166
    .line 167
    .line 168
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 169
    .line 170
    .line 171
    move-result-object v11

    .line 172
    invoke-virtual {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 173
    .line 174
    .line 175
    move-result v1

    .line 176
    invoke-virtual {v11, v1, v3, v4}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 177
    .line 178
    .line 179
    move-result v1

    .line 180
    invoke-virtual {v5, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 181
    .line 182
    .line 183
    move-object v1, v5

    .line 184
    :cond_1
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 185
    .line 186
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 187
    .line 188
    .line 189
    new-instance v4, Landroid/graphics/Path;

    .line 190
    .line 191
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 192
    .line 193
    .line 194
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 195
    .line 196
    int-to-float v5, v5

    .line 197
    invoke-static {v9, v10}, Ljava/lang/Math;->toRadians(D)D

    .line 198
    .line 199
    .line 200
    move-result-wide v11

    .line 201
    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    .line 202
    .line 203
    .line 204
    move-result-wide v11

    .line 205
    double-to-float v11, v11

    .line 206
    mul-float v11, v11, v2

    .line 207
    .line 208
    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    .line 209
    .line 210
    .line 211
    move-result-wide v12

    .line 212
    double-to-float v12, v12

    .line 213
    mul-float v11, v11, v12

    .line 214
    .line 215
    div-float/2addr v11, v6

    .line 216
    sub-float/2addr v5, v11

    .line 217
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 218
    .line 219
    int-to-float v11, v11

    .line 220
    invoke-static {v9, v10}, Ljava/lang/Math;->toRadians(D)D

    .line 221
    .line 222
    .line 223
    move-result-wide v9

    .line 224
    invoke-static {v9, v10}, Ljava/lang/Math;->sin(D)D

    .line 225
    .line 226
    .line 227
    move-result-wide v9

    .line 228
    double-to-float v9, v9

    .line 229
    mul-float v9, v9, v2

    .line 230
    .line 231
    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    .line 232
    .line 233
    .line 234
    move-result-wide v7

    .line 235
    double-to-float v7, v7

    .line 236
    mul-float v9, v9, v7

    .line 237
    .line 238
    div-float/2addr v9, v6

    .line 239
    sub-float/2addr v11, v9

    .line 240
    invoke-virtual {v4, v5, v11}, Landroid/graphics/Path;->moveTo(FF)V

    .line 241
    .line 242
    .line 243
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 244
    .line 245
    int-to-float v5, v5

    .line 246
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    .line 247
    .line 248
    int-to-float v6, v6

    .line 249
    sub-float/2addr v6, v2

    .line 250
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 251
    .line 252
    .line 253
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 254
    .line 255
    int-to-float v5, v5

    .line 256
    sub-float/2addr v5, v2

    .line 257
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 258
    .line 259
    int-to-float v0, v0

    .line 260
    invoke-virtual {v4, v5, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 261
    .line 262
    .line 263
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 264
    .line 265
    .line 266
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 267
    .line 268
    .line 269
    move-result-object v0

    .line 270
    invoke-virtual {v3, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 271
    .line 272
    .line 273
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 274
    .line 275
    .line 276
    invoke-virtual {v3, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 277
    .line 278
    .line 279
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 280
    .line 281
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    .line 283
    .line 284
    goto/16 :goto_1

    .line 285
    .line 286
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 287
    .line 288
    .line 289
    move-result v2

    .line 290
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 291
    .line 292
    .line 293
    move-result v12

    .line 294
    invoke-static {v2, v12}, Ljava/lang/Math;->min(II)I

    .line 295
    .line 296
    .line 297
    move-result v2

    .line 298
    int-to-float v2, v2

    .line 299
    const/high16 v12, 0x3e000000    # 0.125f

    .line 300
    .line 301
    mul-float v2, v2, v12

    .line 302
    .line 303
    if-eqz v1, :cond_3

    .line 304
    .line 305
    array-length v12, v1

    .line 306
    if-lt v12, v5, :cond_3

    .line 307
    .line 308
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 309
    .line 310
    .line 311
    move-result v2

    .line 312
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 313
    .line 314
    .line 315
    move-result v5

    .line 316
    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    .line 317
    .line 318
    .line 319
    move-result v2

    .line 320
    int-to-float v2, v2

    .line 321
    aget-object v1, v1, v11

    .line 322
    .line 323
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 324
    .line 325
    .line 326
    move-result v1

    .line 327
    const/high16 v5, 0x3f800000    # 1.0f

    .line 328
    .line 329
    sub-float/2addr v5, v1

    .line 330
    mul-float v2, v2, v5

    .line 331
    .line 332
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 333
    .line 334
    .line 335
    move-result v1

    .line 336
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 337
    .line 338
    .line 339
    move-result v5

    .line 340
    if-le v1, v5, :cond_4

    .line 341
    .line 342
    float-to-double v1, v2

    .line 343
    const-wide v12, 0x3ff6db8bac710cb3L    # 1.4286

    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    mul-double v1, v1, v12

    .line 349
    .line 350
    double-to-float v2, v1

    .line 351
    const v1, 0x3f333333    # 0.7f

    .line 352
    .line 353
    .line 354
    goto :goto_0

    .line 355
    :cond_4
    const v1, 0x3fb6dc5d    # 1.4286f

    .line 356
    .line 357
    .line 358
    :goto_0
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 359
    .line 360
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 361
    .line 362
    .line 363
    new-instance v12, Landroid/graphics/Path;

    .line 364
    .line 365
    invoke-direct {v12}, Landroid/graphics/Path;-><init>()V

    .line 366
    .line 367
    .line 368
    iget v13, v0, Landroid/graphics/Rect;->left:I

    .line 369
    .line 370
    int-to-float v13, v13

    .line 371
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 372
    .line 373
    int-to-float v14, v14

    .line 374
    invoke-virtual {v12, v13, v14}, Landroid/graphics/Path;->moveTo(FF)V

    .line 375
    .line 376
    .line 377
    iget v13, v0, Landroid/graphics/Rect;->right:I

    .line 378
    .line 379
    int-to-float v13, v13

    .line 380
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 381
    .line 382
    int-to-float v14, v14

    .line 383
    invoke-virtual {v12, v13, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 384
    .line 385
    .line 386
    iget v13, v0, Landroid/graphics/Rect;->right:I

    .line 387
    .line 388
    int-to-float v13, v13

    .line 389
    iget v14, v0, Landroid/graphics/Rect;->bottom:I

    .line 390
    .line 391
    int-to-float v14, v14

    .line 392
    sub-float/2addr v14, v2

    .line 393
    invoke-virtual {v12, v13, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 394
    .line 395
    .line 396
    iget v13, v0, Landroid/graphics/Rect;->right:I

    .line 397
    .line 398
    int-to-float v13, v13

    .line 399
    mul-float v1, v1, v2

    .line 400
    .line 401
    sub-float/2addr v13, v1

    .line 402
    iget v14, v0, Landroid/graphics/Rect;->bottom:I

    .line 403
    .line 404
    int-to-float v14, v14

    .line 405
    invoke-virtual {v12, v13, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 406
    .line 407
    .line 408
    iget v13, v0, Landroid/graphics/Rect;->left:I

    .line 409
    .line 410
    int-to-float v13, v13

    .line 411
    iget v14, v0, Landroid/graphics/Rect;->bottom:I

    .line 412
    .line 413
    int-to-float v14, v14

    .line 414
    invoke-virtual {v12, v13, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 415
    .line 416
    .line 417
    invoke-virtual {v12}, Landroid/graphics/Path;->close()V

    .line 418
    .line 419
    .line 420
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 421
    .line 422
    .line 423
    move-result-object v13

    .line 424
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 425
    .line 426
    .line 427
    move-result-object v14

    .line 428
    invoke-virtual {v5, v14}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 429
    .line 430
    .line 431
    invoke-virtual {v5, v12}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 432
    .line 433
    .line 434
    invoke-virtual {v5, v13}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 435
    .line 436
    .line 437
    sget-object v12, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 438
    .line 439
    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 440
    .line 441
    .line 442
    if-eqz v13, :cond_5

    .line 443
    .line 444
    new-instance v5, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 445
    .line 446
    invoke-direct {v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 447
    .line 448
    .line 449
    invoke-virtual {v5, v11}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 450
    .line 451
    .line 452
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 453
    .line 454
    .line 455
    move-result-object v11

    .line 456
    invoke-virtual {v13}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 457
    .line 458
    .line 459
    move-result v12

    .line 460
    invoke-virtual {v11, v12, v3, v4}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 461
    .line 462
    .line 463
    move-result v3

    .line 464
    invoke-virtual {v5, v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 465
    .line 466
    .line 467
    move-object v13, v5

    .line 468
    :cond_5
    new-instance v3, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 469
    .line 470
    invoke-direct {v3}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 471
    .line 472
    .line 473
    new-instance v4, Landroid/graphics/Path;

    .line 474
    .line 475
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 476
    .line 477
    .line 478
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 479
    .line 480
    int-to-float v5, v5

    .line 481
    invoke-static {v9, v10}, Ljava/lang/Math;->toRadians(D)D

    .line 482
    .line 483
    .line 484
    move-result-wide v11

    .line 485
    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    .line 486
    .line 487
    .line 488
    move-result-wide v11

    .line 489
    double-to-float v11, v11

    .line 490
    mul-float v11, v11, v1

    .line 491
    .line 492
    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    .line 493
    .line 494
    .line 495
    move-result-wide v14

    .line 496
    double-to-float v12, v14

    .line 497
    mul-float v11, v11, v12

    .line 498
    .line 499
    div-float/2addr v11, v6

    .line 500
    sub-float/2addr v5, v11

    .line 501
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 502
    .line 503
    int-to-float v11, v11

    .line 504
    invoke-static {v9, v10}, Ljava/lang/Math;->toRadians(D)D

    .line 505
    .line 506
    .line 507
    move-result-wide v9

    .line 508
    invoke-static {v9, v10}, Ljava/lang/Math;->sin(D)D

    .line 509
    .line 510
    .line 511
    move-result-wide v9

    .line 512
    double-to-float v9, v9

    .line 513
    mul-float v9, v9, v2

    .line 514
    .line 515
    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    .line 516
    .line 517
    .line 518
    move-result-wide v7

    .line 519
    double-to-float v7, v7

    .line 520
    mul-float v9, v9, v7

    .line 521
    .line 522
    div-float/2addr v9, v6

    .line 523
    sub-float/2addr v11, v9

    .line 524
    invoke-virtual {v4, v5, v11}, Landroid/graphics/Path;->moveTo(FF)V

    .line 525
    .line 526
    .line 527
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 528
    .line 529
    int-to-float v5, v5

    .line 530
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    .line 531
    .line 532
    int-to-float v6, v6

    .line 533
    sub-float/2addr v6, v2

    .line 534
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 535
    .line 536
    .line 537
    iget v2, v0, Landroid/graphics/Rect;->right:I

    .line 538
    .line 539
    int-to-float v2, v2

    .line 540
    sub-float/2addr v2, v1

    .line 541
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 542
    .line 543
    int-to-float v0, v0

    .line 544
    invoke-virtual {v4, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 545
    .line 546
    .line 547
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 548
    .line 549
    .line 550
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 551
    .line 552
    .line 553
    move-result-object v0

    .line 554
    invoke-virtual {v3, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 555
    .line 556
    .line 557
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 558
    .line 559
    .line 560
    invoke-virtual {v3, v13}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 561
    .line 562
    .line 563
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 564
    .line 565
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 566
    .line 567
    .line 568
    :goto_1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 569
    .line 570
    return-object v0
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getFramePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const v1, 0x3dcccccd    # 0.1f

    .line 15
    .line 16
    .line 17
    mul-float v0, v0, v1

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    if-eqz p0, :cond_0

    .line 24
    .line 25
    array-length v1, p0

    .line 26
    const/4 v2, 0x1

    .line 27
    if-lt v1, v2, :cond_0

    .line 28
    .line 29
    const/4 v1, 0x0

    .line 30
    aget-object v2, p0, v1

    .line 31
    .line 32
    if-eqz v2, :cond_0

    .line 33
    .line 34
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    int-to-float v0, v0

    .line 47
    aget-object p0, p0, v1

    .line 48
    .line 49
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 50
    .line 51
    .line 52
    move-result p0

    .line 53
    mul-float v0, v0, p0

    .line 54
    .line 55
    :cond_0
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 56
    .line 57
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 58
    .line 59
    int-to-float v1, v1

    .line 60
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 61
    .line 62
    int-to-float v2, v2

    .line 63
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 64
    .line 65
    int-to-float v3, v3

    .line 66
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 67
    .line 68
    int-to-float v4, v4

    .line 69
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 70
    .line 71
    .line 72
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 73
    .line 74
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 75
    .line 76
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 77
    .line 78
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 79
    .line 80
    .line 81
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 82
    .line 83
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 84
    .line 85
    int-to-float v1, v1

    .line 86
    add-float/2addr v1, v0

    .line 87
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 88
    .line 89
    int-to-float v2, v2

    .line 90
    add-float/2addr v2, v0

    .line 91
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 92
    .line 93
    int-to-float v3, v3

    .line 94
    sub-float/2addr v3, v0

    .line 95
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 96
    .line 97
    int-to-float p1, p1

    .line 98
    sub-float/2addr p1, v0

    .line 99
    invoke-virtual {p0, v1, v2, v3, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 100
    .line 101
    .line 102
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 103
    .line 104
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 105
    .line 106
    sget-object v0, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 107
    .line 108
    invoke-virtual {p0, p1, v0}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 109
    .line 110
    .line 111
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 112
    .line 113
    return-object p0
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getHalfFramePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const v1, 0x3eaaaa3b    # 0.33333f

    .line 15
    .line 16
    .line 17
    mul-float v0, v0, v1

    .line 18
    .line 19
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 24
    .line 25
    .line 26
    move-result v3

    .line 27
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    int-to-float v2, v2

    .line 32
    mul-float v2, v2, v1

    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 35
    .line 36
    .line 37
    move-result-object p0

    .line 38
    if-eqz p0, :cond_1

    .line 39
    .line 40
    array-length v1, p0

    .line 41
    const/4 v3, 0x2

    .line 42
    if-lt v1, v3, :cond_1

    .line 43
    .line 44
    const/4 v1, 0x0

    .line 45
    aget-object v3, p0, v1

    .line 46
    .line 47
    if-eqz v3, :cond_0

    .line 48
    .line 49
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 50
    .line 51
    .line 52
    move-result v2

    .line 53
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 58
    .line 59
    .line 60
    move-result v2

    .line 61
    int-to-float v2, v2

    .line 62
    aget-object v1, p0, v1

    .line 63
    .line 64
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 65
    .line 66
    .line 67
    move-result v1

    .line 68
    mul-float v2, v2, v1

    .line 69
    .line 70
    :cond_0
    const/4 v1, 0x1

    .line 71
    aget-object v3, p0, v1

    .line 72
    .line 73
    if-eqz v3, :cond_1

    .line 74
    .line 75
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 76
    .line 77
    .line 78
    move-result v0

    .line 79
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 80
    .line 81
    .line 82
    move-result v3

    .line 83
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    .line 84
    .line 85
    .line 86
    move-result v0

    .line 87
    int-to-float v0, v0

    .line 88
    aget-object p0, p0, v1

    .line 89
    .line 90
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 91
    .line 92
    .line 93
    move-result p0

    .line 94
    mul-float v0, v0, p0

    .line 95
    .line 96
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 97
    .line 98
    .line 99
    move-result p0

    .line 100
    int-to-float p0, p0

    .line 101
    mul-float p0, p0, v2

    .line 102
    .line 103
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 104
    .line 105
    .line 106
    move-result v1

    .line 107
    int-to-float v1, v1

    .line 108
    div-float/2addr p0, v1

    .line 109
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 110
    .line 111
    .line 112
    move-result v1

    .line 113
    int-to-float v1, v1

    .line 114
    mul-float v1, v1, v0

    .line 115
    .line 116
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 117
    .line 118
    .line 119
    move-result v3

    .line 120
    int-to-float v3, v3

    .line 121
    div-float/2addr v1, v3

    .line 122
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 123
    .line 124
    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    .line 125
    .line 126
    .line 127
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 128
    .line 129
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 130
    .line 131
    int-to-float v4, v4

    .line 132
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 133
    .line 134
    int-to-float v5, v5

    .line 135
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 136
    .line 137
    .line 138
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 139
    .line 140
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 141
    .line 142
    int-to-float v4, v4

    .line 143
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 144
    .line 145
    int-to-float v5, v5

    .line 146
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 147
    .line 148
    .line 149
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 150
    .line 151
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 152
    .line 153
    int-to-float v4, v4

    .line 154
    sub-float/2addr v4, p0

    .line 155
    iget p0, p1, Landroid/graphics/Rect;->top:I

    .line 156
    .line 157
    int-to-float p0, p0

    .line 158
    add-float/2addr p0, v2

    .line 159
    invoke-virtual {v3, v4, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 160
    .line 161
    .line 162
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 163
    .line 164
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 165
    .line 166
    int-to-float v3, v3

    .line 167
    add-float/2addr v3, v0

    .line 168
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 169
    .line 170
    int-to-float v4, v4

    .line 171
    add-float/2addr v4, v2

    .line 172
    invoke-virtual {p0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 173
    .line 174
    .line 175
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 176
    .line 177
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 178
    .line 179
    int-to-float v2, v2

    .line 180
    add-float/2addr v2, v0

    .line 181
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 182
    .line 183
    int-to-float v0, v0

    .line 184
    sub-float/2addr v0, v1

    .line 185
    invoke-virtual {p0, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 186
    .line 187
    .line 188
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 189
    .line 190
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 191
    .line 192
    int-to-float v0, v0

    .line 193
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 194
    .line 195
    int-to-float p1, p1

    .line 196
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 197
    .line 198
    .line 199
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 200
    .line 201
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 202
    .line 203
    .line 204
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 205
    .line 206
    return-object p0
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getHeartPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 18

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/high16 v3, 0x41f00000    # 30.0f

    .line 7
    .line 8
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 9
    .line 10
    .line 11
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 12
    .line 13
    const/4 v5, 0x0

    .line 14
    const/high16 v6, -0x3ee00000    # -10.0f

    .line 15
    .line 16
    const/high16 v7, 0x42200000    # 40.0f

    .line 17
    .line 18
    const/4 v8, 0x0

    .line 19
    const/high16 v9, 0x42480000    # 50.0f

    .line 20
    .line 21
    const/high16 v10, 0x41a00000    # 20.0f

    .line 22
    .line 23
    invoke-virtual/range {v4 .. v10}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 24
    .line 25
    .line 26
    sget-object v11, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 27
    .line 28
    const/high16 v12, 0x42700000    # 60.0f

    .line 29
    .line 30
    const/4 v13, 0x0

    .line 31
    const/high16 v14, 0x42c80000    # 100.0f

    .line 32
    .line 33
    const/high16 v15, -0x3ee00000    # -10.0f

    .line 34
    .line 35
    const/high16 v16, 0x42c80000    # 100.0f

    .line 36
    .line 37
    const/high16 v17, 0x41f00000    # 30.0f

    .line 38
    .line 39
    invoke-virtual/range {v11 .. v17}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 40
    .line 41
    .line 42
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 43
    .line 44
    const/high16 v2, 0x42c80000    # 100.0f

    .line 45
    .line 46
    const/high16 v3, 0x42700000    # 60.0f

    .line 47
    .line 48
    const/high16 v4, 0x42700000    # 60.0f

    .line 49
    .line 50
    const/high16 v5, 0x42c80000    # 100.0f

    .line 51
    .line 52
    const/high16 v6, 0x42480000    # 50.0f

    .line 53
    .line 54
    const/high16 v7, 0x42c80000    # 100.0f

    .line 55
    .line 56
    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 57
    .line 58
    .line 59
    sget-object v8, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 60
    .line 61
    const/high16 v9, 0x42200000    # 40.0f

    .line 62
    .line 63
    const/high16 v10, 0x42c80000    # 100.0f

    .line 64
    .line 65
    const/4 v11, 0x0

    .line 66
    const/high16 v14, 0x41f00000    # 30.0f

    .line 67
    .line 68
    invoke-virtual/range {v8 .. v14}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 69
    .line 70
    .line 71
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 72
    .line 73
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 74
    .line 75
    .line 76
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->m:Landroid/graphics/Matrix;

    .line 77
    .line 78
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 79
    .line 80
    .line 81
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->m:Landroid/graphics/Matrix;

    .line 82
    .line 83
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 84
    .line 85
    .line 86
    move-result v2

    .line 87
    int-to-float v2, v2

    .line 88
    const/high16 v3, 0x42c80000    # 100.0f

    .line 89
    .line 90
    div-float/2addr v2, v3

    .line 91
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 92
    .line 93
    .line 94
    move-result v4

    .line 95
    int-to-float v4, v4

    .line 96
    div-float/2addr v4, v3

    .line 97
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 98
    .line 99
    .line 100
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 101
    .line 102
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->m:Landroid/graphics/Matrix;

    .line 103
    .line 104
    invoke-virtual {v1, v2}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 105
    .line 106
    .line 107
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 108
    .line 109
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 110
    .line 111
    int-to-float v2, v2

    .line 112
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 113
    .line 114
    int-to-float v0, v0

    .line 115
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->offset(FF)V

    .line 116
    .line 117
    .line 118
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 119
    .line 120
    return-object v0
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getHeptagonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    int-to-float p0, p0

    .line 6
    const v0, 0x3dcccccd    # 0.1f

    .line 7
    .line 8
    .line 9
    mul-float p0, p0, v0

    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    int-to-float v0, v0

    .line 16
    const v1, 0x3e8ccccd    # 0.275f

    .line 17
    .line 18
    .line 19
    mul-float v0, v0, v1

    .line 20
    .line 21
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    int-to-float v1, v1

    .line 26
    const v2, 0x3e4ccccd    # 0.2f

    .line 27
    .line 28
    .line 29
    mul-float v1, v1, v2

    .line 30
    .line 31
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    int-to-float v2, v2

    .line 36
    const v3, 0x3eb33333    # 0.35f

    .line 37
    .line 38
    .line 39
    mul-float v2, v2, v3

    .line 40
    .line 41
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 42
    .line 43
    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    .line 44
    .line 45
    .line 46
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 47
    .line 48
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 49
    .line 50
    .line 51
    move-result v4

    .line 52
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 53
    .line 54
    int-to-float v5, v5

    .line 55
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 56
    .line 57
    .line 58
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 59
    .line 60
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 61
    .line 62
    int-to-float v4, v4

    .line 63
    sub-float/2addr v4, p0

    .line 64
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 65
    .line 66
    int-to-float v5, v5

    .line 67
    add-float/2addr v5, v1

    .line 68
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 69
    .line 70
    .line 71
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 72
    .line 73
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 74
    .line 75
    int-to-float v4, v4

    .line 76
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 77
    .line 78
    int-to-float v5, v5

    .line 79
    sub-float/2addr v5, v2

    .line 80
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 81
    .line 82
    .line 83
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 84
    .line 85
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 86
    .line 87
    int-to-float v4, v4

    .line 88
    sub-float/2addr v4, v0

    .line 89
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 90
    .line 91
    int-to-float v5, v5

    .line 92
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 93
    .line 94
    .line 95
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 96
    .line 97
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 98
    .line 99
    int-to-float v4, v4

    .line 100
    add-float/2addr v4, v0

    .line 101
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 102
    .line 103
    int-to-float v0, v0

    .line 104
    invoke-virtual {v3, v4, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 105
    .line 106
    .line 107
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 108
    .line 109
    iget v3, p1, Landroid/graphics/Rect;->left:I

    .line 110
    .line 111
    int-to-float v3, v3

    .line 112
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 113
    .line 114
    int-to-float v4, v4

    .line 115
    sub-float/2addr v4, v2

    .line 116
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 117
    .line 118
    .line 119
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 120
    .line 121
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 122
    .line 123
    int-to-float v2, v2

    .line 124
    add-float/2addr v2, p0

    .line 125
    iget p0, p1, Landroid/graphics/Rect;->top:I

    .line 126
    .line 127
    int-to-float p0, p0

    .line 128
    add-float/2addr p0, v1

    .line 129
    invoke-virtual {v0, v2, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 130
    .line 131
    .line 132
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 133
    .line 134
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 135
    .line 136
    .line 137
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 138
    .line 139
    return-object p0
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getHexagonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 6
    .line 7
    .line 8
    move-result p0

    .line 9
    const/4 v1, 0x1

    .line 10
    const/high16 v2, 0x3e800000    # 0.25f

    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    if-eqz p0, :cond_0

    .line 14
    .line 15
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 16
    .line 17
    .line 18
    move-result p0

    .line 19
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    invoke-static {p0, v4}, Ljava/lang/Math;->min(II)I

    .line 24
    .line 25
    .line 26
    move-result p0

    .line 27
    int-to-float p0, p0

    .line 28
    mul-float p0, p0, v2

    .line 29
    .line 30
    if-eqz v0, :cond_1

    .line 31
    .line 32
    array-length v2, v0

    .line 33
    if-lt v2, v1, :cond_1

    .line 34
    .line 35
    aget-object v1, v0, v3

    .line 36
    .line 37
    if-eqz v1, :cond_1

    .line 38
    .line 39
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 40
    .line 41
    .line 42
    move-result p0

    .line 43
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    invoke-static {p0, v1}, Ljava/lang/Math;->min(II)I

    .line 48
    .line 49
    .line 50
    move-result p0

    .line 51
    int-to-float p0, p0

    .line 52
    aget-object v0, v0, v3

    .line 53
    .line 54
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    goto :goto_0

    .line 59
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 60
    .line 61
    .line 62
    move-result p0

    .line 63
    int-to-float p0, p0

    .line 64
    mul-float p0, p0, v2

    .line 65
    .line 66
    if-eqz v0, :cond_1

    .line 67
    .line 68
    array-length v2, v0

    .line 69
    if-lt v2, v1, :cond_1

    .line 70
    .line 71
    aget-object v1, v0, v3

    .line 72
    .line 73
    if-eqz v1, :cond_1

    .line 74
    .line 75
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 76
    .line 77
    .line 78
    move-result p0

    .line 79
    int-to-float p0, p0

    .line 80
    aget-object v0, v0, v3

    .line 81
    .line 82
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    :goto_0
    mul-float p0, p0, v0

    .line 87
    .line 88
    :cond_1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 89
    .line 90
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 91
    .line 92
    int-to-float v1, v1

    .line 93
    add-float/2addr v1, p0

    .line 94
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 95
    .line 96
    int-to-float v2, v2

    .line 97
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 98
    .line 99
    .line 100
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 101
    .line 102
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 103
    .line 104
    int-to-float v1, v1

    .line 105
    sub-float/2addr v1, p0

    .line 106
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 107
    .line 108
    int-to-float v2, v2

    .line 109
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 110
    .line 111
    .line 112
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 113
    .line 114
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 115
    .line 116
    int-to-float v1, v1

    .line 117
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 118
    .line 119
    .line 120
    move-result v2

    .line 121
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 122
    .line 123
    .line 124
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 125
    .line 126
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 127
    .line 128
    int-to-float v1, v1

    .line 129
    sub-float/2addr v1, p0

    .line 130
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 131
    .line 132
    int-to-float v2, v2

    .line 133
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 134
    .line 135
    .line 136
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 137
    .line 138
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 139
    .line 140
    int-to-float v1, v1

    .line 141
    add-float/2addr v1, p0

    .line 142
    iget p0, p1, Landroid/graphics/Rect;->bottom:I

    .line 143
    .line 144
    int-to-float p0, p0

    .line 145
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 146
    .line 147
    .line 148
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 149
    .line 150
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 151
    .line 152
    int-to-float v0, v0

    .line 153
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 154
    .line 155
    .line 156
    move-result p1

    .line 157
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 158
    .line 159
    .line 160
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 161
    .line 162
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 163
    .line 164
    .line 165
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 166
    .line 167
    return-object p0
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getLeftBracePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    int-to-float v1, v1

    .line 8
    const/high16 v2, 0x3f000000    # 0.5f

    .line 9
    .line 10
    mul-float v1, v1, v2

    .line 11
    .line 12
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    const/4 v4, 0x2

    .line 21
    const v5, 0x3daaa8eb    # 0.08333f

    .line 22
    .line 23
    .line 24
    const/4 v6, 0x1

    .line 25
    const/4 v7, 0x0

    .line 26
    if-eqz v3, :cond_1

    .line 27
    .line 28
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 33
    .line 34
    .line 35
    move-result v8

    .line 36
    invoke-static {v3, v8}, Ljava/lang/Math;->min(II)I

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    int-to-float v3, v3

    .line 41
    mul-float v3, v3, v5

    .line 42
    .line 43
    if-eqz v2, :cond_3

    .line 44
    .line 45
    array-length v5, v2

    .line 46
    if-lt v5, v4, :cond_3

    .line 47
    .line 48
    aget-object v4, v2, v7

    .line 49
    .line 50
    if-eqz v4, :cond_0

    .line 51
    .line 52
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 57
    .line 58
    .line 59
    move-result v4

    .line 60
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    int-to-float v3, v3

    .line 65
    aget-object v4, v2, v7

    .line 66
    .line 67
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 68
    .line 69
    .line 70
    move-result v4

    .line 71
    mul-float v3, v3, v4

    .line 72
    .line 73
    :cond_0
    aget-object v4, v2, v6

    .line 74
    .line 75
    if-eqz v4, :cond_3

    .line 76
    .line 77
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    int-to-float v1, v1

    .line 82
    aget-object v2, v2, v6

    .line 83
    .line 84
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 85
    .line 86
    .line 87
    move-result v2

    .line 88
    goto :goto_0

    .line 89
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 90
    .line 91
    .line 92
    move-result v3

    .line 93
    int-to-float v3, v3

    .line 94
    mul-float v3, v3, v5

    .line 95
    .line 96
    if-eqz v2, :cond_3

    .line 97
    .line 98
    array-length v5, v2

    .line 99
    if-lt v5, v4, :cond_3

    .line 100
    .line 101
    aget-object v4, v2, v7

    .line 102
    .line 103
    if-eqz v4, :cond_2

    .line 104
    .line 105
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 106
    .line 107
    .line 108
    move-result v3

    .line 109
    int-to-float v3, v3

    .line 110
    aget-object v4, v2, v7

    .line 111
    .line 112
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 113
    .line 114
    .line 115
    move-result v4

    .line 116
    mul-float v3, v3, v4

    .line 117
    .line 118
    :cond_2
    aget-object v4, v2, v6

    .line 119
    .line 120
    if-eqz v4, :cond_3

    .line 121
    .line 122
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 123
    .line 124
    .line 125
    move-result v1

    .line 126
    int-to-float v1, v1

    .line 127
    aget-object v2, v2, v6

    .line 128
    .line 129
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 130
    .line 131
    .line 132
    move-result v2

    .line 133
    :goto_0
    mul-float v1, v1, v2

    .line 134
    .line 135
    :cond_3
    iget v2, v0, Landroid/graphics/Rect;->top:I

    .line 136
    .line 137
    int-to-float v2, v2

    .line 138
    add-float/2addr v2, v1

    .line 139
    const/high16 v4, 0x40000000    # 2.0f

    .line 140
    .line 141
    mul-float v5, v3, v4

    .line 142
    .line 143
    add-float/2addr v2, v5

    .line 144
    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 145
    .line 146
    int-to-float v5, v5

    .line 147
    cmpl-float v2, v2, v5

    .line 148
    .line 149
    if-lez v2, :cond_4

    .line 150
    .line 151
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 152
    .line 153
    .line 154
    move-result v2

    .line 155
    int-to-float v2, v2

    .line 156
    sub-float/2addr v2, v1

    .line 157
    div-float v3, v2, v4

    .line 158
    .line 159
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 160
    .line 161
    .line 162
    move-result-object v2

    .line 163
    const/4 v6, 0x0

    .line 164
    const/high16 v7, -0x3d4c0000    # -90.0f

    .line 165
    .line 166
    const/high16 v8, 0x42b40000    # 90.0f

    .line 167
    .line 168
    if-eqz v2, :cond_5

    .line 169
    .line 170
    new-instance v9, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 171
    .line 172
    invoke-direct {v9}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 173
    .line 174
    .line 175
    new-instance v10, Landroid/graphics/Path;

    .line 176
    .line 177
    invoke-direct {v10}, Landroid/graphics/Path;-><init>()V

    .line 178
    .line 179
    .line 180
    sget-object v11, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 181
    .line 182
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 183
    .line 184
    .line 185
    move-result v12

    .line 186
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 187
    .line 188
    int-to-float v13, v13

    .line 189
    mul-float v14, v3, v4

    .line 190
    .line 191
    sub-float/2addr v13, v14

    .line 192
    iget v15, v0, Landroid/graphics/Rect;->right:I

    .line 193
    .line 194
    int-to-float v15, v15

    .line 195
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 196
    .line 197
    .line 198
    move-result v5

    .line 199
    int-to-float v5, v5

    .line 200
    div-float/2addr v5, v4

    .line 201
    add-float/2addr v15, v5

    .line 202
    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 203
    .line 204
    int-to-float v5, v5

    .line 205
    invoke-virtual {v11, v12, v13, v15, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 206
    .line 207
    .line 208
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 209
    .line 210
    invoke-virtual {v10, v5, v8, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 211
    .line 212
    .line 213
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 214
    .line 215
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 216
    .line 217
    int-to-float v11, v11

    .line 218
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 219
    .line 220
    .line 221
    move-result v12

    .line 222
    int-to-float v12, v12

    .line 223
    div-float/2addr v12, v4

    .line 224
    sub-float/2addr v11, v12

    .line 225
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 226
    .line 227
    int-to-float v12, v12

    .line 228
    add-float/2addr v12, v1

    .line 229
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 230
    .line 231
    .line 232
    move-result v13

    .line 233
    iget v15, v0, Landroid/graphics/Rect;->top:I

    .line 234
    .line 235
    int-to-float v15, v15

    .line 236
    add-float/2addr v15, v1

    .line 237
    add-float/2addr v15, v14

    .line 238
    invoke-virtual {v5, v11, v12, v13, v15}, Landroid/graphics/RectF;->set(FFFF)V

    .line 239
    .line 240
    .line 241
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 242
    .line 243
    invoke-virtual {v10, v5, v6, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 244
    .line 245
    .line 246
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 247
    .line 248
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 249
    .line 250
    int-to-float v11, v11

    .line 251
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 252
    .line 253
    .line 254
    move-result v12

    .line 255
    int-to-float v12, v12

    .line 256
    div-float/2addr v12, v4

    .line 257
    sub-float/2addr v11, v12

    .line 258
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 259
    .line 260
    int-to-float v12, v12

    .line 261
    add-float/2addr v12, v1

    .line 262
    sub-float/2addr v12, v14

    .line 263
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 264
    .line 265
    .line 266
    move-result v13

    .line 267
    iget v15, v0, Landroid/graphics/Rect;->top:I

    .line 268
    .line 269
    int-to-float v15, v15

    .line 270
    add-float/2addr v15, v1

    .line 271
    invoke-virtual {v5, v11, v12, v13, v15}, Landroid/graphics/RectF;->set(FFFF)V

    .line 272
    .line 273
    .line 274
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 275
    .line 276
    invoke-virtual {v10, v5, v8, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 277
    .line 278
    .line 279
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 280
    .line 281
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 282
    .line 283
    .line 284
    move-result v11

    .line 285
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 286
    .line 287
    int-to-float v12, v12

    .line 288
    iget v13, v0, Landroid/graphics/Rect;->right:I

    .line 289
    .line 290
    int-to-float v13, v13

    .line 291
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 292
    .line 293
    .line 294
    move-result v15

    .line 295
    int-to-float v15, v15

    .line 296
    div-float/2addr v15, v4

    .line 297
    add-float/2addr v13, v15

    .line 298
    iget v15, v0, Landroid/graphics/Rect;->top:I

    .line 299
    .line 300
    int-to-float v15, v15

    .line 301
    add-float/2addr v15, v14

    .line 302
    invoke-virtual {v5, v11, v12, v13, v15}, Landroid/graphics/RectF;->set(FFFF)V

    .line 303
    .line 304
    .line 305
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 306
    .line 307
    const/high16 v11, 0x43340000    # 180.0f

    .line 308
    .line 309
    invoke-virtual {v10, v5, v11, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 310
    .line 311
    .line 312
    invoke-virtual {v10}, Landroid/graphics/Path;->close()V

    .line 313
    .line 314
    .line 315
    invoke-virtual {v9, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 316
    .line 317
    .line 318
    invoke-virtual {v9, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 319
    .line 320
    .line 321
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 322
    .line 323
    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 324
    .line 325
    .line 326
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 327
    .line 328
    .line 329
    move-result v2

    .line 330
    if-eqz v2, :cond_6

    .line 331
    .line 332
    new-instance v2, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 333
    .line 334
    invoke-direct {v2}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 335
    .line 336
    .line 337
    new-instance v5, Landroid/graphics/Path;

    .line 338
    .line 339
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 340
    .line 341
    .line 342
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 343
    .line 344
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 345
    .line 346
    .line 347
    move-result v10

    .line 348
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 349
    .line 350
    int-to-float v11, v11

    .line 351
    mul-float v3, v3, v4

    .line 352
    .line 353
    sub-float/2addr v11, v3

    .line 354
    iget v12, v0, Landroid/graphics/Rect;->right:I

    .line 355
    .line 356
    int-to-float v12, v12

    .line 357
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 358
    .line 359
    .line 360
    move-result v13

    .line 361
    int-to-float v13, v13

    .line 362
    div-float/2addr v13, v4

    .line 363
    add-float/2addr v12, v13

    .line 364
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    .line 365
    .line 366
    int-to-float v13, v13

    .line 367
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 368
    .line 369
    .line 370
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 371
    .line 372
    invoke-virtual {v5, v9, v8, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 373
    .line 374
    .line 375
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 376
    .line 377
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 378
    .line 379
    int-to-float v10, v10

    .line 380
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 381
    .line 382
    .line 383
    move-result v11

    .line 384
    int-to-float v11, v11

    .line 385
    div-float/2addr v11, v4

    .line 386
    sub-float/2addr v10, v11

    .line 387
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 388
    .line 389
    int-to-float v11, v11

    .line 390
    add-float/2addr v11, v1

    .line 391
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 392
    .line 393
    .line 394
    move-result v12

    .line 395
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 396
    .line 397
    int-to-float v13, v13

    .line 398
    add-float/2addr v13, v1

    .line 399
    add-float/2addr v13, v3

    .line 400
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 401
    .line 402
    .line 403
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 404
    .line 405
    invoke-virtual {v5, v9, v6, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 406
    .line 407
    .line 408
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 409
    .line 410
    iget v9, v0, Landroid/graphics/Rect;->left:I

    .line 411
    .line 412
    int-to-float v9, v9

    .line 413
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 414
    .line 415
    .line 416
    move-result v10

    .line 417
    int-to-float v10, v10

    .line 418
    div-float/2addr v10, v4

    .line 419
    sub-float/2addr v9, v10

    .line 420
    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 421
    .line 422
    int-to-float v10, v10

    .line 423
    add-float/2addr v10, v1

    .line 424
    sub-float/2addr v10, v3

    .line 425
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 426
    .line 427
    .line 428
    move-result v11

    .line 429
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 430
    .line 431
    int-to-float v12, v12

    .line 432
    add-float/2addr v12, v1

    .line 433
    invoke-virtual {v6, v9, v10, v11, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 434
    .line 435
    .line 436
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 437
    .line 438
    invoke-virtual {v5, v1, v8, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 439
    .line 440
    .line 441
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 442
    .line 443
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 444
    .line 445
    .line 446
    move-result v6

    .line 447
    iget v7, v0, Landroid/graphics/Rect;->top:I

    .line 448
    .line 449
    int-to-float v7, v7

    .line 450
    iget v9, v0, Landroid/graphics/Rect;->right:I

    .line 451
    .line 452
    int-to-float v9, v9

    .line 453
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 454
    .line 455
    .line 456
    move-result v10

    .line 457
    int-to-float v10, v10

    .line 458
    div-float/2addr v10, v4

    .line 459
    add-float/2addr v9, v10

    .line 460
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 461
    .line 462
    int-to-float v0, v0

    .line 463
    add-float/2addr v0, v3

    .line 464
    invoke-virtual {v1, v6, v7, v9, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 465
    .line 466
    .line 467
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 468
    .line 469
    const/high16 v1, 0x43340000    # 180.0f

    .line 470
    .line 471
    invoke-virtual {v5, v0, v1, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 472
    .line 473
    .line 474
    invoke-virtual {v2, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 475
    .line 476
    .line 477
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 478
    .line 479
    .line 480
    move-result-object v0

    .line 481
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 482
    .line 483
    .line 484
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 485
    .line 486
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 487
    .line 488
    .line 489
    :cond_6
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 490
    .line 491
    return-object v0
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getLeftBracketPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x1

    .line 10
    const v3, 0x3da3d70a    # 0.08f

    .line 11
    .line 12
    .line 13
    const/4 v4, 0x0

    .line 14
    if-eqz v1, :cond_1

    .line 15
    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    array-length v1, v0

    .line 19
    if-lt v1, v2, :cond_0

    .line 20
    .line 21
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    int-to-float v1, v1

    .line 34
    aget-object v0, v0, v4

    .line 35
    .line 36
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    :goto_0
    mul-float v1, v1, v0

    .line 41
    .line 42
    goto :goto_2

    .line 43
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    goto :goto_1

    .line 56
    :cond_1
    if-eqz v0, :cond_3

    .line 57
    .line 58
    array-length v1, v0

    .line 59
    if-lt v1, v2, :cond_3

    .line 60
    .line 61
    aget-object v1, v0, v4

    .line 62
    .line 63
    if-eqz v1, :cond_3

    .line 64
    .line 65
    if-eqz v1, :cond_2

    .line 66
    .line 67
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    int-to-float v1, v1

    .line 72
    aget-object v0, v0, v4

    .line 73
    .line 74
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 75
    .line 76
    .line 77
    move-result v0

    .line 78
    goto :goto_0

    .line 79
    :cond_2
    const/4 v1, 0x0

    .line 80
    goto :goto_2

    .line 81
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 82
    .line 83
    .line 84
    move-result v0

    .line 85
    :goto_1
    int-to-float v0, v0

    .line 86
    mul-float v1, v0, v3

    .line 87
    .line 88
    :goto_2
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    const/high16 v2, 0x43340000    # 180.0f

    .line 93
    .line 94
    const/high16 v3, 0x40000000    # 2.0f

    .line 95
    .line 96
    const/high16 v4, 0x42b40000    # 90.0f

    .line 97
    .line 98
    if-eqz v0, :cond_4

    .line 99
    .line 100
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 101
    .line 102
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 103
    .line 104
    .line 105
    new-instance v6, Landroid/graphics/Path;

    .line 106
    .line 107
    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    .line 108
    .line 109
    .line 110
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 111
    .line 112
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 113
    .line 114
    int-to-float v9, v8

    .line 115
    iget v10, p1, Landroid/graphics/Rect;->bottom:I

    .line 116
    .line 117
    int-to-float v11, v10

    .line 118
    mul-float v12, v1, v3

    .line 119
    .line 120
    sub-float/2addr v11, v12

    .line 121
    iget v13, p1, Landroid/graphics/Rect;->right:I

    .line 122
    .line 123
    mul-int/lit8 v13, v13, 0x2

    .line 124
    .line 125
    sub-int/2addr v13, v8

    .line 126
    int-to-float v8, v13

    .line 127
    int-to-float v10, v10

    .line 128
    invoke-virtual {v7, v9, v11, v8, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 129
    .line 130
    .line 131
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 132
    .line 133
    invoke-virtual {v6, v7, v4, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 134
    .line 135
    .line 136
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 137
    .line 138
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 139
    .line 140
    int-to-float v9, v8

    .line 141
    iget v10, p1, Landroid/graphics/Rect;->top:I

    .line 142
    .line 143
    int-to-float v11, v10

    .line 144
    iget v13, p1, Landroid/graphics/Rect;->right:I

    .line 145
    .line 146
    mul-int/lit8 v13, v13, 0x2

    .line 147
    .line 148
    sub-int/2addr v13, v8

    .line 149
    int-to-float v8, v13

    .line 150
    int-to-float v10, v10

    .line 151
    add-float/2addr v10, v12

    .line 152
    invoke-virtual {v7, v9, v11, v8, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 153
    .line 154
    .line 155
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 156
    .line 157
    invoke-virtual {v6, v7, v2, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 158
    .line 159
    .line 160
    invoke-virtual {v6}, Landroid/graphics/Path;->close()V

    .line 161
    .line 162
    .line 163
    invoke-virtual {v5, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {v5, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 167
    .line 168
    .line 169
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 170
    .line 171
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    .line 173
    .line 174
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 175
    .line 176
    .line 177
    move-result v0

    .line 178
    if-eqz v0, :cond_5

    .line 179
    .line 180
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 181
    .line 182
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 183
    .line 184
    .line 185
    new-instance v5, Landroid/graphics/Path;

    .line 186
    .line 187
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 188
    .line 189
    .line 190
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 191
    .line 192
    iget v7, p1, Landroid/graphics/Rect;->left:I

    .line 193
    .line 194
    int-to-float v8, v7

    .line 195
    iget v9, p1, Landroid/graphics/Rect;->bottom:I

    .line 196
    .line 197
    int-to-float v10, v9

    .line 198
    mul-float v1, v1, v3

    .line 199
    .line 200
    sub-float/2addr v10, v1

    .line 201
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 202
    .line 203
    mul-int/lit8 v3, v3, 0x2

    .line 204
    .line 205
    sub-int/2addr v3, v7

    .line 206
    int-to-float v3, v3

    .line 207
    int-to-float v7, v9

    .line 208
    invoke-virtual {v6, v8, v10, v3, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 209
    .line 210
    .line 211
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 212
    .line 213
    invoke-virtual {v5, v3, v4, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 214
    .line 215
    .line 216
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 217
    .line 218
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 219
    .line 220
    int-to-float v7, v6

    .line 221
    iget v8, p1, Landroid/graphics/Rect;->top:I

    .line 222
    .line 223
    int-to-float v9, v8

    .line 224
    iget p1, p1, Landroid/graphics/Rect;->right:I

    .line 225
    .line 226
    mul-int/lit8 p1, p1, 0x2

    .line 227
    .line 228
    sub-int/2addr p1, v6

    .line 229
    int-to-float p1, p1

    .line 230
    int-to-float v6, v8

    .line 231
    add-float/2addr v6, v1

    .line 232
    invoke-virtual {v3, v7, v9, p1, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 233
    .line 234
    .line 235
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 236
    .line 237
    invoke-virtual {v5, p1, v2, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 238
    .line 239
    .line 240
    invoke-virtual {v0, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 241
    .line 242
    .line 243
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 244
    .line 245
    .line 246
    move-result-object p0

    .line 247
    invoke-virtual {v0, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 248
    .line 249
    .line 250
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 251
    .line 252
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 253
    .line 254
    .line 255
    :cond_5
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 256
    .line 257
    return-object p0
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getLightningBoltPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    int-to-float p0, p0

    .line 6
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    int-to-float v0, v0

    .line 11
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 12
    .line 13
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 14
    .line 15
    int-to-float v2, v2

    .line 16
    const v3, 0x3ecccccd    # 0.4f

    .line 17
    .line 18
    .line 19
    mul-float v4, p0, v3

    .line 20
    .line 21
    add-float/2addr v2, v4

    .line 22
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 23
    .line 24
    int-to-float v4, v4

    .line 25
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 26
    .line 27
    .line 28
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 29
    .line 30
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 31
    .line 32
    int-to-float v2, v2

    .line 33
    const v4, 0x3f19999a    # 0.6f

    .line 34
    .line 35
    .line 36
    mul-float v4, v4, p0

    .line 37
    .line 38
    add-float/2addr v2, v4

    .line 39
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 40
    .line 41
    int-to-float v4, v4

    .line 42
    const v5, 0x3e924745    # 0.2857f

    .line 43
    .line 44
    .line 45
    mul-float v5, v5, v0

    .line 46
    .line 47
    add-float/2addr v4, v5

    .line 48
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 49
    .line 50
    .line 51
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 52
    .line 53
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 54
    .line 55
    int-to-float v2, v2

    .line 56
    const v4, 0x3f044674    # 0.5167f

    .line 57
    .line 58
    .line 59
    mul-float v4, v4, p0

    .line 60
    .line 61
    add-float/2addr v2, v4

    .line 62
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 63
    .line 64
    int-to-float v4, v4

    .line 65
    const v5, 0x3e99999a    # 0.3f

    .line 66
    .line 67
    .line 68
    mul-float v5, v5, v0

    .line 69
    .line 70
    add-float/2addr v4, v5

    .line 71
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 72
    .line 73
    .line 74
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 75
    .line 76
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 77
    .line 78
    int-to-float v2, v2

    .line 79
    const v4, 0x3e6b851f    # 0.23f

    .line 80
    .line 81
    .line 82
    mul-float v4, v4, p0

    .line 83
    .line 84
    sub-float/2addr v2, v4

    .line 85
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 86
    .line 87
    int-to-float v4, v4

    .line 88
    const v5, 0x3ee147ae    # 0.44f

    .line 89
    .line 90
    .line 91
    mul-float v5, v5, v0

    .line 92
    .line 93
    sub-float/2addr v4, v5

    .line 94
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 95
    .line 96
    .line 97
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 98
    .line 99
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 100
    .line 101
    int-to-float v2, v2

    .line 102
    const v4, 0x3eb089a0    # 0.3448f

    .line 103
    .line 104
    .line 105
    mul-float v4, v4, p0

    .line 106
    .line 107
    sub-float/2addr v2, v4

    .line 108
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 109
    .line 110
    int-to-float v4, v4

    .line 111
    mul-float v3, v3, v0

    .line 112
    .line 113
    sub-float/2addr v4, v3

    .line 114
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 115
    .line 116
    .line 117
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 118
    .line 119
    iget v2, p1, Landroid/graphics/Rect;->right:I

    .line 120
    .line 121
    int-to-float v2, v2

    .line 122
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 123
    .line 124
    int-to-float v3, v3

    .line 125
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 126
    .line 127
    .line 128
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 129
    .line 130
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 131
    .line 132
    int-to-float v2, v2

    .line 133
    const v3, 0x3eec49ba    # 0.4615f

    .line 134
    .line 135
    .line 136
    mul-float v3, v3, p0

    .line 137
    .line 138
    add-float/2addr v2, v3

    .line 139
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 140
    .line 141
    int-to-float v3, v3

    .line 142
    const v4, 0x3ea22681    # 0.3167f

    .line 143
    .line 144
    .line 145
    mul-float v4, v4, v0

    .line 146
    .line 147
    sub-float/2addr v3, v4

    .line 148
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 149
    .line 150
    .line 151
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 152
    .line 153
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 154
    .line 155
    int-to-float v2, v2

    .line 156
    const v3, 0x3f0ba5e3    # 0.5455f

    .line 157
    .line 158
    .line 159
    mul-float v3, v3, p0

    .line 160
    .line 161
    add-float/2addr v2, v3

    .line 162
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 163
    .line 164
    int-to-float v3, v3

    .line 165
    const v4, 0x3eb33333    # 0.35f

    .line 166
    .line 167
    .line 168
    mul-float v5, v0, v4

    .line 169
    .line 170
    sub-float/2addr v3, v5

    .line 171
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 172
    .line 173
    .line 174
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 175
    .line 176
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 177
    .line 178
    int-to-float v2, v2

    .line 179
    const/high16 v3, 0x3e800000    # 0.25f

    .line 180
    .line 181
    mul-float v3, v3, p0

    .line 182
    .line 183
    add-float/2addr v2, v3

    .line 184
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 185
    .line 186
    int-to-float v3, v3

    .line 187
    const v5, 0x3ee8b439    # 0.4545f

    .line 188
    .line 189
    .line 190
    mul-float v5, v5, v0

    .line 191
    .line 192
    add-float/2addr v3, v5

    .line 193
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 194
    .line 195
    .line 196
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 197
    .line 198
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 199
    .line 200
    int-to-float v2, v2

    .line 201
    mul-float p0, p0, v4

    .line 202
    .line 203
    add-float/2addr v2, p0

    .line 204
    iget p0, p1, Landroid/graphics/Rect;->top:I

    .line 205
    .line 206
    int-to-float p0, p0

    .line 207
    const v3, 0x3ec8c155    # 0.3921f

    .line 208
    .line 209
    .line 210
    mul-float v3, v3, v0

    .line 211
    .line 212
    add-float/2addr p0, v3

    .line 213
    invoke-virtual {v1, v2, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 214
    .line 215
    .line 216
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 217
    .line 218
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 219
    .line 220
    int-to-float v1, v1

    .line 221
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 222
    .line 223
    int-to-float p1, p1

    .line 224
    const v2, 0x3e428f5c    # 0.19f

    .line 225
    .line 226
    .line 227
    mul-float v0, v0, v2

    .line 228
    .line 229
    add-float/2addr p1, v0

    .line 230
    invoke-virtual {p0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 231
    .line 232
    .line 233
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 234
    .line 235
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 236
    .line 237
    .line 238
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 239
    .line 240
    return-object p0
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getMoonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    const/high16 v1, 0x3f000000    # 0.5f

    .line 7
    .line 8
    mul-float v0, v0, v1

    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 11
    .line 12
    .line 13
    move-result-object p0

    .line 14
    if-eqz p0, :cond_0

    .line 15
    .line 16
    array-length v1, p0

    .line 17
    const/4 v2, 0x1

    .line 18
    if-lt v1, v2, :cond_0

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    aget-object v2, p0, v1

    .line 22
    .line 23
    if-eqz v2, :cond_0

    .line 24
    .line 25
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    int-to-float v0, v0

    .line 30
    aget-object p0, p0, v1

    .line 31
    .line 32
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 33
    .line 34
    .line 35
    move-result p0

    .line 36
    const/high16 v1, 0x3f800000    # 1.0f

    .line 37
    .line 38
    sub-float/2addr v1, p0

    .line 39
    mul-float v0, v0, v1

    .line 40
    .line 41
    :cond_0
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 42
    .line 43
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 44
    .line 45
    int-to-float v2, v1

    .line 46
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 47
    .line 48
    int-to-float v3, v3

    .line 49
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 50
    .line 51
    mul-int/lit8 v4, v4, 0x2

    .line 52
    .line 53
    sub-int/2addr v4, v1

    .line 54
    int-to-float v1, v4

    .line 55
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 56
    .line 57
    int-to-float v4, v4

    .line 58
    invoke-virtual {p0, v2, v3, v1, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 59
    .line 60
    .line 61
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 62
    .line 63
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 64
    .line 65
    const/high16 v2, 0x42b40000    # 90.0f

    .line 66
    .line 67
    const/high16 v3, 0x43340000    # 180.0f

    .line 68
    .line 69
    invoke-virtual {p0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 70
    .line 71
    .line 72
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 73
    .line 74
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 75
    .line 76
    int-to-float v2, v1

    .line 77
    sub-float/2addr v2, v0

    .line 78
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 79
    .line 80
    int-to-float v3, v3

    .line 81
    int-to-float v1, v1

    .line 82
    add-float/2addr v1, v0

    .line 83
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 84
    .line 85
    int-to-float p1, p1

    .line 86
    invoke-virtual {p0, v2, v3, v1, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 87
    .line 88
    .line 89
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 90
    .line 91
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 92
    .line 93
    const/high16 v0, 0x43870000    # 270.0f

    .line 94
    .line 95
    const/high16 v1, -0x3ccc0000    # -180.0f

    .line 96
    .line 97
    invoke-virtual {p0, p1, v0, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 98
    .line 99
    .line 100
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 101
    .line 102
    return-object p0
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getNoSmokingPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 13

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 15
    .line 16
    .line 17
    move-result-object p0

    .line 18
    const/4 v1, 0x1

    .line 19
    if-eqz p0, :cond_0

    .line 20
    .line 21
    array-length v2, p0

    .line 22
    if-lt v2, v1, :cond_0

    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    aget-object p0, p0, v2

    .line 26
    .line 27
    if-eqz p0, :cond_0

    .line 28
    .line 29
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 30
    .line 31
    .line 32
    move-result p0

    .line 33
    goto :goto_0

    .line 34
    :cond_0
    const p0, 0x3e4ccccd    # 0.2f

    .line 35
    .line 36
    .line 37
    :goto_0
    mul-float v2, v0, p0

    .line 38
    .line 39
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 40
    .line 41
    const/4 v4, 0x0

    .line 42
    invoke-virtual {v3, v4, v4, v0, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 43
    .line 44
    .line 45
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 46
    .line 47
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 48
    .line 49
    sget-object v5, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 50
    .line 51
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 52
    .line 53
    .line 54
    const/high16 v3, 0x3e800000    # 0.25f

    .line 55
    .line 56
    const/high16 v4, 0x3f000000    # 0.5f

    .line 57
    .line 58
    cmpg-float v3, p0, v3

    .line 59
    .line 60
    if-gtz v3, :cond_1

    .line 61
    .line 62
    mul-float v3, p0, v4

    .line 63
    .line 64
    sub-float/2addr v4, p0

    .line 65
    div-float/2addr v3, v4

    .line 66
    float-to-double v3, v3

    .line 67
    invoke-static {v3, v4}, Ljava/lang/Math;->acos(D)D

    .line 68
    .line 69
    .line 70
    move-result-wide v3

    .line 71
    goto :goto_1

    .line 72
    :cond_1
    sub-float/2addr v4, p0

    .line 73
    float-to-double v3, v4

    .line 74
    const-wide v5, 0x3ff0c152382d7365L    # 1.0471975511965976

    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    mul-double v3, v3, v5

    .line 80
    .line 81
    const-wide/high16 v5, 0x3fd0000000000000L    # 0.25

    .line 82
    .line 83
    div-double/2addr v3, v5

    .line 84
    :goto_1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 85
    .line 86
    sub-float v5, v0, v2

    .line 87
    .line 88
    invoke-virtual {p0, v2, v2, v5, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 89
    .line 90
    .line 91
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 92
    .line 93
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 94
    .line 95
    const-wide v5, 0x4015fdbbe9bba775L    # 5.497787143782138

    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    sub-double/2addr v5, v3

    .line 101
    const-wide v7, 0x400921fb54442d18L    # Math.PI

    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    div-double/2addr v5, v7

    .line 107
    const-wide v9, 0x4066800000000000L    # 180.0

    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    mul-double v5, v5, v9

    .line 113
    .line 114
    double-to-float v5, v5

    .line 115
    const-wide/high16 v11, 0x4000000000000000L    # 2.0

    .line 116
    .line 117
    mul-double v11, v11, v3

    .line 118
    .line 119
    div-double/2addr v11, v7

    .line 120
    mul-double v11, v11, v9

    .line 121
    .line 122
    double-to-float v6, v11

    .line 123
    invoke-virtual {p0, v2, v5, v6, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    .line 124
    .line 125
    .line 126
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 127
    .line 128
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 129
    .line 130
    .line 131
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 132
    .line 133
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 134
    .line 135
    const-wide v11, 0x4002d97c7f3321d2L    # 2.356194490192345

    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    sub-double/2addr v11, v3

    .line 141
    div-double/2addr v11, v7

    .line 142
    mul-double v11, v11, v9

    .line 143
    .line 144
    double-to-float v3, v11

    .line 145
    invoke-virtual {p0, v2, v3, v6, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    .line 146
    .line 147
    .line 148
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 149
    .line 150
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 151
    .line 152
    .line 153
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->m:Landroid/graphics/Matrix;

    .line 154
    .line 155
    invoke-virtual {p0}, Landroid/graphics/Matrix;->reset()V

    .line 156
    .line 157
    .line 158
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->m:Landroid/graphics/Matrix;

    .line 159
    .line 160
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 161
    .line 162
    .line 163
    move-result v1

    .line 164
    int-to-float v1, v1

    .line 165
    div-float/2addr v1, v0

    .line 166
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 167
    .line 168
    .line 169
    move-result v2

    .line 170
    int-to-float v2, v2

    .line 171
    div-float/2addr v2, v0

    .line 172
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 173
    .line 174
    .line 175
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 176
    .line 177
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->m:Landroid/graphics/Matrix;

    .line 178
    .line 179
    invoke-virtual {p0, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 180
    .line 181
    .line 182
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 183
    .line 184
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 185
    .line 186
    int-to-float v0, v0

    .line 187
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 188
    .line 189
    int-to-float p1, p1

    .line 190
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->offset(FF)V

    .line 191
    .line 192
    .line 193
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 194
    .line 195
    return-object p0
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getOctagonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const/high16 v1, 0x3e800000    # 0.25f

    .line 15
    .line 16
    mul-float v0, v0, v1

    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 19
    .line 20
    .line 21
    move-result-object p0

    .line 22
    if-eqz p0, :cond_0

    .line 23
    .line 24
    array-length v1, p0

    .line 25
    const/4 v2, 0x1

    .line 26
    if-lt v1, v2, :cond_0

    .line 27
    .line 28
    const/4 v1, 0x0

    .line 29
    aget-object v2, p0, v1

    .line 30
    .line 31
    if-eqz v2, :cond_0

    .line 32
    .line 33
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    int-to-float v0, v0

    .line 46
    aget-object p0, p0, v1

    .line 47
    .line 48
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 49
    .line 50
    .line 51
    move-result p0

    .line 52
    mul-float v0, v0, p0

    .line 53
    .line 54
    :cond_0
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 55
    .line 56
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 57
    .line 58
    int-to-float v1, v1

    .line 59
    add-float/2addr v1, v0

    .line 60
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 61
    .line 62
    int-to-float v2, v2

    .line 63
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 64
    .line 65
    .line 66
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 67
    .line 68
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 69
    .line 70
    int-to-float v1, v1

    .line 71
    sub-float/2addr v1, v0

    .line 72
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 73
    .line 74
    int-to-float v2, v2

    .line 75
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 76
    .line 77
    .line 78
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 79
    .line 80
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 81
    .line 82
    int-to-float v1, v1

    .line 83
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 84
    .line 85
    int-to-float v2, v2

    .line 86
    add-float/2addr v2, v0

    .line 87
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 88
    .line 89
    .line 90
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 91
    .line 92
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 93
    .line 94
    int-to-float v1, v1

    .line 95
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 96
    .line 97
    int-to-float v2, v2

    .line 98
    sub-float/2addr v2, v0

    .line 99
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 100
    .line 101
    .line 102
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 103
    .line 104
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 105
    .line 106
    int-to-float v1, v1

    .line 107
    sub-float/2addr v1, v0

    .line 108
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 109
    .line 110
    int-to-float v2, v2

    .line 111
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 112
    .line 113
    .line 114
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 115
    .line 116
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 117
    .line 118
    int-to-float v1, v1

    .line 119
    add-float/2addr v1, v0

    .line 120
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 121
    .line 122
    int-to-float v2, v2

    .line 123
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 124
    .line 125
    .line 126
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 127
    .line 128
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 129
    .line 130
    int-to-float v1, v1

    .line 131
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 132
    .line 133
    int-to-float v2, v2

    .line 134
    sub-float/2addr v2, v0

    .line 135
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 136
    .line 137
    .line 138
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 139
    .line 140
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 141
    .line 142
    int-to-float v1, v1

    .line 143
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 144
    .line 145
    int-to-float p1, p1

    .line 146
    add-float/2addr p1, v0

    .line 147
    invoke-virtual {p0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 148
    .line 149
    .line 150
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 151
    .line 152
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 153
    .line 154
    .line 155
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 156
    .line 157
    return-object p0
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getParallelogramPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 6
    .line 7
    .line 8
    move-result p0

    .line 9
    const/4 v1, 0x1

    .line 10
    const/4 v2, 0x0

    .line 11
    if-eqz p0, :cond_0

    .line 12
    .line 13
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 14
    .line 15
    .line 16
    move-result p0

    .line 17
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    invoke-static {p0, v3}, Ljava/lang/Math;->min(II)I

    .line 22
    .line 23
    .line 24
    move-result p0

    .line 25
    int-to-float p0, p0

    .line 26
    const v3, 0x3e4ccccd    # 0.2f

    .line 27
    .line 28
    .line 29
    mul-float p0, p0, v3

    .line 30
    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    array-length v3, v0

    .line 34
    if-lt v3, v1, :cond_1

    .line 35
    .line 36
    aget-object v1, v0, v2

    .line 37
    .line 38
    if-eqz v1, :cond_1

    .line 39
    .line 40
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 41
    .line 42
    .line 43
    move-result p0

    .line 44
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    invoke-static {p0, v1}, Ljava/lang/Math;->min(II)I

    .line 49
    .line 50
    .line 51
    move-result p0

    .line 52
    int-to-float p0, p0

    .line 53
    aget-object v0, v0, v2

    .line 54
    .line 55
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    goto :goto_0

    .line 60
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 61
    .line 62
    .line 63
    move-result p0

    .line 64
    int-to-float p0, p0

    .line 65
    const/high16 v3, 0x3e800000    # 0.25f

    .line 66
    .line 67
    mul-float p0, p0, v3

    .line 68
    .line 69
    if-eqz v0, :cond_1

    .line 70
    .line 71
    array-length v3, v0

    .line 72
    if-lt v3, v1, :cond_1

    .line 73
    .line 74
    aget-object v1, v0, v2

    .line 75
    .line 76
    if-eqz v1, :cond_1

    .line 77
    .line 78
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 79
    .line 80
    .line 81
    move-result p0

    .line 82
    int-to-float p0, p0

    .line 83
    aget-object v0, v0, v2

    .line 84
    .line 85
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    :goto_0
    mul-float p0, p0, v0

    .line 90
    .line 91
    :cond_1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 92
    .line 93
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 94
    .line 95
    .line 96
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 97
    .line 98
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 99
    .line 100
    int-to-float v1, v1

    .line 101
    add-float/2addr v1, p0

    .line 102
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 103
    .line 104
    int-to-float v2, v2

    .line 105
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 106
    .line 107
    .line 108
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 109
    .line 110
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 111
    .line 112
    int-to-float v1, v1

    .line 113
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 114
    .line 115
    int-to-float v2, v2

    .line 116
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 117
    .line 118
    .line 119
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 120
    .line 121
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 122
    .line 123
    int-to-float v1, v1

    .line 124
    sub-float/2addr v1, p0

    .line 125
    iget p0, p1, Landroid/graphics/Rect;->bottom:I

    .line 126
    .line 127
    int-to-float p0, p0

    .line 128
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 129
    .line 130
    .line 131
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 132
    .line 133
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 134
    .line 135
    int-to-float v0, v0

    .line 136
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 137
    .line 138
    int-to-float p1, p1

    .line 139
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 140
    .line 141
    .line 142
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 143
    .line 144
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 145
    .line 146
    .line 147
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 148
    .line 149
    return-object p0
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getPentagonPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    int-to-float p0, p0

    .line 6
    const/high16 v0, 0x40000000    # 2.0f

    .line 7
    .line 8
    div-float/2addr p0, v0

    .line 9
    const-wide/high16 v0, 0x4042000000000000L    # 36.0

    .line 10
    .line 11
    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    .line 16
    .line 17
    .line 18
    move-result-wide v0

    .line 19
    double-to-float v0, v0

    .line 20
    mul-float v0, v0, p0

    .line 21
    .line 22
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 23
    .line 24
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 25
    .line 26
    int-to-float v2, v2

    .line 27
    add-float/2addr v2, p0

    .line 28
    iget p0, p1, Landroid/graphics/Rect;->top:I

    .line 29
    .line 30
    int-to-float p0, p0

    .line 31
    invoke-virtual {v1, v2, p0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 32
    .line 33
    .line 34
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 35
    .line 36
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 37
    .line 38
    int-to-float v1, v1

    .line 39
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 40
    .line 41
    int-to-float v2, v2

    .line 42
    add-float/2addr v2, v0

    .line 43
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 44
    .line 45
    .line 46
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 47
    .line 48
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 49
    .line 50
    int-to-float v1, v1

    .line 51
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    int-to-float v2, v2

    .line 56
    sub-float/2addr v2, v0

    .line 57
    const-wide/high16 v3, 0x4032000000000000L    # 18.0

    .line 58
    .line 59
    invoke-static {v3, v4}, Ljava/lang/Math;->toRadians(D)D

    .line 60
    .line 61
    .line 62
    move-result-wide v5

    .line 63
    invoke-static {v5, v6}, Ljava/lang/Math;->tan(D)D

    .line 64
    .line 65
    .line 66
    move-result-wide v5

    .line 67
    double-to-float v5, v5

    .line 68
    mul-float v2, v2, v5

    .line 69
    .line 70
    sub-float/2addr v1, v2

    .line 71
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 72
    .line 73
    int-to-float v2, v2

    .line 74
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 75
    .line 76
    .line 77
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 78
    .line 79
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 80
    .line 81
    int-to-float v1, v1

    .line 82
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 83
    .line 84
    .line 85
    move-result v2

    .line 86
    int-to-float v2, v2

    .line 87
    sub-float/2addr v2, v0

    .line 88
    invoke-static {v3, v4}, Ljava/lang/Math;->toRadians(D)D

    .line 89
    .line 90
    .line 91
    move-result-wide v3

    .line 92
    invoke-static {v3, v4}, Ljava/lang/Math;->tan(D)D

    .line 93
    .line 94
    .line 95
    move-result-wide v3

    .line 96
    double-to-float v3, v3

    .line 97
    mul-float v2, v2, v3

    .line 98
    .line 99
    add-float/2addr v1, v2

    .line 100
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 101
    .line 102
    int-to-float v2, v2

    .line 103
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 104
    .line 105
    .line 106
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 107
    .line 108
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 109
    .line 110
    int-to-float v1, v1

    .line 111
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 112
    .line 113
    int-to-float p1, p1

    .line 114
    add-float/2addr p1, v0

    .line 115
    invoke-virtual {p0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 116
    .line 117
    .line 118
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 119
    .line 120
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 121
    .line 122
    .line 123
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 124
    .line 125
    return-object p0
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getPiePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/4 v0, 0x0

    .line 6
    const/high16 v1, 0x43870000    # 270.0f

    .line 7
    .line 8
    if-eqz p0, :cond_1

    .line 9
    .line 10
    array-length v2, p0

    .line 11
    const/4 v3, 0x2

    .line 12
    if-lt v2, v3, :cond_1

    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    aget-object v2, p0, v2

    .line 16
    .line 17
    const v3, 0x3fd55555

    .line 18
    .line 19
    .line 20
    if-eqz v2, :cond_0

    .line 21
    .line 22
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    mul-float v0, v0, v3

    .line 27
    .line 28
    :cond_0
    const/4 v2, 0x1

    .line 29
    aget-object p0, p0, v2

    .line 30
    .line 31
    if-eqz p0, :cond_1

    .line 32
    .line 33
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 34
    .line 35
    .line 36
    move-result p0

    .line 37
    mul-float v1, p0, v3

    .line 38
    .line 39
    :cond_1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 40
    .line 41
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    int-to-float v2, v2

    .line 46
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 47
    .line 48
    .line 49
    move-result v3

    .line 50
    int-to-float v3, v3

    .line 51
    invoke-virtual {p0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 52
    .line 53
    .line 54
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 55
    .line 56
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 57
    .line 58
    int-to-float v2, v2

    .line 59
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 60
    .line 61
    int-to-float v3, v3

    .line 62
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 63
    .line 64
    int-to-float v4, v4

    .line 65
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 66
    .line 67
    int-to-float p1, p1

    .line 68
    invoke-virtual {p0, v2, v3, v4, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 69
    .line 70
    .line 71
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 72
    .line 73
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 74
    .line 75
    sub-float/2addr v1, v0

    .line 76
    const/high16 v2, 0x43b40000    # 360.0f

    .line 77
    .line 78
    add-float/2addr v1, v2

    .line 79
    rem-float/2addr v1, v2

    .line 80
    invoke-virtual {p0, p1, v0, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 81
    .line 82
    .line 83
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 84
    .line 85
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 86
    .line 87
    .line 88
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 89
    .line 90
    return-object p0
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getPlaquePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const v1, 0x3e23d70a    # 0.16f

    .line 15
    .line 16
    .line 17
    mul-float v0, v0, v1

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    if-eqz p0, :cond_0

    .line 24
    .line 25
    array-length v1, p0

    .line 26
    const/4 v2, 0x1

    .line 27
    if-lt v1, v2, :cond_0

    .line 28
    .line 29
    const/4 v1, 0x0

    .line 30
    aget-object v2, p0, v1

    .line 31
    .line 32
    if-eqz v2, :cond_0

    .line 33
    .line 34
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    int-to-float v0, v0

    .line 47
    aget-object p0, p0, v1

    .line 48
    .line 49
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 50
    .line 51
    .line 52
    move-result p0

    .line 53
    mul-float v0, v0, p0

    .line 54
    .line 55
    :cond_0
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 56
    .line 57
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 58
    .line 59
    int-to-float v2, v1

    .line 60
    sub-float/2addr v2, v0

    .line 61
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 62
    .line 63
    int-to-float v4, v3

    .line 64
    sub-float/2addr v4, v0

    .line 65
    int-to-float v1, v1

    .line 66
    add-float/2addr v1, v0

    .line 67
    int-to-float v3, v3

    .line 68
    add-float/2addr v3, v0

    .line 69
    invoke-virtual {p0, v2, v4, v1, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 70
    .line 71
    .line 72
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 73
    .line 74
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 75
    .line 76
    const/high16 v2, 0x43340000    # 180.0f

    .line 77
    .line 78
    const/high16 v3, -0x3d4c0000    # -90.0f

    .line 79
    .line 80
    invoke-virtual {p0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 81
    .line 82
    .line 83
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 84
    .line 85
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 86
    .line 87
    int-to-float v2, v1

    .line 88
    sub-float/2addr v2, v0

    .line 89
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 90
    .line 91
    int-to-float v5, v4

    .line 92
    sub-float/2addr v5, v0

    .line 93
    int-to-float v1, v1

    .line 94
    add-float/2addr v1, v0

    .line 95
    int-to-float v4, v4

    .line 96
    add-float/2addr v4, v0

    .line 97
    invoke-virtual {p0, v2, v5, v1, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 98
    .line 99
    .line 100
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 101
    .line 102
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 103
    .line 104
    const/high16 v2, 0x43870000    # 270.0f

    .line 105
    .line 106
    invoke-virtual {p0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 107
    .line 108
    .line 109
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 110
    .line 111
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 112
    .line 113
    int-to-float v2, v1

    .line 114
    sub-float/2addr v2, v0

    .line 115
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 116
    .line 117
    int-to-float v5, v4

    .line 118
    sub-float/2addr v5, v0

    .line 119
    int-to-float v1, v1

    .line 120
    add-float/2addr v1, v0

    .line 121
    int-to-float v4, v4

    .line 122
    add-float/2addr v4, v0

    .line 123
    invoke-virtual {p0, v2, v5, v1, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 124
    .line 125
    .line 126
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 127
    .line 128
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 129
    .line 130
    const/4 v2, 0x0

    .line 131
    invoke-virtual {p0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 132
    .line 133
    .line 134
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 135
    .line 136
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 137
    .line 138
    int-to-float v2, v1

    .line 139
    sub-float/2addr v2, v0

    .line 140
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 141
    .line 142
    int-to-float v4, p1

    .line 143
    sub-float/2addr v4, v0

    .line 144
    int-to-float v1, v1

    .line 145
    add-float/2addr v1, v0

    .line 146
    int-to-float p1, p1

    .line 147
    add-float/2addr p1, v0

    .line 148
    invoke-virtual {p0, v2, v4, v1, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 149
    .line 150
    .line 151
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 152
    .line 153
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 154
    .line 155
    const/high16 v0, 0x42b40000    # 90.0f

    .line 156
    .line 157
    invoke-virtual {p0, p1, v0, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 158
    .line 159
    .line 160
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 161
    .line 162
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 163
    .line 164
    .line 165
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 166
    .line 167
    return-object p0
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getPlusPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const/high16 v1, 0x3e800000    # 0.25f

    .line 15
    .line 16
    mul-float v0, v0, v1

    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 19
    .line 20
    .line 21
    move-result-object p0

    .line 22
    if-eqz p0, :cond_0

    .line 23
    .line 24
    array-length v1, p0

    .line 25
    const/4 v2, 0x1

    .line 26
    if-lt v1, v2, :cond_0

    .line 27
    .line 28
    const/4 v1, 0x0

    .line 29
    aget-object v2, p0, v1

    .line 30
    .line 31
    if-eqz v2, :cond_0

    .line 32
    .line 33
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    int-to-float v0, v0

    .line 46
    aget-object p0, p0, v1

    .line 47
    .line 48
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 49
    .line 50
    .line 51
    move-result p0

    .line 52
    mul-float v0, v0, p0

    .line 53
    .line 54
    :cond_0
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 55
    .line 56
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 57
    .line 58
    int-to-float v1, v1

    .line 59
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 60
    .line 61
    int-to-float v2, v2

    .line 62
    add-float/2addr v2, v0

    .line 63
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 64
    .line 65
    .line 66
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 67
    .line 68
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 69
    .line 70
    int-to-float v1, v1

    .line 71
    add-float/2addr v1, v0

    .line 72
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 73
    .line 74
    int-to-float v2, v2

    .line 75
    add-float/2addr v2, v0

    .line 76
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 77
    .line 78
    .line 79
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 80
    .line 81
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 82
    .line 83
    int-to-float v1, v1

    .line 84
    add-float/2addr v1, v0

    .line 85
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 86
    .line 87
    int-to-float v2, v2

    .line 88
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 89
    .line 90
    .line 91
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 92
    .line 93
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 94
    .line 95
    int-to-float v1, v1

    .line 96
    sub-float/2addr v1, v0

    .line 97
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 98
    .line 99
    int-to-float v2, v2

    .line 100
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 101
    .line 102
    .line 103
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 104
    .line 105
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 106
    .line 107
    int-to-float v1, v1

    .line 108
    sub-float/2addr v1, v0

    .line 109
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 110
    .line 111
    int-to-float v2, v2

    .line 112
    add-float/2addr v2, v0

    .line 113
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 114
    .line 115
    .line 116
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 117
    .line 118
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 119
    .line 120
    int-to-float v1, v1

    .line 121
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 122
    .line 123
    int-to-float v2, v2

    .line 124
    add-float/2addr v2, v0

    .line 125
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 126
    .line 127
    .line 128
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 129
    .line 130
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 131
    .line 132
    int-to-float v1, v1

    .line 133
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 134
    .line 135
    int-to-float v2, v2

    .line 136
    sub-float/2addr v2, v0

    .line 137
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 138
    .line 139
    .line 140
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 141
    .line 142
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 143
    .line 144
    int-to-float v1, v1

    .line 145
    sub-float/2addr v1, v0

    .line 146
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 147
    .line 148
    int-to-float v2, v2

    .line 149
    sub-float/2addr v2, v0

    .line 150
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 151
    .line 152
    .line 153
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 154
    .line 155
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 156
    .line 157
    int-to-float v1, v1

    .line 158
    sub-float/2addr v1, v0

    .line 159
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 160
    .line 161
    int-to-float v2, v2

    .line 162
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 163
    .line 164
    .line 165
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 166
    .line 167
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 168
    .line 169
    int-to-float v1, v1

    .line 170
    add-float/2addr v1, v0

    .line 171
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 172
    .line 173
    int-to-float v2, v2

    .line 174
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 175
    .line 176
    .line 177
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 178
    .line 179
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 180
    .line 181
    int-to-float v1, v1

    .line 182
    add-float/2addr v1, v0

    .line 183
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 184
    .line 185
    int-to-float v2, v2

    .line 186
    sub-float/2addr v2, v0

    .line 187
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 188
    .line 189
    .line 190
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 191
    .line 192
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 193
    .line 194
    int-to-float v1, v1

    .line 195
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 196
    .line 197
    int-to-float p1, p1

    .line 198
    sub-float/2addr p1, v0

    .line 199
    invoke-virtual {p0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 200
    .line 201
    .line 202
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 203
    .line 204
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 205
    .line 206
    .line 207
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 208
    .line 209
    return-object p0
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getRightBracePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    int-to-float v1, v1

    .line 8
    const/high16 v2, 0x3f000000    # 0.5f

    .line 9
    .line 10
    mul-float v1, v1, v2

    .line 11
    .line 12
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    const/4 v4, 0x2

    .line 21
    const v5, 0x3daaa8eb    # 0.08333f

    .line 22
    .line 23
    .line 24
    const/4 v6, 0x1

    .line 25
    const/4 v7, 0x0

    .line 26
    if-eqz v3, :cond_1

    .line 27
    .line 28
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 33
    .line 34
    .line 35
    move-result v8

    .line 36
    invoke-static {v3, v8}, Ljava/lang/Math;->min(II)I

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    int-to-float v3, v3

    .line 41
    mul-float v3, v3, v5

    .line 42
    .line 43
    if-eqz v2, :cond_3

    .line 44
    .line 45
    array-length v5, v2

    .line 46
    if-lt v5, v4, :cond_3

    .line 47
    .line 48
    aget-object v4, v2, v7

    .line 49
    .line 50
    if-eqz v4, :cond_0

    .line 51
    .line 52
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 57
    .line 58
    .line 59
    move-result v4

    .line 60
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    int-to-float v3, v3

    .line 65
    aget-object v4, v2, v7

    .line 66
    .line 67
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 68
    .line 69
    .line 70
    move-result v4

    .line 71
    mul-float v3, v3, v4

    .line 72
    .line 73
    :cond_0
    aget-object v4, v2, v6

    .line 74
    .line 75
    if-eqz v4, :cond_3

    .line 76
    .line 77
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    int-to-float v1, v1

    .line 82
    aget-object v2, v2, v6

    .line 83
    .line 84
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 85
    .line 86
    .line 87
    move-result v2

    .line 88
    goto :goto_0

    .line 89
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 90
    .line 91
    .line 92
    move-result v3

    .line 93
    int-to-float v3, v3

    .line 94
    mul-float v3, v3, v5

    .line 95
    .line 96
    if-eqz v2, :cond_3

    .line 97
    .line 98
    array-length v5, v2

    .line 99
    if-lt v5, v4, :cond_3

    .line 100
    .line 101
    aget-object v4, v2, v7

    .line 102
    .line 103
    if-eqz v4, :cond_2

    .line 104
    .line 105
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 106
    .line 107
    .line 108
    move-result v3

    .line 109
    int-to-float v3, v3

    .line 110
    aget-object v4, v2, v7

    .line 111
    .line 112
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 113
    .line 114
    .line 115
    move-result v4

    .line 116
    mul-float v3, v3, v4

    .line 117
    .line 118
    :cond_2
    aget-object v4, v2, v6

    .line 119
    .line 120
    if-eqz v4, :cond_3

    .line 121
    .line 122
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 123
    .line 124
    .line 125
    move-result v1

    .line 126
    int-to-float v1, v1

    .line 127
    aget-object v2, v2, v6

    .line 128
    .line 129
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 130
    .line 131
    .line 132
    move-result v2

    .line 133
    :goto_0
    mul-float v1, v1, v2

    .line 134
    .line 135
    :cond_3
    iget v2, v0, Landroid/graphics/Rect;->top:I

    .line 136
    .line 137
    int-to-float v2, v2

    .line 138
    add-float/2addr v2, v1

    .line 139
    const/high16 v4, 0x40000000    # 2.0f

    .line 140
    .line 141
    mul-float v5, v3, v4

    .line 142
    .line 143
    add-float/2addr v2, v5

    .line 144
    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 145
    .line 146
    int-to-float v5, v5

    .line 147
    cmpl-float v2, v2, v5

    .line 148
    .line 149
    if-lez v2, :cond_4

    .line 150
    .line 151
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 152
    .line 153
    .line 154
    move-result v2

    .line 155
    int-to-float v2, v2

    .line 156
    sub-float/2addr v2, v1

    .line 157
    div-float v3, v2, v4

    .line 158
    .line 159
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 160
    .line 161
    .line 162
    move-result-object v2

    .line 163
    const/high16 v7, -0x3d4c0000    # -90.0f

    .line 164
    .line 165
    const/high16 v8, 0x42b40000    # 90.0f

    .line 166
    .line 167
    const/high16 v9, 0x43870000    # 270.0f

    .line 168
    .line 169
    if-eqz v2, :cond_5

    .line 170
    .line 171
    new-instance v10, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 172
    .line 173
    invoke-direct {v10}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 174
    .line 175
    .line 176
    new-instance v11, Landroid/graphics/Path;

    .line 177
    .line 178
    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 179
    .line 180
    .line 181
    sget-object v12, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 182
    .line 183
    iget v13, v0, Landroid/graphics/Rect;->left:I

    .line 184
    .line 185
    int-to-float v13, v13

    .line 186
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 187
    .line 188
    .line 189
    move-result v14

    .line 190
    int-to-float v14, v14

    .line 191
    div-float/2addr v14, v4

    .line 192
    sub-float/2addr v13, v14

    .line 193
    iget v14, v0, Landroid/graphics/Rect;->top:I

    .line 194
    .line 195
    int-to-float v15, v14

    .line 196
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 197
    .line 198
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 199
    .line 200
    add-int/2addr v5, v6

    .line 201
    int-to-float v5, v5

    .line 202
    div-float/2addr v5, v4

    .line 203
    int-to-float v6, v14

    .line 204
    mul-float v14, v3, v4

    .line 205
    .line 206
    add-float/2addr v6, v14

    .line 207
    invoke-virtual {v12, v13, v15, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 208
    .line 209
    .line 210
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 211
    .line 212
    invoke-virtual {v11, v5, v9, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 213
    .line 214
    .line 215
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 216
    .line 217
    iget v6, v0, Landroid/graphics/Rect;->right:I

    .line 218
    .line 219
    iget v12, v0, Landroid/graphics/Rect;->left:I

    .line 220
    .line 221
    add-int/2addr v12, v6

    .line 222
    int-to-float v12, v12

    .line 223
    div-float/2addr v12, v4

    .line 224
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 225
    .line 226
    int-to-float v13, v13

    .line 227
    add-float/2addr v13, v1

    .line 228
    sub-float/2addr v13, v14

    .line 229
    int-to-float v6, v6

    .line 230
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 231
    .line 232
    .line 233
    move-result v15

    .line 234
    int-to-float v15, v15

    .line 235
    div-float/2addr v15, v4

    .line 236
    add-float/2addr v6, v15

    .line 237
    iget v15, v0, Landroid/graphics/Rect;->top:I

    .line 238
    .line 239
    int-to-float v15, v15

    .line 240
    add-float/2addr v15, v1

    .line 241
    invoke-virtual {v5, v12, v13, v6, v15}, Landroid/graphics/RectF;->set(FFFF)V

    .line 242
    .line 243
    .line 244
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 245
    .line 246
    const/high16 v6, 0x43340000    # 180.0f

    .line 247
    .line 248
    invoke-virtual {v11, v5, v6, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 249
    .line 250
    .line 251
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 252
    .line 253
    iget v6, v0, Landroid/graphics/Rect;->right:I

    .line 254
    .line 255
    iget v12, v0, Landroid/graphics/Rect;->left:I

    .line 256
    .line 257
    add-int/2addr v12, v6

    .line 258
    int-to-float v12, v12

    .line 259
    div-float/2addr v12, v4

    .line 260
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 261
    .line 262
    int-to-float v13, v13

    .line 263
    add-float/2addr v13, v1

    .line 264
    int-to-float v6, v6

    .line 265
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 266
    .line 267
    .line 268
    move-result v15

    .line 269
    int-to-float v15, v15

    .line 270
    div-float/2addr v15, v4

    .line 271
    add-float/2addr v6, v15

    .line 272
    iget v15, v0, Landroid/graphics/Rect;->top:I

    .line 273
    .line 274
    int-to-float v15, v15

    .line 275
    add-float/2addr v15, v1

    .line 276
    add-float/2addr v15, v14

    .line 277
    invoke-virtual {v5, v12, v13, v6, v15}, Landroid/graphics/RectF;->set(FFFF)V

    .line 278
    .line 279
    .line 280
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 281
    .line 282
    invoke-virtual {v11, v5, v9, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 283
    .line 284
    .line 285
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 286
    .line 287
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 288
    .line 289
    int-to-float v6, v6

    .line 290
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 291
    .line 292
    .line 293
    move-result v12

    .line 294
    int-to-float v12, v12

    .line 295
    div-float/2addr v12, v4

    .line 296
    sub-float/2addr v6, v12

    .line 297
    iget v12, v0, Landroid/graphics/Rect;->bottom:I

    .line 298
    .line 299
    int-to-float v13, v12

    .line 300
    sub-float/2addr v13, v14

    .line 301
    iget v14, v0, Landroid/graphics/Rect;->right:I

    .line 302
    .line 303
    iget v15, v0, Landroid/graphics/Rect;->left:I

    .line 304
    .line 305
    add-int/2addr v14, v15

    .line 306
    int-to-float v14, v14

    .line 307
    div-float/2addr v14, v4

    .line 308
    int-to-float v12, v12

    .line 309
    invoke-virtual {v5, v6, v13, v14, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 310
    .line 311
    .line 312
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 313
    .line 314
    const/4 v6, 0x0

    .line 315
    invoke-virtual {v11, v5, v6, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 316
    .line 317
    .line 318
    invoke-virtual {v11}, Landroid/graphics/Path;->close()V

    .line 319
    .line 320
    .line 321
    invoke-virtual {v10, v11}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 322
    .line 323
    .line 324
    invoke-virtual {v10, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 325
    .line 326
    .line 327
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 328
    .line 329
    invoke-interface {v2, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    .line 331
    .line 332
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 333
    .line 334
    .line 335
    move-result v2

    .line 336
    if-eqz v2, :cond_6

    .line 337
    .line 338
    new-instance v2, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 339
    .line 340
    invoke-direct {v2}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 341
    .line 342
    .line 343
    new-instance v5, Landroid/graphics/Path;

    .line 344
    .line 345
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 346
    .line 347
    .line 348
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 349
    .line 350
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 351
    .line 352
    int-to-float v10, v10

    .line 353
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 354
    .line 355
    .line 356
    move-result v11

    .line 357
    int-to-float v11, v11

    .line 358
    div-float/2addr v11, v4

    .line 359
    sub-float/2addr v10, v11

    .line 360
    iget v11, v0, Landroid/graphics/Rect;->top:I

    .line 361
    .line 362
    int-to-float v12, v11

    .line 363
    iget v13, v0, Landroid/graphics/Rect;->right:I

    .line 364
    .line 365
    iget v14, v0, Landroid/graphics/Rect;->left:I

    .line 366
    .line 367
    add-int/2addr v13, v14

    .line 368
    int-to-float v13, v13

    .line 369
    div-float/2addr v13, v4

    .line 370
    int-to-float v11, v11

    .line 371
    mul-float v3, v3, v4

    .line 372
    .line 373
    add-float/2addr v11, v3

    .line 374
    invoke-virtual {v6, v10, v12, v13, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 375
    .line 376
    .line 377
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 378
    .line 379
    invoke-virtual {v5, v6, v9, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 380
    .line 381
    .line 382
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 383
    .line 384
    iget v10, v0, Landroid/graphics/Rect;->right:I

    .line 385
    .line 386
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 387
    .line 388
    add-int/2addr v11, v10

    .line 389
    int-to-float v11, v11

    .line 390
    div-float/2addr v11, v4

    .line 391
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 392
    .line 393
    int-to-float v12, v12

    .line 394
    add-float/2addr v12, v1

    .line 395
    sub-float/2addr v12, v3

    .line 396
    int-to-float v10, v10

    .line 397
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 398
    .line 399
    .line 400
    move-result v13

    .line 401
    int-to-float v13, v13

    .line 402
    div-float/2addr v13, v4

    .line 403
    add-float/2addr v10, v13

    .line 404
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 405
    .line 406
    int-to-float v13, v13

    .line 407
    add-float/2addr v13, v1

    .line 408
    invoke-virtual {v6, v11, v12, v10, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 409
    .line 410
    .line 411
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 412
    .line 413
    const/high16 v10, 0x43340000    # 180.0f

    .line 414
    .line 415
    invoke-virtual {v5, v6, v10, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 416
    .line 417
    .line 418
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 419
    .line 420
    iget v10, v0, Landroid/graphics/Rect;->right:I

    .line 421
    .line 422
    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 423
    .line 424
    add-int/2addr v11, v10

    .line 425
    int-to-float v11, v11

    .line 426
    div-float/2addr v11, v4

    .line 427
    iget v12, v0, Landroid/graphics/Rect;->top:I

    .line 428
    .line 429
    int-to-float v12, v12

    .line 430
    add-float/2addr v12, v1

    .line 431
    int-to-float v10, v10

    .line 432
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 433
    .line 434
    .line 435
    move-result v13

    .line 436
    int-to-float v13, v13

    .line 437
    div-float/2addr v13, v4

    .line 438
    add-float/2addr v10, v13

    .line 439
    iget v13, v0, Landroid/graphics/Rect;->top:I

    .line 440
    .line 441
    int-to-float v13, v13

    .line 442
    add-float/2addr v13, v1

    .line 443
    add-float/2addr v13, v3

    .line 444
    invoke-virtual {v6, v11, v12, v10, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 445
    .line 446
    .line 447
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 448
    .line 449
    invoke-virtual {v5, v1, v9, v7}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 450
    .line 451
    .line 452
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 453
    .line 454
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 455
    .line 456
    int-to-float v6, v6

    .line 457
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 458
    .line 459
    .line 460
    move-result v7

    .line 461
    int-to-float v7, v7

    .line 462
    div-float/2addr v7, v4

    .line 463
    sub-float/2addr v6, v7

    .line 464
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    .line 465
    .line 466
    int-to-float v9, v7

    .line 467
    sub-float/2addr v9, v3

    .line 468
    iget v3, v0, Landroid/graphics/Rect;->right:I

    .line 469
    .line 470
    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 471
    .line 472
    add-int/2addr v3, v0

    .line 473
    int-to-float v0, v3

    .line 474
    div-float/2addr v0, v4

    .line 475
    int-to-float v3, v7

    .line 476
    invoke-virtual {v1, v6, v9, v0, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 477
    .line 478
    .line 479
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 480
    .line 481
    const/4 v1, 0x0

    .line 482
    invoke-virtual {v5, v0, v1, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 483
    .line 484
    .line 485
    invoke-virtual {v2, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 486
    .line 487
    .line 488
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 489
    .line 490
    .line 491
    move-result-object v0

    .line 492
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 493
    .line 494
    .line 495
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 496
    .line 497
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 498
    .line 499
    .line 500
    :cond_6
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 501
    .line 502
    return-object v0
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getRightBracketPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x1

    .line 10
    const v3, 0x3da3d70a    # 0.08f

    .line 11
    .line 12
    .line 13
    const/4 v4, 0x0

    .line 14
    if-eqz v1, :cond_1

    .line 15
    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    array-length v1, v0

    .line 19
    if-lt v1, v2, :cond_0

    .line 20
    .line 21
    aget-object v1, v0, v4

    .line 22
    .line 23
    if-eqz v1, :cond_0

    .line 24
    .line 25
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    int-to-float v1, v1

    .line 38
    aget-object v0, v0, v4

    .line 39
    .line 40
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    goto :goto_0

    .line 45
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    goto :goto_1

    .line 58
    :cond_1
    if-eqz v0, :cond_2

    .line 59
    .line 60
    array-length v1, v0

    .line 61
    if-lt v1, v2, :cond_2

    .line 62
    .line 63
    aget-object v1, v0, v4

    .line 64
    .line 65
    if-eqz v1, :cond_2

    .line 66
    .line 67
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    int-to-float v1, v1

    .line 72
    aget-object v0, v0, v4

    .line 73
    .line 74
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 75
    .line 76
    .line 77
    move-result v0

    .line 78
    :goto_0
    mul-float v1, v1, v0

    .line 79
    .line 80
    goto :goto_2

    .line 81
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 82
    .line 83
    .line 84
    move-result v0

    .line 85
    :goto_1
    int-to-float v0, v0

    .line 86
    mul-float v1, v0, v3

    .line 87
    .line 88
    :goto_2
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    const/4 v2, 0x0

    .line 93
    const/high16 v3, 0x43870000    # 270.0f

    .line 94
    .line 95
    const/high16 v4, 0x40000000    # 2.0f

    .line 96
    .line 97
    const/high16 v5, 0x42b40000    # 90.0f

    .line 98
    .line 99
    if-eqz v0, :cond_3

    .line 100
    .line 101
    new-instance v6, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 102
    .line 103
    invoke-direct {v6}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 104
    .line 105
    .line 106
    new-instance v7, Landroid/graphics/Path;

    .line 107
    .line 108
    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 109
    .line 110
    .line 111
    sget-object v8, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 112
    .line 113
    iget v9, p1, Landroid/graphics/Rect;->left:I

    .line 114
    .line 115
    mul-int/lit8 v9, v9, 0x2

    .line 116
    .line 117
    iget v10, p1, Landroid/graphics/Rect;->right:I

    .line 118
    .line 119
    sub-int/2addr v9, v10

    .line 120
    int-to-float v9, v9

    .line 121
    iget v11, p1, Landroid/graphics/Rect;->top:I

    .line 122
    .line 123
    int-to-float v12, v11

    .line 124
    int-to-float v10, v10

    .line 125
    int-to-float v11, v11

    .line 126
    mul-float v13, v1, v4

    .line 127
    .line 128
    add-float/2addr v11, v13

    .line 129
    invoke-virtual {v8, v9, v12, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 130
    .line 131
    .line 132
    sget-object v8, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 133
    .line 134
    invoke-virtual {v7, v8, v3, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 135
    .line 136
    .line 137
    sget-object v8, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 138
    .line 139
    iget v9, p1, Landroid/graphics/Rect;->left:I

    .line 140
    .line 141
    mul-int/lit8 v9, v9, 0x2

    .line 142
    .line 143
    iget v10, p1, Landroid/graphics/Rect;->right:I

    .line 144
    .line 145
    sub-int/2addr v9, v10

    .line 146
    int-to-float v9, v9

    .line 147
    iget v11, p1, Landroid/graphics/Rect;->bottom:I

    .line 148
    .line 149
    int-to-float v12, v11

    .line 150
    sub-float/2addr v12, v13

    .line 151
    int-to-float v10, v10

    .line 152
    int-to-float v11, v11

    .line 153
    invoke-virtual {v8, v9, v12, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 154
    .line 155
    .line 156
    sget-object v8, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 157
    .line 158
    invoke-virtual {v7, v8, v2, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 159
    .line 160
    .line 161
    invoke-virtual {v7}, Landroid/graphics/Path;->close()V

    .line 162
    .line 163
    .line 164
    invoke-virtual {v6, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 165
    .line 166
    .line 167
    invoke-virtual {v6, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 168
    .line 169
    .line 170
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 171
    .line 172
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    .line 174
    .line 175
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->hasLine()Z

    .line 176
    .line 177
    .line 178
    move-result v0

    .line 179
    if-eqz v0, :cond_4

    .line 180
    .line 181
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 182
    .line 183
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 184
    .line 185
    .line 186
    new-instance v6, Landroid/graphics/Path;

    .line 187
    .line 188
    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    .line 189
    .line 190
    .line 191
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 192
    .line 193
    iget v8, p1, Landroid/graphics/Rect;->left:I

    .line 194
    .line 195
    mul-int/lit8 v8, v8, 0x2

    .line 196
    .line 197
    iget v9, p1, Landroid/graphics/Rect;->right:I

    .line 198
    .line 199
    sub-int/2addr v8, v9

    .line 200
    int-to-float v8, v8

    .line 201
    iget v10, p1, Landroid/graphics/Rect;->top:I

    .line 202
    .line 203
    int-to-float v11, v10

    .line 204
    int-to-float v9, v9

    .line 205
    int-to-float v10, v10

    .line 206
    mul-float v1, v1, v4

    .line 207
    .line 208
    add-float/2addr v10, v1

    .line 209
    invoke-virtual {v7, v8, v11, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 210
    .line 211
    .line 212
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 213
    .line 214
    invoke-virtual {v6, v4, v3, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 215
    .line 216
    .line 217
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 218
    .line 219
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 220
    .line 221
    mul-int/lit8 v4, v4, 0x2

    .line 222
    .line 223
    iget v7, p1, Landroid/graphics/Rect;->right:I

    .line 224
    .line 225
    sub-int/2addr v4, v7

    .line 226
    int-to-float v4, v4

    .line 227
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 228
    .line 229
    int-to-float v8, p1

    .line 230
    sub-float/2addr v8, v1

    .line 231
    int-to-float v1, v7

    .line 232
    int-to-float p1, p1

    .line 233
    invoke-virtual {v3, v4, v8, v1, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 234
    .line 235
    .line 236
    sget-object p1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 237
    .line 238
    invoke-virtual {v6, p1, v2, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 239
    .line 240
    .line 241
    invoke-virtual {v0, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 242
    .line 243
    .line 244
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 245
    .line 246
    .line 247
    move-result-object p0

    .line 248
    invoke-virtual {v0, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 249
    .line 250
    .line 251
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 252
    .line 253
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    .line 255
    .line 256
    :cond_4
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 257
    .line 258
    return-object p0
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getRtTrianglePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 2

    .line 1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 2
    .line 3
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 4
    .line 5
    int-to-float v0, v0

    .line 6
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 7
    .line 8
    int-to-float v1, v1

    .line 9
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 10
    .line 11
    .line 12
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 13
    .line 14
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 15
    .line 16
    int-to-float v0, v0

    .line 17
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 18
    .line 19
    int-to-float v1, v1

    .line 20
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 21
    .line 22
    .line 23
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 24
    .line 25
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 26
    .line 27
    int-to-float v0, v0

    .line 28
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 29
    .line 30
    int-to-float p1, p1

    .line 31
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 32
    .line 33
    .line 34
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 35
    .line 36
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 37
    .line 38
    .line 39
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 40
    .line 41
    return-object p0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static getSmileyFacePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/common/shape/AutoShape;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/common/autoshape/ExtendPath;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    const v1, 0x3d3e963e    # 0.04653f

    .line 7
    .line 8
    .line 9
    mul-float v0, v0, v1

    .line 10
    .line 11
    const/high16 v1, 0x40000000    # 2.0f

    .line 12
    .line 13
    mul-float v0, v0, v1

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    const/4 v3, 0x0

    .line 20
    if-eqz v2, :cond_1

    .line 21
    .line 22
    array-length v4, v2

    .line 23
    const/4 v5, 0x1

    .line 24
    if-lt v4, v5, :cond_1

    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 27
    .line 28
    .line 29
    move-result v4

    .line 30
    if-eqz v4, :cond_0

    .line 31
    .line 32
    aget-object v4, v2, v3

    .line 33
    .line 34
    if-eqz v4, :cond_1

    .line 35
    .line 36
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    int-to-float v0, v0

    .line 41
    aget-object v2, v2, v3

    .line 42
    .line 43
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    goto :goto_0

    .line 48
    :cond_0
    aget-object v4, v2, v3

    .line 49
    .line 50
    if-eqz v4, :cond_1

    .line 51
    .line 52
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    int-to-float v0, v0

    .line 57
    aget-object v2, v2, v3

    .line 58
    .line 59
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    const v4, 0x3f451eb8    # 0.77f

    .line 64
    .line 65
    .line 66
    sub-float/2addr v2, v4

    .line 67
    :goto_0
    mul-float v0, v0, v2

    .line 68
    .line 69
    mul-float v0, v0, v1

    .line 70
    .line 71
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    new-instance v2, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 76
    .line 77
    invoke-direct {v2}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 78
    .line 79
    .line 80
    new-instance v4, Landroid/graphics/Path;

    .line 81
    .line 82
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 83
    .line 84
    .line 85
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 86
    .line 87
    iget v6, p1, Landroid/graphics/Rect;->left:I

    .line 88
    .line 89
    int-to-float v6, v6

    .line 90
    iget v7, p1, Landroid/graphics/Rect;->top:I

    .line 91
    .line 92
    int-to-float v7, v7

    .line 93
    iget v8, p1, Landroid/graphics/Rect;->right:I

    .line 94
    .line 95
    int-to-float v8, v8

    .line 96
    iget v9, p1, Landroid/graphics/Rect;->bottom:I

    .line 97
    .line 98
    int-to-float v9, v9

    .line 99
    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 100
    .line 101
    .line 102
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 103
    .line 104
    sget-object v6, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 105
    .line 106
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {v2, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 110
    .line 111
    .line 112
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 113
    .line 114
    .line 115
    move-result-object v4

    .line 116
    invoke-virtual {v2, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 117
    .line 118
    .line 119
    invoke-virtual {v2, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 120
    .line 121
    .line 122
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 123
    .line 124
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    .line 126
    .line 127
    iget v2, p1, Landroid/graphics/Rect;->left:I

    .line 128
    .line 129
    int-to-float v2, v2

    .line 130
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 131
    .line 132
    .line 133
    move-result v4

    .line 134
    int-to-float v4, v4

    .line 135
    const/high16 v5, 0x40800000    # 4.0f

    .line 136
    .line 137
    div-float/2addr v4, v5

    .line 138
    add-float/2addr v2, v4

    .line 139
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 140
    .line 141
    int-to-float v4, v4

    .line 142
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 143
    .line 144
    .line 145
    move-result v6

    .line 146
    int-to-float v6, v6

    .line 147
    div-float/2addr v6, v5

    .line 148
    sub-float/2addr v4, v6

    .line 149
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 150
    .line 151
    int-to-float v6, v6

    .line 152
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 153
    .line 154
    .line 155
    move-result v7

    .line 156
    int-to-float v7, v7

    .line 157
    div-float/2addr v7, v5

    .line 158
    sub-float/2addr v6, v7

    .line 159
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    .line 160
    .line 161
    .line 162
    move-result v7

    .line 163
    sub-float/2addr v6, v7

    .line 164
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 165
    .line 166
    int-to-float v7, v7

    .line 167
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 168
    .line 169
    .line 170
    move-result v8

    .line 171
    int-to-float v8, v8

    .line 172
    div-float/2addr v8, v5

    .line 173
    sub-float/2addr v7, v8

    .line 174
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    .line 175
    .line 176
    .line 177
    move-result v5

    .line 178
    add-float/2addr v7, v5

    .line 179
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 180
    .line 181
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 182
    .line 183
    .line 184
    new-instance v8, Landroid/graphics/Path;

    .line 185
    .line 186
    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    .line 187
    .line 188
    .line 189
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 190
    .line 191
    invoke-virtual {v9, v2, v6, v4, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 192
    .line 193
    .line 194
    const/4 v2, 0x0

    .line 195
    const/high16 v4, 0x43160000    # 150.0f

    .line 196
    .line 197
    cmpl-float v0, v0, v2

    .line 198
    .line 199
    if-ltz v0, :cond_2

    .line 200
    .line 201
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 202
    .line 203
    const/high16 v2, 0x41700000    # 15.0f

    .line 204
    .line 205
    invoke-virtual {v8, v0, v2, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 206
    .line 207
    .line 208
    goto :goto_1

    .line 209
    :cond_2
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 210
    .line 211
    const/high16 v2, 0x43430000    # 195.0f

    .line 212
    .line 213
    invoke-virtual {v8, v0, v2, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 214
    .line 215
    .line 216
    :goto_1
    invoke-virtual {v5, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 217
    .line 218
    .line 219
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 220
    .line 221
    .line 222
    move-result-object v0

    .line 223
    invoke-virtual {v5, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 224
    .line 225
    .line 226
    invoke-virtual {v5, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 227
    .line 228
    .line 229
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 230
    .line 231
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    .line 233
    .line 234
    if-eqz v1, :cond_3

    .line 235
    .line 236
    new-instance v0, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 237
    .line 238
    invoke-direct {v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 239
    .line 240
    .line 241
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 242
    .line 243
    .line 244
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 245
    .line 246
    .line 247
    move-result-object v2

    .line 248
    invoke-virtual {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 249
    .line 250
    .line 251
    move-result v1

    .line 252
    const-wide v3, -0x4036666666666666L    # -0.2

    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    invoke-virtual {v2, v1, v3, v4}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 258
    .line 259
    .line 260
    move-result v1

    .line 261
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 262
    .line 263
    .line 264
    move-object v1, v0

    .line 265
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 266
    .line 267
    .line 268
    move-result v0

    .line 269
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 270
    .line 271
    .line 272
    move-result v2

    .line 273
    int-to-float v2, v2

    .line 274
    const/high16 v3, 0x40a00000    # 5.0f

    .line 275
    .line 276
    div-float/2addr v2, v3

    .line 277
    sub-float/2addr v0, v2

    .line 278
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 279
    .line 280
    .line 281
    move-result v2

    .line 282
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 283
    .line 284
    .line 285
    move-result v4

    .line 286
    int-to-float v4, v4

    .line 287
    const/high16 v5, 0x41200000    # 10.0f

    .line 288
    .line 289
    div-float/2addr v4, v5

    .line 290
    sub-float/2addr v2, v4

    .line 291
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 292
    .line 293
    .line 294
    move-result v4

    .line 295
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 296
    .line 297
    .line 298
    move-result v6

    .line 299
    int-to-float v6, v6

    .line 300
    div-float/2addr v6, v3

    .line 301
    sub-float/2addr v4, v6

    .line 302
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 303
    .line 304
    .line 305
    move-result v6

    .line 306
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 307
    .line 308
    .line 309
    move-result v7

    .line 310
    int-to-float v7, v7

    .line 311
    div-float/2addr v7, v5

    .line 312
    sub-float/2addr v6, v7

    .line 313
    new-instance v7, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 314
    .line 315
    invoke-direct {v7}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 316
    .line 317
    .line 318
    new-instance v8, Landroid/graphics/Path;

    .line 319
    .line 320
    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    .line 321
    .line 322
    .line 323
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 324
    .line 325
    invoke-virtual {v9, v0, v4, v2, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 326
    .line 327
    .line 328
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 329
    .line 330
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 331
    .line 332
    invoke-virtual {v8, v0, v2}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 333
    .line 334
    .line 335
    invoke-virtual {v7, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 336
    .line 337
    .line 338
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 339
    .line 340
    .line 341
    move-result-object v0

    .line 342
    invoke-virtual {v7, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 343
    .line 344
    .line 345
    invoke-virtual {v7, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 346
    .line 347
    .line 348
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 349
    .line 350
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 351
    .line 352
    .line 353
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 354
    .line 355
    .line 356
    move-result v0

    .line 357
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 358
    .line 359
    .line 360
    move-result v2

    .line 361
    int-to-float v2, v2

    .line 362
    div-float/2addr v2, v5

    .line 363
    add-float/2addr v0, v2

    .line 364
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 365
    .line 366
    .line 367
    move-result v2

    .line 368
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 369
    .line 370
    .line 371
    move-result p1

    .line 372
    int-to-float p1, p1

    .line 373
    div-float/2addr p1, v3

    .line 374
    add-float/2addr v2, p1

    .line 375
    new-instance p1, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 376
    .line 377
    invoke-direct {p1}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 378
    .line 379
    .line 380
    new-instance v3, Landroid/graphics/Path;

    .line 381
    .line 382
    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 383
    .line 384
    .line 385
    sget-object v5, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 386
    .line 387
    invoke-virtual {v5, v0, v4, v2, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 388
    .line 389
    .line 390
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 391
    .line 392
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 393
    .line 394
    invoke-virtual {v3, v0, v2}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 395
    .line 396
    .line 397
    invoke-virtual {p1, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 398
    .line 399
    .line 400
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 401
    .line 402
    .line 403
    move-result-object p0

    .line 404
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 405
    .line 406
    .line 407
    invoke-virtual {p1, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 408
    .line 409
    .line 410
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 411
    .line 412
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 413
    .line 414
    .line 415
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->paths:Ljava/util/List;

    .line 416
    .line 417
    return-object p0
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getSunPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 14

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    if-eqz p0, :cond_1

    .line 6
    .line 7
    array-length v0, p0

    .line 8
    const/4 v1, 0x1

    .line 9
    if-lt v0, v1, :cond_1

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    aget-object v1, p0, v0

    .line 13
    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    int-to-float v1, v1

    .line 21
    aget-object v2, p0, v0

    .line 22
    .line 23
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    mul-float v1, v1, v2

    .line 28
    .line 29
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    int-to-float v2, v2

    .line 34
    aget-object p0, p0, v0

    .line 35
    .line 36
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 37
    .line 38
    .line 39
    move-result p0

    .line 40
    mul-float v2, v2, p0

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_0
    const/4 v1, 0x0

    .line 44
    const/4 v2, 0x0

    .line 45
    goto :goto_0

    .line 46
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 47
    .line 48
    .line 49
    move-result p0

    .line 50
    int-to-float p0, p0

    .line 51
    const/high16 v0, 0x3e800000    # 0.25f

    .line 52
    .line 53
    mul-float v1, p0, v0

    .line 54
    .line 55
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 56
    .line 57
    .line 58
    move-result p0

    .line 59
    int-to-float p0, p0

    .line 60
    mul-float v2, p0, v0

    .line 61
    .line 62
    :goto_0
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 63
    .line 64
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 65
    .line 66
    int-to-float v0, v0

    .line 67
    add-float/2addr v0, v1

    .line 68
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 69
    .line 70
    int-to-float v3, v3

    .line 71
    add-float/2addr v3, v2

    .line 72
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 73
    .line 74
    int-to-float v4, v4

    .line 75
    sub-float/2addr v4, v1

    .line 76
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 77
    .line 78
    int-to-float v5, v5

    .line 79
    sub-float/2addr v5, v2

    .line 80
    invoke-virtual {p0, v0, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 81
    .line 82
    .line 83
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 84
    .line 85
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 86
    .line 87
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 88
    .line 89
    invoke-virtual {p0, v0, v3}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 90
    .line 91
    .line 92
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 93
    .line 94
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    int-to-float v0, v0

    .line 99
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 100
    .line 101
    int-to-float v3, v3

    .line 102
    invoke-virtual {p0, v0, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 103
    .line 104
    .line 105
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 106
    .line 107
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 108
    .line 109
    .line 110
    move-result v0

    .line 111
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 112
    .line 113
    .line 114
    move-result v3

    .line 115
    div-int/lit8 v3, v3, 0xe

    .line 116
    .line 117
    add-int/2addr v0, v3

    .line 118
    int-to-float v0, v0

    .line 119
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 120
    .line 121
    int-to-float v3, v3

    .line 122
    const/high16 v4, 0x3f400000    # 0.75f

    .line 123
    .line 124
    mul-float v2, v2, v4

    .line 125
    .line 126
    add-float/2addr v3, v2

    .line 127
    invoke-virtual {p0, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 128
    .line 129
    .line 130
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 131
    .line 132
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 133
    .line 134
    .line 135
    move-result v0

    .line 136
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 137
    .line 138
    .line 139
    move-result v3

    .line 140
    div-int/lit8 v3, v3, 0xe

    .line 141
    .line 142
    sub-int/2addr v0, v3

    .line 143
    int-to-float v0, v0

    .line 144
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 145
    .line 146
    int-to-float v3, v3

    .line 147
    add-float/2addr v3, v2

    .line 148
    invoke-virtual {p0, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 149
    .line 150
    .line 151
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 152
    .line 153
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 154
    .line 155
    .line 156
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 157
    .line 158
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 159
    .line 160
    .line 161
    move-result v0

    .line 162
    int-to-float v0, v0

    .line 163
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 164
    .line 165
    int-to-float v3, v3

    .line 166
    invoke-virtual {p0, v0, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 167
    .line 168
    .line 169
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 170
    .line 171
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 172
    .line 173
    .line 174
    move-result v0

    .line 175
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 176
    .line 177
    .line 178
    move-result v3

    .line 179
    div-int/lit8 v3, v3, 0xe

    .line 180
    .line 181
    sub-int/2addr v0, v3

    .line 182
    int-to-float v0, v0

    .line 183
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 184
    .line 185
    int-to-float v3, v3

    .line 186
    sub-float/2addr v3, v2

    .line 187
    invoke-virtual {p0, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 188
    .line 189
    .line 190
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 191
    .line 192
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 193
    .line 194
    .line 195
    move-result v0

    .line 196
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 197
    .line 198
    .line 199
    move-result v3

    .line 200
    div-int/lit8 v3, v3, 0xe

    .line 201
    .line 202
    add-int/2addr v0, v3

    .line 203
    int-to-float v0, v0

    .line 204
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 205
    .line 206
    int-to-float v3, v3

    .line 207
    sub-float/2addr v3, v2

    .line 208
    invoke-virtual {p0, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 209
    .line 210
    .line 211
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 212
    .line 213
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 214
    .line 215
    .line 216
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 217
    .line 218
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 219
    .line 220
    int-to-float v0, v0

    .line 221
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 222
    .line 223
    .line 224
    move-result v3

    .line 225
    int-to-float v3, v3

    .line 226
    invoke-virtual {p0, v0, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 227
    .line 228
    .line 229
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 230
    .line 231
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 232
    .line 233
    int-to-float v0, v0

    .line 234
    mul-float v1, v1, v4

    .line 235
    .line 236
    add-float/2addr v0, v1

    .line 237
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 238
    .line 239
    .line 240
    move-result v3

    .line 241
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 242
    .line 243
    .line 244
    move-result v4

    .line 245
    div-int/lit8 v4, v4, 0xe

    .line 246
    .line 247
    sub-int/2addr v3, v4

    .line 248
    int-to-float v3, v3

    .line 249
    invoke-virtual {p0, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 250
    .line 251
    .line 252
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 253
    .line 254
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 255
    .line 256
    int-to-float v0, v0

    .line 257
    add-float/2addr v0, v1

    .line 258
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 259
    .line 260
    .line 261
    move-result v3

    .line 262
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 263
    .line 264
    .line 265
    move-result v4

    .line 266
    div-int/lit8 v4, v4, 0xe

    .line 267
    .line 268
    add-int/2addr v3, v4

    .line 269
    int-to-float v3, v3

    .line 270
    invoke-virtual {p0, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 271
    .line 272
    .line 273
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 274
    .line 275
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 276
    .line 277
    .line 278
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 279
    .line 280
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 281
    .line 282
    int-to-float v0, v0

    .line 283
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 284
    .line 285
    .line 286
    move-result v3

    .line 287
    int-to-float v3, v3

    .line 288
    invoke-virtual {p0, v0, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 289
    .line 290
    .line 291
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 292
    .line 293
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 294
    .line 295
    int-to-float v0, v0

    .line 296
    sub-float/2addr v0, v1

    .line 297
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 298
    .line 299
    .line 300
    move-result v3

    .line 301
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 302
    .line 303
    .line 304
    move-result v4

    .line 305
    div-int/lit8 v4, v4, 0xe

    .line 306
    .line 307
    add-int/2addr v3, v4

    .line 308
    int-to-float v3, v3

    .line 309
    invoke-virtual {p0, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 310
    .line 311
    .line 312
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 313
    .line 314
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 315
    .line 316
    int-to-float v0, v0

    .line 317
    sub-float/2addr v0, v1

    .line 318
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 319
    .line 320
    .line 321
    move-result v3

    .line 322
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 323
    .line 324
    .line 325
    move-result v4

    .line 326
    div-int/lit8 v4, v4, 0xe

    .line 327
    .line 328
    sub-int/2addr v3, v4

    .line 329
    int-to-float v3, v3

    .line 330
    invoke-virtual {p0, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 331
    .line 332
    .line 333
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 334
    .line 335
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 336
    .line 337
    .line 338
    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    .line 339
    .line 340
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    .line 341
    .line 342
    .line 343
    move-result-wide v5

    .line 344
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 345
    .line 346
    .line 347
    move-result p0

    .line 348
    int-to-double v7, p0

    .line 349
    mul-double v5, v5, v7

    .line 350
    .line 351
    double-to-float p0, v5

    .line 352
    const/high16 v0, 0x40000000    # 2.0f

    .line 353
    .line 354
    div-float/2addr p0, v0

    .line 355
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    .line 356
    .line 357
    .line 358
    move-result-wide v5

    .line 359
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 360
    .line 361
    .line 362
    move-result v7

    .line 363
    int-to-double v7, v7

    .line 364
    mul-double v5, v5, v7

    .line 365
    .line 366
    double-to-float v5, v5

    .line 367
    div-float/2addr v5, v0

    .line 368
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    .line 369
    .line 370
    .line 371
    move-result-wide v6

    .line 372
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 373
    .line 374
    .line 375
    move-result v8

    .line 376
    int-to-float v8, v8

    .line 377
    mul-float v1, v1, v0

    .line 378
    .line 379
    sub-float/2addr v8, v1

    .line 380
    float-to-double v8, v8

    .line 381
    mul-double v6, v6, v8

    .line 382
    .line 383
    double-to-float v1, v6

    .line 384
    div-float/2addr v1, v0

    .line 385
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    .line 386
    .line 387
    .line 388
    move-result-wide v3

    .line 389
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 390
    .line 391
    .line 392
    move-result v6

    .line 393
    int-to-float v6, v6

    .line 394
    mul-float v2, v2, v0

    .line 395
    .line 396
    sub-float/2addr v6, v2

    .line 397
    float-to-double v6, v6

    .line 398
    mul-double v3, v3, v6

    .line 399
    .line 400
    double-to-float v2, v3

    .line 401
    div-float/2addr v2, v0

    .line 402
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 403
    .line 404
    .line 405
    move-result v0

    .line 406
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 407
    .line 408
    .line 409
    move-result v3

    .line 410
    add-int/2addr v0, v3

    .line 411
    div-int/lit8 v0, v0, 0x1c

    .line 412
    .line 413
    int-to-float v0, v0

    .line 414
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 415
    .line 416
    .line 417
    move-result v3

    .line 418
    int-to-float v3, v3

    .line 419
    mul-float v3, v3, v0

    .line 420
    .line 421
    float-to-double v3, v3

    .line 422
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 423
    .line 424
    .line 425
    move-result v6

    .line 426
    int-to-double v6, v6

    .line 427
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 428
    .line 429
    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    .line 430
    .line 431
    .line 432
    move-result-wide v6

    .line 433
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 434
    .line 435
    .line 436
    move-result v10

    .line 437
    int-to-double v10, v10

    .line 438
    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->pow(DD)D

    .line 439
    .line 440
    .line 441
    move-result-wide v10

    .line 442
    add-double/2addr v6, v10

    .line 443
    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    .line 444
    .line 445
    .line 446
    move-result-wide v6

    .line 447
    div-double/2addr v3, v6

    .line 448
    double-to-float v3, v3

    .line 449
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 450
    .line 451
    .line 452
    move-result v4

    .line 453
    int-to-float v4, v4

    .line 454
    mul-float v0, v0, v4

    .line 455
    .line 456
    float-to-double v6, v0

    .line 457
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 458
    .line 459
    .line 460
    move-result v0

    .line 461
    int-to-double v10, v0

    .line 462
    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->pow(DD)D

    .line 463
    .line 464
    .line 465
    move-result-wide v10

    .line 466
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 467
    .line 468
    .line 469
    move-result v0

    .line 470
    int-to-double v12, v0

    .line 471
    invoke-static {v12, v13, v8, v9}, Ljava/lang/Math;->pow(DD)D

    .line 472
    .line 473
    .line 474
    move-result-wide v8

    .line 475
    add-double/2addr v10, v8

    .line 476
    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    .line 477
    .line 478
    .line 479
    move-result-wide v8

    .line 480
    div-double/2addr v6, v8

    .line 481
    double-to-float v0, v6

    .line 482
    add-float v4, v1, v3

    .line 483
    .line 484
    sub-float v6, v2, v0

    .line 485
    .line 486
    sub-float/2addr v1, v3

    .line 487
    add-float/2addr v2, v0

    .line 488
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 489
    .line 490
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 491
    .line 492
    .line 493
    move-result v3

    .line 494
    int-to-float v3, v3

    .line 495
    add-float/2addr v3, p0

    .line 496
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 497
    .line 498
    .line 499
    move-result v7

    .line 500
    int-to-float v7, v7

    .line 501
    add-float/2addr v7, v5

    .line 502
    invoke-virtual {v0, v3, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 503
    .line 504
    .line 505
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 506
    .line 507
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 508
    .line 509
    .line 510
    move-result v3

    .line 511
    int-to-float v3, v3

    .line 512
    add-float/2addr v3, v4

    .line 513
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 514
    .line 515
    .line 516
    move-result v7

    .line 517
    int-to-float v7, v7

    .line 518
    add-float/2addr v7, v6

    .line 519
    invoke-virtual {v0, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 520
    .line 521
    .line 522
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 523
    .line 524
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 525
    .line 526
    .line 527
    move-result v3

    .line 528
    int-to-float v3, v3

    .line 529
    add-float/2addr v3, v1

    .line 530
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 531
    .line 532
    .line 533
    move-result v7

    .line 534
    int-to-float v7, v7

    .line 535
    add-float/2addr v7, v2

    .line 536
    invoke-virtual {v0, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 537
    .line 538
    .line 539
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 540
    .line 541
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 542
    .line 543
    .line 544
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 545
    .line 546
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 547
    .line 548
    .line 549
    move-result v3

    .line 550
    int-to-float v3, v3

    .line 551
    sub-float/2addr v3, p0

    .line 552
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 553
    .line 554
    .line 555
    move-result v7

    .line 556
    int-to-float v7, v7

    .line 557
    sub-float/2addr v7, v5

    .line 558
    invoke-virtual {v0, v3, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 559
    .line 560
    .line 561
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 562
    .line 563
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 564
    .line 565
    .line 566
    move-result v3

    .line 567
    int-to-float v3, v3

    .line 568
    sub-float/2addr v3, v4

    .line 569
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 570
    .line 571
    .line 572
    move-result v7

    .line 573
    int-to-float v7, v7

    .line 574
    sub-float/2addr v7, v6

    .line 575
    invoke-virtual {v0, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 576
    .line 577
    .line 578
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 579
    .line 580
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 581
    .line 582
    .line 583
    move-result v3

    .line 584
    int-to-float v3, v3

    .line 585
    sub-float/2addr v3, v1

    .line 586
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 587
    .line 588
    .line 589
    move-result v7

    .line 590
    int-to-float v7, v7

    .line 591
    sub-float/2addr v7, v2

    .line 592
    invoke-virtual {v0, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 593
    .line 594
    .line 595
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 596
    .line 597
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 598
    .line 599
    .line 600
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 601
    .line 602
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 603
    .line 604
    .line 605
    move-result v3

    .line 606
    int-to-float v3, v3

    .line 607
    add-float/2addr v3, p0

    .line 608
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 609
    .line 610
    .line 611
    move-result v7

    .line 612
    int-to-float v7, v7

    .line 613
    sub-float/2addr v7, v5

    .line 614
    invoke-virtual {v0, v3, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 615
    .line 616
    .line 617
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 618
    .line 619
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 620
    .line 621
    .line 622
    move-result v3

    .line 623
    int-to-float v3, v3

    .line 624
    add-float/2addr v3, v4

    .line 625
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 626
    .line 627
    .line 628
    move-result v7

    .line 629
    int-to-float v7, v7

    .line 630
    sub-float/2addr v7, v6

    .line 631
    invoke-virtual {v0, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 632
    .line 633
    .line 634
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 635
    .line 636
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 637
    .line 638
    .line 639
    move-result v3

    .line 640
    int-to-float v3, v3

    .line 641
    add-float/2addr v3, v1

    .line 642
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 643
    .line 644
    .line 645
    move-result v7

    .line 646
    int-to-float v7, v7

    .line 647
    sub-float/2addr v7, v2

    .line 648
    invoke-virtual {v0, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 649
    .line 650
    .line 651
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 652
    .line 653
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 654
    .line 655
    .line 656
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 657
    .line 658
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 659
    .line 660
    .line 661
    move-result v3

    .line 662
    int-to-float v3, v3

    .line 663
    sub-float/2addr v3, p0

    .line 664
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 665
    .line 666
    .line 667
    move-result p0

    .line 668
    int-to-float p0, p0

    .line 669
    add-float/2addr p0, v5

    .line 670
    invoke-virtual {v0, v3, p0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 671
    .line 672
    .line 673
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 674
    .line 675
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 676
    .line 677
    .line 678
    move-result v0

    .line 679
    int-to-float v0, v0

    .line 680
    sub-float/2addr v0, v4

    .line 681
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 682
    .line 683
    .line 684
    move-result v3

    .line 685
    int-to-float v3, v3

    .line 686
    add-float/2addr v3, v6

    .line 687
    invoke-virtual {p0, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 688
    .line 689
    .line 690
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 691
    .line 692
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 693
    .line 694
    .line 695
    move-result v0

    .line 696
    int-to-float v0, v0

    .line 697
    sub-float/2addr v0, v1

    .line 698
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 699
    .line 700
    .line 701
    move-result p1

    .line 702
    int-to-float p1, p1

    .line 703
    add-float/2addr p1, v2

    .line 704
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 705
    .line 706
    .line 707
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 708
    .line 709
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 710
    .line 711
    .line 712
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 713
    .line 714
    return-object p0
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getTeardropPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const/high16 v0, 0x40000000    # 2.0f

    .line 6
    .line 7
    if-eqz p0, :cond_0

    .line 8
    .line 9
    array-length v1, p0

    .line 10
    const/4 v2, 0x1

    .line 11
    if-lt v1, v2, :cond_0

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    aget-object v2, p0, v1

    .line 15
    .line 16
    if-eqz v2, :cond_0

    .line 17
    .line 18
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    int-to-float v2, v2

    .line 23
    div-float/2addr v2, v0

    .line 24
    aget-object v3, p0, v1

    .line 25
    .line 26
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    mul-float v2, v2, v3

    .line 31
    .line 32
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    int-to-float v3, v3

    .line 37
    div-float/2addr v3, v0

    .line 38
    aget-object p0, p0, v1

    .line 39
    .line 40
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 41
    .line 42
    .line 43
    move-result p0

    .line 44
    mul-float v3, v3, p0

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 48
    .line 49
    .line 50
    move-result p0

    .line 51
    int-to-float p0, p0

    .line 52
    div-float v2, p0, v0

    .line 53
    .line 54
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 55
    .line 56
    .line 57
    move-result p0

    .line 58
    int-to-float p0, p0

    .line 59
    div-float v3, p0, v0

    .line 60
    .line 61
    :goto_0
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 62
    .line 63
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 64
    .line 65
    int-to-float v1, v1

    .line 66
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 67
    .line 68
    .line 69
    move-result v4

    .line 70
    int-to-float v4, v4

    .line 71
    invoke-virtual {p0, v1, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 72
    .line 73
    .line 74
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 75
    .line 76
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 77
    .line 78
    int-to-float v1, v1

    .line 79
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 80
    .line 81
    int-to-float v4, v4

    .line 82
    iget v5, p1, Landroid/graphics/Rect;->right:I

    .line 83
    .line 84
    int-to-float v5, v5

    .line 85
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 86
    .line 87
    int-to-float v6, v6

    .line 88
    invoke-virtual {p0, v1, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 89
    .line 90
    .line 91
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 92
    .line 93
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->rectF:Landroid/graphics/RectF;

    .line 94
    .line 95
    const/4 v4, 0x0

    .line 96
    const/high16 v5, 0x43870000    # 270.0f

    .line 97
    .line 98
    invoke-virtual {p0, v1, v4, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 99
    .line 100
    .line 101
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 102
    .line 103
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 104
    .line 105
    .line 106
    move-result v1

    .line 107
    int-to-float v1, v1

    .line 108
    div-float v4, v2, v0

    .line 109
    .line 110
    add-float/2addr v1, v4

    .line 111
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 112
    .line 113
    int-to-float v4, v4

    .line 114
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 115
    .line 116
    .line 117
    move-result v5

    .line 118
    int-to-float v5, v5

    .line 119
    add-float/2addr v5, v2

    .line 120
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 121
    .line 122
    .line 123
    move-result v2

    .line 124
    int-to-float v2, v2

    .line 125
    sub-float/2addr v2, v3

    .line 126
    invoke-virtual {p0, v1, v4, v5, v2}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 127
    .line 128
    .line 129
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 130
    .line 131
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 132
    .line 133
    int-to-float v1, v1

    .line 134
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 135
    .line 136
    .line 137
    move-result v2

    .line 138
    int-to-float v2, v2

    .line 139
    div-float/2addr v3, v0

    .line 140
    sub-float/2addr v2, v3

    .line 141
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 142
    .line 143
    int-to-float v0, v0

    .line 144
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 145
    .line 146
    .line 147
    move-result p1

    .line 148
    int-to-float p1, p1

    .line 149
    invoke-virtual {p0, v1, v2, v0, p1}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 150
    .line 151
    .line 152
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 153
    .line 154
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 155
    .line 156
    .line 157
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 158
    .line 159
    return-object p0
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getTrapezoidPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->isAutoShape07()Z

    .line 6
    .line 7
    .line 8
    move-result p0

    .line 9
    const/4 v1, 0x1

    .line 10
    const/4 v2, 0x0

    .line 11
    if-eqz p0, :cond_1

    .line 12
    .line 13
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 14
    .line 15
    .line 16
    move-result p0

    .line 17
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    invoke-static {p0, v3}, Ljava/lang/Math;->min(II)I

    .line 22
    .line 23
    .line 24
    move-result p0

    .line 25
    int-to-float p0, p0

    .line 26
    const v3, 0x3e4ccccd    # 0.2f

    .line 27
    .line 28
    .line 29
    mul-float p0, p0, v3

    .line 30
    .line 31
    if-eqz v0, :cond_0

    .line 32
    .line 33
    array-length v3, v0

    .line 34
    if-lt v3, v1, :cond_0

    .line 35
    .line 36
    aget-object v1, v0, v2

    .line 37
    .line 38
    if-eqz v1, :cond_0

    .line 39
    .line 40
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 41
    .line 42
    .line 43
    move-result p0

    .line 44
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    invoke-static {p0, v1}, Ljava/lang/Math;->min(II)I

    .line 49
    .line 50
    .line 51
    move-result p0

    .line 52
    int-to-float p0, p0

    .line 53
    aget-object v0, v0, v2

    .line 54
    .line 55
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    mul-float p0, p0, v0

    .line 60
    .line 61
    :cond_0
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 62
    .line 63
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 64
    .line 65
    int-to-float v1, v1

    .line 66
    add-float/2addr v1, p0

    .line 67
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 68
    .line 69
    int-to-float v2, v2

    .line 70
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 71
    .line 72
    .line 73
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 74
    .line 75
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 76
    .line 77
    int-to-float v1, v1

    .line 78
    sub-float/2addr v1, p0

    .line 79
    iget p0, p1, Landroid/graphics/Rect;->top:I

    .line 80
    .line 81
    int-to-float p0, p0

    .line 82
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 83
    .line 84
    .line 85
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 86
    .line 87
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 88
    .line 89
    int-to-float v0, v0

    .line 90
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 91
    .line 92
    int-to-float v1, v1

    .line 93
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 94
    .line 95
    .line 96
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 97
    .line 98
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 99
    .line 100
    int-to-float v0, v0

    .line 101
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 102
    .line 103
    int-to-float p1, p1

    .line 104
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 105
    .line 106
    .line 107
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 108
    .line 109
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 110
    .line 111
    .line 112
    goto :goto_0

    .line 113
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 114
    .line 115
    .line 116
    move-result p0

    .line 117
    int-to-float p0, p0

    .line 118
    const/high16 v3, 0x3e800000    # 0.25f

    .line 119
    .line 120
    mul-float p0, p0, v3

    .line 121
    .line 122
    if-eqz v0, :cond_2

    .line 123
    .line 124
    array-length v3, v0

    .line 125
    if-lt v3, v1, :cond_2

    .line 126
    .line 127
    aget-object v1, v0, v2

    .line 128
    .line 129
    if-eqz v1, :cond_2

    .line 130
    .line 131
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 132
    .line 133
    .line 134
    move-result p0

    .line 135
    int-to-float p0, p0

    .line 136
    aget-object v0, v0, v2

    .line 137
    .line 138
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 139
    .line 140
    .line 141
    move-result v0

    .line 142
    mul-float p0, p0, v0

    .line 143
    .line 144
    :cond_2
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 145
    .line 146
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 147
    .line 148
    int-to-float v1, v1

    .line 149
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 150
    .line 151
    int-to-float v2, v2

    .line 152
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 153
    .line 154
    .line 155
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 156
    .line 157
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 158
    .line 159
    int-to-float v1, v1

    .line 160
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 161
    .line 162
    int-to-float v2, v2

    .line 163
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 164
    .line 165
    .line 166
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 167
    .line 168
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 169
    .line 170
    int-to-float v1, v1

    .line 171
    sub-float/2addr v1, p0

    .line 172
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 173
    .line 174
    int-to-float v2, v2

    .line 175
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 176
    .line 177
    .line 178
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 179
    .line 180
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 181
    .line 182
    int-to-float v1, v1

    .line 183
    add-float/2addr v1, p0

    .line 184
    iget p0, p1, Landroid/graphics/Rect;->bottom:I

    .line 185
    .line 186
    int-to-float p0, p0

    .line 187
    invoke-virtual {v0, v1, p0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 188
    .line 189
    .line 190
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 191
    .line 192
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 193
    .line 194
    .line 195
    :goto_0
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 196
    .line 197
    return-object p0
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getTrianglePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    const/high16 v1, 0x3f000000    # 0.5f

    .line 7
    .line 8
    mul-float v0, v0, v1

    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 11
    .line 12
    .line 13
    move-result-object p0

    .line 14
    if-eqz p0, :cond_0

    .line 15
    .line 16
    array-length v1, p0

    .line 17
    const/4 v2, 0x1

    .line 18
    if-lt v1, v2, :cond_0

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    aget-object v2, p0, v1

    .line 22
    .line 23
    if-eqz v2, :cond_0

    .line 24
    .line 25
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    int-to-float v0, v0

    .line 30
    aget-object p0, p0, v1

    .line 31
    .line 32
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 33
    .line 34
    .line 35
    move-result p0

    .line 36
    mul-float v0, v0, p0

    .line 37
    .line 38
    :cond_0
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 39
    .line 40
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 41
    .line 42
    int-to-float v1, v1

    .line 43
    add-float/2addr v1, v0

    .line 44
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 45
    .line 46
    int-to-float v0, v0

    .line 47
    invoke-virtual {p0, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 48
    .line 49
    .line 50
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 51
    .line 52
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 53
    .line 54
    int-to-float v0, v0

    .line 55
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 56
    .line 57
    int-to-float v1, v1

    .line 58
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 59
    .line 60
    .line 61
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 62
    .line 63
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 64
    .line 65
    int-to-float v0, v0

    .line 66
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 67
    .line 68
    int-to-float p1, p1

    .line 69
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 70
    .line 71
    .line 72
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 73
    .line 74
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 75
    .line 76
    .line 77
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/baseshape/BaseShapePathBuilder;->path:Landroid/graphics/Path;

    .line 78
    .line 79
    return-object p0
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.class public Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;
.super Ljava/lang/Object;
.source "MathPathBuilder.java"


# static fields
.field private static path:Landroid/graphics/Path;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static getMathDividePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 10

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    const v1, 0x3e70d845    # 0.2352f

    .line 7
    .line 8
    .line 9
    mul-float v0, v0, v1

    .line 10
    .line 11
    const/high16 v1, 0x40000000    # 2.0f

    .line 12
    .line 13
    div-float/2addr v0, v1

    .line 14
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    int-to-float v2, v2

    .line 19
    const v3, 0x3d70d845    # 0.0588f

    .line 20
    .line 21
    .line 22
    mul-float v2, v2, v3

    .line 23
    .line 24
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    int-to-float v3, v3

    .line 29
    const v4, 0x3df0d845    # 0.1176f

    .line 30
    .line 31
    .line 32
    mul-float v3, v3, v4

    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 35
    .line 36
    .line 37
    move-result-object p0

    .line 38
    if-eqz p0, :cond_2

    .line 39
    .line 40
    array-length v4, p0

    .line 41
    const/4 v5, 0x3

    .line 42
    if-lt v4, v5, :cond_2

    .line 43
    .line 44
    const/4 v4, 0x0

    .line 45
    aget-object v5, p0, v4

    .line 46
    .line 47
    if-eqz v5, :cond_0

    .line 48
    .line 49
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    int-to-float v0, v0

    .line 54
    aget-object v4, p0, v4

    .line 55
    .line 56
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 57
    .line 58
    .line 59
    move-result v4

    .line 60
    mul-float v0, v0, v4

    .line 61
    .line 62
    div-float/2addr v0, v1

    .line 63
    :cond_0
    const/4 v1, 0x1

    .line 64
    aget-object v4, p0, v1

    .line 65
    .line 66
    if-eqz v4, :cond_1

    .line 67
    .line 68
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 69
    .line 70
    .line 71
    move-result v2

    .line 72
    int-to-float v2, v2

    .line 73
    aget-object v1, p0, v1

    .line 74
    .line 75
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    mul-float v2, v2, v1

    .line 80
    .line 81
    :cond_1
    const/4 v1, 0x2

    .line 82
    aget-object v4, p0, v1

    .line 83
    .line 84
    if-eqz v4, :cond_2

    .line 85
    .line 86
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 87
    .line 88
    .line 89
    move-result v3

    .line 90
    int-to-float v3, v3

    .line 91
    aget-object p0, p0, v1

    .line 92
    .line 93
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 94
    .line 95
    .line 96
    move-result p0

    .line 97
    mul-float v3, v3, p0

    .line 98
    .line 99
    :cond_2
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 100
    .line 101
    iget p0, p1, Landroid/graphics/Rect;->left:I

    .line 102
    .line 103
    int-to-float p0, p0

    .line 104
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 105
    .line 106
    .line 107
    move-result v1

    .line 108
    int-to-float v1, v1

    .line 109
    const/high16 v5, 0x41000000    # 8.0f

    .line 110
    .line 111
    div-float/2addr v1, v5

    .line 112
    add-float/2addr p0, v1

    .line 113
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 114
    .line 115
    .line 116
    move-result v1

    .line 117
    sub-float v6, v1, v0

    .line 118
    .line 119
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 120
    .line 121
    int-to-float v1, v1

    .line 122
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 123
    .line 124
    .line 125
    move-result v7

    .line 126
    int-to-float v7, v7

    .line 127
    div-float/2addr v7, v5

    .line 128
    sub-float v7, v1, v7

    .line 129
    .line 130
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 131
    .line 132
    .line 133
    move-result v1

    .line 134
    add-float v8, v1, v0

    .line 135
    .line 136
    sget-object v9, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 137
    .line 138
    move v5, p0

    .line 139
    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 140
    .line 141
    .line 142
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 143
    .line 144
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 145
    .line 146
    .line 147
    move-result v1

    .line 148
    add-float/2addr v1, v3

    .line 149
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 150
    .line 151
    .line 152
    move-result v4

    .line 153
    sub-float/2addr v4, v0

    .line 154
    sub-float/2addr v4, v2

    .line 155
    sub-float/2addr v4, v3

    .line 156
    invoke-virtual {p0, v1, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 157
    .line 158
    .line 159
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 160
    .line 161
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 162
    .line 163
    .line 164
    move-result v1

    .line 165
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 166
    .line 167
    .line 168
    move-result v4

    .line 169
    sub-float/2addr v4, v0

    .line 170
    sub-float/2addr v4, v2

    .line 171
    sub-float/2addr v4, v3

    .line 172
    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 173
    .line 174
    invoke-virtual {p0, v1, v4, v3, v5}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 175
    .line 176
    .line 177
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 178
    .line 179
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 180
    .line 181
    .line 182
    move-result v1

    .line 183
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 184
    .line 185
    .line 186
    move-result v4

    .line 187
    add-float/2addr v4, v0

    .line 188
    add-float/2addr v4, v2

    .line 189
    add-float/2addr v4, v3

    .line 190
    invoke-virtual {p0, v1, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 191
    .line 192
    .line 193
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 194
    .line 195
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 196
    .line 197
    .line 198
    move-result v1

    .line 199
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 200
    .line 201
    .line 202
    move-result p1

    .line 203
    add-float/2addr p1, v0

    .line 204
    add-float/2addr p1, v2

    .line 205
    add-float/2addr p1, v3

    .line 206
    sget-object v0, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 207
    .line 208
    invoke-virtual {p0, v1, p1, v3, v0}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 209
    .line 210
    .line 211
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 212
    .line 213
    return-object p0
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getMathEqualPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 17

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    int-to-float v1, v1

    .line 8
    const v2, 0x3e70d845    # 0.2352f

    .line 9
    .line 10
    .line 11
    mul-float v1, v1, v2

    .line 12
    .line 13
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    int-to-float v2, v2

    .line 18
    const v3, 0x3df0d845    # 0.1176f

    .line 19
    .line 20
    .line 21
    mul-float v2, v2, v3

    .line 22
    .line 23
    const/high16 v3, 0x40000000    # 2.0f

    .line 24
    .line 25
    div-float/2addr v2, v3

    .line 26
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 27
    .line 28
    .line 29
    move-result-object v4

    .line 30
    if-eqz v4, :cond_1

    .line 31
    .line 32
    array-length v5, v4

    .line 33
    const/4 v6, 0x2

    .line 34
    if-lt v5, v6, :cond_1

    .line 35
    .line 36
    const/4 v5, 0x0

    .line 37
    aget-object v6, v4, v5

    .line 38
    .line 39
    if-eqz v6, :cond_0

    .line 40
    .line 41
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    int-to-float v1, v1

    .line 46
    aget-object v5, v4, v5

    .line 47
    .line 48
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 49
    .line 50
    .line 51
    move-result v5

    .line 52
    mul-float v1, v1, v5

    .line 53
    .line 54
    :cond_0
    const/4 v5, 0x1

    .line 55
    aget-object v6, v4, v5

    .line 56
    .line 57
    if-eqz v6, :cond_1

    .line 58
    .line 59
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    int-to-float v2, v2

    .line 64
    aget-object v4, v4, v5

    .line 65
    .line 66
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 67
    .line 68
    .line 69
    move-result v4

    .line 70
    mul-float v2, v2, v4

    .line 71
    .line 72
    div-float/2addr v2, v3

    .line 73
    :cond_1
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 74
    .line 75
    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    .line 76
    .line 77
    .line 78
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 79
    .line 80
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 81
    .line 82
    int-to-float v3, v3

    .line 83
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 84
    .line 85
    .line 86
    move-result v5

    .line 87
    int-to-float v5, v5

    .line 88
    const/high16 v10, 0x41000000    # 8.0f

    .line 89
    .line 90
    div-float/2addr v5, v10

    .line 91
    add-float/2addr v5, v3

    .line 92
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 93
    .line 94
    .line 95
    move-result v3

    .line 96
    sub-float/2addr v3, v2

    .line 97
    sub-float v6, v3, v1

    .line 98
    .line 99
    iget v3, v0, Landroid/graphics/Rect;->right:I

    .line 100
    .line 101
    int-to-float v3, v3

    .line 102
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 103
    .line 104
    .line 105
    move-result v7

    .line 106
    int-to-float v7, v7

    .line 107
    div-float/2addr v7, v10

    .line 108
    sub-float v7, v3, v7

    .line 109
    .line 110
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 111
    .line 112
    .line 113
    move-result v3

    .line 114
    sub-float v8, v3, v2

    .line 115
    .line 116
    sget-object v9, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 117
    .line 118
    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 119
    .line 120
    .line 121
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 122
    .line 123
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 124
    .line 125
    int-to-float v4, v4

    .line 126
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 127
    .line 128
    .line 129
    move-result v5

    .line 130
    int-to-float v5, v5

    .line 131
    div-float/2addr v5, v10

    .line 132
    add-float/2addr v4, v5

    .line 133
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 134
    .line 135
    .line 136
    move-result v5

    .line 137
    add-float/2addr v5, v2

    .line 138
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 139
    .line 140
    .line 141
    sget-object v11, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 142
    .line 143
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 144
    .line 145
    int-to-float v3, v3

    .line 146
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 147
    .line 148
    .line 149
    move-result v4

    .line 150
    int-to-float v4, v4

    .line 151
    div-float/2addr v4, v10

    .line 152
    add-float v12, v3, v4

    .line 153
    .line 154
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 155
    .line 156
    .line 157
    move-result v3

    .line 158
    add-float v13, v3, v2

    .line 159
    .line 160
    iget v3, v0, Landroid/graphics/Rect;->right:I

    .line 161
    .line 162
    int-to-float v3, v3

    .line 163
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 164
    .line 165
    .line 166
    move-result v4

    .line 167
    int-to-float v4, v4

    .line 168
    div-float/2addr v4, v10

    .line 169
    sub-float v14, v3, v4

    .line 170
    .line 171
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 172
    .line 173
    .line 174
    move-result v0

    .line 175
    add-float/2addr v0, v2

    .line 176
    add-float v15, v0, v1

    .line 177
    .line 178
    sget-object v16, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 179
    .line 180
    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 181
    .line 182
    .line 183
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 184
    .line 185
    return-object v0
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getMathMinusPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 9

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    const v1, 0x3e75c28f    # 0.24f

    .line 7
    .line 8
    .line 9
    mul-float v0, v0, v1

    .line 10
    .line 11
    const/high16 v1, 0x40000000    # 2.0f

    .line 12
    .line 13
    div-float/2addr v0, v1

    .line 14
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 15
    .line 16
    .line 17
    move-result-object p0

    .line 18
    if-eqz p0, :cond_0

    .line 19
    .line 20
    array-length v2, p0

    .line 21
    const/4 v3, 0x1

    .line 22
    if-lt v2, v3, :cond_0

    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    aget-object v3, p0, v2

    .line 26
    .line 27
    if-eqz v3, :cond_0

    .line 28
    .line 29
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    int-to-float v0, v0

    .line 34
    aget-object p0, p0, v2

    .line 35
    .line 36
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 37
    .line 38
    .line 39
    move-result p0

    .line 40
    mul-float v0, v0, p0

    .line 41
    .line 42
    div-float/2addr v0, v1

    .line 43
    :cond_0
    iget p0, p1, Landroid/graphics/Rect;->left:I

    .line 44
    .line 45
    int-to-float p0, p0

    .line 46
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    int-to-float v1, v1

    .line 51
    const/high16 v2, 0x41000000    # 8.0f

    .line 52
    .line 53
    div-float/2addr v1, v2

    .line 54
    add-float v4, p0, v1

    .line 55
    .line 56
    iget p0, p1, Landroid/graphics/Rect;->right:I

    .line 57
    .line 58
    int-to-float p0, p0

    .line 59
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 60
    .line 61
    .line 62
    move-result v1

    .line 63
    int-to-float v1, v1

    .line 64
    div-float/2addr v1, v2

    .line 65
    sub-float v6, p0, v1

    .line 66
    .line 67
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 68
    .line 69
    .line 70
    move-result p0

    .line 71
    sub-float v5, p0, v0

    .line 72
    .line 73
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 74
    .line 75
    .line 76
    move-result p0

    .line 77
    add-float v7, p0, v0

    .line 78
    .line 79
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 80
    .line 81
    sget-object v8, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 82
    .line 83
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 84
    .line 85
    .line 86
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 87
    .line 88
    return-object p0
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getMathMultiplyPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 12

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    const v1, 0x3e75c28f    # 0.24f

    .line 7
    .line 8
    .line 9
    mul-float v0, v0, v1

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    if-eqz p0, :cond_0

    .line 16
    .line 17
    array-length v1, p0

    .line 18
    const/4 v2, 0x1

    .line 19
    if-lt v1, v2, :cond_0

    .line 20
    .line 21
    const/4 v1, 0x0

    .line 22
    aget-object v2, p0, v1

    .line 23
    .line 24
    if-eqz v2, :cond_0

    .line 25
    .line 26
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    int-to-float v0, v0

    .line 31
    aget-object p0, p0, v1

    .line 32
    .line 33
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 34
    .line 35
    .line 36
    move-result p0

    .line 37
    mul-float v0, v0, p0

    .line 38
    .line 39
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 40
    .line 41
    .line 42
    move-result p0

    .line 43
    int-to-float p0, p0

    .line 44
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    int-to-float v1, v1

    .line 49
    div-float/2addr p0, v1

    .line 50
    mul-float v1, p0, p0

    .line 51
    .line 52
    const/high16 v2, 0x3f800000    # 1.0f

    .line 53
    .line 54
    add-float v3, v1, v2

    .line 55
    .line 56
    float-to-double v3, v3

    .line 57
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    .line 58
    .line 59
    .line 60
    move-result-wide v3

    .line 61
    double-to-float v3, v3

    .line 62
    mul-float v0, v0, v3

    .line 63
    .line 64
    const/high16 v3, 0x40000000    # 2.0f

    .line 65
    .line 66
    div-float/2addr v0, v3

    .line 67
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 68
    .line 69
    .line 70
    move-result v4

    .line 71
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 72
    .line 73
    .line 74
    move-result v5

    .line 75
    mul-int v4, v4, v5

    .line 76
    .line 77
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 78
    .line 79
    .line 80
    move-result v5

    .line 81
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 82
    .line 83
    .line 84
    move-result v6

    .line 85
    mul-int v5, v5, v6

    .line 86
    .line 87
    add-int/2addr v4, v5

    .line 88
    int-to-double v4, v4

    .line 89
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    .line 90
    .line 91
    .line 92
    move-result-wide v4

    .line 93
    double-to-float v4, v4

    .line 94
    div-float v1, v2, v1

    .line 95
    .line 96
    add-float/2addr v1, v2

    .line 97
    float-to-double v5, v1

    .line 98
    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    .line 99
    .line 100
    .line 101
    move-result-wide v5

    .line 102
    double-to-float v1, v5

    .line 103
    mul-float v4, v4, v1

    .line 104
    .line 105
    const/high16 v1, 0x40800000    # 4.0f

    .line 106
    .line 107
    div-float/2addr v4, v1

    .line 108
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 109
    .line 110
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 111
    .line 112
    add-int/2addr v1, v5

    .line 113
    int-to-float v1, v1

    .line 114
    div-float/2addr v1, v3

    .line 115
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 116
    .line 117
    .line 118
    move-result p1

    .line 119
    sub-float v3, v4, v0

    .line 120
    .line 121
    div-float/2addr v2, p0

    .line 122
    add-float/2addr v2, p0

    .line 123
    div-float/2addr v3, v2

    .line 124
    mul-float v5, p0, v3

    .line 125
    .line 126
    add-float/2addr v5, v0

    .line 127
    add-float/2addr v4, v0

    .line 128
    div-float/2addr v4, v2

    .line 129
    mul-float v2, p0, v4

    .line 130
    .line 131
    sub-float/2addr v2, v0

    .line 132
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 133
    .line 134
    sub-float v7, p1, v0

    .line 135
    .line 136
    invoke-virtual {v6, v1, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 137
    .line 138
    .line 139
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 140
    .line 141
    add-float v7, v1, v3

    .line 142
    .line 143
    sub-float v8, p1, v5

    .line 144
    .line 145
    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 146
    .line 147
    .line 148
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 149
    .line 150
    add-float v9, v1, v4

    .line 151
    .line 152
    sub-float v10, p1, v2

    .line 153
    .line 154
    invoke-virtual {v6, v9, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 155
    .line 156
    .line 157
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 158
    .line 159
    div-float p0, v0, p0

    .line 160
    .line 161
    add-float v11, v1, p0

    .line 162
    .line 163
    invoke-virtual {v6, v11, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 164
    .line 165
    .line 166
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 167
    .line 168
    add-float/2addr v2, p1

    .line 169
    invoke-virtual {v6, v9, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 170
    .line 171
    .line 172
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 173
    .line 174
    add-float/2addr v5, p1

    .line 175
    invoke-virtual {v6, v7, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 176
    .line 177
    .line 178
    sget-object v6, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 179
    .line 180
    add-float/2addr v0, p1

    .line 181
    invoke-virtual {v6, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 182
    .line 183
    .line 184
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 185
    .line 186
    sub-float v3, v1, v3

    .line 187
    .line 188
    invoke-virtual {v0, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 189
    .line 190
    .line 191
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 192
    .line 193
    sub-float v4, v1, v4

    .line 194
    .line 195
    invoke-virtual {v0, v4, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 196
    .line 197
    .line 198
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 199
    .line 200
    sub-float/2addr v1, p0

    .line 201
    invoke-virtual {v0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 202
    .line 203
    .line 204
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 205
    .line 206
    invoke-virtual {p0, v4, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 207
    .line 208
    .line 209
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 210
    .line 211
    invoke-virtual {p0, v3, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 212
    .line 213
    .line 214
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 215
    .line 216
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 217
    .line 218
    .line 219
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 220
    .line 221
    return-object p0
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getMathNotEqualPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 19

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    int-to-float v1, v1

    .line 8
    const v2, 0x3e70d845    # 0.2352f

    .line 9
    .line 10
    .line 11
    mul-float v1, v1, v2

    .line 12
    .line 13
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    int-to-float v2, v2

    .line 18
    const v3, 0x3df0d845    # 0.1176f

    .line 19
    .line 20
    .line 21
    mul-float v2, v2, v3

    .line 22
    .line 23
    const/high16 v3, 0x40000000    # 2.0f

    .line 24
    .line 25
    div-float/2addr v2, v3

    .line 26
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 27
    .line 28
    .line 29
    move-result-object v4

    .line 30
    const/high16 v5, 0x42dc0000    # 110.0f

    .line 31
    .line 32
    if-eqz v4, :cond_2

    .line 33
    .line 34
    array-length v6, v4

    .line 35
    const/4 v7, 0x3

    .line 36
    if-lt v6, v7, :cond_2

    .line 37
    .line 38
    const/4 v6, 0x0

    .line 39
    aget-object v7, v4, v6

    .line 40
    .line 41
    if-eqz v7, :cond_0

    .line 42
    .line 43
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    int-to-float v1, v1

    .line 48
    aget-object v6, v4, v6

    .line 49
    .line 50
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 51
    .line 52
    .line 53
    move-result v6

    .line 54
    mul-float v1, v1, v6

    .line 55
    .line 56
    :cond_0
    const/4 v6, 0x1

    .line 57
    aget-object v6, v4, v6

    .line 58
    .line 59
    if-eqz v6, :cond_1

    .line 60
    .line 61
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 62
    .line 63
    .line 64
    move-result v5

    .line 65
    const/high16 v6, 0x41200000    # 10.0f

    .line 66
    .line 67
    mul-float v5, v5, v6

    .line 68
    .line 69
    const/high16 v6, 0x40c00000    # 6.0f

    .line 70
    .line 71
    div-float/2addr v5, v6

    .line 72
    :cond_1
    const/4 v6, 0x2

    .line 73
    aget-object v7, v4, v6

    .line 74
    .line 75
    if-eqz v7, :cond_2

    .line 76
    .line 77
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 78
    .line 79
    .line 80
    move-result v2

    .line 81
    int-to-float v2, v2

    .line 82
    aget-object v4, v4, v6

    .line 83
    .line 84
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 85
    .line 86
    .line 87
    move-result v4

    .line 88
    mul-float v2, v2, v4

    .line 89
    .line 90
    div-float/2addr v2, v3

    .line 91
    :cond_2
    float-to-double v4, v5

    .line 92
    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    .line 93
    .line 94
    .line 95
    move-result-wide v4

    .line 96
    invoke-static {v4, v5}, Ljava/lang/Math;->tan(D)D

    .line 97
    .line 98
    .line 99
    move-result-wide v4

    .line 100
    double-to-float v4, v4

    .line 101
    neg-float v4, v4

    .line 102
    mul-float v5, v4, v4

    .line 103
    .line 104
    const/high16 v6, 0x3f800000    # 1.0f

    .line 105
    .line 106
    add-float v7, v5, v6

    .line 107
    .line 108
    float-to-double v7, v7

    .line 109
    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    .line 110
    .line 111
    .line 112
    move-result-wide v7

    .line 113
    double-to-float v7, v7

    .line 114
    mul-float v7, v7, v1

    .line 115
    .line 116
    div-float/2addr v7, v3

    .line 117
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 118
    .line 119
    .line 120
    move-result v8

    .line 121
    int-to-float v8, v8

    .line 122
    div-float/2addr v8, v3

    .line 123
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 124
    .line 125
    .line 126
    move-result v9

    .line 127
    int-to-float v9, v9

    .line 128
    div-float/2addr v9, v3

    .line 129
    sub-float v9, v7, v9

    .line 130
    .line 131
    div-float/2addr v9, v5

    .line 132
    sub-float/2addr v8, v9

    .line 133
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 134
    .line 135
    .line 136
    move-result v5

    .line 137
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 138
    .line 139
    .line 140
    move-result v9

    .line 141
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 142
    .line 143
    .line 144
    move-result v10

    .line 145
    int-to-float v10, v10

    .line 146
    div-float/2addr v10, v3

    .line 147
    sub-float/2addr v10, v7

    .line 148
    div-float/2addr v10, v4

    .line 149
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 150
    .line 151
    .line 152
    move-result v11

    .line 153
    int-to-float v11, v11

    .line 154
    div-float/2addr v11, v3

    .line 155
    add-float/2addr v8, v7

    .line 156
    div-float/2addr v6, v4

    .line 157
    add-float/2addr v6, v4

    .line 158
    div-float/2addr v8, v6

    .line 159
    mul-float v3, v4, v8

    .line 160
    .line 161
    sub-float/2addr v3, v7

    .line 162
    add-float v6, v2, v1

    .line 163
    .line 164
    sub-float v12, v6, v7

    .line 165
    .line 166
    div-float/2addr v12, v4

    .line 167
    add-float v13, v6, v7

    .line 168
    .line 169
    div-float/2addr v13, v4

    .line 170
    sub-float v14, v2, v7

    .line 171
    .line 172
    div-float/2addr v14, v4

    .line 173
    add-float/2addr v7, v2

    .line 174
    div-float/2addr v7, v4

    .line 175
    sget-object v15, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 176
    .line 177
    invoke-virtual {v15}, Landroid/graphics/Path;->reset()V

    .line 178
    .line 179
    .line 180
    sget-object v15, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 181
    .line 182
    move/from16 p0, v14

    .line 183
    .line 184
    iget v14, v0, Landroid/graphics/Rect;->left:I

    .line 185
    .line 186
    int-to-float v14, v14

    .line 187
    move/from16 v16, v7

    .line 188
    .line 189
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 190
    .line 191
    .line 192
    move-result v7

    .line 193
    int-to-float v7, v7

    .line 194
    const/high16 v17, 0x41000000    # 8.0f

    .line 195
    .line 196
    div-float v7, v7, v17

    .line 197
    .line 198
    add-float/2addr v14, v7

    .line 199
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 200
    .line 201
    .line 202
    move-result v7

    .line 203
    sub-float/2addr v7, v2

    .line 204
    sub-float/2addr v7, v1

    .line 205
    invoke-virtual {v15, v14, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 206
    .line 207
    .line 208
    const/4 v7, 0x0

    .line 209
    cmpl-float v4, v4, v7

    .line 210
    .line 211
    if-ltz v4, :cond_3

    .line 212
    .line 213
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 214
    .line 215
    add-float v7, v5, v12

    .line 216
    .line 217
    sub-float v14, v9, v6

    .line 218
    .line 219
    invoke-virtual {v4, v7, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 220
    .line 221
    .line 222
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 223
    .line 224
    add-float v7, v5, v10

    .line 225
    .line 226
    sub-float v15, v9, v11

    .line 227
    .line 228
    invoke-virtual {v4, v7, v15}, Landroid/graphics/Path;->lineTo(FF)V

    .line 229
    .line 230
    .line 231
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 232
    .line 233
    add-float v7, v5, v8

    .line 234
    .line 235
    sub-float v15, v9, v3

    .line 236
    .line 237
    invoke-virtual {v4, v7, v15}, Landroid/graphics/Path;->lineTo(FF)V

    .line 238
    .line 239
    .line 240
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 241
    .line 242
    add-float v7, v5, v13

    .line 243
    .line 244
    invoke-virtual {v4, v7, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 245
    .line 246
    .line 247
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 248
    .line 249
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 250
    .line 251
    int-to-float v7, v7

    .line 252
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 253
    .line 254
    .line 255
    move-result v14

    .line 256
    int-to-float v14, v14

    .line 257
    div-float v14, v14, v17

    .line 258
    .line 259
    sub-float/2addr v7, v14

    .line 260
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 261
    .line 262
    .line 263
    move-result v14

    .line 264
    sub-float/2addr v14, v2

    .line 265
    sub-float/2addr v14, v1

    .line 266
    invoke-virtual {v4, v7, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 267
    .line 268
    .line 269
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 270
    .line 271
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 272
    .line 273
    int-to-float v7, v7

    .line 274
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 275
    .line 276
    .line 277
    move-result v14

    .line 278
    int-to-float v14, v14

    .line 279
    div-float v14, v14, v17

    .line 280
    .line 281
    sub-float/2addr v7, v14

    .line 282
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 283
    .line 284
    .line 285
    move-result v14

    .line 286
    sub-float/2addr v14, v2

    .line 287
    invoke-virtual {v4, v7, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 288
    .line 289
    .line 290
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 291
    .line 292
    add-float v7, v5, v16

    .line 293
    .line 294
    sub-float v14, v9, v2

    .line 295
    .line 296
    invoke-virtual {v4, v7, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 297
    .line 298
    .line 299
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 300
    .line 301
    sub-float v7, v5, p0

    .line 302
    .line 303
    add-float v15, v9, v2

    .line 304
    .line 305
    invoke-virtual {v4, v7, v15}, Landroid/graphics/Path;->lineTo(FF)V

    .line 306
    .line 307
    .line 308
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 309
    .line 310
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 311
    .line 312
    int-to-float v7, v7

    .line 313
    move/from16 v18, v14

    .line 314
    .line 315
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 316
    .line 317
    .line 318
    move-result v14

    .line 319
    int-to-float v14, v14

    .line 320
    div-float v14, v14, v17

    .line 321
    .line 322
    sub-float/2addr v7, v14

    .line 323
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 324
    .line 325
    .line 326
    move-result v14

    .line 327
    add-float/2addr v14, v2

    .line 328
    invoke-virtual {v4, v7, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 329
    .line 330
    .line 331
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 332
    .line 333
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 334
    .line 335
    int-to-float v7, v7

    .line 336
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 337
    .line 338
    .line 339
    move-result v14

    .line 340
    int-to-float v14, v14

    .line 341
    div-float v14, v14, v17

    .line 342
    .line 343
    sub-float/2addr v7, v14

    .line 344
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 345
    .line 346
    .line 347
    move-result v14

    .line 348
    add-float/2addr v14, v2

    .line 349
    add-float/2addr v14, v1

    .line 350
    invoke-virtual {v4, v7, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 351
    .line 352
    .line 353
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 354
    .line 355
    sub-float v7, v5, v12

    .line 356
    .line 357
    add-float/2addr v6, v9

    .line 358
    invoke-virtual {v4, v7, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 359
    .line 360
    .line 361
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 362
    .line 363
    sub-float v7, v5, v10

    .line 364
    .line 365
    add-float/2addr v11, v9

    .line 366
    invoke-virtual {v4, v7, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 367
    .line 368
    .line 369
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 370
    .line 371
    sub-float v7, v5, v8

    .line 372
    .line 373
    add-float/2addr v9, v3

    .line 374
    invoke-virtual {v4, v7, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 375
    .line 376
    .line 377
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 378
    .line 379
    sub-float v4, v5, v13

    .line 380
    .line 381
    invoke-virtual {v3, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 382
    .line 383
    .line 384
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 385
    .line 386
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 387
    .line 388
    int-to-float v4, v4

    .line 389
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 390
    .line 391
    .line 392
    move-result v6

    .line 393
    int-to-float v6, v6

    .line 394
    div-float v6, v6, v17

    .line 395
    .line 396
    add-float/2addr v4, v6

    .line 397
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 398
    .line 399
    .line 400
    move-result v6

    .line 401
    add-float/2addr v6, v2

    .line 402
    add-float/2addr v6, v1

    .line 403
    invoke-virtual {v3, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 404
    .line 405
    .line 406
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 407
    .line 408
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 409
    .line 410
    int-to-float v3, v3

    .line 411
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 412
    .line 413
    .line 414
    move-result v4

    .line 415
    int-to-float v4, v4

    .line 416
    div-float v4, v4, v17

    .line 417
    .line 418
    add-float/2addr v3, v4

    .line 419
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 420
    .line 421
    .line 422
    move-result v4

    .line 423
    add-float/2addr v4, v2

    .line 424
    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 425
    .line 426
    .line 427
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 428
    .line 429
    sub-float v3, v5, v16

    .line 430
    .line 431
    invoke-virtual {v1, v3, v15}, Landroid/graphics/Path;->lineTo(FF)V

    .line 432
    .line 433
    .line 434
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 435
    .line 436
    add-float v5, v5, p0

    .line 437
    .line 438
    move/from16 v9, v18

    .line 439
    .line 440
    invoke-virtual {v1, v5, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 441
    .line 442
    .line 443
    goto/16 :goto_0

    .line 444
    .line 445
    :cond_3
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 446
    .line 447
    add-float v7, v5, v13

    .line 448
    .line 449
    sub-float v14, v9, v6

    .line 450
    .line 451
    invoke-virtual {v4, v7, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 452
    .line 453
    .line 454
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 455
    .line 456
    add-float v7, v5, v8

    .line 457
    .line 458
    sub-float v15, v9, v3

    .line 459
    .line 460
    invoke-virtual {v4, v7, v15}, Landroid/graphics/Path;->lineTo(FF)V

    .line 461
    .line 462
    .line 463
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 464
    .line 465
    add-float v7, v5, v10

    .line 466
    .line 467
    sub-float v15, v9, v11

    .line 468
    .line 469
    invoke-virtual {v4, v7, v15}, Landroid/graphics/Path;->lineTo(FF)V

    .line 470
    .line 471
    .line 472
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 473
    .line 474
    add-float v7, v5, v12

    .line 475
    .line 476
    invoke-virtual {v4, v7, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 477
    .line 478
    .line 479
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 480
    .line 481
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 482
    .line 483
    int-to-float v7, v7

    .line 484
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 485
    .line 486
    .line 487
    move-result v14

    .line 488
    int-to-float v14, v14

    .line 489
    div-float v14, v14, v17

    .line 490
    .line 491
    sub-float/2addr v7, v14

    .line 492
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 493
    .line 494
    .line 495
    move-result v14

    .line 496
    sub-float/2addr v14, v2

    .line 497
    sub-float/2addr v14, v1

    .line 498
    invoke-virtual {v4, v7, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 499
    .line 500
    .line 501
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 502
    .line 503
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 504
    .line 505
    int-to-float v7, v7

    .line 506
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 507
    .line 508
    .line 509
    move-result v14

    .line 510
    int-to-float v14, v14

    .line 511
    div-float v14, v14, v17

    .line 512
    .line 513
    sub-float/2addr v7, v14

    .line 514
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 515
    .line 516
    .line 517
    move-result v14

    .line 518
    sub-float/2addr v14, v2

    .line 519
    invoke-virtual {v4, v7, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 520
    .line 521
    .line 522
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 523
    .line 524
    add-float v14, v5, p0

    .line 525
    .line 526
    sub-float v7, v9, v2

    .line 527
    .line 528
    invoke-virtual {v4, v14, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 529
    .line 530
    .line 531
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 532
    .line 533
    sub-float v14, v5, v16

    .line 534
    .line 535
    add-float v15, v9, v2

    .line 536
    .line 537
    invoke-virtual {v4, v14, v15}, Landroid/graphics/Path;->lineTo(FF)V

    .line 538
    .line 539
    .line 540
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 541
    .line 542
    iget v14, v0, Landroid/graphics/Rect;->right:I

    .line 543
    .line 544
    int-to-float v14, v14

    .line 545
    move/from16 v18, v7

    .line 546
    .line 547
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 548
    .line 549
    .line 550
    move-result v7

    .line 551
    int-to-float v7, v7

    .line 552
    div-float v7, v7, v17

    .line 553
    .line 554
    sub-float/2addr v14, v7

    .line 555
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 556
    .line 557
    .line 558
    move-result v7

    .line 559
    add-float/2addr v7, v2

    .line 560
    invoke-virtual {v4, v14, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 561
    .line 562
    .line 563
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 564
    .line 565
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 566
    .line 567
    int-to-float v7, v7

    .line 568
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 569
    .line 570
    .line 571
    move-result v14

    .line 572
    int-to-float v14, v14

    .line 573
    div-float v14, v14, v17

    .line 574
    .line 575
    sub-float/2addr v7, v14

    .line 576
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 577
    .line 578
    .line 579
    move-result v14

    .line 580
    add-float/2addr v14, v2

    .line 581
    add-float/2addr v14, v1

    .line 582
    invoke-virtual {v4, v7, v14}, Landroid/graphics/Path;->lineTo(FF)V

    .line 583
    .line 584
    .line 585
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 586
    .line 587
    sub-float v7, v5, v13

    .line 588
    .line 589
    add-float/2addr v6, v9

    .line 590
    invoke-virtual {v4, v7, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 591
    .line 592
    .line 593
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 594
    .line 595
    sub-float v7, v5, v8

    .line 596
    .line 597
    add-float/2addr v3, v9

    .line 598
    invoke-virtual {v4, v7, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 599
    .line 600
    .line 601
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 602
    .line 603
    sub-float v4, v5, v10

    .line 604
    .line 605
    add-float/2addr v9, v11

    .line 606
    invoke-virtual {v3, v4, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 607
    .line 608
    .line 609
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 610
    .line 611
    sub-float v4, v5, v12

    .line 612
    .line 613
    invoke-virtual {v3, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 614
    .line 615
    .line 616
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 617
    .line 618
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 619
    .line 620
    int-to-float v4, v4

    .line 621
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 622
    .line 623
    .line 624
    move-result v6

    .line 625
    int-to-float v6, v6

    .line 626
    div-float v6, v6, v17

    .line 627
    .line 628
    add-float/2addr v4, v6

    .line 629
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 630
    .line 631
    .line 632
    move-result v6

    .line 633
    add-float/2addr v6, v2

    .line 634
    add-float/2addr v6, v1

    .line 635
    invoke-virtual {v3, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 636
    .line 637
    .line 638
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 639
    .line 640
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 641
    .line 642
    int-to-float v3, v3

    .line 643
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 644
    .line 645
    .line 646
    move-result v4

    .line 647
    int-to-float v4, v4

    .line 648
    div-float v4, v4, v17

    .line 649
    .line 650
    add-float/2addr v3, v4

    .line 651
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 652
    .line 653
    .line 654
    move-result v4

    .line 655
    add-float/2addr v4, v2

    .line 656
    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 657
    .line 658
    .line 659
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 660
    .line 661
    sub-float v3, v5, p0

    .line 662
    .line 663
    invoke-virtual {v1, v3, v15}, Landroid/graphics/Path;->lineTo(FF)V

    .line 664
    .line 665
    .line 666
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 667
    .line 668
    add-float v5, v5, v16

    .line 669
    .line 670
    move/from16 v9, v18

    .line 671
    .line 672
    invoke-virtual {v1, v5, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 673
    .line 674
    .line 675
    :goto_0
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 676
    .line 677
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 678
    .line 679
    int-to-float v3, v3

    .line 680
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 681
    .line 682
    .line 683
    move-result v4

    .line 684
    int-to-float v4, v4

    .line 685
    div-float v4, v4, v17

    .line 686
    .line 687
    add-float/2addr v3, v4

    .line 688
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 689
    .line 690
    .line 691
    move-result v0

    .line 692
    sub-float/2addr v0, v2

    .line 693
    invoke-virtual {v1, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 694
    .line 695
    .line 696
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 697
    .line 698
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 699
    .line 700
    .line 701
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 702
    .line 703
    return-object v0
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method public static getMathPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    packed-switch v0, :pswitch_data_0

    .line 11
    .line 12
    .line 13
    const/4 p0, 0x0

    .line 14
    return-object p0

    .line 15
    :pswitch_0
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->getMathNotEqualPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 16
    .line 17
    .line 18
    move-result-object p0

    .line 19
    return-object p0

    .line 20
    :pswitch_1
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->getMathEqualPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 21
    .line 22
    .line 23
    move-result-object p0

    .line 24
    return-object p0

    .line 25
    :pswitch_2
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->getMathDividePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 26
    .line 27
    .line 28
    move-result-object p0

    .line 29
    return-object p0

    .line 30
    :pswitch_3
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->getMathMultiplyPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 31
    .line 32
    .line 33
    move-result-object p0

    .line 34
    return-object p0

    .line 35
    :pswitch_4
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->getMathMinusPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 36
    .line 37
    .line 38
    move-result-object p0

    .line 39
    return-object p0

    .line 40
    :pswitch_5
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->getMathPlusPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 41
    .line 42
    .line 43
    move-result-object p0

    .line 44
    return-object p0

    .line 45
    :pswitch_data_0
    .packed-switch 0xe3
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static getMathPlusPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    const v1, 0x3e75c28f    # 0.24f

    .line 15
    .line 16
    .line 17
    mul-float v0, v0, v1

    .line 18
    .line 19
    const/high16 v1, 0x40000000    # 2.0f

    .line 20
    .line 21
    div-float/2addr v0, v1

    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 23
    .line 24
    .line 25
    move-result-object p0

    .line 26
    if-eqz p0, :cond_0

    .line 27
    .line 28
    array-length v2, p0

    .line 29
    const/4 v3, 0x1

    .line 30
    if-lt v2, v3, :cond_0

    .line 31
    .line 32
    const/4 v2, 0x0

    .line 33
    aget-object v3, p0, v2

    .line 34
    .line 35
    if-eqz v3, :cond_0

    .line 36
    .line 37
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    int-to-float v0, v0

    .line 50
    aget-object p0, p0, v2

    .line 51
    .line 52
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 53
    .line 54
    .line 55
    move-result p0

    .line 56
    mul-float v0, v0, p0

    .line 57
    .line 58
    div-float/2addr v0, v1

    .line 59
    :cond_0
    iget p0, p1, Landroid/graphics/Rect;->left:I

    .line 60
    .line 61
    int-to-float p0, p0

    .line 62
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    int-to-float v1, v1

    .line 67
    const/high16 v2, 0x41000000    # 8.0f

    .line 68
    .line 69
    div-float/2addr v1, v2

    .line 70
    add-float/2addr p0, v1

    .line 71
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 72
    .line 73
    int-to-float v1, v1

    .line 74
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 75
    .line 76
    .line 77
    move-result v3

    .line 78
    int-to-float v3, v3

    .line 79
    div-float/2addr v3, v2

    .line 80
    sub-float/2addr v1, v3

    .line 81
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 82
    .line 83
    int-to-float v3, v3

    .line 84
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 85
    .line 86
    .line 87
    move-result v4

    .line 88
    int-to-float v4, v4

    .line 89
    div-float/2addr v4, v2

    .line 90
    add-float/2addr v3, v4

    .line 91
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 92
    .line 93
    int-to-float v4, v4

    .line 94
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 95
    .line 96
    .line 97
    move-result v5

    .line 98
    int-to-float v5, v5

    .line 99
    div-float/2addr v5, v2

    .line 100
    sub-float/2addr v4, v5

    .line 101
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 102
    .line 103
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 104
    .line 105
    .line 106
    move-result v5

    .line 107
    sub-float/2addr v5, v0

    .line 108
    invoke-virtual {v2, p0, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 109
    .line 110
    .line 111
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 112
    .line 113
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 114
    .line 115
    .line 116
    move-result v5

    .line 117
    sub-float/2addr v5, v0

    .line 118
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 119
    .line 120
    .line 121
    move-result v6

    .line 122
    sub-float/2addr v6, v0

    .line 123
    invoke-virtual {v2, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 124
    .line 125
    .line 126
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 127
    .line 128
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 129
    .line 130
    .line 131
    move-result v5

    .line 132
    sub-float/2addr v5, v0

    .line 133
    invoke-virtual {v2, v5, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 134
    .line 135
    .line 136
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 137
    .line 138
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 139
    .line 140
    .line 141
    move-result v5

    .line 142
    add-float/2addr v5, v0

    .line 143
    invoke-virtual {v2, v5, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 144
    .line 145
    .line 146
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 147
    .line 148
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 149
    .line 150
    .line 151
    move-result v3

    .line 152
    add-float/2addr v3, v0

    .line 153
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 154
    .line 155
    .line 156
    move-result v5

    .line 157
    sub-float/2addr v5, v0

    .line 158
    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 159
    .line 160
    .line 161
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 162
    .line 163
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 164
    .line 165
    .line 166
    move-result v3

    .line 167
    sub-float/2addr v3, v0

    .line 168
    invoke-virtual {v2, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    .line 170
    .line 171
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 172
    .line 173
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 174
    .line 175
    .line 176
    move-result v3

    .line 177
    add-float/2addr v3, v0

    .line 178
    invoke-virtual {v2, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 179
    .line 180
    .line 181
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 182
    .line 183
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 184
    .line 185
    .line 186
    move-result v2

    .line 187
    add-float/2addr v2, v0

    .line 188
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 189
    .line 190
    .line 191
    move-result v3

    .line 192
    add-float/2addr v3, v0

    .line 193
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 194
    .line 195
    .line 196
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 197
    .line 198
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 199
    .line 200
    .line 201
    move-result v2

    .line 202
    add-float/2addr v2, v0

    .line 203
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 204
    .line 205
    .line 206
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 207
    .line 208
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 209
    .line 210
    .line 211
    move-result v2

    .line 212
    sub-float/2addr v2, v0

    .line 213
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 214
    .line 215
    .line 216
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 217
    .line 218
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    .line 219
    .line 220
    .line 221
    move-result v2

    .line 222
    sub-float/2addr v2, v0

    .line 223
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 224
    .line 225
    .line 226
    move-result v3

    .line 227
    add-float/2addr v3, v0

    .line 228
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 229
    .line 230
    .line 231
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 232
    .line 233
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    .line 234
    .line 235
    .line 236
    move-result p1

    .line 237
    add-float/2addr p1, v0

    .line 238
    invoke-virtual {v1, p0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 239
    .line 240
    .line 241
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 242
    .line 243
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 244
    .line 245
    .line 246
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/math/MathPathBuilder;->path:Landroid/graphics/Path;

    .line 247
    .line 248
    return-object p0
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

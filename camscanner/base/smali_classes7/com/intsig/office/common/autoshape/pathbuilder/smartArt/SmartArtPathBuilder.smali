.class public Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;
.super Ljava/lang/Object;
.source "SmartArtPathBuilder.java"


# static fields
.field private static final TODEGREE:F = 1.6666666f

.field private static path:Landroid/graphics/Path;

.field private static s_rect:Landroid/graphics/RectF;

.field private static sm:Landroid/graphics/Matrix;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/Matrix;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 7
    .line 8
    new-instance v0, Landroid/graphics/RectF;

    .line 9
    .line 10
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 14
    .line 15
    new-instance v0, Landroid/graphics/Path;

    .line 16
    .line 17
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static getAngle(DD)D
    .locals 4

    .line 1
    mul-double v0, p0, p0

    .line 2
    .line 3
    mul-double v2, p2, p2

    .line 4
    .line 5
    add-double/2addr v0, v2

    .line 6
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    .line 7
    .line 8
    .line 9
    move-result-wide v0

    .line 10
    div-double/2addr p0, v0

    .line 11
    invoke-static {p0, p1}, Ljava/lang/Math;->acos(D)D

    .line 12
    .line 13
    .line 14
    move-result-wide p0

    .line 15
    const-wide v0, 0x4066800000000000L    # 180.0

    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    mul-double p0, p0, v0

    .line 21
    .line 22
    const-wide v0, 0x400921fb54442d18L    # Math.PI

    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    div-double/2addr p0, v0

    .line 28
    const-wide/16 v0, 0x0

    .line 29
    .line 30
    cmpg-double v2, p2, v0

    .line 31
    .line 32
    if-gez v2, :cond_0

    .line 33
    .line 34
    const-wide p2, 0x4076800000000000L    # 360.0

    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    sub-double p0, p2, p0

    .line 40
    .line 41
    :cond_0
    return-wide p0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static getFunnelPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 2
    .line 3
    new-instance v0, Landroid/graphics/RectF;

    .line 4
    .line 5
    const/high16 v1, 0x442c0000    # 688.0f

    .line 6
    .line 7
    const/high16 v2, 0x436e0000    # 238.0f

    .line 8
    .line 9
    const/high16 v3, 0x41e00000    # 28.0f

    .line 10
    .line 11
    const/high16 v4, 0x41b00000    # 22.0f

    .line 12
    .line 13
    invoke-direct {v0, v3, v4, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 14
    .line 15
    .line 16
    sget-object v1, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 17
    .line 18
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 19
    .line 20
    .line 21
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 22
    .line 23
    const/high16 v0, 0x43020000    # 130.0f

    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    invoke-virtual {p0, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 27
    .line 28
    .line 29
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 30
    .line 31
    new-instance v0, Landroid/graphics/RectF;

    .line 32
    .line 33
    const/high16 v2, 0x43820000    # 260.0f

    .line 34
    .line 35
    const/high16 v3, 0x44330000    # 716.0f

    .line 36
    .line 37
    invoke-direct {v0, v1, v1, v3, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 38
    .line 39
    .line 40
    const/high16 v1, 0x43340000    # 180.0f

    .line 41
    .line 42
    invoke-virtual {p0, v0, v1, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 43
    .line 44
    .line 45
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 46
    .line 47
    new-instance v0, Landroid/graphics/RectF;

    .line 48
    .line 49
    const/high16 v1, 0x43de0000    # 444.0f

    .line 50
    .line 51
    const/high16 v2, 0x43e50000    # 458.0f

    .line 52
    .line 53
    const/high16 v4, 0x43810000    # 258.0f

    .line 54
    .line 55
    const/high16 v5, 0x44060000    # 536.0f

    .line 56
    .line 57
    invoke-direct {v0, v4, v1, v2, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 58
    .line 59
    .line 60
    const/high16 v1, 0x41f00000    # 30.0f

    .line 61
    .line 62
    const/high16 v2, 0x43160000    # 150.0f

    .line 63
    .line 64
    invoke-virtual {p0, v0, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 65
    .line 66
    .line 67
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 68
    .line 69
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 70
    .line 71
    .line 72
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 73
    .line 74
    invoke-virtual {p0}, Landroid/graphics/Matrix;->reset()V

    .line 75
    .line 76
    .line 77
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 78
    .line 79
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    int-to-float v0, v0

    .line 84
    div-float/2addr v0, v3

    .line 85
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 86
    .line 87
    .line 88
    move-result v1

    .line 89
    int-to-float v1, v1

    .line 90
    div-float/2addr v1, v5

    .line 91
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 92
    .line 93
    .line 94
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 95
    .line 96
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 97
    .line 98
    invoke-virtual {p0, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 99
    .line 100
    .line 101
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 102
    .line 103
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 104
    .line 105
    int-to-float v0, v0

    .line 106
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 107
    .line 108
    int-to-float p1, p1

    .line 109
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->offset(FF)V

    .line 110
    .line 111
    .line 112
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 113
    .line 114
    return-object p0
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getGear6Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 18

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 4
    .line 5
    const v2, 0x4a9c99b4    # 5131482.0f

    .line 6
    .line 7
    .line 8
    const v3, 0x49d40808    # 1736961.0f

    .line 9
    .line 10
    .line 11
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 12
    .line 13
    .line 14
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 15
    .line 16
    const v4, 0x4abb7a4a    # 6143269.0f

    .line 17
    .line 18
    .line 19
    const v5, 0x49aecef0    # 1432030.0f

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 23
    .line 24
    .line 25
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 26
    .line 27
    const v6, 0x4ac6d6e0    # 6515568.0f

    .line 28
    .line 29
    .line 30
    const v7, 0x49fd8648    # 2076873.0f

    .line 31
    .line 32
    .line 33
    invoke-virtual {v1, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 34
    .line 35
    .line 36
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 37
    .line 38
    const v8, 0x4aaf5772    # 5745593.0f

    .line 39
    .line 40
    .line 41
    const v9, 0x4a2aeff8    # 2800638.0f

    .line 42
    .line 43
    .line 44
    invoke-virtual {v1, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 45
    .line 46
    .line 47
    sget-object v10, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 48
    .line 49
    const v11, 0x4ab2bf66    # 5857203.0f

    .line 50
    .line 51
    .line 52
    const v12, 0x4a440d48    # 3212114.0f

    .line 53
    .line 54
    .line 55
    const v13, 0x4ab2bf66    # 5857203.0f

    .line 56
    .line 57
    .line 58
    const v14, 0x4a5e8710    # 3645892.0f

    .line 59
    .line 60
    .line 61
    const v15, 0x4aaf5770    # 5745592.0f

    .line 62
    .line 63
    .line 64
    const v16, 0x4a77a460    # 4057368.0f

    .line 65
    .line 66
    .line 67
    invoke-virtual/range {v10 .. v16}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 68
    .line 69
    .line 70
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 71
    .line 72
    const v8, 0x4a91e88e    # 4781127.0f

    .line 73
    .line 74
    .line 75
    invoke-virtual {v1, v6, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 76
    .line 77
    .line 78
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 79
    .line 80
    const v6, 0x4aa59664    # 5425970.0f

    .line 81
    .line 82
    .line 83
    invoke-virtual {v1, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 84
    .line 85
    .line 86
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 87
    .line 88
    const v4, 0x4a9c481e    # 5121039.0f

    .line 89
    .line 90
    .line 91
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 92
    .line 93
    .line 94
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 95
    .line 96
    const v10, 0x4a936db2    # 4830937.0f

    .line 97
    .line 98
    .line 99
    const v11, 0x4aa5829a    # 5423437.0f

    .line 100
    .line 101
    .line 102
    const v12, 0x4a87f6ce    # 4455271.0f

    .line 103
    .line 104
    .line 105
    const v13, 0x4aac2110    # 5640328.0f

    .line 106
    .line 107
    .line 108
    const v14, 0x4a76c5a8    # 4043114.0f

    .line 109
    .line 110
    .line 111
    const v15, 0x4aaf753e    # 5749407.0f

    .line 112
    .line 113
    .line 114
    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 115
    .line 116
    .line 117
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 118
    .line 119
    const v2, 0x4a68035c    # 3801303.0f

    .line 120
    .line 121
    .line 122
    const v4, 0x4aced9fc    # 6778110.0f

    .line 123
    .line 124
    .line 125
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 126
    .line 127
    .line 128
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 129
    .line 130
    const v9, 0x4a3a90e4    # 3056697.0f

    .line 131
    .line 132
    .line 133
    invoke-virtual {v1, v9, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 134
    .line 135
    .line 136
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 137
    .line 138
    const v4, 0x4a2bce90    # 2814884.0f

    .line 139
    .line 140
    .line 141
    const v10, 0x4aaf7544    # 5749410.0f

    .line 142
    .line 143
    .line 144
    invoke-virtual {v1, v4, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 145
    .line 146
    .line 147
    sget-object v11, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 148
    .line 149
    const v12, 0x4a12a69c    # 2402727.0f

    .line 150
    .line 151
    .line 152
    const v13, 0x4aac2114    # 5640330.0f

    .line 153
    .line 154
    .line 155
    const v14, 0x49f771b0    # 2027062.0f

    .line 156
    .line 157
    .line 158
    const v15, 0x4aa5829c    # 5423438.0f

    .line 159
    .line 160
    .line 161
    const v16, 0x49d2c1b0    # 1726518.0f

    .line 162
    .line 163
    .line 164
    const v17, 0x4a9c4820    # 5121040.0f

    .line 165
    .line 166
    .line 167
    invoke-virtual/range {v11 .. v17}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 168
    .line 169
    .line 170
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 171
    .line 172
    const v4, 0x492e7eb0    # 714731.0f

    .line 173
    .line 174
    .line 175
    invoke-virtual {v1, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 176
    .line 177
    .line 178
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 179
    .line 180
    const v6, 0x48a73400    # 342432.0f

    .line 181
    .line 182
    .line 183
    invoke-virtual {v1, v6, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 184
    .line 185
    .line 186
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 187
    .line 188
    const v8, 0x4987cab8    # 1112407.0f

    .line 189
    .line 190
    .line 191
    const v10, 0x4a77a448    # 4057362.0f

    .line 192
    .line 193
    .line 194
    invoke-virtual {v1, v8, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 195
    .line 196
    .line 197
    sget-object v11, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 198
    .line 199
    const v12, 0x497455c0    # 1000796.0f

    .line 200
    .line 201
    .line 202
    const v13, 0x4a5e86f8    # 3645886.0f

    .line 203
    .line 204
    .line 205
    const v14, 0x497455c0    # 1000796.0f

    .line 206
    .line 207
    .line 208
    const v15, 0x4a440d30    # 3212108.0f

    .line 209
    .line 210
    .line 211
    const v16, 0x4987cab8    # 1112407.0f

    .line 212
    .line 213
    .line 214
    const v17, 0x4a2aefe0    # 2800632.0f

    .line 215
    .line 216
    .line 217
    invoke-virtual/range {v11 .. v17}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 218
    .line 219
    .line 220
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 221
    .line 222
    invoke-virtual {v1, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 223
    .line 224
    .line 225
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 226
    .line 227
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 228
    .line 229
    .line 230
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 231
    .line 232
    const v4, 0x49d2c1b0    # 1726518.0f

    .line 233
    .line 234
    .line 235
    invoke-virtual {v1, v4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 236
    .line 237
    .line 238
    sget-object v10, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 239
    .line 240
    const v11, 0x49f771b8    # 2027063.0f

    .line 241
    .line 242
    .line 243
    const v12, 0x49af1e18    # 1434563.0f

    .line 244
    .line 245
    .line 246
    const v13, 0x4a12a6a4    # 2402729.0f

    .line 247
    .line 248
    .line 249
    const v14, 0x4994a448    # 1217673.0f

    .line 250
    .line 251
    .line 252
    const v15, 0x4a2bce98    # 2814886.0f

    .line 253
    .line 254
    .line 255
    const v16, 0x49875390    # 1108594.0f

    .line 256
    .line 257
    .line 258
    invoke-virtual/range {v10 .. v16}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 259
    .line 260
    .line 261
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 262
    .line 263
    const v3, 0x479c0900    # 79890.0f

    .line 264
    .line 265
    .line 266
    invoke-virtual {v1, v9, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 267
    .line 268
    .line 269
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 270
    .line 271
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 272
    .line 273
    .line 274
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 275
    .line 276
    const v2, 0x4a76c5b0    # 4043116.0f

    .line 277
    .line 278
    .line 279
    const v3, 0x49875370    # 1108590.0f

    .line 280
    .line 281
    .line 282
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 283
    .line 284
    .line 285
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 286
    .line 287
    const v5, 0x4a87f6d2    # 4455273.0f

    .line 288
    .line 289
    .line 290
    const v6, 0x4994a438    # 1217671.0f

    .line 291
    .line 292
    .line 293
    const v7, 0x4a936db4    # 4830938.0f

    .line 294
    .line 295
    .line 296
    const v8, 0x49af1e10    # 1434562.0f

    .line 297
    .line 298
    .line 299
    const v9, 0x4a9c99b4    # 5131482.0f

    .line 300
    .line 301
    .line 302
    const v10, 0x49d40808    # 1736961.0f

    .line 303
    .line 304
    .line 305
    invoke-virtual/range {v4 .. v10}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 306
    .line 307
    .line 308
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 309
    .line 310
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 311
    .line 312
    .line 313
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 314
    .line 315
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 316
    .line 317
    .line 318
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 319
    .line 320
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 321
    .line 322
    .line 323
    move-result v2

    .line 324
    int-to-float v2, v2

    .line 325
    const v3, 0x4ad14a20    # 6858000.0f

    .line 326
    .line 327
    .line 328
    div-float/2addr v2, v3

    .line 329
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 330
    .line 331
    .line 332
    move-result v4

    .line 333
    int-to-float v4, v4

    .line 334
    div-float/2addr v4, v3

    .line 335
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 336
    .line 337
    .line 338
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 339
    .line 340
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 341
    .line 342
    invoke-virtual {v1, v2}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 343
    .line 344
    .line 345
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 346
    .line 347
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 348
    .line 349
    int-to-float v2, v2

    .line 350
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 351
    .line 352
    int-to-float v0, v0

    .line 353
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->offset(FF)V

    .line 354
    .line 355
    .line 356
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 357
    .line 358
    return-object v0
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getGear9Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 20

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 4
    .line 5
    const v2, 0x4a7797d0    # 4056564.0f

    .line 6
    .line 7
    .line 8
    const v3, 0x495e7600    # 911200.0f

    .line 9
    .line 10
    .line 11
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 12
    .line 13
    .line 14
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 15
    .line 16
    const v2, 0x4a895ce2    # 4501105.0f

    .line 17
    .line 18
    .line 19
    const v4, 0x49036380    # 538168.0f

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 23
    .line 24
    .line 25
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 26
    .line 27
    const v2, 0x4a94335e    # 4856239.0f

    .line 28
    .line 29
    .line 30
    const v5, 0x494c2430    # 836163.0f

    .line 31
    .line 32
    .line 33
    invoke-virtual {v1, v2, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 34
    .line 35
    .line 36
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 37
    .line 38
    const v2, 0x4a8b5864    # 4566066.0f

    .line 39
    .line 40
    .line 41
    const v6, 0x49a36b28    # 1338725.0f

    .line 42
    .line 43
    .line 44
    invoke-virtual {v1, v2, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 45
    .line 46
    .line 47
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 48
    .line 49
    const v8, 0x4a91a456    # 4772395.0f

    .line 50
    .line 51
    .line 52
    const v9, 0x49bfc078    # 1570831.0f

    .line 53
    .line 54
    .line 55
    const v10, 0x4a966de6    # 4929267.0f

    .line 56
    .line 57
    .line 58
    const v11, 0x49e0eb80    # 1842544.0f

    .line 59
    .line 60
    .line 61
    const v12, 0x4a996a4e    # 5027111.0f

    .line 62
    .line 63
    .line 64
    const v13, 0x4a02730c    # 2137283.0f

    .line 65
    .line 66
    .line 67
    invoke-virtual/range {v7 .. v13}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 68
    .line 69
    .line 70
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 71
    .line 72
    const v2, 0x4aab200a    # 5607429.0f

    .line 73
    .line 74
    .line 75
    const v6, 0x4a0272d4    # 2137269.0f

    .line 76
    .line 77
    .line 78
    invoke-virtual {v1, v2, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 79
    .line 80
    .line 81
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 82
    .line 83
    const v2, 0x4aad94f6    # 5687931.0f

    .line 84
    .line 85
    .line 86
    const v7, 0x4a1e507c    # 2593823.0f

    .line 87
    .line 88
    .line 89
    invoke-virtual {v1, v2, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 90
    .line 91
    .line 92
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 93
    .line 94
    const v2, 0x4a9cf09a    # 5142605.0f

    .line 95
    .line 96
    .line 97
    const v8, 0x4a2a6d80    # 2792288.0f

    .line 98
    .line 99
    .line 100
    invoke-virtual {v1, v2, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 101
    .line 102
    .line 103
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 104
    .line 105
    const v10, 0x4a9d35d6    # 5151467.0f

    .line 106
    .line 107
    .line 108
    const v11, 0x4a3d5ff0    # 3102716.0f

    .line 109
    .line 110
    .line 111
    const v12, 0x4a9b8c32    # 5096985.0f

    .line 112
    .line 113
    .line 114
    const v13, 0x4a503bb8    # 3411694.0f

    .line 115
    .line 116
    .line 117
    const v14, 0x4a980daa    # 4982485.0f

    .line 118
    .line 119
    .line 120
    const v15, 0x4a61da44    # 3700369.0f

    .line 121
    .line 122
    .line 123
    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 124
    .line 125
    .line 126
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 127
    .line 128
    const v2, 0x4aa59ec8    # 5427044.0f

    .line 129
    .line 130
    .line 131
    const v8, 0x4a789e88    # 4073378.0f

    .line 132
    .line 133
    .line 134
    invoke-virtual {v1, v2, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 135
    .line 136
    .line 137
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 138
    .line 139
    const v2, 0x4a9e8bda    # 5195245.0f

    .line 140
    .line 141
    .line 142
    const v9, 0x4a888fe0    # 4474864.0f

    .line 143
    .line 144
    .line 145
    invoke-virtual {v1, v2, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 146
    .line 147
    .line 148
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 149
    .line 150
    const v2, 0x4a8de794    # 4649930.0f

    .line 151
    .line 152
    .line 153
    const v10, 0x4a828124    # 4276370.0f

    .line 154
    .line 155
    .line 156
    invoke-virtual {v1, v2, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 157
    .line 158
    .line 159
    sget-object v11, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 160
    .line 161
    const v12, 0x4a8805b6    # 4457179.0f

    .line 162
    .line 163
    .line 164
    const v13, 0x4a89ef7c    # 4519870.0f

    .line 165
    .line 166
    .line 167
    const v14, 0x4a80b006    # 4216835.0f

    .line 168
    .line 169
    .line 170
    const v15, 0x4a90170c    # 4721542.0f

    .line 171
    .line 172
    .line 173
    const v16, 0x4a70b22c    # 3943563.0f

    .line 174
    .line 175
    .line 176
    const v17, 0x4a9497b2    # 4869081.0f

    .line 177
    .line 178
    .line 179
    invoke-virtual/range {v11 .. v17}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 180
    .line 181
    .line 182
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 183
    .line 184
    const v2, 0x4a76d8f8    # 4044350.0f

    .line 185
    .line 186
    .line 187
    const v10, 0x4aa60888    # 5440580.0f

    .line 188
    .line 189
    .line 190
    invoke-virtual {v1, v2, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 191
    .line 192
    .line 193
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 194
    .line 195
    const v2, 0x4a5c421c    # 3608711.0f

    .line 196
    .line 197
    .line 198
    const v11, 0x4aaadf46    # 5599139.0f

    .line 199
    .line 200
    .line 201
    invoke-virtual {v1, v2, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 202
    .line 203
    .line 204
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 205
    .line 206
    const v2, 0x4a4a8c98    # 3318566.0f

    .line 207
    .line 208
    .line 209
    const v12, 0x4a9b88e2    # 5096561.0f

    .line 210
    .line 211
    .line 212
    invoke-virtual {v1, v2, v12}, Landroid/graphics/Path;->lineTo(FF)V

    .line 213
    .line 214
    .line 215
    sget-object v13, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 216
    .line 217
    const v14, 0x4a37fbe0    # 3014392.0f

    .line 218
    .line 219
    .line 220
    const v15, 0x4a9d7234    # 5159194.0f

    .line 221
    .line 222
    .line 223
    const v16, 0x4a24d598    # 2700646.0f

    .line 224
    .line 225
    .line 226
    const v17, 0x4a9d7234    # 5159194.0f

    .line 227
    .line 228
    .line 229
    const v18, 0x4a1244e0    # 2396472.0f

    .line 230
    .line 231
    .line 232
    const v19, 0x4a9b88e2    # 5096561.0f

    .line 233
    .line 234
    .line 235
    invoke-virtual/range {v13 .. v19}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 236
    .line 237
    .line 238
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 239
    .line 240
    const v2, 0x4a008f64    # 2106329.0f

    .line 241
    .line 242
    .line 243
    invoke-virtual {v1, v2, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 244
    .line 245
    .line 246
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 247
    .line 248
    const v2, 0x49cbf110    # 1670690.0f

    .line 249
    .line 250
    .line 251
    invoke-virtual {v1, v2, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 252
    .line 253
    .line 254
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 255
    .line 256
    const v2, 0x49d83ea0    # 1771476.0f

    .line 257
    .line 258
    .line 259
    const v10, 0x4a9497b2    # 4869081.0f

    .line 260
    .line 261
    .line 262
    invoke-virtual {v1, v2, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 263
    .line 264
    .line 265
    sget-object v11, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 266
    .line 267
    const v12, 0x49b6e2e8    # 1498205.0f

    .line 268
    .line 269
    .line 270
    const v13, 0x4a90170a    # 4721541.0f

    .line 271
    .line 272
    .line 273
    const v14, 0x49998c28    # 1257861.0f

    .line 274
    .line 275
    .line 276
    const v15, 0x4a89ef7a    # 4519869.0f

    .line 277
    .line 278
    .line 279
    const v16, 0x498204b0    # 1065110.0f

    .line 280
    .line 281
    .line 282
    const v17, 0x4a828122    # 4276369.0f

    .line 283
    .line 284
    .line 285
    invoke-virtual/range {v11 .. v17}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 286
    .line 287
    .line 288
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 289
    .line 290
    const v2, 0x48fdce60    # 519795.0f

    .line 291
    .line 292
    .line 293
    invoke-virtual {v1, v2, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 294
    .line 295
    .line 296
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 297
    .line 298
    const v2, 0x488c9f80    # 287996.0f

    .line 299
    .line 300
    .line 301
    invoke-virtual {v1, v2, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 302
    .line 303
    .line 304
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 305
    .line 306
    const v2, 0x4932d8b0    # 732555.0f

    .line 307
    .line 308
    .line 309
    const v8, 0x4a61da44    # 3700369.0f

    .line 310
    .line 311
    .line 312
    invoke-virtual {v1, v2, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 313
    .line 314
    .line 315
    sget-object v9, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 316
    .line 317
    const v10, 0x4916e470    # 618055.0f

    .line 318
    .line 319
    .line 320
    const v11, 0x4a503bb8    # 3411694.0f

    .line 321
    .line 322
    .line 323
    const v12, 0x49099760    # 563574.0f

    .line 324
    .line 325
    .line 326
    const v13, 0x4a3d5fec    # 3102715.0f

    .line 327
    .line 328
    .line 329
    const v14, 0x490bc140    # 572436.0f

    .line 330
    .line 331
    .line 332
    const v15, 0x4a2a6d80    # 2792288.0f

    .line 333
    .line 334
    .line 335
    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 336
    .line 337
    .line 338
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 339
    .line 340
    const v2, 0x46d3ca00    # 27109.0f

    .line 341
    .line 342
    .line 343
    invoke-virtual {v1, v2, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 344
    .line 345
    .line 346
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 347
    .line 348
    const v2, 0x47d22d80    # 107611.0f

    .line 349
    .line 350
    .line 351
    invoke-virtual {v1, v2, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 352
    .line 353
    .line 354
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 355
    .line 356
    const v2, 0x4927f380    # 687928.0f

    .line 357
    .line 358
    .line 359
    const v6, 0x4a02730c    # 2137283.0f

    .line 360
    .line 361
    .line 362
    invoke-virtual {v1, v2, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 363
    .line 364
    .line 365
    sget-object v7, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 366
    .line 367
    const v8, 0x493fd6d0    # 785773.0f

    .line 368
    .line 369
    .line 370
    const v9, 0x49e0eb80    # 1842544.0f

    .line 371
    .line 372
    .line 373
    const v10, 0x49662370    # 942647.0f

    .line 374
    .line 375
    .line 376
    const v11, 0x49bfc080    # 1570832.0f

    .line 377
    .line 378
    .line 379
    const v12, 0x498c4180    # 1148976.0f

    .line 380
    .line 381
    .line 382
    const v13, 0x49a36b30    # 1338726.0f

    .line 383
    .line 384
    .line 385
    invoke-virtual/range {v7 .. v13}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 386
    .line 387
    .line 388
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 389
    .line 390
    const v2, 0x4951ab10    # 858801.0f

    .line 391
    .line 392
    .line 393
    invoke-virtual {v1, v2, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 394
    .line 395
    .line 396
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 397
    .line 398
    const v2, 0x49942f78    # 1213935.0f

    .line 399
    .line 400
    .line 401
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 402
    .line 403
    .line 404
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 405
    .line 406
    const v2, 0x49ca7360    # 1658476.0f

    .line 407
    .line 408
    .line 409
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 410
    .line 411
    .line 412
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 413
    .line 414
    const v5, 0x49eaba20    # 1922884.0f

    .line 415
    .line 416
    .line 417
    const v6, 0x4936b170    # 748311.0f

    .line 418
    .line 419
    .line 420
    const v7, 0x4a075bb8    # 2217710.0f

    .line 421
    .line 422
    .line 423
    const v8, 0x491c7eb0    # 641003.0f

    .line 424
    .line 425
    .line 426
    const v9, 0x4a1a1c88    # 2524962.0f

    .line 427
    .line 428
    .line 429
    const v10, 0x49117720    # 595826.0f

    .line 430
    .line 431
    .line 432
    invoke-virtual/range {v4 .. v10}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 433
    .line 434
    .line 435
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 436
    .line 437
    const v2, 0x4a2042dc    # 2625719.0f

    .line 438
    .line 439
    .line 440
    const v3, 0x46bdfe00    # 24319.0f

    .line 441
    .line 442
    .line 443
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 444
    .line 445
    .line 446
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 447
    .line 448
    const v2, 0x4a3c8ea4    # 3089321.0f

    .line 449
    .line 450
    .line 451
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 452
    .line 453
    .line 454
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 455
    .line 456
    const v2, 0x4a42b4f4    # 3190077.0f

    .line 457
    .line 458
    .line 459
    const v3, 0x491176f0    # 595823.0f

    .line 460
    .line 461
    .line 462
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 463
    .line 464
    .line 465
    sget-object v4, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 466
    .line 467
    const v5, 0x4a5575c4    # 3497329.0f

    .line 468
    .line 469
    .line 470
    const v6, 0x491c7e90    # 641001.0f

    .line 471
    .line 472
    .line 473
    const v7, 0x4a677468    # 3792154.0f

    .line 474
    .line 475
    .line 476
    const v8, 0x4936b150    # 748309.0f

    .line 477
    .line 478
    .line 479
    const v9, 0x4a7797c8    # 4056562.0f

    .line 480
    .line 481
    .line 482
    const v10, 0x495e75f0    # 911199.0f

    .line 483
    .line 484
    .line 485
    invoke-virtual/range {v4 .. v10}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 486
    .line 487
    .line 488
    sget-object v11, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 489
    .line 490
    const v12, 0x4a7797cc    # 4056563.0f

    .line 491
    .line 492
    .line 493
    const v13, 0x495e75f0    # 911199.0f

    .line 494
    .line 495
    .line 496
    const v14, 0x4a7797cc    # 4056563.0f

    .line 497
    .line 498
    .line 499
    const v15, 0x495e7600    # 911200.0f

    .line 500
    .line 501
    .line 502
    const v16, 0x4a7797d0    # 4056564.0f

    .line 503
    .line 504
    .line 505
    const v17, 0x495e7600    # 911200.0f

    .line 506
    .line 507
    .line 508
    invoke-virtual/range {v11 .. v17}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 509
    .line 510
    .line 511
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 512
    .line 513
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 514
    .line 515
    .line 516
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 517
    .line 518
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 519
    .line 520
    .line 521
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 522
    .line 523
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 524
    .line 525
    .line 526
    move-result v2

    .line 527
    int-to-float v2, v2

    .line 528
    const v3, 0x4aae68c0    # 5715040.0f

    .line 529
    .line 530
    .line 531
    div-float/2addr v2, v3

    .line 532
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 533
    .line 534
    .line 535
    move-result v4

    .line 536
    int-to-float v4, v4

    .line 537
    div-float/2addr v4, v3

    .line 538
    invoke-virtual {v1, v2, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 539
    .line 540
    .line 541
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 542
    .line 543
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 544
    .line 545
    invoke-virtual {v1, v2}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 546
    .line 547
    .line 548
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 549
    .line 550
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 551
    .line 552
    int-to-float v2, v2

    .line 553
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 554
    .line 555
    int-to-float v0, v0

    .line 556
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->offset(FF)V

    .line 557
    .line 558
    .line 559
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 560
    .line 561
    return-object v0
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getLeftCircularArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 28

    .line 1
    const/4 v0, 0x1

    .line 2
    move-object/from16 v1, p0

    .line 3
    .line 4
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setFlipVertical(Z)V

    .line 5
    .line 6
    .line 7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const/4 v2, 0x2

    .line 12
    const/16 v3, 0x64

    .line 13
    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    array-length v4, v1

    .line 17
    const/4 v5, 0x5

    .line 18
    if-ne v4, v5, :cond_0

    .line 19
    .line 20
    int-to-float v3, v3

    .line 21
    const/4 v4, 0x0

    .line 22
    aget-object v4, v1, v4

    .line 23
    .line 24
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 25
    .line 26
    .line 27
    move-result v4

    .line 28
    mul-float v4, v4, v3

    .line 29
    .line 30
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 31
    .line 32
    .line 33
    move-result v4

    .line 34
    aget-object v0, v1, v0

    .line 35
    .line 36
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    const v5, 0x3fd55555

    .line 41
    .line 42
    .line 43
    mul-float v0, v0, v5

    .line 44
    .line 45
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    aget-object v6, v1, v2

    .line 50
    .line 51
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    .line 52
    .line 53
    .line 54
    move-result v6

    .line 55
    mul-float v6, v6, v5

    .line 56
    .line 57
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 58
    .line 59
    .line 60
    move-result v6

    .line 61
    const/4 v7, 0x3

    .line 62
    aget-object v7, v1, v7

    .line 63
    .line 64
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    .line 65
    .line 66
    .line 67
    move-result v7

    .line 68
    mul-float v7, v7, v5

    .line 69
    .line 70
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 71
    .line 72
    .line 73
    move-result v5

    .line 74
    neg-int v5, v5

    .line 75
    const/4 v7, 0x4

    .line 76
    aget-object v1, v1, v7

    .line 77
    .line 78
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 79
    .line 80
    .line 81
    move-result v1

    .line 82
    mul-float v3, v3, v1

    .line 83
    .line 84
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 85
    .line 86
    .line 87
    move-result v1

    .line 88
    goto :goto_0

    .line 89
    :cond_0
    int-to-float v0, v3

    .line 90
    const/high16 v1, 0x3e000000    # 0.125f

    .line 91
    .line 92
    mul-float v0, v0, v1

    .line 93
    .line 94
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 95
    .line 96
    .line 97
    move-result v4

    .line 98
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 99
    .line 100
    .line 101
    move-result v1

    .line 102
    const/16 v0, 0x14

    .line 103
    .line 104
    const/16 v6, 0x154

    .line 105
    .line 106
    const/16 v5, -0xb4

    .line 107
    .line 108
    :goto_0
    rsub-int/lit8 v3, v1, 0x32

    .line 109
    .line 110
    int-to-double v7, v3

    .line 111
    int-to-double v9, v6

    .line 112
    const-wide v11, 0x400921fb54442d18L    # Math.PI

    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    mul-double v9, v9, v11

    .line 118
    .line 119
    const-wide v13, 0x4066800000000000L    # 180.0

    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    div-double/2addr v9, v13

    .line 125
    invoke-static {v9, v10}, Ljava/lang/Math;->sin(D)D

    .line 126
    .line 127
    .line 128
    move-result-wide v15

    .line 129
    mul-double v15, v15, v7

    .line 130
    .line 131
    invoke-static {v9, v10}, Ljava/lang/Math;->cos(D)D

    .line 132
    .line 133
    .line 134
    move-result-wide v9

    .line 135
    mul-double v9, v9, v7

    .line 136
    .line 137
    add-int/2addr v0, v6

    .line 138
    move/from16 v17, v3

    .line 139
    .line 140
    int-to-double v2, v0

    .line 141
    mul-double v2, v2, v11

    .line 142
    .line 143
    div-double/2addr v2, v13

    .line 144
    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    .line 145
    .line 146
    .line 147
    move-result-wide v11

    .line 148
    mul-double v13, v11, v9

    .line 149
    .line 150
    sub-double/2addr v15, v13

    .line 151
    int-to-double v13, v1

    .line 152
    move-wide/from16 v18, v7

    .line 153
    .line 154
    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    .line 155
    .line 156
    invoke-static {v13, v14, v7, v8}, Ljava/lang/Math;->pow(DD)D

    .line 157
    .line 158
    .line 159
    move-result-wide v13

    .line 160
    invoke-static {v11, v12, v7, v8}, Ljava/lang/Math;->pow(DD)D

    .line 161
    .line 162
    .line 163
    move-result-wide v20

    .line 164
    const-wide/high16 v22, 0x3ff0000000000000L    # 1.0

    .line 165
    .line 166
    add-double v20, v20, v22

    .line 167
    .line 168
    div-double v13, v13, v20

    .line 169
    .line 170
    invoke-static {v13, v14}, Ljava/lang/Math;->sqrt(D)D

    .line 171
    .line 172
    .line 173
    move-result-wide v13

    .line 174
    const/4 v0, 0x2

    .line 175
    div-int/2addr v4, v0

    .line 176
    move-wide/from16 v20, v2

    .line 177
    .line 178
    int-to-double v2, v4

    .line 179
    invoke-static {v2, v3, v7, v8}, Ljava/lang/Math;->pow(DD)D

    .line 180
    .line 181
    .line 182
    move-result-wide v2

    .line 183
    invoke-static {v11, v12, v7, v8}, Ljava/lang/Math;->pow(DD)D

    .line 184
    .line 185
    .line 186
    move-result-wide v7

    .line 187
    add-double v7, v7, v22

    .line 188
    .line 189
    div-double/2addr v2, v7

    .line 190
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    .line 191
    .line 192
    .line 193
    move-result-wide v2

    .line 194
    const/16 v0, 0x5a

    .line 195
    .line 196
    if-le v6, v0, :cond_1

    .line 197
    .line 198
    const/16 v0, 0x10e

    .line 199
    .line 200
    if-ge v6, v0, :cond_1

    .line 201
    .line 202
    neg-double v13, v13

    .line 203
    neg-double v2, v2

    .line 204
    :cond_1
    add-double v6, v9, v2

    .line 205
    .line 206
    mul-double v22, v11, v6

    .line 207
    .line 208
    move-wide/from16 v24, v13

    .line 209
    .line 210
    add-double v13, v22, v15

    .line 211
    .line 212
    invoke-static {v6, v7, v13, v14}, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->getAngle(DD)D

    .line 213
    .line 214
    .line 215
    move-result-wide v6

    .line 216
    sub-double v2, v9, v2

    .line 217
    .line 218
    mul-double v13, v11, v2

    .line 219
    .line 220
    add-double/2addr v13, v15

    .line 221
    invoke-static {v2, v3, v13, v14}, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->getAngle(DD)D

    .line 222
    .line 223
    .line 224
    move-result-wide v2

    .line 225
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 226
    .line 227
    sub-int v8, v1, v4

    .line 228
    .line 229
    add-int/lit8 v8, v8, -0x32

    .line 230
    .line 231
    int-to-float v8, v8

    .line 232
    add-int v13, v17, v4

    .line 233
    .line 234
    int-to-float v13, v13

    .line 235
    invoke-virtual {v0, v8, v8, v13, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 236
    .line 237
    .line 238
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 239
    .line 240
    sget-object v8, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 241
    .line 242
    int-to-float v13, v5

    .line 243
    move-wide/from16 v22, v2

    .line 244
    .line 245
    int-to-double v2, v5

    .line 246
    sub-double/2addr v6, v2

    .line 247
    const-wide v26, 0x4076800000000000L    # 360.0

    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    add-double v6, v6, v26

    .line 253
    .line 254
    double-to-float v5, v6

    .line 255
    const/high16 v6, 0x43b40000    # 360.0f

    .line 256
    .line 257
    rem-float/2addr v5, v6

    .line 258
    invoke-virtual {v0, v8, v13, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 259
    .line 260
    .line 261
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 262
    .line 263
    add-double v13, v9, v24

    .line 264
    .line 265
    double-to-float v5, v13

    .line 266
    mul-double v13, v13, v11

    .line 267
    .line 268
    add-double/2addr v13, v15

    .line 269
    double-to-float v7, v13

    .line 270
    invoke-virtual {v0, v5, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 271
    .line 272
    .line 273
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 274
    .line 275
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->cos(D)D

    .line 276
    .line 277
    .line 278
    move-result-wide v7

    .line 279
    mul-double v7, v7, v18

    .line 280
    .line 281
    double-to-float v5, v7

    .line 282
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->sin(D)D

    .line 283
    .line 284
    .line 285
    move-result-wide v7

    .line 286
    mul-double v7, v7, v18

    .line 287
    .line 288
    double-to-float v7, v7

    .line 289
    invoke-virtual {v0, v5, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 290
    .line 291
    .line 292
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 293
    .line 294
    sub-double v9, v9, v24

    .line 295
    .line 296
    double-to-float v5, v9

    .line 297
    mul-double v11, v11, v9

    .line 298
    .line 299
    add-double/2addr v11, v15

    .line 300
    double-to-float v7, v11

    .line 301
    invoke-virtual {v0, v5, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 302
    .line 303
    .line 304
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 305
    .line 306
    add-int/2addr v1, v4

    .line 307
    add-int/lit8 v1, v1, -0x32

    .line 308
    .line 309
    int-to-float v1, v1

    .line 310
    sub-int v4, v17, v4

    .line 311
    .line 312
    int-to-float v4, v4

    .line 313
    invoke-virtual {v0, v1, v1, v4, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 314
    .line 315
    .line 316
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 317
    .line 318
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 319
    .line 320
    move-wide/from16 v4, v22

    .line 321
    .line 322
    double-to-float v7, v4

    .line 323
    sub-double/2addr v2, v4

    .line 324
    sub-double v2, v2, v26

    .line 325
    .line 326
    double-to-float v2, v2

    .line 327
    rem-float/2addr v2, v6

    .line 328
    invoke-virtual {v0, v1, v7, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 329
    .line 330
    .line 331
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 332
    .line 333
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 334
    .line 335
    .line 336
    new-instance v0, Landroid/graphics/Matrix;

    .line 337
    .line 338
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 339
    .line 340
    .line 341
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    .line 342
    .line 343
    .line 344
    move-result v1

    .line 345
    int-to-float v1, v1

    .line 346
    const/high16 v2, 0x42c80000    # 100.0f

    .line 347
    .line 348
    div-float/2addr v1, v2

    .line 349
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    .line 350
    .line 351
    .line 352
    move-result v3

    .line 353
    int-to-float v3, v3

    .line 354
    div-float/2addr v3, v2

    .line 355
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 356
    .line 357
    .line 358
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 359
    .line 360
    invoke-virtual {v1, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 361
    .line 362
    .line 363
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 364
    .line 365
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerX()I

    .line 366
    .line 367
    .line 368
    move-result v1

    .line 369
    int-to-float v1, v1

    .line 370
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->centerY()I

    .line 371
    .line 372
    .line 373
    move-result v2

    .line 374
    int-to-float v2, v2

    .line 375
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->offset(FF)V

    .line 376
    .line 377
    .line 378
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 379
    .line 380
    return-object v0
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private static getPieWedgePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 2
    .line 3
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 4
    .line 5
    int-to-float v0, v0

    .line 6
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 7
    .line 8
    int-to-float v1, v1

    .line 9
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 10
    .line 11
    .line 12
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 13
    .line 14
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 15
    .line 16
    int-to-float v0, v0

    .line 17
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 18
    .line 19
    int-to-float v1, v1

    .line 20
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 21
    .line 22
    .line 23
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 24
    .line 25
    new-instance v0, Landroid/graphics/RectF;

    .line 26
    .line 27
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 28
    .line 29
    int-to-float v2, v1

    .line 30
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    int-to-float v3, v3

    .line 33
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    mul-int/lit8 v4, v4, 0x2

    .line 38
    .line 39
    add-int/2addr v1, v4

    .line 40
    int-to-float v1, v1

    .line 41
    iget v4, p1, Landroid/graphics/Rect;->top:I

    .line 42
    .line 43
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    mul-int/lit8 p1, p1, 0x2

    .line 48
    .line 49
    add-int/2addr v4, p1

    .line 50
    int-to-float p1, v4

    .line 51
    invoke-direct {v0, v2, v3, v1, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 52
    .line 53
    .line 54
    const/high16 p1, 0x43340000    # 180.0f

    .line 55
    .line 56
    const/high16 v1, 0x42b40000    # 90.0f

    .line 57
    .line 58
    invoke-virtual {p0, v0, p1, v1}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 59
    .line 60
    .line 61
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 62
    .line 63
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 64
    .line 65
    .line 66
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 67
    .line 68
    return-object p0
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static getStarPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    packed-switch v0, :pswitch_data_0

    .line 11
    .line 12
    .line 13
    :pswitch_0
    const/4 p0, 0x0

    .line 14
    return-object p0

    .line 15
    :pswitch_1
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->getSwooshArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 16
    .line 17
    .line 18
    move-result-object p0

    .line 19
    return-object p0

    .line 20
    :pswitch_2
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->getPieWedgePath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 21
    .line 22
    .line 23
    move-result-object p0

    .line 24
    return-object p0

    .line 25
    :pswitch_3
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->getLeftCircularArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 26
    .line 27
    .line 28
    move-result-object p0

    .line 29
    return-object p0

    .line 30
    :pswitch_4
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->getGear9Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 31
    .line 32
    .line 33
    move-result-object p0

    .line 34
    return-object p0

    .line 35
    :pswitch_5
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->getGear6Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 36
    .line 37
    .line 38
    move-result-object p0

    .line 39
    return-object p0

    .line 40
    :pswitch_6
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->getFunnelPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 41
    .line 42
    .line 43
    move-result-object p0

    .line 44
    return-object p0

    .line 45
    :pswitch_data_0
    .packed-switch 0xf0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static getSwooshArrowPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 10

    .line 1
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    const v1, 0x4a5bba00    # 3600000.0f

    .line 5
    .line 6
    .line 7
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 8
    .line 9
    .line 10
    sget-object v2, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 11
    .line 12
    const v3, 0x48c35000    # 400000.0f

    .line 13
    .line 14
    .line 15
    const v4, 0x49f42400    # 2000000.0f

    .line 16
    .line 17
    .line 18
    const v5, 0x499eb100    # 1300000.0f

    .line 19
    .line 20
    .line 21
    const v6, 0x4967ef00    # 950000.0f

    .line 22
    .line 23
    .line 24
    const v7, 0x4a24cb80    # 2700000.0f

    .line 25
    .line 26
    .line 27
    const v8, 0x48dbba00    # 450000.0f

    .line 28
    .line 29
    .line 30
    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 31
    .line 32
    .line 33
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 34
    .line 35
    const v2, 0x4a21b344    # 2649297.0f

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 39
    .line 40
    .line 41
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 42
    .line 43
    const v0, 0x492fc800    # 720000.0f

    .line 44
    .line 45
    .line 46
    invoke-virtual {p0, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 47
    .line 48
    .line 49
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 50
    .line 51
    const v0, 0x4a2e1434    # 2852109.0f

    .line 52
    .line 53
    .line 54
    const v2, 0x49dbba00    # 1800000.0f

    .line 55
    .line 56
    .line 57
    invoke-virtual {p0, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 58
    .line 59
    .line 60
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 61
    .line 62
    const v0, 0x4a2afbf8

    .line 63
    .line 64
    .line 65
    const v2, 0x49a4cb80    # 1350000.0f

    .line 66
    .line 67
    .line 68
    invoke-virtual {p0, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 69
    .line 70
    .line 71
    sget-object v3, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 72
    .line 73
    const v4, 0x49bb3b50    # 1533802.0f

    .line 74
    .line 75
    .line 76
    const v5, 0x49bd3580    # 1550000.0f

    .line 77
    .line 78
    .line 79
    const v6, 0x49127c00    # 600000.0f

    .line 80
    .line 81
    .line 82
    const v7, 0x4a0c6180    # 2300000.0f

    .line 83
    .line 84
    .line 85
    const/4 v8, 0x0

    .line 86
    const v9, 0x4a5bba00    # 3600000.0f

    .line 87
    .line 88
    .line 89
    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 90
    .line 91
    .line 92
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 93
    .line 94
    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    .line 95
    .line 96
    .line 97
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 98
    .line 99
    invoke-virtual {p0}, Landroid/graphics/Matrix;->reset()V

    .line 100
    .line 101
    .line 102
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 103
    .line 104
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    int-to-float v0, v0

    .line 109
    div-float/2addr v0, v1

    .line 110
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 111
    .line 112
    .line 113
    move-result v2

    .line 114
    int-to-float v2, v2

    .line 115
    div-float/2addr v2, v1

    .line 116
    invoke-virtual {p0, v0, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 117
    .line 118
    .line 119
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 120
    .line 121
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 122
    .line 123
    invoke-virtual {p0, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 124
    .line 125
    .line 126
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 127
    .line 128
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 129
    .line 130
    int-to-float v0, v0

    .line 131
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 132
    .line 133
    int-to-float p1, p1

    .line 134
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->offset(FF)V

    .line 135
    .line 136
    .line 137
    sget-object p0, Lcom/intsig/office/common/autoshape/pathbuilder/smartArt/SmartArtPathBuilder;->path:Landroid/graphics/Path;

    .line 138
    .line 139
    return-object p0
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

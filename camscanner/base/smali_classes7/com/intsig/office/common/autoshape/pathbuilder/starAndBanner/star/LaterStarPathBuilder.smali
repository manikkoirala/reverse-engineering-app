.class public Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;
.super Ljava/lang/Object;
.source "LaterStarPathBuilder.java"


# static fields
.field public static s_rect:Landroid/graphics/RectF;

.field private static sm:Landroid/graphics/Matrix;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/Matrix;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 7
    .line 8
    new-instance v0, Landroid/graphics/RectF;

    .line 9
    .line 10
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->s_rect:Landroid/graphics/RectF;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static getStar10Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    int-to-float v0, v0

    .line 18
    const/4 v1, 0x2

    .line 19
    if-eqz p0, :cond_1

    .line 20
    .line 21
    array-length v2, p0

    .line 22
    if-ne v2, v1, :cond_1

    .line 23
    .line 24
    const/4 v2, 0x1

    .line 25
    aget-object v2, p0, v2

    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    mul-float v2, v2, v0

    .line 32
    .line 33
    const/4 v3, 0x0

    .line 34
    aget-object v4, p0, v3

    .line 35
    .line 36
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 37
    .line 38
    .line 39
    move-result v4

    .line 40
    const/high16 v5, 0x3f000000    # 0.5f

    .line 41
    .line 42
    cmpl-float v4, v4, v5

    .line 43
    .line 44
    if-lez v4, :cond_0

    .line 45
    .line 46
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 47
    .line 48
    .line 49
    move-result-object v4

    .line 50
    aput-object v4, p0, v3

    .line 51
    .line 52
    :cond_0
    aget-object v4, p0, v3

    .line 53
    .line 54
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 55
    .line 56
    .line 57
    move-result v4

    .line 58
    mul-float v4, v4, v2

    .line 59
    .line 60
    aget-object p0, p0, v3

    .line 61
    .line 62
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 63
    .line 64
    .line 65
    move-result p0

    .line 66
    goto :goto_0

    .line 67
    :cond_1
    const p0, 0x3f86963e

    .line 68
    .line 69
    .line 70
    mul-float v2, v0, p0

    .line 71
    .line 72
    const p0, 0x3ed70a3d    # 0.42f

    .line 73
    .line 74
    .line 75
    mul-float v4, v2, p0

    .line 76
    .line 77
    :goto_0
    mul-float p0, p0, v0

    .line 78
    .line 79
    float-to-int v2, v2

    .line 80
    div-int/2addr v2, v1

    .line 81
    float-to-int v3, v0

    .line 82
    div-int/2addr v3, v1

    .line 83
    float-to-int v1, v4

    .line 84
    float-to-int p0, p0

    .line 85
    const/16 v4, 0xa

    .line 86
    .line 87
    invoke-static {v2, v3, v1, p0, v4}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/StarPathBuilder;->getStarPath(IIIII)Landroid/graphics/Path;

    .line 88
    .line 89
    .line 90
    move-result-object p0

    .line 91
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 92
    .line 93
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 94
    .line 95
    .line 96
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 97
    .line 98
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 99
    .line 100
    .line 101
    move-result v2

    .line 102
    int-to-float v2, v2

    .line 103
    div-float/2addr v2, v0

    .line 104
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 105
    .line 106
    .line 107
    move-result v3

    .line 108
    int-to-float v3, v3

    .line 109
    div-float/2addr v3, v0

    .line 110
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 111
    .line 112
    .line 113
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 114
    .line 115
    invoke-virtual {p0, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 116
    .line 117
    .line 118
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    int-to-float v0, v0

    .line 123
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 124
    .line 125
    .line 126
    move-result p1

    .line 127
    int-to-float p1, p1

    .line 128
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->offset(FF)V

    .line 129
    .line 130
    .line 131
    return-object p0
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getStar12Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    int-to-float v0, v0

    .line 18
    if-eqz p0, :cond_1

    .line 19
    .line 20
    array-length v1, p0

    .line 21
    const/4 v2, 0x1

    .line 22
    if-ne v1, v2, :cond_1

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    aget-object v2, p0, v1

    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    const/high16 v3, 0x3f000000    # 0.5f

    .line 32
    .line 33
    cmpl-float v2, v2, v3

    .line 34
    .line 35
    if-lez v2, :cond_0

    .line 36
    .line 37
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    aput-object v2, p0, v1

    .line 42
    .line 43
    :cond_0
    aget-object v2, p0, v1

    .line 44
    .line 45
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    mul-float v2, v2, v0

    .line 50
    .line 51
    aget-object p0, p0, v1

    .line 52
    .line 53
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 54
    .line 55
    .line 56
    move-result p0

    .line 57
    mul-float p0, p0, v0

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_1
    const p0, 0x3ebc6a7f    # 0.368f

    .line 61
    .line 62
    .line 63
    mul-float v2, v0, p0

    .line 64
    .line 65
    move p0, v2

    .line 66
    :goto_0
    float-to-int v1, v0

    .line 67
    div-int/lit8 v1, v1, 0x2

    .line 68
    .line 69
    float-to-int v2, v2

    .line 70
    float-to-int p0, p0

    .line 71
    const/16 v3, 0xc

    .line 72
    .line 73
    invoke-static {v1, v1, v2, p0, v3}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/StarPathBuilder;->getStarPath(IIIII)Landroid/graphics/Path;

    .line 74
    .line 75
    .line 76
    move-result-object p0

    .line 77
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 78
    .line 79
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 80
    .line 81
    .line 82
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 83
    .line 84
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 85
    .line 86
    .line 87
    move-result v2

    .line 88
    int-to-float v2, v2

    .line 89
    div-float/2addr v2, v0

    .line 90
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 91
    .line 92
    .line 93
    move-result v3

    .line 94
    int-to-float v3, v3

    .line 95
    div-float/2addr v3, v0

    .line 96
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 97
    .line 98
    .line 99
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 100
    .line 101
    invoke-virtual {p0, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    int-to-float v0, v0

    .line 109
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 110
    .line 111
    .line 112
    move-result p1

    .line 113
    int-to-float p1, p1

    .line 114
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->offset(FF)V

    .line 115
    .line 116
    .line 117
    return-object p0
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getStar16Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    int-to-float v0, v0

    .line 18
    if-eqz p0, :cond_1

    .line 19
    .line 20
    array-length v1, p0

    .line 21
    const/4 v2, 0x1

    .line 22
    if-ne v1, v2, :cond_1

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    aget-object v2, p0, v1

    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    const/high16 v3, 0x3f000000    # 0.5f

    .line 32
    .line 33
    cmpl-float v2, v2, v3

    .line 34
    .line 35
    if-lez v2, :cond_0

    .line 36
    .line 37
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    aput-object v2, p0, v1

    .line 42
    .line 43
    :cond_0
    aget-object v2, p0, v1

    .line 44
    .line 45
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    mul-float v2, v2, v0

    .line 50
    .line 51
    aget-object p0, p0, v1

    .line 52
    .line 53
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 54
    .line 55
    .line 56
    move-result p0

    .line 57
    mul-float p0, p0, v0

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_1
    const p0, 0x3ebc6a7f    # 0.368f

    .line 61
    .line 62
    .line 63
    mul-float v2, v0, p0

    .line 64
    .line 65
    move p0, v2

    .line 66
    :goto_0
    float-to-int v1, v0

    .line 67
    div-int/lit8 v1, v1, 0x2

    .line 68
    .line 69
    float-to-int v2, v2

    .line 70
    float-to-int p0, p0

    .line 71
    const/16 v3, 0x10

    .line 72
    .line 73
    invoke-static {v1, v1, v2, p0, v3}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/StarPathBuilder;->getStarPath(IIIII)Landroid/graphics/Path;

    .line 74
    .line 75
    .line 76
    move-result-object p0

    .line 77
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 78
    .line 79
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 80
    .line 81
    .line 82
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 83
    .line 84
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 85
    .line 86
    .line 87
    move-result v2

    .line 88
    int-to-float v2, v2

    .line 89
    div-float/2addr v2, v0

    .line 90
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 91
    .line 92
    .line 93
    move-result v3

    .line 94
    int-to-float v3, v3

    .line 95
    div-float/2addr v3, v0

    .line 96
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 97
    .line 98
    .line 99
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 100
    .line 101
    invoke-virtual {p0, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    int-to-float v0, v0

    .line 109
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 110
    .line 111
    .line 112
    move-result p1

    .line 113
    int-to-float p1, p1

    .line 114
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->offset(FF)V

    .line 115
    .line 116
    .line 117
    return-object p0
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getStar24Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    int-to-float v0, v0

    .line 18
    if-eqz p0, :cond_1

    .line 19
    .line 20
    array-length v1, p0

    .line 21
    const/4 v2, 0x1

    .line 22
    if-ne v1, v2, :cond_1

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    aget-object v2, p0, v1

    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    const/high16 v3, 0x3f000000    # 0.5f

    .line 32
    .line 33
    cmpl-float v2, v2, v3

    .line 34
    .line 35
    if-lez v2, :cond_0

    .line 36
    .line 37
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    aput-object v2, p0, v1

    .line 42
    .line 43
    :cond_0
    aget-object v2, p0, v1

    .line 44
    .line 45
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    mul-float v2, v2, v0

    .line 50
    .line 51
    aget-object p0, p0, v1

    .line 52
    .line 53
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 54
    .line 55
    .line 56
    move-result p0

    .line 57
    mul-float p0, p0, v0

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_1
    const p0, 0x3ebc6a7f    # 0.368f

    .line 61
    .line 62
    .line 63
    mul-float v2, v0, p0

    .line 64
    .line 65
    move p0, v2

    .line 66
    :goto_0
    float-to-int v1, v0

    .line 67
    div-int/lit8 v1, v1, 0x2

    .line 68
    .line 69
    float-to-int v2, v2

    .line 70
    float-to-int p0, p0

    .line 71
    const/16 v3, 0x18

    .line 72
    .line 73
    invoke-static {v1, v1, v2, p0, v3}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/StarPathBuilder;->getStarPath(IIIII)Landroid/graphics/Path;

    .line 74
    .line 75
    .line 76
    move-result-object p0

    .line 77
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 78
    .line 79
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 80
    .line 81
    .line 82
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 83
    .line 84
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 85
    .line 86
    .line 87
    move-result v2

    .line 88
    int-to-float v2, v2

    .line 89
    div-float/2addr v2, v0

    .line 90
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 91
    .line 92
    .line 93
    move-result v3

    .line 94
    int-to-float v3, v3

    .line 95
    div-float/2addr v3, v0

    .line 96
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 97
    .line 98
    .line 99
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 100
    .line 101
    invoke-virtual {p0, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    int-to-float v0, v0

    .line 109
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 110
    .line 111
    .line 112
    move-result p1

    .line 113
    int-to-float p1, p1

    .line 114
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->offset(FF)V

    .line 115
    .line 116
    .line 117
    return-object p0
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getStar32Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    int-to-float v0, v0

    .line 18
    if-eqz p0, :cond_1

    .line 19
    .line 20
    array-length v1, p0

    .line 21
    const/4 v2, 0x1

    .line 22
    if-ne v1, v2, :cond_1

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    aget-object v2, p0, v1

    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    const/high16 v3, 0x3f000000    # 0.5f

    .line 32
    .line 33
    cmpl-float v2, v2, v3

    .line 34
    .line 35
    if-lez v2, :cond_0

    .line 36
    .line 37
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    aput-object v2, p0, v1

    .line 42
    .line 43
    :cond_0
    aget-object v2, p0, v1

    .line 44
    .line 45
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    mul-float v2, v2, v0

    .line 50
    .line 51
    aget-object p0, p0, v1

    .line 52
    .line 53
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 54
    .line 55
    .line 56
    move-result p0

    .line 57
    mul-float p0, p0, v0

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_1
    const p0, 0x3ebc6a7f    # 0.368f

    .line 61
    .line 62
    .line 63
    mul-float v2, v0, p0

    .line 64
    .line 65
    move p0, v2

    .line 66
    :goto_0
    float-to-int v1, v0

    .line 67
    div-int/lit8 v1, v1, 0x2

    .line 68
    .line 69
    float-to-int v2, v2

    .line 70
    float-to-int p0, p0

    .line 71
    const/16 v3, 0x20

    .line 72
    .line 73
    invoke-static {v1, v1, v2, p0, v3}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/StarPathBuilder;->getStarPath(IIIII)Landroid/graphics/Path;

    .line 74
    .line 75
    .line 76
    move-result-object p0

    .line 77
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 78
    .line 79
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 80
    .line 81
    .line 82
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 83
    .line 84
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 85
    .line 86
    .line 87
    move-result v2

    .line 88
    int-to-float v2, v2

    .line 89
    div-float/2addr v2, v0

    .line 90
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 91
    .line 92
    .line 93
    move-result v3

    .line 94
    int-to-float v3, v3

    .line 95
    div-float/2addr v3, v0

    .line 96
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 97
    .line 98
    .line 99
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 100
    .line 101
    invoke-virtual {p0, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    int-to-float v0, v0

    .line 109
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 110
    .line 111
    .line 112
    move-result p1

    .line 113
    int-to-float p1, p1

    .line 114
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->offset(FF)V

    .line 115
    .line 116
    .line 117
    return-object p0
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getStar4Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    int-to-float v0, v0

    .line 18
    if-eqz p0, :cond_1

    .line 19
    .line 20
    array-length v1, p0

    .line 21
    const/4 v2, 0x1

    .line 22
    if-ne v1, v2, :cond_1

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    aget-object v2, p0, v1

    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    const/high16 v3, 0x3f000000    # 0.5f

    .line 32
    .line 33
    cmpl-float v2, v2, v3

    .line 34
    .line 35
    if-lez v2, :cond_0

    .line 36
    .line 37
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    aput-object v2, p0, v1

    .line 42
    .line 43
    :cond_0
    aget-object v2, p0, v1

    .line 44
    .line 45
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    mul-float v2, v2, v0

    .line 50
    .line 51
    aget-object p0, p0, v1

    .line 52
    .line 53
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 54
    .line 55
    .line 56
    move-result p0

    .line 57
    mul-float p0, p0, v0

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_1
    const/high16 p0, 0x3e000000    # 0.125f

    .line 61
    .line 62
    mul-float v2, v0, p0

    .line 63
    .line 64
    move p0, v2

    .line 65
    :goto_0
    const/high16 v1, 0x40000000    # 2.0f

    .line 66
    .line 67
    div-float v1, v0, v1

    .line 68
    .line 69
    float-to-int v1, v1

    .line 70
    float-to-int v2, v2

    .line 71
    float-to-int p0, p0

    .line 72
    const/4 v3, 0x4

    .line 73
    invoke-static {v1, v1, v2, p0, v3}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/StarPathBuilder;->getStarPath(IIIII)Landroid/graphics/Path;

    .line 74
    .line 75
    .line 76
    move-result-object p0

    .line 77
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 78
    .line 79
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 80
    .line 81
    .line 82
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 83
    .line 84
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 85
    .line 86
    .line 87
    move-result v2

    .line 88
    int-to-float v2, v2

    .line 89
    div-float/2addr v2, v0

    .line 90
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 91
    .line 92
    .line 93
    move-result v3

    .line 94
    int-to-float v3, v3

    .line 95
    div-float/2addr v3, v0

    .line 96
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 97
    .line 98
    .line 99
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 100
    .line 101
    invoke-virtual {p0, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    int-to-float v0, v0

    .line 109
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 110
    .line 111
    .line 112
    move-result p1

    .line 113
    int-to-float p1, p1

    .line 114
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->offset(FF)V

    .line 115
    .line 116
    .line 117
    return-object p0
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getStar5Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    int-to-float v0, v0

    .line 18
    if-eqz p0, :cond_1

    .line 19
    .line 20
    array-length v1, p0

    .line 21
    const/4 v2, 0x3

    .line 22
    if-ne v1, v2, :cond_1

    .line 23
    .line 24
    const/4 v1, 0x1

    .line 25
    aget-object v1, p0, v1

    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    mul-float v1, v1, v0

    .line 32
    .line 33
    const/4 v2, 0x2

    .line 34
    aget-object v2, p0, v2

    .line 35
    .line 36
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    mul-float v2, v2, v0

    .line 41
    .line 42
    const/4 v3, 0x0

    .line 43
    aget-object v4, p0, v3

    .line 44
    .line 45
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 46
    .line 47
    .line 48
    move-result v4

    .line 49
    const/high16 v5, 0x3f000000    # 0.5f

    .line 50
    .line 51
    cmpl-float v4, v4, v5

    .line 52
    .line 53
    if-lez v4, :cond_0

    .line 54
    .line 55
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 56
    .line 57
    .line 58
    move-result-object v4

    .line 59
    aput-object v4, p0, v3

    .line 60
    .line 61
    :cond_0
    aget-object v4, p0, v3

    .line 62
    .line 63
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 64
    .line 65
    .line 66
    move-result v4

    .line 67
    mul-float v4, v4, v1

    .line 68
    .line 69
    aget-object p0, p0, v3

    .line 70
    .line 71
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 72
    .line 73
    .line 74
    move-result p0

    .line 75
    goto :goto_0

    .line 76
    :cond_1
    const p0, 0x3f86963e

    .line 77
    .line 78
    .line 79
    mul-float v1, v0, p0

    .line 80
    .line 81
    const p0, 0x3f8d8351

    .line 82
    .line 83
    .line 84
    mul-float v2, v0, p0

    .line 85
    .line 86
    const p0, 0x3e4ccccd    # 0.2f

    .line 87
    .line 88
    .line 89
    mul-float v4, v1, p0

    .line 90
    .line 91
    :goto_0
    mul-float p0, p0, v2

    .line 92
    .line 93
    const/high16 v3, 0x40000000    # 2.0f

    .line 94
    .line 95
    div-float/2addr v1, v3

    .line 96
    div-float v5, v2, v3

    .line 97
    .line 98
    float-to-int v1, v1

    .line 99
    float-to-int v5, v5

    .line 100
    float-to-int v4, v4

    .line 101
    float-to-int p0, p0

    .line 102
    const/4 v6, 0x5

    .line 103
    invoke-static {v1, v5, v4, p0, v6}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/StarPathBuilder;->getStarPath(IIIII)Landroid/graphics/Path;

    .line 104
    .line 105
    .line 106
    move-result-object p0

    .line 107
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 108
    .line 109
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 110
    .line 111
    .line 112
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 113
    .line 114
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 115
    .line 116
    .line 117
    move-result v4

    .line 118
    int-to-float v4, v4

    .line 119
    div-float/2addr v4, v0

    .line 120
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 121
    .line 122
    .line 123
    move-result v5

    .line 124
    int-to-float v5, v5

    .line 125
    div-float/2addr v5, v0

    .line 126
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 127
    .line 128
    .line 129
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 130
    .line 131
    invoke-virtual {p0, v1}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 132
    .line 133
    .line 134
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 135
    .line 136
    .line 137
    move-result v1

    .line 138
    int-to-float v1, v1

    .line 139
    mul-float v2, v2, v1

    .line 140
    .line 141
    div-float/2addr v2, v0

    .line 142
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 143
    .line 144
    .line 145
    move-result v0

    .line 146
    int-to-float v0, v0

    .line 147
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 148
    .line 149
    .line 150
    move-result v1

    .line 151
    int-to-float v1, v1

    .line 152
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 153
    .line 154
    .line 155
    move-result p1

    .line 156
    int-to-float p1, p1

    .line 157
    sub-float/2addr v2, p1

    .line 158
    div-float/2addr v2, v3

    .line 159
    add-float/2addr v1, v2

    .line 160
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->offset(FF)V

    .line 161
    .line 162
    .line 163
    return-object p0
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getStar6Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    int-to-float v0, v0

    .line 18
    if-eqz p0, :cond_1

    .line 19
    .line 20
    array-length v1, p0

    .line 21
    const/4 v2, 0x2

    .line 22
    if-ne v1, v2, :cond_1

    .line 23
    .line 24
    const/4 v1, 0x1

    .line 25
    aget-object v1, p0, v1

    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    mul-float v1, v1, v0

    .line 32
    .line 33
    const/4 v2, 0x0

    .line 34
    aget-object v3, p0, v2

    .line 35
    .line 36
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    const/high16 v4, 0x3f000000    # 0.5f

    .line 41
    .line 42
    cmpl-float v3, v3, v4

    .line 43
    .line 44
    if-lez v3, :cond_0

    .line 45
    .line 46
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    aput-object v3, p0, v2

    .line 51
    .line 52
    :cond_0
    aget-object v3, p0, v2

    .line 53
    .line 54
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 55
    .line 56
    .line 57
    move-result v3

    .line 58
    mul-float v3, v3, v1

    .line 59
    .line 60
    aget-object p0, p0, v2

    .line 61
    .line 62
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 63
    .line 64
    .line 65
    move-result p0

    .line 66
    goto :goto_0

    .line 67
    :cond_1
    const p0, 0x3f93cd36    # 1.1547f

    .line 68
    .line 69
    .line 70
    mul-float v1, v0, p0

    .line 71
    .line 72
    const p0, 0x3e8f5c29    # 0.28f

    .line 73
    .line 74
    .line 75
    mul-float v3, v1, p0

    .line 76
    .line 77
    :goto_0
    mul-float p0, p0, v0

    .line 78
    .line 79
    const/high16 v2, 0x40000000    # 2.0f

    .line 80
    .line 81
    div-float/2addr v1, v2

    .line 82
    div-float v2, v0, v2

    .line 83
    .line 84
    float-to-int v1, v1

    .line 85
    float-to-int v2, v2

    .line 86
    float-to-int v3, v3

    .line 87
    float-to-int p0, p0

    .line 88
    const/4 v4, 0x6

    .line 89
    invoke-static {v1, v2, v3, p0, v4}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/StarPathBuilder;->getStarPath(IIIII)Landroid/graphics/Path;

    .line 90
    .line 91
    .line 92
    move-result-object p0

    .line 93
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 94
    .line 95
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 96
    .line 97
    .line 98
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 99
    .line 100
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 101
    .line 102
    .line 103
    move-result v2

    .line 104
    int-to-float v2, v2

    .line 105
    div-float/2addr v2, v0

    .line 106
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 107
    .line 108
    .line 109
    move-result v3

    .line 110
    int-to-float v3, v3

    .line 111
    div-float/2addr v3, v0

    .line 112
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 113
    .line 114
    .line 115
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 116
    .line 117
    invoke-virtual {p0, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 118
    .line 119
    .line 120
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 121
    .line 122
    .line 123
    move-result v0

    .line 124
    int-to-float v0, v0

    .line 125
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 126
    .line 127
    .line 128
    move-result p1

    .line 129
    int-to-float p1, p1

    .line 130
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->offset(FF)V

    .line 131
    .line 132
    .line 133
    return-object p0
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getStar7Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    int-to-float v0, v0

    .line 18
    if-eqz p0, :cond_1

    .line 19
    .line 20
    array-length v1, p0

    .line 21
    const/4 v2, 0x3

    .line 22
    if-ne v1, v2, :cond_1

    .line 23
    .line 24
    const/4 v1, 0x1

    .line 25
    aget-object v1, p0, v1

    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    mul-float v1, v1, v0

    .line 32
    .line 33
    const/4 v2, 0x2

    .line 34
    aget-object v2, p0, v2

    .line 35
    .line 36
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    mul-float v2, v2, v0

    .line 41
    .line 42
    const/4 v3, 0x0

    .line 43
    aget-object v4, p0, v3

    .line 44
    .line 45
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 46
    .line 47
    .line 48
    move-result v4

    .line 49
    const/high16 v5, 0x3f000000    # 0.5f

    .line 50
    .line 51
    cmpl-float v4, v4, v5

    .line 52
    .line 53
    if-lez v4, :cond_0

    .line 54
    .line 55
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 56
    .line 57
    .line 58
    move-result-object v4

    .line 59
    aput-object v4, p0, v3

    .line 60
    .line 61
    :cond_0
    aget-object v4, p0, v3

    .line 62
    .line 63
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    .line 64
    .line 65
    .line 66
    move-result v4

    .line 67
    mul-float v4, v4, v1

    .line 68
    .line 69
    aget-object p0, p0, v3

    .line 70
    .line 71
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 72
    .line 73
    .line 74
    move-result p0

    .line 75
    goto :goto_0

    .line 76
    :cond_1
    const p0, 0x3f834acb    # 1.02572f

    .line 77
    .line 78
    .line 79
    mul-float v1, v0, p0

    .line 80
    .line 81
    const p0, 0x3f86ab36    # 1.0521f

    .line 82
    .line 83
    .line 84
    mul-float v2, v0, p0

    .line 85
    .line 86
    const p0, 0x3ea3d70a    # 0.32f

    .line 87
    .line 88
    .line 89
    mul-float v4, v1, p0

    .line 90
    .line 91
    :goto_0
    mul-float p0, p0, v2

    .line 92
    .line 93
    const/high16 v3, 0x40000000    # 2.0f

    .line 94
    .line 95
    div-float/2addr v1, v3

    .line 96
    div-float/2addr v2, v3

    .line 97
    float-to-int v1, v1

    .line 98
    float-to-int v2, v2

    .line 99
    float-to-int v3, v4

    .line 100
    float-to-int p0, p0

    .line 101
    const/4 v4, 0x7

    .line 102
    invoke-static {v1, v2, v3, p0, v4}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/StarPathBuilder;->getStarPath(IIIII)Landroid/graphics/Path;

    .line 103
    .line 104
    .line 105
    move-result-object p0

    .line 106
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 107
    .line 108
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 109
    .line 110
    .line 111
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 112
    .line 113
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 114
    .line 115
    .line 116
    move-result v2

    .line 117
    int-to-float v2, v2

    .line 118
    div-float/2addr v2, v0

    .line 119
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 120
    .line 121
    .line 122
    move-result v3

    .line 123
    int-to-float v3, v3

    .line 124
    div-float/2addr v3, v0

    .line 125
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 126
    .line 127
    .line 128
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 129
    .line 130
    invoke-virtual {p0, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 131
    .line 132
    .line 133
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 134
    .line 135
    .line 136
    move-result v0

    .line 137
    int-to-float v0, v0

    .line 138
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 139
    .line 140
    .line 141
    move-result p1

    .line 142
    int-to-float p1, p1

    .line 143
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->offset(FF)V

    .line 144
    .line 145
    .line 146
    return-object p0
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static getStar8Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getAdjustData()[Ljava/lang/Float;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    int-to-float v0, v0

    .line 18
    if-eqz p0, :cond_1

    .line 19
    .line 20
    array-length v1, p0

    .line 21
    const/4 v2, 0x1

    .line 22
    if-ne v1, v2, :cond_1

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    aget-object v2, p0, v1

    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    const/high16 v3, 0x3f000000    # 0.5f

    .line 32
    .line 33
    cmpl-float v2, v2, v3

    .line 34
    .line 35
    if-lez v2, :cond_0

    .line 36
    .line 37
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    aput-object v2, p0, v1

    .line 42
    .line 43
    :cond_0
    aget-object v2, p0, v1

    .line 44
    .line 45
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    mul-float v2, v2, v0

    .line 50
    .line 51
    aget-object p0, p0, v1

    .line 52
    .line 53
    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    .line 54
    .line 55
    .line 56
    move-result p0

    .line 57
    mul-float p0, p0, v0

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_1
    const p0, 0x3eb851ec    # 0.36f

    .line 61
    .line 62
    .line 63
    mul-float v2, v0, p0

    .line 64
    .line 65
    move p0, v2

    .line 66
    :goto_0
    const/high16 v1, 0x40000000    # 2.0f

    .line 67
    .line 68
    div-float v1, v0, v1

    .line 69
    .line 70
    float-to-int v1, v1

    .line 71
    float-to-int v2, v2

    .line 72
    float-to-int p0, p0

    .line 73
    const/16 v3, 0x8

    .line 74
    .line 75
    invoke-static {v1, v1, v2, p0, v3}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/StarPathBuilder;->getStarPath(IIIII)Landroid/graphics/Path;

    .line 76
    .line 77
    .line 78
    move-result-object p0

    .line 79
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 80
    .line 81
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 82
    .line 83
    .line 84
    sget-object v1, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 85
    .line 86
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 87
    .line 88
    .line 89
    move-result v2

    .line 90
    int-to-float v2, v2

    .line 91
    div-float/2addr v2, v0

    .line 92
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 93
    .line 94
    .line 95
    move-result v3

    .line 96
    int-to-float v3, v3

    .line 97
    div-float/2addr v3, v0

    .line 98
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 99
    .line 100
    .line 101
    sget-object v0, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->sm:Landroid/graphics/Matrix;

    .line 102
    .line 103
    invoke-virtual {p0, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    .line 107
    .line 108
    .line 109
    move-result v0

    .line 110
    int-to-float v0, v0

    .line 111
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    .line 112
    .line 113
    .line 114
    move-result p1

    .line 115
    int-to-float p1, p1

    .line 116
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Path;->offset(FF)V

    .line 117
    .line 118
    .line 119
    return-object p0
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static getStarPath(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/16 v1, 0xc

    .line 6
    .line 7
    if-eq v0, v1, :cond_2

    .line 8
    .line 9
    const/16 v1, 0x5c

    .line 10
    .line 11
    if-eq v0, v1, :cond_1

    .line 12
    .line 13
    const/16 v1, 0xbb

    .line 14
    .line 15
    if-eq v0, v1, :cond_0

    .line 16
    .line 17
    packed-switch v0, :pswitch_data_0

    .line 18
    .line 19
    .line 20
    packed-switch v0, :pswitch_data_1

    .line 21
    .line 22
    .line 23
    const/4 p0, 0x0

    .line 24
    return-object p0

    .line 25
    :pswitch_0
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->getStar12Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 26
    .line 27
    .line 28
    move-result-object p0

    .line 29
    return-object p0

    .line 30
    :pswitch_1
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->getStar10Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 31
    .line 32
    .line 33
    move-result-object p0

    .line 34
    return-object p0

    .line 35
    :pswitch_2
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->getStar7Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 36
    .line 37
    .line 38
    move-result-object p0

    .line 39
    return-object p0

    .line 40
    :pswitch_3
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->getStar6Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 41
    .line 42
    .line 43
    move-result-object p0

    .line 44
    return-object p0

    .line 45
    :pswitch_4
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->getStar32Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 46
    .line 47
    .line 48
    move-result-object p0

    .line 49
    return-object p0

    .line 50
    :pswitch_5
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->getStar16Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 51
    .line 52
    .line 53
    move-result-object p0

    .line 54
    return-object p0

    .line 55
    :pswitch_6
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->getStar8Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 56
    .line 57
    .line 58
    move-result-object p0

    .line 59
    return-object p0

    .line 60
    :cond_0
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->getStar4Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 61
    .line 62
    .line 63
    move-result-object p0

    .line 64
    return-object p0

    .line 65
    :cond_1
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->getStar24Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 66
    .line 67
    .line 68
    move-result-object p0

    .line 69
    return-object p0

    .line 70
    :cond_2
    :pswitch_7
    invoke-static {p0, p1}, Lcom/intsig/office/common/autoshape/pathbuilder/starAndBanner/star/LaterStarPathBuilder;->getStar5Path(Lcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;)Landroid/graphics/Path;

    .line 71
    .line 72
    .line 73
    move-result-object p0

    .line 74
    return-object p0

    .line 75
    :pswitch_data_0
    .packed-switch 0x3a
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    :pswitch_data_1
    .packed-switch 0xeb
        :pswitch_7
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

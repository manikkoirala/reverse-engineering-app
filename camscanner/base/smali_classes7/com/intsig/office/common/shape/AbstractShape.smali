.class public Lcom/intsig/office/common/shape/AbstractShape;
.super Ljava/lang/Object;
.source "AbstractShape.java"

# interfaces
.implements Lcom/intsig/office/common/shape/IShape;


# static fields
.field public static final SHAPE_AUTOGRAPH:S = 0x2s

.field public static final SHAPE_BG_FILL:S = 0x3s

.field public static final SHAPE_CHART:S = 0x5s

.field public static final SHAPE_GROUP:S = 0x7s

.field public static final SHAPE_LINE:S = 0x4s

.field public static final SHAPE_PICTURE:S = 0x0s

.field public static final SHAPE_SMART_ART:S = 0x8s

.field public static final SHAPE_TABLE:S = 0x6s

.field public static final SHAPE_TEXT:S = 0x1s


# instance fields
.field private angle:F

.field private animation:Lcom/intsig/office/pg/animate/IAnimation;

.field private bgFill:Lcom/intsig/office/common/bg/BackgroundAndFill;

.field private flipH:Z

.field private flipV:Z

.field private grpSpID:I

.field private hasLine:Z

.field private hidden:Z

.field private id:I

.field private line:Lcom/intsig/office/common/borders/Line;

.field private parent:Lcom/intsig/office/common/shape/IShape;

.field private placeHolderID:I

.field protected rect:Lcom/intsig/office/java/awt/Rectangle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, -0x1

    .line 5
    iput v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->grpSpID:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public createLine()Lcom/intsig/office/common/borders/Line;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/common/borders/Line;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/borders/Line;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->line:Lcom/intsig/office/common/borders/Line;

    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public dispose()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->parent:Lcom/intsig/office/common/shape/IShape;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-interface {v0}, Lcom/intsig/office/common/shape/IShape;->dispose()V

    .line 7
    .line 8
    .line 9
    iput-object v1, p0, Lcom/intsig/office/common/shape/AbstractShape;->parent:Lcom/intsig/office/common/shape/IShape;

    .line 10
    .line 11
    :cond_0
    iput-object v1, p0, Lcom/intsig/office/common/shape/AbstractShape;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->animation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 14
    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    invoke-interface {v0}, Lcom/intsig/office/pg/animate/IAnimation;->dispose()V

    .line 18
    .line 19
    .line 20
    iput-object v1, p0, Lcom/intsig/office/common/shape/AbstractShape;->animation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 21
    .line 22
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->bgFill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 23
    .line 24
    if-eqz v0, :cond_2

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->dispose()V

    .line 27
    .line 28
    .line 29
    iput-object v1, p0, Lcom/intsig/office/common/shape/AbstractShape;->bgFill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 30
    .line 31
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->line:Lcom/intsig/office/common/borders/Line;

    .line 32
    .line 33
    if-eqz v0, :cond_3

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/office/common/borders/Line;->dispose()V

    .line 36
    .line 37
    .line 38
    iput-object v1, p0, Lcom/intsig/office/common/shape/AbstractShape;->line:Lcom/intsig/office/common/borders/Line;

    .line 39
    .line 40
    :cond_3
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getAnimation()Lcom/intsig/office/pg/animate/IAnimation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->animation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->bgFill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBounds()Lcom/intsig/office/java/awt/Rectangle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getData()Ljava/lang/Object;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFlipHorizontal()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->flipH:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFlipVertical()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->flipV:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getGroupShapeID()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->grpSpID:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLine()Lcom/intsig/office/common/borders/Line;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->line:Lcom/intsig/office/common/borders/Line;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getParent()Lcom/intsig/office/common/shape/IShape;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->parent:Lcom/intsig/office/common/shape/IShape;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPlaceHolderID()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->placeHolderID:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRotation()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->angle:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShapeID()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->id:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getType()S
    .locals 1

    .line 1
    const/4 v0, -0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public hasLine()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->line:Lcom/intsig/office/common/borders/Line;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isHidden()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->hidden:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setAnimation(Lcom/intsig/office/pg/animate/IAnimation;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->animation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->bgFill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBounds(Lcom/intsig/office/java/awt/Rectangle;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setData(Ljava/lang/Object;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFlipHorizontal(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->flipH:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFlipVertical(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->flipV:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setGroupShapeID(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->grpSpID:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setHidden(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->hidden:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLine(Lcom/intsig/office/common/borders/Line;)V
    .locals 0

    .line 4
    iput-object p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->line:Lcom/intsig/office/common/borders/Line;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 5
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->hasLine:Z

    :cond_0
    return-void
.end method

.method public setLine(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->hasLine:Z

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->line:Lcom/intsig/office/common/borders/Line;

    if-nez p1, :cond_0

    .line 3
    new-instance p1, Lcom/intsig/office/common/borders/Line;

    invoke-direct {p1}, Lcom/intsig/office/common/borders/Line;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->line:Lcom/intsig/office/common/borders/Line;

    :cond_0
    return-void
.end method

.method public setParent(Lcom/intsig/office/common/shape/IShape;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->parent:Lcom/intsig/office/common/shape/IShape;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPlaceHolderID(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->placeHolderID:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRotation(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->angle:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setShapeID(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->id:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public Lcom/intsig/office/common/shape/TextBox;
.super Lcom/intsig/office/common/shape/AbstractShape;
.source "TextBox.java"


# static fields
.field public static final MC_DateTime:B = 0x2t

.field public static final MC_Footer:B = 0x4t

.field public static final MC_GenericDate:B = 0x3t

.field public static final MC_None:B = 0x0t

.field public static final MC_RTFDateTime:B = 0x5t

.field public static final MC_SlideNumber:B = 0x1t


# instance fields
.field private elem:Lcom/intsig/office/simpletext/model/SectionElement;

.field private isEditor:Z

.field private isWordArt:Z

.field private isWrapLine:Z

.field private mcType:B

.field private rootView:Lcom/intsig/office/simpletext/view/STRoot;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/common/shape/AbstractShape;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-byte v0, p0, Lcom/intsig/office/common/shape/TextBox;->mcType:B

    .line 6
    .line 7
    const/4 v0, -0x1

    .line 8
    invoke-virtual {p0, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setPlaceHolderID(I)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/common/shape/AbstractShape;->dispose()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/common/shape/TextBox;->elem:Lcom/intsig/office/simpletext/model/SectionElement;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/SectionElement;->dispose()V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-object v0, p0, Lcom/intsig/office/common/shape/TextBox;->elem:Lcom/intsig/office/simpletext/model/SectionElement;

    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getData()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/TextBox;->elem:Lcom/intsig/office/simpletext/model/SectionElement;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getElement()Lcom/intsig/office/simpletext/model/SectionElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/TextBox;->elem:Lcom/intsig/office/simpletext/model/SectionElement;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMCType()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/shape/TextBox;->mcType:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRootView()Lcom/intsig/office/simpletext/view/STRoot;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/TextBox;->rootView:Lcom/intsig/office/simpletext/view/STRoot;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getType()S
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isEditor()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/shape/TextBox;->isEditor:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isWordArt()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/shape/TextBox;->isWordArt:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isWrapLine()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/shape/TextBox;->isWrapLine:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setData(Ljava/lang/Object;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/office/common/shape/TextBox;->elem:Lcom/intsig/office/simpletext/model/SectionElement;

    .line 8
    .line 9
    :cond_0
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setEditor(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/TextBox;->isEditor:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setElement(Lcom/intsig/office/simpletext/model/SectionElement;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/shape/TextBox;->elem:Lcom/intsig/office/simpletext/model/SectionElement;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMCType(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/common/shape/TextBox;->mcType:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRootView(Lcom/intsig/office/simpletext/view/STRoot;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/shape/TextBox;->rootView:Lcom/intsig/office/simpletext/view/STRoot;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setWordArt(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/TextBox;->isWordArt:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setWrapLine(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/TextBox;->isWrapLine:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public Lcom/intsig/office/common/shape/WPAutoShape;
.super Lcom/intsig/office/common/shape/WPAbstractShape;
.source "WPAutoShape.java"


# instance fields
.field private groupShape:Lcom/intsig/office/common/shape/WPGroupShape;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/common/shape/WPAbstractShape;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public addGroupShape(Lcom/intsig/office/common/shape/WPGroupShape;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/shape/WPAutoShape;->groupShape:Lcom/intsig/office/common/shape/WPGroupShape;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public dispose()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/common/shape/LineShape;->dispose()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/common/shape/WPAutoShape;->groupShape:Lcom/intsig/office/common/shape/WPGroupShape;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/AbstractShape;->dispose()V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-object v0, p0, Lcom/intsig/office/common/shape/WPAutoShape;->groupShape:Lcom/intsig/office/common/shape/WPGroupShape;

    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBounds()Lcom/intsig/office/java/awt/Rectangle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/WPAutoShape;->groupShape:Lcom/intsig/office/common/shape/WPGroupShape;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/AbstractShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0

    .line 10
    :cond_0
    invoke-super {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getGroupShape()Lcom/intsig/office/common/shape/WPGroupShape;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/WPAutoShape;->groupShape:Lcom/intsig/office/common/shape/WPGroupShape;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getType()S
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isWatermarkShape()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

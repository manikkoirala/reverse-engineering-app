.class public Lcom/intsig/office/common/shape/AChart;
.super Lcom/intsig/office/common/shape/AbstractShape;
.source "AChart.java"


# instance fields
.field private chart:Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

.field private picIndex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/common/shape/AbstractShape;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, -0x1

    .line 5
    iput v0, p0, Lcom/intsig/office/common/shape/AChart;->picIndex:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private saveChartToPicture(Lcom/intsig/office/system/IControl;)V
    .locals 10

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/intsig/office/common/shape/AbstractShape;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 3
    .line 4
    iget v1, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 5
    .line 6
    int-to-float v1, v1

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AChart;->getAChart()Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getZoomRate()F

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    mul-float v1, v1, v2

    .line 16
    .line 17
    float-to-int v7, v1

    .line 18
    iget-object v1, p0, Lcom/intsig/office/common/shape/AbstractShape;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 19
    .line 20
    iget v1, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 21
    .line 22
    int-to-float v1, v1

    .line 23
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AChart;->getAChart()Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getZoomRate()F

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    mul-float v1, v1, v2

    .line 32
    .line 33
    float-to-int v8, v1

    .line 34
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 35
    .line 36
    invoke-static {v7, v8, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    new-instance v1, Landroid/graphics/Canvas;

    .line 41
    .line 42
    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 43
    .line 44
    .line 45
    iget-object v2, p0, Lcom/intsig/office/common/shape/AChart;->chart:Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 46
    .line 47
    const/4 v5, 0x0

    .line 48
    const/4 v6, 0x0

    .line 49
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    .line 50
    .line 51
    .line 52
    move-result-object v3

    .line 53
    invoke-virtual {v3}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    .line 54
    .line 55
    .line 56
    move-result-object v9

    .line 57
    move-object v3, v1

    .line 58
    move-object v4, p1

    .line 59
    invoke-virtual/range {v2 .. v9}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->draw(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;IIIILandroid/graphics/Paint;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v1}, Landroid/graphics/Canvas;->save()I

    .line 63
    .line 64
    .line 65
    invoke-virtual {v1}, Landroid/graphics/Canvas;->restore()V

    .line 66
    .line 67
    .line 68
    new-instance v1, Lcom/intsig/office/common/picture/Picture;

    .line 69
    .line 70
    invoke-direct {v1}, Lcom/intsig/office/common/picture/Picture;-><init>()V

    .line 71
    .line 72
    .line 73
    new-instance v2, Ljava/lang/StringBuilder;

    .line 74
    .line 75
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    .line 77
    .line 78
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 79
    .line 80
    .line 81
    move-result-wide v3

    .line 82
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v3

    .line 86
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    const-string v3, ".tmp"

    .line 90
    .line 91
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v2

    .line 98
    new-instance v3, Ljava/io/File;

    .line 99
    .line 100
    new-instance v4, Ljava/lang/StringBuilder;

    .line 101
    .line 102
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    .line 104
    .line 105
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 106
    .line 107
    .line 108
    move-result-object v5

    .line 109
    invoke-virtual {v5}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 110
    .line 111
    .line 112
    move-result-object v5

    .line 113
    invoke-virtual {v5}, Lcom/intsig/office/common/picture/PictureManage;->getPicTempPath()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object v5

    .line 117
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    .line 121
    .line 122
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    .line 127
    .line 128
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v2

    .line 132
    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 133
    .line 134
    .line 135
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 136
    .line 137
    .line 138
    new-instance v2, Ljava/io/FileOutputStream;

    .line 139
    .line 140
    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 141
    .line 142
    .line 143
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    .line 144
    .line 145
    const/16 v5, 0x64

    .line 146
    .line 147
    invoke-virtual {v0, v4, v5, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 148
    .line 149
    .line 150
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 151
    .line 152
    .line 153
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 154
    .line 155
    .line 156
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object v2

    .line 160
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/picture/Picture;->setTempFilePath(Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 164
    .line 165
    .line 166
    move-result-object v2

    .line 167
    invoke-virtual {v2}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 168
    .line 169
    .line 170
    move-result-object v2

    .line 171
    invoke-virtual {v2, v1}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/common/picture/Picture;)I

    .line 172
    .line 173
    .line 174
    move-result v1

    .line 175
    iput v1, p0, Lcom/intsig/office/common/shape/AChart;->picIndex:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    .line 177
    goto :goto_0

    .line 178
    :catch_0
    move-exception v1

    .line 179
    if-eqz v0, :cond_0

    .line 180
    .line 181
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 182
    .line 183
    .line 184
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 185
    .line 186
    .line 187
    move-result-object p1

    .line 188
    invoke-virtual {p1}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    .line 189
    .line 190
    .line 191
    move-result-object p1

    .line 192
    invoke-virtual {p1, v1}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;)V

    .line 193
    .line 194
    .line 195
    :goto_0
    return-void
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/common/shape/AbstractShape;->dispose()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/common/shape/AChart;->chart:Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAChart()Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/AChart;->chart:Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDrawingPicture(Lcom/intsig/office/system/IControl;)I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/AChart;->picIndex:I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/intsig/office/common/shape/AChart;->saveChartToPicture(Lcom/intsig/office/system/IControl;)V

    .line 7
    .line 8
    .line 9
    :cond_0
    iget p1, p0, Lcom/intsig/office/common/shape/AChart;->picIndex:I

    .line 10
    .line 11
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getType()S
    .locals 1

    .line 1
    const/4 v0, 0x5

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setAChart(Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/shape/AChart;->chart:Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

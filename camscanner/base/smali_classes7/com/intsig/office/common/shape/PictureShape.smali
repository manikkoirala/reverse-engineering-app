.class public Lcom/intsig/office/common/shape/PictureShape;
.super Lcom/intsig/office/common/shape/AbstractShape;
.source "PictureShape.java"


# instance fields
.field private effectInfor:Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

.field private pictureIndex:I

.field private zoomX:S

.field private zoomY:S


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/common/shape/AbstractShape;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static getPicture(Lcom/intsig/office/system/IControl;I)Lcom/intsig/office/common/picture/Picture;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 2
    :cond_0
    invoke-interface {p0}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    move-result-object p0

    invoke-virtual {p0}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/intsig/office/common/picture/PictureManage;->getPicture(I)Lcom/intsig/office/common/picture/Picture;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public dispose()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/intsig/office/common/shape/AbstractShape;->dispose()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPicture(Lcom/intsig/office/system/IControl;)Lcom/intsig/office/common/picture/Picture;
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 1
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    move-result-object p1

    invoke-virtual {p1}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    move-result-object p1

    iget v0, p0, Lcom/intsig/office/common/shape/PictureShape;->pictureIndex:I

    invoke-virtual {p1, v0}, Lcom/intsig/office/common/picture/PictureManage;->getPicture(I)Lcom/intsig/office/common/picture/Picture;

    move-result-object p1

    return-object p1
.end method

.method public getPictureEffectInfor()Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/PictureShape;->effectInfor:Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPictureIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/PictureShape;->pictureIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getType()S
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setPictureEffectInfor(Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/shape/PictureShape;->effectInfor:Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPictureIndex(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/shape/PictureShape;->pictureIndex:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setZoomX(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/common/shape/PictureShape;->zoomX:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setZoomY(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/common/shape/PictureShape;->zoomY:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public Lcom/intsig/office/common/shape/WPPictureShape;
.super Lcom/intsig/office/common/shape/WPAutoShape;
.source "WPPictureShape.java"


# instance fields
.field private pictureShape:Lcom/intsig/office/common/shape/PictureShape;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/common/shape/WPAutoShape;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/WPPictureShape;->pictureShape:Lcom/intsig/office/common/shape/PictureShape;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/PictureShape;->dispose()V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/common/shape/WPPictureShape;->pictureShape:Lcom/intsig/office/common/shape/PictureShape;

    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPictureShape()Lcom/intsig/office/common/shape/PictureShape;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/WPPictureShape;->pictureShape:Lcom/intsig/office/common/shape/PictureShape;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getType()S
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isWatermarkShape()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setPictureShape(Lcom/intsig/office/common/shape/PictureShape;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/shape/WPPictureShape;->pictureShape:Lcom/intsig/office/common/shape/PictureShape;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/common/shape/AbstractShape;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AbstractShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    iput-object p1, p0, Lcom/intsig/office/common/shape/AbstractShape;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

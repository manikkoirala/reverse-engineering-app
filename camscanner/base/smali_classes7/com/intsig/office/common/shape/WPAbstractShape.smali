.class public Lcom/intsig/office/common/shape/WPAbstractShape;
.super Lcom/intsig/office/common/shape/ArbitraryPolygonShape;
.source "WPAbstractShape.java"


# static fields
.field public static final ALIGNMENT_ABSOLUTE:B = 0x0t

.field public static final ALIGNMENT_BOTTOM:B = 0x5t

.field public static final ALIGNMENT_CENTER:B = 0x2t

.field public static final ALIGNMENT_INSIDE:B = 0x6t

.field public static final ALIGNMENT_LEFT:B = 0x1t

.field public static final ALIGNMENT_OUTSIDE:B = 0x7t

.field public static final ALIGNMENT_RIGHT:B = 0x3t

.field public static final ALIGNMENT_TOP:B = 0x4t

.field public static final POSITIONTYPE_ABSOLUTE:B = 0x0t

.field public static final POSITIONTYPE_RELATIVE:B = 0x1t

.field public static final RELATIVE_BOTTOM:B = 0x7t

.field public static final RELATIVE_CHARACTER:B = 0x3t

.field public static final RELATIVE_COLUMN:B = 0x0t

.field public static final RELATIVE_INNER:B = 0x8t

.field public static final RELATIVE_LEFT:B = 0x4t

.field public static final RELATIVE_LINE:B = 0xbt

.field public static final RELATIVE_MARGIN:B = 0x1t

.field public static final RELATIVE_OUTER:B = 0x9t

.field public static final RELATIVE_PAGE:B = 0x2t

.field public static final RELATIVE_PARAGRAPH:B = 0xat

.field public static final RELATIVE_RIGHT:B = 0x5t

.field public static final RELATIVE_TOP:B = 0x6t

.field public static final WRAP_BOTTOM:S = 0x6s

.field public static final WRAP_OLE:S = 0x2s

.field public static final WRAP_SQUARE:S = 0x1s

.field public static final WRAP_THROUGH:S = 0x4s

.field public static final WRAP_TIGHT:S = 0x0s

.field public static final WRAP_TOP:S = 0x3s

.field public static final WRAP_TOPANDBOTTOM:S = 0x5s


# instance fields
.field private elementIndex:I

.field private horAlignment:B

.field private horPositionType:B

.field private horRelativeTo:B

.field private horRelativeValue:I

.field private isTextWrapLine:Z

.field private verAlignment:B

.field private verPositionType:B

.field private verRelativeTo:B

.field private verRelativeValue:I

.field private wrapType:S


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-byte v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->horAlignment:B

    .line 6
    .line 7
    const/16 v1, 0xa

    .line 8
    .line 9
    iput-byte v1, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->verRelativeTo:B

    .line 10
    .line 11
    iput-byte v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->verAlignment:B

    .line 12
    .line 13
    const/4 v0, 0x3

    .line 14
    iput-short v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->wrapType:S

    .line 15
    .line 16
    const/4 v0, -0x1

    .line 17
    iput v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->elementIndex:I

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    iput-boolean v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->isTextWrapLine:Z

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method


# virtual methods
.method public getElementIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->elementIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHorPositionType()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->horPositionType:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHorRelativeValue()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->horRelativeValue:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHorizontalAlignment()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->horAlignment:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHorizontalRelativeTo()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->horRelativeTo:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getVerPositionType()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->verPositionType:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getVerRelativeValue()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->verRelativeValue:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getVerticalAlignment()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->verAlignment:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getVerticalRelativeTo()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->verRelativeTo:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getWrap()I
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->wrapType:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isTextWrapLine()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->isTextWrapLine:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setElementIndex(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->elementIndex:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setHorPositionType(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->horPositionType:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setHorRelativeValue(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->horRelativeValue:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setHorizontalAlignment(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->horAlignment:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setHorizontalRelativeTo(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->horRelativeTo:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTextWrapLine(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->isTextWrapLine:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setVerPositionType(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->verPositionType:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setVerRelativeValue(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->verRelativeValue:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setVerticalAlignment(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->verAlignment:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setVerticalRelativeTo(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->verRelativeTo:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setWrap(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/common/shape/WPAbstractShape;->wrapType:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public Lcom/intsig/office/common/shape/TableShape;
.super Lcom/intsig/office/common/shape/AbstractShape;
.source "TableShape.java"


# instance fields
.field private bandCol:Z

.field private bandRow:Z

.field private cells:[Lcom/intsig/office/common/shape/TableCell;

.field private colCnt:I

.field private firstCol:Z

.field private firstRow:Z

.field private lastCol:Z

.field private lastRow:Z

.field private rowCnt:I

.field private shape07:Z


# direct methods
.method public constructor <init>(II)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/common/shape/AbstractShape;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/office/common/shape/TableShape;->shape07:Z

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    iput-boolean v1, p0, Lcom/intsig/office/common/shape/TableShape;->firstRow:Z

    .line 9
    .line 10
    iput-boolean v1, p0, Lcom/intsig/office/common/shape/TableShape;->lastRow:Z

    .line 11
    .line 12
    iput-boolean v1, p0, Lcom/intsig/office/common/shape/TableShape;->firstCol:Z

    .line 13
    .line 14
    iput-boolean v1, p0, Lcom/intsig/office/common/shape/TableShape;->lastCol:Z

    .line 15
    .line 16
    iput-boolean v1, p0, Lcom/intsig/office/common/shape/TableShape;->bandRow:Z

    .line 17
    .line 18
    iput-boolean v1, p0, Lcom/intsig/office/common/shape/TableShape;->bandCol:Z

    .line 19
    .line 20
    if-lt p1, v0, :cond_1

    .line 21
    .line 22
    if-lt p2, v0, :cond_0

    .line 23
    .line 24
    iput p1, p0, Lcom/intsig/office/common/shape/TableShape;->rowCnt:I

    .line 25
    .line 26
    iput p2, p0, Lcom/intsig/office/common/shape/TableShape;->colCnt:I

    .line 27
    .line 28
    mul-int p1, p1, p2

    .line 29
    .line 30
    new-array p1, p1, [Lcom/intsig/office/common/shape/TableCell;

    .line 31
    .line 32
    iput-object p1, p0, Lcom/intsig/office/common/shape/TableShape;->cells:[Lcom/intsig/office/common/shape/TableCell;

    .line 33
    .line 34
    return-void

    .line 35
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 36
    .line 37
    const-string p2, "The number of columns must be greater than 1"

    .line 38
    .line 39
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    throw p1

    .line 43
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 44
    .line 45
    const-string p2, "The number of rows must be greater than 1"

    .line 46
    .line 47
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    throw p1
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public addCell(ILcom/intsig/office/common/shape/TableCell;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/TableShape;->cells:[Lcom/intsig/office/common/shape/TableCell;

    .line 2
    .line 3
    aput-object p2, v0, p1

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public dispose()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/TableShape;->cells:[Lcom/intsig/office/common/shape/TableCell;

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/common/shape/TableShape;->cells:[Lcom/intsig/office/common/shape/TableCell;

    .line 7
    .line 8
    array-length v2, v1

    .line 9
    if-ge v0, v2, :cond_1

    .line 10
    .line 11
    aget-object v1, v1, v0

    .line 12
    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/office/common/shape/TableCell;->dispose()V

    .line 16
    .line 17
    .line 18
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_1
    const/4 v0, 0x0

    .line 22
    iput-object v0, p0, Lcom/intsig/office/common/shape/TableShape;->cells:[Lcom/intsig/office/common/shape/TableCell;

    .line 23
    .line 24
    :cond_2
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getCell(I)Lcom/intsig/office/common/shape/TableCell;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/TableShape;->cells:[Lcom/intsig/office/common/shape/TableCell;

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    if-lt p1, v1, :cond_0

    .line 5
    .line 6
    const/4 p1, 0x0

    .line 7
    return-object p1

    .line 8
    :cond_0
    aget-object p1, v0, p1

    .line 9
    .line 10
    return-object p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getCellCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/TableShape;->cells:[Lcom/intsig/office/common/shape/TableCell;

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getColumnCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/TableShape;->colCnt:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRowCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/TableShape;->rowCnt:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getType()S
    .locals 1

    .line 1
    const/4 v0, 0x6

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isBandCol()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/shape/TableShape;->bandCol:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isBandRow()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/shape/TableShape;->bandRow:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isFirstCol()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/shape/TableShape;->firstCol:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isFirstRow()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/shape/TableShape;->firstRow:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isLastCol()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/shape/TableShape;->lastCol:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isLastRow()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/shape/TableShape;->lastRow:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isTable07()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/shape/TableShape;->shape07:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setBandCol(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/TableShape;->bandCol:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBandRow(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/TableShape;->bandRow:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setColumnCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/shape/TableShape;->colCnt:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFirstCol(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/TableShape;->firstCol:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFirstRow(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/TableShape;->firstRow:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLastCol(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/TableShape;->lastCol:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLastRow(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/TableShape;->lastRow:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRowCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/shape/TableShape;->rowCnt:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTable07(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/TableShape;->shape07:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public interface abstract Lcom/intsig/office/common/shape/IShape;
.super Ljava/lang/Object;
.source "IShape.java"


# virtual methods
.method public abstract dispose()V
.end method

.method public abstract getAnimation()Lcom/intsig/office/pg/animate/IAnimation;
.end method

.method public abstract getBounds()Lcom/intsig/office/java/awt/Rectangle;
.end method

.method public abstract getData()Ljava/lang/Object;
.end method

.method public abstract getFlipHorizontal()Z
.end method

.method public abstract getFlipVertical()Z
.end method

.method public abstract getGroupShapeID()I
.end method

.method public abstract getParent()Lcom/intsig/office/common/shape/IShape;
.end method

.method public abstract getPlaceHolderID()I
.end method

.method public abstract getRotation()F
.end method

.method public abstract getShapeID()I
.end method

.method public abstract getType()S
.end method

.method public abstract isHidden()Z
.end method

.method public abstract setAnimation(Lcom/intsig/office/pg/animate/IAnimation;)V
.end method

.method public abstract setBounds(Lcom/intsig/office/java/awt/Rectangle;)V
.end method

.method public abstract setData(Ljava/lang/Object;)V
.end method

.method public abstract setFlipHorizontal(Z)V
.end method

.method public abstract setFlipVertical(Z)V
.end method

.method public abstract setGroupShapeID(I)V
.end method

.method public abstract setHidden(Z)V
.end method

.method public abstract setParent(Lcom/intsig/office/common/shape/IShape;)V
.end method

.method public abstract setPlaceHolderID(I)V
.end method

.method public abstract setRotation(F)V
.end method

.method public abstract setShapeID(I)V
.end method

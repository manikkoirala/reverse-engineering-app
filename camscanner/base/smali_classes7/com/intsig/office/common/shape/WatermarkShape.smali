.class public Lcom/intsig/office/common/shape/WatermarkShape;
.super Lcom/intsig/office/common/shape/WPAutoShape;
.source "WatermarkShape.java"


# static fields
.field public static final Watermark_Picture:B = 0x1t

.field public static final Watermark_Text:B


# instance fields
.field private final OPACITY:F

.field private blacklevel:F

.field private effect:Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

.field private fontColor:I

.field private fontSize:I

.field private gain:F

.field private isAutoFontSize:Z

.field private opacity:F

.field private pictureIndex:I

.field private watermarkType:B

.field private watermartString:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/common/shape/WPAutoShape;-><init>()V

    .line 2
    .line 3
    .line 4
    const v0, 0x3e4ccccd    # 0.2f

    .line 5
    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->OPACITY:F

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    iput-boolean v1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->isAutoFontSize:Z

    .line 11
    .line 12
    const/16 v1, 0x24

    .line 13
    .line 14
    iput v1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->fontSize:I

    .line 15
    .line 16
    const/high16 v1, -0x1000000

    .line 17
    .line 18
    iput v1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->fontColor:I

    .line 19
    .line 20
    const/4 v1, -0x1

    .line 21
    iput v1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->pictureIndex:I

    .line 22
    .line 23
    iput v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->opacity:F

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->watermartString:Ljava/lang/String;

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->effect:Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->dispose()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->effect:Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBlacklevel()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->blacklevel:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEffectInfor()Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;
    .locals 3

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->watermarkType:B

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_1

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->effect:Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 7
    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    new-instance v0, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 11
    .line 12
    invoke-direct {v0}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;-><init>()V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->effect:Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 16
    .line 17
    iget v1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->opacity:F

    .line 18
    .line 19
    const/high16 v2, 0x437f0000    # 255.0f

    .line 20
    .line 21
    mul-float v1, v1, v2

    .line 22
    .line 23
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setAlpha(Ljava/lang/Integer;)V

    .line 32
    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->effect:Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 35
    .line 36
    iget v1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->blacklevel:F

    .line 37
    .line 38
    mul-float v1, v1, v2

    .line 39
    .line 40
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    int-to-float v1, v1

    .line 45
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;->setBrightness(F)V

    .line 46
    .line 47
    .line 48
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->effect:Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 49
    .line 50
    return-object v0

    .line 51
    :cond_1
    const/4 v0, 0x0

    .line 52
    return-object v0
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getFontColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->fontColor:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFontSize()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->fontSize:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getGain()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->gain:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOpacity()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->opacity:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPictureIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->pictureIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getType()S
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->watermarkType:B

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x2

    .line 6
    return v0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getWatermarkType()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->watermarkType:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getWatermartString()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->watermartString:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isAutoFontSize()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/common/shape/WatermarkShape;->isAutoFontSize:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isWatermarkShape()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setAutoFontSize(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->isAutoFontSize:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBlacklevel(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->blacklevel:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFontColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->fontColor:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFontSize(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->fontSize:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setGain(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->gain:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOpacity(F)V
    .locals 1

    .line 1
    const v0, 0x3e4ccccd    # 0.2f

    .line 2
    .line 3
    .line 4
    mul-float p1, p1, v0

    .line 5
    .line 6
    iput p1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->opacity:F

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPictureIndex(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->pictureIndex:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setWatermarkType(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->watermarkType:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setWatermartString(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/shape/WatermarkShape;->watermartString:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

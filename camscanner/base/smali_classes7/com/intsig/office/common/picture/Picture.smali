.class public Lcom/intsig/office/common/picture/Picture;
.super Ljava/lang/Object;
.source "Picture.java"


# static fields
.field public static final DIB:B = 0x7t

.field public static final DIB_TYPE:Ljava/lang/String; = "dib"

.field public static final EMF:B = 0x2t

.field public static final EMF_TYPE:Ljava/lang/String; = "emf"

.field public static final GIF:B = 0x8t

.field public static final GIF_TYPE:Ljava/lang/String; = "gif"

.field public static final JPEG:B = 0x5t

.field public static final JPEG_TYPE:Ljava/lang/String; = "jpeg"

.field public static final PICT:B = 0x4t

.field public static final PICT_TYPE:Ljava/lang/String; = "pict"

.field public static final PNG:B = 0x6t

.field public static final PNG_TYPE:Ljava/lang/String; = "png"

.field public static final WMF:B = 0x3t

.field public static final WMF_TYPE:Ljava/lang/String; = "wmf"


# instance fields
.field private data:[B

.field private tempFilePath:Ljava/lang/String;

.field private type:B

.field private zoomX:S

.field private zoomY:S


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/common/picture/Picture;->tempFilePath:Ljava/lang/String;

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getData()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/picture/Picture;->data:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPictureType()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/common/picture/Picture;->type:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTempFilePath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/common/picture/Picture;->tempFilePath:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getZoomX()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/common/picture/Picture;->zoomX:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getZoomY()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/common/picture/Picture;->zoomY:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setData([B)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/picture/Picture;->data:[B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPictureType(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/common/picture/Picture;->type:B

    return-void
.end method

.method public setPictureType(Ljava/lang/String;)V
    .locals 1

    const-string v0, "emf"

    .line 2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x2

    .line 3
    iput-byte p1, p0, Lcom/intsig/office/common/picture/Picture;->type:B

    goto :goto_0

    :cond_0
    const-string v0, "wmf"

    .line 4
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, 0x3

    .line 5
    iput-byte p1, p0, Lcom/intsig/office/common/picture/Picture;->type:B

    goto :goto_0

    :cond_1
    const-string v0, "pict"

    .line 6
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p1, 0x4

    .line 7
    iput-byte p1, p0, Lcom/intsig/office/common/picture/Picture;->type:B

    goto :goto_0

    :cond_2
    const-string v0, "jpeg"

    .line 8
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 p1, 0x5

    .line 9
    iput-byte p1, p0, Lcom/intsig/office/common/picture/Picture;->type:B

    goto :goto_0

    :cond_3
    const-string v0, "png"

    .line 10
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 p1, 0x6

    .line 11
    iput-byte p1, p0, Lcom/intsig/office/common/picture/Picture;->type:B

    goto :goto_0

    :cond_4
    const-string v0, "dib"

    .line 12
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 p1, 0x7

    .line 13
    iput-byte p1, p0, Lcom/intsig/office/common/picture/Picture;->type:B

    goto :goto_0

    :cond_5
    const-string v0, "gif"

    .line 14
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_6

    const/16 p1, 0x8

    .line 15
    iput-byte p1, p0, Lcom/intsig/office/common/picture/Picture;->type:B

    :cond_6
    :goto_0
    return-void
.end method

.method public setTempFilePath(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/common/picture/Picture;->tempFilePath:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setZoomX(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/common/picture/Picture;->zoomX:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setZoomY(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/common/picture/Picture;->zoomY:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

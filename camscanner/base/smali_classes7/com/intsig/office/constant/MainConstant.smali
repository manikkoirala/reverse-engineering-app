.class public final Lcom/intsig/office/constant/MainConstant;
.super Ljava/lang/Object;
.source "MainConstant.java"


# static fields
.field public static final APPLICATION_TYPE_PDF:B = 0x3t

.field public static final APPLICATION_TYPE_PPT:B = 0x2t

.field public static final APPLICATION_TYPE_SS:B = 0x1t

.field public static final APPLICATION_TYPE_TXT:B = 0x4t

.field public static final APPLICATION_TYPE_WP:B = 0x0t

.field public static final DEFAULT_TAB_WIDTH_PIXEL:F = 28.0f

.field public static final DEFAULT_TAB_WIDTH_POINT:F = 21.0f

.field public static final DRAWMODE_CALLOUTDRAW:I = 0x1

.field public static final DRAWMODE_CALLOUTERASE:I = 0x2

.field public static final DRAWMODE_NORMAL:I = 0x0

.field public static final EMU_PER_INCH:I = 0xdf3e0

.field public static final FILE_TYPE_DOC:Ljava/lang/String; = "doc"

.field public static final FILE_TYPE_DOCX:Ljava/lang/String; = "docx"

.field public static final FILE_TYPE_DOT:Ljava/lang/String; = "dot"

.field public static final FILE_TYPE_DOTM:Ljava/lang/String; = "dotm"

.field public static final FILE_TYPE_DOTX:Ljava/lang/String; = "dotx"

.field public static final FILE_TYPE_PDF:Ljava/lang/String; = "pdf"

.field public static final FILE_TYPE_POT:Ljava/lang/String; = "pot"

.field public static final FILE_TYPE_POTM:Ljava/lang/String; = "potm"

.field public static final FILE_TYPE_POTX:Ljava/lang/String; = "potx"

.field public static final FILE_TYPE_PPT:Ljava/lang/String; = "ppt"

.field public static final FILE_TYPE_PPTM:Ljava/lang/String; = "pptm"

.field public static final FILE_TYPE_PPTX:Ljava/lang/String; = "pptx"

.field public static final FILE_TYPE_TXT:Ljava/lang/String; = "txt"

.field public static final FILE_TYPE_XLS:Ljava/lang/String; = "xls"

.field public static final FILE_TYPE_XLSM:Ljava/lang/String; = "xlsm"

.field public static final FILE_TYPE_XLSX:Ljava/lang/String; = "xlsx"

.field public static final FILE_TYPE_XLT:Ljava/lang/String; = "xlt"

.field public static final FILE_TYPE_XLTM:Ljava/lang/String; = "xltm"

.field public static final FILE_TYPE_XLTX:Ljava/lang/String; = "xltx"

.field public static final GAP:I = 0x5

.field public static final HANDLER_MESSAGE_DISMISS_PROGRESS:I = 0x3

.field public static final HANDLER_MESSAGE_DISPOSE:I = 0x4

.field public static final HANDLER_MESSAGE_ERROR:I = 0x1

.field public static final HANDLER_MESSAGE_SEND_READER_INSTANCE:I = 0x4

.field public static final HANDLER_MESSAGE_SHOW_PROGRESS:I = 0x2

.field public static final HANDLER_MESSAGE_SUCCESS:I = 0x0

.field public static final INTENT_FILED_FILE_LIST_TYPE:Ljava/lang/String; = "fileListType"

.field public static final INTENT_FILED_FILE_NAME:Ljava/lang/String; = "fileName"

.field public static final INTENT_FILED_FILE_PATH:Ljava/lang/String; = "filePath"

.field public static final INTENT_FILED_FILE_URI:Ljava/lang/String; = "fileUri"

.field public static final INTENT_FILED_MARK_FILES:Ljava/lang/String; = "markFiles"

.field public static final INTENT_FILED_MARK_STATUS:Ljava/lang/String; = "markFileStatus"

.field public static final INTENT_FILED_RECENT_FILES:Ljava/lang/String; = "recentFiles"

.field public static final INTENT_FILED_SDCARD_FILES:Ljava/lang/String; = "sdcard"

.field public static final INTENT_OBJECT_ITEM:Ljava/lang/String; = "file"

.field public static final MAXZOOM:I = 0x7530

.field public static final MAXZOOM_THUMBNAIL:I = 0x1388

.field public static final MM_TO_POINT:F = 2.835f

.field public static final PIXEL_DPI:F = 96.0f

.field public static final PIXEL_TO_POINT:F = 0.75f

.field public static final PIXEL_TO_TWIPS:F = 15.0f

.field public static final POINT_DPI:F = 72.0f

.field public static final POINT_TO_PIXEL:F = 1.3333334f

.field public static final POINT_TO_TWIPS:F = 20.0f

.field public static final STANDARD_RATE:I = 0x2710

.field public static final TABLE_RECENT:Ljava/lang/String; = "openedfiles"

.field public static final TABLE_SETTING:Ljava/lang/String; = "settings"

.field public static final TABLE_STAR:Ljava/lang/String; = "starredfiles"

.field public static final TWIPS_TO_PIXEL:F = 0.06666667f

.field public static final TWIPS_TO_POINT:F = 0.05f

.field public static final ZOOM_ROUND:I = 0x989680


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

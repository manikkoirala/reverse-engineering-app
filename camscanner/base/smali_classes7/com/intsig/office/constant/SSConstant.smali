.class public final Lcom/intsig/office/constant/SSConstant;
.super Ljava/lang/Object;
.source "SSConstant.java"


# static fields
.field public static final ACTIVE_CELL_COLOR:I

.field public static final ACTIVE_COLOR:I

.field public static final COLUMN_CHAR_WIDTH:F = 6.0f

.field public static final DEFAULT_COLUMN_HEADER_HEIGHT:I = 0x1e

.field public static final DEFAULT_COLUMN_WIDTH:I = 0x48

.field public static final DEFAULT_ROW_HEADER_WIDTH:I = 0x32

.field public static final DEFAULT_ROW_HEIGHT:I = 0x12

.field public static final GRIDLINE_COLOR:I = -0x382e27

.field public static final HEADER_FILL_COLOR:I

.field public static final HEADER_GRIDLINE_COLOR:I = -0x382e27

.field public static final HEADER_TEXT_COLOR:I = -0x1000000

.field public static final HEADER_TEXT_FONTSZIE:I = 0x10

.field public static final INDENT_TO_PIXEL:I = 0x22

.field public static final PAGE_DEFAULT_ZOOM:F = 0.9f

.field public static final PAGE_MAX_ZOOM:F = 9.0f

.field public static final PAGE_MIN_ZOOM:F = 0.9f

.field public static final SHEET_SPACETOBORDER:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-string v0, "#F8F8F8"

    .line 2
    .line 3
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    sput v0, Lcom/intsig/office/constant/SSConstant;->HEADER_FILL_COLOR:I

    .line 8
    .line 9
    const-string v0, "#F1F1F1"

    .line 10
    .line 11
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    sput v0, Lcom/intsig/office/constant/SSConstant;->ACTIVE_COLOR:I

    .line 16
    .line 17
    const-string v0, "#2E7D32"

    .line 18
    .line 19
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    sput v0, Lcom/intsig/office/constant/SSConstant;->ACTIVE_CELL_COLOR:I

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

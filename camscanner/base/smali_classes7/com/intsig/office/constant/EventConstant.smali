.class public final Lcom/intsig/office/constant/EventConstant;
.super Ljava/lang/Object;
.source "EventConstant.java"


# static fields
.field public static final APP_ABORTREADING:I = 0x20000009

.field public static final APP_APPROVE_ID:I = 0x20000004

.field public static final APP_AUTHENTICATE_PASSWORD:I = 0x20000011

.field public static final APP_BACK_ID:I = 0x2000001a

.field public static final APP_COLOR_ID:I = 0x2000001d

.field public static final APP_CONTENT_SELECTED:I = 0x20000007

.field public static final APP_COUNT_PAGES_CHANGE_ID:I = 0x2000000f

.field public static final APP_COUNT_PAGES_ID:I = 0x2000000b

.field public static final APP_CURRENT_PAGE_NUMBER_ID:I = 0x2000000c

.field public static final APP_DRAW_ID:I = 0x20000019

.field public static final APP_ERASER_ID:I = 0x2000001c

.field public static final APP_FINDING:I = 0x2f000000

.field public static final APP_FIND_BACKWARD:I = 0x2f000001

.field public static final APP_FIND_FORWARD:I = 0x2f000002

.field public static final APP_FIND_ID:I = 0x20000000

.field public static final APP_FIT_ZOOM_ID:I = 0x20000006

.field public static final APP_GENERATED_PICTURE_ID:I = 0x2000000a

.field public static final APP_GET_FIT_SIZE_STATE_ID:I = 0x20000016

.field public static final APP_GET_HYPERLINK_URL_ID:I = 0x20000014

.field public static final APP_GET_REAL_PAGE_COUNT_ID:I = 0x20000017

.field public static final APP_GET_SNAPSHOT_ID:I = 0x20000018

.field public static final APP_HYPERLINK:I = 0x20000008

.field public static final APP_INIT_CALLOUTVIEW_ID:I = 0x2000001e

.field public static final APP_INTERNET_SEARCH_ID:I = 0x20000002

.field public static final APP_PAGEAREA_TO_IMAGE:I = 0x20000013

.field public static final APP_PAGE_DOWN_ID:I = 0x2000000e

.field public static final APP_PAGE_UP_ID:I = 0x2000000d

.field public static final APP_PASSWORD_OK_INIT:I = 0x20000012

.field public static final APP_PEN_ID:I = 0x2000001b

.field public static final APP_READ_ID:I = 0x20000003

.field public static final APP_SCROLL_PERCENT_Y:I = 0x20000022

.field public static final APP_SET_FIT_SIZE_ID:I = 0x20000015

.field public static final APP_SHARE_ID:I = 0x20000001

.field public static final APP_THUMBNAIL_ID:I = 0x20000010

.field public static final APP_ZOOM_DOWN_ID:I = 0x20000020

.field public static final APP_ZOOM_ID:I = 0x20000005

.field public static final APP_ZOOM_MAX_ID:I = 0x2000001f

.field public static final APP_ZOOM_UP_ID:I = 0x20000021

.field public static final FILE_COPY_ID:I = 0x10000002

.field public static final FILE_CREATE_FOLDER_FAILED_ID:I = 0x1000000c

.field public static final FILE_CREATE_FOLDER_ID:I = 0x10000000

.field public static final FILE_CUT_ID:I = 0x10000003

.field public static final FILE_DELETE_ID:I = 0x10000005

.field public static final FILE_MARK_STAR_ID:I = 0x10000008

.field public static final FILE_PASTE_ID:I = 0x10000004

.field public static final FILE_PRINT_ID:I = 0x10000009

.field public static final FILE_REFRESH_ID:I = 0x1000000a

.field public static final FILE_RENAME_ID:I = 0x10000001

.field public static final FILE_SHARE_ID:I = 0x10000006

.field public static final FILE_SORT_ID:I = 0x10000007

.field public static final FILE_SORT_TYPE_ID:I = 0x1000000b

.field public static final PDF_GET_PAGE_SIZE:I = 0x60000002

.field public static final PDF_PAGE_TO_IMAGE:I = 0x60000001

.field public static final PDF_SHOW_PAGE:I = 0x60000000

.field public static final PG_GET_SLIDE_NOTE:I = 0x50000004

.field public static final PG_GET_SLIDE_SIZE:I = 0x50000005

.field public static final PG_NOTE_ID:I = 0x50000000

.field public static final PG_REPAINT_ID:I = 0x50000001

.field public static final PG_SHOW_SLIDE_ID:I = 0x50000002

.field public static final PG_SLIDESHOW:I = 0x51000000

.field public static final PG_SLIDESHOW_ANIMATIONSTEPS:I = 0x51000009

.field public static final PG_SLIDESHOW_DURATION:I = 0x51000007

.field public static final PG_SLIDESHOW_END:I = 0x51000002

.field public static final PG_SLIDESHOW_GEGIN:I = 0x51000001

.field public static final PG_SLIDESHOW_HASNEXTACTION:I = 0x51000006

.field public static final PG_SLIDESHOW_HASPREVIOUSACTION:I = 0x51000005

.field public static final PG_SLIDESHOW_NEXT:I = 0x51000004

.field public static final PG_SLIDESHOW_PREVIOUS:I = 0x51000003

.field public static final PG_SLIDESHOW_SLIDEEXIST:I = 0x51000008

.field public static final PG_SLIDESHOW_SLIDESHOWTOIMAGE:I = 0x5100000a

.field public static final PG_SLIDE_TO_IMAGE:I = 0x50000003

.field public static final SS_CHANGE_SHEET:I = 0x40000004

.field public static final SS_GET_ALL_SHEET_NAME:I = 0x40000002

.field public static final SS_GET_SHEET_NAME:I = 0x40000003

.field public static final SS_REMOVE_SHEET_BAR:I = 0x40000005

.field public static final SS_SHEET_CHANGE:I = 0x40000000

.field public static final SS_SHOW_SHEET:I = 0x40000001

.field public static final SYS_ABOUT_ID:I = 0x10

.field public static final SYS_ACCOUNT_ID:I = 0xc

.field public static final SYS_AUTO_TEST_FINISH_ID:I = 0x16

.field public static final SYS_CLOSE_TOOLTIP:I = 0x12

.field public static final SYS_FILEPAHT_ID:I = 0x1

.field public static final SYS_HELP_ID:I = 0xf

.field public static final SYS_INIT_ID:I = 0x13

.field public static final SYS_MARK_FILES_ID:I = 0x3

.field public static final SYS_MEMORY_CARD_ID:I = 0x4

.field public static final SYS_ONBACK_ID:I = 0x0

.field public static final SYS_READER_FINSH_ID:I = 0x17

.field public static final SYS_RECENTLY_FILES_ID:I = 0x2

.field public static final SYS_REGISTER_ID:I = 0xd

.field public static final SYS_RESET_TITLE_ID:I = 0x19

.field public static final SYS_SEARCH_ID:I = 0x5

.field public static final SYS_SETTINGS_ID:I = 0x6

.field public static final SYS_SET_MAX_RECENT_NUMBER:I = 0x15

.field public static final SYS_SET_PROGRESS_BAR_ID:I = 0x1a

.field public static final SYS_SHOW_TOOLTIP:I = 0x11

.field public static final SYS_START_BACK_READER_ID:I = 0x18

.field public static final SYS_UPDATE_ID:I = 0xe

.field public static final SYS_UPDATE_TOOLSBAR_BUTTON_STATUS:I = 0x14

.field public static final SYS_VECTORGRAPH_PROGRESS:I = 0x1b

.field public static final TEST_REPAINT_ID:I = -0x10000000

.field public static final TXT_DIALOG_FINISH_ID:I = 0x7000000

.field public static final TXT_REOPNE_ID:I = 0x7000001

.field public static final WP_GET_PAGE_SIZE:I = 0x30000004

.field public static final WP_GET_VIEW_MODE:I = 0x30000006

.field public static final WP_LAYOUT_COMPLETED:I = 0x30000008

.field public static final WP_LAYOUT_NORMAL_VIEW:I = 0x30000005

.field public static final WP_PAGE_TO_IMAGE:I = 0x30000003

.field public static final WP_PRINT_MODE:I = 0x30000007

.field public static final WP_SELECT_TEXT_ID:I = 0x30000000

.field public static final WP_SHOW_PAGE:I = 0x30000002

.field public static final WP_SWITCH_VIEW:I = 0x30000001


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public Lcom/intsig/office/constant/wp/AttrIDConstant;
.super Ljava/lang/Object;
.source "AttrIDConstant.java"


# static fields
.field public static final FONT_BOLD_ID:S = 0x4s

.field public static final FONT_COLOR_ID:S = 0x3s

.field public static final FONT_DOUBLESTRIKE_ID:S = 0x7s

.field public static final FONT_ENCLOSE_CHARACTER_TYPE_ID:S = 0x10s

.field public static final FONT_HIGHLIGHT_ID:S = 0xbs

.field public static final FONT_HYPERLINK_ID:S = 0xcs

.field public static final FONT_ITALIC_ID:S = 0x5s

.field public static final FONT_NAME_ID:S = 0x2s

.field public static final FONT_PAGE_NUMBER_TYPE_ID:S = 0xfs

.field public static final FONT_SCALE_ID:S = 0xes

.field public static final FONT_SCRIPT_ID:S = 0xas

.field public static final FONT_SHAPE_ID:S = 0xds

.field public static final FONT_SIZE_ID:S = 0x1s

.field public static final FONT_STRIKE_ID:S = 0x6s

.field public static final FONT_STYLE_ID:S = 0x0s

.field public static final FONT_UNDERLINE_COLOR_ID:S = 0x9s

.field public static final FONT_UNDERLINE_ID:S = 0x8s

.field public static final PAGE_BACKGROUND_COLOR_ID:S = 0x200as

.field public static final PAGE_BORDER_ID:S = 0x200bs

.field public static final PAGE_BOTTOM_ID:S = 0x2005s

.field public static final PAGE_FOOTER_ID:S = 0x2008s

.field public static final PAGE_HEADER_ID:S = 0x2007s

.field public static final PAGE_HEIGHT_ID:S = 0x2001s

.field public static final PAGE_HORIZONTAL_ID:S = 0x2009s

.field public static final PAGE_LEFT_ID:S = 0x2002s

.field public static final PAGE_LINEPITCH_ID:S = 0x200cs

.field public static final PAGE_RIGHT_ID:S = 0x2003s

.field public static final PAGE_TOP_ID:S = 0x2004s

.field public static final PAGE_VERTICAL_ID:S = 0x2006s

.field public static final PAGE_WIDTH_ID:S = 0x2000s

.field public static final PARA_AFTER_ID:S = 0x1005s

.field public static final PARA_BEFORE_ID:S = 0x1004s

.field public static final PARA_HORIZONTAL_ID:S = 0x1006s

.field public static final PARA_INDENT_INITLEFT_ID:S = 0x1002s

.field public static final PARA_INDENT_LEFT_ID:S = 0x1001s

.field public static final PARA_INDENT_RIGHT_ID:S = 0x1003s

.field public static final PARA_LEVEL_ID:S = 0x100bs

.field public static final PARA_LINESPACE_ID:S = 0x1009s

.field public static final PARA_LINESPACE_TYPE_ID:S = 0x100as

.field public static final PARA_LIST_ID:S = 0x100ds

.field public static final PARA_LIST_LEVEL_ID:S = 0x100cs

.field public static final PARA_PG_BULLET_ID:S = 0x100es

.field public static final PARA_SPECIALINDENT_ID:S = 0x1008s

.field public static final PARA_STYLE_ID:S = 0x1000s

.field public static final PARA_TABS_CLEAR_POSITION_ID:S = 0x100fs

.field public static final PARA_VERTICAL_ID:S = 0x1007s

.field public static final TABLE_BOTTOM_BORDER_COLOR_ID:S = 0x3003s

.field public static final TABLE_BOTTOM_BORDER_ID:S = 0x3002s

.field public static final TABLE_BOTTOM_MARGIN_ID:S = 0x3012s

.field public static final TABLE_CELL_HORIZONTAL_MERGED_ID:S = 0x300ds

.field public static final TABLE_CELL_HOR_FIRST_MERGED_ID:S = 0x300cs

.field public static final TABLE_CELL_VERTICAL_ALIGN_ID:S = 0x3010s

.field public static final TABLE_CELL_VERTICAL_MERGED_ID:S = 0x300fs

.field public static final TABLE_CELL_VER_FIRST_MERGED_ID:S = 0x300es

.field public static final TABLE_CELL_WIDTH_ID:S = 0x3009s

.field public static final TABLE_LEFT_BORDER_COLOR_ID:S = 0x3005s

.field public static final TABLE_LEFT_BORDER_ID:S = 0x3004s

.field public static final TABLE_LEFT_MARGIN_ID:S = 0x3013s

.field public static final TABLE_RIGHT_BORDER_COLOR_ID:S = 0x3007s

.field public static final TABLE_RIGHT_BORDER_ID:S = 0x3006s

.field public static final TABLE_RIGHT_MARGIN_ID:S = 0x3014s

.field public static final TABLE_ROW_HEADER_ID:S = 0x300as

.field public static final TABLE_ROW_HEIGHT_ID:S = 0x3008s

.field public static final TABLE_ROW_SPLIT_ID:S = 0x300bs

.field public static final TABLE_TOP_BORDER_COLOR_ID:S = 0x3001s

.field public static final TABLE_TOP_BORDER_ID:S = 0x3000s

.field public static final TABLE_TOP_MARGIN_ID:S = 0x3011s


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

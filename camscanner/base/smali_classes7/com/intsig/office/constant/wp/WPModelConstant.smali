.class public final Lcom/intsig/office/constant/wp/WPModelConstant;
.super Ljava/lang/Object;
.source "WPModelConstant.java"


# static fields
.field public static final AREA_MASK:J = -0x1000000000000000L

.field public static final ENCLOSURE_TYPE_RHOMBUS:B = 0x3t

.field public static final ENCLOSURE_TYPE_ROUND:B = 0x0t

.field public static final ENCLOSURE_TYPE_SQUARE:B = 0x1t

.field public static final ENCLOSURE_TYPE_TRIANGLE:B = 0x2t

.field public static final ENDNOTE:J = 0x4000000000000000L

.field public static final ENDNOTE_COLLECTION:S = 0x4s

.field public static final FOOTER:J = 0x2000000000000000L

.field public static final FOOTER_COLLECTION:S = 0x2s

.field public static final FOOTER_ELEMENT:S = 0x6s

.field public static final FOOTNOTE:J = 0x3000000000000000L

.field public static final FOOTNOTE_COLLECTION:S = 0x3s

.field public static final HEADER:J = 0x1000000000000000L

.field public static final HEADER_COLLECTION:S = 0x1s

.field public static final HEADER_ELEMENT:S = 0x5s

.field public static final HF_EVEN:B = 0x2t

.field public static final HF_FIRST:B = 0x0t

.field public static final HF_ODD:B = 0x1t

.field public static final LEAF_ELEMENT:S = 0x1s

.field public static final MAIN:J = 0x0L

.field public static final PARAGRAPH_ELEMENT:S = 0x1s

.field public static final PN_PAGE_NUMBER:B = 0x1t

.field public static final PN_TOTAL_PAGES:B = 0x2t

.field public static final SECTION_COLLECTION:S = 0x0s

.field public static final SECTION_ELEMENT:S = 0x0s

.field public static final TABLE_CELL_ELEMENT:S = 0x4s

.field public static final TABLE_ELEMENT:S = 0x2s

.field public static final TABLE_ROW_ELEMENT:S = 0x3s

.field public static final TEXTBOX:J = 0x5000000000000000L

.field public static final TEXTBOX_COLLECTION:S = 0x5s

.field public static final TEXTBOX_MASK:J = 0xfffffff00000000L


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public Lcom/intsig/office/fc/hpsf/VariantSupport;
.super Lcom/intsig/office/fc/hpsf/Variant;
.source "VariantSupport.java"


# static fields
.field public static final SUPPORTED_TYPES:[I

.field private static logUnsupportedTypes:Z = false

.field protected static unsupportedMessage:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/16 v0, 0xa

    .line 2
    .line 3
    new-array v0, v0, [I

    .line 4
    .line 5
    fill-array-data v0, :array_0

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/intsig/office/fc/hpsf/VariantSupport;->SUPPORTED_TYPES:[I

    .line 9
    .line 10
    return-void

    .line 11
    :array_0
    .array-data 4
        0x0
        0x2
        0x3
        0x14
        0x5
        0x40
        0x1e
        0x1f
        0x47
        0xb
    .end array-data
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hpsf/Variant;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static codepageToEncoding(I)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 1
    if-lez p0, :cond_4

    .line 2
    .line 3
    const/16 v0, 0x4b0

    .line 4
    .line 5
    if-eq p0, v0, :cond_3

    .line 6
    .line 7
    const/16 v0, 0x4b1

    .line 8
    .line 9
    if-eq p0, v0, :cond_2

    .line 10
    .line 11
    const/16 v0, 0x2761

    .line 12
    .line 13
    if-eq p0, v0, :cond_1

    .line 14
    .line 15
    const/16 v0, 0x2762

    .line 16
    .line 17
    if-eq p0, v0, :cond_0

    .line 18
    .line 19
    const-string v0, "EUC-KR"

    .line 20
    .line 21
    const-string v1, "SJIS"

    .line 22
    .line 23
    sparse-switch p0, :sswitch_data_0

    .line 24
    .line 25
    .line 26
    packed-switch p0, :pswitch_data_0

    .line 27
    .line 28
    .line 29
    packed-switch p0, :pswitch_data_1

    .line 30
    .line 31
    .line 32
    packed-switch p0, :pswitch_data_2

    .line 33
    .line 34
    .line 35
    packed-switch p0, :pswitch_data_3

    .line 36
    .line 37
    .line 38
    packed-switch p0, :pswitch_data_4

    .line 39
    .line 40
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    .line 42
    .line 43
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 44
    .line 45
    .line 46
    const-string v1, "cp"

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object p0

    .line 58
    return-object p0

    .line 59
    :pswitch_0
    const-string p0, "UTF-8"

    .line 60
    .line 61
    return-object p0

    .line 62
    :pswitch_1
    const-string p0, "ISO-2022-JP"

    .line 63
    .line 64
    return-object p0

    .line 65
    :pswitch_2
    const-string p0, "ISO-8859-9"

    .line 66
    .line 67
    return-object p0

    .line 68
    :pswitch_3
    const-string p0, "ISO-8859-8"

    .line 69
    .line 70
    return-object p0

    .line 71
    :pswitch_4
    const-string p0, "ISO-8859-7"

    .line 72
    .line 73
    return-object p0

    .line 74
    :pswitch_5
    const-string p0, "ISO-8859-6"

    .line 75
    .line 76
    return-object p0

    .line 77
    :pswitch_6
    const-string p0, "ISO-8859-5"

    .line 78
    .line 79
    return-object p0

    .line 80
    :pswitch_7
    const-string p0, "ISO-8859-4"

    .line 81
    .line 82
    return-object p0

    .line 83
    :pswitch_8
    const-string p0, "ISO-8859-3"

    .line 84
    .line 85
    return-object p0

    .line 86
    :pswitch_9
    const-string p0, "ISO-8859-2"

    .line 87
    .line 88
    return-object p0

    .line 89
    :pswitch_a
    const-string p0, "ISO-8859-1"

    .line 90
    .line 91
    return-object p0

    .line 92
    :pswitch_b
    const-string p0, "EUC_CN"

    .line 93
    .line 94
    return-object p0

    .line 95
    :pswitch_c
    const-string p0, "MacCyrillic"

    .line 96
    .line 97
    return-object p0

    .line 98
    :pswitch_d
    const-string p0, "MacGreek"

    .line 99
    .line 100
    return-object p0

    .line 101
    :pswitch_e
    const-string p0, "MacHebrew"

    .line 102
    .line 103
    return-object p0

    .line 104
    :pswitch_f
    const-string p0, "MacArabic"

    .line 105
    .line 106
    return-object p0

    .line 107
    :pswitch_10
    return-object v0

    .line 108
    :pswitch_11
    const-string p0, "Big5"

    .line 109
    .line 110
    return-object p0

    .line 111
    :pswitch_12
    return-object v1

    .line 112
    :pswitch_13
    const-string p0, "MacRoman"

    .line 113
    .line 114
    return-object p0

    .line 115
    :pswitch_14
    const-string p0, "windows-1258"

    .line 116
    .line 117
    return-object p0

    .line 118
    :pswitch_15
    const-string p0, "windows-1257"

    .line 119
    .line 120
    return-object p0

    .line 121
    :pswitch_16
    const-string p0, "windows-1256"

    .line 122
    .line 123
    return-object p0

    .line 124
    :pswitch_17
    const-string p0, "windows-1255"

    .line 125
    .line 126
    return-object p0

    .line 127
    :pswitch_18
    const-string p0, "windows-1254"

    .line 128
    .line 129
    return-object p0

    .line 130
    :pswitch_19
    const-string p0, "windows-1253"

    .line 131
    .line 132
    return-object p0

    .line 133
    :pswitch_1a
    const-string p0, "windows-1252"

    .line 134
    .line 135
    return-object p0

    .line 136
    :pswitch_1b
    const-string p0, "windows-1251"

    .line 137
    .line 138
    return-object p0

    .line 139
    :pswitch_1c
    const-string p0, "windows-1250"

    .line 140
    .line 141
    return-object p0

    .line 142
    :sswitch_0
    const-string p0, "GB18030"

    .line 143
    .line 144
    return-object p0

    .line 145
    :sswitch_1
    const-string p0, "GB2312"

    .line 146
    .line 147
    return-object p0

    .line 148
    :sswitch_2
    return-object v0

    .line 149
    :sswitch_3
    const-string p0, "EUC-JP"

    .line 150
    .line 151
    return-object p0

    .line 152
    :sswitch_4
    const-string p0, "ISO-2022-KR"

    .line 153
    .line 154
    return-object p0

    .line 155
    :sswitch_5
    const-string p0, "KOI8-R"

    .line 156
    .line 157
    return-object p0

    .line 158
    :pswitch_1d
    :sswitch_6
    const-string p0, "US-ASCII"

    .line 159
    .line 160
    return-object p0

    .line 161
    :sswitch_7
    const-string p0, "MacIceland"

    .line 162
    .line 163
    return-object p0

    .line 164
    :sswitch_8
    const-string p0, "MacCentralEurope"

    .line 165
    .line 166
    return-object p0

    .line 167
    :sswitch_9
    const-string p0, "MacThai"

    .line 168
    .line 169
    return-object p0

    .line 170
    :sswitch_a
    const-string p0, "MacUkraine"

    .line 171
    .line 172
    return-object p0

    .line 173
    :sswitch_b
    const-string p0, "MacRomania"

    .line 174
    .line 175
    return-object p0

    .line 176
    :sswitch_c
    const-string p0, "johab"

    .line 177
    .line 178
    return-object p0

    .line 179
    :sswitch_d
    const-string p0, "ms949"

    .line 180
    .line 181
    return-object p0

    .line 182
    :sswitch_e
    const-string p0, "GBK"

    .line 183
    .line 184
    return-object p0

    .line 185
    :sswitch_f
    return-object v1

    .line 186
    :sswitch_10
    const-string p0, "cp037"

    .line 187
    .line 188
    return-object p0

    .line 189
    :cond_0
    const-string p0, "MacCroatian"

    .line 190
    .line 191
    return-object p0

    .line 192
    :cond_1
    const-string p0, "MacTurkish"

    .line 193
    .line 194
    return-object p0

    .line 195
    :cond_2
    const-string p0, "UTF-16BE"

    .line 196
    .line 197
    return-object p0

    .line 198
    :cond_3
    const-string p0, "UTF-16"

    .line 199
    .line 200
    return-object p0

    .line 201
    :cond_4
    new-instance v0, Ljava/io/UnsupportedEncodingException;

    .line 202
    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    .line 204
    .line 205
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 206
    .line 207
    .line 208
    const-string v2, "Codepage number may not be "

    .line 209
    .line 210
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    .line 212
    .line 213
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 214
    .line 215
    .line 216
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 217
    .line 218
    .line 219
    move-result-object p0

    .line 220
    invoke-direct {v0, p0}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    .line 221
    .line 222
    .line 223
    throw v0

    .line 224
    nop

    .line 225
    :sswitch_data_0
    .sparse-switch
        0x25 -> :sswitch_10
        0x3a4 -> :sswitch_f
        0x3a8 -> :sswitch_e
        0x3b5 -> :sswitch_d
        0x551 -> :sswitch_c
        0x271a -> :sswitch_b
        0x2721 -> :sswitch_a
        0x2725 -> :sswitch_9
        0x272d -> :sswitch_8
        0x275f -> :sswitch_7
        0x4e9f -> :sswitch_6
        0x5182 -> :sswitch_5
        0xc431 -> :sswitch_4
        0xcadc -> :sswitch_3
        0xcaed -> :sswitch_2
        0xcec8 -> :sswitch_1
        0xd698 -> :sswitch_0
    .end sparse-switch

    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    :pswitch_data_0
    .packed-switch 0x4e2
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2710
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x6faf
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0xc42c
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0xfde8
        :pswitch_1d
        :pswitch_0
    .end packed-switch
.end method

.method public static isLogUnsupportedTypes()Z
    .locals 1

    .line 1
    sget-boolean v0, Lcom/intsig/office/fc/hpsf/VariantSupport;->logUnsupportedTypes:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static read([BIIJI)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hpsf/ReadingNotSupportedException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 1
    add-int/lit8 p2, p2, -0x4

    .line 2
    .line 3
    const/16 v0, 0x4b0

    .line 4
    .line 5
    if-ne p5, v0, :cond_0

    .line 6
    .line 7
    const-wide/16 v0, 0x1e

    .line 8
    .line 9
    cmp-long v2, p3, v0

    .line 10
    .line 11
    if-nez v2, :cond_0

    .line 12
    .line 13
    const-wide/16 v0, 0x1f

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    move-wide v0, p3

    .line 17
    :goto_0
    long-to-int v1, v0

    .line 18
    if-eqz v1, :cond_11

    .line 19
    .line 20
    const/4 v0, 0x5

    .line 21
    if-eq v1, v0, :cond_10

    .line 22
    .line 23
    const/16 v0, 0xb

    .line 24
    .line 25
    if-eq v1, v0, :cond_e

    .line 26
    .line 27
    const/16 v0, 0x14

    .line 28
    .line 29
    if-eq v1, v0, :cond_d

    .line 30
    .line 31
    const/16 v0, 0x40

    .line 32
    .line 33
    if-eq v1, v0, :cond_c

    .line 34
    .line 35
    const/16 v0, 0x47

    .line 36
    .line 37
    const/4 v2, 0x0

    .line 38
    if-eq v1, v0, :cond_a

    .line 39
    .line 40
    const/4 v0, 0x2

    .line 41
    if-eq v1, v0, :cond_9

    .line 42
    .line 43
    const/4 v0, 0x3

    .line 44
    if-eq v1, v0, :cond_8

    .line 45
    .line 46
    const/16 v0, 0x1e

    .line 47
    .line 48
    const-wide/16 v3, 0x1

    .line 49
    .line 50
    if-eq v1, v0, :cond_5

    .line 51
    .line 52
    const/16 p5, 0x1f

    .line 53
    .line 54
    if-eq v1, p5, :cond_2

    .line 55
    .line 56
    new-array p5, p2, [B

    .line 57
    .line 58
    :goto_1
    if-ge v2, p2, :cond_1

    .line 59
    .line 60
    add-int v0, p1, v2

    .line 61
    .line 62
    aget-byte v0, p0, v0

    .line 63
    .line 64
    aput-byte v0, p5, v2

    .line 65
    .line 66
    add-int/lit8 v2, v2, 0x1

    .line 67
    .line 68
    goto :goto_1

    .line 69
    :cond_1
    new-instance p0, Lcom/intsig/office/fc/hpsf/ReadingNotSupportedException;

    .line 70
    .line 71
    invoke-direct {p0, p3, p4, p5}, Lcom/intsig/office/fc/hpsf/ReadingNotSupportedException;-><init>(JLjava/lang/Object;)V

    .line 72
    .line 73
    .line 74
    throw p0

    .line 75
    :cond_2
    add-int/lit8 p2, p1, 0x4

    .line 76
    .line 77
    int-to-long p3, p2

    .line 78
    invoke-static {p0, p1}, Lcom/intsig/office/fc/util/LittleEndian;->getUInt([BI)J

    .line 79
    .line 80
    .line 81
    move-result-wide v0

    .line 82
    add-long/2addr v0, p3

    .line 83
    sub-long/2addr v0, v3

    .line 84
    sub-long/2addr v0, p3

    .line 85
    new-instance p1, Ljava/lang/StringBuffer;

    .line 86
    .line 87
    long-to-int p3, v0

    .line 88
    invoke-direct {p1, p3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 89
    .line 90
    .line 91
    :goto_2
    int-to-long p3, v2

    .line 92
    cmp-long p5, p3, v0

    .line 93
    .line 94
    if-gtz p5, :cond_3

    .line 95
    .line 96
    mul-int/lit8 p3, v2, 0x2

    .line 97
    .line 98
    add-int/2addr p3, p2

    .line 99
    add-int/lit8 p4, p3, 0x1

    .line 100
    .line 101
    aget-byte p4, p0, p4

    .line 102
    .line 103
    shl-int/lit8 p4, p4, 0x8

    .line 104
    .line 105
    aget-byte p3, p0, p3

    .line 106
    .line 107
    and-int/lit16 p3, p3, 0xff

    .line 108
    .line 109
    or-int/2addr p3, p4

    .line 110
    int-to-char p3, p3

    .line 111
    invoke-virtual {p1, p3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 112
    .line 113
    .line 114
    add-int/lit8 v2, v2, 0x1

    .line 115
    .line 116
    goto :goto_2

    .line 117
    :cond_3
    :goto_3
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    .line 118
    .line 119
    .line 120
    move-result p0

    .line 121
    if-lez p0, :cond_4

    .line 122
    .line 123
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    .line 124
    .line 125
    .line 126
    move-result p0

    .line 127
    add-int/lit8 p0, p0, -0x1

    .line 128
    .line 129
    invoke-virtual {p1, p0}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 130
    .line 131
    .line 132
    move-result p0

    .line 133
    if-nez p0, :cond_4

    .line 134
    .line 135
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    .line 136
    .line 137
    .line 138
    move-result p0

    .line 139
    add-int/lit8 p0, p0, -0x1

    .line 140
    .line 141
    invoke-virtual {p1, p0}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 142
    .line 143
    .line 144
    goto :goto_3

    .line 145
    :cond_4
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object p0

    .line 149
    goto/16 :goto_6

    .line 150
    .line 151
    :cond_5
    add-int/lit8 p2, p1, 0x4

    .line 152
    .line 153
    int-to-long p3, p2

    .line 154
    invoke-static {p0, p1}, Lcom/intsig/office/fc/util/LittleEndian;->getUInt([BI)J

    .line 155
    .line 156
    .line 157
    move-result-wide v0

    .line 158
    add-long/2addr v0, p3

    .line 159
    :goto_4
    sub-long/2addr v0, v3

    .line 160
    long-to-int p1, v0

    .line 161
    aget-byte p1, p0, p1

    .line 162
    .line 163
    if-nez p1, :cond_6

    .line 164
    .line 165
    cmp-long p1, p3, v0

    .line 166
    .line 167
    if-gtz p1, :cond_6

    .line 168
    .line 169
    goto :goto_4

    .line 170
    :cond_6
    sub-long/2addr v0, p3

    .line 171
    add-long/2addr v0, v3

    .line 172
    long-to-int p1, v0

    .line 173
    const/4 p3, -0x1

    .line 174
    if-eq p5, p3, :cond_7

    .line 175
    .line 176
    new-instance p3, Ljava/lang/String;

    .line 177
    .line 178
    invoke-static {p5}, Lcom/intsig/office/fc/hpsf/VariantSupport;->codepageToEncoding(I)Ljava/lang/String;

    .line 179
    .line 180
    .line 181
    move-result-object p4

    .line 182
    invoke-direct {p3, p0, p2, p1, p4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 183
    .line 184
    .line 185
    goto :goto_5

    .line 186
    :cond_7
    new-instance p3, Ljava/lang/String;

    .line 187
    .line 188
    invoke-direct {p3, p0, p2, p1}, Ljava/lang/String;-><init>([BII)V

    .line 189
    .line 190
    .line 191
    goto :goto_5

    .line 192
    :cond_8
    invoke-static {p0, p1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 193
    .line 194
    .line 195
    move-result p0

    .line 196
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 197
    .line 198
    .line 199
    move-result-object p0

    .line 200
    goto :goto_6

    .line 201
    :cond_9
    invoke-static {p0, p1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 202
    .line 203
    .line 204
    move-result p0

    .line 205
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 206
    .line 207
    .line 208
    move-result-object p0

    .line 209
    goto :goto_6

    .line 210
    :cond_a
    if-gez p2, :cond_b

    .line 211
    .line 212
    invoke-static {p0, p1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 213
    .line 214
    .line 215
    move-result p2

    .line 216
    add-int/lit8 p1, p1, 0x4

    .line 217
    .line 218
    :cond_b
    new-array p3, p2, [B

    .line 219
    .line 220
    invoke-static {p0, p1, p3, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 221
    .line 222
    .line 223
    :goto_5
    move-object p0, p3

    .line 224
    goto :goto_6

    .line 225
    :cond_c
    invoke-static {p0, p1}, Lcom/intsig/office/fc/util/LittleEndian;->getUInt([BI)J

    .line 226
    .line 227
    .line 228
    move-result-wide p2

    .line 229
    add-int/lit8 p1, p1, 0x4

    .line 230
    .line 231
    invoke-static {p0, p1}, Lcom/intsig/office/fc/util/LittleEndian;->getUInt([BI)J

    .line 232
    .line 233
    .line 234
    move-result-wide p0

    .line 235
    long-to-int p1, p0

    .line 236
    long-to-int p0, p2

    .line 237
    invoke-static {p1, p0}, Lcom/intsig/office/fc/hpsf/Util;->filetimeToDate(II)Ljava/util/Date;

    .line 238
    .line 239
    .line 240
    move-result-object p0

    .line 241
    goto :goto_6

    .line 242
    :cond_d
    invoke-static {p0, p1}, Lcom/intsig/office/fc/util/LittleEndian;->getLong([BI)J

    .line 243
    .line 244
    .line 245
    move-result-wide p0

    .line 246
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 247
    .line 248
    .line 249
    move-result-object p0

    .line 250
    goto :goto_6

    .line 251
    :cond_e
    invoke-static {p0, p1}, Lcom/intsig/office/fc/util/LittleEndian;->getUInt([BI)J

    .line 252
    .line 253
    .line 254
    move-result-wide p0

    .line 255
    const-wide/16 p2, 0x0

    .line 256
    .line 257
    cmp-long p4, p0, p2

    .line 258
    .line 259
    if-eqz p4, :cond_f

    .line 260
    .line 261
    sget-object p0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 262
    .line 263
    goto :goto_6

    .line 264
    :cond_f
    sget-object p0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 265
    .line 266
    goto :goto_6

    .line 267
    :cond_10
    new-instance p2, Ljava/lang/Double;

    .line 268
    .line 269
    invoke-static {p0, p1}, Lcom/intsig/office/fc/util/LittleEndian;->getDouble([BI)D

    .line 270
    .line 271
    .line 272
    move-result-wide p0

    .line 273
    invoke-direct {p2, p0, p1}, Ljava/lang/Double;-><init>(D)V

    .line 274
    .line 275
    .line 276
    move-object p0, p2

    .line 277
    goto :goto_6

    .line 278
    :cond_11
    const/4 p0, 0x0

    .line 279
    :goto_6
    return-object p0
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method public static setLogUnsupportedTypes(Z)V
    .locals 0

    .line 1
    sput-boolean p0, Lcom/intsig/office/fc/hpsf/VariantSupport;->logUnsupportedTypes:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static write(Ljava/io/OutputStream;JLjava/lang/Object;I)I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/intsig/office/fc/hpsf/WritingNotSupportedException;
        }
    .end annotation

    .line 1
    long-to-int v0, p1

    .line 2
    if-eqz v0, :cond_d

    .line 3
    .line 4
    const/4 v1, 0x5

    .line 5
    const/4 v2, 0x0

    .line 6
    if-eq v0, v1, :cond_c

    .line 7
    .line 8
    const/16 v1, 0xb

    .line 9
    .line 10
    if-eq v0, v1, :cond_b

    .line 11
    .line 12
    const/16 v1, 0x14

    .line 13
    .line 14
    const/16 v3, 0x8

    .line 15
    .line 16
    if-eq v0, v1, :cond_a

    .line 17
    .line 18
    const/16 v1, 0x40

    .line 19
    .line 20
    if-eq v0, v1, :cond_9

    .line 21
    .line 22
    const/16 v1, 0x47

    .line 23
    .line 24
    if-eq v0, v1, :cond_8

    .line 25
    .line 26
    const/4 v1, 0x2

    .line 27
    if-eq v0, v1, :cond_7

    .line 28
    .line 29
    const/4 v4, 0x3

    .line 30
    if-eq v0, v4, :cond_5

    .line 31
    .line 32
    const/16 v4, 0x1e

    .line 33
    .line 34
    if-eq v0, v4, :cond_3

    .line 35
    .line 36
    const/16 p4, 0x1f

    .line 37
    .line 38
    if-eq v0, p4, :cond_1

    .line 39
    .line 40
    instance-of p4, p3, [B

    .line 41
    .line 42
    if-eqz p4, :cond_0

    .line 43
    .line 44
    move-object p4, p3

    .line 45
    check-cast p4, [B

    .line 46
    .line 47
    invoke-virtual {p0, p4}, Ljava/io/OutputStream;->write([B)V

    .line 48
    .line 49
    .line 50
    array-length v3, p4

    .line 51
    new-instance p0, Lcom/intsig/office/fc/hpsf/WritingNotSupportedException;

    .line 52
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/fc/hpsf/WritingNotSupportedException;-><init>(JLjava/lang/Object;)V

    .line 54
    .line 55
    .line 56
    invoke-static {p0}, Lcom/intsig/office/fc/hpsf/VariantSupport;->writeUnsupportedTypeMessage(Lcom/intsig/office/fc/hpsf/UnsupportedVariantTypeException;)V

    .line 57
    .line 58
    .line 59
    goto/16 :goto_3

    .line 60
    .line 61
    :cond_0
    new-instance p0, Lcom/intsig/office/fc/hpsf/WritingNotSupportedException;

    .line 62
    .line 63
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/fc/hpsf/WritingNotSupportedException;-><init>(JLjava/lang/Object;)V

    .line 64
    .line 65
    .line 66
    throw p0

    .line 67
    :cond_1
    check-cast p3, Ljava/lang/String;

    .line 68
    .line 69
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    .line 70
    .line 71
    .line 72
    move-result p1

    .line 73
    add-int/lit8 p1, p1, 0x1

    .line 74
    .line 75
    int-to-long p1, p1

    .line 76
    invoke-static {p0, p1, p2}, Lcom/intsig/office/fc/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    .line 77
    .line 78
    .line 79
    move-result p1

    .line 80
    add-int/2addr p1, v2

    .line 81
    invoke-static {p3}, Lcom/intsig/office/fc/hpsf/Util;->pad4(Ljava/lang/String;)[C

    .line 82
    .line 83
    .line 84
    move-result-object p2

    .line 85
    const/4 p3, 0x0

    .line 86
    :goto_0
    array-length p4, p2

    .line 87
    if-ge p3, p4, :cond_2

    .line 88
    .line 89
    aget-char p4, p2, p3

    .line 90
    .line 91
    const v0, 0xff00

    .line 92
    .line 93
    .line 94
    and-int/2addr v0, p4

    .line 95
    shr-int/2addr v0, v3

    .line 96
    and-int/lit16 p4, p4, 0xff

    .line 97
    .line 98
    int-to-byte v0, v0

    .line 99
    int-to-byte p4, p4

    .line 100
    invoke-virtual {p0, p4}, Ljava/io/OutputStream;->write(I)V

    .line 101
    .line 102
    .line 103
    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    .line 104
    .line 105
    .line 106
    add-int/lit8 p1, p1, 0x2

    .line 107
    .line 108
    add-int/lit8 p3, p3, 0x1

    .line 109
    .line 110
    goto :goto_0

    .line 111
    :cond_2
    invoke-virtual {p0, v2}, Ljava/io/OutputStream;->write(I)V

    .line 112
    .line 113
    .line 114
    invoke-virtual {p0, v2}, Ljava/io/OutputStream;->write(I)V

    .line 115
    .line 116
    .line 117
    add-int/lit8 v3, p1, 0x2

    .line 118
    .line 119
    goto/16 :goto_3

    .line 120
    .line 121
    :cond_3
    const/4 p1, -0x1

    .line 122
    if-ne p4, p1, :cond_4

    .line 123
    .line 124
    check-cast p3, Ljava/lang/String;

    .line 125
    .line 126
    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    .line 127
    .line 128
    .line 129
    move-result-object p1

    .line 130
    goto :goto_1

    .line 131
    :cond_4
    check-cast p3, Ljava/lang/String;

    .line 132
    .line 133
    invoke-static {p4}, Lcom/intsig/office/fc/hpsf/VariantSupport;->codepageToEncoding(I)Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object p1

    .line 137
    invoke-virtual {p3, p1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    .line 138
    .line 139
    .line 140
    move-result-object p1

    .line 141
    :goto_1
    array-length p2, p1

    .line 142
    add-int/lit8 p2, p2, 0x1

    .line 143
    .line 144
    int-to-long p2, p2

    .line 145
    invoke-static {p0, p2, p3}, Lcom/intsig/office/fc/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    .line 146
    .line 147
    .line 148
    move-result p2

    .line 149
    array-length p3, p1

    .line 150
    add-int/lit8 p3, p3, 0x1

    .line 151
    .line 152
    new-array p4, p3, [B

    .line 153
    .line 154
    array-length v0, p1

    .line 155
    invoke-static {p1, v2, p4, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 156
    .line 157
    .line 158
    add-int/lit8 p1, p3, -0x1

    .line 159
    .line 160
    aput-byte v2, p4, p1

    .line 161
    .line 162
    invoke-virtual {p0, p4}, Ljava/io/OutputStream;->write([B)V

    .line 163
    .line 164
    .line 165
    add-int v3, p2, p3

    .line 166
    .line 167
    goto/16 :goto_3

    .line 168
    .line 169
    :cond_5
    instance-of p1, p3, Ljava/lang/Integer;

    .line 170
    .line 171
    if-eqz p1, :cond_6

    .line 172
    .line 173
    check-cast p3, Ljava/lang/Integer;

    .line 174
    .line 175
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    .line 176
    .line 177
    .line 178
    move-result p1

    .line 179
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;I)I

    .line 180
    .line 181
    .line 182
    move-result p0

    .line 183
    goto/16 :goto_2

    .line 184
    .line 185
    :cond_6
    new-instance p0, Ljava/lang/ClassCastException;

    .line 186
    .line 187
    new-instance p1, Ljava/lang/StringBuilder;

    .line 188
    .line 189
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 190
    .line 191
    .line 192
    const-string p2, "Could not cast an object to "

    .line 193
    .line 194
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    .line 196
    .line 197
    const-class p2, Ljava/lang/Integer;

    .line 198
    .line 199
    invoke-virtual {p2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    .line 200
    .line 201
    .line 202
    move-result-object p2

    .line 203
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    .line 205
    .line 206
    const-string p2, ": "

    .line 207
    .line 208
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    .line 210
    .line 211
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 212
    .line 213
    .line 214
    move-result-object p2

    .line 215
    invoke-virtual {p2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    .line 216
    .line 217
    .line 218
    move-result-object p2

    .line 219
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    .line 221
    .line 222
    const-string p2, ", "

    .line 223
    .line 224
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    .line 226
    .line 227
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 228
    .line 229
    .line 230
    move-result-object p2

    .line 231
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    .line 233
    .line 234
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 235
    .line 236
    .line 237
    move-result-object p1

    .line 238
    invoke-direct {p0, p1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    .line 239
    .line 240
    .line 241
    throw p0

    .line 242
    :cond_7
    check-cast p3, Ljava/lang/Integer;

    .line 243
    .line 244
    invoke-virtual {p3}, Ljava/lang/Integer;->shortValue()S

    .line 245
    .line 246
    .line 247
    move-result p1

    .line 248
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;S)I

    .line 249
    .line 250
    .line 251
    const/4 v3, 0x2

    .line 252
    goto :goto_3

    .line 253
    :cond_8
    check-cast p3, [B

    .line 254
    .line 255
    invoke-virtual {p0, p3}, Ljava/io/OutputStream;->write([B)V

    .line 256
    .line 257
    .line 258
    array-length v3, p3

    .line 259
    goto :goto_3

    .line 260
    :cond_9
    check-cast p3, Ljava/util/Date;

    .line 261
    .line 262
    invoke-static {p3}, Lcom/intsig/office/fc/hpsf/Util;->dateToFileTime(Ljava/util/Date;)J

    .line 263
    .line 264
    .line 265
    move-result-wide p1

    .line 266
    const/16 p3, 0x20

    .line 267
    .line 268
    shr-long p3, p1, p3

    .line 269
    .line 270
    const-wide v0, 0xffffffffL

    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    and-long/2addr p3, v0

    .line 276
    long-to-int p4, p3

    .line 277
    and-long/2addr p1, v0

    .line 278
    long-to-int p2, p1

    .line 279
    int-to-long p1, p2

    .line 280
    and-long/2addr p1, v0

    .line 281
    invoke-static {p0, p1, p2}, Lcom/intsig/office/fc/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    .line 282
    .line 283
    .line 284
    move-result p1

    .line 285
    add-int/2addr p1, v2

    .line 286
    int-to-long p2, p4

    .line 287
    and-long/2addr p2, v0

    .line 288
    invoke-static {p0, p2, p3}, Lcom/intsig/office/fc/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    .line 289
    .line 290
    .line 291
    move-result p0

    .line 292
    add-int v3, p1, p0

    .line 293
    .line 294
    goto :goto_3

    .line 295
    :cond_a
    check-cast p3, Ljava/lang/Long;

    .line 296
    .line 297
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    .line 298
    .line 299
    .line 300
    move-result-wide p1

    .line 301
    invoke-static {p0, p1, p2}, Lcom/intsig/office/fc/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;J)I

    .line 302
    .line 303
    .line 304
    goto :goto_3

    .line 305
    :cond_b
    check-cast p3, Ljava/lang/Boolean;

    .line 306
    .line 307
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 308
    .line 309
    .line 310
    move-result p1

    .line 311
    int-to-long p1, p1

    .line 312
    invoke-static {p0, p1, p2}, Lcom/intsig/office/fc/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    .line 313
    .line 314
    .line 315
    move-result v3

    .line 316
    goto :goto_3

    .line 317
    :cond_c
    check-cast p3, Ljava/lang/Double;

    .line 318
    .line 319
    invoke-virtual {p3}, Ljava/lang/Double;->doubleValue()D

    .line 320
    .line 321
    .line 322
    move-result-wide p1

    .line 323
    invoke-static {p0, p1, p2}, Lcom/intsig/office/fc/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;D)I

    .line 324
    .line 325
    .line 326
    move-result p0

    .line 327
    :goto_2
    add-int/lit8 v3, p0, 0x0

    .line 328
    .line 329
    goto :goto_3

    .line 330
    :cond_d
    const-wide/16 p1, 0x0

    .line 331
    .line 332
    invoke-static {p0, p1, p2}, Lcom/intsig/office/fc/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    .line 333
    .line 334
    .line 335
    const/4 v3, 0x4

    .line 336
    :goto_3
    return v3
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
.end method

.method protected static writeUnsupportedTypeMessage(Lcom/intsig/office/fc/hpsf/UnsupportedVariantTypeException;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hpsf/VariantSupport;->isLogUnsupportedTypes()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/office/fc/hpsf/VariantSupport;->unsupportedMessage:Ljava/util/List;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    new-instance v0, Ljava/util/LinkedList;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 14
    .line 15
    .line 16
    sput-object v0, Lcom/intsig/office/fc/hpsf/VariantSupport;->unsupportedMessage:Ljava/util/List;

    .line 17
    .line 18
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/VariantTypeException;->getVariantType()J

    .line 19
    .line 20
    .line 21
    move-result-wide v0

    .line 22
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    sget-object v1, Lcom/intsig/office/fc/hpsf/VariantSupport;->unsupportedMessage:Ljava/util/List;

    .line 27
    .line 28
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    if-nez v1, :cond_1

    .line 33
    .line 34
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    .line 35
    .line 36
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p0

    .line 40
    invoke-virtual {v1, p0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    sget-object p0, Lcom/intsig/office/fc/hpsf/VariantSupport;->unsupportedMessage:Ljava/util/List;

    .line 44
    .line 45
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    :cond_1
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public isSupportedType(I)Z
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    sget-object v2, Lcom/intsig/office/fc/hpsf/VariantSupport;->SUPPORTED_TYPES:[I

    .line 4
    .line 5
    array-length v3, v2

    .line 6
    if-ge v1, v3, :cond_1

    .line 7
    .line 8
    aget v2, v2, v1

    .line 9
    .line 10
    if-ne p1, v2, :cond_0

    .line 11
    .line 12
    const/4 p1, 0x1

    .line 13
    return p1

    .line 14
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_1
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

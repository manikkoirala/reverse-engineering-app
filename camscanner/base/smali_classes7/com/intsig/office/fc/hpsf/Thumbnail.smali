.class public final Lcom/intsig/office/fc/hpsf/Thumbnail;
.super Ljava/lang/Object;
.source "Thumbnail.java"


# static fields
.field public static CFTAG_FMTID:I = -0x3

.field public static CFTAG_MACINTOSH:I = -0x2

.field public static CFTAG_NODATA:I = 0x0

.field public static CFTAG_WINDOWS:I = -0x1

.field public static CF_BITMAP:I = 0x2

.field public static CF_DIB:I = 0x8

.field public static CF_ENHMETAFILE:I = 0xe

.field public static CF_METAFILEPICT:I = 0x3

.field public static OFFSET_CF:I = 0x8

.field public static OFFSET_CFTAG:I = 0x4

.field public static OFFSET_WMFDATA:I = 0x14


# instance fields
.field private _thumbnailData:[B


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hpsf/Thumbnail;->_thumbnailData:[B

    return-void
.end method

.method public constructor <init>([B)V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hpsf/Thumbnail;->_thumbnailData:[B

    return-void
.end method


# virtual methods
.method public getClipboardFormat()J
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hpsf/HPSFException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/Thumbnail;->getClipboardFormatTag()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    sget v2, Lcom/intsig/office/fc/hpsf/Thumbnail;->CFTAG_WINDOWS:I

    .line 6
    .line 7
    int-to-long v2, v2

    .line 8
    cmp-long v4, v0, v2

    .line 9
    .line 10
    if-nez v4, :cond_0

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/Thumbnail;->getThumbnail()[B

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    sget v1, Lcom/intsig/office/fc/hpsf/Thumbnail;->OFFSET_CF:I

    .line 17
    .line 18
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getUInt([BI)J

    .line 19
    .line 20
    .line 21
    move-result-wide v0

    .line 22
    return-wide v0

    .line 23
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hpsf/HPSFException;

    .line 24
    .line 25
    const-string v1, "Clipboard Format Tag of Thumbnail must be CFTAG_WINDOWS."

    .line 26
    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hpsf/HPSFException;-><init>(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    throw v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getClipboardFormatTag()J
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/Thumbnail;->getThumbnail()[B

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sget v1, Lcom/intsig/office/fc/hpsf/Thumbnail;->OFFSET_CFTAG:I

    .line 6
    .line 7
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getUInt([BI)J

    .line 8
    .line 9
    .line 10
    move-result-wide v0

    .line 11
    return-wide v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getThumbnail()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/Thumbnail;->_thumbnailData:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getThumbnailAsWMF()[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hpsf/HPSFException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/Thumbnail;->getClipboardFormatTag()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    sget v2, Lcom/intsig/office/fc/hpsf/Thumbnail;->CFTAG_WINDOWS:I

    .line 6
    .line 7
    int-to-long v2, v2

    .line 8
    cmp-long v4, v0, v2

    .line 9
    .line 10
    if-nez v4, :cond_1

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/Thumbnail;->getClipboardFormat()J

    .line 13
    .line 14
    .line 15
    move-result-wide v0

    .line 16
    sget v2, Lcom/intsig/office/fc/hpsf/Thumbnail;->CF_METAFILEPICT:I

    .line 17
    .line 18
    int-to-long v2, v2

    .line 19
    cmp-long v4, v0, v2

    .line 20
    .line 21
    if-nez v4, :cond_0

    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/Thumbnail;->getThumbnail()[B

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    array-length v1, v0

    .line 28
    sget v2, Lcom/intsig/office/fc/hpsf/Thumbnail;->OFFSET_WMFDATA:I

    .line 29
    .line 30
    sub-int/2addr v1, v2

    .line 31
    new-array v3, v1, [B

    .line 32
    .line 33
    const/4 v4, 0x0

    .line 34
    invoke-static {v0, v2, v3, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 35
    .line 36
    .line 37
    return-object v3

    .line 38
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hpsf/HPSFException;

    .line 39
    .line 40
    const-string v1, "Clipboard Format of Thumbnail must be CF_METAFILEPICT."

    .line 41
    .line 42
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hpsf/HPSFException;-><init>(Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    throw v0

    .line 46
    :cond_1
    new-instance v0, Lcom/intsig/office/fc/hpsf/HPSFException;

    .line 47
    .line 48
    const-string v1, "Clipboard Format Tag of Thumbnail must be CFTAG_WINDOWS."

    .line 49
    .line 50
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hpsf/HPSFException;-><init>(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    throw v0
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public setThumbnail([B)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hpsf/Thumbnail;->_thumbnailData:[B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

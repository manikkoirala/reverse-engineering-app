.class public Lcom/intsig/office/fc/hpsf/CustomProperties;
.super Ljava/util/HashMap;
.source "CustomProperties.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap<",
        "Ljava/lang/Object;",
        "Lcom/intsig/office/fc/hpsf/CustomProperty;",
        ">;"
    }
.end annotation


# instance fields
.field private dictionaryIDToName:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private dictionaryNameToID:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private isPure:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/HashMap;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryIDToName:Ljava/util/Map;

    .line 10
    .line 11
    new-instance v0, Ljava/util/HashMap;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryNameToID:Ljava/util/Map;

    .line 17
    .line 18
    const/4 v0, 0x1

    .line 19
    iput-boolean v0, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->isPure:Z

    .line 20
    .line 21
    return-void
.end method

.method private put(Lcom/intsig/office/fc/hpsf/CustomProperty;)Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassCastException;
        }
    .end annotation

    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/fc/hpsf/CustomProperty;->getName()Ljava/lang/String;

    move-result-object v0

    .line 13
    iget-object v1, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryNameToID:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 14
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setID(J)V

    goto :goto_1

    .line 15
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryIDToName:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const-wide/16 v2, 0x1

    move-wide v4, v2

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 16
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v8, v6, v4

    if-lez v8, :cond_1

    move-wide v4, v6

    goto :goto_0

    :cond_2
    add-long/2addr v4, v2

    .line 17
    invoke-virtual {p1, v4, v5}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setID(J)V

    .line 18
    :goto_1
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hpsf/CustomProperties;->put(Ljava/lang/String;Lcom/intsig/office/fc/hpsf/CustomProperty;)Lcom/intsig/office/fc/hpsf/CustomProperty;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    instance-of v0, p1, Ljava/lang/Long;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Ljava/lang/Long;

    .line 6
    .line 7
    invoke-super {p0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    return p1

    .line 12
    :cond_0
    instance-of v0, p1, Ljava/lang/String;

    .line 13
    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryNameToID:Ljava/util/Map;

    .line 17
    .line 18
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    check-cast p1, Ljava/lang/Long;

    .line 23
    .line 24
    invoke-super {p0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    return p1

    .line 29
    :cond_1
    const/4 p1, 0x0

    .line 30
    return p1
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/hpsf/CustomProperty;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/fc/hpsf/CustomProperty;

    .line 6
    .line 7
    invoke-super {p0, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    return p1

    .line 12
    :cond_0
    invoke-super {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-eqz v1, :cond_2

    .line 25
    .line 26
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    check-cast v1, Lcom/intsig/office/fc/hpsf/CustomProperty;

    .line 31
    .line 32
    invoke-virtual {v1}, Lcom/intsig/office/fc/hpsf/Property;->getValue()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    if-ne v1, p1, :cond_1

    .line 37
    .line 38
    const/4 p1, 0x1

    .line 39
    return p1

    .line 40
    :cond_2
    const/4 p1, 0x0

    .line 41
    return p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public get(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryNameToID:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Ljava/lang/Long;

    .line 8
    .line 9
    invoke-super {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    check-cast p1, Lcom/intsig/office/fc/hpsf/CustomProperty;

    .line 14
    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/fc/hpsf/Property;->getValue()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 p1, 0x0

    .line 23
    :goto_0
    return-object p1
    .line 24
.end method

.method public getCodepage()I
    .locals 9

    .line 1
    invoke-virtual {p0}, Ljava/util/AbstractMap;->values()Ljava/util/Collection;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, -0x1

    .line 10
    const/4 v2, -0x1

    .line 11
    :cond_0
    :goto_0
    if-ne v2, v1, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 14
    .line 15
    .line 16
    move-result v3

    .line 17
    if-eqz v3, :cond_1

    .line 18
    .line 19
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v3

    .line 23
    check-cast v3, Lcom/intsig/office/fc/hpsf/CustomProperty;

    .line 24
    .line 25
    invoke-virtual {v3}, Lcom/intsig/office/fc/hpsf/Property;->getID()J

    .line 26
    .line 27
    .line 28
    move-result-wide v4

    .line 29
    const-wide/16 v6, 0x1

    .line 30
    .line 31
    cmp-long v8, v4, v6

    .line 32
    .line 33
    if-nez v8, :cond_0

    .line 34
    .line 35
    invoke-virtual {v3}, Lcom/intsig/office/fc/hpsf/Property;->getValue()Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    check-cast v2, Ljava/lang/Integer;

    .line 40
    .line 41
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    goto :goto_0

    .line 46
    :cond_1
    return v2
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method getDictionary()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryIDToName:Ljava/util/Map;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public idSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryNameToID:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isPure()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->isPure:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public keySet()Ljava/util/Set;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryNameToID:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public nameSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryNameToID:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public put(Ljava/lang/String;Lcom/intsig/office/fc/hpsf/CustomProperty;)Lcom/intsig/office/fc/hpsf/CustomProperty;
    .locals 3

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->isPure:Z

    const/4 p1, 0x0

    return-object p1

    .line 2
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/office/fc/hpsf/CustomProperty;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3
    invoke-virtual {p2}, Lcom/intsig/office/fc/hpsf/Property;->getID()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 4
    iget-object v1, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryNameToID:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 5
    iget-object v2, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryIDToName:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    iget-object v2, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryNameToID:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    iget-object v2, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryIDToName:Ljava/util/Map;

    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    invoke-super {p0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/hpsf/CustomProperty;

    .line 9
    invoke-super {p0, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p1

    .line 10
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Parameter \"name\" ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ") and custom property\'s name ("

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    invoke-virtual {p2}, Lcom/intsig/office/fc/hpsf/CustomProperty;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ") do not match."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public put(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Object;
    .locals 3

    .line 43
    new-instance v0, Lcom/intsig/office/fc/hpsf/MutableProperty;

    invoke-direct {v0}, Lcom/intsig/office/fc/hpsf/MutableProperty;-><init>()V

    const-wide/16 v1, -0x1

    .line 44
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setID(J)V

    const-wide/16 v1, 0xb

    .line 45
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setType(J)V

    .line 46
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setValue(Ljava/lang/Object;)V

    .line 47
    new-instance p2, Lcom/intsig/office/fc/hpsf/CustomProperty;

    invoke-direct {p2, v0, p1}, Lcom/intsig/office/fc/hpsf/CustomProperty;-><init>(Lcom/intsig/office/fc/hpsf/Property;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hpsf/CustomProperties;->put(Lcom/intsig/office/fc/hpsf/CustomProperty;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public put(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/Object;
    .locals 3

    .line 31
    new-instance v0, Lcom/intsig/office/fc/hpsf/MutableProperty;

    invoke-direct {v0}, Lcom/intsig/office/fc/hpsf/MutableProperty;-><init>()V

    const-wide/16 v1, -0x1

    .line 32
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setID(J)V

    const-wide/16 v1, 0x5

    .line 33
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setType(J)V

    .line 34
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setValue(Ljava/lang/Object;)V

    .line 35
    new-instance p2, Lcom/intsig/office/fc/hpsf/CustomProperty;

    invoke-direct {p2, v0, p1}, Lcom/intsig/office/fc/hpsf/CustomProperty;-><init>(Lcom/intsig/office/fc/hpsf/Property;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hpsf/CustomProperties;->put(Lcom/intsig/office/fc/hpsf/CustomProperty;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public put(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Object;
    .locals 3

    .line 37
    new-instance v0, Lcom/intsig/office/fc/hpsf/MutableProperty;

    invoke-direct {v0}, Lcom/intsig/office/fc/hpsf/MutableProperty;-><init>()V

    const-wide/16 v1, -0x1

    .line 38
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setID(J)V

    const-wide/16 v1, 0x3

    .line 39
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setType(J)V

    .line 40
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setValue(Ljava/lang/Object;)V

    .line 41
    new-instance p2, Lcom/intsig/office/fc/hpsf/CustomProperty;

    invoke-direct {p2, v0, p1}, Lcom/intsig/office/fc/hpsf/CustomProperty;-><init>(Lcom/intsig/office/fc/hpsf/Property;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hpsf/CustomProperties;->put(Lcom/intsig/office/fc/hpsf/CustomProperty;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public put(Ljava/lang/String;Ljava/lang/Long;)Ljava/lang/Object;
    .locals 3

    .line 25
    new-instance v0, Lcom/intsig/office/fc/hpsf/MutableProperty;

    invoke-direct {v0}, Lcom/intsig/office/fc/hpsf/MutableProperty;-><init>()V

    const-wide/16 v1, -0x1

    .line 26
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setID(J)V

    const-wide/16 v1, 0x14

    .line 27
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setType(J)V

    .line 28
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setValue(Ljava/lang/Object;)V

    .line 29
    new-instance p2, Lcom/intsig/office/fc/hpsf/CustomProperty;

    invoke-direct {p2, v0, p1}, Lcom/intsig/office/fc/hpsf/CustomProperty;-><init>(Lcom/intsig/office/fc/hpsf/Property;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hpsf/CustomProperties;->put(Lcom/intsig/office/fc/hpsf/CustomProperty;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 3

    .line 19
    new-instance v0, Lcom/intsig/office/fc/hpsf/MutableProperty;

    invoke-direct {v0}, Lcom/intsig/office/fc/hpsf/MutableProperty;-><init>()V

    const-wide/16 v1, -0x1

    .line 20
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setID(J)V

    const-wide/16 v1, 0x1f

    .line 21
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setType(J)V

    .line 22
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setValue(Ljava/lang/Object;)V

    .line 23
    new-instance p2, Lcom/intsig/office/fc/hpsf/CustomProperty;

    invoke-direct {p2, v0, p1}, Lcom/intsig/office/fc/hpsf/CustomProperty;-><init>(Lcom/intsig/office/fc/hpsf/Property;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hpsf/CustomProperties;->put(Lcom/intsig/office/fc/hpsf/CustomProperty;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public put(Ljava/lang/String;Ljava/util/Date;)Ljava/lang/Object;
    .locals 3

    .line 49
    new-instance v0, Lcom/intsig/office/fc/hpsf/MutableProperty;

    invoke-direct {v0}, Lcom/intsig/office/fc/hpsf/MutableProperty;-><init>()V

    const-wide/16 v1, -0x1

    .line 50
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setID(J)V

    const-wide/16 v1, 0x40

    .line 51
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setType(J)V

    .line 52
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setValue(Ljava/lang/Object;)V

    .line 53
    new-instance p2, Lcom/intsig/office/fc/hpsf/CustomProperty;

    invoke-direct {p2, v0, p1}, Lcom/intsig/office/fc/hpsf/CustomProperty;-><init>(Lcom/intsig/office/fc/hpsf/Property;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hpsf/CustomProperties;->put(Lcom/intsig/office/fc/hpsf/CustomProperty;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public remove(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryNameToID:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Long;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    return-object p1

    .line 13
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryIDToName:Ljava/util/Map;

    .line 14
    .line 15
    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->dictionaryNameToID:Ljava/util/Map;

    .line 19
    .line 20
    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    invoke-super {p0, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    return-object p1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setCodepage(I)V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hpsf/MutableProperty;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hpsf/MutableProperty;-><init>()V

    .line 4
    .line 5
    .line 6
    const-wide/16 v1, 0x1

    .line 7
    .line 8
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setID(J)V

    .line 9
    .line 10
    .line 11
    const-wide/16 v1, 0x2

    .line 12
    .line 13
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setType(J)V

    .line 14
    .line 15
    .line 16
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hpsf/MutableProperty;->setValue(Ljava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    new-instance p1, Lcom/intsig/office/fc/hpsf/CustomProperty;

    .line 24
    .line 25
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hpsf/CustomProperty;-><init>(Lcom/intsig/office/fc/hpsf/Property;)V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hpsf/CustomProperties;->put(Lcom/intsig/office/fc/hpsf/CustomProperty;)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setPure(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hpsf/CustomProperties;->isPure:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

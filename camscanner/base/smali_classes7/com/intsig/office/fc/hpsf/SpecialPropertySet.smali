.class public abstract Lcom/intsig/office/fc/hpsf/SpecialPropertySet;
.super Lcom/intsig/office/fc/hpsf/MutablePropertySet;
.source "SpecialPropertySet.java"


# instance fields
.field private delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hpsf/MutablePropertySet;)V
    .locals 0

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/fc/hpsf/MutablePropertySet;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hpsf/PropertySet;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hpsf/MutablePropertySet;-><init>()V

    .line 2
    new-instance v0, Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hpsf/MutablePropertySet;-><init>(Lcom/intsig/office/fc/hpsf/PropertySet;)V

    iput-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    return-void
.end method


# virtual methods
.method public addSection(Lcom/intsig/office/fc/hpsf/Section;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hpsf/MutablePropertySet;->addSection(Lcom/intsig/office/fc/hpsf/Section;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public clearSections()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/MutablePropertySet;->clearSections()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hpsf/PropertySet;->equals(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getByteOrder()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/PropertySet;->getByteOrder()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getClassID()Lcom/intsig/office/fc/hpsf/ClassID;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/PropertySet;->getClassID()Lcom/intsig/office/fc/hpsf/ClassID;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFirstSection()Lcom/intsig/office/fc/hpsf/Section;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/PropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFormat()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/PropertySet;->getFormat()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOSVersion()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/PropertySet;->getOSVersion()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getProperties()[Lcom/intsig/office/fc/hpsf/Property;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hpsf/NoSingleSectionException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/PropertySet;->getProperties()[Lcom/intsig/office/fc/hpsf/Property;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getProperty(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hpsf/NoSingleSectionException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hpsf/PropertySet;->getProperty(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected getPropertyBooleanValue(I)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hpsf/NoSingleSectionException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hpsf/PropertySet;->getPropertyBooleanValue(I)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected getPropertyIntValue(I)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hpsf/NoSingleSectionException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hpsf/PropertySet;->getPropertyIntValue(I)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public abstract getPropertySetIDMap()Lcom/intsig/office/fc/hpsf/PropertyIDMap;
.end method

.method public getSectionCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/PropertySet;->getSectionCount()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSections()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/PropertySet;->getSections()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/PropertySet;->hashCode()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isDocumentSummaryInformation()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/PropertySet;->isDocumentSummaryInformation()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isSummaryInformation()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/PropertySet;->isSummaryInformation()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setByteOrder(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hpsf/MutablePropertySet;->setByteOrder(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setClassID(Lcom/intsig/office/fc/hpsf/ClassID;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hpsf/MutablePropertySet;->setClassID(Lcom/intsig/office/fc/hpsf/ClassID;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFormat(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hpsf/MutablePropertySet;->setFormat(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOSVersion(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hpsf/MutablePropertySet;->setOSVersion(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/intsig/office/fc/hpsf/WritingNotSupportedException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/MutablePropertySet;->toInputStream()Ljava/io/InputStream;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/PropertySet;->toString()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public wasNull()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hpsf/NoSingleSectionException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/PropertySet;->wasNull()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public write(Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hpsf/WritingNotSupportedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hpsf/MutablePropertySet;->write(Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;Ljava/lang/String;)V

    return-void
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hpsf/WritingNotSupportedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->delegate:Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hpsf/MutablePropertySet;->write(Ljava/io/OutputStream;)V

    return-void
.end method

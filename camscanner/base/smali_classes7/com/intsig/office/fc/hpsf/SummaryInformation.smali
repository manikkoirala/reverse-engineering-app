.class public final Lcom/intsig/office/fc/hpsf/SummaryInformation;
.super Lcom/intsig/office/fc/hpsf/SpecialPropertySet;
.source "SummaryInformation.java"


# static fields
.field public static final DEFAULT_STREAM_NAME:Ljava/lang/String; = "\u0005SummaryInformation"


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hpsf/PropertySet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hpsf/UnexpectedPropertySetTypeException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;-><init>(Lcom/intsig/office/fc/hpsf/PropertySet;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->isSummaryInformation()Z

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    new-instance p1, Lcom/intsig/office/fc/hpsf/UnexpectedPropertySetTypeException;

    .line 12
    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    const-string v1, "Not a "

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-class v1, Lcom/intsig/office/fc/hpsf/SummaryInformation;

    .line 24
    .line 25
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hpsf/UnexpectedPropertySetTypeException;-><init>(Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    throw p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public getApplicationName()Ljava/lang/String;
    .locals 1

    .line 1
    const/16 v0, 0x12

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/String;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAuthor()Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    check-cast v0, Ljava/lang/String;

    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCharCount()I
    .locals 1

    .line 1
    const/16 v0, 0x10

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getPropertyIntValue(I)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getComments()Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x6

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    check-cast v0, Ljava/lang/String;

    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCreateDateTime()Ljava/util/Date;
    .locals 1

    .line 1
    const/16 v0, 0xc

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/util/Date;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEditTime()J
    .locals 2

    .line 1
    const/16 v0, 0xa

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/util/Date;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const-wide/16 v0, 0x0

    .line 12
    .line 13
    return-wide v0

    .line 14
    :cond_0
    invoke-static {v0}, Lcom/intsig/office/fc/hpsf/Util;->dateToFileTime(Ljava/util/Date;)J

    .line 15
    .line 16
    .line 17
    move-result-wide v0

    .line 18
    return-wide v0
    .line 19
    .line 20
    .line 21
.end method

.method public getKeywords()Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x5

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    check-cast v0, Ljava/lang/String;

    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLastAuthor()Ljava/lang/String;
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/String;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLastPrinted()Ljava/util/Date;
    .locals 1

    .line 1
    const/16 v0, 0xb

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/util/Date;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLastSaveDateTime()Ljava/util/Date;
    .locals 1

    .line 1
    const/16 v0, 0xd

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/util/Date;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPageCount()I
    .locals 1

    .line 1
    const/16 v0, 0xe

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getPropertyIntValue(I)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPropertySetIDMap()Lcom/intsig/office/fc/hpsf/PropertyIDMap;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hpsf/PropertyIDMap;->getSummaryInformationProperties()Lcom/intsig/office/fc/hpsf/PropertyIDMap;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRevNumber()Ljava/lang/String;
    .locals 1

    .line 1
    const/16 v0, 0x9

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/String;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSecurity()I
    .locals 1

    .line 1
    const/16 v0, 0x13

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getPropertyIntValue(I)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x3

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    check-cast v0, Ljava/lang/String;

    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTemplate()Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x7

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    check-cast v0, Ljava/lang/String;

    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getThumbnail()[B
    .locals 1

    .line 1
    const/16 v0, 0x11

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [B

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    check-cast v0, Ljava/lang/String;

    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getWordCount()I
    .locals 1

    .line 1
    const/16 v0, 0xf

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getPropertyIntValue(I)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeApplicationName()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x12

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeAuthor()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x4

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeCharCount()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x10

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeComments()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x6

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeCreateDateTime()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0xc

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeEditTime()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0xa

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeKeywords()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x5

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeLastAuthor()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x8

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeLastPrinted()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0xb

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeLastSaveDateTime()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0xd

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removePageCount()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0xe

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeRevNumber()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x9

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeSecurity()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x13

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeSubject()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x3

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeTemplate()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x7

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeThumbnail()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x11

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeTitle()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x2

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeWordCount()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0xf

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setApplicationName(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0x12

    .line 8
    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setAuthor(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/4 v1, 0x4

    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCharCount(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0x10

    .line 8
    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(II)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setComments(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/4 v1, 0x6

    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCreateDateTime(Ljava/util/Date;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0xc

    .line 8
    .line 9
    const-wide/16 v2, 0x40

    .line 10
    .line 11
    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(IJLjava/lang/Object;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setEditTime(J)V
    .locals 3

    .line 1
    invoke-static {p1, p2}, Lcom/intsig/office/fc/hpsf/Util;->filetimeToDate(J)Ljava/util/Date;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 6
    .line 7
    .line 8
    move-result-object p2

    .line 9
    check-cast p2, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 10
    .line 11
    const/16 v0, 0xa

    .line 12
    .line 13
    const-wide/16 v1, 0x40

    .line 14
    .line 15
    invoke-virtual {p2, v0, v1, v2, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(IJLjava/lang/Object;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setKeywords(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/4 v1, 0x5

    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLastAuthor(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0x8

    .line 8
    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLastPrinted(Ljava/util/Date;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0xb

    .line 8
    .line 9
    const-wide/16 v2, 0x40

    .line 10
    .line 11
    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(IJLjava/lang/Object;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLastSaveDateTime(Ljava/util/Date;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0xd

    .line 8
    .line 9
    const-wide/16 v2, 0x40

    .line 10
    .line 11
    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(IJLjava/lang/Object;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPageCount(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0xe

    .line 8
    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(II)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRevNumber(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0x9

    .line 8
    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSecurity(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0x13

    .line 8
    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(II)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/4 v1, 0x3

    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTemplate(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/4 v1, 0x7

    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setThumbnail([B)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0x11

    .line 8
    .line 9
    const-wide/16 v2, 0x1e

    .line 10
    .line 11
    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(IJLjava/lang/Object;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/4 v1, 0x2

    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setWordCount(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0xf

    .line 8
    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(II)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

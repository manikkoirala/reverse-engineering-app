.class public Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;
.super Lcom/intsig/office/fc/hpsf/SpecialPropertySet;
.source "DocumentSummaryInformation.java"


# static fields
.field public static final DEFAULT_STREAM_NAME:Ljava/lang/String; = "\u0005DocumentSummaryInformation"


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hpsf/PropertySet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hpsf/UnexpectedPropertySetTypeException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;-><init>(Lcom/intsig/office/fc/hpsf/PropertySet;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->isDocumentSummaryInformation()Z

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    new-instance p1, Lcom/intsig/office/fc/hpsf/UnexpectedPropertySetTypeException;

    .line 12
    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    const-string v1, "Not a "

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hpsf/UnexpectedPropertySetTypeException;-><init>(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    throw p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private ensureSection2()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getSectionCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x2

    .line 6
    if-ge v0, v1, :cond_0

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/office/fc/hpsf/MutableSection;-><init>()V

    .line 11
    .line 12
    .line 13
    sget-object v1, Lcom/intsig/office/fc/hpsf/SectionIDMap;->DOCUMENT_SUMMARY_INFORMATION_ID:[[B

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    aget-object v1, v1, v2

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setFormatID([B)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->addSection(Lcom/intsig/office/fc/hpsf/Section;)V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private notYetImplemented(Ljava/lang/String;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    const-string p1, " is not yet implemented."

    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    throw v0
    .line 24
.end method


# virtual methods
.method public getByteCount()I
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getPropertyIntValue(I)I

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    check-cast v0, Ljava/lang/String;

    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCompany()Ljava/lang/String;
    .locals 1

    .line 1
    const/16 v0, 0xf

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/String;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCustomProperties()Lcom/intsig/office/fc/hpsf/CustomProperties;
    .locals 12

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getSectionCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x2

    .line 6
    if-lt v0, v1, :cond_2

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/office/fc/hpsf/CustomProperties;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/office/fc/hpsf/CustomProperties;-><init>()V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getSections()Ljava/util/List;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const/4 v2, 0x1

    .line 18
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Lcom/intsig/office/fc/hpsf/Section;

    .line 23
    .line 24
    invoke-virtual {v1}, Lcom/intsig/office/fc/hpsf/Section;->getDictionary()Ljava/util/Map;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-virtual {v1}, Lcom/intsig/office/fc/hpsf/Section;->getProperties()[Lcom/intsig/office/fc/hpsf/Property;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    const/4 v3, 0x0

    .line 33
    const/4 v4, 0x0

    .line 34
    const/4 v5, 0x0

    .line 35
    :goto_0
    array-length v6, v1

    .line 36
    if-ge v4, v6, :cond_1

    .line 37
    .line 38
    aget-object v6, v1, v4

    .line 39
    .line 40
    invoke-virtual {v6}, Lcom/intsig/office/fc/hpsf/Property;->getID()J

    .line 41
    .line 42
    .line 43
    move-result-wide v7

    .line 44
    const-wide/16 v9, 0x0

    .line 45
    .line 46
    cmp-long v11, v7, v9

    .line 47
    .line 48
    if-eqz v11, :cond_0

    .line 49
    .line 50
    const-wide/16 v9, 0x1

    .line 51
    .line 52
    cmp-long v11, v7, v9

    .line 53
    .line 54
    if-eqz v11, :cond_0

    .line 55
    .line 56
    add-int/lit8 v5, v5, 0x1

    .line 57
    .line 58
    new-instance v9, Lcom/intsig/office/fc/hpsf/CustomProperty;

    .line 59
    .line 60
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 61
    .line 62
    .line 63
    move-result-object v7

    .line 64
    invoke-interface {v2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    .line 66
    .line 67
    move-result-object v7

    .line 68
    check-cast v7, Ljava/lang/String;

    .line 69
    .line 70
    invoke-direct {v9, v6, v7}, Lcom/intsig/office/fc/hpsf/CustomProperty;-><init>(Lcom/intsig/office/fc/hpsf/Property;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v9}, Lcom/intsig/office/fc/hpsf/CustomProperty;->getName()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v6

    .line 77
    invoke-virtual {v0, v6, v9}, Lcom/intsig/office/fc/hpsf/CustomProperties;->put(Ljava/lang/String;Lcom/intsig/office/fc/hpsf/CustomProperty;)Lcom/intsig/office/fc/hpsf/CustomProperty;

    .line 78
    .line 79
    .line 80
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_1
    invoke-virtual {v0}, Ljava/util/AbstractMap;->size()I

    .line 84
    .line 85
    .line 86
    move-result v1

    .line 87
    if-eq v1, v5, :cond_3

    .line 88
    .line 89
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hpsf/CustomProperties;->setPure(Z)V

    .line 90
    .line 91
    .line 92
    goto :goto_1

    .line 93
    :cond_2
    const/4 v0, 0x0

    .line 94
    :cond_3
    :goto_1
    return-object v0
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public getDocparts()[B
    .locals 1

    .line 1
    const-string v0, "Reading byte arrays"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;->notYetImplemented(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/16 v0, 0xd

    .line 7
    .line 8
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, [B

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHeadingPair()[B
    .locals 1

    .line 1
    const-string v0, "Reading byte arrays "

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;->notYetImplemented(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/16 v0, 0xc

    .line 7
    .line 8
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, [B

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHiddenCount()I
    .locals 1

    .line 1
    const/16 v0, 0x9

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getPropertyIntValue(I)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLineCount()I
    .locals 1

    .line 1
    const/4 v0, 0x5

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getPropertyIntValue(I)I

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLinksDirty()Z
    .locals 1

    .line 1
    const/16 v0, 0x10

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getPropertyBooleanValue(I)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMMClipCount()I
    .locals 1

    .line 1
    const/16 v0, 0xa

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getPropertyIntValue(I)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getManager()Ljava/lang/String;
    .locals 1

    .line 1
    const/16 v0, 0xe

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/String;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNoteCount()I
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getPropertyIntValue(I)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getParCount()I
    .locals 1

    .line 1
    const/4 v0, 0x6

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getPropertyIntValue(I)I

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPresentationFormat()Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x3

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getProperty(I)Ljava/lang/Object;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    check-cast v0, Ljava/lang/String;

    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPropertySetIDMap()Lcom/intsig/office/fc/hpsf/PropertyIDMap;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hpsf/PropertyIDMap;->getDocumentSummaryInformationProperties()Lcom/intsig/office/fc/hpsf/PropertyIDMap;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getScale()Z
    .locals 1

    .line 1
    const/16 v0, 0xb

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getPropertyBooleanValue(I)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideCount()I
    .locals 1

    .line 1
    const/4 v0, 0x7

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getPropertyIntValue(I)I

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeByteCount()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x4

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeCategory()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x2

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeCompany()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0xf

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeCustomProperties()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getSectionCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x2

    .line 6
    if-lt v0, v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getSections()Ljava/util/List;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v1, 0x1

    .line 13
    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hpsf/HPSFRuntimeException;

    .line 18
    .line 19
    const-string v1, "Illegal internal format of Document SummaryInformation stream: second section is missing."

    .line 20
    .line 21
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hpsf/HPSFRuntimeException;-><init>(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    throw v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public removeDocparts()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0xd

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeHeadingPair()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0xc

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeHiddenCount()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x9

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeLineCount()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x5

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeLinksDirty()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x10

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeMMClipCount()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0xa

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeManager()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0xe

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeNoteCount()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x8

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeParCount()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x6

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removePresentationFormat()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x3

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeScale()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0xb

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeSlideCount()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const-wide/16 v1, 0x7

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->removeProperty(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setByteCount(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/4 v1, 0x4

    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(II)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/4 v1, 0x2

    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCompany(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0xf

    .line 8
    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCustomProperties(Lcom/intsig/office/fc/hpsf/CustomProperties;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;->ensureSection2()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getSections()Ljava/util/List;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    const/4 v1, 0x1

    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/office/fc/hpsf/CustomProperties;->getDictionary()Ljava/util/Map;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/MutableSection;->clear()V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/office/fc/hpsf/CustomProperties;->getCodepage()I

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    if-gez v2, :cond_0

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/office/fc/hpsf/Section;->getCodepage()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    :cond_0
    if-gez v2, :cond_1

    .line 33
    .line 34
    const/16 v2, 0x4b0

    .line 35
    .line 36
    :cond_1
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hpsf/CustomProperties;->setCodepage(I)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hpsf/MutableSection;->setCodepage(I)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setDictionary(Ljava/util/Map;)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {p1}, Ljava/util/AbstractMap;->values()Ljava/util/Collection;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 54
    .line 55
    .line 56
    move-result v1

    .line 57
    if-eqz v1, :cond_2

    .line 58
    .line 59
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    check-cast v1, Lcom/intsig/office/fc/hpsf/Property;

    .line 64
    .line 65
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(Lcom/intsig/office/fc/hpsf/Property;)V

    .line 66
    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_2
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setDocparts([B)V
    .locals 0

    .line 1
    const-string p1, "Writing byte arrays"

    .line 2
    .line 3
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;->notYetImplemented(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setHeadingPair([B)V
    .locals 0

    .line 1
    const-string p1, "Writing byte arrays "

    .line 2
    .line 3
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;->notYetImplemented(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setHiddenCount(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getSections()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 11
    .line 12
    const/16 v1, 0x9

    .line 13
    .line 14
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(II)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLineCount(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/4 v1, 0x5

    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(II)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLinksDirty(Z)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0x10

    .line 8
    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(IZ)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMMClipCount(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0xa

    .line 8
    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(II)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setManager(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0xe

    .line 8
    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setNoteCount(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0x8

    .line 8
    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(II)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setParCount(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/4 v1, 0x6

    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(II)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPresentationFormat(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/4 v1, 0x3

    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setScale(Z)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/16 v1, 0xb

    .line 8
    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(IZ)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSlideCount(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hpsf/SpecialPropertySet;->getFirstSection()Lcom/intsig/office/fc/hpsf/Section;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hpsf/MutableSection;

    .line 6
    .line 7
    const/4 v1, 0x7

    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hpsf/MutableSection;->setProperty(II)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public final Lcom/intsig/office/fc/ddf/EscherDggRecord;
.super Lcom/intsig/office/fc/ddf/EscherRecord;
.source "EscherDggRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;
    }
.end annotation


# static fields
.field private static final MY_COMP:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;",
            ">;"
        }
    .end annotation
.end field

.field public static final RECORD_DESCRIPTION:Ljava/lang/String; = "MsofbtDgg"

.field public static final RECORD_ID:S = -0xffas


# instance fields
.field private field_1_shapeIdMax:I

.field private field_3_numShapesSaved:I

.field private field_4_drawingsSaved:I

.field private field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

.field private maxDgId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherDggRecord$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/ddf/EscherDggRecord$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->MY_COMP:Ljava/util/Comparator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/ddf/EscherRecord;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public addCluster(II)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, p1, p2, v0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->addCluster(IIZ)V

    return-void
.end method

.method public addCluster(IIZ)V
    .locals 2

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 3
    new-instance v1, Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    invoke-direct {v1, p1, p2}, Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz p3, :cond_0

    .line 4
    sget-object p2, Lcom/intsig/office/fc/ddf/EscherDggRecord;->MY_COMP:Ljava/util/Comparator;

    invoke-static {v0, p2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 5
    :cond_0
    iget p2, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->maxDgId:I

    invoke-static {p2, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->maxDgId:I

    .line 6
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    iput-object p1, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    return-void
.end method

.method public dispose()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public fillFields([BILcom/intsig/office/fc/ddf/EscherRecordFactory;)I
    .locals 6

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/ddf/EscherRecord;->readHeader([BI)I

    .line 2
    .line 3
    .line 4
    move-result p3

    .line 5
    add-int/lit8 p2, p2, 0x8

    .line 6
    .line 7
    add-int/lit8 v0, p2, 0x0

    .line 8
    .line 9
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iput v0, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_1_shapeIdMax:I

    .line 14
    .line 15
    add-int/lit8 v0, p2, 0x4

    .line 16
    .line 17
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 18
    .line 19
    .line 20
    add-int/lit8 v0, p2, 0x8

    .line 21
    .line 22
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    iput v0, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_3_numShapesSaved:I

    .line 27
    .line 28
    add-int/lit8 v0, p2, 0xc

    .line 29
    .line 30
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    iput v0, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_4_drawingsSaved:I

    .line 35
    .line 36
    add-int/lit8 v0, p3, -0x10

    .line 37
    .line 38
    div-int/lit8 v0, v0, 0x8

    .line 39
    .line 40
    new-array v0, v0, [Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 41
    .line 42
    iput-object v0, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 43
    .line 44
    const/4 v0, 0x0

    .line 45
    const/16 v1, 0x10

    .line 46
    .line 47
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 48
    .line 49
    array-length v3, v2

    .line 50
    if-ge v0, v3, :cond_0

    .line 51
    .line 52
    new-instance v3, Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 53
    .line 54
    add-int v4, p2, v1

    .line 55
    .line 56
    invoke-static {p1, v4}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 57
    .line 58
    .line 59
    move-result v5

    .line 60
    add-int/lit8 v4, v4, 0x4

    .line 61
    .line 62
    invoke-static {p1, v4}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 63
    .line 64
    .line 65
    move-result v4

    .line 66
    invoke-direct {v3, v5, v4}, Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;-><init>(II)V

    .line 67
    .line 68
    .line 69
    aput-object v3, v2, v0

    .line 70
    .line 71
    iget v2, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->maxDgId:I

    .line 72
    .line 73
    iget-object v3, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 74
    .line 75
    aget-object v3, v3, v0

    .line 76
    .line 77
    invoke-virtual {v3}, Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;->getDrawingGroupId()I

    .line 78
    .line 79
    .line 80
    move-result v3

    .line 81
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    .line 82
    .line 83
    .line 84
    move-result v2

    .line 85
    iput v2, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->maxDgId:I

    .line 86
    .line 87
    add-int/lit8 v1, v1, 0x8

    .line 88
    .line 89
    add-int/lit8 v0, v0, 0x1

    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_0
    sub-int/2addr p3, v1

    .line 93
    if-nez p3, :cond_1

    .line 94
    .line 95
    add-int/lit8 v1, v1, 0x8

    .line 96
    .line 97
    add-int/2addr v1, p3

    .line 98
    return v1

    .line 99
    :cond_1
    new-instance p1, Lcom/intsig/office/fc/util/RecordFormatException;

    .line 100
    .line 101
    new-instance p2, Ljava/lang/StringBuilder;

    .line 102
    .line 103
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .line 105
    .line 106
    const-string v0, "Expecting no remaining data but got "

    .line 107
    .line 108
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    const-string p3, " byte(s)."

    .line 115
    .line 116
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object p2

    .line 123
    invoke-direct {p1, p2}, Lcom/intsig/office/fc/util/RecordFormatException;-><init>(Ljava/lang/String;)V

    .line 124
    .line 125
    .line 126
    throw p1
    .line 127
    .line 128
    .line 129
.end method

.method public getDrawingsSaved()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_4_drawingsSaved:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFileIdClusters()[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMaxDrawingGroupId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->maxDgId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNumIdClusters()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    array-length v0, v0

    .line 8
    add-int/lit8 v0, v0, 0x1

    .line 9
    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNumShapesSaved()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_3_numShapesSaved:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordId()S
    .locals 1

    .line 1
    const/16 v0, -0xffa

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordName()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "Dgg"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordSize()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    mul-int/lit8 v0, v0, 0x8

    .line 5
    .line 6
    add-int/lit8 v0, v0, 0x18

    .line 7
    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShapeIdMax()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_1_shapeIdMax:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public serialize(I[BLcom/intsig/office/fc/ddf/EscherSerializationListener;)I
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getRecordId()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-interface {p3, p1, v0, p0}, Lcom/intsig/office/fc/ddf/EscherSerializationListener;->beforeRecordSerialize(ISLcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherRecord;->getOptions()S

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-static {p2, p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 13
    .line 14
    .line 15
    add-int/lit8 p1, p1, 0x2

    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getRecordId()S

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    invoke-static {p2, p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 22
    .line 23
    .line 24
    add-int/lit8 p1, p1, 0x2

    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getRecordSize()I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    add-int/lit8 v0, v0, -0x8

    .line 31
    .line 32
    invoke-static {p2, p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 33
    .line 34
    .line 35
    add-int/lit8 p1, p1, 0x4

    .line 36
    .line 37
    iget v0, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_1_shapeIdMax:I

    .line 38
    .line 39
    invoke-static {p2, p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 40
    .line 41
    .line 42
    add-int/lit8 p1, p1, 0x4

    .line 43
    .line 44
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getNumIdClusters()I

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    invoke-static {p2, p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 49
    .line 50
    .line 51
    add-int/lit8 p1, p1, 0x4

    .line 52
    .line 53
    iget v0, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_3_numShapesSaved:I

    .line 54
    .line 55
    invoke-static {p2, p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 56
    .line 57
    .line 58
    add-int/lit8 p1, p1, 0x4

    .line 59
    .line 60
    iget v0, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_4_drawingsSaved:I

    .line 61
    .line 62
    invoke-static {p2, p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 63
    .line 64
    .line 65
    add-int/lit8 p1, p1, 0x4

    .line 66
    .line 67
    const/4 v0, 0x0

    .line 68
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 69
    .line 70
    array-length v2, v1

    .line 71
    if-ge v0, v2, :cond_0

    .line 72
    .line 73
    aget-object v1, v1, v0

    .line 74
    .line 75
    invoke-static {v1}, Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;->〇080(Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;)I

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    invoke-static {p2, p1, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 80
    .line 81
    .line 82
    add-int/lit8 p1, p1, 0x4

    .line 83
    .line 84
    iget-object v1, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 85
    .line 86
    aget-object v1, v1, v0

    .line 87
    .line 88
    invoke-static {v1}, Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;->〇o00〇〇Oo(Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;)I

    .line 89
    .line 90
    .line 91
    move-result v1

    .line 92
    invoke-static {p2, p1, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 93
    .line 94
    .line 95
    add-int/lit8 p1, p1, 0x4

    .line 96
    .line 97
    add-int/lit8 v0, v0, 0x1

    .line 98
    .line 99
    goto :goto_0

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getRecordId()S

    .line 101
    .line 102
    .line 103
    move-result p2

    .line 104
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getRecordSize()I

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    invoke-interface {p3, p1, p2, v0, p0}, Lcom/intsig/office/fc/ddf/EscherSerializationListener;->afterRecordSerialize(ISILcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 109
    .line 110
    .line 111
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getRecordSize()I

    .line 112
    .line 113
    .line 114
    move-result p1

    .line 115
    return p1
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public setDrawingsSaved(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_4_drawingsSaved:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFileIdClusters([Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMaxDrawingGroupId(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->maxDgId:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setNumShapesSaved(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_3_numShapesSaved:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setShapeIdMax(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_1_shapeIdMax:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 7
    .line 8
    const/16 v2, 0xa

    .line 9
    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    :goto_0
    iget-object v3, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 14
    .line 15
    array-length v3, v3

    .line 16
    if-ge v1, v3, :cond_0

    .line 17
    .line 18
    const-string v3, "  DrawingGroupId"

    .line 19
    .line 20
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 21
    .line 22
    .line 23
    add-int/lit8 v3, v1, 0x1

    .line 24
    .line 25
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 26
    .line 27
    .line 28
    const-string v4, ": "

    .line 29
    .line 30
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 31
    .line 32
    .line 33
    iget-object v5, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 34
    .line 35
    aget-object v5, v5, v1

    .line 36
    .line 37
    invoke-static {v5}, Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;->〇080(Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;)I

    .line 38
    .line 39
    .line 40
    move-result v5

    .line 41
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 45
    .line 46
    .line 47
    const-string v5, "  NumShapeIdsUsed"

    .line 48
    .line 49
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 56
    .line 57
    .line 58
    iget-object v4, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_5_fileIdClusters:[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 59
    .line 60
    aget-object v1, v4, v1

    .line 61
    .line 62
    invoke-static {v1}, Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;->〇o00〇〇Oo(Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;)I

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 70
    .line 71
    .line 72
    move v1, v3

    .line 73
    goto :goto_0

    .line 74
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 75
    .line 76
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .line 78
    .line 79
    const-class v3, Lcom/intsig/office/fc/ddf/EscherDggRecord;

    .line 80
    .line 81
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v3

    .line 85
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    const-string v3, ":"

    .line 89
    .line 90
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    const-string v3, "  RecordId: 0x"

    .line 97
    .line 98
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    const/16 v3, -0xffa

    .line 102
    .line 103
    invoke-static {v3}, Lcom/intsig/office/fc/util/HexDump;->toHex(S)Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v3

    .line 107
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    const-string v3, "  Options: 0x"

    .line 114
    .line 115
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherRecord;->getOptions()S

    .line 119
    .line 120
    .line 121
    move-result v3

    .line 122
    invoke-static {v3}, Lcom/intsig/office/fc/util/HexDump;->toHex(S)Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v3

    .line 126
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    const-string v3, "  ShapeIdMax: "

    .line 133
    .line 134
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    iget v3, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_1_shapeIdMax:I

    .line 138
    .line 139
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 140
    .line 141
    .line 142
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    const-string v3, "  NumIdClusters: "

    .line 146
    .line 147
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    .line 149
    .line 150
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getNumIdClusters()I

    .line 151
    .line 152
    .line 153
    move-result v3

    .line 154
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 155
    .line 156
    .line 157
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 158
    .line 159
    .line 160
    const-string v3, "  NumShapesSaved: "

    .line 161
    .line 162
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    .line 164
    .line 165
    iget v3, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_3_numShapesSaved:I

    .line 166
    .line 167
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 168
    .line 169
    .line 170
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 171
    .line 172
    .line 173
    const-string v3, "  DrawingsSaved: "

    .line 174
    .line 175
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    iget v3, p0, Lcom/intsig/office/fc/ddf/EscherDggRecord;->field_4_drawingsSaved:I

    .line 179
    .line 180
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 181
    .line 182
    .line 183
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 184
    .line 185
    .line 186
    const-string v2, ""

    .line 187
    .line 188
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    .line 190
    .line 191
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 192
    .line 193
    .line 194
    move-result-object v0

    .line 195
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object v0

    .line 202
    return-object v0
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.class public final Lcom/intsig/office/fc/ddf/EscherProperties;
.super Ljava/lang/Object;
.source "EscherProperties.java"


# static fields
.field public static final BLIP__BLIPFILENAME:S = 0x105s

.field public static final BLIP__BLIPFLAGS:S = 0x106s

.field public static final BLIP__BLIPTODISPLAY:S = 0x104s

.field public static final BLIP__BRIGHTNESSSETTING:S = 0x109s

.field public static final BLIP__CONTRASTSETTING:S = 0x108s

.field public static final BLIP__CROPFROMBOTTOM:S = 0x101s

.field public static final BLIP__CROPFROMLEFT:S = 0x102s

.field public static final BLIP__CROPFROMRIGHT:S = 0x103s

.field public static final BLIP__CROPFROMTOP:S = 0x100s

.field public static final BLIP__DOUBLEMOD:S = 0x10cs

.field public static final BLIP__GAMMA:S = 0x10as

.field public static final BLIP__NOHITTESTPICTURE:S = 0x13cs

.field public static final BLIP__PICTUREACTIVE:S = 0x13fs

.field public static final BLIP__PICTUREBILEVEL:S = 0x13es

.field public static final BLIP__PICTUREFILLMOD:S = 0x10ds

.field public static final BLIP__PICTUREGRAY:S = 0x13ds

.field public static final BLIP__PICTUREID:S = 0x10bs

.field public static final BLIP__PICTURELINE:S = 0x10es

.field public static final BLIP__PRINTBLIP:S = 0x10fs

.field public static final BLIP__PRINTBLIPFILENAME:S = 0x110s

.field public static final BLIP__PRINTFLAGS:S = 0x111s

.field public static final BLIP__TRANSPARENTCOLOR:S = 0x107s

.field public static final CALLOUT__CALLOUTACCENTBAR:S = 0x37as

.field public static final CALLOUT__CALLOUTANGLE:S = 0x342s

.field public static final CALLOUT__CALLOUTDROPSPECIFIED:S = 0x344s

.field public static final CALLOUT__CALLOUTDROPTYPE:S = 0x343s

.field public static final CALLOUT__CALLOUTLENGTHSPECIFIED:S = 0x345s

.field public static final CALLOUT__CALLOUTMINUSX:S = 0x37cs

.field public static final CALLOUT__CALLOUTMINUSY:S = 0x37ds

.field public static final CALLOUT__CALLOUTTEXTBORDER:S = 0x37bs

.field public static final CALLOUT__CALLOUTTYPE:S = 0x340s

.field public static final CALLOUT__DROPAUTO:S = 0x37es

.field public static final CALLOUT__ISCALLOUT:S = 0x379s

.field public static final CALLOUT__LENGTHSPECIFIED:S = 0x37fs

.field public static final CALLOUT__XYCALLOUTGAP:S = 0x341s

.field public static final FILL__ANGLE:S = 0x18bs

.field public static final FILL__BACKOPACITY:S = 0x184s

.field public static final FILL__BLIPFILENAME:S = 0x187s

.field public static final FILL__BLIPFLAGS:S = 0x188s

.field public static final FILL__CRMOD:S = 0x185s

.field public static final FILL__DZTYPE:S = 0x195s

.field public static final FILL__FILLBACKCOLOR:S = 0x183s

.field public static final FILL__FILLCOLOR:S = 0x181s

.field public static final FILL__FILLED:S = 0x1bbs

.field public static final FILL__FILLOPACITY:S = 0x182s

.field public static final FILL__FILLTYPE:S = 0x180s

.field public static final FILL__FOCUS:S = 0x18cs

.field public static final FILL__HEIGHT:S = 0x18as

.field public static final FILL__HITTESTFILL:S = 0x1bcs

.field public static final FILL__NOFILLHITTEST:S = 0x1bfs

.field public static final FILL__ORIGINX:S = 0x198s

.field public static final FILL__ORIGINY:S = 0x199s

.field public static final FILL__PATTERNTEXTURE:S = 0x186s

.field public static final FILL__RECTBOTTOM:S = 0x194s

.field public static final FILL__RECTLEFT:S = 0x191s

.field public static final FILL__RECTRIGHT:S = 0x193s

.field public static final FILL__RECTTOP:S = 0x192s

.field public static final FILL__SHADECOLORS:S = 0x197s

.field public static final FILL__SHADEPRESET:S = 0x196s

.field public static final FILL__SHADETYPE:S = 0x19cs

.field public static final FILL__SHAPE:S = 0x1bds

.field public static final FILL__SHAPEORIGINX:S = 0x19as

.field public static final FILL__SHAPEORIGINY:S = 0x19bs

.field public static final FILL__TOBOTTOM:S = 0x190s

.field public static final FILL__TOLEFT:S = 0x18ds

.field public static final FILL__TORIGHT:S = 0x18fs

.field public static final FILL__TOTOP:S = 0x18es

.field public static final FILL__USERECT:S = 0x1bes

.field public static final FILL__WIDTH:S = 0x189s

.field public static final GEOMETRY__3DOK:S = 0x17bs

.field public static final GEOMETRY__ADJUST10VALUE:S = 0x150s

.field public static final GEOMETRY__ADJUST2VALUE:S = 0x148s

.field public static final GEOMETRY__ADJUST3VALUE:S = 0x149s

.field public static final GEOMETRY__ADJUST4VALUE:S = 0x14as

.field public static final GEOMETRY__ADJUST5VALUE:S = 0x14bs

.field public static final GEOMETRY__ADJUST6VALUE:S = 0x14cs

.field public static final GEOMETRY__ADJUST7VALUE:S = 0x14ds

.field public static final GEOMETRY__ADJUST8VALUE:S = 0x14es

.field public static final GEOMETRY__ADJUST9VALUE:S = 0x14fs

.field public static final GEOMETRY__ADJUSTVALUE:S = 0x147s

.field public static final GEOMETRY__BOTTOM:S = 0x143s

.field public static final GEOMETRY__FILLOK:S = 0x17fs

.field public static final GEOMETRY__FILLSHADESHAPEOK:S = 0x17es

.field public static final GEOMETRY__GEOTEXTOK:S = 0x17ds

.field public static final GEOMETRY__LEFT:S = 0x140s

.field public static final GEOMETRY__LINEOK:S = 0x17cs

.field public static final GEOMETRY__RIGHT:S = 0x142s

.field public static final GEOMETRY__SEGMENTINFO:S = 0x146s

.field public static final GEOMETRY__SHADOWok:S = 0x17as

.field public static final GEOMETRY__SHAPEPATH:S = 0x144s

.field public static final GEOMETRY__TOP:S = 0x141s

.field public static final GEOMETRY__VERTICES:S = 0x145s

.field public static final GEOTEXT__ALIGNMENTONCURVE:S = 0xc2s

.field public static final GEOTEXT__BOLDFONT:S = 0xfas

.field public static final GEOTEXT__CHARBOUNDINGBOX:S = 0xf6s

.field public static final GEOTEXT__DEFAULTPOINTSIZE:S = 0xc3s

.field public static final GEOTEXT__FONTFAMILYNAME:S = 0xc5s

.field public static final GEOTEXT__HASTEXTEFFECT:S = 0xf1s

.field public static final GEOTEXT__ITALICFONT:S = 0xfbs

.field public static final GEOTEXT__KERNCHARACTERS:S = 0xf3s

.field public static final GEOTEXT__NOMEASUREALONGPATH:S = 0xf9s

.field public static final GEOTEXT__REVERSEROWORDER:S = 0xf0s

.field public static final GEOTEXT__ROTATECHARACTERS:S = 0xf2s

.field public static final GEOTEXT__RTFTEXT:S = 0xc1s

.field public static final GEOTEXT__SCALETEXTONPATH:S = 0xf7s

.field public static final GEOTEXT__SHADOWFONT:S = 0xfds

.field public static final GEOTEXT__SMALLCAPSFONT:S = 0xfes

.field public static final GEOTEXT__STRETCHCHARHEIGHT:S = 0xf8s

.field public static final GEOTEXT__STRETCHTOFITSHAPE:S = 0xf5s

.field public static final GEOTEXT__STRIKETHROUGHFONT:S = 0xffs

.field public static final GEOTEXT__TEXTSPACING:S = 0xc4s

.field public static final GEOTEXT__TIGHTORTRACK:S = 0xf4s

.field public static final GEOTEXT__UNDERLINEFONT:S = 0xfcs

.field public static final GEOTEXT__UNICODE:S = 0xc0s

.field public static final GROUPSHAPE__1DADJUSTMENT:S = 0x3bds

.field public static final GROUPSHAPE__BEHINDDOCUMENT:S = 0x3bas

.field public static final GROUPSHAPE__BORDERBOTTOMCOLOR:S = 0x39ds

.field public static final GROUPSHAPE__BORDERLEFTCOLOR:S = 0x39cs

.field public static final GROUPSHAPE__BORDERRIGHTCOLOR:S = 0x39es

.field public static final GROUPSHAPE__BORDERTOPCOLOR:S = 0x39bs

.field public static final GROUPSHAPE__DESCRIPTION:S = 0x381s

.field public static final GROUPSHAPE__EDITEDWRAP:S = 0x3b9s

.field public static final GROUPSHAPE__FLAGS:S = 0x3bfs

.field public static final GROUPSHAPE__HIDDEN:S = 0x3bes

.field public static final GROUPSHAPE__HR_ALIGN:S = 0x394s

.field public static final GROUPSHAPE__HR_HEIGHT:S = 0x395s

.field public static final GROUPSHAPE__HR_PCT:S = 0x393s

.field public static final GROUPSHAPE__HR_WIDTH:S = 0x396s

.field public static final GROUPSHAPE__HYPERLINK:S = 0x382s

.field public static final GROUPSHAPE__ISBUTTON:S = 0x3bcs

.field public static final GROUPSHAPE__METROBLOB:S = 0x3a9s

.field public static final GROUPSHAPE__ONDBLCLICKNOTIFY:S = 0x3bbs

.field public static final GROUPSHAPE__POSH:S = 0x38fs

.field public static final GROUPSHAPE__POSRELH:S = 0x390s

.field public static final GROUPSHAPE__POSRELV:S = 0x392s

.field public static final GROUPSHAPE__POSV:S = 0x391s

.field public static final GROUPSHAPE__PRINT:S = 0x3bfs

.field public static final GROUPSHAPE__REGROUPID:S = 0x388s

.field public static final GROUPSHAPE__SCRIPT:S = 0x38es

.field public static final GROUPSHAPE__SCRIPTEXT:S = 0x397s

.field public static final GROUPSHAPE__SCRIPTLANG:S = 0x398s

.field public static final GROUPSHAPE__SHAPENAME:S = 0x380s

.field public static final GROUPSHAPE__TABLEPROPERTIES:S = 0x39fs

.field public static final GROUPSHAPE__TABLEROWPROPERTIES:S = 0x3a0s

.field public static final GROUPSHAPE__TOOLTIP:S = 0x38ds

.field public static final GROUPSHAPE__UNUSED906:S = 0x38as

.field public static final GROUPSHAPE__WEBBOT:S = 0x3a5s

.field public static final GROUPSHAPE__WRAPDISTBOTTOM:S = 0x387s

.field public static final GROUPSHAPE__WRAPDISTLEFT:S = 0x384s

.field public static final GROUPSHAPE__WRAPDISTRIGHT:S = 0x386s

.field public static final GROUPSHAPE__WRAPDISTTOP:S = 0x385s

.field public static final GROUPSHAPE__WRAPPOLYGONVERTICES:S = 0x383s

.field public static final GROUPSHAPE__ZORDER:S = 0x3aas

.field public static final LINESTYLE__ANYLINE:S = 0x1fcs

.field public static final LINESTYLE__ARROWHEADSOK:S = 0x1fbs

.field public static final LINESTYLE__BACKCOLOR:S = 0x1c2s

.field public static final LINESTYLE__COLOR:S = 0x1c0s

.field public static final LINESTYLE__CRMOD:S = 0x1c3s

.field public static final LINESTYLE__FILLBLIP:S = 0x1c5s

.field public static final LINESTYLE__FILLBLIPFLAGS:S = 0x1c7s

.field public static final LINESTYLE__FILLBLIPNAME:S = 0x1c6s

.field public static final LINESTYLE__FILLDZTYPE:S = 0x1cas

.field public static final LINESTYLE__FILLHEIGHT:S = 0x1c9s

.field public static final LINESTYLE__FILLWIDTH:S = 0x1c8s

.field public static final LINESTYLE__HITLINETEST:S = 0x1fds

.field public static final LINESTYLE__LINEDASHING:S = 0x1ces

.field public static final LINESTYLE__LINEDASHSTYLE:S = 0x1cfs

.field public static final LINESTYLE__LINEENDARROWHEAD:S = 0x1d1s

.field public static final LINESTYLE__LINEENDARROWLENGTH:S = 0x1d5s

.field public static final LINESTYLE__LINEENDARROWWIDTH:S = 0x1d4s

.field public static final LINESTYLE__LINEENDCAPSTYLE:S = 0x1d7s

.field public static final LINESTYLE__LINEESTARTARROWLENGTH:S = 0x1d3s

.field public static final LINESTYLE__LINEFILLSHAPE:S = 0x1fes

.field public static final LINESTYLE__LINEJOINSTYLE:S = 0x1d6s

.field public static final LINESTYLE__LINEMITERLIMIT:S = 0x1ccs

.field public static final LINESTYLE__LINESTARTARROWHEAD:S = 0x1d0s

.field public static final LINESTYLE__LINESTARTARROWWIDTH:S = 0x1d2s

.field public static final LINESTYLE__LINESTYLE:S = 0x1cds

.field public static final LINESTYLE__LINETYPE:S = 0x1c4s

.field public static final LINESTYLE__LINEWIDTH:S = 0x1cbs

.field public static final LINESTYLE__NOLINEDRAWDASH:S = 0x1ffs

.field public static final LINESTYLE__OPACITY:S = 0x1c1s

.field public static final PERSPECTIVE__OFFSETX:S = 0x241s

.field public static final PERSPECTIVE__OFFSETY:S = 0x242s

.field public static final PERSPECTIVE__ORIGINX:S = 0x24as

.field public static final PERSPECTIVE__ORIGINY:S = 0x24bs

.field public static final PERSPECTIVE__PERSPECTIVEON:S = 0x27fs

.field public static final PERSPECTIVE__PERSPECTIVEX:S = 0x247s

.field public static final PERSPECTIVE__PERSPECTIVEY:S = 0x248s

.field public static final PERSPECTIVE__SCALEXTOX:S = 0x243s

.field public static final PERSPECTIVE__SCALEXTOY:S = 0x245s

.field public static final PERSPECTIVE__SCALEYTOX:S = 0x244s

.field public static final PERSPECTIVE__SCALEYTOY:S = 0x246s

.field public static final PERSPECTIVE__TYPE:S = 0x240s

.field public static final PERSPECTIVE__WEIGHT:S = 0x249s

.field public static final PROTECTION__LOCKADJUSTHANDLES:S = 0x7es

.field public static final PROTECTION__LOCKAGAINSTGROUPING:S = 0x7fs

.field public static final PROTECTION__LOCKAGAINSTSELECT:S = 0x7as

.field public static final PROTECTION__LOCKASPECTRATIO:S = 0x78s

.field public static final PROTECTION__LOCKCROPPING:S = 0x7bs

.field public static final PROTECTION__LOCKPOSITION:S = 0x79s

.field public static final PROTECTION__LOCKROTATION:S = 0x77s

.field public static final PROTECTION__LOCKTEXT:S = 0x7ds

.field public static final PROTECTION__LOCKVERTICES:S = 0x7cs

.field public static final SHADOWSTYLE__COLOR:S = 0x201s

.field public static final SHADOWSTYLE__CRMOD:S = 0x203s

.field public static final SHADOWSTYLE__HIGHLIGHT:S = 0x202s

.field public static final SHADOWSTYLE__OFFSETX:S = 0x205s

.field public static final SHADOWSTYLE__OFFSETY:S = 0x206s

.field public static final SHADOWSTYLE__OPACITY:S = 0x204s

.field public static final SHADOWSTYLE__ORIGINX:S = 0x210s

.field public static final SHADOWSTYLE__ORIGINY:S = 0x211s

.field public static final SHADOWSTYLE__PERSPECTIVEX:S = 0x20ds

.field public static final SHADOWSTYLE__PERSPECTIVEY:S = 0x20es

.field public static final SHADOWSTYLE__SCALEXTOX:S = 0x209s

.field public static final SHADOWSTYLE__SCALEXTOY:S = 0x20bs

.field public static final SHADOWSTYLE__SCALEYTOX:S = 0x20as

.field public static final SHADOWSTYLE__SCALEYTOY:S = 0x20cs

.field public static final SHADOWSTYLE__SECONDOFFSETX:S = 0x207s

.field public static final SHADOWSTYLE__SECONDOFFSETY:S = 0x208s

.field public static final SHADOWSTYLE__SHADOW:S = 0x23es

.field public static final SHADOWSTYLE__SHADOWOBSURED:S = 0x23fs

.field public static final SHADOWSTYLE__TYPE:S = 0x200s

.field public static final SHADOWSTYLE__WEIGHT:S = 0x20fs

.field public static final SHAPE__BACKGROUNDSHAPE:S = 0x33fs

.field public static final SHAPE__BLACKANDWHITESETTINGS:S = 0x304s

.field public static final SHAPE__CONNECTORSTYLE:S = 0x303s

.field public static final SHAPE__DELETEATTACHEDOBJECT:S = 0x33es

.field public static final SHAPE__LOCKSHAPETYPE:S = 0x33cs

.field public static final SHAPE__MASTER:S = 0x301s

.field public static final SHAPE__OLEICON:S = 0x33as

.field public static final SHAPE__PREFERRELATIVERESIZE:S = 0x33bs

.field public static final SHAPE__WMODEBW:S = 0x306s

.field public static final SHAPE__WMODEPUREBW:S = 0x305s

.field public static final TEXT__ANCHORTEXT:S = 0x87s

.field public static final TEXT__BIDIR:S = 0x8bs

.field public static final TEXT__FONTROTATION:S = 0x89s

.field public static final TEXT__IDOFNEXTSHAPE:S = 0x8as

.field public static final TEXT__ROTATETEXTWITHSHAPE:S = 0xbds

.field public static final TEXT__SCALETEXT:S = 0x86s

.field public static final TEXT__SINGLECLICKSELECTS:S = 0xbbs

.field public static final TEXT__SIZESHAPETOFITTEXT:S = 0xbes

.field public static final TEXT__SIZE_TEXT_TO_FIT_SHAPE:S = 0xbfs

.field public static final TEXT__TEXTBOTTOM:S = 0x84s

.field public static final TEXT__TEXTFLOW:S = 0x88s

.field public static final TEXT__TEXTID:S = 0x80s

.field public static final TEXT__TEXTLEFT:S = 0x81s

.field public static final TEXT__TEXTRIGHT:S = 0x83s

.field public static final TEXT__TEXTTOP:S = 0x82s

.field public static final TEXT__USEHOSTMARGINS:S = 0xbcs

.field public static final TEXT__WRAPTEXT:S = 0x85s

.field public static final THREEDSTYLE__AMBIENTINTENSITY:S = 0x2d2s

.field public static final THREEDSTYLE__CONSTRAINROTATION:S = 0x2fbs

.field public static final THREEDSTYLE__FILLHARSH:S = 0x2ffs

.field public static final THREEDSTYLE__FILLINTENSITY:S = 0x2das

.field public static final THREEDSTYLE__FILLX:S = 0x2d7s

.field public static final THREEDSTYLE__FILLY:S = 0x2d8s

.field public static final THREEDSTYLE__FILLZ:S = 0x2d9s

.field public static final THREEDSTYLE__KEYHARSH:S = 0x2fes

.field public static final THREEDSTYLE__KEYINTENSITY:S = 0x2d6s

.field public static final THREEDSTYLE__KEYX:S = 0x2d3s

.field public static final THREEDSTYLE__KEYY:S = 0x2d4s

.field public static final THREEDSTYLE__KEYZ:S = 0x2d5s

.field public static final THREEDSTYLE__ORIGINX:S = 0x2ces

.field public static final THREEDSTYLE__ORIGINY:S = 0x2cfs

.field public static final THREEDSTYLE__PARALLEL:S = 0x2fds

.field public static final THREEDSTYLE__RENDERMODE:S = 0x2c9s

.field public static final THREEDSTYLE__ROTATIONANGLE:S = 0x2c5s

.field public static final THREEDSTYLE__ROTATIONAXISX:S = 0x2c2s

.field public static final THREEDSTYLE__ROTATIONAXISY:S = 0x2c3s

.field public static final THREEDSTYLE__ROTATIONAXISZ:S = 0x2c4s

.field public static final THREEDSTYLE__ROTATIONCENTERAUTO:S = 0x2fcs

.field public static final THREEDSTYLE__ROTATIONCENTERX:S = 0x2c6s

.field public static final THREEDSTYLE__ROTATIONCENTERY:S = 0x2c7s

.field public static final THREEDSTYLE__ROTATIONCENTERZ:S = 0x2c8s

.field public static final THREEDSTYLE__SKEWAMOUNT:S = 0x2d1s

.field public static final THREEDSTYLE__SKEWANGLE:S = 0x2d0s

.field public static final THREEDSTYLE__TOLERANCE:S = 0x2cas

.field public static final THREEDSTYLE__XROTATIONANGLE:S = 0x2c1s

.field public static final THREEDSTYLE__XVIEWPOINT:S = 0x2cbs

.field public static final THREEDSTYLE__YROTATIONANGLE:S = 0x2c0s

.field public static final THREEDSTYLE__YVIEWPOINT:S = 0x2ccs

.field public static final THREEDSTYLE__ZVIEWPOINT:S = 0x2cds

.field public static final THREED__3DEFFECT:S = 0x2bcs

.field public static final THREED__CRMOD:S = 0x288s

.field public static final THREED__DIFFUSEAMOUNT:S = 0x295s

.field public static final THREED__EDGETHICKNESS:S = 0x297s

.field public static final THREED__EXTRUDEBACKWARD:S = 0x299s

.field public static final THREED__EXTRUDEFORWARD:S = 0x298s

.field public static final THREED__EXTRUDEPLANE:S = 0x29as

.field public static final THREED__EXTRUSIONCOLOR:S = 0x29bs

.field public static final THREED__LIGHTFACE:S = 0x2bfs

.field public static final THREED__METALLIC:S = 0x2bds

.field public static final THREED__SHININESS:S = 0x296s

.field public static final THREED__SPECULARAMOUNT:S = 0x280s

.field public static final THREED__USEEXTRUSIONCOLOR:S = 0x2bes

.field public static final TRANSFORM__ROTATION:S = 0x4s

.field private static final properties:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Short;",
            "Lcom/intsig/office/fc/ddf/EscherPropertyMetaData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/ddf/EscherProperties;->initProps()Ljava/util/Map;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sput-object v0, Lcom/intsig/office/fc/ddf/EscherProperties;->properties:Ljava/util/Map;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static addProp(Ljava/util/Map;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Short;",
            "Lcom/intsig/office/fc/ddf/EscherPropertyMetaData;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    int-to-short p1, p1

    .line 1
    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object p1

    new-instance v0, Lcom/intsig/office/fc/ddf/EscherPropertyMetaData;

    invoke-direct {v0, p2}, Lcom/intsig/office/fc/ddf/EscherPropertyMetaData;-><init>(Ljava/lang/String;)V

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static addProp(Ljava/util/Map;ILjava/lang/String;B)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Short;",
            "Lcom/intsig/office/fc/ddf/EscherPropertyMetaData;",
            ">;I",
            "Ljava/lang/String;",
            "B)V"
        }
    .end annotation

    int-to-short p1, p1

    .line 2
    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object p1

    new-instance v0, Lcom/intsig/office/fc/ddf/EscherPropertyMetaData;

    invoke-direct {v0, p2, p3}, Lcom/intsig/office/fc/ddf/EscherPropertyMetaData;-><init>(Ljava/lang/String;B)V

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static getPropertyName(S)Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ddf/EscherProperties;->properties:Ljava/util/Map;

    .line 2
    .line 3
    invoke-static {p0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherPropertyMetaData;

    .line 12
    .line 13
    if-nez p0, :cond_0

    .line 14
    .line 15
    const-string p0, "unknown"

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherPropertyMetaData;->getDescription()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object p0

    .line 22
    :goto_0
    return-object p0
    .line 23
    .line 24
.end method

.method public static getPropertyType(S)B
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ddf/EscherProperties;->properties:Ljava/util/Map;

    .line 2
    .line 3
    invoke-static {p0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherPropertyMetaData;

    .line 12
    .line 13
    if-nez p0, :cond_0

    .line 14
    .line 15
    const/4 p0, 0x0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherPropertyMetaData;->getType()B

    .line 18
    .line 19
    .line 20
    move-result p0

    .line 21
    :goto_0
    return p0
    .line 22
    .line 23
    .line 24
.end method

.method private static initProps()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Short;",
            "Lcom/intsig/office/fc/ddf/EscherPropertyMetaData;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x4

    .line 7
    const-string v2, "transform.rotation"

    .line 8
    .line 9
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const/16 v1, 0x77

    .line 13
    .line 14
    const-string v2, "protection.lockrotation"

    .line 15
    .line 16
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 17
    .line 18
    .line 19
    const/16 v1, 0x78

    .line 20
    .line 21
    const-string v2, "protection.lockaspectratio"

    .line 22
    .line 23
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 24
    .line 25
    .line 26
    const/16 v1, 0x79

    .line 27
    .line 28
    const-string v2, "protection.lockposition"

    .line 29
    .line 30
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 31
    .line 32
    .line 33
    const/16 v1, 0x7a

    .line 34
    .line 35
    const-string v2, "protection.lockagainstselect"

    .line 36
    .line 37
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 38
    .line 39
    .line 40
    const/16 v1, 0x7b

    .line 41
    .line 42
    const-string v2, "protection.lockcropping"

    .line 43
    .line 44
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 45
    .line 46
    .line 47
    const/16 v1, 0x7c

    .line 48
    .line 49
    const-string v2, "protection.lockvertices"

    .line 50
    .line 51
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 52
    .line 53
    .line 54
    const/16 v1, 0x7d

    .line 55
    .line 56
    const-string v2, "protection.locktext"

    .line 57
    .line 58
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 59
    .line 60
    .line 61
    const/16 v1, 0x7e

    .line 62
    .line 63
    const-string v2, "protection.lockadjusthandles"

    .line 64
    .line 65
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 66
    .line 67
    .line 68
    const/16 v1, 0x7f

    .line 69
    .line 70
    const-string v2, "protection.lockagainstgrouping"

    .line 71
    .line 72
    const/4 v3, 0x1

    .line 73
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 74
    .line 75
    .line 76
    const/16 v1, 0x80

    .line 77
    .line 78
    const-string v2, "text.textid"

    .line 79
    .line 80
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 81
    .line 82
    .line 83
    const/16 v1, 0x81

    .line 84
    .line 85
    const-string v2, "text.textleft"

    .line 86
    .line 87
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 88
    .line 89
    .line 90
    const/16 v1, 0x82

    .line 91
    .line 92
    const-string v2, "text.texttop"

    .line 93
    .line 94
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 95
    .line 96
    .line 97
    const/16 v1, 0x83

    .line 98
    .line 99
    const-string v2, "text.textright"

    .line 100
    .line 101
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 102
    .line 103
    .line 104
    const/16 v1, 0x84

    .line 105
    .line 106
    const-string v2, "text.textbottom"

    .line 107
    .line 108
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 109
    .line 110
    .line 111
    const/16 v1, 0x85

    .line 112
    .line 113
    const-string v2, "text.wraptext"

    .line 114
    .line 115
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 116
    .line 117
    .line 118
    const/16 v1, 0x86

    .line 119
    .line 120
    const-string v2, "text.scaletext"

    .line 121
    .line 122
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 123
    .line 124
    .line 125
    const/16 v1, 0x87

    .line 126
    .line 127
    const-string v2, "text.anchortext"

    .line 128
    .line 129
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 130
    .line 131
    .line 132
    const/16 v1, 0x88

    .line 133
    .line 134
    const-string v2, "text.textflow"

    .line 135
    .line 136
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 137
    .line 138
    .line 139
    const/16 v1, 0x89

    .line 140
    .line 141
    const-string v2, "text.fontrotation"

    .line 142
    .line 143
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 144
    .line 145
    .line 146
    const/16 v1, 0x8a

    .line 147
    .line 148
    const-string v2, "text.idofnextshape"

    .line 149
    .line 150
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 151
    .line 152
    .line 153
    const/16 v1, 0x8b

    .line 154
    .line 155
    const-string v2, "text.bidir"

    .line 156
    .line 157
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 158
    .line 159
    .line 160
    const/16 v1, 0xbb

    .line 161
    .line 162
    const-string v2, "text.singleclickselects"

    .line 163
    .line 164
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 165
    .line 166
    .line 167
    const/16 v1, 0xbc

    .line 168
    .line 169
    const-string v2, "text.usehostmargins"

    .line 170
    .line 171
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 172
    .line 173
    .line 174
    const/16 v1, 0xbd

    .line 175
    .line 176
    const-string v2, "text.rotatetextwithshape"

    .line 177
    .line 178
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 179
    .line 180
    .line 181
    const/16 v1, 0xbe

    .line 182
    .line 183
    const-string v2, "text.sizeshapetofittext"

    .line 184
    .line 185
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 186
    .line 187
    .line 188
    const/16 v1, 0xbf

    .line 189
    .line 190
    const-string v2, "text.sizetexttofitshape"

    .line 191
    .line 192
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 193
    .line 194
    .line 195
    const/16 v1, 0xc0

    .line 196
    .line 197
    const-string v2, "geotext.unicode"

    .line 198
    .line 199
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 200
    .line 201
    .line 202
    const/16 v1, 0xc1

    .line 203
    .line 204
    const-string v2, "geotext.rtftext"

    .line 205
    .line 206
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 207
    .line 208
    .line 209
    const/16 v1, 0xc2

    .line 210
    .line 211
    const-string v2, "geotext.alignmentoncurve"

    .line 212
    .line 213
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 214
    .line 215
    .line 216
    const/16 v1, 0xc3

    .line 217
    .line 218
    const-string v2, "geotext.defaultpointsize"

    .line 219
    .line 220
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 221
    .line 222
    .line 223
    const/16 v1, 0xc4

    .line 224
    .line 225
    const-string v2, "geotext.textspacing"

    .line 226
    .line 227
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 228
    .line 229
    .line 230
    const/16 v1, 0xc5

    .line 231
    .line 232
    const-string v2, "geotext.fontfamilyname"

    .line 233
    .line 234
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 235
    .line 236
    .line 237
    const/16 v1, 0xf0

    .line 238
    .line 239
    const-string v2, "geotext.reverseroworder"

    .line 240
    .line 241
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 242
    .line 243
    .line 244
    const/16 v1, 0xf1

    .line 245
    .line 246
    const-string v2, "geotext.hastexteffect"

    .line 247
    .line 248
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 249
    .line 250
    .line 251
    const/16 v1, 0xf2

    .line 252
    .line 253
    const-string v2, "geotext.rotatecharacters"

    .line 254
    .line 255
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 256
    .line 257
    .line 258
    const/16 v1, 0xf3

    .line 259
    .line 260
    const-string v2, "geotext.kerncharacters"

    .line 261
    .line 262
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 263
    .line 264
    .line 265
    const/16 v1, 0xf4

    .line 266
    .line 267
    const-string v2, "geotext.tightortrack"

    .line 268
    .line 269
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 270
    .line 271
    .line 272
    const/16 v1, 0xf5

    .line 273
    .line 274
    const-string v2, "geotext.stretchtofitshape"

    .line 275
    .line 276
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 277
    .line 278
    .line 279
    const/16 v1, 0xf6

    .line 280
    .line 281
    const-string v2, "geotext.charboundingbox"

    .line 282
    .line 283
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 284
    .line 285
    .line 286
    const/16 v1, 0xf7

    .line 287
    .line 288
    const-string v2, "geotext.scaletextonpath"

    .line 289
    .line 290
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 291
    .line 292
    .line 293
    const/16 v1, 0xf8

    .line 294
    .line 295
    const-string v2, "geotext.stretchcharheight"

    .line 296
    .line 297
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 298
    .line 299
    .line 300
    const/16 v1, 0xf9

    .line 301
    .line 302
    const-string v2, "geotext.nomeasurealongpath"

    .line 303
    .line 304
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 305
    .line 306
    .line 307
    const/16 v1, 0xfa

    .line 308
    .line 309
    const-string v2, "geotext.boldfont"

    .line 310
    .line 311
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 312
    .line 313
    .line 314
    const/16 v1, 0xfb

    .line 315
    .line 316
    const-string v2, "geotext.italicfont"

    .line 317
    .line 318
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 319
    .line 320
    .line 321
    const/16 v1, 0xfc

    .line 322
    .line 323
    const-string v2, "geotext.underlinefont"

    .line 324
    .line 325
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 326
    .line 327
    .line 328
    const/16 v1, 0xfd

    .line 329
    .line 330
    const-string v2, "geotext.shadowfont"

    .line 331
    .line 332
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 333
    .line 334
    .line 335
    const/16 v1, 0xfe

    .line 336
    .line 337
    const-string v2, "geotext.smallcapsfont"

    .line 338
    .line 339
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 340
    .line 341
    .line 342
    const/16 v1, 0xff

    .line 343
    .line 344
    const-string v2, "geotext.strikethroughfont"

    .line 345
    .line 346
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 347
    .line 348
    .line 349
    const/16 v1, 0x100

    .line 350
    .line 351
    const-string v2, "blip.cropfromtop"

    .line 352
    .line 353
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 354
    .line 355
    .line 356
    const/16 v1, 0x101

    .line 357
    .line 358
    const-string v2, "blip.cropfrombottom"

    .line 359
    .line 360
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 361
    .line 362
    .line 363
    const/16 v1, 0x102

    .line 364
    .line 365
    const-string v2, "blip.cropfromleft"

    .line 366
    .line 367
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 368
    .line 369
    .line 370
    const/16 v1, 0x103

    .line 371
    .line 372
    const-string v2, "blip.cropfromright"

    .line 373
    .line 374
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 375
    .line 376
    .line 377
    const/16 v1, 0x104

    .line 378
    .line 379
    const-string v2, "blip.bliptodisplay"

    .line 380
    .line 381
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 382
    .line 383
    .line 384
    const/16 v1, 0x105

    .line 385
    .line 386
    const-string v2, "blip.blipfilename"

    .line 387
    .line 388
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 389
    .line 390
    .line 391
    const/16 v1, 0x106

    .line 392
    .line 393
    const-string v2, "blip.blipflags"

    .line 394
    .line 395
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 396
    .line 397
    .line 398
    const/16 v1, 0x107

    .line 399
    .line 400
    const-string v2, "blip.transparentcolor"

    .line 401
    .line 402
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 403
    .line 404
    .line 405
    const/16 v1, 0x108

    .line 406
    .line 407
    const-string v2, "blip.contrastsetting"

    .line 408
    .line 409
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 410
    .line 411
    .line 412
    const/16 v1, 0x109

    .line 413
    .line 414
    const-string v2, "blip.brightnesssetting"

    .line 415
    .line 416
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 417
    .line 418
    .line 419
    const/16 v1, 0x10a

    .line 420
    .line 421
    const-string v2, "blip.gamma"

    .line 422
    .line 423
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 424
    .line 425
    .line 426
    const/16 v1, 0x10b

    .line 427
    .line 428
    const-string v2, "blip.pictureid"

    .line 429
    .line 430
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 431
    .line 432
    .line 433
    const/16 v1, 0x10c

    .line 434
    .line 435
    const-string v2, "blip.doublemod"

    .line 436
    .line 437
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 438
    .line 439
    .line 440
    const/16 v1, 0x10d

    .line 441
    .line 442
    const-string v2, "blip.picturefillmod"

    .line 443
    .line 444
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 445
    .line 446
    .line 447
    const/16 v1, 0x10e

    .line 448
    .line 449
    const-string v2, "blip.pictureline"

    .line 450
    .line 451
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 452
    .line 453
    .line 454
    const/16 v1, 0x10f

    .line 455
    .line 456
    const-string v2, "blip.printblip"

    .line 457
    .line 458
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 459
    .line 460
    .line 461
    const/16 v1, 0x110

    .line 462
    .line 463
    const-string v2, "blip.printblipfilename"

    .line 464
    .line 465
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 466
    .line 467
    .line 468
    const/16 v1, 0x111

    .line 469
    .line 470
    const-string v2, "blip.printflags"

    .line 471
    .line 472
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 473
    .line 474
    .line 475
    const/16 v1, 0x13c

    .line 476
    .line 477
    const-string v2, "blip.nohittestpicture"

    .line 478
    .line 479
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 480
    .line 481
    .line 482
    const/16 v1, 0x13d

    .line 483
    .line 484
    const-string v2, "blip.picturegray"

    .line 485
    .line 486
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 487
    .line 488
    .line 489
    const/16 v1, 0x13e

    .line 490
    .line 491
    const-string v2, "blip.picturebilevel"

    .line 492
    .line 493
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 494
    .line 495
    .line 496
    const/16 v1, 0x13f

    .line 497
    .line 498
    const-string v2, "blip.pictureactive"

    .line 499
    .line 500
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 501
    .line 502
    .line 503
    const/16 v1, 0x140

    .line 504
    .line 505
    const-string v2, "geometry.left"

    .line 506
    .line 507
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 508
    .line 509
    .line 510
    const/16 v1, 0x141

    .line 511
    .line 512
    const-string v2, "geometry.top"

    .line 513
    .line 514
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 515
    .line 516
    .line 517
    const/16 v1, 0x142

    .line 518
    .line 519
    const-string v2, "geometry.right"

    .line 520
    .line 521
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 522
    .line 523
    .line 524
    const/16 v1, 0x143

    .line 525
    .line 526
    const-string v2, "geometry.bottom"

    .line 527
    .line 528
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 529
    .line 530
    .line 531
    const-string v1, "geometry.shapepath"

    .line 532
    .line 533
    const/4 v2, 0x3

    .line 534
    const/16 v4, 0x144

    .line 535
    .line 536
    invoke-static {v0, v4, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 537
    .line 538
    .line 539
    const/16 v1, 0x145

    .line 540
    .line 541
    const-string v2, "geometry.vertices"

    .line 542
    .line 543
    const/4 v4, 0x5

    .line 544
    invoke-static {v0, v1, v2, v4}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 545
    .line 546
    .line 547
    const/16 v1, 0x146

    .line 548
    .line 549
    const-string v2, "geometry.segmentinfo"

    .line 550
    .line 551
    invoke-static {v0, v1, v2, v4}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 552
    .line 553
    .line 554
    const/16 v1, 0x147

    .line 555
    .line 556
    const-string v2, "geometry.adjustvalue"

    .line 557
    .line 558
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 559
    .line 560
    .line 561
    const/16 v1, 0x148

    .line 562
    .line 563
    const-string v2, "geometry.adjust2value"

    .line 564
    .line 565
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 566
    .line 567
    .line 568
    const/16 v1, 0x149

    .line 569
    .line 570
    const-string v2, "geometry.adjust3value"

    .line 571
    .line 572
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 573
    .line 574
    .line 575
    const/16 v1, 0x14a

    .line 576
    .line 577
    const-string v2, "geometry.adjust4value"

    .line 578
    .line 579
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 580
    .line 581
    .line 582
    const/16 v1, 0x14b

    .line 583
    .line 584
    const-string v2, "geometry.adjust5value"

    .line 585
    .line 586
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 587
    .line 588
    .line 589
    const/16 v1, 0x14c

    .line 590
    .line 591
    const-string v2, "geometry.adjust6value"

    .line 592
    .line 593
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 594
    .line 595
    .line 596
    const/16 v1, 0x14d

    .line 597
    .line 598
    const-string v2, "geometry.adjust7value"

    .line 599
    .line 600
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 601
    .line 602
    .line 603
    const/16 v1, 0x14e

    .line 604
    .line 605
    const-string v2, "geometry.adjust8value"

    .line 606
    .line 607
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 608
    .line 609
    .line 610
    const/16 v1, 0x14f

    .line 611
    .line 612
    const-string v2, "geometry.adjust9value"

    .line 613
    .line 614
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 615
    .line 616
    .line 617
    const/16 v1, 0x150

    .line 618
    .line 619
    const-string v2, "geometry.adjust10value"

    .line 620
    .line 621
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 622
    .line 623
    .line 624
    const/16 v1, 0x17a

    .line 625
    .line 626
    const-string v2, "geometry.shadowOK"

    .line 627
    .line 628
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 629
    .line 630
    .line 631
    const/16 v1, 0x17b

    .line 632
    .line 633
    const-string v2, "geometry.3dok"

    .line 634
    .line 635
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 636
    .line 637
    .line 638
    const/16 v1, 0x17c

    .line 639
    .line 640
    const-string v2, "geometry.lineok"

    .line 641
    .line 642
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 643
    .line 644
    .line 645
    const/16 v1, 0x17d

    .line 646
    .line 647
    const-string v2, "geometry.geotextok"

    .line 648
    .line 649
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 650
    .line 651
    .line 652
    const/16 v1, 0x17e

    .line 653
    .line 654
    const-string v2, "geometry.fillshadeshapeok"

    .line 655
    .line 656
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 657
    .line 658
    .line 659
    const/16 v1, 0x17f

    .line 660
    .line 661
    const-string v2, "geometry.fillok"

    .line 662
    .line 663
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 664
    .line 665
    .line 666
    const/16 v1, 0x180

    .line 667
    .line 668
    const-string v2, "fill.filltype"

    .line 669
    .line 670
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 671
    .line 672
    .line 673
    const/16 v1, 0x181

    .line 674
    .line 675
    const-string v2, "fill.fillcolor"

    .line 676
    .line 677
    const/4 v5, 0x2

    .line 678
    invoke-static {v0, v1, v2, v5}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 679
    .line 680
    .line 681
    const/16 v1, 0x182

    .line 682
    .line 683
    const-string v2, "fill.fillopacity"

    .line 684
    .line 685
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 686
    .line 687
    .line 688
    const/16 v1, 0x183

    .line 689
    .line 690
    const-string v2, "fill.fillbackcolor"

    .line 691
    .line 692
    invoke-static {v0, v1, v2, v5}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 693
    .line 694
    .line 695
    const/16 v1, 0x184

    .line 696
    .line 697
    const-string v2, "fill.backopacity"

    .line 698
    .line 699
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 700
    .line 701
    .line 702
    const/16 v1, 0x185

    .line 703
    .line 704
    const-string v2, "fill.crmod"

    .line 705
    .line 706
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 707
    .line 708
    .line 709
    const/16 v1, 0x186

    .line 710
    .line 711
    const-string v2, "fill.patterntexture"

    .line 712
    .line 713
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 714
    .line 715
    .line 716
    const/16 v1, 0x187

    .line 717
    .line 718
    const-string v2, "fill.blipfilename"

    .line 719
    .line 720
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 721
    .line 722
    .line 723
    const/16 v1, 0x188

    .line 724
    .line 725
    const-string v2, "fill.blipflags"

    .line 726
    .line 727
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 728
    .line 729
    .line 730
    const/16 v1, 0x189

    .line 731
    .line 732
    const-string v2, "fill.width"

    .line 733
    .line 734
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 735
    .line 736
    .line 737
    const/16 v1, 0x18a

    .line 738
    .line 739
    const-string v2, "fill.height"

    .line 740
    .line 741
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 742
    .line 743
    .line 744
    const/16 v1, 0x18b

    .line 745
    .line 746
    const-string v2, "fill.angle"

    .line 747
    .line 748
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 749
    .line 750
    .line 751
    const/16 v1, 0x18c

    .line 752
    .line 753
    const-string v2, "fill.focus"

    .line 754
    .line 755
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 756
    .line 757
    .line 758
    const/16 v1, 0x18d

    .line 759
    .line 760
    const-string v2, "fill.toleft"

    .line 761
    .line 762
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 763
    .line 764
    .line 765
    const/16 v1, 0x18e

    .line 766
    .line 767
    const-string v2, "fill.totop"

    .line 768
    .line 769
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 770
    .line 771
    .line 772
    const/16 v1, 0x18f

    .line 773
    .line 774
    const-string v2, "fill.toright"

    .line 775
    .line 776
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 777
    .line 778
    .line 779
    const/16 v1, 0x190

    .line 780
    .line 781
    const-string v2, "fill.tobottom"

    .line 782
    .line 783
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 784
    .line 785
    .line 786
    const/16 v1, 0x191

    .line 787
    .line 788
    const-string v2, "fill.rectleft"

    .line 789
    .line 790
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 791
    .line 792
    .line 793
    const/16 v1, 0x192

    .line 794
    .line 795
    const-string v2, "fill.recttop"

    .line 796
    .line 797
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 798
    .line 799
    .line 800
    const/16 v1, 0x193

    .line 801
    .line 802
    const-string v2, "fill.rectright"

    .line 803
    .line 804
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 805
    .line 806
    .line 807
    const/16 v1, 0x194

    .line 808
    .line 809
    const-string v2, "fill.rectbottom"

    .line 810
    .line 811
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 812
    .line 813
    .line 814
    const/16 v1, 0x195

    .line 815
    .line 816
    const-string v2, "fill.dztype"

    .line 817
    .line 818
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 819
    .line 820
    .line 821
    const/16 v1, 0x196

    .line 822
    .line 823
    const-string v2, "fill.shadepreset"

    .line 824
    .line 825
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 826
    .line 827
    .line 828
    const/16 v1, 0x197

    .line 829
    .line 830
    const-string v2, "fill.shadecolors"

    .line 831
    .line 832
    invoke-static {v0, v1, v2, v4}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 833
    .line 834
    .line 835
    const/16 v1, 0x198

    .line 836
    .line 837
    const-string v2, "fill.originx"

    .line 838
    .line 839
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 840
    .line 841
    .line 842
    const/16 v1, 0x199

    .line 843
    .line 844
    const-string v2, "fill.originy"

    .line 845
    .line 846
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 847
    .line 848
    .line 849
    const/16 v1, 0x19a

    .line 850
    .line 851
    const-string v2, "fill.shapeoriginx"

    .line 852
    .line 853
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 854
    .line 855
    .line 856
    const/16 v1, 0x19b

    .line 857
    .line 858
    const-string v2, "fill.shapeoriginy"

    .line 859
    .line 860
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 861
    .line 862
    .line 863
    const/16 v1, 0x19c

    .line 864
    .line 865
    const-string v2, "fill.shadetype"

    .line 866
    .line 867
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 868
    .line 869
    .line 870
    const/16 v1, 0x1bb

    .line 871
    .line 872
    const-string v2, "fill.filled"

    .line 873
    .line 874
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 875
    .line 876
    .line 877
    const/16 v1, 0x1bc

    .line 878
    .line 879
    const-string v2, "fill.hittestfill"

    .line 880
    .line 881
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 882
    .line 883
    .line 884
    const/16 v1, 0x1bd

    .line 885
    .line 886
    const-string v2, "fill.shape"

    .line 887
    .line 888
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 889
    .line 890
    .line 891
    const/16 v1, 0x1be

    .line 892
    .line 893
    const-string v2, "fill.userect"

    .line 894
    .line 895
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 896
    .line 897
    .line 898
    const/16 v1, 0x1bf

    .line 899
    .line 900
    const-string v2, "fill.nofillhittest"

    .line 901
    .line 902
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 903
    .line 904
    .line 905
    const/16 v1, 0x1c0

    .line 906
    .line 907
    const-string v2, "linestyle.color"

    .line 908
    .line 909
    invoke-static {v0, v1, v2, v5}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 910
    .line 911
    .line 912
    const/16 v1, 0x1c1

    .line 913
    .line 914
    const-string v2, "linestyle.opacity"

    .line 915
    .line 916
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 917
    .line 918
    .line 919
    const/16 v1, 0x1c2

    .line 920
    .line 921
    const-string v2, "linestyle.backcolor"

    .line 922
    .line 923
    invoke-static {v0, v1, v2, v5}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 924
    .line 925
    .line 926
    const/16 v1, 0x1c3

    .line 927
    .line 928
    const-string v2, "linestyle.crmod"

    .line 929
    .line 930
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 931
    .line 932
    .line 933
    const/16 v1, 0x1c4

    .line 934
    .line 935
    const-string v2, "linestyle.linetype"

    .line 936
    .line 937
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 938
    .line 939
    .line 940
    const/16 v1, 0x1c5

    .line 941
    .line 942
    const-string v2, "linestyle.fillblip"

    .line 943
    .line 944
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 945
    .line 946
    .line 947
    const/16 v1, 0x1c6

    .line 948
    .line 949
    const-string v2, "linestyle.fillblipname"

    .line 950
    .line 951
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 952
    .line 953
    .line 954
    const/16 v1, 0x1c7

    .line 955
    .line 956
    const-string v2, "linestyle.fillblipflags"

    .line 957
    .line 958
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 959
    .line 960
    .line 961
    const/16 v1, 0x1c8

    .line 962
    .line 963
    const-string v2, "linestyle.fillwidth"

    .line 964
    .line 965
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 966
    .line 967
    .line 968
    const/16 v1, 0x1c9

    .line 969
    .line 970
    const-string v2, "linestyle.fillheight"

    .line 971
    .line 972
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 973
    .line 974
    .line 975
    const/16 v1, 0x1ca

    .line 976
    .line 977
    const-string v2, "linestyle.filldztype"

    .line 978
    .line 979
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 980
    .line 981
    .line 982
    const/16 v1, 0x1cb

    .line 983
    .line 984
    const-string v2, "linestyle.linewidth"

    .line 985
    .line 986
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 987
    .line 988
    .line 989
    const/16 v1, 0x1cc

    .line 990
    .line 991
    const-string v2, "linestyle.linemiterlimit"

    .line 992
    .line 993
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 994
    .line 995
    .line 996
    const/16 v1, 0x1cd

    .line 997
    .line 998
    const-string v2, "linestyle.linestyle"

    .line 999
    .line 1000
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1001
    .line 1002
    .line 1003
    const/16 v1, 0x1ce

    .line 1004
    .line 1005
    const-string v2, "linestyle.linedashing"

    .line 1006
    .line 1007
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1008
    .line 1009
    .line 1010
    const/16 v1, 0x1cf

    .line 1011
    .line 1012
    const-string v2, "linestyle.linedashstyle"

    .line 1013
    .line 1014
    invoke-static {v0, v1, v2, v4}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 1015
    .line 1016
    .line 1017
    const/16 v1, 0x1d0

    .line 1018
    .line 1019
    const-string v2, "linestyle.linestartarrowhead"

    .line 1020
    .line 1021
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1022
    .line 1023
    .line 1024
    const/16 v1, 0x1d1

    .line 1025
    .line 1026
    const-string v2, "linestyle.lineendarrowhead"

    .line 1027
    .line 1028
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1029
    .line 1030
    .line 1031
    const/16 v1, 0x1d2

    .line 1032
    .line 1033
    const-string v2, "linestyle.linestartarrowwidth"

    .line 1034
    .line 1035
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1036
    .line 1037
    .line 1038
    const/16 v1, 0x1d3

    .line 1039
    .line 1040
    const-string v2, "linestyle.lineestartarrowlength"

    .line 1041
    .line 1042
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1043
    .line 1044
    .line 1045
    const/16 v1, 0x1d4

    .line 1046
    .line 1047
    const-string v2, "linestyle.lineendarrowwidth"

    .line 1048
    .line 1049
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1050
    .line 1051
    .line 1052
    const/16 v1, 0x1d5

    .line 1053
    .line 1054
    const-string v2, "linestyle.lineendarrowlength"

    .line 1055
    .line 1056
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1057
    .line 1058
    .line 1059
    const/16 v1, 0x1d6

    .line 1060
    .line 1061
    const-string v2, "linestyle.linejoinstyle"

    .line 1062
    .line 1063
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1064
    .line 1065
    .line 1066
    const/16 v1, 0x1d7

    .line 1067
    .line 1068
    const-string v2, "linestyle.lineendcapstyle"

    .line 1069
    .line 1070
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1071
    .line 1072
    .line 1073
    const/16 v1, 0x1fb

    .line 1074
    .line 1075
    const-string v2, "linestyle.arrowheadsok"

    .line 1076
    .line 1077
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1078
    .line 1079
    .line 1080
    const/16 v1, 0x1fc

    .line 1081
    .line 1082
    const-string v2, "linestyle.anyline"

    .line 1083
    .line 1084
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1085
    .line 1086
    .line 1087
    const/16 v1, 0x1fd

    .line 1088
    .line 1089
    const-string v2, "linestyle.hitlinetest"

    .line 1090
    .line 1091
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1092
    .line 1093
    .line 1094
    const/16 v1, 0x1fe

    .line 1095
    .line 1096
    const-string v2, "linestyle.linefillshape"

    .line 1097
    .line 1098
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1099
    .line 1100
    .line 1101
    const/16 v1, 0x1ff

    .line 1102
    .line 1103
    const-string v2, "linestyle.nolinedrawdash"

    .line 1104
    .line 1105
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 1106
    .line 1107
    .line 1108
    const/16 v1, 0x200

    .line 1109
    .line 1110
    const-string v2, "shadowstyle.type"

    .line 1111
    .line 1112
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1113
    .line 1114
    .line 1115
    const/16 v1, 0x201

    .line 1116
    .line 1117
    const-string v2, "shadowstyle.color"

    .line 1118
    .line 1119
    invoke-static {v0, v1, v2, v5}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 1120
    .line 1121
    .line 1122
    const/16 v1, 0x202

    .line 1123
    .line 1124
    const-string v2, "shadowstyle.highlight"

    .line 1125
    .line 1126
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1127
    .line 1128
    .line 1129
    const/16 v1, 0x203

    .line 1130
    .line 1131
    const-string v2, "shadowstyle.crmod"

    .line 1132
    .line 1133
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1134
    .line 1135
    .line 1136
    const/16 v1, 0x204

    .line 1137
    .line 1138
    const-string v2, "shadowstyle.opacity"

    .line 1139
    .line 1140
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1141
    .line 1142
    .line 1143
    const/16 v1, 0x205

    .line 1144
    .line 1145
    const-string v2, "shadowstyle.offsetx"

    .line 1146
    .line 1147
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1148
    .line 1149
    .line 1150
    const/16 v1, 0x206

    .line 1151
    .line 1152
    const-string v2, "shadowstyle.offsety"

    .line 1153
    .line 1154
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1155
    .line 1156
    .line 1157
    const/16 v1, 0x207

    .line 1158
    .line 1159
    const-string v2, "shadowstyle.secondoffsetx"

    .line 1160
    .line 1161
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1162
    .line 1163
    .line 1164
    const/16 v1, 0x208

    .line 1165
    .line 1166
    const-string v2, "shadowstyle.secondoffsety"

    .line 1167
    .line 1168
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1169
    .line 1170
    .line 1171
    const/16 v1, 0x209

    .line 1172
    .line 1173
    const-string v2, "shadowstyle.scalextox"

    .line 1174
    .line 1175
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1176
    .line 1177
    .line 1178
    const/16 v1, 0x20a

    .line 1179
    .line 1180
    const-string v2, "shadowstyle.scaleytox"

    .line 1181
    .line 1182
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1183
    .line 1184
    .line 1185
    const/16 v1, 0x20b

    .line 1186
    .line 1187
    const-string v2, "shadowstyle.scalextoy"

    .line 1188
    .line 1189
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1190
    .line 1191
    .line 1192
    const/16 v1, 0x20c

    .line 1193
    .line 1194
    const-string v2, "shadowstyle.scaleytoy"

    .line 1195
    .line 1196
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1197
    .line 1198
    .line 1199
    const/16 v1, 0x20d

    .line 1200
    .line 1201
    const-string v2, "shadowstyle.perspectivex"

    .line 1202
    .line 1203
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1204
    .line 1205
    .line 1206
    const/16 v1, 0x20e

    .line 1207
    .line 1208
    const-string v2, "shadowstyle.perspectivey"

    .line 1209
    .line 1210
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1211
    .line 1212
    .line 1213
    const/16 v1, 0x20f

    .line 1214
    .line 1215
    const-string v2, "shadowstyle.weight"

    .line 1216
    .line 1217
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1218
    .line 1219
    .line 1220
    const/16 v1, 0x210

    .line 1221
    .line 1222
    const-string v2, "shadowstyle.originx"

    .line 1223
    .line 1224
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1225
    .line 1226
    .line 1227
    const/16 v1, 0x211

    .line 1228
    .line 1229
    const-string v2, "shadowstyle.originy"

    .line 1230
    .line 1231
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1232
    .line 1233
    .line 1234
    const/16 v1, 0x23e

    .line 1235
    .line 1236
    const-string v2, "shadowstyle.shadow"

    .line 1237
    .line 1238
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1239
    .line 1240
    .line 1241
    const/16 v1, 0x23f

    .line 1242
    .line 1243
    const-string v2, "shadowstyle.shadowobsured"

    .line 1244
    .line 1245
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1246
    .line 1247
    .line 1248
    const/16 v1, 0x240

    .line 1249
    .line 1250
    const-string v2, "perspective.type"

    .line 1251
    .line 1252
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1253
    .line 1254
    .line 1255
    const/16 v1, 0x241

    .line 1256
    .line 1257
    const-string v2, "perspective.offsetx"

    .line 1258
    .line 1259
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1260
    .line 1261
    .line 1262
    const/16 v1, 0x242

    .line 1263
    .line 1264
    const-string v2, "perspective.offsety"

    .line 1265
    .line 1266
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1267
    .line 1268
    .line 1269
    const/16 v1, 0x243

    .line 1270
    .line 1271
    const-string v2, "perspective.scalextox"

    .line 1272
    .line 1273
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1274
    .line 1275
    .line 1276
    const/16 v1, 0x244

    .line 1277
    .line 1278
    const-string v2, "perspective.scaleytox"

    .line 1279
    .line 1280
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1281
    .line 1282
    .line 1283
    const/16 v1, 0x245

    .line 1284
    .line 1285
    const-string v2, "perspective.scalextoy"

    .line 1286
    .line 1287
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1288
    .line 1289
    .line 1290
    const/16 v1, 0x246

    .line 1291
    .line 1292
    const-string v2, "perspective.scaleytoy"

    .line 1293
    .line 1294
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1295
    .line 1296
    .line 1297
    const/16 v1, 0x247

    .line 1298
    .line 1299
    const-string v2, "perspective.perspectivex"

    .line 1300
    .line 1301
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1302
    .line 1303
    .line 1304
    const/16 v1, 0x248

    .line 1305
    .line 1306
    const-string v2, "perspective.perspectivey"

    .line 1307
    .line 1308
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1309
    .line 1310
    .line 1311
    const/16 v1, 0x249

    .line 1312
    .line 1313
    const-string v2, "perspective.weight"

    .line 1314
    .line 1315
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1316
    .line 1317
    .line 1318
    const/16 v1, 0x24a

    .line 1319
    .line 1320
    const-string v2, "perspective.originx"

    .line 1321
    .line 1322
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1323
    .line 1324
    .line 1325
    const/16 v1, 0x24b

    .line 1326
    .line 1327
    const-string v2, "perspective.originy"

    .line 1328
    .line 1329
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1330
    .line 1331
    .line 1332
    const/16 v1, 0x27f

    .line 1333
    .line 1334
    const-string v2, "perspective.perspectiveon"

    .line 1335
    .line 1336
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1337
    .line 1338
    .line 1339
    const/16 v1, 0x280

    .line 1340
    .line 1341
    const-string v2, "3d.specularamount"

    .line 1342
    .line 1343
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1344
    .line 1345
    .line 1346
    const/16 v1, 0x295

    .line 1347
    .line 1348
    const-string v2, "3d.diffuseamount"

    .line 1349
    .line 1350
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1351
    .line 1352
    .line 1353
    const/16 v1, 0x296

    .line 1354
    .line 1355
    const-string v2, "3d.shininess"

    .line 1356
    .line 1357
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1358
    .line 1359
    .line 1360
    const/16 v1, 0x297

    .line 1361
    .line 1362
    const-string v2, "3d.edgethickness"

    .line 1363
    .line 1364
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1365
    .line 1366
    .line 1367
    const/16 v1, 0x298

    .line 1368
    .line 1369
    const-string v2, "3d.extrudeforward"

    .line 1370
    .line 1371
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1372
    .line 1373
    .line 1374
    const/16 v1, 0x299

    .line 1375
    .line 1376
    const-string v2, "3d.extrudebackward"

    .line 1377
    .line 1378
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1379
    .line 1380
    .line 1381
    const/16 v1, 0x29a

    .line 1382
    .line 1383
    const-string v2, "3d.extrudeplane"

    .line 1384
    .line 1385
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1386
    .line 1387
    .line 1388
    const/16 v1, 0x29b

    .line 1389
    .line 1390
    const-string v2, "3d.extrusioncolor"

    .line 1391
    .line 1392
    invoke-static {v0, v1, v2, v5}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 1393
    .line 1394
    .line 1395
    const/16 v1, 0x288

    .line 1396
    .line 1397
    const-string v2, "3d.crmod"

    .line 1398
    .line 1399
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1400
    .line 1401
    .line 1402
    const/16 v1, 0x2bc

    .line 1403
    .line 1404
    const-string v2, "3d.3deffect"

    .line 1405
    .line 1406
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1407
    .line 1408
    .line 1409
    const/16 v1, 0x2bd

    .line 1410
    .line 1411
    const-string v2, "3d.metallic"

    .line 1412
    .line 1413
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1414
    .line 1415
    .line 1416
    const/16 v1, 0x2be

    .line 1417
    .line 1418
    const-string v2, "3d.useextrusioncolor"

    .line 1419
    .line 1420
    invoke-static {v0, v1, v2, v5}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 1421
    .line 1422
    .line 1423
    const/16 v1, 0x2bf

    .line 1424
    .line 1425
    const-string v2, "3d.lightface"

    .line 1426
    .line 1427
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1428
    .line 1429
    .line 1430
    const/16 v1, 0x2c0

    .line 1431
    .line 1432
    const-string v2, "3dstyle.yrotationangle"

    .line 1433
    .line 1434
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1435
    .line 1436
    .line 1437
    const/16 v1, 0x2c1

    .line 1438
    .line 1439
    const-string v2, "3dstyle.xrotationangle"

    .line 1440
    .line 1441
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1442
    .line 1443
    .line 1444
    const/16 v1, 0x2c2

    .line 1445
    .line 1446
    const-string v2, "3dstyle.rotationaxisx"

    .line 1447
    .line 1448
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1449
    .line 1450
    .line 1451
    const/16 v1, 0x2c3

    .line 1452
    .line 1453
    const-string v2, "3dstyle.rotationaxisy"

    .line 1454
    .line 1455
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1456
    .line 1457
    .line 1458
    const/16 v1, 0x2c4

    .line 1459
    .line 1460
    const-string v2, "3dstyle.rotationaxisz"

    .line 1461
    .line 1462
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1463
    .line 1464
    .line 1465
    const/16 v1, 0x2c5

    .line 1466
    .line 1467
    const-string v2, "3dstyle.rotationangle"

    .line 1468
    .line 1469
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1470
    .line 1471
    .line 1472
    const/16 v1, 0x2c6

    .line 1473
    .line 1474
    const-string v2, "3dstyle.rotationcenterx"

    .line 1475
    .line 1476
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1477
    .line 1478
    .line 1479
    const/16 v1, 0x2c7

    .line 1480
    .line 1481
    const-string v2, "3dstyle.rotationcentery"

    .line 1482
    .line 1483
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1484
    .line 1485
    .line 1486
    const/16 v1, 0x2c8

    .line 1487
    .line 1488
    const-string v2, "3dstyle.rotationcenterz"

    .line 1489
    .line 1490
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1491
    .line 1492
    .line 1493
    const/16 v1, 0x2c9

    .line 1494
    .line 1495
    const-string v2, "3dstyle.rendermode"

    .line 1496
    .line 1497
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1498
    .line 1499
    .line 1500
    const/16 v1, 0x2ca

    .line 1501
    .line 1502
    const-string v2, "3dstyle.tolerance"

    .line 1503
    .line 1504
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1505
    .line 1506
    .line 1507
    const/16 v1, 0x2cb

    .line 1508
    .line 1509
    const-string v2, "3dstyle.xviewpoint"

    .line 1510
    .line 1511
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1512
    .line 1513
    .line 1514
    const/16 v1, 0x2cc

    .line 1515
    .line 1516
    const-string v2, "3dstyle.yviewpoint"

    .line 1517
    .line 1518
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1519
    .line 1520
    .line 1521
    const/16 v1, 0x2cd

    .line 1522
    .line 1523
    const-string v2, "3dstyle.zviewpoint"

    .line 1524
    .line 1525
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1526
    .line 1527
    .line 1528
    const/16 v1, 0x2ce

    .line 1529
    .line 1530
    const-string v2, "3dstyle.originx"

    .line 1531
    .line 1532
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1533
    .line 1534
    .line 1535
    const/16 v1, 0x2cf

    .line 1536
    .line 1537
    const-string v2, "3dstyle.originy"

    .line 1538
    .line 1539
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1540
    .line 1541
    .line 1542
    const/16 v1, 0x2d0

    .line 1543
    .line 1544
    const-string v2, "3dstyle.skewangle"

    .line 1545
    .line 1546
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1547
    .line 1548
    .line 1549
    const/16 v1, 0x2d1

    .line 1550
    .line 1551
    const-string v2, "3dstyle.skewamount"

    .line 1552
    .line 1553
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1554
    .line 1555
    .line 1556
    const/16 v1, 0x2d2

    .line 1557
    .line 1558
    const-string v2, "3dstyle.ambientintensity"

    .line 1559
    .line 1560
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1561
    .line 1562
    .line 1563
    const/16 v1, 0x2d3

    .line 1564
    .line 1565
    const-string v2, "3dstyle.keyx"

    .line 1566
    .line 1567
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1568
    .line 1569
    .line 1570
    const/16 v1, 0x2d4

    .line 1571
    .line 1572
    const-string v2, "3dstyle.keyy"

    .line 1573
    .line 1574
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1575
    .line 1576
    .line 1577
    const/16 v1, 0x2d5

    .line 1578
    .line 1579
    const-string v2, "3dstyle.keyz"

    .line 1580
    .line 1581
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1582
    .line 1583
    .line 1584
    const/16 v1, 0x2d6

    .line 1585
    .line 1586
    const-string v2, "3dstyle.keyintensity"

    .line 1587
    .line 1588
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1589
    .line 1590
    .line 1591
    const/16 v1, 0x2d7

    .line 1592
    .line 1593
    const-string v2, "3dstyle.fillx"

    .line 1594
    .line 1595
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1596
    .line 1597
    .line 1598
    const/16 v1, 0x2d8

    .line 1599
    .line 1600
    const-string v2, "3dstyle.filly"

    .line 1601
    .line 1602
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1603
    .line 1604
    .line 1605
    const/16 v1, 0x2d9

    .line 1606
    .line 1607
    const-string v2, "3dstyle.fillz"

    .line 1608
    .line 1609
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1610
    .line 1611
    .line 1612
    const/16 v1, 0x2da

    .line 1613
    .line 1614
    const-string v2, "3dstyle.fillintensity"

    .line 1615
    .line 1616
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1617
    .line 1618
    .line 1619
    const/16 v1, 0x2fb

    .line 1620
    .line 1621
    const-string v2, "3dstyle.constrainrotation"

    .line 1622
    .line 1623
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1624
    .line 1625
    .line 1626
    const/16 v1, 0x2fc

    .line 1627
    .line 1628
    const-string v2, "3dstyle.rotationcenterauto"

    .line 1629
    .line 1630
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1631
    .line 1632
    .line 1633
    const/16 v1, 0x2fd

    .line 1634
    .line 1635
    const-string v2, "3dstyle.parallel"

    .line 1636
    .line 1637
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1638
    .line 1639
    .line 1640
    const/16 v1, 0x2fe

    .line 1641
    .line 1642
    const-string v2, "3dstyle.keyharsh"

    .line 1643
    .line 1644
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1645
    .line 1646
    .line 1647
    const/16 v1, 0x2ff

    .line 1648
    .line 1649
    const-string v2, "3dstyle.fillharsh"

    .line 1650
    .line 1651
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1652
    .line 1653
    .line 1654
    const/16 v1, 0x301

    .line 1655
    .line 1656
    const-string v2, "shape.master"

    .line 1657
    .line 1658
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1659
    .line 1660
    .line 1661
    const/16 v1, 0x303

    .line 1662
    .line 1663
    const-string v2, "shape.connectorstyle"

    .line 1664
    .line 1665
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1666
    .line 1667
    .line 1668
    const/16 v1, 0x304

    .line 1669
    .line 1670
    const-string v2, "shape.blackandwhitesettings"

    .line 1671
    .line 1672
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1673
    .line 1674
    .line 1675
    const/16 v1, 0x305

    .line 1676
    .line 1677
    const-string v2, "shape.wmodepurebw"

    .line 1678
    .line 1679
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1680
    .line 1681
    .line 1682
    const/16 v1, 0x306

    .line 1683
    .line 1684
    const-string v2, "shape.wmodebw"

    .line 1685
    .line 1686
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1687
    .line 1688
    .line 1689
    const/16 v1, 0x33a

    .line 1690
    .line 1691
    const-string v2, "shape.oleicon"

    .line 1692
    .line 1693
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1694
    .line 1695
    .line 1696
    const/16 v1, 0x33b

    .line 1697
    .line 1698
    const-string v2, "shape.preferrelativeresize"

    .line 1699
    .line 1700
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1701
    .line 1702
    .line 1703
    const/16 v1, 0x33c

    .line 1704
    .line 1705
    const-string v2, "shape.lockshapetype"

    .line 1706
    .line 1707
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1708
    .line 1709
    .line 1710
    const/16 v1, 0x33e

    .line 1711
    .line 1712
    const-string v2, "shape.deleteattachedobject"

    .line 1713
    .line 1714
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1715
    .line 1716
    .line 1717
    const/16 v1, 0x33f

    .line 1718
    .line 1719
    const-string v2, "shape.backgroundshape"

    .line 1720
    .line 1721
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1722
    .line 1723
    .line 1724
    const/16 v1, 0x340

    .line 1725
    .line 1726
    const-string v2, "callout.callouttype"

    .line 1727
    .line 1728
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1729
    .line 1730
    .line 1731
    const/16 v1, 0x341

    .line 1732
    .line 1733
    const-string v2, "callout.xycalloutgap"

    .line 1734
    .line 1735
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1736
    .line 1737
    .line 1738
    const/16 v1, 0x342

    .line 1739
    .line 1740
    const-string v2, "callout.calloutangle"

    .line 1741
    .line 1742
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1743
    .line 1744
    .line 1745
    const/16 v1, 0x343

    .line 1746
    .line 1747
    const-string v2, "callout.calloutdroptype"

    .line 1748
    .line 1749
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1750
    .line 1751
    .line 1752
    const/16 v1, 0x344

    .line 1753
    .line 1754
    const-string v2, "callout.calloutdropspecified"

    .line 1755
    .line 1756
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1757
    .line 1758
    .line 1759
    const/16 v1, 0x345

    .line 1760
    .line 1761
    const-string v2, "callout.calloutlengthspecified"

    .line 1762
    .line 1763
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1764
    .line 1765
    .line 1766
    const/16 v1, 0x379

    .line 1767
    .line 1768
    const-string v2, "callout.iscallout"

    .line 1769
    .line 1770
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1771
    .line 1772
    .line 1773
    const/16 v1, 0x37a

    .line 1774
    .line 1775
    const-string v2, "callout.calloutaccentbar"

    .line 1776
    .line 1777
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1778
    .line 1779
    .line 1780
    const/16 v1, 0x37b

    .line 1781
    .line 1782
    const-string v2, "callout.callouttextborder"

    .line 1783
    .line 1784
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1785
    .line 1786
    .line 1787
    const/16 v1, 0x37c

    .line 1788
    .line 1789
    const-string v2, "callout.calloutminusx"

    .line 1790
    .line 1791
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1792
    .line 1793
    .line 1794
    const/16 v1, 0x37d

    .line 1795
    .line 1796
    const-string v2, "callout.calloutminusy"

    .line 1797
    .line 1798
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1799
    .line 1800
    .line 1801
    const/16 v1, 0x37e

    .line 1802
    .line 1803
    const-string v2, "callout.dropauto"

    .line 1804
    .line 1805
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1806
    .line 1807
    .line 1808
    const/16 v1, 0x37f

    .line 1809
    .line 1810
    const-string v2, "callout.lengthspecified"

    .line 1811
    .line 1812
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1813
    .line 1814
    .line 1815
    const/16 v1, 0x380

    .line 1816
    .line 1817
    const-string v2, "groupshape.shapename"

    .line 1818
    .line 1819
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1820
    .line 1821
    .line 1822
    const/16 v1, 0x381

    .line 1823
    .line 1824
    const-string v2, "groupshape.description"

    .line 1825
    .line 1826
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1827
    .line 1828
    .line 1829
    const/16 v1, 0x382

    .line 1830
    .line 1831
    const-string v2, "groupshape.hyperlink"

    .line 1832
    .line 1833
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1834
    .line 1835
    .line 1836
    const/16 v1, 0x383

    .line 1837
    .line 1838
    const-string v2, "groupshape.wrappolygonvertices"

    .line 1839
    .line 1840
    invoke-static {v0, v1, v2, v4}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 1841
    .line 1842
    .line 1843
    const/16 v1, 0x384

    .line 1844
    .line 1845
    const-string v2, "groupshape.wrapdistleft"

    .line 1846
    .line 1847
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1848
    .line 1849
    .line 1850
    const/16 v1, 0x385

    .line 1851
    .line 1852
    const-string v2, "groupshape.wrapdisttop"

    .line 1853
    .line 1854
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1855
    .line 1856
    .line 1857
    const/16 v1, 0x386

    .line 1858
    .line 1859
    const-string v2, "groupshape.wrapdistright"

    .line 1860
    .line 1861
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1862
    .line 1863
    .line 1864
    const/16 v1, 0x387

    .line 1865
    .line 1866
    const-string v2, "groupshape.wrapdistbottom"

    .line 1867
    .line 1868
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1869
    .line 1870
    .line 1871
    const/16 v1, 0x388

    .line 1872
    .line 1873
    const-string v2, "groupshape.regroupid"

    .line 1874
    .line 1875
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1876
    .line 1877
    .line 1878
    const/16 v1, 0x38a

    .line 1879
    .line 1880
    const-string v2, "unused906"

    .line 1881
    .line 1882
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1883
    .line 1884
    .line 1885
    const/16 v1, 0x38d

    .line 1886
    .line 1887
    const-string v2, "groupshape.wzTooltip"

    .line 1888
    .line 1889
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1890
    .line 1891
    .line 1892
    const/16 v1, 0x38e

    .line 1893
    .line 1894
    const-string v2, "groupshape.wzScript"

    .line 1895
    .line 1896
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1897
    .line 1898
    .line 1899
    const/16 v1, 0x38f

    .line 1900
    .line 1901
    const-string v2, "groupshape.posh"

    .line 1902
    .line 1903
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1904
    .line 1905
    .line 1906
    const/16 v1, 0x390

    .line 1907
    .line 1908
    const-string v2, "groupshape.posrelh"

    .line 1909
    .line 1910
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1911
    .line 1912
    .line 1913
    const/16 v1, 0x391

    .line 1914
    .line 1915
    const-string v2, "groupshape.posv"

    .line 1916
    .line 1917
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1918
    .line 1919
    .line 1920
    const/16 v1, 0x392

    .line 1921
    .line 1922
    const-string v2, "groupshape.posrelv"

    .line 1923
    .line 1924
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1925
    .line 1926
    .line 1927
    const/16 v1, 0x393

    .line 1928
    .line 1929
    const-string v2, "groupshape.pctHR"

    .line 1930
    .line 1931
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1932
    .line 1933
    .line 1934
    const/16 v1, 0x394

    .line 1935
    .line 1936
    const-string v2, "groupshape.alignHR"

    .line 1937
    .line 1938
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1939
    .line 1940
    .line 1941
    const/16 v1, 0x395

    .line 1942
    .line 1943
    const-string v2, "groupshape.dxHeightHR"

    .line 1944
    .line 1945
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1946
    .line 1947
    .line 1948
    const/16 v1, 0x396

    .line 1949
    .line 1950
    const-string v2, "groupshape.dxWidthHR"

    .line 1951
    .line 1952
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1953
    .line 1954
    .line 1955
    const/16 v1, 0x397

    .line 1956
    .line 1957
    const-string v2, "groupshape.wzScriptExtAttr"

    .line 1958
    .line 1959
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1960
    .line 1961
    .line 1962
    const/16 v1, 0x398

    .line 1963
    .line 1964
    const-string v2, "groupshape.scriptLang"

    .line 1965
    .line 1966
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1967
    .line 1968
    .line 1969
    const/16 v1, 0x39b

    .line 1970
    .line 1971
    const-string v2, "groupshape.borderTopColor"

    .line 1972
    .line 1973
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1974
    .line 1975
    .line 1976
    const/16 v1, 0x39c

    .line 1977
    .line 1978
    const-string v2, "groupshape.borderLeftColor"

    .line 1979
    .line 1980
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1981
    .line 1982
    .line 1983
    const/16 v1, 0x39d

    .line 1984
    .line 1985
    const-string v2, "groupshape.borderBottomColor"

    .line 1986
    .line 1987
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1988
    .line 1989
    .line 1990
    const/16 v1, 0x39e

    .line 1991
    .line 1992
    const-string v2, "groupshape.borderRightColor"

    .line 1993
    .line 1994
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 1995
    .line 1996
    .line 1997
    const/16 v1, 0x39f

    .line 1998
    .line 1999
    const-string v2, "groupshape.tableProperties"

    .line 2000
    .line 2001
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 2002
    .line 2003
    .line 2004
    const/16 v1, 0x3a0

    .line 2005
    .line 2006
    const-string v2, "groupshape.tableRowProperties"

    .line 2007
    .line 2008
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 2009
    .line 2010
    .line 2011
    const/16 v1, 0x3a5

    .line 2012
    .line 2013
    const-string v2, "groupshape.wzWebBot"

    .line 2014
    .line 2015
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 2016
    .line 2017
    .line 2018
    const/16 v1, 0x3a9

    .line 2019
    .line 2020
    const-string v2, "groupshape.metroBlob"

    .line 2021
    .line 2022
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 2023
    .line 2024
    .line 2025
    const/16 v1, 0x3aa

    .line 2026
    .line 2027
    const-string v2, "groupshape.dhgt"

    .line 2028
    .line 2029
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 2030
    .line 2031
    .line 2032
    const-string v1, "groupshape.GroupShapeBooleanProperties"

    .line 2033
    .line 2034
    const/16 v2, 0x3bf

    .line 2035
    .line 2036
    invoke-static {v0, v2, v1}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 2037
    .line 2038
    .line 2039
    const/16 v1, 0x3b9

    .line 2040
    .line 2041
    const-string v4, "groupshape.editedwrap"

    .line 2042
    .line 2043
    invoke-static {v0, v1, v4}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 2044
    .line 2045
    .line 2046
    const/16 v1, 0x3ba

    .line 2047
    .line 2048
    const-string v4, "groupshape.behinddocument"

    .line 2049
    .line 2050
    invoke-static {v0, v1, v4}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 2051
    .line 2052
    .line 2053
    const/16 v1, 0x3bb

    .line 2054
    .line 2055
    const-string v4, "groupshape.ondblclicknotify"

    .line 2056
    .line 2057
    invoke-static {v0, v1, v4}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 2058
    .line 2059
    .line 2060
    const/16 v1, 0x3bc

    .line 2061
    .line 2062
    const-string v4, "groupshape.isbutton"

    .line 2063
    .line 2064
    invoke-static {v0, v1, v4}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 2065
    .line 2066
    .line 2067
    const/16 v1, 0x3bd

    .line 2068
    .line 2069
    const-string v4, "groupshape.1dadjustment"

    .line 2070
    .line 2071
    invoke-static {v0, v1, v4}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 2072
    .line 2073
    .line 2074
    const/16 v1, 0x3be

    .line 2075
    .line 2076
    const-string v4, "groupshape.hidden"

    .line 2077
    .line 2078
    invoke-static {v0, v1, v4}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;)V

    .line 2079
    .line 2080
    .line 2081
    const-string v1, "groupshape.print"

    .line 2082
    .line 2083
    invoke-static {v0, v2, v1, v3}, Lcom/intsig/office/fc/ddf/EscherProperties;->addProp(Ljava/util/Map;ILjava/lang/String;B)V

    .line 2084
    .line 2085
    .line 2086
    return-object v0
.end method

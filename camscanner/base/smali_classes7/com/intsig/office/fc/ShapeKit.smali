.class public Lcom/intsig/office/fc/ShapeKit;
.super Ljava/lang/Object;
.source "ShapeKit.java"


# static fields
.field public static final DefaultMargin_Pixel:F = 4.8f

.field public static final DefaultMargin_Twip:I = 0x48

.field public static final EMU_PER_CENTIMETER:I = 0x57e40

.field public static final EMU_PER_INCH:I = 0xdf3e0

.field public static final EMU_PER_POINT:I = 0x319c

.field public static final MASTER_DPI:I = 0x240

.field public static final SEGMENTINFO_CLOSE:[B

.field public static final SEGMENTINFO_CUBICTO:[B

.field public static final SEGMENTINFO_CUBICTO1:[B

.field public static final SEGMENTINFO_CUBICTO2:[B

.field public static final SEGMENTINFO_END:[B

.field public static final SEGMENTINFO_ESCAPE:[B

.field public static final SEGMENTINFO_ESCAPE1:[B

.field public static final SEGMENTINFO_ESCAPE2:[B

.field public static final SEGMENTINFO_LINETO:[B

.field public static final SEGMENTINFO_LINETO2:[B

.field public static final SEGMENTINFO_MOVETO:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v1, v0, [B

    .line 3
    .line 4
    fill-array-data v1, :array_0

    .line 5
    .line 6
    .line 7
    sput-object v1, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_MOVETO:[B

    .line 8
    .line 9
    new-array v1, v0, [B

    .line 10
    .line 11
    fill-array-data v1, :array_1

    .line 12
    .line 13
    .line 14
    sput-object v1, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO:[B

    .line 15
    .line 16
    new-array v1, v0, [B

    .line 17
    .line 18
    fill-array-data v1, :array_2

    .line 19
    .line 20
    .line 21
    sput-object v1, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO2:[B

    .line 22
    .line 23
    new-array v1, v0, [B

    .line 24
    .line 25
    fill-array-data v1, :array_3

    .line 26
    .line 27
    .line 28
    sput-object v1, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE:[B

    .line 29
    .line 30
    new-array v1, v0, [B

    .line 31
    .line 32
    fill-array-data v1, :array_4

    .line 33
    .line 34
    .line 35
    sput-object v1, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE1:[B

    .line 36
    .line 37
    new-array v1, v0, [B

    .line 38
    .line 39
    fill-array-data v1, :array_5

    .line 40
    .line 41
    .line 42
    sput-object v1, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE2:[B

    .line 43
    .line 44
    new-array v1, v0, [B

    .line 45
    .line 46
    fill-array-data v1, :array_6

    .line 47
    .line 48
    .line 49
    sput-object v1, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO:[B

    .line 50
    .line 51
    new-array v1, v0, [B

    .line 52
    .line 53
    fill-array-data v1, :array_7

    .line 54
    .line 55
    .line 56
    sput-object v1, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO1:[B

    .line 57
    .line 58
    new-array v1, v0, [B

    .line 59
    .line 60
    fill-array-data v1, :array_8

    .line 61
    .line 62
    .line 63
    sput-object v1, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO2:[B

    .line 64
    .line 65
    new-array v1, v0, [B

    .line 66
    .line 67
    fill-array-data v1, :array_9

    .line 68
    .line 69
    .line 70
    sput-object v1, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CLOSE:[B

    .line 71
    .line 72
    new-array v0, v0, [B

    .line 73
    .line 74
    fill-array-data v0, :array_a

    .line 75
    .line 76
    .line 77
    sput-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_END:[B

    .line 78
    .line 79
    return-void

    .line 80
    nop

    .line 81
    :array_0
    .array-data 1
        0x0t
        0x40t
    .end array-data

    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    nop

    .line 87
    :array_1
    .array-data 1
        0x0t
        -0x54t
    .end array-data

    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    nop

    .line 93
    :array_2
    .array-data 1
        0x0t
        -0x50t
    .end array-data

    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    nop

    .line 99
    :array_3
    .array-data 1
        0x1t
        0x0t
    .end array-data

    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    nop

    .line 105
    :array_4
    .array-data 1
        0x3t
        0x0t
    .end array-data

    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    nop

    .line 111
    :array_5
    .array-data 1
        0x1t
        0x20t
    .end array-data

    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    nop

    .line 117
    :array_6
    .array-data 1
        0x0t
        -0x53t
    .end array-data

    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    nop

    .line 123
    :array_7
    .array-data 1
        0x0t
        -0x51t
    .end array-data

    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    nop

    .line 129
    :array_8
    .array-data 1
        0x0t
        -0x4dt
    .end array-data

    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    nop

    .line 135
    :array_9
    .array-data 1
        0x1t
        0x60t
    .end array-data

    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    nop

    .line 141
    :array_a
    .array-data 1
        0x0t
        -0x80t
    .end array-data
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static getAdjustmentValue(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)[Ljava/lang/Float;
    .locals 6

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    if-eqz p0, :cond_3

    .line 10
    .line 11
    new-instance v0, Ljava/util/HashMap;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 14
    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    const/4 v2, 0x0

    .line 18
    const/4 v3, 0x0

    .line 19
    :goto_0
    const/16 v4, 0xa

    .line 20
    .line 21
    if-ge v2, v4, :cond_1

    .line 22
    .line 23
    add-int/lit16 v4, v2, 0x147

    .line 24
    .line 25
    invoke-static {p0, v4}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 26
    .line 27
    .line 28
    move-result-object v4

    .line 29
    check-cast v4, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 30
    .line 31
    if-eqz v4, :cond_0

    .line 32
    .line 33
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 34
    .line 35
    .line 36
    move-result-object v5

    .line 37
    invoke-virtual {v4}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 38
    .line 39
    .line 40
    move-result v4

    .line 41
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 42
    .line 43
    .line 44
    move-result-object v4

    .line 45
    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    .line 47
    .line 48
    if-le v2, v3, :cond_0

    .line 49
    .line 50
    move v3, v2

    .line 51
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_1
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 55
    .line 56
    .line 57
    move-result p0

    .line 58
    if-lez p0, :cond_3

    .line 59
    .line 60
    add-int/lit8 p0, v3, 0x1

    .line 61
    .line 62
    new-array p0, p0, [Ljava/lang/Float;

    .line 63
    .line 64
    :goto_1
    if-gt v1, v3, :cond_4

    .line 65
    .line 66
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    .line 72
    .line 73
    move-result-object v2

    .line 74
    check-cast v2, Ljava/lang/Integer;

    .line 75
    .line 76
    if-eqz v2, :cond_2

    .line 77
    .line 78
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 79
    .line 80
    .line 81
    move-result v2

    .line 82
    int-to-float v2, v2

    .line 83
    const v4, 0x46a8c000    # 21600.0f

    .line 84
    .line 85
    .line 86
    div-float/2addr v2, v4

    .line 87
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 88
    .line 89
    .line 90
    move-result-object v2

    .line 91
    aput-object v2, p0, v1

    .line 92
    .line 93
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 94
    .line 95
    goto :goto_1

    .line 96
    :cond_3
    const/4 p0, 0x0

    .line 97
    :cond_4
    return-object p0
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public static getColorByIndex(Ljava/lang/Object;IIZ)I
    .locals 7

    .line 1
    const v0, 0x100001f0

    .line 2
    .line 3
    .line 4
    const/4 v1, 0x2

    .line 5
    const/4 v2, -0x1

    .line 6
    const/high16 v3, -0x1000000

    .line 7
    .line 8
    if-lt p1, v0, :cond_1

    .line 9
    .line 10
    if-ne p2, v1, :cond_0

    .line 11
    .line 12
    return v2

    .line 13
    :cond_0
    return v3

    .line 14
    :cond_1
    const/high16 v0, 0xff0000

    .line 15
    .line 16
    const v4, 0xff00

    .line 17
    .line 18
    .line 19
    const v5, 0xffffff

    .line 20
    .line 21
    .line 22
    const/high16 v6, 0x8000000

    .line 23
    .line 24
    if-lt p1, v6, :cond_8

    .line 25
    .line 26
    rem-int v6, p1, v6

    .line 27
    .line 28
    if-ne p2, v1, :cond_3

    .line 29
    .line 30
    check-cast p0, Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 31
    .line 32
    if-eqz p0, :cond_9

    .line 33
    .line 34
    if-ltz v6, :cond_9

    .line 35
    .line 36
    const/4 p2, 0x7

    .line 37
    if-gt v6, p2, :cond_9

    .line 38
    .line 39
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getColorScheme()Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 40
    .line 41
    .line 42
    move-result-object p0

    .line 43
    if-eqz p0, :cond_2

    .line 44
    .line 45
    invoke-virtual {p0, v6}, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;->getColor(I)I

    .line 46
    .line 47
    .line 48
    move-result v6

    .line 49
    :cond_2
    if-gt v6, v5, :cond_9

    .line 50
    .line 51
    and-int/lit16 p0, v6, 0xff

    .line 52
    .line 53
    and-int p1, v6, v4

    .line 54
    .line 55
    shr-int/lit8 p1, p1, 0x8

    .line 56
    .line 57
    and-int p2, v6, v0

    .line 58
    .line 59
    shr-int/lit8 p2, p2, 0x10

    .line 60
    .line 61
    and-int/lit16 p0, p0, 0xff

    .line 62
    .line 63
    shl-int/lit8 p0, p0, 0x10

    .line 64
    .line 65
    or-int/2addr p0, v3

    .line 66
    and-int/lit16 p1, p1, 0xff

    .line 67
    .line 68
    shl-int/lit8 p1, p1, 0x8

    .line 69
    .line 70
    or-int/2addr p0, p1

    .line 71
    and-int/lit16 p1, p2, 0xff

    .line 72
    .line 73
    goto/16 :goto_1

    .line 74
    .line 75
    :cond_3
    const/4 v0, 0x1

    .line 76
    if-ne p2, v0, :cond_6

    .line 77
    .line 78
    const p2, 0x8000041

    .line 79
    .line 80
    .line 81
    if-gt p1, p2, :cond_5

    .line 82
    .line 83
    const/16 p2, 0x40

    .line 84
    .line 85
    if-lt v6, p2, :cond_4

    .line 86
    .line 87
    rem-int/lit8 v6, v6, 0x40

    .line 88
    .line 89
    add-int/lit8 v6, v6, 0x8

    .line 90
    .line 91
    :cond_4
    check-cast p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 92
    .line 93
    if-eqz p0, :cond_9

    .line 94
    .line 95
    invoke-virtual {p0, v6, p3}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getColor(IZ)I

    .line 96
    .line 97
    .line 98
    move-result p1

    .line 99
    goto/16 :goto_2

    .line 100
    .line 101
    :cond_5
    if-eqz p3, :cond_7

    .line 102
    .line 103
    goto :goto_0

    .line 104
    :cond_6
    const/16 p0, -0x100

    .line 105
    .line 106
    const/high16 p2, -0x10000

    .line 107
    .line 108
    const p3, -0xff01

    .line 109
    .line 110
    .line 111
    const v0, -0xff0100

    .line 112
    .line 113
    .line 114
    const v1, -0xffff01

    .line 115
    .line 116
    .line 117
    packed-switch v6, :pswitch_data_0

    .line 118
    .line 119
    .line 120
    goto :goto_2

    .line 121
    :pswitch_0
    const p0, -0x333334

    .line 122
    .line 123
    .line 124
    const p1, -0x333334

    .line 125
    .line 126
    .line 127
    goto :goto_2

    .line 128
    :pswitch_1
    const p0, -0x777778

    .line 129
    .line 130
    .line 131
    const p1, -0x777778

    .line 132
    .line 133
    .line 134
    goto :goto_2

    .line 135
    :pswitch_2
    const p0, -0xbbbbbc

    .line 136
    .line 137
    .line 138
    const p1, -0xbbbbbc

    .line 139
    .line 140
    .line 141
    goto :goto_2

    .line 142
    :cond_7
    :pswitch_3
    const/4 p1, -0x1

    .line 143
    goto :goto_2

    .line 144
    :pswitch_4
    const/16 p1, -0x100

    .line 145
    .line 146
    goto :goto_2

    .line 147
    :pswitch_5
    const/high16 p1, -0x10000

    .line 148
    .line 149
    goto :goto_2

    .line 150
    :pswitch_6
    const p1, -0xff01

    .line 151
    .line 152
    .line 153
    goto :goto_2

    .line 154
    :pswitch_7
    const p1, -0xff0100

    .line 155
    .line 156
    .line 157
    goto :goto_2

    .line 158
    :pswitch_8
    const p0, -0xff0001

    .line 159
    .line 160
    .line 161
    const p1, -0xff0001

    .line 162
    .line 163
    .line 164
    goto :goto_2

    .line 165
    :pswitch_9
    const p1, -0xffff01

    .line 166
    .line 167
    .line 168
    goto :goto_2

    .line 169
    :goto_0
    :pswitch_a
    const/high16 p1, -0x1000000

    .line 170
    .line 171
    goto :goto_2

    .line 172
    :cond_8
    if-gt p1, v5, :cond_9

    .line 173
    .line 174
    and-int/lit16 p0, p1, 0xff

    .line 175
    .line 176
    and-int p2, p1, v4

    .line 177
    .line 178
    shr-int/lit8 p2, p2, 0x8

    .line 179
    .line 180
    and-int/2addr p1, v0

    .line 181
    shr-int/lit8 p1, p1, 0x10

    .line 182
    .line 183
    and-int/lit16 p0, p0, 0xff

    .line 184
    .line 185
    shl-int/lit8 p0, p0, 0x10

    .line 186
    .line 187
    or-int/2addr p0, v3

    .line 188
    and-int/lit16 p2, p2, 0xff

    .line 189
    .line 190
    shl-int/lit8 p2, p2, 0x8

    .line 191
    .line 192
    or-int/2addr p0, p2

    .line 193
    and-int/lit16 p1, p1, 0xff

    .line 194
    .line 195
    :goto_1
    shl-int/lit8 p1, p1, 0x0

    .line 196
    .line 197
    or-int/2addr p1, p0

    .line 198
    :cond_9
    :goto_2
    return p1

    .line 199
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_9
        :pswitch_2
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public static getEndArrowLength(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    if-eqz p0, :cond_0

    .line 10
    .line 11
    const/16 v0, 0x1d5

    .line 12
    .line 13
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-eqz p0, :cond_0

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 22
    .line 23
    .line 24
    move-result p0

    .line 25
    return p0

    .line 26
    :cond_0
    const/4 p0, 0x1

    .line 27
    return p0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getEndArrowPathAndTail(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;
    .locals 22

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    move-object/from16 v2, p0

    .line 6
    .line 7
    invoke-static {v2, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 12
    .line 13
    new-instance v3, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 14
    .line 15
    const/16 v4, 0x144

    .line 16
    .line 17
    const/4 v5, 0x4

    .line 18
    invoke-direct {v3, v4, v5}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, v3}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 22
    .line 23
    .line 24
    const/16 v3, 0x4145

    .line 25
    .line 26
    invoke-static {v1, v3}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 31
    .line 32
    if-nez v3, :cond_0

    .line 33
    .line 34
    const/16 v3, 0x145

    .line 35
    .line 36
    invoke-static {v1, v3}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 37
    .line 38
    .line 39
    move-result-object v3

    .line 40
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 41
    .line 42
    :cond_0
    const/16 v4, 0x4146

    .line 43
    .line 44
    invoke-static {v1, v4}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 45
    .line 46
    .line 47
    move-result-object v4

    .line 48
    check-cast v4, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 49
    .line 50
    if-nez v4, :cond_1

    .line 51
    .line 52
    const/16 v4, 0x146

    .line 53
    .line 54
    invoke-static {v1, v4}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 55
    .line 56
    .line 57
    move-result-object v4

    .line 58
    check-cast v4, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 59
    .line 60
    :cond_1
    const/4 v6, 0x0

    .line 61
    if-nez v3, :cond_2

    .line 62
    .line 63
    return-object v6

    .line 64
    :cond_2
    if-nez v4, :cond_3

    .line 65
    .line 66
    return-object v6

    .line 67
    :cond_3
    const/16 v7, 0x142

    .line 68
    .line 69
    invoke-static {v1, v7}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 70
    .line 71
    .line 72
    move-result-object v7

    .line 73
    check-cast v7, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 74
    .line 75
    const/16 v8, 0x143

    .line 76
    .line 77
    invoke-static {v1, v8}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 78
    .line 79
    .line 80
    move-result-object v8

    .line 81
    check-cast v8, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 82
    .line 83
    const/4 v9, 0x0

    .line 84
    if-eqz v7, :cond_4

    .line 85
    .line 86
    invoke-virtual {v7}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 87
    .line 88
    .line 89
    move-result v7

    .line 90
    int-to-float v7, v7

    .line 91
    goto :goto_0

    .line 92
    :cond_4
    const/4 v7, 0x0

    .line 93
    :goto_0
    if-eqz v8, :cond_5

    .line 94
    .line 95
    invoke-virtual {v8}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 96
    .line 97
    .line 98
    move-result v8

    .line 99
    int-to-float v8, v8

    .line 100
    goto :goto_1

    .line 101
    :cond_5
    const/4 v8, 0x0

    .line 102
    :goto_1
    new-instance v10, Landroid/graphics/Matrix;

    .line 103
    .line 104
    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    .line 105
    .line 106
    .line 107
    cmpl-float v11, v7, v9

    .line 108
    .line 109
    if-lez v11, :cond_6

    .line 110
    .line 111
    cmpl-float v9, v8, v9

    .line 112
    .line 113
    if-lez v9, :cond_6

    .line 114
    .line 115
    iget v9, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 116
    .line 117
    int-to-float v9, v9

    .line 118
    div-float/2addr v9, v7

    .line 119
    iget v11, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 120
    .line 121
    int-to-float v11, v11

    .line 122
    div-float/2addr v11, v8

    .line 123
    invoke-virtual {v10, v9, v11}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 124
    .line 125
    .line 126
    :cond_6
    const/16 v8, 0x1d1

    .line 127
    .line 128
    invoke-static {v1, v8}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 129
    .line 130
    .line 131
    move-result-object v1

    .line 132
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 133
    .line 134
    if-eqz v1, :cond_14

    .line 135
    .line 136
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 137
    .line 138
    .line 139
    move-result v8

    .line 140
    if-lez v8, :cond_14

    .line 141
    .line 142
    new-instance v8, Lcom/intsig/office/common/shape/Arrow;

    .line 143
    .line 144
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 145
    .line 146
    .line 147
    move-result v1

    .line 148
    int-to-byte v1, v1

    .line 149
    invoke-static/range {p0 .. p0}, Lcom/intsig/office/fc/ShapeKit;->getEndArrowWidth(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 150
    .line 151
    .line 152
    move-result v9

    .line 153
    invoke-static/range {p0 .. p0}, Lcom/intsig/office/fc/ShapeKit;->getEndArrowLength(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 154
    .line 155
    .line 156
    move-result v11

    .line 157
    invoke-direct {v8, v1, v9, v11}, Lcom/intsig/office/common/shape/Arrow;-><init>(BII)V

    .line 158
    .line 159
    .line 160
    invoke-static/range {p0 .. p0}, Lcom/intsig/office/fc/ShapeKit;->getLineWidth(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 161
    .line 162
    .line 163
    move-result v1

    .line 164
    int-to-float v1, v1

    .line 165
    const v2, 0x3faaaaab

    .line 166
    .line 167
    .line 168
    mul-float v1, v1, v2

    .line 169
    .line 170
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 171
    .line 172
    .line 173
    move-result v1

    .line 174
    invoke-virtual {v3}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    .line 175
    .line 176
    .line 177
    move-result v2

    .line 178
    invoke-virtual {v4}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    .line 179
    .line 180
    .line 181
    move-result v9

    .line 182
    const/4 v11, 0x0

    .line 183
    const/4 v12, 0x0

    .line 184
    const/4 v13, 0x0

    .line 185
    :goto_2
    if-ge v12, v9, :cond_c

    .line 186
    .line 187
    if-ge v13, v2, :cond_c

    .line 188
    .line 189
    invoke-virtual {v4, v12}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 190
    .line 191
    .line 192
    move-result-object v14

    .line 193
    sget-object v15, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_MOVETO:[B

    .line 194
    .line 195
    invoke-static {v14, v15}, Ljava/util/Arrays;->equals([B[B)Z

    .line 196
    .line 197
    .line 198
    move-result v15

    .line 199
    if-eqz v15, :cond_7

    .line 200
    .line 201
    add-int/lit8 v13, v13, 0x1

    .line 202
    .line 203
    goto :goto_5

    .line 204
    :cond_7
    sget-object v15, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO:[B

    .line 205
    .line 206
    invoke-static {v14, v15}, Ljava/util/Arrays;->equals([B[B)Z

    .line 207
    .line 208
    .line 209
    move-result v15

    .line 210
    if-nez v15, :cond_a

    .line 211
    .line 212
    sget-object v15, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO1:[B

    .line 213
    .line 214
    invoke-static {v14, v15}, Ljava/util/Arrays;->equals([B[B)Z

    .line 215
    .line 216
    .line 217
    move-result v15

    .line 218
    if-nez v15, :cond_a

    .line 219
    .line 220
    sget-object v15, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO2:[B

    .line 221
    .line 222
    invoke-static {v14, v15}, Ljava/util/Arrays;->equals([B[B)Z

    .line 223
    .line 224
    .line 225
    move-result v15

    .line 226
    if-nez v15, :cond_a

    .line 227
    .line 228
    sget-object v15, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE2:[B

    .line 229
    .line 230
    invoke-static {v14, v15}, Ljava/util/Arrays;->equals([B[B)Z

    .line 231
    .line 232
    .line 233
    move-result v15

    .line 234
    if-eqz v15, :cond_8

    .line 235
    .line 236
    goto :goto_3

    .line 237
    :cond_8
    sget-object v15, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO:[B

    .line 238
    .line 239
    invoke-static {v14, v15}, Ljava/util/Arrays;->equals([B[B)Z

    .line 240
    .line 241
    .line 242
    move-result v15

    .line 243
    if-nez v15, :cond_9

    .line 244
    .line 245
    sget-object v15, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO2:[B

    .line 246
    .line 247
    invoke-static {v14, v15}, Ljava/util/Arrays;->equals([B[B)Z

    .line 248
    .line 249
    .line 250
    move-result v15

    .line 251
    if-nez v15, :cond_9

    .line 252
    .line 253
    sget-object v15, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE:[B

    .line 254
    .line 255
    invoke-static {v14, v15}, Ljava/util/Arrays;->equals([B[B)Z

    .line 256
    .line 257
    .line 258
    move-result v15

    .line 259
    if-nez v15, :cond_9

    .line 260
    .line 261
    sget-object v15, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE1:[B

    .line 262
    .line 263
    invoke-static {v14, v15}, Ljava/util/Arrays;->equals([B[B)Z

    .line 264
    .line 265
    .line 266
    move-result v14

    .line 267
    if-eqz v14, :cond_b

    .line 268
    .line 269
    :cond_9
    add-int/lit8 v14, v13, 0x1

    .line 270
    .line 271
    if-gt v14, v2, :cond_b

    .line 272
    .line 273
    goto :goto_4

    .line 274
    :cond_a
    :goto_3
    add-int/lit8 v14, v13, 0x3

    .line 275
    .line 276
    if-gt v14, v2, :cond_b

    .line 277
    .line 278
    :goto_4
    move v13, v14

    .line 279
    :cond_b
    :goto_5
    add-int/lit8 v12, v12, 0x1

    .line 280
    .line 281
    goto :goto_2

    .line 282
    :cond_c
    invoke-virtual {v4, v12}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 283
    .line 284
    .line 285
    move-result-object v9

    .line 286
    :goto_6
    sget-object v13, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CLOSE:[B

    .line 287
    .line 288
    invoke-static {v9, v13}, Ljava/util/Arrays;->equals([B[B)Z

    .line 289
    .line 290
    .line 291
    move-result v13

    .line 292
    if-nez v13, :cond_d

    .line 293
    .line 294
    sget-object v13, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_END:[B

    .line 295
    .line 296
    invoke-static {v9, v13}, Ljava/util/Arrays;->equals([B[B)Z

    .line 297
    .line 298
    .line 299
    move-result v13

    .line 300
    if-eqz v13, :cond_e

    .line 301
    .line 302
    :cond_d
    if-lez v12, :cond_e

    .line 303
    .line 304
    add-int/lit8 v9, v12, -0x1

    .line 305
    .line 306
    invoke-virtual {v4, v12}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 307
    .line 308
    .line 309
    move-result-object v12

    .line 310
    move-object/from16 v21, v12

    .line 311
    .line 312
    move v12, v9

    .line 313
    move-object/from16 v9, v21

    .line 314
    .line 315
    goto :goto_6

    .line 316
    :cond_e
    sget-object v4, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO:[B

    .line 317
    .line 318
    invoke-static {v9, v4}, Ljava/util/Arrays;->equals([B[B)Z

    .line 319
    .line 320
    .line 321
    move-result v4

    .line 322
    const/16 v12, 0x8

    .line 323
    .line 324
    const/4 v13, 0x2

    .line 325
    if-nez v4, :cond_12

    .line 326
    .line 327
    sget-object v4, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO1:[B

    .line 328
    .line 329
    invoke-static {v9, v4}, Ljava/util/Arrays;->equals([B[B)Z

    .line 330
    .line 331
    .line 332
    move-result v4

    .line 333
    if-nez v4, :cond_12

    .line 334
    .line 335
    sget-object v4, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO2:[B

    .line 336
    .line 337
    invoke-static {v9, v4}, Ljava/util/Arrays;->equals([B[B)Z

    .line 338
    .line 339
    .line 340
    move-result v4

    .line 341
    if-nez v4, :cond_12

    .line 342
    .line 343
    sget-object v4, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE2:[B

    .line 344
    .line 345
    invoke-static {v9, v4}, Ljava/util/Arrays;->equals([B[B)Z

    .line 346
    .line 347
    .line 348
    move-result v4

    .line 349
    if-eqz v4, :cond_f

    .line 350
    .line 351
    goto/16 :goto_8

    .line 352
    .line 353
    :cond_f
    sget-object v4, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_MOVETO:[B

    .line 354
    .line 355
    invoke-static {v9, v4}, Ljava/util/Arrays;->equals([B[B)Z

    .line 356
    .line 357
    .line 358
    move-result v4

    .line 359
    if-nez v4, :cond_10

    .line 360
    .line 361
    sget-object v4, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO:[B

    .line 362
    .line 363
    invoke-static {v9, v4}, Ljava/util/Arrays;->equals([B[B)Z

    .line 364
    .line 365
    .line 366
    move-result v4

    .line 367
    if-nez v4, :cond_10

    .line 368
    .line 369
    sget-object v4, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO2:[B

    .line 370
    .line 371
    invoke-static {v9, v4}, Ljava/util/Arrays;->equals([B[B)Z

    .line 372
    .line 373
    .line 374
    move-result v4

    .line 375
    if-nez v4, :cond_10

    .line 376
    .line 377
    sget-object v4, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE:[B

    .line 378
    .line 379
    invoke-static {v9, v4}, Ljava/util/Arrays;->equals([B[B)Z

    .line 380
    .line 381
    .line 382
    move-result v4

    .line 383
    if-nez v4, :cond_10

    .line 384
    .line 385
    sget-object v4, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE1:[B

    .line 386
    .line 387
    invoke-static {v9, v4}, Ljava/util/Arrays;->equals([B[B)Z

    .line 388
    .line 389
    .line 390
    move-result v4

    .line 391
    if-eqz v4, :cond_14

    .line 392
    .line 393
    :cond_10
    add-int/lit8 v4, v2, -0x2

    .line 394
    .line 395
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 396
    .line 397
    .line 398
    move-result-object v4

    .line 399
    add-int/lit8 v2, v2, -0x1

    .line 400
    .line 401
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 402
    .line 403
    .line 404
    move-result-object v2

    .line 405
    array-length v3, v4

    .line 406
    if-ne v3, v12, :cond_11

    .line 407
    .line 408
    array-length v3, v2

    .line 409
    if-ne v3, v12, :cond_11

    .line 410
    .line 411
    invoke-static {v4, v11}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 412
    .line 413
    .line 414
    move-result v3

    .line 415
    int-to-float v3, v3

    .line 416
    invoke-static {v4, v5}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 417
    .line 418
    .line 419
    move-result v4

    .line 420
    int-to-float v4, v4

    .line 421
    invoke-static {v2, v11}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 422
    .line 423
    .line 424
    move-result v6

    .line 425
    int-to-float v6, v6

    .line 426
    invoke-static {v2, v5}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 427
    .line 428
    .line 429
    move-result v2

    .line 430
    int-to-float v2, v2

    .line 431
    move v14, v2

    .line 432
    move v11, v3

    .line 433
    move v12, v4

    .line 434
    move v13, v6

    .line 435
    goto :goto_7

    .line 436
    :cond_11
    invoke-static {v4, v11}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 437
    .line 438
    .line 439
    move-result v3

    .line 440
    int-to-float v3, v3

    .line 441
    invoke-static {v4, v13}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 442
    .line 443
    .line 444
    move-result v4

    .line 445
    int-to-float v4, v4

    .line 446
    invoke-static {v2, v11}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 447
    .line 448
    .line 449
    move-result v5

    .line 450
    int-to-float v5, v5

    .line 451
    invoke-static {v2, v13}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 452
    .line 453
    .line 454
    move-result v2

    .line 455
    int-to-float v2, v2

    .line 456
    move v14, v2

    .line 457
    move v11, v3

    .line 458
    move v12, v4

    .line 459
    move v13, v5

    .line 460
    :goto_7
    int-to-float v1, v1

    .line 461
    mul-float v1, v1, v7

    .line 462
    .line 463
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 464
    .line 465
    int-to-float v0, v0

    .line 466
    div-float/2addr v1, v0

    .line 467
    float-to-int v0, v1

    .line 468
    move-object v15, v8

    .line 469
    move/from16 v16, v0

    .line 470
    .line 471
    invoke-static/range {v11 .. v16}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 472
    .line 473
    .line 474
    move-result-object v6

    .line 475
    goto/16 :goto_a

    .line 476
    .line 477
    :cond_12
    :goto_8
    add-int/lit8 v4, v2, -0x4

    .line 478
    .line 479
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 480
    .line 481
    .line 482
    move-result-object v4

    .line 483
    add-int/lit8 v6, v2, -0x3

    .line 484
    .line 485
    invoke-virtual {v3, v6}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 486
    .line 487
    .line 488
    move-result-object v6

    .line 489
    add-int/lit8 v9, v2, -0x2

    .line 490
    .line 491
    invoke-virtual {v3, v9}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 492
    .line 493
    .line 494
    move-result-object v9

    .line 495
    add-int/lit8 v2, v2, -0x1

    .line 496
    .line 497
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 498
    .line 499
    .line 500
    move-result-object v2

    .line 501
    array-length v3, v4

    .line 502
    if-ne v3, v12, :cond_13

    .line 503
    .line 504
    array-length v3, v6

    .line 505
    if-ne v3, v12, :cond_13

    .line 506
    .line 507
    array-length v3, v9

    .line 508
    if-ne v3, v12, :cond_13

    .line 509
    .line 510
    array-length v3, v2

    .line 511
    if-ne v3, v12, :cond_13

    .line 512
    .line 513
    invoke-static {v4, v11}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 514
    .line 515
    .line 516
    move-result v3

    .line 517
    int-to-float v3, v3

    .line 518
    invoke-static {v4, v5}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 519
    .line 520
    .line 521
    move-result v4

    .line 522
    int-to-float v4, v4

    .line 523
    invoke-static {v6, v11}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 524
    .line 525
    .line 526
    move-result v12

    .line 527
    int-to-float v12, v12

    .line 528
    invoke-static {v6, v5}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 529
    .line 530
    .line 531
    move-result v6

    .line 532
    int-to-float v6, v6

    .line 533
    invoke-static {v9, v11}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 534
    .line 535
    .line 536
    move-result v13

    .line 537
    int-to-float v13, v13

    .line 538
    invoke-static {v9, v5}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 539
    .line 540
    .line 541
    move-result v9

    .line 542
    int-to-float v9, v9

    .line 543
    invoke-static {v2, v11}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 544
    .line 545
    .line 546
    move-result v11

    .line 547
    int-to-float v11, v11

    .line 548
    invoke-static {v2, v5}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 549
    .line 550
    .line 551
    move-result v2

    .line 552
    int-to-float v2, v2

    .line 553
    move/from16 v18, v2

    .line 554
    .line 555
    move v14, v6

    .line 556
    move/from16 v16, v9

    .line 557
    .line 558
    move/from16 v17, v11

    .line 559
    .line 560
    move v15, v13

    .line 561
    move v11, v3

    .line 562
    move v13, v12

    .line 563
    goto :goto_9

    .line 564
    :cond_13
    invoke-static {v4, v11}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 565
    .line 566
    .line 567
    move-result v3

    .line 568
    int-to-float v3, v3

    .line 569
    invoke-static {v4, v13}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 570
    .line 571
    .line 572
    move-result v4

    .line 573
    int-to-float v4, v4

    .line 574
    invoke-static {v6, v11}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 575
    .line 576
    .line 577
    move-result v5

    .line 578
    int-to-float v5, v5

    .line 579
    invoke-static {v6, v13}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 580
    .line 581
    .line 582
    move-result v6

    .line 583
    int-to-float v6, v6

    .line 584
    invoke-static {v9, v11}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 585
    .line 586
    .line 587
    move-result v12

    .line 588
    int-to-float v12, v12

    .line 589
    invoke-static {v9, v13}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 590
    .line 591
    .line 592
    move-result v9

    .line 593
    int-to-float v9, v9

    .line 594
    invoke-static {v2, v11}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 595
    .line 596
    .line 597
    move-result v11

    .line 598
    int-to-float v11, v11

    .line 599
    invoke-static {v2, v13}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 600
    .line 601
    .line 602
    move-result v2

    .line 603
    int-to-float v2, v2

    .line 604
    move/from16 v18, v2

    .line 605
    .line 606
    move v13, v5

    .line 607
    move v14, v6

    .line 608
    move/from16 v16, v9

    .line 609
    .line 610
    move/from16 v17, v11

    .line 611
    .line 612
    move v15, v12

    .line 613
    move v11, v3

    .line 614
    :goto_9
    move v12, v4

    .line 615
    int-to-float v1, v1

    .line 616
    mul-float v1, v1, v7

    .line 617
    .line 618
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 619
    .line 620
    int-to-float v0, v0

    .line 621
    div-float/2addr v1, v0

    .line 622
    float-to-int v0, v1

    .line 623
    move-object/from16 v19, v8

    .line 624
    .line 625
    move/from16 v20, v0

    .line 626
    .line 627
    invoke-static/range {v11 .. v20}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getCubicBezArrowPath(FFFFFFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 628
    .line 629
    .line 630
    move-result-object v6

    .line 631
    :cond_14
    :goto_a
    if-eqz v6, :cond_15

    .line 632
    .line 633
    invoke-virtual {v6}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 634
    .line 635
    .line 636
    move-result-object v0

    .line 637
    if-eqz v0, :cond_15

    .line 638
    .line 639
    invoke-virtual {v6}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 640
    .line 641
    .line 642
    move-result-object v0

    .line 643
    invoke-virtual {v0, v10}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 644
    .line 645
    .line 646
    :cond_15
    return-object v6
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method public static getEndArrowType(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    if-eqz p0, :cond_0

    .line 10
    .line 11
    const/16 v0, 0x1d1

    .line 12
    .line 13
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-eqz p0, :cond_0

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-lez v0, :cond_0

    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 28
    .line 29
    .line 30
    move-result p0

    .line 31
    return p0

    .line 32
    :cond_0
    const/4 p0, 0x0

    .line 33
    return p0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getEndArrowWidth(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    if-eqz p0, :cond_0

    .line 10
    .line 11
    const/16 v0, 0x1d4

    .line 12
    .line 13
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-eqz p0, :cond_0

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 22
    .line 23
    .line 24
    move-result p0

    .line 25
    return p0

    .line 26
    :cond_0
    const/4 p0, 0x1

    .line 27
    return p0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-ne v1, p1, :cond_0

    .line 22
    .line 23
    return-object v0

    .line 24
    :cond_1
    const/4 p0, 0x0

    .line 25
    return-object p0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static getEscherProperty(Lcom/intsig/office/fc/ddf/EscherContainerRecord;S)I
    .locals 1

    const/4 v0, 0x0

    .line 7
    invoke-static {p0, p1, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherContainerRecord;SI)I

    move-result p0

    return p0
.end method

.method public static getEscherProperty(Lcom/intsig/office/fc/ddf/EscherContainerRecord;SI)I
    .locals 1

    const/16 v0, -0xff5

    .line 4
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    move-result-object p0

    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    if-eqz p0, :cond_0

    .line 5
    invoke-static {p0, p1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    move-result-object p0

    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    if-eqz p0, :cond_0

    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result p0

    return p0

    :cond_0
    return p2
.end method

.method public static getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;
    .locals 2

    if-eqz p0, :cond_1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->getEscherProperties()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherProperty;->getPropertyNumber()S

    move-result v1

    if-ne v1, p1, :cond_0

    return-object v0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getFillAngle(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x18b

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 20
    .line 21
    .line 22
    move-result p0

    .line 23
    shr-int/lit8 p0, p0, 0x10

    .line 24
    .line 25
    rem-int/lit16 p0, p0, 0x168

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const/4 p0, 0x0

    .line 29
    :goto_0
    return p0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getFillFocus(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x18c

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 20
    .line 21
    .line 22
    move-result p0

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 p0, 0x0

    .line 25
    :goto_0
    return p0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getFillType(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x180

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    if-nez p0, :cond_0

    .line 18
    .line 19
    const/4 p0, 0x0

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 22
    .line 23
    .line 24
    move-result p0

    .line 25
    :goto_0
    return p0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getFillbackColor(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/lang/Object;I)Lcom/intsig/office/java/awt/Color;
    .locals 5

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x183

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    const/16 v1, 0x1bf

    .line 18
    .line 19
    invoke-static {p0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 24
    .line 25
    const/16 v2, 0x184

    .line 26
    .line 27
    invoke-static {p0, v2}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 28
    .line 29
    .line 30
    move-result-object p0

    .line 31
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 32
    .line 33
    const/4 v2, 0x0

    .line 34
    if-nez v1, :cond_0

    .line 35
    .line 36
    const/4 v1, 0x0

    .line 37
    goto :goto_0

    .line 38
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    :goto_0
    const/16 v3, 0xff

    .line 43
    .line 44
    if-nez p0, :cond_1

    .line 45
    .line 46
    const/16 p0, 0xff

    .line 47
    .line 48
    goto :goto_1

    .line 49
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 50
    .line 51
    .line 52
    move-result p0

    .line 53
    shr-int/lit8 p0, p0, 0x8

    .line 54
    .line 55
    and-int/2addr p0, v3

    .line 56
    :goto_1
    if-eqz v0, :cond_3

    .line 57
    .line 58
    and-int/lit8 v4, v1, 0x10

    .line 59
    .line 60
    if-nez v4, :cond_2

    .line 61
    .line 62
    if-nez v1, :cond_3

    .line 63
    .line 64
    :cond_2
    new-instance v1, Lcom/intsig/office/java/awt/Color;

    .line 65
    .line 66
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    invoke-static {p1, v0, p2, v2}, Lcom/intsig/office/fc/ShapeKit;->getColorByIndex(Ljava/lang/Object;IIZ)I

    .line 71
    .line 72
    .line 73
    move-result p1

    .line 74
    invoke-direct {v1, p1, p0}, Lcom/intsig/office/java/awt/Color;-><init>(II)V

    .line 75
    .line 76
    .line 77
    goto :goto_2

    .line 78
    :cond_3
    if-nez v1, :cond_4

    .line 79
    .line 80
    const/4 p0, 0x1

    .line 81
    if-ne p2, p0, :cond_4

    .line 82
    .line 83
    new-instance v1, Lcom/intsig/office/java/awt/Color;

    .line 84
    .line 85
    invoke-direct {v1, v3, v3, v3}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 86
    .line 87
    .line 88
    goto :goto_2

    .line 89
    :cond_4
    const/4 v1, 0x0

    .line 90
    :goto_2
    return-object v1
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static getFlipHorizontal(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z
    .locals 1

    .line 1
    const/16 v0, -0xff6

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->getFlags()I

    .line 10
    .line 11
    .line 12
    move-result p0

    .line 13
    and-int/lit8 p0, p0, 0x40

    .line 14
    .line 15
    if-eqz p0, :cond_0

    .line 16
    .line 17
    const/4 p0, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 p0, 0x0

    .line 20
    :goto_0
    return p0
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static getFlipVertical(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z
    .locals 1

    .line 1
    const/16 v0, -0xff6

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->getFlags()I

    .line 10
    .line 11
    .line 12
    move-result p0

    .line 13
    and-int/lit16 p0, p0, 0x80

    .line 14
    .line 15
    if-eqz p0, :cond_0

    .line 16
    .line 17
    const/4 p0, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 p0, 0x0

    .line 20
    :goto_0
    return p0
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static getForegroundColor(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/lang/Object;I)Lcom/intsig/office/java/awt/Color;
    .locals 6

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x181

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    const/16 v1, 0x1bf

    .line 18
    .line 19
    invoke-static {p0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 24
    .line 25
    const/16 v2, 0x182

    .line 26
    .line 27
    invoke-static {p0, v2}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 32
    .line 33
    const/16 v3, 0x105

    .line 34
    .line 35
    invoke-static {p0, v3}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 36
    .line 37
    .line 38
    move-result-object p0

    .line 39
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherComplexProperty;

    .line 40
    .line 41
    const/4 v3, 0x0

    .line 42
    if-nez v1, :cond_0

    .line 43
    .line 44
    const/4 v1, 0x0

    .line 45
    goto :goto_0

    .line 46
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    :goto_0
    const/16 v4, 0xff

    .line 51
    .line 52
    if-nez v2, :cond_1

    .line 53
    .line 54
    const/16 v2, 0xff

    .line 55
    .line 56
    goto :goto_1

    .line 57
    :cond_1
    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 58
    .line 59
    .line 60
    move-result v2

    .line 61
    mul-int/lit16 v2, v2, 0xff

    .line 62
    .line 63
    const/high16 v5, 0x10000

    .line 64
    .line 65
    div-int/2addr v2, v5

    .line 66
    :goto_1
    if-eqz v0, :cond_3

    .line 67
    .line 68
    and-int/lit8 v5, v1, 0x10

    .line 69
    .line 70
    if-nez v5, :cond_2

    .line 71
    .line 72
    if-nez v1, :cond_3

    .line 73
    .line 74
    :cond_2
    new-instance p0, Lcom/intsig/office/java/awt/Color;

    .line 75
    .line 76
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    invoke-static {p1, v0, p2, v3}, Lcom/intsig/office/fc/ShapeKit;->getColorByIndex(Ljava/lang/Object;IIZ)I

    .line 81
    .line 82
    .line 83
    move-result p1

    .line 84
    invoke-direct {p0, p1, v2}, Lcom/intsig/office/java/awt/Color;-><init>(II)V

    .line 85
    .line 86
    .line 87
    goto :goto_2

    .line 88
    :cond_3
    and-int/lit8 p1, v1, 0x10

    .line 89
    .line 90
    if-eqz p1, :cond_4

    .line 91
    .line 92
    if-nez p0, :cond_4

    .line 93
    .line 94
    new-instance p0, Lcom/intsig/office/java/awt/Color;

    .line 95
    .line 96
    invoke-direct {p0, v4, v4, v4, v2}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    .line 97
    .line 98
    .line 99
    goto :goto_2

    .line 100
    :cond_4
    const/4 p0, 0x0

    .line 101
    :goto_2
    return-object p0
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static getFreeformPath(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/java/awt/Rectangle;Landroid/graphics/PointF;BLandroid/graphics/PointF;B)[Landroid/graphics/Path;
    .locals 26

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    move/from16 v1, p3

    .line 4
    .line 5
    move/from16 v2, p5

    .line 6
    .line 7
    const/16 v3, -0xff5

    .line 8
    .line 9
    move-object/from16 v4, p0

    .line 10
    .line 11
    invoke-static {v4, v3}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 12
    .line 13
    .line 14
    move-result-object v3

    .line 15
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 16
    .line 17
    new-instance v4, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    const/16 v5, 0x144

    .line 20
    .line 21
    const/4 v6, 0x4

    .line 22
    invoke-direct {v4, v5, v6}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 26
    .line 27
    .line 28
    const/16 v4, 0x4145

    .line 29
    .line 30
    invoke-static {v3, v4}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 31
    .line 32
    .line 33
    move-result-object v4

    .line 34
    check-cast v4, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 35
    .line 36
    if-nez v4, :cond_0

    .line 37
    .line 38
    const/16 v4, 0x145

    .line 39
    .line 40
    invoke-static {v3, v4}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 41
    .line 42
    .line 43
    move-result-object v4

    .line 44
    check-cast v4, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 45
    .line 46
    :cond_0
    const/16 v5, 0x4146

    .line 47
    .line 48
    invoke-static {v3, v5}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 49
    .line 50
    .line 51
    move-result-object v5

    .line 52
    check-cast v5, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 53
    .line 54
    if-nez v5, :cond_1

    .line 55
    .line 56
    const/16 v5, 0x146

    .line 57
    .line 58
    invoke-static {v3, v5}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 59
    .line 60
    .line 61
    move-result-object v5

    .line 62
    check-cast v5, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 63
    .line 64
    :cond_1
    const/4 v7, 0x0

    .line 65
    if-nez v4, :cond_2

    .line 66
    .line 67
    return-object v7

    .line 68
    :cond_2
    if-nez v5, :cond_3

    .line 69
    .line 70
    return-object v7

    .line 71
    :cond_3
    const/16 v7, 0x142

    .line 72
    .line 73
    invoke-static {v3, v7}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 74
    .line 75
    .line 76
    move-result-object v7

    .line 77
    check-cast v7, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 78
    .line 79
    const/16 v8, 0x143

    .line 80
    .line 81
    invoke-static {v3, v8}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 82
    .line 83
    .line 84
    move-result-object v8

    .line 85
    check-cast v8, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 86
    .line 87
    const/4 v9, 0x0

    .line 88
    if-eqz v7, :cond_6

    .line 89
    .line 90
    invoke-virtual {v7}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 91
    .line 92
    .line 93
    move-result v10

    .line 94
    const/16 v11, 0x3e8

    .line 95
    .line 96
    if-ne v10, v11, :cond_6

    .line 97
    .line 98
    if-eqz v8, :cond_6

    .line 99
    .line 100
    invoke-virtual {v8}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 101
    .line 102
    .line 103
    move-result v10

    .line 104
    if-ne v10, v11, :cond_6

    .line 105
    .line 106
    const/16 v1, 0x147

    .line 107
    .line 108
    invoke-static {v3, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 113
    .line 114
    const/high16 v2, 0x447a0000    # 1000.0f

    .line 115
    .line 116
    if-eqz v1, :cond_4

    .line 117
    .line 118
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 119
    .line 120
    .line 121
    move-result v1

    .line 122
    int-to-float v1, v1

    .line 123
    div-float/2addr v1, v2

    .line 124
    goto :goto_0

    .line 125
    :cond_4
    const/4 v1, 0x0

    .line 126
    :goto_0
    const/16 v4, 0x149

    .line 127
    .line 128
    invoke-static {v3, v4}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 129
    .line 130
    .line 131
    move-result-object v3

    .line 132
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 133
    .line 134
    if-eqz v3, :cond_5

    .line 135
    .line 136
    invoke-virtual {v3}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 137
    .line 138
    .line 139
    move-result v3

    .line 140
    int-to-float v3, v3

    .line 141
    div-float/2addr v3, v2

    .line 142
    goto :goto_1

    .line 143
    :cond_5
    const/4 v3, 0x0

    .line 144
    :goto_1
    new-instance v2, Ljava/util/ArrayList;

    .line 145
    .line 146
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 147
    .line 148
    .line 149
    new-instance v4, Landroid/graphics/Path;

    .line 150
    .line 151
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 152
    .line 153
    .line 154
    invoke-virtual {v4, v9, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 155
    .line 156
    .line 157
    iget v5, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 158
    .line 159
    int-to-float v5, v5

    .line 160
    invoke-virtual {v4, v5, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 161
    .line 162
    .line 163
    iget v5, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 164
    .line 165
    int-to-float v5, v5

    .line 166
    iget v6, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 167
    .line 168
    int-to-float v6, v6

    .line 169
    mul-float v6, v6, v3

    .line 170
    .line 171
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 172
    .line 173
    .line 174
    iget v5, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 175
    .line 176
    int-to-float v5, v5

    .line 177
    mul-float v5, v5, v1

    .line 178
    .line 179
    iget v6, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 180
    .line 181
    int-to-float v6, v6

    .line 182
    mul-float v6, v6, v3

    .line 183
    .line 184
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 185
    .line 186
    .line 187
    iget v3, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 188
    .line 189
    int-to-float v3, v3

    .line 190
    mul-float v3, v3, v1

    .line 191
    .line 192
    iget v1, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 193
    .line 194
    int-to-float v1, v1

    .line 195
    invoke-virtual {v4, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 196
    .line 197
    .line 198
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 199
    .line 200
    int-to-float v0, v0

    .line 201
    invoke-virtual {v4, v9, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 202
    .line 203
    .line 204
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 205
    .line 206
    .line 207
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    .line 209
    .line 210
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 211
    .line 212
    .line 213
    move-result v0

    .line 214
    new-array v0, v0, [Landroid/graphics/Path;

    .line 215
    .line 216
    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 217
    .line 218
    .line 219
    move-result-object v0

    .line 220
    check-cast v0, [Landroid/graphics/Path;

    .line 221
    .line 222
    return-object v0

    .line 223
    :cond_6
    if-eqz v7, :cond_7

    .line 224
    .line 225
    invoke-virtual {v7}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 226
    .line 227
    .line 228
    move-result v3

    .line 229
    int-to-float v3, v3

    .line 230
    goto :goto_2

    .line 231
    :cond_7
    const/4 v3, 0x0

    .line 232
    :goto_2
    if-eqz v8, :cond_8

    .line 233
    .line 234
    invoke-virtual {v8}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 235
    .line 236
    .line 237
    move-result v7

    .line 238
    int-to-float v7, v7

    .line 239
    goto :goto_3

    .line 240
    :cond_8
    const/4 v7, 0x0

    .line 241
    :goto_3
    new-instance v8, Landroid/graphics/Matrix;

    .line 242
    .line 243
    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 244
    .line 245
    .line 246
    cmpl-float v10, v3, v9

    .line 247
    .line 248
    if-lez v10, :cond_9

    .line 249
    .line 250
    cmpl-float v9, v7, v9

    .line 251
    .line 252
    if-lez v9, :cond_9

    .line 253
    .line 254
    iget v9, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 255
    .line 256
    int-to-float v9, v9

    .line 257
    div-float/2addr v9, v3

    .line 258
    iget v10, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 259
    .line 260
    int-to-float v10, v10

    .line 261
    div-float/2addr v10, v7

    .line 262
    invoke-virtual {v8, v9, v10}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 263
    .line 264
    .line 265
    :cond_9
    new-instance v9, Ljava/util/ArrayList;

    .line 266
    .line 267
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 268
    .line 269
    .line 270
    invoke-virtual {v4}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    .line 271
    .line 272
    .line 273
    move-result v10

    .line 274
    invoke-virtual {v5}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    .line 275
    .line 276
    .line 277
    move-result v11

    .line 278
    new-instance v15, Landroid/graphics/Path;

    .line 279
    .line 280
    invoke-direct {v15}, Landroid/graphics/Path;-><init>()V

    .line 281
    .line 282
    .line 283
    move-object/from16 v13, p2

    .line 284
    .line 285
    move-object/from16 v12, p4

    .line 286
    .line 287
    const/4 v6, 0x0

    .line 288
    const/4 v14, 0x0

    .line 289
    :goto_4
    if-ge v6, v11, :cond_1d

    .line 290
    .line 291
    if-gt v14, v10, :cond_1d

    .line 292
    .line 293
    move/from16 v19, v11

    .line 294
    .line 295
    invoke-virtual {v5, v6}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 296
    .line 297
    .line 298
    move-result-object v11

    .line 299
    move-object/from16 v20, v5

    .line 300
    .line 301
    if-nez v14, :cond_b

    .line 302
    .line 303
    if-eqz v13, :cond_b

    .line 304
    .line 305
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_MOVETO:[B

    .line 306
    .line 307
    invoke-static {v11, v5}, Ljava/util/Arrays;->equals([B[B)Z

    .line 308
    .line 309
    .line 310
    move-result v5

    .line 311
    if-eqz v5, :cond_12

    .line 312
    .line 313
    add-int/lit8 v5, v14, 0x1

    .line 314
    .line 315
    invoke-virtual {v4, v14}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 316
    .line 317
    .line 318
    move-result-object v11

    .line 319
    array-length v14, v11

    .line 320
    move/from16 v17, v5

    .line 321
    .line 322
    const/16 v5, 0x8

    .line 323
    .line 324
    if-ne v14, v5, :cond_a

    .line 325
    .line 326
    const/4 v5, 0x0

    .line 327
    invoke-static {v11, v5}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 328
    .line 329
    .line 330
    move-result v14

    .line 331
    const/4 v5, 0x4

    .line 332
    invoke-static {v11, v5}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 333
    .line 334
    .line 335
    move-result v11

    .line 336
    int-to-float v5, v14

    .line 337
    int-to-float v11, v11

    .line 338
    iget v14, v13, Landroid/graphics/PointF;->x:F

    .line 339
    .line 340
    iget v13, v13, Landroid/graphics/PointF;->y:F

    .line 341
    .line 342
    invoke-static {v5, v11, v14, v13, v1}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    .line 343
    .line 344
    .line 345
    move-result-object v5

    .line 346
    goto :goto_5

    .line 347
    :cond_a
    const/4 v5, 0x0

    .line 348
    invoke-static {v11, v5}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 349
    .line 350
    .line 351
    move-result v14

    .line 352
    const/4 v5, 0x2

    .line 353
    invoke-static {v11, v5}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 354
    .line 355
    .line 356
    move-result v5

    .line 357
    int-to-float v11, v14

    .line 358
    int-to-float v5, v5

    .line 359
    iget v14, v13, Landroid/graphics/PointF;->x:F

    .line 360
    .line 361
    iget v13, v13, Landroid/graphics/PointF;->y:F

    .line 362
    .line 363
    invoke-static {v11, v5, v14, v13, v1}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    .line 364
    .line 365
    .line 366
    move-result-object v5

    .line 367
    :goto_5
    iget v11, v5, Landroid/graphics/PointF;->x:F

    .line 368
    .line 369
    iget v13, v5, Landroid/graphics/PointF;->y:F

    .line 370
    .line 371
    invoke-virtual {v15, v11, v13}, Landroid/graphics/Path;->moveTo(FF)V

    .line 372
    .line 373
    .line 374
    move-object v13, v5

    .line 375
    move-object/from16 v23, v8

    .line 376
    .line 377
    move-object/from16 v22, v9

    .line 378
    .line 379
    move-object v2, v15

    .line 380
    move/from16 v14, v17

    .line 381
    .line 382
    const/4 v1, 0x0

    .line 383
    goto/16 :goto_b

    .line 384
    .line 385
    :cond_b
    if-eqz v12, :cond_12

    .line 386
    .line 387
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO:[B

    .line 388
    .line 389
    invoke-static {v11, v5}, Ljava/util/Arrays;->equals([B[B)Z

    .line 390
    .line 391
    .line 392
    move-result v5

    .line 393
    if-nez v5, :cond_c

    .line 394
    .line 395
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO1:[B

    .line 396
    .line 397
    invoke-static {v11, v5}, Ljava/util/Arrays;->equals([B[B)Z

    .line 398
    .line 399
    .line 400
    move-result v5

    .line 401
    if-nez v5, :cond_c

    .line 402
    .line 403
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO2:[B

    .line 404
    .line 405
    invoke-static {v11, v5}, Ljava/util/Arrays;->equals([B[B)Z

    .line 406
    .line 407
    .line 408
    move-result v5

    .line 409
    if-nez v5, :cond_c

    .line 410
    .line 411
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE2:[B

    .line 412
    .line 413
    invoke-static {v11, v5}, Ljava/util/Arrays;->equals([B[B)Z

    .line 414
    .line 415
    .line 416
    move-result v5

    .line 417
    if-eqz v5, :cond_e

    .line 418
    .line 419
    :cond_c
    add-int/lit8 v5, v10, -0x3

    .line 420
    .line 421
    if-ne v14, v5, :cond_e

    .line 422
    .line 423
    add-int/lit8 v5, v14, 0x3

    .line 424
    .line 425
    if-gt v5, v10, :cond_12

    .line 426
    .line 427
    add-int/lit8 v5, v14, 0x1

    .line 428
    .line 429
    invoke-virtual {v4, v14}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 430
    .line 431
    .line 432
    move-result-object v11

    .line 433
    add-int/lit8 v14, v5, 0x1

    .line 434
    .line 435
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 436
    .line 437
    .line 438
    move-result-object v5

    .line 439
    add-int/lit8 v21, v14, 0x1

    .line 440
    .line 441
    invoke-virtual {v4, v14}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 442
    .line 443
    .line 444
    move-result-object v14

    .line 445
    array-length v1, v11

    .line 446
    move-object/from16 v17, v13

    .line 447
    .line 448
    const/16 v13, 0x8

    .line 449
    .line 450
    if-ne v1, v13, :cond_d

    .line 451
    .line 452
    array-length v1, v5

    .line 453
    if-ne v1, v13, :cond_d

    .line 454
    .line 455
    array-length v1, v14

    .line 456
    if-ne v1, v13, :cond_d

    .line 457
    .line 458
    const/4 v1, 0x0

    .line 459
    invoke-static {v11, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 460
    .line 461
    .line 462
    move-result v13

    .line 463
    move-object/from16 v22, v9

    .line 464
    .line 465
    const/4 v9, 0x4

    .line 466
    invoke-static {v11, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 467
    .line 468
    .line 469
    move-result v11

    .line 470
    move-object/from16 v23, v8

    .line 471
    .line 472
    invoke-static {v5, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 473
    .line 474
    .line 475
    move-result v8

    .line 476
    invoke-static {v5, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 477
    .line 478
    .line 479
    move-result v5

    .line 480
    invoke-static {v14, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 481
    .line 482
    .line 483
    move-result v0

    .line 484
    invoke-static {v14, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 485
    .line 486
    .line 487
    move-result v14

    .line 488
    int-to-float v0, v0

    .line 489
    int-to-float v9, v14

    .line 490
    iget v14, v12, Landroid/graphics/PointF;->x:F

    .line 491
    .line 492
    iget v12, v12, Landroid/graphics/PointF;->y:F

    .line 493
    .line 494
    invoke-static {v0, v9, v14, v12, v2}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    .line 495
    .line 496
    .line 497
    move-result-object v0

    .line 498
    int-to-float v13, v13

    .line 499
    int-to-float v14, v11

    .line 500
    int-to-float v8, v8

    .line 501
    int-to-float v5, v5

    .line 502
    iget v9, v0, Landroid/graphics/PointF;->x:F

    .line 503
    .line 504
    iget v11, v0, Landroid/graphics/PointF;->y:F

    .line 505
    .line 506
    move-object v12, v15

    .line 507
    move-object/from16 v24, v17

    .line 508
    .line 509
    move-object/from16 v25, v15

    .line 510
    .line 511
    move v15, v8

    .line 512
    move/from16 v16, v5

    .line 513
    .line 514
    move/from16 v17, v9

    .line 515
    .line 516
    move/from16 v18, v11

    .line 517
    .line 518
    invoke-virtual/range {v12 .. v18}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 519
    .line 520
    .line 521
    move-object v12, v0

    .line 522
    goto :goto_6

    .line 523
    :cond_d
    move-object/from16 v23, v8

    .line 524
    .line 525
    move-object/from16 v22, v9

    .line 526
    .line 527
    move-object/from16 v25, v15

    .line 528
    .line 529
    move-object/from16 v24, v17

    .line 530
    .line 531
    const/4 v1, 0x0

    .line 532
    invoke-static {v11, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 533
    .line 534
    .line 535
    move-result v0

    .line 536
    const/4 v8, 0x2

    .line 537
    invoke-static {v11, v8}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 538
    .line 539
    .line 540
    move-result v9

    .line 541
    invoke-static {v5, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 542
    .line 543
    .line 544
    move-result v11

    .line 545
    invoke-static {v5, v8}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 546
    .line 547
    .line 548
    move-result v5

    .line 549
    invoke-static {v14, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 550
    .line 551
    .line 552
    move-result v13

    .line 553
    invoke-static {v14, v8}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 554
    .line 555
    .line 556
    move-result v8

    .line 557
    int-to-float v13, v13

    .line 558
    int-to-float v8, v8

    .line 559
    iget v14, v12, Landroid/graphics/PointF;->x:F

    .line 560
    .line 561
    iget v12, v12, Landroid/graphics/PointF;->y:F

    .line 562
    .line 563
    invoke-static {v13, v8, v14, v12, v2}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    .line 564
    .line 565
    .line 566
    move-result-object v8

    .line 567
    int-to-float v13, v0

    .line 568
    int-to-float v14, v9

    .line 569
    int-to-float v15, v11

    .line 570
    int-to-float v0, v5

    .line 571
    iget v5, v8, Landroid/graphics/PointF;->x:F

    .line 572
    .line 573
    iget v9, v8, Landroid/graphics/PointF;->y:F

    .line 574
    .line 575
    move-object/from16 v12, v25

    .line 576
    .line 577
    move/from16 v16, v0

    .line 578
    .line 579
    move/from16 v17, v5

    .line 580
    .line 581
    move/from16 v18, v9

    .line 582
    .line 583
    invoke-virtual/range {v12 .. v18}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 584
    .line 585
    .line 586
    move-object v12, v8

    .line 587
    :goto_6
    move/from16 v14, v21

    .line 588
    .line 589
    move-object/from16 v13, v24

    .line 590
    .line 591
    move-object/from16 v2, v25

    .line 592
    .line 593
    goto/16 :goto_b

    .line 594
    .line 595
    :cond_e
    move-object/from16 v23, v8

    .line 596
    .line 597
    move-object/from16 v22, v9

    .line 598
    .line 599
    move-object/from16 v24, v13

    .line 600
    .line 601
    move-object/from16 v25, v15

    .line 602
    .line 603
    const/4 v1, 0x0

    .line 604
    sget-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO:[B

    .line 605
    .line 606
    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 607
    .line 608
    .line 609
    move-result v0

    .line 610
    if-nez v0, :cond_f

    .line 611
    .line 612
    sget-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO2:[B

    .line 613
    .line 614
    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 615
    .line 616
    .line 617
    move-result v0

    .line 618
    if-nez v0, :cond_f

    .line 619
    .line 620
    sget-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE:[B

    .line 621
    .line 622
    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 623
    .line 624
    .line 625
    move-result v0

    .line 626
    if-nez v0, :cond_f

    .line 627
    .line 628
    sget-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE1:[B

    .line 629
    .line 630
    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 631
    .line 632
    .line 633
    move-result v0

    .line 634
    if-eqz v0, :cond_11

    .line 635
    .line 636
    :cond_f
    add-int/lit8 v0, v10, -0x1

    .line 637
    .line 638
    if-ne v14, v0, :cond_11

    .line 639
    .line 640
    add-int/lit8 v0, v14, 0x1

    .line 641
    .line 642
    if-gt v0, v10, :cond_11

    .line 643
    .line 644
    invoke-virtual {v4, v14}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 645
    .line 646
    .line 647
    move-result-object v5

    .line 648
    array-length v8, v5

    .line 649
    const/16 v9, 0x8

    .line 650
    .line 651
    if-ne v8, v9, :cond_10

    .line 652
    .line 653
    invoke-static {v5, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 654
    .line 655
    .line 656
    move-result v8

    .line 657
    const/4 v9, 0x4

    .line 658
    invoke-static {v5, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 659
    .line 660
    .line 661
    move-result v5

    .line 662
    int-to-float v8, v8

    .line 663
    int-to-float v5, v5

    .line 664
    iget v9, v12, Landroid/graphics/PointF;->x:F

    .line 665
    .line 666
    iget v11, v12, Landroid/graphics/PointF;->y:F

    .line 667
    .line 668
    invoke-static {v8, v5, v9, v11, v2}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    .line 669
    .line 670
    .line 671
    move-result-object v5

    .line 672
    iget v8, v5, Landroid/graphics/PointF;->x:F

    .line 673
    .line 674
    iget v9, v5, Landroid/graphics/PointF;->y:F

    .line 675
    .line 676
    move-object/from16 v15, v25

    .line 677
    .line 678
    invoke-virtual {v15, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 679
    .line 680
    .line 681
    goto :goto_7

    .line 682
    :cond_10
    move-object/from16 v15, v25

    .line 683
    .line 684
    invoke-static {v5, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 685
    .line 686
    .line 687
    move-result v8

    .line 688
    const/4 v9, 0x2

    .line 689
    invoke-static {v5, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 690
    .line 691
    .line 692
    move-result v5

    .line 693
    int-to-float v8, v8

    .line 694
    int-to-float v5, v5

    .line 695
    iget v9, v12, Landroid/graphics/PointF;->x:F

    .line 696
    .line 697
    iget v11, v12, Landroid/graphics/PointF;->y:F

    .line 698
    .line 699
    invoke-static {v8, v5, v9, v11, v2}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    .line 700
    .line 701
    .line 702
    move-result-object v5

    .line 703
    iget v8, v5, Landroid/graphics/PointF;->x:F

    .line 704
    .line 705
    iget v9, v5, Landroid/graphics/PointF;->y:F

    .line 706
    .line 707
    invoke-virtual {v15, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 708
    .line 709
    .line 710
    :goto_7
    move v14, v0

    .line 711
    move-object v12, v5

    .line 712
    goto :goto_a

    .line 713
    :cond_11
    move-object/from16 v15, v25

    .line 714
    .line 715
    goto :goto_8

    .line 716
    :cond_12
    move-object/from16 v23, v8

    .line 717
    .line 718
    move-object/from16 v22, v9

    .line 719
    .line 720
    move-object/from16 v24, v13

    .line 721
    .line 722
    const/4 v1, 0x0

    .line 723
    :goto_8
    sget-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_MOVETO:[B

    .line 724
    .line 725
    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 726
    .line 727
    .line 728
    move-result v0

    .line 729
    if-eqz v0, :cond_14

    .line 730
    .line 731
    add-int/lit8 v0, v14, 0x1

    .line 732
    .line 733
    invoke-virtual {v4, v14}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 734
    .line 735
    .line 736
    move-result-object v5

    .line 737
    array-length v8, v5

    .line 738
    const/16 v9, 0x8

    .line 739
    .line 740
    if-ne v8, v9, :cond_13

    .line 741
    .line 742
    invoke-static {v5, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 743
    .line 744
    .line 745
    move-result v8

    .line 746
    const/4 v9, 0x4

    .line 747
    invoke-static {v5, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 748
    .line 749
    .line 750
    move-result v5

    .line 751
    int-to-float v8, v8

    .line 752
    int-to-float v5, v5

    .line 753
    invoke-virtual {v15, v8, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 754
    .line 755
    .line 756
    goto :goto_9

    .line 757
    :cond_13
    invoke-static {v5, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 758
    .line 759
    .line 760
    move-result v8

    .line 761
    const/4 v9, 0x2

    .line 762
    invoke-static {v5, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 763
    .line 764
    .line 765
    move-result v5

    .line 766
    int-to-float v8, v8

    .line 767
    int-to-float v5, v5

    .line 768
    invoke-virtual {v15, v8, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 769
    .line 770
    .line 771
    :goto_9
    move v14, v0

    .line 772
    :goto_a
    move-object v2, v15

    .line 773
    move-object/from16 v13, v24

    .line 774
    .line 775
    :goto_b
    const/4 v11, 0x4

    .line 776
    goto/16 :goto_11

    .line 777
    .line 778
    :cond_14
    sget-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO:[B

    .line 779
    .line 780
    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 781
    .line 782
    .line 783
    move-result v0

    .line 784
    if-nez v0, :cond_1a

    .line 785
    .line 786
    sget-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO1:[B

    .line 787
    .line 788
    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 789
    .line 790
    .line 791
    move-result v0

    .line 792
    if-nez v0, :cond_1a

    .line 793
    .line 794
    sget-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO2:[B

    .line 795
    .line 796
    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 797
    .line 798
    .line 799
    move-result v0

    .line 800
    if-nez v0, :cond_1a

    .line 801
    .line 802
    sget-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE2:[B

    .line 803
    .line 804
    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 805
    .line 806
    .line 807
    move-result v0

    .line 808
    if-eqz v0, :cond_15

    .line 809
    .line 810
    goto :goto_d

    .line 811
    :cond_15
    sget-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO:[B

    .line 812
    .line 813
    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 814
    .line 815
    .line 816
    move-result v0

    .line 817
    if-nez v0, :cond_18

    .line 818
    .line 819
    sget-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO2:[B

    .line 820
    .line 821
    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 822
    .line 823
    .line 824
    move-result v0

    .line 825
    if-nez v0, :cond_18

    .line 826
    .line 827
    sget-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE:[B

    .line 828
    .line 829
    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 830
    .line 831
    .line 832
    move-result v0

    .line 833
    if-nez v0, :cond_18

    .line 834
    .line 835
    sget-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE1:[B

    .line 836
    .line 837
    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 838
    .line 839
    .line 840
    move-result v0

    .line 841
    if-eqz v0, :cond_16

    .line 842
    .line 843
    goto :goto_c

    .line 844
    :cond_16
    sget-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CLOSE:[B

    .line 845
    .line 846
    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 847
    .line 848
    .line 849
    move-result v0

    .line 850
    if-eqz v0, :cond_17

    .line 851
    .line 852
    invoke-virtual {v15}, Landroid/graphics/Path;->close()V

    .line 853
    .line 854
    .line 855
    goto/16 :goto_f

    .line 856
    .line 857
    :cond_17
    sget-object v0, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_END:[B

    .line 858
    .line 859
    invoke-static {v11, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 860
    .line 861
    .line 862
    goto/16 :goto_f

    .line 863
    .line 864
    :cond_18
    :goto_c
    add-int/lit8 v0, v14, 0x1

    .line 865
    .line 866
    if-gt v0, v10, :cond_1c

    .line 867
    .line 868
    invoke-virtual {v4, v14}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 869
    .line 870
    .line 871
    move-result-object v5

    .line 872
    array-length v8, v5

    .line 873
    const/16 v9, 0x8

    .line 874
    .line 875
    if-ne v8, v9, :cond_19

    .line 876
    .line 877
    invoke-static {v5, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 878
    .line 879
    .line 880
    move-result v8

    .line 881
    const/4 v9, 0x4

    .line 882
    invoke-static {v5, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 883
    .line 884
    .line 885
    move-result v5

    .line 886
    int-to-float v8, v8

    .line 887
    int-to-float v5, v5

    .line 888
    invoke-virtual {v15, v8, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 889
    .line 890
    .line 891
    goto :goto_9

    .line 892
    :cond_19
    invoke-static {v5, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 893
    .line 894
    .line 895
    move-result v8

    .line 896
    const/4 v9, 0x2

    .line 897
    invoke-static {v5, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 898
    .line 899
    .line 900
    move-result v5

    .line 901
    int-to-float v8, v8

    .line 902
    int-to-float v5, v5

    .line 903
    invoke-virtual {v15, v8, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 904
    .line 905
    .line 906
    goto/16 :goto_9

    .line 907
    .line 908
    :cond_1a
    :goto_d
    add-int/lit8 v0, v14, 0x3

    .line 909
    .line 910
    if-gt v0, v10, :cond_1c

    .line 911
    .line 912
    add-int/lit8 v0, v14, 0x1

    .line 913
    .line 914
    invoke-virtual {v4, v14}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 915
    .line 916
    .line 917
    move-result-object v5

    .line 918
    add-int/lit8 v8, v0, 0x1

    .line 919
    .line 920
    invoke-virtual {v4, v0}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 921
    .line 922
    .line 923
    move-result-object v0

    .line 924
    add-int/lit8 v9, v8, 0x1

    .line 925
    .line 926
    invoke-virtual {v4, v8}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 927
    .line 928
    .line 929
    move-result-object v8

    .line 930
    array-length v11, v5

    .line 931
    const/16 v13, 0x8

    .line 932
    .line 933
    if-ne v11, v13, :cond_1b

    .line 934
    .line 935
    array-length v11, v0

    .line 936
    if-ne v11, v13, :cond_1b

    .line 937
    .line 938
    array-length v11, v8

    .line 939
    if-ne v11, v13, :cond_1b

    .line 940
    .line 941
    invoke-static {v5, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 942
    .line 943
    .line 944
    move-result v11

    .line 945
    const/4 v14, 0x4

    .line 946
    invoke-static {v5, v14}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 947
    .line 948
    .line 949
    move-result v5

    .line 950
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 951
    .line 952
    .line 953
    move-result v13

    .line 954
    invoke-static {v0, v14}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 955
    .line 956
    .line 957
    move-result v0

    .line 958
    invoke-static {v8, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 959
    .line 960
    .line 961
    move-result v2

    .line 962
    invoke-static {v8, v14}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 963
    .line 964
    .line 965
    move-result v8

    .line 966
    int-to-float v11, v11

    .line 967
    int-to-float v5, v5

    .line 968
    int-to-float v13, v13

    .line 969
    int-to-float v0, v0

    .line 970
    int-to-float v2, v2

    .line 971
    int-to-float v8, v8

    .line 972
    move-object/from16 v21, v12

    .line 973
    .line 974
    move-object v12, v15

    .line 975
    move/from16 v16, v13

    .line 976
    .line 977
    move v13, v11

    .line 978
    const/4 v11, 0x4

    .line 979
    move v14, v5

    .line 980
    move-object v5, v15

    .line 981
    move/from16 v15, v16

    .line 982
    .line 983
    move/from16 v16, v0

    .line 984
    .line 985
    move/from16 v17, v2

    .line 986
    .line 987
    move/from16 v18, v8

    .line 988
    .line 989
    invoke-virtual/range {v12 .. v18}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 990
    .line 991
    .line 992
    move-object v2, v5

    .line 993
    goto :goto_e

    .line 994
    :cond_1b
    move-object/from16 v21, v12

    .line 995
    .line 996
    move-object v2, v15

    .line 997
    const/4 v11, 0x4

    .line 998
    invoke-static {v5, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 999
    .line 1000
    .line 1001
    move-result v12

    .line 1002
    const/4 v13, 0x2

    .line 1003
    invoke-static {v5, v13}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 1004
    .line 1005
    .line 1006
    move-result v5

    .line 1007
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 1008
    .line 1009
    .line 1010
    move-result v14

    .line 1011
    invoke-static {v0, v13}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 1012
    .line 1013
    .line 1014
    move-result v0

    .line 1015
    invoke-static {v8, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 1016
    .line 1017
    .line 1018
    move-result v15

    .line 1019
    invoke-static {v8, v13}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 1020
    .line 1021
    .line 1022
    move-result v8

    .line 1023
    int-to-float v13, v12

    .line 1024
    int-to-float v5, v5

    .line 1025
    int-to-float v14, v14

    .line 1026
    int-to-float v0, v0

    .line 1027
    int-to-float v15, v15

    .line 1028
    int-to-float v8, v8

    .line 1029
    move-object v12, v2

    .line 1030
    move/from16 v16, v14

    .line 1031
    .line 1032
    move v14, v5

    .line 1033
    move v5, v15

    .line 1034
    move/from16 v15, v16

    .line 1035
    .line 1036
    move/from16 v16, v0

    .line 1037
    .line 1038
    move/from16 v17, v5

    .line 1039
    .line 1040
    move/from16 v18, v8

    .line 1041
    .line 1042
    invoke-virtual/range {v12 .. v18}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 1043
    .line 1044
    .line 1045
    :goto_e
    move v14, v9

    .line 1046
    goto :goto_10

    .line 1047
    :cond_1c
    :goto_f
    move-object/from16 v21, v12

    .line 1048
    .line 1049
    move-object v2, v15

    .line 1050
    const/4 v11, 0x4

    .line 1051
    :goto_10
    move-object/from16 v12, v21

    .line 1052
    .line 1053
    move-object/from16 v13, v24

    .line 1054
    .line 1055
    :goto_11
    add-int/lit8 v6, v6, 0x1

    .line 1056
    .line 1057
    move-object/from16 v0, p1

    .line 1058
    .line 1059
    move/from16 v1, p3

    .line 1060
    .line 1061
    move-object v15, v2

    .line 1062
    move/from16 v11, v19

    .line 1063
    .line 1064
    move-object/from16 v5, v20

    .line 1065
    .line 1066
    move-object/from16 v9, v22

    .line 1067
    .line 1068
    move-object/from16 v8, v23

    .line 1069
    .line 1070
    move/from16 v2, p5

    .line 1071
    .line 1072
    goto/16 :goto_4

    .line 1073
    .line 1074
    :cond_1d
    move-object/from16 v23, v8

    .line 1075
    .line 1076
    move-object/from16 v22, v9

    .line 1077
    .line 1078
    move-object v2, v15

    .line 1079
    const/4 v1, 0x0

    .line 1080
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    .line 1081
    .line 1082
    .line 1083
    move-result v0

    .line 1084
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1085
    .line 1086
    cmpg-float v0, v0, v3

    .line 1087
    .line 1088
    if-ltz v0, :cond_1f

    .line 1089
    .line 1090
    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    .line 1091
    .line 1092
    .line 1093
    move-result v0

    .line 1094
    cmpg-float v0, v0, v3

    .line 1095
    .line 1096
    if-gez v0, :cond_1e

    .line 1097
    .line 1098
    goto :goto_12

    .line 1099
    :cond_1e
    move-object/from16 v0, v23

    .line 1100
    .line 1101
    goto :goto_13

    .line 1102
    :cond_1f
    :goto_12
    new-instance v0, Landroid/graphics/RectF;

    .line 1103
    .line 1104
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 1105
    .line 1106
    .line 1107
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 1108
    .line 1109
    .line 1110
    move-object/from16 v1, p1

    .line 1111
    .line 1112
    iget v3, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 1113
    .line 1114
    int-to-float v3, v3

    .line 1115
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 1116
    .line 1117
    .line 1118
    move-result v4

    .line 1119
    div-float/2addr v3, v4

    .line 1120
    iget v1, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 1121
    .line 1122
    int-to-float v1, v1

    .line 1123
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 1124
    .line 1125
    .line 1126
    move-result v0

    .line 1127
    div-float/2addr v1, v0

    .line 1128
    move-object/from16 v0, v23

    .line 1129
    .line 1130
    invoke-virtual {v0, v3, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1131
    .line 1132
    .line 1133
    :goto_13
    invoke-virtual {v2, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 1134
    .line 1135
    .line 1136
    move-object/from16 v0, v22

    .line 1137
    .line 1138
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1139
    .line 1140
    .line 1141
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 1142
    .line 1143
    .line 1144
    move-result v1

    .line 1145
    new-array v1, v1, [Landroid/graphics/Path;

    .line 1146
    .line 1147
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1148
    .line 1149
    .line 1150
    move-result-object v0

    .line 1151
    check-cast v0, [Landroid/graphics/Path;

    .line 1152
    .line 1153
    return-object v0
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
.end method

.method public static getGroupFlipHorizontal(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChild(I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 3
    .line 4
    .line 5
    move-result-object p0

    .line 6
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    const/16 v1, -0xff6

    .line 11
    .line 12
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 13
    .line 14
    .line 15
    move-result-object p0

    .line 16
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 17
    .line 18
    if-eqz p0, :cond_0

    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->getFlags()I

    .line 21
    .line 22
    .line 23
    move-result p0

    .line 24
    and-int/lit8 p0, p0, 0x40

    .line 25
    .line 26
    if-eqz p0, :cond_0

    .line 27
    .line 28
    const/4 v0, 0x1

    .line 29
    :cond_0
    return v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getGroupFlipVertical(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChild(I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 3
    .line 4
    .line 5
    move-result-object p0

    .line 6
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    const/16 v1, -0xff6

    .line 11
    .line 12
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 13
    .line 14
    .line 15
    move-result-object p0

    .line 16
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 17
    .line 18
    if-eqz p0, :cond_0

    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->getFlags()I

    .line 21
    .line 22
    .line 23
    move-result p0

    .line 24
    and-int/lit16 p0, p0, 0x80

    .line 25
    .line 26
    if-eqz p0, :cond_0

    .line 27
    .line 28
    const/4 v0, 0x1

    .line 29
    :cond_0
    return v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getGroupRotation(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChild(I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 3
    .line 4
    .line 5
    move-result-object p0

    .line 6
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x4

    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherContainerRecord;S)I

    .line 12
    .line 13
    .line 14
    move-result p0

    .line 15
    shr-int/lit8 p0, p0, 0x10

    .line 16
    .line 17
    rem-int/lit16 p0, p0, 0x168

    .line 18
    .line 19
    return p0

    .line 20
    :cond_0
    return v0
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static getLineColor(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/lang/Object;I)Lcom/intsig/office/java/awt/Color;
    .locals 4

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x1c0

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    const/16 v1, 0x1ff

    .line 18
    .line 19
    invoke-static {p0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    if-nez p0, :cond_0

    .line 27
    .line 28
    const/4 p0, 0x0

    .line 29
    goto :goto_0

    .line 30
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 31
    .line 32
    .line 33
    move-result p0

    .line 34
    :goto_0
    and-int/lit8 p0, p0, 0x8

    .line 35
    .line 36
    const/4 v2, 0x1

    .line 37
    if-eqz p0, :cond_2

    .line 38
    .line 39
    if-eqz v0, :cond_1

    .line 40
    .line 41
    new-instance p0, Lcom/intsig/office/java/awt/Color;

    .line 42
    .line 43
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    invoke-static {p1, v0, p2, v2}, Lcom/intsig/office/fc/ShapeKit;->getColorByIndex(Ljava/lang/Object;IIZ)I

    .line 48
    .line 49
    .line 50
    move-result p1

    .line 51
    invoke-direct {p0, p1}, Lcom/intsig/office/java/awt/Color;-><init>(I)V

    .line 52
    .line 53
    .line 54
    goto :goto_2

    .line 55
    :cond_1
    new-instance p0, Lcom/intsig/office/java/awt/Color;

    .line 56
    .line 57
    invoke-direct {p0, v1, v1, v1}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 58
    .line 59
    .line 60
    goto :goto_2

    .line 61
    :cond_2
    const/4 p0, 0x2

    .line 62
    const/4 v3, 0x0

    .line 63
    if-eq p2, p0, :cond_6

    .line 64
    .line 65
    if-nez v0, :cond_3

    .line 66
    .line 67
    if-eqz p2, :cond_3

    .line 68
    .line 69
    return-object v3

    .line 70
    :cond_3
    if-nez v0, :cond_4

    .line 71
    .line 72
    goto :goto_1

    .line 73
    :cond_4
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 74
    .line 75
    .line 76
    move-result v1

    .line 77
    :goto_1
    const/high16 p0, 0x8000000

    .line 78
    .line 79
    if-lt v1, p0, :cond_5

    .line 80
    .line 81
    rem-int/2addr v1, p0

    .line 82
    invoke-static {p1, v1, p2, v2}, Lcom/intsig/office/fc/ShapeKit;->getColorByIndex(Ljava/lang/Object;IIZ)I

    .line 83
    .line 84
    .line 85
    move-result v1

    .line 86
    :cond_5
    new-instance p0, Lcom/intsig/office/java/awt/Color;

    .line 87
    .line 88
    invoke-direct {p0, v1, v2}, Lcom/intsig/office/java/awt/Color;-><init>(IZ)V

    .line 89
    .line 90
    .line 91
    new-instance p1, Lcom/intsig/office/java/awt/Color;

    .line 92
    .line 93
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getBlue()I

    .line 94
    .line 95
    .line 96
    move-result p2

    .line 97
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getGreen()I

    .line 98
    .line 99
    .line 100
    move-result v0

    .line 101
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getRed()I

    .line 102
    .line 103
    .line 104
    move-result p0

    .line 105
    invoke-direct {p1, p2, v0, p0}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 106
    .line 107
    .line 108
    move-object p0, p1

    .line 109
    goto :goto_2

    .line 110
    :cond_6
    move-object p0, v3

    .line 111
    :goto_2
    return-object p0
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static getLineDashing(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x1ce

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    if-nez p0, :cond_0

    .line 18
    .line 19
    const/4 p0, 0x0

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 22
    .line 23
    .line 24
    move-result p0

    .line 25
    :goto_0
    return p0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getLineWidth(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x1cb

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    if-nez p0, :cond_0

    .line 18
    .line 19
    const/4 p0, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 22
    .line 23
    .line 24
    move-result p0

    .line 25
    div-int/lit16 p0, p0, 0x2535

    .line 26
    .line 27
    :goto_0
    return p0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getMasterShapeID(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    if-eqz p0, :cond_0

    .line 10
    .line 11
    const/16 v0, 0x301

    .line 12
    .line 13
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-eqz p0, :cond_0

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 22
    .line 23
    .line 24
    move-result p0

    .line 25
    return p0

    .line 26
    :cond_0
    const/4 p0, 0x0

    .line 27
    return p0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getPositionRelTo_H(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xede

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherTertiaryOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x390

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 20
    .line 21
    .line 22
    move-result p0

    .line 23
    return p0

    .line 24
    :cond_0
    const/4 p0, 0x2

    .line 25
    return p0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getPositionRelTo_V(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xede

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherTertiaryOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x392

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 20
    .line 21
    .line 22
    move-result p0

    .line 23
    return p0

    .line 24
    :cond_0
    const/4 p0, 0x2

    .line 25
    return p0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getPosition_H(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xede

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherTertiaryOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x38f

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 20
    .line 21
    .line 22
    move-result p0

    .line 23
    return p0

    .line 24
    :cond_0
    const/4 p0, 0x0

    .line 25
    return p0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getPosition_V(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xede

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherTertiaryOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x391

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 20
    .line 21
    .line 22
    move-result p0

    .line 23
    return p0

    .line 24
    :cond_0
    const/4 p0, 0x0

    .line 25
    return p0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getRadialGradientPositionType(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 6

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x18d

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    const/16 v1, 0x18e

    .line 18
    .line 19
    invoke-static {p0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 24
    .line 25
    const/16 v2, 0x18f

    .line 26
    .line 27
    invoke-static {p0, v2}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 32
    .line 33
    const/16 v3, 0x190

    .line 34
    .line 35
    invoke-static {p0, v3}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 36
    .line 37
    .line 38
    move-result-object p0

    .line 39
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 40
    .line 41
    const/high16 v3, 0x10000

    .line 42
    .line 43
    if-eqz v0, :cond_0

    .line 44
    .line 45
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 46
    .line 47
    .line 48
    move-result v4

    .line 49
    if-ne v4, v3, :cond_0

    .line 50
    .line 51
    if-eqz v2, :cond_0

    .line 52
    .line 53
    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 54
    .line 55
    .line 56
    move-result v4

    .line 57
    if-ne v4, v3, :cond_0

    .line 58
    .line 59
    if-eqz v1, :cond_0

    .line 60
    .line 61
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 62
    .line 63
    .line 64
    move-result v4

    .line 65
    if-ne v4, v3, :cond_0

    .line 66
    .line 67
    if-eqz p0, :cond_0

    .line 68
    .line 69
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 70
    .line 71
    .line 72
    move-result v4

    .line 73
    if-ne v4, v3, :cond_0

    .line 74
    .line 75
    const/4 p0, 0x3

    .line 76
    return p0

    .line 77
    :cond_0
    if-eqz v0, :cond_1

    .line 78
    .line 79
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 80
    .line 81
    .line 82
    move-result v4

    .line 83
    const v5, 0x8000

    .line 84
    .line 85
    .line 86
    if-ne v4, v5, :cond_1

    .line 87
    .line 88
    if-eqz v2, :cond_1

    .line 89
    .line 90
    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 91
    .line 92
    .line 93
    move-result v4

    .line 94
    if-ne v4, v5, :cond_1

    .line 95
    .line 96
    if-eqz v1, :cond_1

    .line 97
    .line 98
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 99
    .line 100
    .line 101
    move-result v4

    .line 102
    if-ne v4, v5, :cond_1

    .line 103
    .line 104
    if-eqz p0, :cond_1

    .line 105
    .line 106
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 107
    .line 108
    .line 109
    move-result v4

    .line 110
    if-ne v4, v5, :cond_1

    .line 111
    .line 112
    const/4 p0, 0x4

    .line 113
    return p0

    .line 114
    :cond_1
    if-eqz v0, :cond_2

    .line 115
    .line 116
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 117
    .line 118
    .line 119
    move-result v0

    .line 120
    if-ne v0, v3, :cond_2

    .line 121
    .line 122
    if-eqz v2, :cond_2

    .line 123
    .line 124
    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 125
    .line 126
    .line 127
    move-result v0

    .line 128
    if-ne v0, v3, :cond_2

    .line 129
    .line 130
    const/4 p0, 0x1

    .line 131
    return p0

    .line 132
    :cond_2
    if-eqz v1, :cond_3

    .line 133
    .line 134
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 135
    .line 136
    .line 137
    move-result v0

    .line 138
    if-ne v0, v3, :cond_3

    .line 139
    .line 140
    if-eqz p0, :cond_3

    .line 141
    .line 142
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 143
    .line 144
    .line 145
    move-result p0

    .line 146
    if-ne p0, v3, :cond_3

    .line 147
    .line 148
    const/4 p0, 0x2

    .line 149
    return p0

    .line 150
    :cond_3
    const/4 p0, 0x0

    .line 151
    return p0
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private static getRealSegmentsCount(Lcom/intsig/office/fc/ddf/EscherArrayProperty;Lcom/intsig/office/fc/ddf/EscherArrayProperty;)I
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x0

    .line 10
    const/4 v2, 0x0

    .line 11
    const/4 v3, 0x0

    .line 12
    :goto_0
    if-ge v1, v0, :cond_5

    .line 13
    .line 14
    if-ge v2, p0, :cond_5

    .line 15
    .line 16
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 17
    .line 18
    .line 19
    move-result-object v4

    .line 20
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_MOVETO:[B

    .line 21
    .line 22
    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    .line 23
    .line 24
    .line 25
    move-result v5

    .line 26
    if-eqz v5, :cond_0

    .line 27
    .line 28
    add-int/lit8 v2, v2, 0x1

    .line 29
    .line 30
    goto :goto_3

    .line 31
    :cond_0
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO:[B

    .line 32
    .line 33
    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    .line 34
    .line 35
    .line 36
    move-result v5

    .line 37
    if-nez v5, :cond_3

    .line 38
    .line 39
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO1:[B

    .line 40
    .line 41
    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    .line 42
    .line 43
    .line 44
    move-result v5

    .line 45
    if-nez v5, :cond_3

    .line 46
    .line 47
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO2:[B

    .line 48
    .line 49
    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    .line 50
    .line 51
    .line 52
    move-result v5

    .line 53
    if-nez v5, :cond_3

    .line 54
    .line 55
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE2:[B

    .line 56
    .line 57
    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    .line 58
    .line 59
    .line 60
    move-result v5

    .line 61
    if-eqz v5, :cond_1

    .line 62
    .line 63
    goto :goto_1

    .line 64
    :cond_1
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO:[B

    .line 65
    .line 66
    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    .line 67
    .line 68
    .line 69
    move-result v5

    .line 70
    if-nez v5, :cond_2

    .line 71
    .line 72
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO2:[B

    .line 73
    .line 74
    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    .line 75
    .line 76
    .line 77
    move-result v5

    .line 78
    if-nez v5, :cond_2

    .line 79
    .line 80
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE:[B

    .line 81
    .line 82
    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    .line 83
    .line 84
    .line 85
    move-result v5

    .line 86
    if-nez v5, :cond_2

    .line 87
    .line 88
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE1:[B

    .line 89
    .line 90
    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    .line 91
    .line 92
    .line 93
    move-result v4

    .line 94
    if-eqz v4, :cond_4

    .line 95
    .line 96
    :cond_2
    add-int/lit8 v4, v2, 0x1

    .line 97
    .line 98
    if-gt v4, p0, :cond_4

    .line 99
    .line 100
    goto :goto_2

    .line 101
    :cond_3
    :goto_1
    add-int/lit8 v4, v2, 0x3

    .line 102
    .line 103
    if-gt v4, p0, :cond_4

    .line 104
    .line 105
    :goto_2
    add-int/lit8 v3, v3, 0x1

    .line 106
    .line 107
    move v2, v4

    .line 108
    :cond_4
    :goto_3
    add-int/lit8 v1, v1, 0x1

    .line 109
    .line 110
    goto :goto_0

    .line 111
    :cond_5
    add-int/lit8 v3, v3, 0x1

    .line 112
    .line 113
    return v3
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static getRotation(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherContainerRecord;S)I

    .line 3
    .line 4
    .line 5
    move-result p0

    .line 6
    shr-int/lit8 p0, p0, 0x10

    .line 7
    .line 8
    rem-int/lit16 p0, p0, 0x168

    .line 9
    .line 10
    return p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static getShaderColors(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)[I
    .locals 9

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/ShapeKit;->isShaderPreset(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_4

    .line 6
    .line 7
    const/16 v0, -0xff5

    .line 8
    .line 9
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 10
    .line 11
    .line 12
    move-result-object p0

    .line 13
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 14
    .line 15
    const/16 v0, 0x197

    .line 16
    .line 17
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 22
    .line 23
    const/16 v1, 0x1bf

    .line 24
    .line 25
    invoke-static {p0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 26
    .line 27
    .line 28
    move-result-object p0

    .line 29
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 30
    .line 31
    const/4 v1, 0x0

    .line 32
    if-nez p0, :cond_0

    .line 33
    .line 34
    const/4 p0, 0x0

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 37
    .line 38
    .line 39
    move-result p0

    .line 40
    :goto_0
    if-eqz v0, :cond_4

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    .line 43
    .line 44
    .line 45
    move-result v2

    .line 46
    new-array v3, v2, [I

    .line 47
    .line 48
    const/4 v4, 0x0

    .line 49
    :goto_1
    if-ge v4, v2, :cond_3

    .line 50
    .line 51
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 52
    .line 53
    .line 54
    move-result-object v5

    .line 55
    array-length v6, v5

    .line 56
    const/16 v7, 0x8

    .line 57
    .line 58
    if-ne v6, v7, :cond_2

    .line 59
    .line 60
    and-int/lit8 v6, p0, 0x10

    .line 61
    .line 62
    if-nez v6, :cond_1

    .line 63
    .line 64
    const/4 v5, -0x1

    .line 65
    aput v5, v3, v4

    .line 66
    .line 67
    goto :goto_2

    .line 68
    :cond_1
    aget-byte v6, v5, v1

    .line 69
    .line 70
    const/4 v7, 0x1

    .line 71
    aget-byte v7, v5, v7

    .line 72
    .line 73
    const/4 v8, 0x2

    .line 74
    aget-byte v5, v5, v8

    .line 75
    .line 76
    invoke-static {v6, v7, v5}, Lcom/intsig/office/ss/util/ColorUtil;->rgb(BBB)I

    .line 77
    .line 78
    .line 79
    move-result v5

    .line 80
    aput v5, v3, v4

    .line 81
    .line 82
    :cond_2
    :goto_2
    add-int/lit8 v4, v4, 0x1

    .line 83
    .line 84
    goto :goto_1

    .line 85
    :cond_3
    return-object v3

    .line 86
    :cond_4
    const/4 p0, 0x0

    .line 87
    return-object p0
.end method

.method public static getShaderPositions(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)[F
    .locals 6

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/ShapeKit;->isShaderPreset(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    const/16 v0, -0xff5

    .line 8
    .line 9
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 10
    .line 11
    .line 12
    move-result-object p0

    .line 13
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 14
    .line 15
    const/16 v0, 0x197

    .line 16
    .line 17
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 18
    .line 19
    .line 20
    move-result-object p0

    .line 21
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 22
    .line 23
    if-eqz p0, :cond_2

    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    new-array v1, v0, [F

    .line 30
    .line 31
    const/4 v2, 0x0

    .line 32
    :goto_0
    if-ge v2, v0, :cond_1

    .line 33
    .line 34
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 35
    .line 36
    .line 37
    move-result-object v3

    .line 38
    array-length v4, v3

    .line 39
    const/16 v5, 0x8

    .line 40
    .line 41
    if-ne v4, v5, :cond_0

    .line 42
    .line 43
    const/4 v4, 0x4

    .line 44
    invoke-static {v3, v4}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 45
    .line 46
    .line 47
    move-result v3

    .line 48
    int-to-float v3, v3

    .line 49
    const/high16 v4, 0x47800000    # 65536.0f

    .line 50
    .line 51
    div-float/2addr v3, v4

    .line 52
    aput v3, v1, v2

    .line 53
    .line 54
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_1
    return-object v1

    .line 58
    :cond_2
    const/4 p0, 0x0

    .line 59
    return-object p0
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getShapeId(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xff6

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 8
    .line 9
    if-nez p0, :cond_0

    .line 10
    .line 11
    const/4 p0, 0x0

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->getShapeId()I

    .line 14
    .line 15
    .line 16
    move-result p0

    .line 17
    :goto_0
    return p0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static getShapeName(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Ljava/lang/String;
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x380

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherComplexProperty;

    .line 16
    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherComplexProperty;->getComplexData()[B

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    invoke-static {p0}, Lcom/intsig/office/fc/util/StringUtil;->getFromUnicodeLE([B)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p0

    .line 27
    return-object p0

    .line 28
    :cond_0
    const/4 p0, 0x0

    .line 29
    return-object p0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getShapeType(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xff6

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 8
    .line 9
    if-eqz p0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherRecord;->getOptions()S

    .line 12
    .line 13
    .line 14
    move-result p0

    .line 15
    shr-int/lit8 p0, p0, 0x4

    .line 16
    .line 17
    return p0

    .line 18
    :cond_0
    const/4 p0, -0x1

    .line 19
    return p0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static getStartArrowLength(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    if-eqz p0, :cond_0

    .line 10
    .line 11
    const/16 v0, 0x1d3

    .line 12
    .line 13
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-eqz p0, :cond_0

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 22
    .line 23
    .line 24
    move-result p0

    .line 25
    return p0

    .line 26
    :cond_0
    const/4 p0, 0x1

    .line 27
    return p0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getStartArrowPathAndTail(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;
    .locals 21

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    move-object/from16 v2, p0

    .line 6
    .line 7
    invoke-static {v2, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 12
    .line 13
    new-instance v3, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 14
    .line 15
    const/16 v4, 0x144

    .line 16
    .line 17
    const/4 v5, 0x4

    .line 18
    invoke-direct {v3, v4, v5}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, v3}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 22
    .line 23
    .line 24
    const/16 v3, 0x4145

    .line 25
    .line 26
    invoke-static {v1, v3}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 31
    .line 32
    if-nez v3, :cond_0

    .line 33
    .line 34
    const/16 v3, 0x145

    .line 35
    .line 36
    invoke-static {v1, v3}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 37
    .line 38
    .line 39
    move-result-object v3

    .line 40
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 41
    .line 42
    :cond_0
    const/16 v4, 0x4146

    .line 43
    .line 44
    invoke-static {v1, v4}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 45
    .line 46
    .line 47
    move-result-object v4

    .line 48
    check-cast v4, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 49
    .line 50
    if-nez v4, :cond_1

    .line 51
    .line 52
    const/16 v4, 0x146

    .line 53
    .line 54
    invoke-static {v1, v4}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 55
    .line 56
    .line 57
    move-result-object v4

    .line 58
    check-cast v4, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 59
    .line 60
    :cond_1
    const/4 v6, 0x0

    .line 61
    if-nez v3, :cond_2

    .line 62
    .line 63
    return-object v6

    .line 64
    :cond_2
    if-nez v4, :cond_3

    .line 65
    .line 66
    return-object v6

    .line 67
    :cond_3
    const/16 v7, 0x142

    .line 68
    .line 69
    invoke-static {v1, v7}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 70
    .line 71
    .line 72
    move-result-object v7

    .line 73
    check-cast v7, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 74
    .line 75
    const/16 v8, 0x143

    .line 76
    .line 77
    invoke-static {v1, v8}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 78
    .line 79
    .line 80
    move-result-object v8

    .line 81
    check-cast v8, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 82
    .line 83
    const/4 v9, 0x0

    .line 84
    if-eqz v7, :cond_4

    .line 85
    .line 86
    invoke-virtual {v7}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 87
    .line 88
    .line 89
    move-result v7

    .line 90
    int-to-float v7, v7

    .line 91
    goto :goto_0

    .line 92
    :cond_4
    const/4 v7, 0x0

    .line 93
    :goto_0
    if-eqz v8, :cond_5

    .line 94
    .line 95
    invoke-virtual {v8}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 96
    .line 97
    .line 98
    move-result v8

    .line 99
    int-to-float v8, v8

    .line 100
    goto :goto_1

    .line 101
    :cond_5
    const/4 v8, 0x0

    .line 102
    :goto_1
    new-instance v10, Landroid/graphics/Matrix;

    .line 103
    .line 104
    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    .line 105
    .line 106
    .line 107
    cmpl-float v11, v7, v9

    .line 108
    .line 109
    if-lez v11, :cond_6

    .line 110
    .line 111
    cmpl-float v9, v8, v9

    .line 112
    .line 113
    if-lez v9, :cond_6

    .line 114
    .line 115
    iget v9, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 116
    .line 117
    int-to-float v9, v9

    .line 118
    div-float/2addr v9, v7

    .line 119
    iget v11, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 120
    .line 121
    int-to-float v11, v11

    .line 122
    div-float/2addr v11, v8

    .line 123
    invoke-virtual {v10, v9, v11}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 124
    .line 125
    .line 126
    :cond_6
    const/16 v8, 0x1d0

    .line 127
    .line 128
    invoke-static {v1, v8}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 129
    .line 130
    .line 131
    move-result-object v1

    .line 132
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 133
    .line 134
    if-eqz v1, :cond_d

    .line 135
    .line 136
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 137
    .line 138
    .line 139
    move-result v8

    .line 140
    if-lez v8, :cond_d

    .line 141
    .line 142
    new-instance v8, Lcom/intsig/office/common/shape/Arrow;

    .line 143
    .line 144
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 145
    .line 146
    .line 147
    move-result v1

    .line 148
    int-to-byte v1, v1

    .line 149
    invoke-static/range {p0 .. p0}, Lcom/intsig/office/fc/ShapeKit;->getStartArrowWidth(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 150
    .line 151
    .line 152
    move-result v9

    .line 153
    invoke-static/range {p0 .. p0}, Lcom/intsig/office/fc/ShapeKit;->getStartArrowLength(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 154
    .line 155
    .line 156
    move-result v11

    .line 157
    invoke-direct {v8, v1, v9, v11}, Lcom/intsig/office/common/shape/Arrow;-><init>(BII)V

    .line 158
    .line 159
    .line 160
    invoke-static/range {p0 .. p0}, Lcom/intsig/office/fc/ShapeKit;->getLineWidth(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 161
    .line 162
    .line 163
    move-result v1

    .line 164
    int-to-float v1, v1

    .line 165
    const v2, 0x3faaaaab

    .line 166
    .line 167
    .line 168
    mul-float v1, v1, v2

    .line 169
    .line 170
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 171
    .line 172
    .line 173
    move-result v1

    .line 174
    invoke-virtual {v3}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    .line 175
    .line 176
    .line 177
    move-result v2

    .line 178
    invoke-virtual {v4}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    .line 179
    .line 180
    .line 181
    const/4 v9, 0x0

    .line 182
    invoke-virtual {v3, v9}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 183
    .line 184
    .line 185
    move-result-object v11

    .line 186
    array-length v12, v11

    .line 187
    const/16 v13, 0x8

    .line 188
    .line 189
    const/4 v14, 0x2

    .line 190
    if-ne v12, v13, :cond_7

    .line 191
    .line 192
    invoke-static {v11, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 193
    .line 194
    .line 195
    move-result v12

    .line 196
    int-to-float v12, v12

    .line 197
    invoke-static {v11, v5}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 198
    .line 199
    .line 200
    move-result v11

    .line 201
    goto :goto_2

    .line 202
    :cond_7
    invoke-static {v11, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 203
    .line 204
    .line 205
    move-result v12

    .line 206
    int-to-float v12, v12

    .line 207
    invoke-static {v11, v14}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 208
    .line 209
    .line 210
    move-result v11

    .line 211
    :goto_2
    int-to-float v11, v11

    .line 212
    move/from16 v18, v11

    .line 213
    .line 214
    move/from16 v17, v12

    .line 215
    .line 216
    const/4 v11, 0x1

    .line 217
    invoke-virtual {v4, v11}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 218
    .line 219
    .line 220
    move-result-object v4

    .line 221
    sget-object v12, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO:[B

    .line 222
    .line 223
    invoke-static {v4, v12}, Ljava/util/Arrays;->equals([B[B)Z

    .line 224
    .line 225
    .line 226
    move-result v12

    .line 227
    if-nez v12, :cond_b

    .line 228
    .line 229
    sget-object v12, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO1:[B

    .line 230
    .line 231
    invoke-static {v4, v12}, Ljava/util/Arrays;->equals([B[B)Z

    .line 232
    .line 233
    .line 234
    move-result v12

    .line 235
    if-nez v12, :cond_b

    .line 236
    .line 237
    sget-object v12, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO2:[B

    .line 238
    .line 239
    invoke-static {v4, v12}, Ljava/util/Arrays;->equals([B[B)Z

    .line 240
    .line 241
    .line 242
    move-result v12

    .line 243
    if-nez v12, :cond_b

    .line 244
    .line 245
    sget-object v12, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE2:[B

    .line 246
    .line 247
    invoke-static {v4, v12}, Ljava/util/Arrays;->equals([B[B)Z

    .line 248
    .line 249
    .line 250
    move-result v12

    .line 251
    if-eqz v12, :cond_8

    .line 252
    .line 253
    goto :goto_4

    .line 254
    :cond_8
    sget-object v12, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO:[B

    .line 255
    .line 256
    invoke-static {v4, v12}, Ljava/util/Arrays;->equals([B[B)Z

    .line 257
    .line 258
    .line 259
    move-result v12

    .line 260
    if-nez v12, :cond_9

    .line 261
    .line 262
    sget-object v12, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO2:[B

    .line 263
    .line 264
    invoke-static {v4, v12}, Ljava/util/Arrays;->equals([B[B)Z

    .line 265
    .line 266
    .line 267
    move-result v12

    .line 268
    if-nez v12, :cond_9

    .line 269
    .line 270
    sget-object v12, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE:[B

    .line 271
    .line 272
    invoke-static {v4, v12}, Ljava/util/Arrays;->equals([B[B)Z

    .line 273
    .line 274
    .line 275
    move-result v12

    .line 276
    if-nez v12, :cond_9

    .line 277
    .line 278
    sget-object v12, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE1:[B

    .line 279
    .line 280
    invoke-static {v4, v12}, Ljava/util/Arrays;->equals([B[B)Z

    .line 281
    .line 282
    .line 283
    move-result v4

    .line 284
    if-eqz v4, :cond_d

    .line 285
    .line 286
    :cond_9
    if-gt v14, v2, :cond_d

    .line 287
    .line 288
    invoke-virtual {v3, v11}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 289
    .line 290
    .line 291
    move-result-object v2

    .line 292
    array-length v3, v2

    .line 293
    if-ne v3, v13, :cond_a

    .line 294
    .line 295
    invoke-static {v2, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 296
    .line 297
    .line 298
    move-result v3

    .line 299
    int-to-float v3, v3

    .line 300
    invoke-static {v2, v5}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 301
    .line 302
    .line 303
    move-result v2

    .line 304
    goto :goto_3

    .line 305
    :cond_a
    invoke-static {v2, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 306
    .line 307
    .line 308
    move-result v3

    .line 309
    int-to-float v3, v3

    .line 310
    invoke-static {v2, v14}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 311
    .line 312
    .line 313
    move-result v2

    .line 314
    :goto_3
    int-to-float v2, v2

    .line 315
    move v12, v2

    .line 316
    move v11, v3

    .line 317
    int-to-float v1, v1

    .line 318
    mul-float v1, v1, v7

    .line 319
    .line 320
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 321
    .line 322
    int-to-float v0, v0

    .line 323
    div-float/2addr v1, v0

    .line 324
    float-to-int v0, v1

    .line 325
    move/from16 v13, v17

    .line 326
    .line 327
    move/from16 v14, v18

    .line 328
    .line 329
    move-object v15, v8

    .line 330
    move/from16 v16, v0

    .line 331
    .line 332
    invoke-static/range {v11 .. v16}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 333
    .line 334
    .line 335
    move-result-object v6

    .line 336
    goto/16 :goto_6

    .line 337
    .line 338
    :cond_b
    :goto_4
    if-gt v5, v2, :cond_d

    .line 339
    .line 340
    invoke-virtual {v3, v11}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 341
    .line 342
    .line 343
    move-result-object v2

    .line 344
    invoke-virtual {v3, v14}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 345
    .line 346
    .line 347
    move-result-object v4

    .line 348
    const/4 v6, 0x3

    .line 349
    invoke-virtual {v3, v6}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 350
    .line 351
    .line 352
    move-result-object v3

    .line 353
    array-length v6, v2

    .line 354
    if-ne v6, v13, :cond_c

    .line 355
    .line 356
    array-length v6, v4

    .line 357
    if-ne v6, v13, :cond_c

    .line 358
    .line 359
    array-length v6, v3

    .line 360
    if-ne v6, v13, :cond_c

    .line 361
    .line 362
    invoke-static {v2, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 363
    .line 364
    .line 365
    move-result v6

    .line 366
    int-to-float v6, v6

    .line 367
    invoke-static {v2, v5}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 368
    .line 369
    .line 370
    move-result v2

    .line 371
    int-to-float v2, v2

    .line 372
    invoke-static {v4, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 373
    .line 374
    .line 375
    move-result v11

    .line 376
    int-to-float v11, v11

    .line 377
    invoke-static {v4, v5}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 378
    .line 379
    .line 380
    move-result v4

    .line 381
    int-to-float v4, v4

    .line 382
    invoke-static {v3, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 383
    .line 384
    .line 385
    move-result v9

    .line 386
    int-to-float v9, v9

    .line 387
    invoke-static {v3, v5}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 388
    .line 389
    .line 390
    move-result v3

    .line 391
    int-to-float v3, v3

    .line 392
    move/from16 v16, v2

    .line 393
    .line 394
    move v12, v3

    .line 395
    move v14, v4

    .line 396
    move v15, v6

    .line 397
    move v13, v11

    .line 398
    goto :goto_5

    .line 399
    :cond_c
    invoke-static {v2, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 400
    .line 401
    .line 402
    move-result v5

    .line 403
    int-to-float v5, v5

    .line 404
    invoke-static {v2, v14}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 405
    .line 406
    .line 407
    move-result v2

    .line 408
    int-to-float v2, v2

    .line 409
    invoke-static {v4, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 410
    .line 411
    .line 412
    move-result v6

    .line 413
    int-to-float v6, v6

    .line 414
    invoke-static {v4, v14}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 415
    .line 416
    .line 417
    move-result v4

    .line 418
    int-to-float v4, v4

    .line 419
    invoke-static {v3, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 420
    .line 421
    .line 422
    move-result v9

    .line 423
    int-to-float v9, v9

    .line 424
    invoke-static {v3, v14}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 425
    .line 426
    .line 427
    move-result v3

    .line 428
    int-to-float v3, v3

    .line 429
    move/from16 v16, v2

    .line 430
    .line 431
    move v12, v3

    .line 432
    move v14, v4

    .line 433
    move v15, v5

    .line 434
    move v13, v6

    .line 435
    :goto_5
    move v11, v9

    .line 436
    int-to-float v1, v1

    .line 437
    mul-float v1, v1, v7

    .line 438
    .line 439
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 440
    .line 441
    int-to-float v0, v0

    .line 442
    div-float/2addr v1, v0

    .line 443
    float-to-int v0, v1

    .line 444
    move-object/from16 v19, v8

    .line 445
    .line 446
    move/from16 v20, v0

    .line 447
    .line 448
    invoke-static/range {v11 .. v20}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getCubicBezArrowPath(FFFFFFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 449
    .line 450
    .line 451
    move-result-object v6

    .line 452
    :cond_d
    :goto_6
    if-eqz v6, :cond_e

    .line 453
    .line 454
    invoke-virtual {v6}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 455
    .line 456
    .line 457
    move-result-object v0

    .line 458
    if-eqz v0, :cond_e

    .line 459
    .line 460
    invoke-virtual {v6}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 461
    .line 462
    .line 463
    move-result-object v0

    .line 464
    invoke-virtual {v0, v10}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 465
    .line 466
    .line 467
    :cond_e
    return-object v6
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method public static getStartArrowType(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    if-eqz p0, :cond_0

    .line 10
    .line 11
    const/16 v0, 0x1d0

    .line 12
    .line 13
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-eqz p0, :cond_0

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 22
    .line 23
    .line 24
    move-result p0

    .line 25
    return p0

    .line 26
    :cond_0
    const/4 p0, 0x0

    .line 27
    return p0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getStartArrowWidth(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    if-eqz p0, :cond_0

    .line 10
    .line 11
    const/16 v0, 0x1d2

    .line 12
    .line 13
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-eqz p0, :cond_0

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 22
    .line 23
    .line 24
    move-result p0

    .line 25
    return p0

    .line 26
    :cond_0
    const/4 p0, 0x1

    .line 27
    return p0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getTextboxMarginBottom(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x84

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 20
    .line 21
    .line 22
    move-result p0

    .line 23
    int-to-float p0, p0

    .line 24
    const v0, 0x4614d400    # 9525.0f

    .line 25
    .line 26
    .line 27
    div-float/2addr p0, v0

    .line 28
    return p0

    .line 29
    :cond_0
    const p0, 0x4099999a    # 4.8f

    .line 30
    .line 31
    .line 32
    return p0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getTextboxMarginLeft(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x81

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 20
    .line 21
    .line 22
    move-result p0

    .line 23
    int-to-float p0, p0

    .line 24
    const v0, 0x4614d400    # 9525.0f

    .line 25
    .line 26
    .line 27
    div-float/2addr p0, v0

    .line 28
    return p0

    .line 29
    :cond_0
    const p0, 0x4119999a    # 9.6f

    .line 30
    .line 31
    .line 32
    return p0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getTextboxMarginRight(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x83

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 20
    .line 21
    .line 22
    move-result p0

    .line 23
    int-to-float p0, p0

    .line 24
    const v0, 0x4614d400    # 9525.0f

    .line 25
    .line 26
    .line 27
    div-float/2addr p0, v0

    .line 28
    return p0

    .line 29
    :cond_0
    const p0, 0x4119999a    # 9.6f

    .line 30
    .line 31
    .line 32
    return p0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getTextboxMarginTop(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x82

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 20
    .line 21
    .line 22
    move-result p0

    .line 23
    int-to-float p0, p0

    .line 24
    const v0, 0x4614d400    # 9525.0f

    .line 25
    .line 26
    .line 27
    div-float/2addr p0, v0

    .line 28
    return p0

    .line 29
    :cond_0
    const p0, 0x4099999a    # 4.8f

    .line 30
    .line 31
    .line 32
    return p0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getUnicodeGeoText(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Ljava/lang/String;
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0xc0

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherComplexProperty;

    .line 16
    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherComplexProperty;->getComplexData()[B

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    invoke-static {p0}, Lcom/intsig/office/fc/util/StringUtil;->getFromUnicodeLE([B)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p0

    .line 27
    return-object p0

    .line 28
    :cond_0
    const/4 p0, 0x0

    .line 29
    return-object p0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static hasBackgroundFill(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x1bf

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    if-nez p0, :cond_0

    .line 19
    .line 20
    const/4 p0, 0x0

    .line 21
    goto :goto_0

    .line 22
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 23
    .line 24
    .line 25
    move-result p0

    .line 26
    :goto_0
    and-int/lit8 p0, p0, 0x10

    .line 27
    .line 28
    if-eqz p0, :cond_1

    .line 29
    .line 30
    const/4 v0, 0x1

    .line 31
    :cond_1
    return v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static hasLine(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x1ff

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    const/4 v0, 0x1

    .line 18
    if-eqz p0, :cond_1

    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 21
    .line 22
    .line 23
    move-result p0

    .line 24
    and-int/lit16 p0, p0, 0xff

    .line 25
    .line 26
    if-eqz p0, :cond_0

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    const/4 v0, 0x0

    .line 30
    :cond_1
    :goto_0
    return v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static isHidden(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z
    .locals 2

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    if-eqz p0, :cond_0

    .line 11
    .line 12
    const/16 v1, 0x3bf

    .line 13
    .line 14
    invoke-static {p0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 15
    .line 16
    .line 17
    move-result-object p0

    .line 18
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 19
    .line 20
    if-eqz p0, :cond_0

    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 23
    .line 24
    .line 25
    move-result p0

    .line 26
    const v1, 0x20002

    .line 27
    .line 28
    .line 29
    if-ne p0, v1, :cond_0

    .line 30
    .line 31
    const/4 v0, 0x1

    .line 32
    :cond_0
    return v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static isShaderPreset(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z
    .locals 1

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x196

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 20
    .line 21
    .line 22
    move-result p0

    .line 23
    if-lez p0, :cond_0

    .line 24
    .line 25
    const/4 p0, 0x1

    .line 26
    return p0

    .line 27
    :cond_0
    const/4 p0, 0x0

    .line 28
    return p0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static isTextboxWrapLine(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z
    .locals 2

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    const/16 v0, 0x85

    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 16
    .line 17
    const/4 v0, 0x1

    .line 18
    if-eqz p0, :cond_1

    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 21
    .line 22
    .line 23
    move-result p0

    .line 24
    const/4 v1, 0x2

    .line 25
    if-eq p0, v1, :cond_0

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const/4 v0, 0x0

    .line 29
    :cond_1
    :goto_0
    return v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

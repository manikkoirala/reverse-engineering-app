.class public Lcom/intsig/office/fc/hslf/record/InteractiveInfo;
.super Lcom/intsig/office/fc/hslf/record/RecordContainer;
.source "InteractiveInfo.java"


# static fields
.field private static _type:J = 0xff2L


# instance fields
.field private _header:[B

.field private infoAtom:Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 5

    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [B

    .line 7
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;->_header:[B

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/intsig/office/fc/hslf/record/Record;

    .line 8
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    const/16 v1, 0xf

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 9
    sget-wide v3, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;->_type:J

    long-to-int v1, v3

    int-to-short v1, v1

    const/4 v3, 0x2

    invoke-static {v0, v3, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 10
    new-instance v0, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;

    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;->infoAtom:Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;

    .line 11
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    aput-object v0, v1, v2

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;-><init>()V

    const/16 v0, 0x8

    new-array v1, v0, [B

    .line 2
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;->_header:[B

    const/4 v2, 0x0

    .line 3
    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, v0

    sub-int/2addr p3, v0

    .line 4
    invoke-static {p1, p2, p3}, Lcom/intsig/office/fc/hslf/record/Record;->findChildRecords([BII)[Lcom/intsig/office/fc/hslf/record/Record;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 5
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;->findInterestingChildren()V

    return-void
.end method

.method private findInterestingChildren()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    aget-object v0, v0, v1

    .line 5
    .line 6
    instance-of v2, v0, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;

    .line 7
    .line 8
    if-eqz v2, :cond_0

    .line 9
    .line 10
    check-cast v0, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;->infoAtom:Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 16
    .line 17
    new-instance v2, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    const-string v3, "First child record wasn\'t a InteractiveInfoAtom, was of type "

    .line 23
    .line 24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 28
    .line 29
    aget-object v1, v3, v1

    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    .line 32
    .line 33
    .line 34
    move-result-wide v3

    .line 35
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    throw v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;->_header:[B

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;->infoAtom:Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->dispose()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;->infoAtom:Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;

    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getInteractiveInfoAtom()Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;->infoAtom:Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

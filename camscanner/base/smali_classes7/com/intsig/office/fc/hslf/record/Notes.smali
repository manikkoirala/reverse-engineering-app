.class public final Lcom/intsig/office/fc/hslf/record/Notes;
.super Lcom/intsig/office/fc/hslf/record/SheetContainer;
.source "Notes.java"


# static fields
.field private static _type:J = 0x3f0L


# instance fields
.field private _colorScheme:Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

.field private _header:[B

.field private notesAtom:Lcom/intsig/office/fc/hslf/record/NotesAtom;

.field private ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected constructor <init>([BII)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/SheetContainer;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x8

    .line 5
    .line 6
    new-array v1, v0, [B

    .line 7
    .line 8
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/Notes;->_header:[B

    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 12
    .line 13
    .line 14
    add-int/2addr p2, v0

    .line 15
    sub-int/2addr p3, v0

    .line 16
    invoke-static {p1, p2, p3}, Lcom/intsig/office/fc/hslf/record/Record;->findChildRecords([BII)[Lcom/intsig/office/fc/hslf/record/Record;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 21
    .line 22
    :goto_0
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 23
    .line 24
    array-length p2, p1

    .line 25
    if-ge v2, p2, :cond_3

    .line 26
    .line 27
    aget-object p1, p1, v2

    .line 28
    .line 29
    instance-of p2, p1, Lcom/intsig/office/fc/hslf/record/NotesAtom;

    .line 30
    .line 31
    if-eqz p2, :cond_0

    .line 32
    .line 33
    move-object p2, p1

    .line 34
    check-cast p2, Lcom/intsig/office/fc/hslf/record/NotesAtom;

    .line 35
    .line 36
    iput-object p2, p0, Lcom/intsig/office/fc/hslf/record/Notes;->notesAtom:Lcom/intsig/office/fc/hslf/record/NotesAtom;

    .line 37
    .line 38
    :cond_0
    instance-of p2, p1, Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 39
    .line 40
    if-eqz p2, :cond_1

    .line 41
    .line 42
    move-object p2, p1

    .line 43
    check-cast p2, Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 44
    .line 45
    iput-object p2, p0, Lcom/intsig/office/fc/hslf/record/Notes;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 46
    .line 47
    :cond_1
    iget-object p2, p0, Lcom/intsig/office/fc/hslf/record/Notes;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 48
    .line 49
    if-eqz p2, :cond_2

    .line 50
    .line 51
    instance-of p2, p1, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 52
    .line 53
    if-eqz p2, :cond_2

    .line 54
    .line 55
    check-cast p1, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 56
    .line 57
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/Notes;->_colorScheme:Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 58
    .line 59
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_3
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->dispose()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Notes;->notesAtom:Lcom/intsig/office/fc/hslf/record/NotesAtom;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/NotesAtom;->dispose()V

    .line 10
    .line 11
    .line 12
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/Notes;->notesAtom:Lcom/intsig/office/fc/hslf/record/NotesAtom;

    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Notes;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 15
    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->dispose()V

    .line 19
    .line 20
    .line 21
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/Notes;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 22
    .line 23
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Notes;->_colorScheme:Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 24
    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;->dispose()V

    .line 28
    .line 29
    .line 30
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/Notes;->_colorScheme:Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 31
    .line 32
    :cond_2
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getColorScheme()Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Notes;->_colorScheme:Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNotesAtom()Lcom/intsig/office/fc/hslf/record/NotesAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Notes;->notesAtom:Lcom/intsig/office/fc/hslf/record/NotesAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPPDrawing()Lcom/intsig/office/fc/hslf/record/PPDrawing;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Notes;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/office/fc/hslf/record/Notes;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

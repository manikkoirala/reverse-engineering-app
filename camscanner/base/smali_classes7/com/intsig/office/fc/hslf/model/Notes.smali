.class public final Lcom/intsig/office/fc/hslf/model/Notes;
.super Lcom/intsig/office/fc/hslf/model/Sheet;
.source "Notes.java"


# instance fields
.field private _runs:[Lcom/intsig/office/fc/hslf/model/TextRun;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hslf/record/Notes;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/record/Notes;->getNotesAtom()Lcom/intsig/office/fc/hslf/record/NotesAtom;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/NotesAtom;->getSlideID()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hslf/model/Sheet;-><init>(Lcom/intsig/office/fc/hslf/record/SheetContainer;I)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getPPDrawing()Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-static {p1}, Lcom/intsig/office/fc/hslf/model/Sheet;->findTextRuns(Lcom/intsig/office/fc/hslf/record/PPDrawing;)[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Notes;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 21
    .line 22
    const/4 p1, 0x0

    .line 23
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Notes;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 24
    .line 25
    array-length v1, v0

    .line 26
    if-ge p1, v1, :cond_0

    .line 27
    .line 28
    aget-object v0, v0, p1

    .line 29
    .line 30
    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/hslf/model/TextRun;->setSheet(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    .line 31
    .line 32
    .line 33
    add-int/lit8 p1, p1, 0x1

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public dispose()V
    .locals 4

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->dispose()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Notes;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 5
    .line 6
    if-eqz v0, :cond_1

    .line 7
    .line 8
    array-length v1, v0

    .line 9
    const/4 v2, 0x0

    .line 10
    :goto_0
    if-ge v2, v1, :cond_0

    .line 11
    .line 12
    aget-object v3, v0, v2

    .line 13
    .line 14
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/TextRun;->dispose()V

    .line 15
    .line 16
    .line 17
    add-int/lit8 v2, v2, 0x1

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v0, 0x0

    .line 21
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Notes;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 22
    .line 23
    :cond_1
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getMasterSheet()Lcom/intsig/office/fc/hslf/model/MasterSheet;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTextRuns()[Lcom/intsig/office/fc/hslf/model/TextRun;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Notes;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

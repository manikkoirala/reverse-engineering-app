.class Lcom/intsig/office/fc/hslf/model/AutoShapes$16;
.super Ljava/lang/Object;
.source "AutoShapes.java"

# interfaces
.implements Lcom/intsig/office/fc/hslf/model/ShapeOutline;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hslf/model/AutoShapes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public getOutline(Lcom/intsig/office/fc/hslf/model/Shape;)Lcom/intsig/office/java/awt/Shape;
    .locals 5

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/16 v1, 0x147

    .line 6
    .line 7
    const/16 v2, 0x1518

    .line 8
    .line 9
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherContainerRecord;SI)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    const/16 v1, 0x148

    .line 18
    .line 19
    invoke-static {p1, v1, v2}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherContainerRecord;SI)I

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    new-instance v1, Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 24
    .line 25
    invoke-direct {v1}, Lcom/intsig/office/java/awt/geom/GeneralPath;-><init>()V

    .line 26
    .line 27
    .line 28
    int-to-float v0, v0

    .line 29
    const/4 v2, 0x0

    .line 30
    invoke-virtual {v1, v0, v2}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->moveTo(FF)V

    .line 31
    .line 32
    .line 33
    int-to-float v3, p1

    .line 34
    invoke-virtual {v1, v0, v3}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->lineTo(FF)V

    .line 35
    .line 36
    .line 37
    const v4, 0x46a8c000    # 21600.0f

    .line 38
    .line 39
    .line 40
    invoke-virtual {v1, v4, v3}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->lineTo(FF)V

    .line 41
    .line 42
    .line 43
    rsub-int p1, p1, 0x5460

    .line 44
    .line 45
    int-to-float p1, p1

    .line 46
    invoke-virtual {v1, v4, p1}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->lineTo(FF)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v1, v0, p1}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->lineTo(FF)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v1, v0, v4}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->lineTo(FF)V

    .line 53
    .line 54
    .line 55
    const p1, 0x4628c000    # 10800.0f

    .line 56
    .line 57
    .line 58
    invoke-virtual {v1, v2, p1}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->lineTo(FF)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/Path2D;->closePath()V

    .line 62
    .line 63
    .line 64
    return-object v1
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

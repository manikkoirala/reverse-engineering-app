.class public final Lcom/intsig/office/fc/hslf/HSLFSlideShow;
.super Ljava/lang/Object;
.source "HSLFSlideShow.java"


# static fields
.field protected static final CHECKSUM_SIZE:I = 0x10


# instance fields
.field private _docProp:Lcom/intsig/office/fc/fs/filesystem/Property;

.field private _docstream:[B

.field private _objects:[Lcom/intsig/office/fc/hslf/usermodel/ObjectData;

.field private _pictures:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hslf/usermodel/PictureData;",
            ">;"
        }
    .end annotation
.end field

.field private _records:[Lcom/intsig/office/fc/hslf/record/Record;

.field private cfbFS:Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

.field private control:Lcom/intsig/office/system/IControl;

.field private currentUser:Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;


# direct methods
.method public constructor <init>(Lcom/intsig/office/system/IControl;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->control:Lcom/intsig/office/system/IControl;

    .line 5
    .line 6
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getActivity()Landroid/app/Activity;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    invoke-static {p1, p3}, Lcom/intsig/office/ss/util/StreamUtils;->getInputStream(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/InputStream;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    new-instance p2, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

    .line 15
    .line 16
    invoke-direct {p2, p1}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;-><init>(Ljava/io/InputStream;)V

    .line 17
    .line 18
    .line 19
    iput-object p2, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->cfbFS:Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->readCurrentUserStream()V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->readPowerPointStream()V

    .line 25
    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->cfbFS:Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

    .line 28
    .line 29
    const-string p2, "EncryptedSummary"

    .line 30
    .line 31
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->getPropertyRawData(Ljava/lang/String;)[B

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    if-eqz p1, :cond_0

    .line 36
    .line 37
    const/4 p1, 0x1

    .line 38
    goto :goto_0

    .line 39
    :cond_0
    const/4 p1, 0x0

    .line 40
    :goto_0
    if-nez p1, :cond_1

    .line 41
    .line 42
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->buildRecords()V

    .line 43
    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->readOtherStreams()V

    .line 46
    .line 47
    .line 48
    return-void

    .line 49
    :cond_1
    new-instance p1, Lcom/intsig/office/fc/hslf/exceptions/EncryptedPowerPointFileException;

    .line 50
    .line 51
    const-string p2, "Cannot process encrypted office files!"

    .line 52
    .line 53
    invoke-direct {p1, p2}, Lcom/intsig/office/fc/hslf/exceptions/EncryptedPowerPointFileException;-><init>(Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    throw p1
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private buildRecords()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->currentUser:Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->getCurrentEditOffset()J

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    long-to-int v1, v0

    .line 8
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->read(I)[Lcom/intsig/office/fc/hslf/record/Record;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_records:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private read(I)[Lcom/intsig/office/fc/hslf/record/Record;
    .locals 7

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Ljava/util/HashMap;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 9
    .line 10
    .line 11
    :goto_0
    const/4 v2, 0x0

    .line 12
    if-eqz p1, :cond_1

    .line 13
    .line 14
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_docProp:Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 15
    .line 16
    invoke-virtual {v3, p1}, Lcom/intsig/office/fc/fs/filesystem/Property;->getRecordData(I)[B

    .line 17
    .line 18
    .line 19
    move-result-object v3

    .line 20
    invoke-static {v3, v2, p1}, Lcom/intsig/office/fc/hslf/record/Record;->buildRecordAtOffset([BII)Lcom/intsig/office/fc/hslf/record/Record;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    check-cast v3, Lcom/intsig/office/fc/hslf/record/UserEditAtom;

    .line 25
    .line 26
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/record/UserEditAtom;->getPersistPointersOffset()I

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    iget-object v4, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_docProp:Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 38
    .line 39
    invoke-virtual {v4, p1}, Lcom/intsig/office/fc/fs/filesystem/Property;->getRecordData(I)[B

    .line 40
    .line 41
    .line 42
    move-result-object v4

    .line 43
    invoke-static {v4, v2, p1}, Lcom/intsig/office/fc/hslf/record/Record;->buildRecordAtOffset([BII)Lcom/intsig/office/fc/hslf/record/Record;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    check-cast v2, Lcom/intsig/office/fc/hslf/record/PersistPtrHolder;

    .line 48
    .line 49
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    .line 55
    .line 56
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/record/PersistPtrHolder;->getSlideLocationsLookup()Ljava/util/Hashtable;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    invoke-virtual {p1}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 69
    .line 70
    .line 71
    move-result v4

    .line 72
    if-eqz v4, :cond_0

    .line 73
    .line 74
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 75
    .line 76
    .line 77
    move-result-object v4

    .line 78
    check-cast v4, Ljava/lang/Integer;

    .line 79
    .line 80
    invoke-virtual {p1, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    .line 82
    .line 83
    move-result-object v5

    .line 84
    check-cast v5, Ljava/lang/Integer;

    .line 85
    .line 86
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    .line 88
    .line 89
    invoke-virtual {v1, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    .line 91
    .line 92
    goto :goto_1

    .line 93
    :cond_0
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/record/UserEditAtom;->getLastUserEditAtomOffset()I

    .line 94
    .line 95
    .line 96
    move-result p1

    .line 97
    goto :goto_0

    .line 98
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 99
    .line 100
    .line 101
    move-result p1

    .line 102
    new-array p1, p1, [Ljava/lang/Integer;

    .line 103
    .line 104
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    check-cast p1, [Ljava/lang/Integer;

    .line 109
    .line 110
    invoke-static {p1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 111
    .line 112
    .line 113
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 114
    .line 115
    .line 116
    move-result v0

    .line 117
    new-array v0, v0, [Lcom/intsig/office/fc/hslf/record/Record;

    .line 118
    .line 119
    const/4 v3, 0x0

    .line 120
    :goto_2
    array-length v4, p1

    .line 121
    if-ge v3, v4, :cond_3

    .line 122
    .line 123
    aget-object v4, p1, v3

    .line 124
    .line 125
    iget-object v5, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_docProp:Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 126
    .line 127
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    .line 128
    .line 129
    .line 130
    move-result v6

    .line 131
    invoke-virtual {v5, v6}, Lcom/intsig/office/fc/fs/filesystem/Property;->getRecordData(I)[B

    .line 132
    .line 133
    .line 134
    move-result-object v5

    .line 135
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    .line 136
    .line 137
    .line 138
    move-result v6

    .line 139
    invoke-static {v5, v2, v6}, Lcom/intsig/office/fc/hslf/record/Record;->buildRecordAtOffset([BII)Lcom/intsig/office/fc/hslf/record/Record;

    .line 140
    .line 141
    .line 142
    move-result-object v5

    .line 143
    aput-object v5, v0, v3

    .line 144
    .line 145
    instance-of v6, v5, Lcom/intsig/office/fc/hslf/record/PersistRecord;

    .line 146
    .line 147
    if-eqz v6, :cond_2

    .line 148
    .line 149
    check-cast v5, Lcom/intsig/office/fc/hslf/record/PersistRecord;

    .line 150
    .line 151
    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    .line 153
    .line 154
    move-result-object v4

    .line 155
    check-cast v4, Ljava/lang/Integer;

    .line 156
    .line 157
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    .line 158
    .line 159
    .line 160
    move-result v4

    .line 161
    invoke-interface {v5, v4}, Lcom/intsig/office/fc/hslf/record/PersistRecord;->setPersistId(I)V

    .line 162
    .line 163
    .line 164
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 165
    .line 166
    goto :goto_2

    .line 167
    :cond_3
    return-object v0
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private readCurrentUserStream()V
    .locals 2

    .line 1
    :try_start_0
    new-instance v0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->cfbFS:Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;-><init>(Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;)V

    .line 6
    .line 7
    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->currentUser:Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :catch_0
    new-instance v0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;

    .line 12
    .line 13
    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->currentUser:Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;

    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private readOtherStreams()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private readPictures()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->cfbFS:Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

    .line 7
    .line 8
    const-string v1, "Pictures"

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->getProperty(Ljava/lang/String;)Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-nez v0, :cond_1

    .line 15
    .line 16
    return-void

    .line 17
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    .line 18
    .line 19
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 20
    .line 21
    .line 22
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/fc/fs/filesystem/Property;->getPropertyRawDataSize()J

    .line 25
    .line 26
    .line 27
    move-result-wide v1

    .line 28
    const/4 v3, 0x0

    .line 29
    :goto_0
    int-to-long v4, v3

    .line 30
    const-wide/16 v6, 0x8

    .line 31
    .line 32
    sub-long v6, v1, v6

    .line 33
    .line 34
    cmp-long v8, v4, v6

    .line 35
    .line 36
    if-gtz v8, :cond_b

    .line 37
    .line 38
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/fs/filesystem/Property;->getUShort(I)I

    .line 39
    .line 40
    .line 41
    add-int/lit8 v4, v3, 0x2

    .line 42
    .line 43
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/fs/filesystem/Property;->getUShort(I)I

    .line 44
    .line 45
    .line 46
    move-result v5

    .line 47
    const/4 v6, 0x2

    .line 48
    add-int/2addr v4, v6

    .line 49
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/fs/filesystem/Property;->getInt(I)I

    .line 50
    .line 51
    .line 52
    move-result v7

    .line 53
    add-int/lit8 v4, v4, 0x4

    .line 54
    .line 55
    if-gez v7, :cond_2

    .line 56
    .line 57
    goto/16 :goto_5

    .line 58
    .line 59
    :cond_2
    if-nez v5, :cond_3

    .line 60
    .line 61
    goto/16 :goto_4

    .line 62
    .line 63
    :cond_3
    const v8, 0xf018

    .line 64
    .line 65
    .line 66
    sub-int/2addr v5, v8

    .line 67
    :try_start_0
    invoke-static {v5}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->create(I)Lcom/intsig/office/fc/hslf/usermodel/PictureData;

    .line 68
    .line 69
    .line 70
    move-result-object v5

    .line 71
    invoke-virtual {v5, v3}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->setOffset(I)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->getType()I

    .line 75
    .line 76
    .line 77
    move-result v3

    .line 78
    const/4 v8, 0x5

    .line 79
    const/4 v9, 0x3

    .line 80
    const/4 v10, 0x6

    .line 81
    if-eq v3, v8, :cond_4

    .line 82
    .line 83
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->getType()I

    .line 84
    .line 85
    .line 86
    move-result v3

    .line 87
    if-eq v3, v10, :cond_4

    .line 88
    .line 89
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->getType()I

    .line 90
    .line 91
    .line 92
    move-result v3

    .line 93
    const/4 v8, 0x7

    .line 94
    if-eq v3, v8, :cond_4

    .line 95
    .line 96
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->getType()I

    .line 97
    .line 98
    .line 99
    move-result v3

    .line 100
    if-eq v3, v9, :cond_4

    .line 101
    .line 102
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->getType()I

    .line 103
    .line 104
    .line 105
    move-result v3

    .line 106
    if-ne v3, v6, :cond_a

    .line 107
    .line 108
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    .line 109
    .line 110
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    .line 112
    .line 113
    invoke-static {}, Lcom/intsig/utils/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object v8

    .line 117
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    const-string v8, ".tmp"

    .line 121
    .line 122
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v3

    .line 129
    new-instance v8, Ljava/io/File;

    .line 130
    .line 131
    new-instance v11, Ljava/lang/StringBuilder;

    .line 132
    .line 133
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    .line 135
    .line 136
    iget-object v12, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->control:Lcom/intsig/office/system/IControl;

    .line 137
    .line 138
    invoke-interface {v12}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 139
    .line 140
    .line 141
    move-result-object v12

    .line 142
    invoke-virtual {v12}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 143
    .line 144
    .line 145
    move-result-object v12

    .line 146
    invoke-virtual {v12}, Lcom/intsig/office/common/picture/PictureManage;->getPicTempPath()Ljava/lang/String;

    .line 147
    .line 148
    .line 149
    move-result-object v12

    .line 150
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    .line 152
    .line 153
    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    .line 154
    .line 155
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    .line 157
    .line 158
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    .line 160
    .line 161
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object v3

    .line 165
    invoke-direct {v8, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 166
    .line 167
    .line 168
    :try_start_1
    invoke-virtual {v8}, Ljava/io/File;->createNewFile()Z

    .line 169
    .line 170
    .line 171
    new-instance v3, Ljava/io/FileOutputStream;

    .line 172
    .line 173
    invoke-direct {v3, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 174
    .line 175
    .line 176
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->getType()I

    .line 177
    .line 178
    .line 179
    move-result v11

    .line 180
    if-eq v11, v9, :cond_8

    .line 181
    .line 182
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->getType()I

    .line 183
    .line 184
    .line 185
    move-result v9

    .line 186
    if-ne v9, v6, :cond_5

    .line 187
    .line 188
    goto :goto_1

    .line 189
    :cond_5
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->getType()I

    .line 190
    .line 191
    .line 192
    move-result v6

    .line 193
    if-ne v6, v10, :cond_7

    .line 194
    .line 195
    add-int/lit8 v6, v4, 0x11

    .line 196
    .line 197
    invoke-virtual {v0, v6}, Lcom/intsig/office/fc/fs/filesystem/Property;->getLong(I)J

    .line 198
    .line 199
    .line 200
    move-result-wide v9

    .line 201
    const-wide v11, 0xa1a0a0d474e5089L

    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    cmp-long v13, v9, v11

    .line 207
    .line 208
    if-nez v13, :cond_6

    .line 209
    .line 210
    add-int/lit8 v9, v7, -0x11

    .line 211
    .line 212
    invoke-virtual {v0, v3, v6, v9}, Lcom/intsig/office/fc/fs/filesystem/Property;->writeByte(Ljava/io/OutputStream;II)V

    .line 213
    .line 214
    .line 215
    goto :goto_2

    .line 216
    :cond_6
    add-int/lit8 v6, v4, 0x21

    .line 217
    .line 218
    invoke-virtual {v0, v6}, Lcom/intsig/office/fc/fs/filesystem/Property;->getLong(I)J

    .line 219
    .line 220
    .line 221
    move-result-wide v9

    .line 222
    cmp-long v13, v9, v11

    .line 223
    .line 224
    if-nez v13, :cond_9

    .line 225
    .line 226
    add-int/lit8 v9, v7, -0x21

    .line 227
    .line 228
    invoke-virtual {v0, v3, v6, v9}, Lcom/intsig/office/fc/fs/filesystem/Property;->writeByte(Ljava/io/OutputStream;II)V

    .line 229
    .line 230
    .line 231
    goto :goto_2

    .line 232
    :cond_7
    add-int/lit8 v6, v4, 0x11

    .line 233
    .line 234
    add-int/lit8 v9, v7, -0x11

    .line 235
    .line 236
    invoke-virtual {v0, v3, v6, v9}, Lcom/intsig/office/fc/fs/filesystem/Property;->writeByte(Ljava/io/OutputStream;II)V

    .line 237
    .line 238
    .line 239
    goto :goto_2

    .line 240
    :cond_8
    :goto_1
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->getOffset()I

    .line 241
    .line 242
    .line 243
    move-result v6

    .line 244
    invoke-virtual {v0, v6}, Lcom/intsig/office/fc/fs/filesystem/Property;->getRecordData(I)[B

    .line 245
    .line 246
    .line 247
    move-result-object v6

    .line 248
    invoke-virtual {v5, v6}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->setRawData([B)V

    .line 249
    .line 250
    .line 251
    move-object v6, v5

    .line 252
    check-cast v6, Lcom/intsig/office/fc/hslf/blip/Metafile;

    .line 253
    .line 254
    invoke-virtual {v6, v3}, Lcom/intsig/office/fc/hslf/blip/Metafile;->writeByte_WMFAndEMF(Ljava/io/FileOutputStream;)V

    .line 255
    .line 256
    .line 257
    :cond_9
    :goto_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 258
    .line 259
    .line 260
    goto :goto_3

    .line 261
    :catch_0
    move-exception v3

    .line 262
    :try_start_2
    iget-object v6, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->control:Lcom/intsig/office/system/IControl;

    .line 263
    .line 264
    invoke-interface {v6}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 265
    .line 266
    .line 267
    move-result-object v6

    .line 268
    invoke-virtual {v6}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    .line 269
    .line 270
    .line 271
    move-result-object v6

    .line 272
    invoke-virtual {v6, v3}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;)V

    .line 273
    .line 274
    .line 275
    :goto_3
    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 276
    .line 277
    .line 278
    move-result-object v3

    .line 279
    invoke-virtual {v5, v3}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->setTempFilePath(Ljava/lang/String;)V

    .line 280
    .line 281
    .line 282
    :cond_a
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    .line 283
    .line 284
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 285
    .line 286
    .line 287
    goto :goto_4

    .line 288
    :catch_1
    move-exception v3

    .line 289
    iget-object v5, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->control:Lcom/intsig/office/system/IControl;

    .line 290
    .line 291
    invoke-interface {v5}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 292
    .line 293
    .line 294
    move-result-object v5

    .line 295
    invoke-virtual {v5}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    .line 296
    .line 297
    .line 298
    move-result-object v5

    .line 299
    invoke-virtual {v5, v3}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;)V

    .line 300
    .line 301
    .line 302
    :goto_4
    add-int v3, v4, v7

    .line 303
    .line 304
    goto/16 :goto_0

    .line 305
    .line 306
    :cond_b
    :goto_5
    return-void
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private readPowerPointStream()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->cfbFS:Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

    .line 2
    .line 3
    const-string v1, "PowerPoint Document"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->getPropertyRawData(Ljava/lang/String;)[B

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_docstream:[B

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->cfbFS:Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->getProperty(Ljava/lang/String;)Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_docProp:Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
.end method


# virtual methods
.method public addPicture(Lcom/intsig/office/fc/hslf/usermodel/PictureData;)I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->readPictures()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :catch_0
    move-exception p1

    .line 10
    new-instance v0, Lcom/intsig/office/fc/hslf/exceptions/CorruptPowerPointFileException;

    .line 11
    .line 12
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    throw v0

    .line 20
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    .line 21
    .line 22
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-lez v0, :cond_1

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    .line 29
    .line 30
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    add-int/lit8 v1, v1, -0x1

    .line 35
    .line 36
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    check-cast v0, Lcom/intsig/office/fc/hslf/usermodel/PictureData;

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->getOffset()I

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->getRawData()[B

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    array-length v0, v0

    .line 51
    add-int/2addr v1, v0

    .line 52
    add-int/lit8 v1, v1, 0x8

    .line 53
    .line 54
    goto :goto_1

    .line 55
    :cond_1
    const/4 v1, 0x0

    .line 56
    :goto_1
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->setOffset(I)V

    .line 57
    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    .line 60
    .line 61
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    return v1
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public declared-synchronized appendRootLevelRecord(Lcom/intsig/office/fc/hslf/record/Record;)I
    .locals 8

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_records:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 3
    .line 4
    array-length v1, v0

    .line 5
    const/4 v2, 0x1

    .line 6
    add-int/2addr v1, v2

    .line 7
    new-array v1, v1, [Lcom/intsig/office/fc/hslf/record/Record;

    .line 8
    .line 9
    array-length v0, v0

    .line 10
    sub-int/2addr v0, v2

    .line 11
    const/4 v3, -0x1

    .line 12
    const/4 v4, 0x0

    .line 13
    :goto_0
    if-ltz v0, :cond_2

    .line 14
    .line 15
    if-eqz v4, :cond_0

    .line 16
    .line 17
    iget-object v5, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_records:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 18
    .line 19
    aget-object v5, v5, v0

    .line 20
    .line 21
    aput-object v5, v1, v0

    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_0
    add-int/lit8 v5, v0, 0x1

    .line 25
    .line 26
    iget-object v6, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_records:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 27
    .line 28
    aget-object v7, v6, v0

    .line 29
    .line 30
    aput-object v7, v1, v5

    .line 31
    .line 32
    aget-object v5, v6, v0

    .line 33
    .line 34
    instance-of v5, v5, Lcom/intsig/office/fc/hslf/record/PersistPtrHolder;

    .line 35
    .line 36
    if-eqz v5, :cond_1

    .line 37
    .line 38
    aput-object p1, v1, v0

    .line 39
    .line 40
    move v3, v0

    .line 41
    const/4 v4, 0x1

    .line 42
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, -0x1

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_2
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_records:[Lcom/intsig/office/fc/hslf/record/Record;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    .line 47
    monitor-exit p0

    .line 48
    return v3

    .line 49
    :catchall_0
    move-exception p1

    .line 50
    monitor-exit p0

    .line 51
    throw p1
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public dispose()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->currentUser:Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->dispose()V

    .line 7
    .line 8
    .line 9
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->currentUser:Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;

    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_records:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    if-eqz v0, :cond_2

    .line 15
    .line 16
    array-length v3, v0

    .line 17
    const/4 v4, 0x0

    .line 18
    :goto_0
    if-ge v4, v3, :cond_1

    .line 19
    .line 20
    aget-object v5, v0, v4

    .line 21
    .line 22
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/record/Record;->dispose()V

    .line 23
    .line 24
    .line 25
    add-int/lit8 v4, v4, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_records:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 29
    .line 30
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    .line 31
    .line 32
    if-eqz v0, :cond_4

    .line 33
    .line 34
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    if-eqz v3, :cond_3

    .line 43
    .line 44
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    check-cast v3, Lcom/intsig/office/fc/hslf/usermodel/PictureData;

    .line 49
    .line 50
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->dispose()V

    .line 51
    .line 52
    .line 53
    goto :goto_1

    .line 54
    :cond_3
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    .line 55
    .line 56
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 57
    .line 58
    .line 59
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    .line 60
    .line 61
    :cond_4
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_objects:[Lcom/intsig/office/fc/hslf/usermodel/ObjectData;

    .line 62
    .line 63
    if-eqz v0, :cond_6

    .line 64
    .line 65
    array-length v3, v0

    .line 66
    :goto_2
    if-ge v2, v3, :cond_5

    .line 67
    .line 68
    aget-object v4, v0, v2

    .line 69
    .line 70
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/usermodel/ObjectData;->dispose()V

    .line 71
    .line 72
    .line 73
    add-int/lit8 v2, v2, 0x1

    .line 74
    .line 75
    goto :goto_2

    .line 76
    :cond_5
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_objects:[Lcom/intsig/office/fc/hslf/usermodel/ObjectData;

    .line 77
    .line 78
    :cond_6
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->cfbFS:Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

    .line 79
    .line 80
    if-eqz v0, :cond_7

    .line 81
    .line 82
    invoke-virtual {v0}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->dispose()V

    .line 83
    .line 84
    .line 85
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->cfbFS:Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

    .line 86
    .line 87
    :cond_7
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->control:Lcom/intsig/office/system/IControl;

    .line 88
    .line 89
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_docstream:[B

    .line 90
    .line 91
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public getCurrentUserAtom()Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->currentUser:Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEmbeddedObjects()[Lcom/intsig/office/fc/hslf/usermodel/ObjectData;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_objects:[Lcom/intsig/office/fc/hslf/usermodel/ObjectData;

    .line 2
    .line 3
    if-nez v0, :cond_2

    .line 4
    .line 5
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_records:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 12
    .line 13
    array-length v3, v2

    .line 14
    if-ge v1, v3, :cond_1

    .line 15
    .line 16
    aget-object v2, v2, v1

    .line 17
    .line 18
    instance-of v3, v2, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;

    .line 19
    .line 20
    if-eqz v3, :cond_0

    .line 21
    .line 22
    new-instance v3, Lcom/intsig/office/fc/hslf/usermodel/ObjectData;

    .line 23
    .line 24
    check-cast v2, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;

    .line 25
    .line 26
    invoke-direct {v3, v2}, Lcom/intsig/office/fc/hslf/usermodel/ObjectData;-><init>(Lcom/intsig/office/fc/hslf/record/ExOleObjStg;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    new-array v1, v1, [Lcom/intsig/office/fc/hslf/usermodel/ObjectData;

    .line 40
    .line 41
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    check-cast v0, [Lcom/intsig/office/fc/hslf/usermodel/ObjectData;

    .line 46
    .line 47
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_objects:[Lcom/intsig/office/fc/hslf/usermodel/ObjectData;

    .line 48
    .line 49
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_objects:[Lcom/intsig/office/fc/hslf/usermodel/ObjectData;

    .line 50
    .line 51
    return-object v0
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getPictures()[Lcom/intsig/office/fc/hslf/usermodel/PictureData;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->readPictures()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 7
    .line 8
    .line 9
    goto :goto_0

    .line 10
    :catch_0
    move-exception v0

    .line 11
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->control:Lcom/intsig/office/system/IControl;

    .line 12
    .line 13
    invoke-interface {v2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-virtual {v2}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    const/4 v3, 0x1

    .line 22
    invoke-virtual {v2, v0, v3}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;Z)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->control:Lcom/intsig/office/system/IControl;

    .line 26
    .line 27
    const/16 v2, 0x17

    .line 28
    .line 29
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 30
    .line 31
    invoke-interface {v0, v2, v3}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 32
    .line 33
    .line 34
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->control:Lcom/intsig/office/system/IControl;

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :catch_1
    move-exception v0

    .line 38
    new-instance v1, Lcom/intsig/office/fc/hslf/exceptions/CorruptPowerPointFileException;

    .line 39
    .line 40
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    throw v1

    .line 48
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_pictures:Ljava/util/List;

    .line 49
    .line 50
    if-eqz v0, :cond_1

    .line 51
    .line 52
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    new-array v1, v1, [Lcom/intsig/office/fc/hslf/usermodel/PictureData;

    .line 57
    .line 58
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    check-cast v0, [Lcom/intsig/office/fc/hslf/usermodel/PictureData;

    .line 63
    .line 64
    return-object v0

    .line 65
    :cond_1
    return-object v1
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getRecords()[Lcom/intsig/office/fc/hslf/record/Record;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_records:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getUnderlyingBytes()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->_docstream:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

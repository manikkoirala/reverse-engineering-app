.class public Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;
.super Ljava/lang/Object;
.source "SlideListWithText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hslf/record/SlideListWithText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SlideAtomsSet"
.end annotation


# instance fields
.field private slidePersistAtom:Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;

.field private slideRecords:[Lcom/intsig/office/fc/hslf/record/Record;

.field final synthetic this$0:Lcom/intsig/office/fc/hslf/record/SlideListWithText;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hslf/record/SlideListWithText;Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;[Lcom/intsig/office/fc/hslf/record/Record;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;->this$0:Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;->slidePersistAtom:Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;

    .line 7
    .line 8
    iput-object p3, p0, Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;->slideRecords:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public dispose()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;->slidePersistAtom:Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->dispose()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;->slideRecords:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 9
    .line 10
    if-eqz v0, :cond_2

    .line 11
    .line 12
    array-length v1, v0

    .line 13
    const/4 v2, 0x0

    .line 14
    :goto_0
    if-ge v2, v1, :cond_1

    .line 15
    .line 16
    aget-object v3, v0, v2

    .line 17
    .line 18
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/record/Record;->dispose()V

    .line 19
    .line 20
    .line 21
    add-int/lit8 v2, v2, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    const/4 v0, 0x0

    .line 25
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;->slideRecords:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 26
    .line 27
    :cond_2
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getSlidePersistAtom()Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;->slidePersistAtom:Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideRecords()[Lcom/intsig/office/fc/hslf/record/Record;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;->slideRecords:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

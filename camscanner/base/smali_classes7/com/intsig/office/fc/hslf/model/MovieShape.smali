.class public final Lcom/intsig/office/fc/hslf/model/MovieShape;
.super Lcom/intsig/office/fc/hslf/model/Picture;
.source "MovieShape.java"


# static fields
.field public static final DEFAULT_MOVIE_THUMBNAIL:I = -0x1

.field public static final MOVIE_AVI:I = 0x2

.field public static final MOVIE_MPEG:I = 0x1


# direct methods
.method public constructor <init>(II)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p2, v0}, Lcom/intsig/office/fc/hslf/model/Picture;-><init>(ILcom/intsig/office/fc/hslf/model/Shape;)V

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/model/MovieShape;->setMovieIndex(I)V

    const/4 p1, 0x1

    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/model/MovieShape;->setAutoPlay(Z)V

    return-void
.end method

.method public constructor <init>(IILcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 0

    .line 4
    invoke-direct {p0, p2, p3}, Lcom/intsig/office/fc/hslf/model/Picture;-><init>(ILcom/intsig/office/fc/hslf/model/Shape;)V

    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/model/MovieShape;->setMovieIndex(I)V

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 0

    .line 6
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/Picture;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    return-void
.end method


# virtual methods
.method protected createSpContainer(IZ)Lcom/intsig/office/fc/ddf/EscherContainerRecord;
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/Picture;->createSpContainer(IZ)Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 6
    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public getPath()Ljava/lang/String;
    .locals 6

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->OEShapeAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 4
    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getClientDataRecord(I)Lcom/intsig/office/fc/hslf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hslf/record/OEShapeAtom;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/OEShapeAtom;->getOptions()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSlideShow()Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getDocumentRecord()Lcom/intsig/office/fc/hslf/record/Document;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    sget-object v2, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExObjList:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 28
    .line 29
    iget v2, v2, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 30
    .line 31
    int-to-long v2, v2

    .line 32
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    check-cast v1, Lcom/intsig/office/fc/hslf/record/ExObjList;

    .line 37
    .line 38
    const/4 v2, 0x0

    .line 39
    if-nez v1, :cond_0

    .line 40
    .line 41
    return-object v2

    .line 42
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    const/4 v3, 0x0

    .line 47
    :goto_0
    array-length v4, v1

    .line 48
    if-ge v3, v4, :cond_2

    .line 49
    .line 50
    aget-object v4, v1, v3

    .line 51
    .line 52
    instance-of v5, v4, Lcom/intsig/office/fc/hslf/record/ExMCIMovie;

    .line 53
    .line 54
    if-eqz v5, :cond_1

    .line 55
    .line 56
    check-cast v4, Lcom/intsig/office/fc/hslf/record/ExMCIMovie;

    .line 57
    .line 58
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/ExMCIMovie;->getExVideo()Lcom/intsig/office/fc/hslf/record/ExVideoContainer;

    .line 59
    .line 60
    .line 61
    move-result-object v4

    .line 62
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/ExVideoContainer;->getExMediaAtom()Lcom/intsig/office/fc/hslf/record/ExMediaAtom;

    .line 63
    .line 64
    .line 65
    move-result-object v5

    .line 66
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/record/ExMediaAtom;->getObjectId()I

    .line 67
    .line 68
    .line 69
    move-result v5

    .line 70
    if-ne v5, v0, :cond_1

    .line 71
    .line 72
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/ExVideoContainer;->getPathAtom()Lcom/intsig/office/fc/hslf/record/CString;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/CString;->getText()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    return-object v0

    .line 81
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 82
    .line 83
    goto :goto_0

    .line 84
    :cond_2
    return-object v2
    .line 85
    .line 86
.end method

.method public isAutoPlay()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->AnimationInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 4
    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getClientDataRecord(I)Lcom/intsig/office/fc/hslf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hslf/record/AnimationInfo;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/AnimationInfo;->getAnimationInfoAtom()Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const/4 v1, 0x4

    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;->getFlag(I)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    return v0

    .line 23
    :cond_0
    const/4 v0, 0x0

    .line 24
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public setAutoPlay(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->AnimationInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 4
    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getClientDataRecord(I)Lcom/intsig/office/fc/hslf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hslf/record/AnimationInfo;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/AnimationInfo;->getAnimationInfoAtom()Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const/4 v1, 0x4

    .line 18
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;->setFlag(IZ)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->updateClientData()V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
.end method

.method public setMovieIndex(I)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->OEShapeAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 4
    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getClientDataRecord(I)Lcom/intsig/office/fc/hslf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hslf/record/OEShapeAtom;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/record/OEShapeAtom;->setOptions(I)V

    .line 12
    .line 13
    .line 14
    sget-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->AnimationInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 15
    .line 16
    iget v0, v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 17
    .line 18
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getClientDataRecord(I)Lcom/intsig/office/fc/hslf/record/Record;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    check-cast v0, Lcom/intsig/office/fc/hslf/record/AnimationInfo;

    .line 23
    .line 24
    if-eqz v0, :cond_0

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/AnimationInfo;->getAnimationInfoAtom()Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    const/high16 v1, 0x7000000

    .line 31
    .line 32
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;->setDimColor(I)V

    .line 33
    .line 34
    .line 35
    const/4 v1, 0x4

    .line 36
    const/4 v2, 0x1

    .line 37
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;->setFlag(IZ)V

    .line 38
    .line 39
    .line 40
    const/16 v1, 0x100

    .line 41
    .line 42
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;->setFlag(IZ)V

    .line 43
    .line 44
    .line 45
    const/16 v1, 0x400

    .line 46
    .line 47
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;->setFlag(IZ)V

    .line 48
    .line 49
    .line 50
    add-int/2addr p1, v2

    .line 51
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;->setOrderID(I)V

    .line 52
    .line 53
    .line 54
    :cond_0
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.class public abstract Lcom/intsig/office/fc/hslf/model/Shape;
.super Ljava/lang/Object;
.source "Shape.java"


# instance fields
.field protected _escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

.field protected _fill:Lcom/intsig/office/fc/hslf/model/Fill;

.field protected _parent:Lcom/intsig/office/fc/hslf/model/Shape;

.field protected _sheet:Lcom/intsig/office/fc/hslf/model/Sheet;


# direct methods
.method protected constructor <init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_parent:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-ne v1, p1, :cond_0

    .line 22
    .line 23
    return-object v0

    .line 24
    :cond_1
    const/4 p0, 0x0

    .line 25
    return-object p0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static getEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;
    .locals 2

    if-eqz p0, :cond_1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->getEscherProperties()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherProperty;->getPropertyNumber()S

    move-result v1

    if-ne v1, p1, :cond_0

    return-object v0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->getEscherProperties()Ljava/util/List;

    move-result-object v0

    .line 2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 4
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherProperty;->getId()S

    move-result v1

    if-ne v1, p1, :cond_0

    .line 5
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    if-eq p2, v0, :cond_2

    .line 6
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->sortProperties()V

    :cond_2
    return-void
.end method


# virtual methods
.method protected afterInsert(Lcom/intsig/office/fc/hslf/model/Sheet;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected abstract createSpContainer(Z)Lcom/intsig/office/fc/ddf/EscherContainerRecord;
.end method

.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_parent:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_sheet:Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 7
    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->dispose()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 14
    .line 15
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_fill:Lcom/intsig/office/fc/hslf/model/Fill;

    .line 16
    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/Fill;->dispose()V

    .line 20
    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_fill:Lcom/intsig/office/fc/hslf/model/Fill;

    .line 23
    .line 24
    :cond_1
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getAdjustmentValue()[Ljava/lang/Float;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getAdjustmentValue(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)[Ljava/lang/Float;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAnchor()Lcom/intsig/office/java/awt/Rectangle;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff6

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->getFlags()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    and-int/lit8 v0, v0, 0x2

    .line 16
    .line 17
    const/16 v1, -0xff0

    .line 18
    .line 19
    const/high16 v2, 0x44100000    # 576.0f

    .line 20
    .line 21
    const/high16 v3, 0x42900000    # 72.0f

    .line 22
    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 26
    .line 27
    const/16 v4, -0xff1

    .line 28
    .line 29
    invoke-static {v0, v4}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;

    .line 34
    .line 35
    if-nez v0, :cond_0

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 38
    .line 39
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;

    .line 44
    .line 45
    new-instance v1, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getCol1()S

    .line 48
    .line 49
    .line 50
    move-result v4

    .line 51
    int-to-float v4, v4

    .line 52
    mul-float v4, v4, v3

    .line 53
    .line 54
    div-float/2addr v4, v2

    .line 55
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getFlag()S

    .line 56
    .line 57
    .line 58
    move-result v5

    .line 59
    int-to-float v5, v5

    .line 60
    mul-float v5, v5, v3

    .line 61
    .line 62
    div-float/2addr v5, v2

    .line 63
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getDx1()S

    .line 64
    .line 65
    .line 66
    move-result v6

    .line 67
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getCol1()S

    .line 68
    .line 69
    .line 70
    move-result v7

    .line 71
    sub-int/2addr v6, v7

    .line 72
    int-to-float v6, v6

    .line 73
    mul-float v6, v6, v3

    .line 74
    .line 75
    div-float/2addr v6, v2

    .line 76
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getRow1()S

    .line 77
    .line 78
    .line 79
    move-result v7

    .line 80
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getFlag()S

    .line 81
    .line 82
    .line 83
    move-result v0

    .line 84
    sub-int/2addr v7, v0

    .line 85
    int-to-float v0, v7

    .line 86
    mul-float v0, v0, v3

    .line 87
    .line 88
    div-float/2addr v0, v2

    .line 89
    invoke-direct {v1, v4, v5, v6, v0}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 90
    .line 91
    .line 92
    goto :goto_0

    .line 93
    :cond_0
    new-instance v1, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;

    .line 94
    .line 95
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDx1()I

    .line 96
    .line 97
    .line 98
    move-result v4

    .line 99
    int-to-float v4, v4

    .line 100
    mul-float v4, v4, v3

    .line 101
    .line 102
    div-float/2addr v4, v2

    .line 103
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDy1()I

    .line 104
    .line 105
    .line 106
    move-result v5

    .line 107
    int-to-float v5, v5

    .line 108
    mul-float v5, v5, v3

    .line 109
    .line 110
    div-float/2addr v5, v2

    .line 111
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDx2()I

    .line 112
    .line 113
    .line 114
    move-result v6

    .line 115
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDx1()I

    .line 116
    .line 117
    .line 118
    move-result v7

    .line 119
    sub-int/2addr v6, v7

    .line 120
    int-to-float v6, v6

    .line 121
    mul-float v6, v6, v3

    .line 122
    .line 123
    div-float/2addr v6, v2

    .line 124
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDy2()I

    .line 125
    .line 126
    .line 127
    move-result v7

    .line 128
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDy1()I

    .line 129
    .line 130
    .line 131
    move-result v0

    .line 132
    sub-int/2addr v7, v0

    .line 133
    int-to-float v0, v7

    .line 134
    mul-float v0, v0, v3

    .line 135
    .line 136
    div-float/2addr v0, v2

    .line 137
    invoke-direct {v1, v4, v5, v6, v0}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 138
    .line 139
    .line 140
    goto :goto_0

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 142
    .line 143
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 144
    .line 145
    .line 146
    move-result-object v0

    .line 147
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;

    .line 148
    .line 149
    new-instance v1, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;

    .line 150
    .line 151
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getCol1()S

    .line 152
    .line 153
    .line 154
    move-result v4

    .line 155
    int-to-float v4, v4

    .line 156
    mul-float v4, v4, v3

    .line 157
    .line 158
    div-float/2addr v4, v2

    .line 159
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getFlag()S

    .line 160
    .line 161
    .line 162
    move-result v5

    .line 163
    int-to-float v5, v5

    .line 164
    mul-float v5, v5, v3

    .line 165
    .line 166
    div-float/2addr v5, v2

    .line 167
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getDx1()S

    .line 168
    .line 169
    .line 170
    move-result v6

    .line 171
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getCol1()S

    .line 172
    .line 173
    .line 174
    move-result v7

    .line 175
    sub-int/2addr v6, v7

    .line 176
    int-to-float v6, v6

    .line 177
    mul-float v6, v6, v3

    .line 178
    .line 179
    div-float/2addr v6, v2

    .line 180
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getRow1()S

    .line 181
    .line 182
    .line 183
    move-result v7

    .line 184
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getFlag()S

    .line 185
    .line 186
    .line 187
    move-result v0

    .line 188
    sub-int/2addr v7, v0

    .line 189
    int-to-float v0, v7

    .line 190
    mul-float v0, v0, v3

    .line 191
    .line 192
    div-float/2addr v0, v2

    .line 193
    invoke-direct {v1, v4, v5, v6, v0}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 194
    .line 195
    .line 196
    :goto_0
    return-object v1
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method protected getColor(II)Lcom/intsig/office/java/awt/Color;
    .locals 3

    .line 1
    const/high16 v0, 0x8000000

    .line 2
    .line 3
    if-lt p1, v0, :cond_0

    .line 4
    .line 5
    sub-int v0, p1, v0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/Sheet;->getColorScheme()Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    if-ltz v0, :cond_0

    .line 16
    .line 17
    const/4 v2, 0x7

    .line 18
    if-gt v0, v2, :cond_0

    .line 19
    .line 20
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;->getColor(I)I

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    :cond_0
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 25
    .line 26
    const/4 v1, 0x1

    .line 27
    invoke-direct {v0, p1, v1}, Lcom/intsig/office/java/awt/Color;-><init>(IZ)V

    .line 28
    .line 29
    .line 30
    new-instance p1, Lcom/intsig/office/java/awt/Color;

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/Color;->getBlue()I

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/Color;->getGreen()I

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/Color;->getRed()I

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    invoke-direct {p1, v1, v2, v0, p2}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    .line 45
    .line 46
    .line 47
    return-object p1
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public getEndArrowLength()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getEndArrowLength(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEndArrowType()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const/16 v1, 0x1d1

    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 20
    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-lez v1, :cond_0

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    return v0

    .line 34
    :cond_0
    const/4 v0, 0x0

    .line 35
    return v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getEndArrowWidth()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getEndArrowWidth(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEscherProperty(S)I
    .locals 2

    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    const/16 v1, -0xff5

    invoke-static {v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 5
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 6
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result p1

    :goto_0
    return p1
.end method

.method public getEscherProperty(SI)I
    .locals 2

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    const/16 v1, -0xff5

    invoke-static {v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    if-nez p1, :cond_0

    goto :goto_0

    .line 9
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result p2

    :goto_0
    return p2
.end method

.method public getFill()Lcom/intsig/office/fc/hslf/model/Fill;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_fill:Lcom/intsig/office/fc/hslf/model/Fill;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/hslf/model/Fill;

    .line 6
    .line 7
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hslf/model/Fill;-><init>(Lcom/intsig/office/fc/hslf/model/Shape;)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_fill:Lcom/intsig/office/fc/hslf/model/Fill;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_fill:Lcom/intsig/office/fc/hslf/model/Fill;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFillColor()Lcom/intsig/office/java/awt/Color;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getFill()Lcom/intsig/office/fc/hslf/model/Fill;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Fill;->getForegroundColor()Lcom/intsig/office/java/awt/Color;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFlipHorizontal()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getFlipHorizontal(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFlipVertical()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getFlipVertical(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHyperlink()Lcom/intsig/office/fc/hslf/model/Hyperlink;
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/hslf/model/Hyperlink;->find(Lcom/intsig/office/fc/hslf/model/Shape;)Lcom/intsig/office/fc/hslf/model/Hyperlink;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLineColor()Lcom/intsig/office/java/awt/Color;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const/4 v2, 0x2

    .line 10
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ShapeKit;->getLineColor(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/lang/Object;I)Lcom/intsig/office/java/awt/Color;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLineWidth()D
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getLineWidth(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-double v0, v0

    .line 10
    return-wide v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLogicalAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMasterShapeID()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getMasterShapeID(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOutline()Lcom/intsig/office/java/awt/Shape;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getLogicalAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getParent()Lcom/intsig/office/fc/hslf/model/Shape;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_parent:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRotation()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getRotation(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShapeId()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getShapeId(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShapeName()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeType()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/hslf/model/ShapeTypes;->typeName(I)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShapeType()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getShapeType(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_sheet:Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStartArrowLength()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getStartArrowLength(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStartArrowType()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const/16 v1, 0x1d0

    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 20
    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-lez v1, :cond_0

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    return v0

    .line 34
    :cond_0
    const/4 v0, 0x0

    .line 35
    return v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getStartArrowWidth()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getStartArrowWidth(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public hasLine()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->hasLine(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isHidden()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->isHidden(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public moveTo(FF)V
    .locals 10

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 2
    .line 3
    .line 4
    move-result-object v9

    .line 5
    float-to-double v1, p1

    .line 6
    float-to-double v3, p2

    .line 7
    invoke-virtual {v9}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 8
    .line 9
    .line 10
    move-result-wide v5

    .line 11
    invoke-virtual {v9}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 12
    .line 13
    .line 14
    move-result-wide v7

    .line 15
    move-object v0, v9

    .line 16
    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Rectangle2D;->setRect(DDDD)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0, v9}, Lcom/intsig/office/fc/hslf/model/Shape;->setAnchor(Lcom/intsig/office/java/awt/geom/Rectangle2D;)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setAnchor(Lcom/intsig/office/java/awt/geom/Rectangle2D;)V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff6

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->getFlags()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    and-int/lit8 v0, v0, 0x2

    .line 16
    .line 17
    const-wide/high16 v1, 0x4052000000000000L    # 72.0

    .line 18
    .line 19
    const-wide/high16 v3, 0x4082000000000000L    # 576.0

    .line 20
    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 24
    .line 25
    const/16 v5, -0xff1

    .line 26
    .line 27
    invoke-static {v0, v5}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 34
    .line 35
    .line 36
    move-result-wide v5

    .line 37
    mul-double v5, v5, v3

    .line 38
    .line 39
    div-double/2addr v5, v1

    .line 40
    double-to-int v5, v5

    .line 41
    invoke-virtual {v0, v5}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->setDx1(I)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 45
    .line 46
    .line 47
    move-result-wide v5

    .line 48
    mul-double v5, v5, v3

    .line 49
    .line 50
    div-double/2addr v5, v1

    .line 51
    double-to-int v5, v5

    .line 52
    invoke-virtual {v0, v5}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->setDy1(I)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 56
    .line 57
    .line 58
    move-result-wide v5

    .line 59
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 60
    .line 61
    .line 62
    move-result-wide v7

    .line 63
    add-double/2addr v5, v7

    .line 64
    mul-double v5, v5, v3

    .line 65
    .line 66
    div-double/2addr v5, v1

    .line 67
    double-to-int v5, v5

    .line 68
    invoke-virtual {v0, v5}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->setDx2(I)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 72
    .line 73
    .line 74
    move-result-wide v5

    .line 75
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 76
    .line 77
    .line 78
    move-result-wide v7

    .line 79
    add-double/2addr v5, v7

    .line 80
    mul-double v5, v5, v3

    .line 81
    .line 82
    div-double/2addr v5, v1

    .line 83
    double-to-int p1, v5

    .line 84
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->setDy2(I)V

    .line 85
    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 89
    .line 90
    const/16 v5, -0xff0

    .line 91
    .line 92
    invoke-static {v0, v5}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;

    .line 97
    .line 98
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 99
    .line 100
    .line 101
    move-result-wide v5

    .line 102
    mul-double v5, v5, v3

    .line 103
    .line 104
    div-double/2addr v5, v1

    .line 105
    double-to-int v5, v5

    .line 106
    int-to-short v5, v5

    .line 107
    invoke-virtual {v0, v5}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->setFlag(S)V

    .line 108
    .line 109
    .line 110
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 111
    .line 112
    .line 113
    move-result-wide v5

    .line 114
    mul-double v5, v5, v3

    .line 115
    .line 116
    div-double/2addr v5, v1

    .line 117
    double-to-int v5, v5

    .line 118
    int-to-short v5, v5

    .line 119
    invoke-virtual {v0, v5}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->setCol1(S)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 123
    .line 124
    .line 125
    move-result-wide v5

    .line 126
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 127
    .line 128
    .line 129
    move-result-wide v7

    .line 130
    add-double/2addr v5, v7

    .line 131
    mul-double v5, v5, v3

    .line 132
    .line 133
    div-double/2addr v5, v1

    .line 134
    double-to-int v5, v5

    .line 135
    int-to-short v5, v5

    .line 136
    invoke-virtual {v0, v5}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->setDx1(S)V

    .line 137
    .line 138
    .line 139
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 140
    .line 141
    .line 142
    move-result-wide v5

    .line 143
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 144
    .line 145
    .line 146
    move-result-wide v7

    .line 147
    add-double/2addr v5, v7

    .line 148
    mul-double v5, v5, v3

    .line 149
    .line 150
    div-double/2addr v5, v1

    .line 151
    double-to-int p1, v5

    .line 152
    int-to-short p1, p1

    .line 153
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->setRow1(S)V

    .line 154
    .line 155
    .line 156
    :goto_0
    return-void
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public setEscherProperty(SI)V
    .locals 2

    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    const/16 v1, -0xff5

    invoke-static {v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 9
    invoke-static {v0, p1, p2}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    return-void
.end method

.method public setShapeId(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff6

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setShapeId(I)V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setShapeType(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff6

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 10
    .line 11
    shl-int/lit8 p1, p1, 0x4

    .line 12
    .line 13
    or-int/lit8 p1, p1, 0x2

    .line 14
    .line 15
    int-to-short p1, p1

    .line 16
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSheet(Lcom/intsig/office/fc/hslf/model/Sheet;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_sheet:Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public abstract Lcom/intsig/office/fc/hslf/model/SimpleShape;
.super Lcom/intsig/office/fc/hslf/model/Shape;
.source "SimpleShape.java"


# instance fields
.field protected _clientData:Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

.field protected _clientRecords:[Lcom/intsig/office/fc/hslf/record/Record;


# direct methods
.method protected constructor <init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/Shape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method protected createSpContainer(Z)Lcom/intsig/office/fc/ddf/EscherContainerRecord;
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 7
    .line 8
    const/16 v1, -0xffc

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 14
    .line 15
    const/16 v1, 0xf

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 18
    .line 19
    .line 20
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 21
    .line 22
    invoke-direct {v0}, Lcom/intsig/office/fc/ddf/EscherSpRecord;-><init>()V

    .line 23
    .line 24
    .line 25
    if-eqz p1, :cond_0

    .line 26
    .line 27
    const/16 v1, 0xa02

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    const/16 v1, 0xa00

    .line 31
    .line 32
    :goto_0
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setFlags(I)V

    .line 33
    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 36
    .line 37
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 38
    .line 39
    .line 40
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 41
    .line 42
    invoke-direct {v0}, Lcom/intsig/office/fc/ddf/EscherOptRecord;-><init>()V

    .line 43
    .line 44
    .line 45
    const/16 v1, -0xff5

    .line 46
    .line 47
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 48
    .line 49
    .line 50
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 51
    .line 52
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 53
    .line 54
    .line 55
    if-eqz p1, :cond_1

    .line 56
    .line 57
    new-instance p1, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;

    .line 58
    .line 59
    invoke-direct {p1}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;-><init>()V

    .line 60
    .line 61
    .line 62
    goto :goto_1

    .line 63
    :cond_1
    new-instance p1, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;

    .line 64
    .line 65
    invoke-direct {p1}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;-><init>()V

    .line 66
    .line 67
    .line 68
    const/16 v0, 0x10

    .line 69
    .line 70
    new-array v0, v0, [B

    .line 71
    .line 72
    const/4 v1, 0x0

    .line 73
    invoke-static {v0, v1, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    .line 74
    .line 75
    .line 76
    const/4 v2, 0x2

    .line 77
    invoke-static {v0, v2, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    .line 78
    .line 79
    .line 80
    const/4 v2, 0x4

    .line 81
    const/16 v3, 0x8

    .line 82
    .line 83
    invoke-static {v0, v2, v3}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 84
    .line 85
    .line 86
    const/4 v2, 0x0

    .line 87
    invoke-virtual {p1, v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherRecord;->fillFields([BILcom/intsig/office/fc/ddf/EscherRecordFactory;)I

    .line 88
    .line 89
    .line 90
    :goto_1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 91
    .line 92
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 93
    .line 94
    .line 95
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 96
    .line 97
    return-object p1
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public dispose()V
    .locals 5

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->dispose()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/SimpleShape;->_clientRecords:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    array-length v2, v0

    .line 10
    const/4 v3, 0x0

    .line 11
    :goto_0
    if-ge v3, v2, :cond_0

    .line 12
    .line 13
    aget-object v4, v0, v3

    .line 14
    .line 15
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/Record;->dispose()V

    .line 16
    .line 17
    .line 18
    add-int/lit8 v3, v3, 0x1

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/model/SimpleShape;->_clientRecords:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 22
    .line 23
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/SimpleShape;->_clientData:Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 24
    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;->dispose()V

    .line 28
    .line 29
    .line 30
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/model/SimpleShape;->_clientData:Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 31
    .line 32
    :cond_2
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected getClientDataRecord(I)Lcom/intsig/office/fc/hslf/record/Record;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getClientRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    array-length v2, v0

    .line 9
    if-ge v1, v2, :cond_1

    .line 10
    .line 11
    aget-object v2, v0, v1

    .line 12
    .line 13
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    .line 14
    .line 15
    .line 16
    move-result-wide v2

    .line 17
    int-to-long v4, p1

    .line 18
    cmp-long v6, v2, v4

    .line 19
    .line 20
    if-nez v6, :cond_0

    .line 21
    .line 22
    aget-object p1, v0, v1

    .line 23
    .line 24
    return-object p1

    .line 25
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    const/4 p1, 0x0

    .line 29
    return-object p1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected getClientRecords()[Lcom/intsig/office/fc/hslf/record/Record;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/SimpleShape;->_clientData:Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_1

    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const/16 v2, -0xfef

    .line 11
    .line 12
    invoke-static {v0, v2}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    instance-of v2, v0, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 19
    .line 20
    if-nez v2, :cond_0

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherRecord;->serialize()[B

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    new-instance v2, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 27
    .line 28
    invoke-direct {v2}, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;-><init>()V

    .line 29
    .line 30
    .line 31
    new-instance v3, Lcom/intsig/office/fc/ddf/DefaultEscherRecordFactory;

    .line 32
    .line 33
    invoke-direct {v3}, Lcom/intsig/office/fc/ddf/DefaultEscherRecordFactory;-><init>()V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v2, v0, v1, v3}, Lcom/intsig/office/fc/ddf/EscherRecord;->fillFields([BILcom/intsig/office/fc/ddf/EscherRecordFactory;)I

    .line 37
    .line 38
    .line 39
    move-object v0, v2

    .line 40
    :cond_0
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 41
    .line 42
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/SimpleShape;->_clientData:Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 43
    .line 44
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/SimpleShape;->_clientData:Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 45
    .line 46
    if-eqz v0, :cond_2

    .line 47
    .line 48
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/model/SimpleShape;->_clientRecords:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 49
    .line 50
    if-nez v2, :cond_2

    .line 51
    .line 52
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;->getRemainingData()[B

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    array-length v2, v0

    .line 57
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hslf/record/Record;->findChildRecords([BII)[Lcom/intsig/office/fc/hslf/record/Record;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/SimpleShape;->_clientRecords:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 62
    .line 63
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/SimpleShape;->_clientRecords:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 64
    .line 65
    return-object v0
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getLineDashing()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    const/16 v1, 0x1ce

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    const/4 v0, 0x0

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    :goto_0
    return v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getLineStyle()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    const/16 v1, 0x1cd

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    const/4 v0, 0x0

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    :goto_0
    return v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getLogicalAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 21

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v2, v0, Lcom/intsig/office/fc/hslf/model/Shape;->_parent:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 8
    .line 9
    if-eqz v2, :cond_0

    .line 10
    .line 11
    move-object v3, v2

    .line 12
    check-cast v3, Lcom/intsig/office/fc/hslf/model/ShapeGroup;

    .line 13
    .line 14
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->getClientAnchor2D(Lcom/intsig/office/fc/hslf/model/Shape;)Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    iget-object v3, v0, Lcom/intsig/office/fc/hslf/model/Shape;->_parent:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 19
    .line 20
    check-cast v3, Lcom/intsig/office/fc/hslf/model/ShapeGroup;

    .line 21
    .line 22
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->getCoordinates()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    invoke-virtual {v3}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 27
    .line 28
    .line 29
    move-result-wide v4

    .line 30
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 31
    .line 32
    .line 33
    move-result-wide v6

    .line 34
    div-double/2addr v4, v6

    .line 35
    invoke-virtual {v3}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 36
    .line 37
    .line 38
    move-result-wide v6

    .line 39
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 40
    .line 41
    .line 42
    move-result-wide v8

    .line 43
    div-double/2addr v6, v8

    .line 44
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 45
    .line 46
    .line 47
    move-result-wide v8

    .line 48
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 49
    .line 50
    .line 51
    move-result-wide v10

    .line 52
    invoke-virtual {v3}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 53
    .line 54
    .line 55
    move-result-wide v12

    .line 56
    sub-double/2addr v10, v12

    .line 57
    div-double/2addr v10, v4

    .line 58
    add-double v13, v8, v10

    .line 59
    .line 60
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 61
    .line 62
    .line 63
    move-result-wide v8

    .line 64
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 65
    .line 66
    .line 67
    move-result-wide v10

    .line 68
    invoke-virtual {v3}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 69
    .line 70
    .line 71
    move-result-wide v2

    .line 72
    sub-double/2addr v10, v2

    .line 73
    div-double/2addr v10, v6

    .line 74
    add-double v15, v8, v10

    .line 75
    .line 76
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 77
    .line 78
    .line 79
    move-result-wide v2

    .line 80
    div-double v17, v2, v4

    .line 81
    .line 82
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 83
    .line 84
    .line 85
    move-result-wide v1

    .line 86
    div-double v19, v1, v6

    .line 87
    .line 88
    new-instance v1, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;

    .line 89
    .line 90
    move-object v12, v1

    .line 91
    invoke-direct/range {v12 .. v20}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;-><init>(DDDD)V

    .line 92
    .line 93
    .line 94
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getRotation()I

    .line 95
    .line 96
    .line 97
    move-result v2

    .line 98
    if-eqz v2, :cond_3

    .line 99
    .line 100
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 101
    .line 102
    .line 103
    move-result-wide v3

    .line 104
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 105
    .line 106
    .line 107
    move-result-wide v5

    .line 108
    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    .line 109
    .line 110
    div-double/2addr v5, v7

    .line 111
    add-double/2addr v3, v5

    .line 112
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 113
    .line 114
    .line 115
    move-result-wide v5

    .line 116
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 117
    .line 118
    .line 119
    move-result-wide v9

    .line 120
    div-double/2addr v9, v7

    .line 121
    add-double/2addr v5, v9

    .line 122
    new-instance v7, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 123
    .line 124
    invoke-direct {v7}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    .line 125
    .line 126
    .line 127
    invoke-virtual {v7, v3, v4, v5, v6}, Lcom/intsig/office/java/awt/geom/AffineTransform;->translate(DD)V

    .line 128
    .line 129
    .line 130
    int-to-double v8, v2

    .line 131
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 132
    .line 133
    .line 134
    move-result-wide v8

    .line 135
    invoke-virtual {v7, v8, v9}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate(D)V

    .line 136
    .line 137
    .line 138
    neg-double v8, v3

    .line 139
    neg-double v10, v5

    .line 140
    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/intsig/office/java/awt/geom/AffineTransform;->translate(DD)V

    .line 141
    .line 142
    .line 143
    invoke-virtual {v7, v1}, Lcom/intsig/office/java/awt/geom/AffineTransform;->createTransformedShape(Lcom/intsig/office/java/awt/Shape;)Lcom/intsig/office/java/awt/Shape;

    .line 144
    .line 145
    .line 146
    move-result-object v2

    .line 147
    invoke-interface {v2}, Lcom/intsig/office/java/awt/Shape;->getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 148
    .line 149
    .line 150
    move-result-object v2

    .line 151
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 152
    .line 153
    .line 154
    move-result-wide v12

    .line 155
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 156
    .line 157
    .line 158
    move-result-wide v14

    .line 159
    cmpg-double v7, v12, v14

    .line 160
    .line 161
    if-gez v7, :cond_1

    .line 162
    .line 163
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 164
    .line 165
    .line 166
    move-result-wide v12

    .line 167
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 168
    .line 169
    .line 170
    move-result-wide v14

    .line 171
    cmpl-double v7, v12, v14

    .line 172
    .line 173
    if-gtz v7, :cond_2

    .line 174
    .line 175
    :cond_1
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 176
    .line 177
    .line 178
    move-result-wide v12

    .line 179
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 180
    .line 181
    .line 182
    move-result-wide v14

    .line 183
    cmpl-double v7, v12, v14

    .line 184
    .line 185
    if-lez v7, :cond_3

    .line 186
    .line 187
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 188
    .line 189
    .line 190
    move-result-wide v12

    .line 191
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 192
    .line 193
    .line 194
    move-result-wide v14

    .line 195
    cmpg-double v2, v12, v14

    .line 196
    .line 197
    if-gez v2, :cond_3

    .line 198
    .line 199
    :cond_2
    new-instance v2, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 200
    .line 201
    invoke-direct {v2}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    .line 202
    .line 203
    .line 204
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/intsig/office/java/awt/geom/AffineTransform;->translate(DD)V

    .line 205
    .line 206
    .line 207
    const-wide v3, 0x3ff921fb54442d18L    # 1.5707963267948966

    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    invoke-virtual {v2, v3, v4}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate(D)V

    .line 213
    .line 214
    .line 215
    invoke-virtual {v2, v8, v9, v10, v11}, Lcom/intsig/office/java/awt/geom/AffineTransform;->translate(DD)V

    .line 216
    .line 217
    .line 218
    invoke-virtual {v2, v1}, Lcom/intsig/office/java/awt/geom/AffineTransform;->createTransformedShape(Lcom/intsig/office/java/awt/Shape;)Lcom/intsig/office/java/awt/Shape;

    .line 219
    .line 220
    .line 221
    move-result-object v1

    .line 222
    invoke-interface {v1}, Lcom/intsig/office/java/awt/Shape;->getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 223
    .line 224
    .line 225
    move-result-object v1

    .line 226
    :cond_3
    return-object v1
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public setFillColor(Lcom/intsig/office/java/awt/Color;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getFill()Lcom/intsig/office/fc/hslf/model/Fill;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/model/Fill;->setForegroundColor(Lcom/intsig/office/java/awt/Color;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setHyperlink(Lcom/intsig/office/fc/hslf/model/Hyperlink;)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Hyperlink;->getId()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, -0x1

    .line 6
    if-eq v0, v1, :cond_5

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;-><init>()V

    .line 11
    .line 12
    .line 13
    const/16 v1, 0xf

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 23
    .line 24
    .line 25
    new-instance v0, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;

    .line 26
    .line 27
    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;-><init>()V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;->getInteractiveInfoAtom()Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Hyperlink;->getType()I

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    const/4 v2, 0x0

    .line 39
    const/4 v3, 0x1

    .line 40
    const/4 v4, 0x3

    .line 41
    if-eqz v1, :cond_4

    .line 42
    .line 43
    const/4 v5, 0x2

    .line 44
    if-eq v1, v3, :cond_3

    .line 45
    .line 46
    if-eq v1, v5, :cond_2

    .line 47
    .line 48
    const/4 v3, 0x4

    .line 49
    if-eq v1, v4, :cond_1

    .line 50
    .line 51
    const/16 v4, 0x8

    .line 52
    .line 53
    if-eq v1, v4, :cond_0

    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_0
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setAction(B)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setJump(B)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setHyperlinkType(B)V

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_1
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setAction(B)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setJump(B)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setHyperlinkType(B)V

    .line 73
    .line 74
    .line 75
    goto :goto_0

    .line 76
    :cond_2
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setAction(B)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setJump(B)V

    .line 80
    .line 81
    .line 82
    invoke-virtual {v0, v5}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setHyperlinkType(B)V

    .line 83
    .line 84
    .line 85
    goto :goto_0

    .line 86
    :cond_3
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setAction(B)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {v0, v5}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setJump(B)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setHyperlinkType(B)V

    .line 93
    .line 94
    .line 95
    goto :goto_0

    .line 96
    :cond_4
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setAction(B)V

    .line 97
    .line 98
    .line 99
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setJump(B)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setHyperlinkType(B)V

    .line 103
    .line 104
    .line 105
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Hyperlink;->getId()I

    .line 106
    .line 107
    .line 108
    move-result p1

    .line 109
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setHyperlinkID(I)V

    .line 110
    .line 111
    .line 112
    return-void

    .line 113
    :cond_5
    new-instance p1, Lcom/intsig/office/fc/hslf/exceptions/HSLFException;

    .line 114
    .line 115
    const-string v0, "You must call SlideShow.addHyperlink(Hyperlink link) first"

    .line 116
    .line 117
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hslf/exceptions/HSLFException;-><init>(Ljava/lang/String;)V

    .line 118
    .line 119
    .line 120
    throw p1
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public setLineColor(Lcom/intsig/office/java/awt/Color;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    const/16 v1, 0x1ff

    .line 12
    .line 13
    if-nez p1, :cond_0

    .line 14
    .line 15
    const/high16 p1, 0x80000

    .line 16
    .line 17
    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    new-instance v2, Lcom/intsig/office/java/awt/Color;

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getBlue()I

    .line 24
    .line 25
    .line 26
    move-result v3

    .line 27
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getGreen()I

    .line 28
    .line 29
    .line 30
    move-result v4

    .line 31
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getRed()I

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    const/4 v5, 0x0

    .line 36
    invoke-direct {v2, v3, v4, p1, v5}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    const/16 v2, 0x1c0

    .line 44
    .line 45
    invoke-static {v0, v2, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 46
    .line 47
    .line 48
    const p1, 0x180018

    .line 49
    .line 50
    .line 51
    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 52
    .line 53
    .line 54
    :goto_0
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setLineDashing(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    const/16 v1, 0x1ce

    .line 12
    .line 13
    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLineStyle(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    if-nez p1, :cond_0

    .line 12
    .line 13
    const/4 p1, -0x1

    .line 14
    :cond_0
    const/16 v1, 0x1cd

    .line 15
    .line 16
    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLineWidth(D)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    const-wide v1, 0x40c8ce0000000000L    # 12700.0

    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    mul-double p1, p1, v1

    .line 17
    .line 18
    double-to-int p1, p1

    .line 19
    const/16 p2, 0x1cb

    .line 20
    .line 21
    invoke-static {v0, p2, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 22
    .line 23
    .line 24
    return-void
.end method

.method public setRotation(I)V
    .locals 1

    .line 1
    shl-int/lit8 p1, p1, 0x10

    .line 2
    .line 3
    const/4 v0, 0x4

    .line 4
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected updateClientData()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/SimpleShape;->_clientData:Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/SimpleShape;->_clientRecords:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 6
    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 12
    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/model/SimpleShape;->_clientRecords:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 16
    .line 17
    array-length v2, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 18
    if-ge v1, v2, :cond_0

    .line 19
    .line 20
    add-int/lit8 v1, v1, 0x1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/model/SimpleShape;->_clientData:Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;->setRemainingData([B)V

    .line 30
    .line 31
    .line 32
    goto :goto_1

    .line 33
    :catch_0
    move-exception v0

    .line 34
    new-instance v1, Lcom/intsig/office/fc/hslf/exceptions/HSLFException;

    .line 35
    .line 36
    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    .line 37
    .line 38
    .line 39
    throw v1

    .line 40
    :cond_1
    :goto_1
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

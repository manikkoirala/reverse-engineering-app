.class public final Lcom/intsig/office/fc/hslf/model/Line;
.super Lcom/intsig/office/fc/hslf/model/SimpleShape;
.source "Line.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 4
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/model/Line;-><init>(Lcom/intsig/office/fc/hslf/model/Shape;)V

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/SimpleShape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/SimpleShape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    .line 3
    instance-of p1, p1, Lcom/intsig/office/fc/hslf/model/ShapeGroup;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/model/Line;->createSpContainer(Z)Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    return-void
.end method


# virtual methods
.method protected createSpContainer(Z)Lcom/intsig/office/fc/ddf/EscherContainerRecord;
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->createSpContainer(Z)Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 6
    .line 7
    const/16 v0, -0xff6

    .line 8
    .line 9
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    check-cast p1, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 14
    .line 15
    const/16 v0, 0x142

    .line 16
    .line 17
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 18
    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 21
    .line 22
    const/16 v0, -0xff5

    .line 23
    .line 24
    invoke-static {p1, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    check-cast p1, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 29
    .line 30
    const/16 v0, 0x144

    .line 31
    .line 32
    const/4 v1, 0x4

    .line 33
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 34
    .line 35
    .line 36
    const/16 v0, 0x17f

    .line 37
    .line 38
    const/high16 v1, 0x10000

    .line 39
    .line 40
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 41
    .line 42
    .line 43
    const/16 v0, 0x1bf

    .line 44
    .line 45
    const/high16 v1, 0x100000

    .line 46
    .line 47
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 48
    .line 49
    .line 50
    const/16 v0, 0x1c0

    .line 51
    .line 52
    const v1, 0x8000001

    .line 53
    .line 54
    .line 55
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 56
    .line 57
    .line 58
    const/16 v0, 0x1ff

    .line 59
    .line 60
    const v1, 0xa0008

    .line 61
    .line 62
    .line 63
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 64
    .line 65
    .line 66
    const/16 v0, 0x201

    .line 67
    .line 68
    const v1, 0x8000002

    .line 69
    .line 70
    .line 71
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 72
    .line 73
    .line 74
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 75
    .line 76
    return-object p1
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public dispose()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->dispose()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAdjustmentValue()[Ljava/lang/Float;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getAdjustmentValue(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)[Ljava/lang/Float;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOutline()Lcom/intsig/office/java/awt/Shape;
    .locals 11

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getLogicalAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v10, Lcom/intsig/office/java/awt/geom/Line2D$Double;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 8
    .line 9
    .line 10
    move-result-wide v2

    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 12
    .line 13
    .line 14
    move-result-wide v4

    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 16
    .line 17
    .line 18
    move-result-wide v6

    .line 19
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 20
    .line 21
    .line 22
    move-result-wide v8

    .line 23
    add-double/2addr v6, v8

    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 25
    .line 26
    .line 27
    move-result-wide v8

    .line 28
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 29
    .line 30
    .line 31
    move-result-wide v0

    .line 32
    add-double/2addr v8, v0

    .line 33
    move-object v1, v10

    .line 34
    invoke-direct/range {v1 .. v9}, Lcom/intsig/office/java/awt/geom/Line2D$Double;-><init>(DDDD)V

    .line 35
    .line 36
    .line 37
    return-object v10
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

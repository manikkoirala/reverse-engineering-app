.class public final Lcom/intsig/office/fc/hslf/model/TableCell;
.super Lcom/intsig/office/fc/hslf/model/TextBox;
.source "TableCell.java"


# static fields
.field protected static final DEFAULT_HEIGHT:I = 0x28

.field protected static final DEFAULT_WIDTH:I = 0x64


# instance fields
.field private borderBottom:Lcom/intsig/office/fc/hslf/model/Line;

.field private borderLeft:Lcom/intsig/office/fc/hslf/model/Line;

.field private borderRight:Lcom/intsig/office/fc/hslf/model/Line;

.field private borderTop:Lcom/intsig/office/fc/hslf/model/Line;


# direct methods
.method protected constructor <init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/TextBox;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hslf/model/TextBox;-><init>(Lcom/intsig/office/fc/hslf/model/Shape;)V

    const/4 p1, 0x1

    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setShapeType(I)V

    return-void
.end method


# virtual methods
.method protected anchorBorder(ILcom/intsig/office/fc/hslf/model/Line;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor()Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/office/java/awt/Rectangle;

    .line 6
    .line 7
    invoke-direct {v1}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    const/4 v3, 0x0

    .line 12
    if-eq p1, v2, :cond_3

    .line 13
    .line 14
    const/4 v2, 0x2

    .line 15
    if-eq p1, v2, :cond_2

    .line 16
    .line 17
    const/4 v2, 0x3

    .line 18
    if-eq p1, v2, :cond_1

    .line 19
    .line 20
    const/4 v2, 0x4

    .line 21
    if-ne p1, v2, :cond_0

    .line 22
    .line 23
    iget p1, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 24
    .line 25
    iput p1, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 26
    .line 27
    iget p1, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 28
    .line 29
    iput p1, v1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 30
    .line 31
    iput v3, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 32
    .line 33
    iget p1, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 34
    .line 35
    iput p1, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    .line 39
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    .line 41
    .line 42
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    .line 44
    .line 45
    const-string v1, "Unknown border type: "

    .line 46
    .line 47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    throw p2

    .line 61
    :cond_1
    iget p1, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 62
    .line 63
    iput p1, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 64
    .line 65
    iget p1, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 66
    .line 67
    iget v2, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 68
    .line 69
    add-int/2addr p1, v2

    .line 70
    iput p1, v1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 71
    .line 72
    iget p1, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 73
    .line 74
    iput p1, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 75
    .line 76
    iput v3, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_2
    iget p1, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 80
    .line 81
    iget v2, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 82
    .line 83
    add-int/2addr p1, v2

    .line 84
    iput p1, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 85
    .line 86
    iget p1, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 87
    .line 88
    iput p1, v1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 89
    .line 90
    iput v3, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 91
    .line 92
    iget p1, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 93
    .line 94
    iput p1, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 95
    .line 96
    goto :goto_0

    .line 97
    :cond_3
    iget p1, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 98
    .line 99
    iput p1, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 100
    .line 101
    iget p1, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 102
    .line 103
    iput p1, v1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 104
    .line 105
    iget p1, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 106
    .line 107
    iput p1, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 108
    .line 109
    iput v3, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 110
    .line 111
    :goto_0
    invoke-virtual {p2, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setAnchor(Lcom/intsig/office/java/awt/geom/Rectangle2D;)V

    .line 112
    .line 113
    .line 114
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method protected createSpContainer(Z)Lcom/intsig/office/fc/ddf/EscherContainerRecord;
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hslf/model/TextBox;->createSpContainer(Z)Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 6
    .line 7
    const/16 v0, -0xff5

    .line 8
    .line 9
    invoke-static {p1, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    check-cast p1, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 14
    .line 15
    const/16 v0, 0x80

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 19
    .line 20
    .line 21
    const/16 v0, 0xbf

    .line 22
    .line 23
    const/high16 v1, 0x20000

    .line 24
    .line 25
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 26
    .line 27
    .line 28
    const/16 v0, 0x1bf

    .line 29
    .line 30
    const v2, 0x150001

    .line 31
    .line 32
    .line 33
    invoke-static {p1, v0, v2}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 34
    .line 35
    .line 36
    const/16 v0, 0x23f

    .line 37
    .line 38
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 39
    .line 40
    .line 41
    const/16 v0, 0x7f

    .line 42
    .line 43
    const/high16 v1, 0x40000

    .line 44
    .line 45
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 46
    .line 47
    .line 48
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 49
    .line 50
    return-object p1
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getBorderBottom()Lcom/intsig/office/fc/hslf/model/Line;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/TableCell;->borderBottom:Lcom/intsig/office/fc/hslf/model/Line;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBorderLeft()Lcom/intsig/office/fc/hslf/model/Line;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/TableCell;->borderLeft:Lcom/intsig/office/fc/hslf/model/Line;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBorderRight()Lcom/intsig/office/fc/hslf/model/Line;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/TableCell;->borderRight:Lcom/intsig/office/fc/hslf/model/Line;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBorderTop()Lcom/intsig/office/fc/hslf/model/Line;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/TableCell;->borderTop:Lcom/intsig/office/fc/hslf/model/Line;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setAnchor(Lcom/intsig/office/java/awt/Rectangle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setAnchor(Lcom/intsig/office/java/awt/geom/Rectangle2D;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/model/TableCell;->borderTop:Lcom/intsig/office/fc/hslf/model/Line;

    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/TableCell;->anchorBorder(ILcom/intsig/office/fc/hslf/model/Line;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/model/TableCell;->borderRight:Lcom/intsig/office/fc/hslf/model/Line;

    .line 13
    .line 14
    if-eqz p1, :cond_1

    .line 15
    .line 16
    const/4 v0, 0x2

    .line 17
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/TableCell;->anchorBorder(ILcom/intsig/office/fc/hslf/model/Line;)V

    .line 18
    .line 19
    .line 20
    :cond_1
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/model/TableCell;->borderBottom:Lcom/intsig/office/fc/hslf/model/Line;

    .line 21
    .line 22
    if-eqz p1, :cond_2

    .line 23
    .line 24
    const/4 v0, 0x3

    .line 25
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/TableCell;->anchorBorder(ILcom/intsig/office/fc/hslf/model/Line;)V

    .line 26
    .line 27
    .line 28
    :cond_2
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/model/TableCell;->borderLeft:Lcom/intsig/office/fc/hslf/model/Line;

    .line 29
    .line 30
    if-eqz p1, :cond_3

    .line 31
    .line 32
    const/4 v0, 0x4

    .line 33
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/TableCell;->anchorBorder(ILcom/intsig/office/fc/hslf/model/Line;)V

    .line 34
    .line 35
    .line 36
    :cond_3
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setBorderBottom(Lcom/intsig/office/fc/hslf/model/Line;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const/4 v0, 0x3

    .line 4
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/TableCell;->anchorBorder(ILcom/intsig/office/fc/hslf/model/Line;)V

    .line 5
    .line 6
    .line 7
    :cond_0
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/TableCell;->borderBottom:Lcom/intsig/office/fc/hslf/model/Line;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBorderLeft(Lcom/intsig/office/fc/hslf/model/Line;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const/4 v0, 0x4

    .line 4
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/TableCell;->anchorBorder(ILcom/intsig/office/fc/hslf/model/Line;)V

    .line 5
    .line 6
    .line 7
    :cond_0
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/TableCell;->borderLeft:Lcom/intsig/office/fc/hslf/model/Line;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBorderRight(Lcom/intsig/office/fc/hslf/model/Line;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const/4 v0, 0x2

    .line 4
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/TableCell;->anchorBorder(ILcom/intsig/office/fc/hslf/model/Line;)V

    .line 5
    .line 6
    .line 7
    :cond_0
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/TableCell;->borderRight:Lcom/intsig/office/fc/hslf/model/Line;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBorderTop(Lcom/intsig/office/fc/hslf/model/Line;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const/4 v0, 0x1

    .line 4
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/TableCell;->anchorBorder(ILcom/intsig/office/fc/hslf/model/Line;)V

    .line 5
    .line 6
    .line 7
    :cond_0
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/TableCell;->borderTop:Lcom/intsig/office/fc/hslf/model/Line;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

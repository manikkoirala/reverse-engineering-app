.class public Lcom/intsig/office/fc/hslf/record/ExOleObjStg;
.super Lcom/intsig/office/fc/hslf/record/RecordAtom;
.source "ExOleObjStg.java"

# interfaces
.implements Lcom/intsig/office/fc/hslf/record/PositionDependentRecord;
.implements Lcom/intsig/office/fc/hslf/record/PersistRecord;


# instance fields
.field private _data:[B

.field private _header:[B

.field private _persistId:I

.field protected myLastOnDiskOffset:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordAtom;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [B

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->_header:[B

    const/4 v1, 0x0

    new-array v2, v1, [B

    .line 3
    iput-object v2, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->_data:[B

    const/16 v2, 0x10

    .line 4
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->_header:[B

    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->getRecordType()J

    move-result-wide v1

    long-to-int v2, v1

    int-to-short v1, v2

    const/4 v2, 0x2

    invoke-static {v0, v2, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->_header:[B

    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->_data:[B

    array-length v1, v1

    const/4 v2, 0x4

    invoke-static {v0, v2, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 0

    .line 7
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordAtom;-><init>()V

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->_header:[B

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->_data:[B

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getData()Ljava/io/InputStream;
    .locals 4

    .line 1
    new-instance v0, Ljava/io/ByteArrayInputStream;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->_data:[B

    .line 4
    .line 5
    const/4 v2, 0x4

    .line 6
    array-length v3, v1

    .line 7
    invoke-direct {v0, v1, v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 8
    .line 9
    .line 10
    new-instance v1, Ljava/util/zip/InflaterInputStream;

    .line 11
    .line 12
    invoke-direct {v1, v0}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 13
    .line 14
    .line 15
    return-object v1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDataLength()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->_data:[B

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLastOnDiskOffset()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->myLastOnDiskOffset:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPersistId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->_persistId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRawData()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->_data:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExOleObjStg:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 4
    .line 5
    int-to-long v0, v0

    .line 6
    return-wide v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setData([B)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x4

    .line 7
    new-array v2, v1, [B

    .line 8
    .line 9
    array-length v3, p1

    .line 10
    invoke-static {v2, v3}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BI)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v2}, Ljava/io/OutputStream;->write([B)V

    .line 14
    .line 15
    .line 16
    new-instance v2, Ljava/util/zip/DeflaterOutputStream;

    .line 17
    .line 18
    invoke-direct {v2, v0}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 19
    .line 20
    .line 21
    const/4 v3, 0x0

    .line 22
    array-length v4, p1

    .line 23
    invoke-virtual {v2, p1, v3, v4}, Ljava/util/zip/DeflaterOutputStream;->write([BII)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v2}, Ljava/util/zip/DeflaterOutputStream;->finish()V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->_data:[B

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->_header:[B

    .line 36
    .line 37
    array-length p1, p1

    .line 38
    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setLastOnDiskOffset(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->myLastOnDiskOffset:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPersistId(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;->_persistId:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public updateOtherRecordReferences(Ljava/util/Hashtable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

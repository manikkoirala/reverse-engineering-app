.class public final Lcom/intsig/office/fc/hslf/EncryptedSlideShow;
.super Ljava/lang/Object;
.source "EncryptedSlideShow.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static checkIfEncrypted(Lcom/intsig/office/fc/hslf/HSLFSlideShow;)Z
    .locals 0

    .line 1
    const/4 p0, 0x0

    .line 2
    return p0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static fetchDocumentEncryptionAtom(Lcom/intsig/office/fc/hslf/HSLFSlideShow;)Lcom/intsig/office/fc/hslf/record/DocumentEncryptionAtom;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->getCurrentUserAtom()Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->getCurrentEditOffset()J

    .line 6
    .line 7
    .line 8
    move-result-wide v1

    .line 9
    const-wide/16 v3, 0x0

    .line 10
    .line 11
    const/4 v5, 0x0

    .line 12
    cmp-long v6, v1, v3

    .line 13
    .line 14
    if-eqz v6, :cond_7

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->getCurrentEditOffset()J

    .line 17
    .line 18
    .line 19
    move-result-wide v1

    .line 20
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->getUnderlyingBytes()[B

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    array-length v3, v3

    .line 25
    int-to-long v3, v3

    .line 26
    cmp-long v6, v1, v3

    .line 27
    .line 28
    if-gtz v6, :cond_6

    .line 29
    .line 30
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->getUnderlyingBytes()[B

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->getCurrentEditOffset()J

    .line 35
    .line 36
    .line 37
    move-result-wide v2

    .line 38
    long-to-int v0, v2

    .line 39
    invoke-static {v1, v0}, Lcom/intsig/office/fc/hslf/record/Record;->buildRecordAtOffset([BI)Lcom/intsig/office/fc/hslf/record/Record;

    .line 40
    .line 41
    .line 42
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    if-nez v0, :cond_0

    .line 44
    .line 45
    return-object v5

    .line 46
    :cond_0
    instance-of v1, v0, Lcom/intsig/office/fc/hslf/record/UserEditAtom;

    .line 47
    .line 48
    if-nez v1, :cond_1

    .line 49
    .line 50
    return-object v5

    .line 51
    :cond_1
    check-cast v0, Lcom/intsig/office/fc/hslf/record/UserEditAtom;

    .line 52
    .line 53
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->getUnderlyingBytes()[B

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/UserEditAtom;->getPersistPointersOffset()I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    invoke-static {v1, v0}, Lcom/intsig/office/fc/hslf/record/Record;->buildRecordAtOffset([BI)Lcom/intsig/office/fc/hslf/record/Record;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    instance-of v1, v0, Lcom/intsig/office/fc/hslf/record/PersistPtrHolder;

    .line 66
    .line 67
    if-nez v1, :cond_2

    .line 68
    .line 69
    return-object v5

    .line 70
    :cond_2
    check-cast v0, Lcom/intsig/office/fc/hslf/record/PersistPtrHolder;

    .line 71
    .line 72
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/PersistPtrHolder;->getKnownSlideIDs()[I

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    const/4 v2, -0x1

    .line 77
    const/4 v3, 0x0

    .line 78
    const/4 v4, -0x1

    .line 79
    :goto_0
    array-length v6, v1

    .line 80
    if-ge v3, v6, :cond_4

    .line 81
    .line 82
    aget v6, v1, v3

    .line 83
    .line 84
    if-le v6, v4, :cond_3

    .line 85
    .line 86
    move v4, v6

    .line 87
    :cond_3
    add-int/lit8 v3, v3, 0x1

    .line 88
    .line 89
    goto :goto_0

    .line 90
    :cond_4
    if-ne v4, v2, :cond_5

    .line 91
    .line 92
    return-object v5

    .line 93
    :cond_5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/PersistPtrHolder;->getSlideLocationsLookup()Ljava/util/Hashtable;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 98
    .line 99
    .line 100
    move-result-object v1

    .line 101
    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    check-cast v0, Ljava/lang/Integer;

    .line 106
    .line 107
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 108
    .line 109
    .line 110
    move-result v0

    .line 111
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/HSLFSlideShow;->getUnderlyingBytes()[B

    .line 112
    .line 113
    .line 114
    move-result-object p0

    .line 115
    invoke-static {p0, v0}, Lcom/intsig/office/fc/hslf/record/Record;->buildRecordAtOffset([BI)Lcom/intsig/office/fc/hslf/record/Record;

    .line 116
    .line 117
    .line 118
    move-result-object p0

    .line 119
    instance-of v0, p0, Lcom/intsig/office/fc/hslf/record/DocumentEncryptionAtom;

    .line 120
    .line 121
    if-eqz v0, :cond_7

    .line 122
    .line 123
    check-cast p0, Lcom/intsig/office/fc/hslf/record/DocumentEncryptionAtom;

    .line 124
    .line 125
    return-object p0

    .line 126
    :catch_0
    return-object v5

    .line 127
    :cond_6
    new-instance p0, Lcom/intsig/office/fc/hslf/exceptions/CorruptPowerPointFileException;

    .line 128
    .line 129
    const-string v0, "The CurrentUserAtom claims that the offset of last edit details are past the end of the file"

    .line 130
    .line 131
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    throw p0

    .line 135
    :cond_7
    return-object v5
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.class public final Lcom/intsig/office/fc/hslf/model/AutoShapes;
.super Ljava/lang/Object;
.source "AutoShapes.java"


# static fields
.field protected static shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    const/16 v0, 0xff

    .line 2
    .line 3
    new-array v0, v0, [Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 4
    .line 5
    sput-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 6
    .line 7
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$1;

    .line 8
    .line 9
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$1;-><init>()V

    .line 10
    .line 11
    .line 12
    const/4 v2, 0x1

    .line 13
    aput-object v1, v0, v2

    .line 14
    .line 15
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 16
    .line 17
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$2;

    .line 18
    .line 19
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$2;-><init>()V

    .line 20
    .line 21
    .line 22
    const/4 v2, 0x2

    .line 23
    aput-object v1, v0, v2

    .line 24
    .line 25
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 26
    .line 27
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$3;

    .line 28
    .line 29
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$3;-><init>()V

    .line 30
    .line 31
    .line 32
    const/4 v2, 0x3

    .line 33
    aput-object v1, v0, v2

    .line 34
    .line 35
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 36
    .line 37
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$4;

    .line 38
    .line 39
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$4;-><init>()V

    .line 40
    .line 41
    .line 42
    const/4 v2, 0x4

    .line 43
    aput-object v1, v0, v2

    .line 44
    .line 45
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 46
    .line 47
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$5;

    .line 48
    .line 49
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$5;-><init>()V

    .line 50
    .line 51
    .line 52
    const/16 v2, 0xcb

    .line 53
    .line 54
    aput-object v1, v0, v2

    .line 55
    .line 56
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 57
    .line 58
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$6;

    .line 59
    .line 60
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$6;-><init>()V

    .line 61
    .line 62
    .line 63
    const/16 v2, 0xcc

    .line 64
    .line 65
    aput-object v1, v0, v2

    .line 66
    .line 67
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 68
    .line 69
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$7;

    .line 70
    .line 71
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$7;-><init>()V

    .line 72
    .line 73
    .line 74
    const/4 v2, 0x7

    .line 75
    aput-object v1, v0, v2

    .line 76
    .line 77
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 78
    .line 79
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$8;

    .line 80
    .line 81
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$8;-><init>()V

    .line 82
    .line 83
    .line 84
    const/16 v2, 0x8

    .line 85
    .line 86
    aput-object v1, v0, v2

    .line 87
    .line 88
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 89
    .line 90
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$9;

    .line 91
    .line 92
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$9;-><init>()V

    .line 93
    .line 94
    .line 95
    const/16 v2, 0x9

    .line 96
    .line 97
    aput-object v1, v0, v2

    .line 98
    .line 99
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 100
    .line 101
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$10;

    .line 102
    .line 103
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$10;-><init>()V

    .line 104
    .line 105
    .line 106
    const/16 v2, 0xa

    .line 107
    .line 108
    aput-object v1, v0, v2

    .line 109
    .line 110
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 111
    .line 112
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$11;

    .line 113
    .line 114
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$11;-><init>()V

    .line 115
    .line 116
    .line 117
    const/16 v2, 0xb

    .line 118
    .line 119
    aput-object v1, v0, v2

    .line 120
    .line 121
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 122
    .line 123
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$12;

    .line 124
    .line 125
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$12;-><init>()V

    .line 126
    .line 127
    .line 128
    const/16 v2, 0x38

    .line 129
    .line 130
    aput-object v1, v0, v2

    .line 131
    .line 132
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 133
    .line 134
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$13;

    .line 135
    .line 136
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$13;-><init>()V

    .line 137
    .line 138
    .line 139
    const/16 v2, 0x43

    .line 140
    .line 141
    aput-object v1, v0, v2

    .line 142
    .line 143
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 144
    .line 145
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$14;

    .line 146
    .line 147
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$14;-><init>()V

    .line 148
    .line 149
    .line 150
    const/16 v2, 0x44

    .line 151
    .line 152
    aput-object v1, v0, v2

    .line 153
    .line 154
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 155
    .line 156
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$15;

    .line 157
    .line 158
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$15;-><init>()V

    .line 159
    .line 160
    .line 161
    const/16 v2, 0xcd

    .line 162
    .line 163
    aput-object v1, v0, v2

    .line 164
    .line 165
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 166
    .line 167
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$16;

    .line 168
    .line 169
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$16;-><init>()V

    .line 170
    .line 171
    .line 172
    const/16 v2, 0x42

    .line 173
    .line 174
    aput-object v1, v0, v2

    .line 175
    .line 176
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 177
    .line 178
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$17;

    .line 179
    .line 180
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$17;-><init>()V

    .line 181
    .line 182
    .line 183
    const/16 v2, 0x16

    .line 184
    .line 185
    aput-object v1, v0, v2

    .line 186
    .line 187
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 188
    .line 189
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$18;

    .line 190
    .line 191
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$18;-><init>()V

    .line 192
    .line 193
    .line 194
    const/16 v2, 0x57

    .line 195
    .line 196
    aput-object v1, v0, v2

    .line 197
    .line 198
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 199
    .line 200
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$19;

    .line 201
    .line 202
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$19;-><init>()V

    .line 203
    .line 204
    .line 205
    const/16 v2, 0x58

    .line 206
    .line 207
    aput-object v1, v0, v2

    .line 208
    .line 209
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 210
    .line 211
    new-instance v1, Lcom/intsig/office/fc/hslf/model/AutoShapes$20;

    .line 212
    .line 213
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes$20;-><init>()V

    .line 214
    .line 215
    .line 216
    const/16 v2, 0x20

    .line 217
    .line 218
    aput-object v1, v0, v2

    .line 219
    .line 220
    return-void
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static getShapeOutline(I)Lcom/intsig/office/fc/hslf/model/ShapeOutline;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hslf/model/AutoShapes;->shapes:[Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 2
    .line 3
    aget-object p0, v0, p0

    .line 4
    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static transform(Lcom/intsig/office/java/awt/Shape;Lcom/intsig/office/java/awt/geom/Rectangle2D;)Lcom/intsig/office/java/awt/Shape;
    .locals 7

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 7
    .line 8
    .line 9
    move-result-wide v1

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 11
    .line 12
    .line 13
    move-result-wide v3

    .line 14
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/intsig/office/java/awt/geom/AffineTransform;->translate(DD)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 18
    .line 19
    .line 20
    move-result-wide v1

    .line 21
    const-wide v3, 0x3f0845c8a0000000L    # 4.6296296204673126E-5

    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    mul-double v1, v1, v3

    .line 27
    .line 28
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 29
    .line 30
    .line 31
    move-result-wide v5

    .line 32
    mul-double v5, v5, v3

    .line 33
    .line 34
    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/intsig/office/java/awt/geom/AffineTransform;->scale(DD)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->createTransformedShape(Lcom/intsig/office/java/awt/Shape;)Lcom/intsig/office/java/awt/Shape;

    .line 38
    .line 39
    .line 40
    move-result-object p0

    .line 41
    return-object p0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.class public final Lcom/intsig/office/fc/hslf/model/Fill;
.super Ljava/lang/Object;
.source "Fill.java"


# instance fields
.field protected shape:Lcom/intsig/office/fc/hslf/model/Shape;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFillAngle()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getFillAngle(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFillFocus()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getFillFocus(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFillType()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getFillType(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFillbackColor()Lcom/intsig/office/java/awt/Color;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/Shape;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/4 v2, 0x2

    .line 14
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ShapeKit;->getFillbackColor(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/lang/Object;I)Lcom/intsig/office/java/awt/Color;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
.end method

.method public getForegroundColor()Lcom/intsig/office/java/awt/Color;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/Shape;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/4 v2, 0x2

    .line 14
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ShapeKit;->getForegroundColor(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/lang/Object;I)Lcom/intsig/office/java/awt/Color;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
.end method

.method public getPictureData()Lcom/intsig/office/fc/hslf/usermodel/PictureData;
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, -0xff5

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 14
    .line 15
    const/16 v1, 0x186

    .line 16
    .line 17
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const/4 v1, 0x0

    .line 22
    if-eqz v0, :cond_3

    .line 23
    .line 24
    instance-of v2, v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 25
    .line 26
    if-nez v2, :cond_0

    .line 27
    .line 28
    goto :goto_1

    .line 29
    :cond_0
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 30
    .line 31
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 32
    .line 33
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Shape;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSlideShow()Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getPictureData()[Lcom/intsig/office/fc/hslf/usermodel/PictureData;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getDocumentRecord()Lcom/intsig/office/fc/hslf/record/Document;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/record/Document;->getPPDrawingGroup()Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;->getDggContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    const/16 v4, -0xfff

    .line 58
    .line 59
    invoke-static {v2, v4}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 64
    .line 65
    if-eqz v2, :cond_3

    .line 66
    .line 67
    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    .line 68
    .line 69
    .line 70
    move-result-object v2

    .line 71
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    const v4, 0xffff

    .line 76
    .line 77
    .line 78
    and-int/2addr v0, v4

    .line 79
    if-nez v0, :cond_1

    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_1
    add-int/lit8 v0, v0, -0x1

    .line 83
    .line 84
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherBSERecord;

    .line 89
    .line 90
    const/4 v2, 0x0

    .line 91
    :goto_0
    array-length v4, v3

    .line 92
    if-ge v2, v4, :cond_3

    .line 93
    .line 94
    aget-object v4, v3, v2

    .line 95
    .line 96
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->getOffset()I

    .line 97
    .line 98
    .line 99
    move-result v4

    .line 100
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->getOffset()I

    .line 101
    .line 102
    .line 103
    move-result v5

    .line 104
    if-ne v4, v5, :cond_2

    .line 105
    .line 106
    aget-object v0, v3, v2

    .line 107
    .line 108
    return-object v0

    .line 109
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 110
    .line 111
    goto :goto_0

    .line 112
    :cond_3
    :goto_1
    return-object v1
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public getRadialGradientPositionType()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getRadialGradientPositionType(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShaderColors()[I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getShaderColors(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)[I

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShaderPositions()[F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getShaderPositions(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)[F

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isShaderPreset()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->isShaderPreset(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setBackgroundColor(Lcom/intsig/office/java/awt/Color;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, -0xff5

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 14
    .line 15
    const/16 v1, 0x183

    .line 16
    .line 17
    if-nez p1, :cond_0

    .line 18
    .line 19
    const/4 p1, -0x1

    .line 20
    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    new-instance v2, Lcom/intsig/office/java/awt/Color;

    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getBlue()I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getGreen()I

    .line 31
    .line 32
    .line 33
    move-result v4

    .line 34
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getRed()I

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    const/4 v5, 0x0

    .line 39
    invoke-direct {v2, v3, v4, p1, v5}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 47
    .line 48
    .line 49
    :goto_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setFillType(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, -0xff5

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 14
    .line 15
    const/16 v1, 0x180

    .line 16
    .line 17
    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setForegroundColor(Lcom/intsig/office/java/awt/Color;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, -0xff5

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 14
    .line 15
    const/16 v1, 0x1bf

    .line 16
    .line 17
    if-nez p1, :cond_0

    .line 18
    .line 19
    const/high16 p1, 0x150000

    .line 20
    .line 21
    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    new-instance v2, Lcom/intsig/office/java/awt/Color;

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getBlue()I

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getGreen()I

    .line 32
    .line 33
    .line 34
    move-result v4

    .line 35
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getRed()I

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    const/4 v5, 0x0

    .line 40
    invoke-direct {v2, v3, v4, p1, v5}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    const/16 v2, 0x181

    .line 48
    .line 49
    invoke-static {v0, v2, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 50
    .line 51
    .line 52
    const p1, 0x150011

    .line 53
    .line 54
    .line 55
    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 56
    .line 57
    .line 58
    :goto_0
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setPictureData(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Fill;->shape:Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, -0xff5

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 14
    .line 15
    const/16 v1, 0x4186

    .line 16
    .line 17
    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public final Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;
.super Lcom/intsig/office/fc/hslf/record/RecordContainer;
.source "EscherTextboxWrapper.java"


# instance fields
.field private _escherRecord:Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

.field private _type:J

.field private shapeId:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;-><init>()V

    .line 7
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    invoke-direct {v0}, Lcom/intsig/office/fc/ddf/EscherTextboxRecord;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->_escherRecord:Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    const/16 v1, -0xff3

    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 9
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->_escherRecord:Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/intsig/office/fc/hslf/record/Record;

    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/ddf/EscherTextboxRecord;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->_escherRecord:Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    move-result p1

    int-to-long v0, p1

    iput-wide v0, p0, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->_type:J

    .line 4
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->_escherRecord:Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    invoke-virtual {p1}, Lcom/intsig/office/fc/ddf/EscherTextboxRecord;->getData()[B

    move-result-object p1

    .line 5
    array-length v0, p1

    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Lcom/intsig/office/fc/hslf/record/Record;->findChildRecords([BII)[Lcom/intsig/office/fc/hslf/record/Record;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->dispose()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->_escherRecord:Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherTextboxRecord;->dispose()V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->_escherRecord:Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEscherRecord()Lcom/intsig/office/fc/ddf/EscherTextboxRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->_escherRecord:Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShapeId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->shapeId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setShapeId(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->shapeId:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

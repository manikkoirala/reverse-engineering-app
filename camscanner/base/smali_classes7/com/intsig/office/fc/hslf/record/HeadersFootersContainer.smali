.class public final Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;
.super Lcom/intsig/office/fc/hslf/record/RecordContainer;
.source "HeadersFootersContainer.java"


# static fields
.field public static final FOOTERATOM:I = 0x2

.field public static final HEADERATOM:I = 0x1

.field public static final NotesHeadersFootersContainer:S = 0x4fs

.field public static final SlideHeadersFootersContainer:S = 0x3fs

.field public static final USERDATEATOM:I


# instance fields
.field private _header:[B

.field private csDate:Lcom/intsig/office/fc/hslf/record/CString;

.field private csFooter:Lcom/intsig/office/fc/hslf/record/CString;

.field private csHeader:Lcom/intsig/office/fc/hslf/record/CString;

.field private hdAtom:Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;


# direct methods
.method public constructor <init>(S)V
    .locals 4

    .line 15
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [B

    .line 16
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->_header:[B

    const/4 v1, 0x0

    .line 17
    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 18
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->_header:[B

    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->getRecordType()J

    move-result-wide v2

    long-to-int v0, v2

    int-to-short v0, v0

    const/4 v2, 0x2

    invoke-static {p1, v2, v0}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 19
    new-instance p1, Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;

    invoke-direct {p1}, Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->hdAtom:Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/intsig/office/fc/hslf/record/Record;

    aput-object p1, v0, v1

    .line 20
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    const/4 p1, 0x0

    .line 21
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csFooter:Lcom/intsig/office/fc/hslf/record/CString;

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csHeader:Lcom/intsig/office/fc/hslf/record/CString;

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csDate:Lcom/intsig/office/fc/hslf/record/CString;

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;-><init>()V

    const/16 v0, 0x8

    new-array v1, v0, [B

    .line 2
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->_header:[B

    const/4 v2, 0x0

    .line 3
    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, v0

    sub-int/2addr p3, v0

    .line 4
    invoke-static {p1, p2, p3}, Lcom/intsig/office/fc/hslf/record/Record;->findChildRecords([BII)[Lcom/intsig/office/fc/hslf/record/Record;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 5
    :goto_0
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    array-length p2, p1

    if-ge v2, p2, :cond_5

    .line 6
    aget-object p1, p1, v2

    instance-of p2, p1, Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;

    if-eqz p2, :cond_0

    check-cast p1, Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->hdAtom:Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;

    goto :goto_1

    .line 7
    :cond_0
    instance-of p2, p1, Lcom/intsig/office/fc/hslf/record/CString;

    if-eqz p2, :cond_4

    .line 8
    check-cast p1, Lcom/intsig/office/fc/hslf/record/CString;

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/record/CString;->getOptions()I

    move-result p2

    shr-int/lit8 p2, p2, 0x4

    if-eqz p2, :cond_3

    const/4 p3, 0x1

    if-eq p2, p3, :cond_2

    const/4 p3, 0x2

    if-eq p2, p3, :cond_1

    .line 10
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/record/Record;->logger:Lcom/intsig/office/fc/util/POILogger;

    sget p3, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected CString.Options in HeadersFootersContainer: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p3, p2}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_1

    .line 11
    :cond_1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csFooter:Lcom/intsig/office/fc/hslf/record/CString;

    goto :goto_1

    .line 12
    :cond_2
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csHeader:Lcom/intsig/office/fc/hslf/record/CString;

    goto :goto_1

    .line 13
    :cond_3
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csDate:Lcom/intsig/office/fc/hslf/record/CString;

    goto :goto_1

    .line 14
    :cond_4
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/record/Record;->logger:Lcom/intsig/office/fc/util/POILogger;

    sget p2, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unexpected record in HeadersFootersContainer: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    aget-object v0, v0, v2

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    return-void
.end method


# virtual methods
.method public addFooterAtom()Lcom/intsig/office/fc/hslf/record/CString;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csFooter:Lcom/intsig/office/fc/hslf/record/CString;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hslf/record/CString;

    .line 7
    .line 8
    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/record/CString;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csFooter:Lcom/intsig/office/fc/hslf/record/CString;

    .line 12
    .line 13
    const/16 v1, 0x20

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/record/CString;->setOptions(I)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->hdAtom:Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csHeader:Lcom/intsig/office/fc/hslf/record/CString;

    .line 21
    .line 22
    if-eqz v1, :cond_1

    .line 23
    .line 24
    :goto_0
    move-object v0, v1

    .line 25
    goto :goto_1

    .line 26
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csDate:Lcom/intsig/office/fc/hslf/record/CString;

    .line 27
    .line 28
    if-eqz v1, :cond_2

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csFooter:Lcom/intsig/office/fc/hslf/record/CString;

    .line 32
    .line 33
    invoke-virtual {p0, v1, v0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->addChildAfter(Lcom/intsig/office/fc/hslf/record/Record;Lcom/intsig/office/fc/hslf/record/Record;)V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csFooter:Lcom/intsig/office/fc/hslf/record/CString;

    .line 37
    .line 38
    return-object v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public addHeaderAtom()Lcom/intsig/office/fc/hslf/record/CString;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csHeader:Lcom/intsig/office/fc/hslf/record/CString;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hslf/record/CString;

    .line 7
    .line 8
    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/record/CString;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csHeader:Lcom/intsig/office/fc/hslf/record/CString;

    .line 12
    .line 13
    const/16 v1, 0x10

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/record/CString;->setOptions(I)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->hdAtom:Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csHeader:Lcom/intsig/office/fc/hslf/record/CString;

    .line 21
    .line 22
    invoke-virtual {p0, v1, v0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->addChildAfter(Lcom/intsig/office/fc/hslf/record/Record;Lcom/intsig/office/fc/hslf/record/Record;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csHeader:Lcom/intsig/office/fc/hslf/record/CString;

    .line 26
    .line 27
    return-object v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public addUserDateAtom()Lcom/intsig/office/fc/hslf/record/CString;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csDate:Lcom/intsig/office/fc/hslf/record/CString;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hslf/record/CString;

    .line 7
    .line 8
    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/record/CString;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csDate:Lcom/intsig/office/fc/hslf/record/CString;

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/record/CString;->setOptions(I)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csDate:Lcom/intsig/office/fc/hslf/record/CString;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->hdAtom:Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;

    .line 20
    .line 21
    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->addChildAfter(Lcom/intsig/office/fc/hslf/record/Record;Lcom/intsig/office/fc/hslf/record/Record;)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csDate:Lcom/intsig/office/fc/hslf/record/CString;

    .line 25
    .line 26
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->_header:[B

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->hdAtom:Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;->dispose()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->hdAtom:Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;

    .line 12
    .line 13
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csDate:Lcom/intsig/office/fc/hslf/record/CString;

    .line 14
    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/CString;->dispose()V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csDate:Lcom/intsig/office/fc/hslf/record/CString;

    .line 21
    .line 22
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csHeader:Lcom/intsig/office/fc/hslf/record/CString;

    .line 23
    .line 24
    if-eqz v1, :cond_2

    .line 25
    .line 26
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/CString;->dispose()V

    .line 27
    .line 28
    .line 29
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csHeader:Lcom/intsig/office/fc/hslf/record/CString;

    .line 30
    .line 31
    :cond_2
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csFooter:Lcom/intsig/office/fc/hslf/record/CString;

    .line 32
    .line 33
    if-eqz v1, :cond_3

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/CString;->dispose()V

    .line 36
    .line 37
    .line 38
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csFooter:Lcom/intsig/office/fc/hslf/record/CString;

    .line 39
    .line 40
    :cond_3
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getFooterAtom()Lcom/intsig/office/fc/hslf/record/CString;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csFooter:Lcom/intsig/office/fc/hslf/record/CString;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHeaderAtom()Lcom/intsig/office/fc/hslf/record/CString;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csHeader:Lcom/intsig/office/fc/hslf/record/CString;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHeadersFootersAtom()Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->hdAtom:Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOptions()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->_header:[B

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->HeadersFooters:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 4
    .line 5
    int-to-long v0, v0

    .line 6
    return-wide v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getUserDateAtom()Lcom/intsig/office/fc/hslf/record/CString;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;->csDate:Lcom/intsig/office/fc/hslf/record/CString;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;
.super Lcom/intsig/office/fc/hslf/record/RecordAtom;
.source "ExtendedParagraphAtom.java"


# static fields
.field private static _type:J = 0xfacL

.field public static extendedParagraphPropTypes:[Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;


# instance fields
.field private _header:[B

.field private autoNumberList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v1, v0, [Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 3
    .line 4
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 5
    .line 6
    const/high16 v3, 0x1000000

    .line 7
    .line 8
    const-string v4, "NumberingType"

    .line 9
    .line 10
    invoke-direct {v2, v0, v3, v4}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const/4 v3, 0x0

    .line 14
    aput-object v2, v1, v3

    .line 15
    .line 16
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 17
    .line 18
    const/high16 v3, 0x800000

    .line 19
    .line 20
    const-string v4, "Start"

    .line 21
    .line 22
    invoke-direct {v2, v0, v3, v4}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 v0, 0x1

    .line 26
    aput-object v2, v1, v0

    .line 27
    .line 28
    sput-object v1, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;->extendedParagraphPropTypes:[Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected constructor <init>()V
    .locals 1

    .line 16
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordAtom;-><init>()V

    .line 17
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;->autoNumberList:Ljava/util/LinkedList;

    return-void
.end method

.method public constructor <init>([BII)V
    .locals 10

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordAtom;-><init>()V

    .line 2
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;->autoNumberList:Ljava/util/LinkedList;

    const/16 v0, 0x8

    if-ge p3, v0, :cond_0

    const/16 p3, 0x8

    :cond_0
    new-array v1, v0, [B

    .line 3
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;->_header:[B

    const/4 v2, 0x0

    .line 4
    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v1, p2, 0x8

    :goto_0
    add-int v3, p2, p3

    if-ge v1, v3, :cond_8

    const/16 v3, 0x1c

    if-lt p3, v3, :cond_8

    sub-int v3, p3, v1

    const/4 v4, 0x4

    if-ge v3, v4, :cond_1

    goto/16 :goto_4

    .line 5
    :cond_1
    new-instance v3, Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;

    invoke-direct {v3}, Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;-><init>()V

    .line 6
    invoke-static {p1, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result v4

    const/high16 v5, 0x3000000

    if-ne v4, v5, :cond_2

    shr-int/lit8 v4, v4, 0x1

    :cond_2
    add-int/lit8 v1, v1, 0x4

    if-eqz v4, :cond_7

    const/high16 v5, 0x1800000

    if-ne v4, v5, :cond_3

    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    :cond_3
    add-int/lit8 v1, v1, 0x4

    :goto_1
    const/4 v6, 0x0

    .line 7
    :goto_2
    sget-object v7, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;->extendedParagraphPropTypes:[Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    array-length v8, v7

    if-ge v6, v8, :cond_6

    .line 8
    aget-object v7, v7, v6

    invoke-virtual {v7}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->getMask()I

    move-result v7

    and-int/2addr v7, v4

    if-eqz v7, :cond_6

    .line 9
    invoke-static {p1, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v7

    .line 10
    sget-object v8, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;->extendedParagraphPropTypes:[Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    aget-object v8, v8, v6

    invoke-virtual {v8}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "NumberingType"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 11
    invoke-virtual {v3, v7}, Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;->setNumberingType(I)V

    goto :goto_3

    .line 12
    :cond_4
    sget-object v8, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;->extendedParagraphPropTypes:[Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    aget-object v8, v8, v6

    invoke-virtual {v8}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Start"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 13
    invoke-virtual {v3, v7}, Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;->setStart(I)V

    .line 14
    :cond_5
    :goto_3
    sget-object v7, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;->extendedParagraphPropTypes:[Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    aget-object v7, v7, v6

    invoke-virtual {v7}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->getSize()I

    move-result v7

    add-int/2addr v1, v7

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_6
    if-ne v4, v5, :cond_7

    add-int/lit8 v1, v1, 0x2

    .line 15
    :cond_7
    iget-object v4, p0, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;->autoNumberList:Ljava/util/LinkedList;

    invoke-virtual {v4, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    add-int/2addr v1, v0

    goto :goto_0

    :cond_8
    :goto_4
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;->_header:[B

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;->autoNumberList:Ljava/util/LinkedList;

    .line 5
    .line 6
    if-eqz v1, :cond_1

    .line 7
    .line 8
    invoke-virtual {v1}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-eqz v2, :cond_0

    .line 17
    .line 18
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    check-cast v2, Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;

    .line 23
    .line 24
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;->dispose()V

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;->autoNumberList:Ljava/util/LinkedList;

    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 31
    .line 32
    .line 33
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;->autoNumberList:Ljava/util/LinkedList;

    .line 34
    .line 35
    :cond_1
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getExtendedParagraphPropList()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;->autoNumberList:Ljava/util/LinkedList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

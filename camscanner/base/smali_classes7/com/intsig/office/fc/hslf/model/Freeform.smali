.class public final Lcom/intsig/office/fc/hslf/model/Freeform;
.super Lcom/intsig/office/fc/hslf/model/AutoShape;
.source "Freeform.java"


# static fields
.field public static final SEGMENTINFO_CLOSE:[B

.field public static final SEGMENTINFO_CUBICTO:[B

.field public static final SEGMENTINFO_CUBICTO1:[B

.field public static final SEGMENTINFO_CUBICTO2:[B

.field public static final SEGMENTINFO_END:[B

.field public static final SEGMENTINFO_ESCAPE:[B

.field public static final SEGMENTINFO_ESCAPE1:[B

.field public static final SEGMENTINFO_ESCAPE2:[B

.field public static final SEGMENTINFO_LINETO:[B

.field public static final SEGMENTINFO_LINETO2:[B

.field public static final SEGMENTINFO_MOVETO:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v1, v0, [B

    .line 3
    .line 4
    fill-array-data v1, :array_0

    .line 5
    .line 6
    .line 7
    sput-object v1, Lcom/intsig/office/fc/hslf/model/Freeform;->SEGMENTINFO_MOVETO:[B

    .line 8
    .line 9
    new-array v1, v0, [B

    .line 10
    .line 11
    fill-array-data v1, :array_1

    .line 12
    .line 13
    .line 14
    sput-object v1, Lcom/intsig/office/fc/hslf/model/Freeform;->SEGMENTINFO_LINETO:[B

    .line 15
    .line 16
    new-array v1, v0, [B

    .line 17
    .line 18
    fill-array-data v1, :array_2

    .line 19
    .line 20
    .line 21
    sput-object v1, Lcom/intsig/office/fc/hslf/model/Freeform;->SEGMENTINFO_LINETO2:[B

    .line 22
    .line 23
    new-array v1, v0, [B

    .line 24
    .line 25
    fill-array-data v1, :array_3

    .line 26
    .line 27
    .line 28
    sput-object v1, Lcom/intsig/office/fc/hslf/model/Freeform;->SEGMENTINFO_ESCAPE:[B

    .line 29
    .line 30
    new-array v1, v0, [B

    .line 31
    .line 32
    fill-array-data v1, :array_4

    .line 33
    .line 34
    .line 35
    sput-object v1, Lcom/intsig/office/fc/hslf/model/Freeform;->SEGMENTINFO_ESCAPE1:[B

    .line 36
    .line 37
    new-array v1, v0, [B

    .line 38
    .line 39
    fill-array-data v1, :array_5

    .line 40
    .line 41
    .line 42
    sput-object v1, Lcom/intsig/office/fc/hslf/model/Freeform;->SEGMENTINFO_ESCAPE2:[B

    .line 43
    .line 44
    new-array v1, v0, [B

    .line 45
    .line 46
    fill-array-data v1, :array_6

    .line 47
    .line 48
    .line 49
    sput-object v1, Lcom/intsig/office/fc/hslf/model/Freeform;->SEGMENTINFO_CUBICTO:[B

    .line 50
    .line 51
    new-array v1, v0, [B

    .line 52
    .line 53
    fill-array-data v1, :array_7

    .line 54
    .line 55
    .line 56
    sput-object v1, Lcom/intsig/office/fc/hslf/model/Freeform;->SEGMENTINFO_CUBICTO1:[B

    .line 57
    .line 58
    new-array v1, v0, [B

    .line 59
    .line 60
    fill-array-data v1, :array_8

    .line 61
    .line 62
    .line 63
    sput-object v1, Lcom/intsig/office/fc/hslf/model/Freeform;->SEGMENTINFO_CUBICTO2:[B

    .line 64
    .line 65
    new-array v1, v0, [B

    .line 66
    .line 67
    fill-array-data v1, :array_9

    .line 68
    .line 69
    .line 70
    sput-object v1, Lcom/intsig/office/fc/hslf/model/Freeform;->SEGMENTINFO_CLOSE:[B

    .line 71
    .line 72
    new-array v0, v0, [B

    .line 73
    .line 74
    fill-array-data v0, :array_a

    .line 75
    .line 76
    .line 77
    sput-object v0, Lcom/intsig/office/fc/hslf/model/Freeform;->SEGMENTINFO_END:[B

    .line 78
    .line 79
    return-void

    .line 80
    nop

    .line 81
    :array_0
    .array-data 1
        0x0t
        0x40t
    .end array-data

    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    nop

    .line 87
    :array_1
    .array-data 1
        0x0t
        -0x54t
    .end array-data

    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    nop

    .line 93
    :array_2
    .array-data 1
        0x0t
        -0x50t
    .end array-data

    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    nop

    .line 99
    :array_3
    .array-data 1
        0x1t
        0x0t
    .end array-data

    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    nop

    .line 105
    :array_4
    .array-data 1
        0x3t
        0x0t
    .end array-data

    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    nop

    .line 111
    :array_5
    .array-data 1
        0x1t
        0x20t
    .end array-data

    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    nop

    .line 117
    :array_6
    .array-data 1
        0x0t
        -0x53t
    .end array-data

    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    nop

    .line 123
    :array_7
    .array-data 1
        0x0t
        -0x51t
    .end array-data

    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    nop

    .line 129
    :array_8
    .array-data 1
        0x0t
        -0x4dt
    .end array-data

    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    nop

    .line 135
    :array_9
    .array-data 1
        0x1t
        0x60t
    .end array-data

    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    nop

    .line 141
    :array_a
    .array-data 1
        0x0t
        -0x80t
    .end array-data
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 4
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/model/Freeform;-><init>(Lcom/intsig/office/fc/hslf/model/Shape;)V

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/AutoShape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/AutoShape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    const/4 v0, 0x0

    .line 3
    instance-of p1, p1, Lcom/intsig/office/fc/hslf/model/ShapeGroup;

    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/AutoShape;->createSpContainer(IZ)Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/model/AutoShape;->dispose()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEndArrowPathAndTail(Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/intsig/office/fc/ShapeKit;->getEndArrowPathAndTail(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getFreeformPath(Lcom/intsig/office/java/awt/Rectangle;Landroid/graphics/PointF;BLandroid/graphics/PointF;B)[Landroid/graphics/Path;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    move-object v1, p1

    .line 6
    move-object v2, p2

    .line 7
    move v3, p3

    .line 8
    move-object v4, p4

    .line 9
    move v5, p5

    .line 10
    invoke-static/range {v0 .. v5}, Lcom/intsig/office/fc/ShapeKit;->getFreeformPath(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/java/awt/Rectangle;Landroid/graphics/PointF;BLandroid/graphics/PointF;B)[Landroid/graphics/Path;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method public getOutline()Lcom/intsig/office/java/awt/Shape;
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Freeform;->getPath()Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    new-instance v3, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 14
    .line 15
    invoke-direct {v3}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 19
    .line 20
    .line 21
    move-result-wide v4

    .line 22
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 23
    .line 24
    .line 25
    move-result-wide v6

    .line 26
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/intsig/office/java/awt/geom/AffineTransform;->translate(DD)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 30
    .line 31
    .line 32
    move-result-wide v4

    .line 33
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 34
    .line 35
    .line 36
    move-result-wide v6

    .line 37
    div-double/2addr v4, v6

    .line 38
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 39
    .line 40
    .line 41
    move-result-wide v6

    .line 42
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 43
    .line 44
    .line 45
    move-result-wide v1

    .line 46
    div-double/2addr v6, v1

    .line 47
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/intsig/office/java/awt/geom/AffineTransform;->scale(DD)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v3, v0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->createTransformedShape(Lcom/intsig/office/java/awt/Shape;)Lcom/intsig/office/java/awt/Shape;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    return-object v0
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getPath()Lcom/intsig/office/java/awt/geom/GeneralPath;
    .locals 19

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget-object v1, v0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    const/16 v2, -0xff5

    .line 6
    .line 7
    invoke-static {v1, v2}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 12
    .line 13
    new-instance v2, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 14
    .line 15
    const/16 v3, 0x144

    .line 16
    .line 17
    const/4 v4, 0x4

    .line 18
    invoke-direct {v2, v3, v4}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 22
    .line 23
    .line 24
    const/16 v2, 0x4145

    .line 25
    .line 26
    invoke-static {v1, v2}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 31
    .line 32
    if-nez v2, :cond_0

    .line 33
    .line 34
    const/16 v2, 0x145

    .line 35
    .line 36
    invoke-static {v1, v2}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 41
    .line 42
    :cond_0
    const/16 v3, 0x4146

    .line 43
    .line 44
    invoke-static {v1, v3}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 49
    .line 50
    if-nez v3, :cond_1

    .line 51
    .line 52
    const/16 v3, 0x146

    .line 53
    .line 54
    invoke-static {v1, v3}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    move-object v3, v1

    .line 59
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 60
    .line 61
    :cond_1
    const/4 v1, 0x0

    .line 62
    if-nez v2, :cond_2

    .line 63
    .line 64
    return-object v1

    .line 65
    :cond_2
    if-nez v3, :cond_3

    .line 66
    .line 67
    return-object v1

    .line 68
    :cond_3
    new-instance v1, Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 69
    .line 70
    invoke-direct {v1}, Lcom/intsig/office/java/awt/geom/GeneralPath;-><init>()V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    .line 74
    .line 75
    .line 76
    move-result v11

    .line 77
    invoke-virtual {v3}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    .line 78
    .line 79
    .line 80
    move-result v12

    .line 81
    const/4 v13, 0x0

    .line 82
    const/4 v4, 0x0

    .line 83
    const/4 v5, 0x0

    .line 84
    :goto_0
    if-ge v4, v12, :cond_9

    .line 85
    .line 86
    if-ge v5, v11, :cond_9

    .line 87
    .line 88
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 89
    .line 90
    .line 91
    move-result-object v6

    .line 92
    sget-object v7, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_MOVETO:[B

    .line 93
    .line 94
    invoke-static {v6, v7}, Ljava/util/Arrays;->equals([B[B)Z

    .line 95
    .line 96
    .line 97
    move-result v7

    .line 98
    const/4 v8, 0x2

    .line 99
    const/high16 v9, 0x44100000    # 576.0f

    .line 100
    .line 101
    const/high16 v10, 0x42900000    # 72.0f

    .line 102
    .line 103
    if-eqz v7, :cond_4

    .line 104
    .line 105
    add-int/lit8 v6, v5, 0x1

    .line 106
    .line 107
    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 108
    .line 109
    .line 110
    move-result-object v5

    .line 111
    invoke-static {v5, v13}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 112
    .line 113
    .line 114
    move-result v7

    .line 115
    invoke-static {v5, v8}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 116
    .line 117
    .line 118
    move-result v5

    .line 119
    int-to-float v7, v7

    .line 120
    mul-float v7, v7, v10

    .line 121
    .line 122
    div-float/2addr v7, v9

    .line 123
    int-to-float v5, v5

    .line 124
    mul-float v5, v5, v10

    .line 125
    .line 126
    div-float/2addr v5, v9

    .line 127
    invoke-virtual {v1, v7, v5}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->moveTo(FF)V

    .line 128
    .line 129
    .line 130
    :goto_1
    move v5, v6

    .line 131
    goto/16 :goto_3

    .line 132
    .line 133
    :cond_4
    sget-object v7, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO:[B

    .line 134
    .line 135
    invoke-static {v6, v7}, Ljava/util/Arrays;->equals([B[B)Z

    .line 136
    .line 137
    .line 138
    move-result v7

    .line 139
    if-nez v7, :cond_7

    .line 140
    .line 141
    sget-object v7, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO2:[B

    .line 142
    .line 143
    invoke-static {v6, v7}, Ljava/util/Arrays;->equals([B[B)Z

    .line 144
    .line 145
    .line 146
    move-result v7

    .line 147
    if-eqz v7, :cond_5

    .line 148
    .line 149
    goto :goto_2

    .line 150
    :cond_5
    sget-object v7, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO:[B

    .line 151
    .line 152
    invoke-static {v6, v7}, Ljava/util/Arrays;->equals([B[B)Z

    .line 153
    .line 154
    .line 155
    move-result v6

    .line 156
    if-eqz v6, :cond_8

    .line 157
    .line 158
    add-int/lit8 v4, v4, 0x1

    .line 159
    .line 160
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 161
    .line 162
    .line 163
    move-result-object v6

    .line 164
    sget-object v7, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE:[B

    .line 165
    .line 166
    invoke-static {v6, v7}, Ljava/util/Arrays;->equals([B[B)Z

    .line 167
    .line 168
    .line 169
    move-result v7

    .line 170
    if-eqz v7, :cond_6

    .line 171
    .line 172
    add-int/lit8 v6, v5, 0x1

    .line 173
    .line 174
    if-ge v6, v11, :cond_8

    .line 175
    .line 176
    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 177
    .line 178
    .line 179
    move-result-object v5

    .line 180
    invoke-static {v5, v13}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 181
    .line 182
    .line 183
    move-result v7

    .line 184
    invoke-static {v5, v8}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 185
    .line 186
    .line 187
    move-result v5

    .line 188
    int-to-float v7, v7

    .line 189
    mul-float v7, v7, v10

    .line 190
    .line 191
    div-float/2addr v7, v9

    .line 192
    int-to-float v5, v5

    .line 193
    mul-float v5, v5, v10

    .line 194
    .line 195
    div-float/2addr v5, v9

    .line 196
    invoke-virtual {v1, v7, v5}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->lineTo(FF)V

    .line 197
    .line 198
    .line 199
    goto :goto_1

    .line 200
    :cond_6
    sget-object v7, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CLOSE:[B

    .line 201
    .line 202
    invoke-static {v6, v7}, Ljava/util/Arrays;->equals([B[B)Z

    .line 203
    .line 204
    .line 205
    move-result v6

    .line 206
    if-eqz v6, :cond_8

    .line 207
    .line 208
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/Path2D;->closePath()V

    .line 209
    .line 210
    .line 211
    goto :goto_3

    .line 212
    :cond_7
    :goto_2
    add-int/lit8 v14, v4, 0x1

    .line 213
    .line 214
    add-int/lit8 v4, v5, 0x1

    .line 215
    .line 216
    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 217
    .line 218
    .line 219
    move-result-object v5

    .line 220
    invoke-static {v5, v13}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 221
    .line 222
    .line 223
    move-result v6

    .line 224
    invoke-static {v5, v8}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 225
    .line 226
    .line 227
    move-result v5

    .line 228
    add-int/lit8 v7, v4, 0x1

    .line 229
    .line 230
    invoke-virtual {v2, v4}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 231
    .line 232
    .line 233
    move-result-object v4

    .line 234
    invoke-static {v4, v13}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 235
    .line 236
    .line 237
    move-result v15

    .line 238
    invoke-static {v4, v8}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 239
    .line 240
    .line 241
    move-result v4

    .line 242
    add-int/lit8 v16, v7, 0x1

    .line 243
    .line 244
    invoke-virtual {v2, v7}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->getElement(I)[B

    .line 245
    .line 246
    .line 247
    move-result-object v7

    .line 248
    invoke-static {v7, v13}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 249
    .line 250
    .line 251
    move-result v9

    .line 252
    invoke-static {v7, v8}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 253
    .line 254
    .line 255
    move-result v7

    .line 256
    int-to-float v6, v6

    .line 257
    mul-float v6, v6, v10

    .line 258
    .line 259
    const/high16 v8, 0x44100000    # 576.0f

    .line 260
    .line 261
    div-float/2addr v6, v8

    .line 262
    int-to-float v5, v5

    .line 263
    mul-float v5, v5, v10

    .line 264
    .line 265
    div-float v17, v5, v8

    .line 266
    .line 267
    int-to-float v5, v15

    .line 268
    mul-float v5, v5, v10

    .line 269
    .line 270
    div-float v15, v5, v8

    .line 271
    .line 272
    int-to-float v4, v4

    .line 273
    mul-float v4, v4, v10

    .line 274
    .line 275
    div-float v18, v4, v8

    .line 276
    .line 277
    int-to-float v4, v9

    .line 278
    mul-float v4, v4, v10

    .line 279
    .line 280
    div-float v9, v4, v8

    .line 281
    .line 282
    int-to-float v4, v7

    .line 283
    mul-float v4, v4, v10

    .line 284
    .line 285
    div-float v10, v4, v8

    .line 286
    .line 287
    move-object v4, v1

    .line 288
    move v5, v6

    .line 289
    move/from16 v6, v17

    .line 290
    .line 291
    move v7, v15

    .line 292
    move/from16 v8, v18

    .line 293
    .line 294
    invoke-virtual/range {v4 .. v10}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->curveTo(FFFFFF)V

    .line 295
    .line 296
    .line 297
    move v4, v14

    .line 298
    move/from16 v5, v16

    .line 299
    .line 300
    :cond_8
    :goto_3
    add-int/lit8 v4, v4, 0x1

    .line 301
    .line 302
    goto/16 :goto_0

    .line 303
    .line 304
    :cond_9
    return-object v1
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public getStartArrowPathAndTail(Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/intsig/office/fc/ShapeKit;->getStartArrowPathAndTail(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPath(Lcom/intsig/office/java/awt/geom/GeneralPath;)V
    .locals 20

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    new-instance v2, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 8
    .line 9
    invoke-direct {v2}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    .line 10
    .line 11
    .line 12
    move-object/from16 v3, p1

    .line 13
    .line 14
    invoke-virtual {v3, v2}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/geom/PathIterator;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    new-instance v3, Ljava/util/ArrayList;

    .line 19
    .line 20
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 21
    .line 22
    .line 23
    new-instance v4, Ljava/util/ArrayList;

    .line 24
    .line 25
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 26
    .line 27
    .line 28
    const/4 v5, 0x0

    .line 29
    const/4 v6, 0x0

    .line 30
    :goto_0
    invoke-interface {v2}, Lcom/intsig/office/java/awt/geom/PathIterator;->isDone()Z

    .line 31
    .line 32
    .line 33
    move-result v7

    .line 34
    const/4 v8, 0x2

    .line 35
    const/4 v9, 0x4

    .line 36
    if-nez v7, :cond_4

    .line 37
    .line 38
    const/4 v7, 0x6

    .line 39
    new-array v7, v7, [D

    .line 40
    .line 41
    invoke-interface {v2, v7}, Lcom/intsig/office/java/awt/geom/PathIterator;->currentSegment([D)I

    .line 42
    .line 43
    .line 44
    move-result v10

    .line 45
    const/4 v11, 0x1

    .line 46
    if-eqz v10, :cond_3

    .line 47
    .line 48
    if-eq v10, v11, :cond_2

    .line 49
    .line 50
    const/4 v12, 0x3

    .line 51
    if-eq v10, v12, :cond_1

    .line 52
    .line 53
    if-eq v10, v9, :cond_0

    .line 54
    .line 55
    move v15, v6

    .line 56
    goto/16 :goto_1

    .line 57
    .line 58
    :cond_0
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    move-result-object v6

    .line 62
    check-cast v6, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    .line 63
    .line 64
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    .line 66
    .line 67
    sget-object v6, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO:[B

    .line 68
    .line 69
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    .line 71
    .line 72
    sget-object v7, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE:[B

    .line 73
    .line 74
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    .line 76
    .line 77
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    sget-object v6, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CLOSE:[B

    .line 81
    .line 82
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    .line 84
    .line 85
    const/4 v6, 0x1

    .line 86
    goto :goto_2

    .line 87
    :cond_1
    new-instance v10, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    .line 88
    .line 89
    aget-wide v13, v7, v5

    .line 90
    .line 91
    move v15, v6

    .line 92
    aget-wide v5, v7, v11

    .line 93
    .line 94
    invoke-direct {v10, v13, v14, v5, v6}, Lcom/intsig/office/java/awt/geom/Point2D$Double;-><init>(DD)V

    .line 95
    .line 96
    .line 97
    invoke-interface {v4, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    .line 99
    .line 100
    new-instance v5, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    .line 101
    .line 102
    aget-wide v10, v7, v8

    .line 103
    .line 104
    aget-wide v12, v7, v12

    .line 105
    .line 106
    invoke-direct {v5, v10, v11, v12, v13}, Lcom/intsig/office/java/awt/geom/Point2D$Double;-><init>(DD)V

    .line 107
    .line 108
    .line 109
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    .line 111
    .line 112
    new-instance v5, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    .line 113
    .line 114
    aget-wide v8, v7, v9

    .line 115
    .line 116
    const/4 v6, 0x5

    .line 117
    aget-wide v6, v7, v6

    .line 118
    .line 119
    invoke-direct {v5, v8, v9, v6, v7}, Lcom/intsig/office/java/awt/geom/Point2D$Double;-><init>(DD)V

    .line 120
    .line 121
    .line 122
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    .line 124
    .line 125
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_CUBICTO:[B

    .line 126
    .line 127
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    .line 129
    .line 130
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE2:[B

    .line 131
    .line 132
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    .line 134
    .line 135
    goto :goto_1

    .line 136
    :cond_2
    move v15, v6

    .line 137
    new-instance v5, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    .line 138
    .line 139
    const/4 v6, 0x0

    .line 140
    aget-wide v8, v7, v6

    .line 141
    .line 142
    aget-wide v10, v7, v11

    .line 143
    .line 144
    invoke-direct {v5, v8, v9, v10, v11}, Lcom/intsig/office/java/awt/geom/Point2D$Double;-><init>(DD)V

    .line 145
    .line 146
    .line 147
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    .line 149
    .line 150
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO:[B

    .line 151
    .line 152
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    .line 154
    .line 155
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_ESCAPE:[B

    .line 156
    .line 157
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    .line 159
    .line 160
    goto :goto_1

    .line 161
    :cond_3
    move v15, v6

    .line 162
    const/4 v6, 0x0

    .line 163
    new-instance v5, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    .line 164
    .line 165
    aget-wide v8, v7, v6

    .line 166
    .line 167
    aget-wide v6, v7, v11

    .line 168
    .line 169
    invoke-direct {v5, v8, v9, v6, v7}, Lcom/intsig/office/java/awt/geom/Point2D$Double;-><init>(DD)V

    .line 170
    .line 171
    .line 172
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    .line 174
    .line 175
    sget-object v5, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_MOVETO:[B

    .line 176
    .line 177
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    .line 179
    .line 180
    :goto_1
    move v6, v15

    .line 181
    :goto_2
    invoke-interface {v2}, Lcom/intsig/office/java/awt/geom/PathIterator;->next()V

    .line 182
    .line 183
    .line 184
    const/4 v5, 0x0

    .line 185
    goto/16 :goto_0

    .line 186
    .line 187
    :cond_4
    move v15, v6

    .line 188
    if-nez v15, :cond_5

    .line 189
    .line 190
    sget-object v2, Lcom/intsig/office/fc/ShapeKit;->SEGMENTINFO_LINETO:[B

    .line 191
    .line 192
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    .line 194
    .line 195
    :cond_5
    new-array v2, v8, [B

    .line 196
    .line 197
    fill-array-data v2, :array_0

    .line 198
    .line 199
    .line 200
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    .line 202
    .line 203
    iget-object v2, v0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 204
    .line 205
    const/16 v5, -0xff5

    .line 206
    .line 207
    invoke-static {v2, v5}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 208
    .line 209
    .line 210
    move-result-object v2

    .line 211
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 212
    .line 213
    new-instance v5, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 214
    .line 215
    const/16 v6, 0x144

    .line 216
    .line 217
    invoke-direct {v5, v6, v9}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 218
    .line 219
    .line 220
    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 221
    .line 222
    .line 223
    new-instance v5, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 224
    .line 225
    const/16 v6, 0x4145

    .line 226
    .line 227
    const/4 v7, 0x0

    .line 228
    const/4 v10, 0x0

    .line 229
    invoke-direct {v5, v6, v10, v7}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;-><init>(SZ[B)V

    .line 230
    .line 231
    .line 232
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 233
    .line 234
    .line 235
    move-result v6

    .line 236
    invoke-virtual {v5, v6}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->setNumberOfElementsInArray(I)V

    .line 237
    .line 238
    .line 239
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 240
    .line 241
    .line 242
    move-result v6

    .line 243
    invoke-virtual {v5, v6}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->setNumberOfElementsInMemory(I)V

    .line 244
    .line 245
    .line 246
    const v6, 0xfff0

    .line 247
    .line 248
    .line 249
    invoke-virtual {v5, v6}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->setSizeOfElements(I)V

    .line 250
    .line 251
    .line 252
    const/4 v6, 0x0

    .line 253
    :goto_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 254
    .line 255
    .line 256
    move-result v10

    .line 257
    const-wide/high16 v11, 0x4052000000000000L    # 72.0

    .line 258
    .line 259
    const-wide/high16 v13, 0x4082000000000000L    # 576.0

    .line 260
    .line 261
    if-ge v6, v10, :cond_6

    .line 262
    .line 263
    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 264
    .line 265
    .line 266
    move-result-object v10

    .line 267
    check-cast v10, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    .line 268
    .line 269
    new-array v15, v9, [B

    .line 270
    .line 271
    invoke-virtual {v10}, Lcom/intsig/office/java/awt/geom/Point2D$Double;->getX()D

    .line 272
    .line 273
    .line 274
    move-result-wide v16

    .line 275
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 276
    .line 277
    .line 278
    move-result-wide v18

    .line 279
    sub-double v16, v16, v18

    .line 280
    .line 281
    mul-double v16, v16, v13

    .line 282
    .line 283
    div-double v7, v16, v11

    .line 284
    .line 285
    double-to-int v7, v7

    .line 286
    int-to-short v7, v7

    .line 287
    const/4 v8, 0x0

    .line 288
    invoke-static {v15, v8, v7}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 289
    .line 290
    .line 291
    invoke-virtual {v10}, Lcom/intsig/office/java/awt/geom/Point2D$Double;->getY()D

    .line 292
    .line 293
    .line 294
    move-result-wide v7

    .line 295
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 296
    .line 297
    .line 298
    move-result-wide v16

    .line 299
    sub-double v7, v7, v16

    .line 300
    .line 301
    mul-double v7, v7, v13

    .line 302
    .line 303
    div-double/2addr v7, v11

    .line 304
    double-to-int v7, v7

    .line 305
    int-to-short v7, v7

    .line 306
    const/4 v8, 0x2

    .line 307
    invoke-static {v15, v8, v7}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 308
    .line 309
    .line 310
    invoke-virtual {v5, v6, v15}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->setElement(I[B)V

    .line 311
    .line 312
    .line 313
    add-int/lit8 v6, v6, 0x1

    .line 314
    .line 315
    const/4 v7, 0x0

    .line 316
    const/4 v8, 0x2

    .line 317
    goto :goto_3

    .line 318
    :cond_6
    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 319
    .line 320
    .line 321
    new-instance v4, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 322
    .line 323
    const/16 v5, 0x4146

    .line 324
    .line 325
    const/4 v6, 0x0

    .line 326
    const/4 v7, 0x0

    .line 327
    invoke-direct {v4, v5, v7, v6}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;-><init>(SZ[B)V

    .line 328
    .line 329
    .line 330
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 331
    .line 332
    .line 333
    move-result v5

    .line 334
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->setNumberOfElementsInArray(I)V

    .line 335
    .line 336
    .line 337
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 338
    .line 339
    .line 340
    move-result v5

    .line 341
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->setNumberOfElementsInMemory(I)V

    .line 342
    .line 343
    .line 344
    const/4 v5, 0x2

    .line 345
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->setSizeOfElements(I)V

    .line 346
    .line 347
    .line 348
    const/4 v5, 0x0

    .line 349
    :goto_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 350
    .line 351
    .line 352
    move-result v6

    .line 353
    if-ge v5, v6, :cond_7

    .line 354
    .line 355
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 356
    .line 357
    .line 358
    move-result-object v6

    .line 359
    check-cast v6, [B

    .line 360
    .line 361
    invoke-virtual {v4, v5, v6}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->setElement(I[B)V

    .line 362
    .line 363
    .line 364
    add-int/lit8 v5, v5, 0x1

    .line 365
    .line 366
    goto :goto_4

    .line 367
    :cond_7
    invoke-virtual {v2, v4}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 368
    .line 369
    .line 370
    new-instance v3, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 371
    .line 372
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 373
    .line 374
    .line 375
    move-result-wide v4

    .line 376
    mul-double v4, v4, v13

    .line 377
    .line 378
    div-double/2addr v4, v11

    .line 379
    double-to-int v4, v4

    .line 380
    const/16 v5, 0x142

    .line 381
    .line 382
    invoke-direct {v3, v5, v4}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 383
    .line 384
    .line 385
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 386
    .line 387
    .line 388
    new-instance v3, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 389
    .line 390
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 391
    .line 392
    .line 393
    move-result-wide v4

    .line 394
    mul-double v4, v4, v13

    .line 395
    .line 396
    div-double/2addr v4, v11

    .line 397
    double-to-int v4, v4

    .line 398
    const/16 v5, 0x143

    .line 399
    .line 400
    invoke-direct {v3, v5, v4}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 401
    .line 402
    .line 403
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 404
    .line 405
    .line 406
    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->sortProperties()V

    .line 407
    .line 408
    .line 409
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setAnchor(Lcom/intsig/office/java/awt/geom/Rectangle2D;)V

    .line 410
    .line 411
    .line 412
    return-void

    .line 413
    :array_0
    .array-data 1
        0x0t
        -0x80t
    .end array-data
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

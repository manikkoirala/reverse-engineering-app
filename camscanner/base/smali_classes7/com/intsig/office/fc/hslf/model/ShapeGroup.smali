.class public Lcom/intsig/office/fc/hslf/model/ShapeGroup;
.super Lcom/intsig/office/fc/hslf/model/Shape;
.source "ShapeGroup.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0, v0}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->createSpContainer(Z)Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/Shape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    return-void
.end method


# virtual methods
.method public addShape(Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hslf/model/Shape;->setSheet(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Sheet;->allocateShapeId()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setShapeId(I)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hslf/model/Shape;->afterInsert(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected createSpContainer(Z)Lcom/intsig/office/fc/ddf/EscherContainerRecord;
    .locals 3

    .line 1
    new-instance p1, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-direct {p1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v0, -0xffd

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 9
    .line 10
    .line 11
    const/16 v0, 0xf

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 14
    .line 15
    .line 16
    new-instance v1, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 17
    .line 18
    invoke-direct {v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;-><init>()V

    .line 19
    .line 20
    .line 21
    const/16 v2, -0xffc

    .line 22
    .line 23
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 27
    .line 28
    .line 29
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;

    .line 30
    .line 31
    invoke-direct {v0}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;-><init>()V

    .line 32
    .line 33
    .line 34
    const/4 v2, 0x1

    .line 35
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 39
    .line 40
    .line 41
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 42
    .line 43
    invoke-direct {v0}, Lcom/intsig/office/fc/ddf/EscherSpRecord;-><init>()V

    .line 44
    .line 45
    .line 46
    const/4 v2, 0x2

    .line 47
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 48
    .line 49
    .line 50
    const/16 v2, 0x201

    .line 51
    .line 52
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setFlags(I)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 56
    .line 57
    .line 58
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;

    .line 59
    .line 60
    invoke-direct {v0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;-><init>()V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 67
    .line 68
    .line 69
    return-object p1
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public dispose()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->dispose()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChild(I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 9
    .line 10
    const/16 v1, -0xff0

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;

    .line 17
    .line 18
    new-instance v2, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;

    .line 19
    .line 20
    invoke-direct {v2}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;-><init>()V

    .line 21
    .line 22
    .line 23
    const/high16 v3, 0x44100000    # 576.0f

    .line 24
    .line 25
    const/high16 v4, 0x42900000    # 72.0f

    .line 26
    .line 27
    if-nez v1, :cond_0

    .line 28
    .line 29
    const/16 v1, -0xff1

    .line 30
    .line 31
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;

    .line 36
    .line 37
    new-instance v2, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDx1()I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    int-to-float v1, v1

    .line 44
    mul-float v1, v1, v4

    .line 45
    .line 46
    div-float/2addr v1, v3

    .line 47
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDy1()I

    .line 48
    .line 49
    .line 50
    move-result v5

    .line 51
    int-to-float v5, v5

    .line 52
    mul-float v5, v5, v4

    .line 53
    .line 54
    div-float/2addr v5, v3

    .line 55
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDx2()I

    .line 56
    .line 57
    .line 58
    move-result v6

    .line 59
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDx1()I

    .line 60
    .line 61
    .line 62
    move-result v7

    .line 63
    sub-int/2addr v6, v7

    .line 64
    int-to-float v6, v6

    .line 65
    mul-float v6, v6, v4

    .line 66
    .line 67
    div-float/2addr v6, v3

    .line 68
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDy2()I

    .line 69
    .line 70
    .line 71
    move-result v7

    .line 72
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDy1()I

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    sub-int/2addr v7, v0

    .line 77
    int-to-float v0, v7

    .line 78
    mul-float v0, v0, v4

    .line 79
    .line 80
    div-float/2addr v0, v3

    .line 81
    invoke-direct {v2, v1, v5, v6, v0}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 82
    .line 83
    .line 84
    goto :goto_0

    .line 85
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getCol1()S

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    int-to-float v0, v0

    .line 90
    mul-float v0, v0, v4

    .line 91
    .line 92
    div-float/2addr v0, v3

    .line 93
    iput v0, v2, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;->x:F

    .line 94
    .line 95
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getFlag()S

    .line 96
    .line 97
    .line 98
    move-result v0

    .line 99
    int-to-float v0, v0

    .line 100
    mul-float v0, v0, v4

    .line 101
    .line 102
    div-float/2addr v0, v3

    .line 103
    iput v0, v2, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;->y:F

    .line 104
    .line 105
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getDx1()S

    .line 106
    .line 107
    .line 108
    move-result v0

    .line 109
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getCol1()S

    .line 110
    .line 111
    .line 112
    move-result v5

    .line 113
    sub-int/2addr v0, v5

    .line 114
    int-to-float v0, v0

    .line 115
    mul-float v0, v0, v4

    .line 116
    .line 117
    div-float/2addr v0, v3

    .line 118
    iput v0, v2, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;->width:F

    .line 119
    .line 120
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getRow1()S

    .line 121
    .line 122
    .line 123
    move-result v0

    .line 124
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getFlag()S

    .line 125
    .line 126
    .line 127
    move-result v1

    .line 128
    sub-int/2addr v0, v1

    .line 129
    int-to-float v0, v0

    .line 130
    mul-float v0, v0, v4

    .line 131
    .line 132
    div-float/2addr v0, v3

    .line 133
    iput v0, v2, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;->height:F

    .line 134
    .line 135
    :goto_0
    return-object v2
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public getClientAnchor2D(Lcom/intsig/office/fc/hslf/model/Shape;)Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 20

    .line 1
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getParent()Lcom/intsig/office/fc/hslf/model/Shape;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getParent()Lcom/intsig/office/fc/hslf/model/Shape;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    check-cast v1, Lcom/intsig/office/fc/hslf/model/ShapeGroup;

    .line 16
    .line 17
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getParent()Lcom/intsig/office/fc/hslf/model/Shape;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->getClientAnchor2D(Lcom/intsig/office/fc/hslf/model/Shape;)Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getParent()Lcom/intsig/office/fc/hslf/model/Shape;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    check-cast v2, Lcom/intsig/office/fc/hslf/model/ShapeGroup;

    .line 30
    .line 31
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->getCoordinates()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 36
    .line 37
    .line 38
    move-result-wide v3

    .line 39
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 40
    .line 41
    .line 42
    move-result-wide v5

    .line 43
    div-double/2addr v3, v5

    .line 44
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 45
    .line 46
    .line 47
    move-result-wide v5

    .line 48
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 49
    .line 50
    .line 51
    move-result-wide v7

    .line 52
    div-double/2addr v5, v7

    .line 53
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 54
    .line 55
    .line 56
    move-result-wide v7

    .line 57
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 58
    .line 59
    .line 60
    move-result-wide v9

    .line 61
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 62
    .line 63
    .line 64
    move-result-wide v11

    .line 65
    sub-double/2addr v9, v11

    .line 66
    div-double/2addr v9, v3

    .line 67
    add-double v12, v7, v9

    .line 68
    .line 69
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 70
    .line 71
    .line 72
    move-result-wide v7

    .line 73
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 74
    .line 75
    .line 76
    move-result-wide v9

    .line 77
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 78
    .line 79
    .line 80
    move-result-wide v1

    .line 81
    sub-double/2addr v9, v1

    .line 82
    div-double/2addr v9, v5

    .line 83
    add-double v14, v7, v9

    .line 84
    .line 85
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 86
    .line 87
    .line 88
    move-result-wide v1

    .line 89
    div-double v16, v1, v3

    .line 90
    .line 91
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 92
    .line 93
    .line 94
    move-result-wide v0

    .line 95
    div-double v18, v0, v5

    .line 96
    .line 97
    new-instance v0, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;

    .line 98
    .line 99
    move-object v11, v0

    .line 100
    invoke-direct/range {v11 .. v19}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;-><init>(DDDD)V

    .line 101
    .line 102
    .line 103
    :cond_0
    return-object v0
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public getCoordinates()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChild(I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 9
    .line 10
    const/16 v1, -0xff7

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;

    .line 17
    .line 18
    new-instance v1, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;

    .line 19
    .line 20
    invoke-direct {v1}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;-><init>()V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectX1()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    int-to-float v2, v2

    .line 28
    const/high16 v3, 0x42900000    # 72.0f

    .line 29
    .line 30
    mul-float v2, v2, v3

    .line 31
    .line 32
    const/high16 v4, 0x44100000    # 576.0f

    .line 33
    .line 34
    div-float/2addr v2, v4

    .line 35
    iput v2, v1, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;->x:F

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectY1()I

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    int-to-float v2, v2

    .line 42
    mul-float v2, v2, v3

    .line 43
    .line 44
    div-float/2addr v2, v4

    .line 45
    iput v2, v1, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;->y:F

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectX2()I

    .line 48
    .line 49
    .line 50
    move-result v2

    .line 51
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectX1()I

    .line 52
    .line 53
    .line 54
    move-result v5

    .line 55
    sub-int/2addr v2, v5

    .line 56
    int-to-float v2, v2

    .line 57
    mul-float v2, v2, v3

    .line 58
    .line 59
    div-float/2addr v2, v4

    .line 60
    iput v2, v1, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;->width:F

    .line 61
    .line 62
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectY2()I

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectY1()I

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    sub-int/2addr v2, v0

    .line 71
    int-to-float v0, v2

    .line 72
    mul-float v0, v0, v3

    .line 73
    .line 74
    div-float/2addr v0, v4

    .line 75
    iput v0, v1, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;->height:F

    .line 76
    .line 77
    return-object v1
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getFlipHorizontal()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getGroupFlipHorizontal(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFlipVertical()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getGroupFlipVertical(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHyperlink()Lcom/intsig/office/fc/hslf/model/Hyperlink;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRotation()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getGroupRotation(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShapeId()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 18
    .line 19
    instance-of v1, v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 20
    .line 21
    if-eqz v1, :cond_0

    .line 22
    .line 23
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 24
    .line 25
    const/16 v1, -0xff6

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->getShapeId()I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    goto :goto_0

    .line 38
    :cond_0
    const/4 v0, 0x0

    .line 39
    :goto_0
    return v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getShapeType()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChild(I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 9
    .line 10
    const/16 v1, -0xff6

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherRecord;->getOptions()S

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    shr-int/lit8 v0, v0, 0x4

    .line 23
    .line 24
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getShapes()[Lcom/intsig/office/fc/hslf/model/Shape;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    .line 17
    .line 18
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 19
    .line 20
    .line 21
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    if-eqz v2, :cond_2

    .line 26
    .line 27
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 32
    .line 33
    instance-of v3, v2, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 34
    .line 35
    if-eqz v3, :cond_1

    .line 36
    .line 37
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 38
    .line 39
    invoke-static {v2, p0}, Lcom/intsig/office/fc/hslf/model/ShapeFactory;->createShape(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)Lcom/intsig/office/fc/hslf/model/Shape;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 44
    .line 45
    .line 46
    move-result-object v3

    .line 47
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/hslf/model/Shape;->setSheet(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    .line 48
    .line 49
    .line 50
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    new-array v0, v0, [Lcom/intsig/office/fc/hslf/model/Shape;

    .line 59
    .line 60
    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    check-cast v0, [Lcom/intsig/office/fc/hslf/model/Shape;

    .line 65
    .line 66
    return-object v0
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public moveTo(II)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor()Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget v1, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 6
    .line 7
    sub-int/2addr p1, v1

    .line 8
    iget v1, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 9
    .line 10
    sub-int/2addr p2, v1

    .line 11
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/java/awt/Rectangle;->translate(II)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->setAnchor(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->getShapes()[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const/4 v1, 0x0

    .line 22
    :goto_0
    array-length v2, v0

    .line 23
    if-ge v1, v2, :cond_0

    .line 24
    .line 25
    aget-object v2, v0, v1

    .line 26
    .line 27
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor()Lcom/intsig/office/java/awt/Rectangle;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-virtual {v2, p1, p2}, Lcom/intsig/office/java/awt/Rectangle;->translate(II)V

    .line 32
    .line 33
    .line 34
    aget-object v3, v0, v1

    .line 35
    .line 36
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/hslf/model/Shape;->setAnchor(Lcom/intsig/office/java/awt/geom/Rectangle2D;)V

    .line 37
    .line 38
    .line 39
    add-int/lit8 v1, v1, 0x1

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setAnchor(Lcom/intsig/office/java/awt/Rectangle;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChild(I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 9
    .line 10
    const/16 v2, -0xff0

    .line 11
    .line 12
    invoke-static {v0, v2}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;

    .line 17
    .line 18
    const/16 v3, 0x10

    .line 19
    .line 20
    new-array v3, v3, [B

    .line 21
    .line 22
    invoke-static {v3, v1, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    .line 23
    .line 24
    .line 25
    const/4 v4, 0x2

    .line 26
    invoke-static {v3, v4, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    .line 27
    .line 28
    .line 29
    const/4 v4, 0x4

    .line 30
    const/16 v5, 0x8

    .line 31
    .line 32
    invoke-static {v3, v4, v5}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 33
    .line 34
    .line 35
    const/4 v4, 0x0

    .line 36
    invoke-virtual {v2, v3, v1, v4}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->fillFields([BILcom/intsig/office/fc/ddf/EscherRecordFactory;)I

    .line 37
    .line 38
    .line 39
    iget v1, p1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 40
    .line 41
    mul-int/lit16 v1, v1, 0x240

    .line 42
    .line 43
    int-to-float v1, v1

    .line 44
    const/high16 v3, 0x42900000    # 72.0f

    .line 45
    .line 46
    div-float/2addr v1, v3

    .line 47
    float-to-int v1, v1

    .line 48
    int-to-short v1, v1

    .line 49
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->setFlag(S)V

    .line 50
    .line 51
    .line 52
    iget v1, p1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 53
    .line 54
    mul-int/lit16 v1, v1, 0x240

    .line 55
    .line 56
    int-to-float v1, v1

    .line 57
    div-float/2addr v1, v3

    .line 58
    float-to-int v1, v1

    .line 59
    int-to-short v1, v1

    .line 60
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->setCol1(S)V

    .line 61
    .line 62
    .line 63
    iget v1, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 64
    .line 65
    iget v4, p1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 66
    .line 67
    add-int/2addr v1, v4

    .line 68
    mul-int/lit16 v1, v1, 0x240

    .line 69
    .line 70
    int-to-float v1, v1

    .line 71
    div-float/2addr v1, v3

    .line 72
    float-to-int v1, v1

    .line 73
    int-to-short v1, v1

    .line 74
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->setDx1(S)V

    .line 75
    .line 76
    .line 77
    iget v1, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 78
    .line 79
    iget v4, p1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 80
    .line 81
    add-int/2addr v1, v4

    .line 82
    mul-int/lit16 v1, v1, 0x240

    .line 83
    .line 84
    int-to-float v1, v1

    .line 85
    div-float/2addr v1, v3

    .line 86
    float-to-int v1, v1

    .line 87
    int-to-short v1, v1

    .line 88
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->setRow1(S)V

    .line 89
    .line 90
    .line 91
    const/16 v1, -0xff7

    .line 92
    .line 93
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;

    .line 98
    .line 99
    iget v1, p1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 100
    .line 101
    mul-int/lit16 v1, v1, 0x240

    .line 102
    .line 103
    int-to-float v1, v1

    .line 104
    div-float/2addr v1, v3

    .line 105
    float-to-int v1, v1

    .line 106
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectX1(I)V

    .line 107
    .line 108
    .line 109
    iget v1, p1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 110
    .line 111
    mul-int/lit16 v1, v1, 0x240

    .line 112
    .line 113
    int-to-float v1, v1

    .line 114
    div-float/2addr v1, v3

    .line 115
    float-to-int v1, v1

    .line 116
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectY1(I)V

    .line 117
    .line 118
    .line 119
    iget v1, p1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 120
    .line 121
    iget v2, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 122
    .line 123
    add-int/2addr v1, v2

    .line 124
    mul-int/lit16 v1, v1, 0x240

    .line 125
    .line 126
    int-to-float v1, v1

    .line 127
    div-float/2addr v1, v3

    .line 128
    float-to-int v1, v1

    .line 129
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectX2(I)V

    .line 130
    .line 131
    .line 132
    iget v1, p1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 133
    .line 134
    iget p1, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 135
    .line 136
    add-int/2addr v1, p1

    .line 137
    mul-int/lit16 v1, v1, 0x240

    .line 138
    .line 139
    int-to-float p1, v1

    .line 140
    div-float/2addr p1, v3

    .line 141
    float-to-int p1, p1

    .line 142
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectY2(I)V

    .line 143
    .line 144
    .line 145
    return-void
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public setCoordinates(Lcom/intsig/office/java/awt/geom/Rectangle2D;)V
    .locals 13

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChild(I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 9
    .line 10
    const/16 v1, -0xff7

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 19
    .line 20
    .line 21
    move-result-wide v1

    .line 22
    const-wide/high16 v3, 0x4082000000000000L    # 576.0

    .line 23
    .line 24
    mul-double v1, v1, v3

    .line 25
    .line 26
    const-wide/high16 v5, 0x4052000000000000L    # 72.0

    .line 27
    .line 28
    div-double/2addr v1, v5

    .line 29
    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    .line 30
    .line 31
    .line 32
    move-result-wide v1

    .line 33
    long-to-int v2, v1

    .line 34
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 35
    .line 36
    .line 37
    move-result-wide v7

    .line 38
    mul-double v7, v7, v3

    .line 39
    .line 40
    div-double/2addr v7, v5

    .line 41
    invoke-static {v7, v8}, Ljava/lang/Math;->round(D)J

    .line 42
    .line 43
    .line 44
    move-result-wide v7

    .line 45
    long-to-int v1, v7

    .line 46
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 47
    .line 48
    .line 49
    move-result-wide v7

    .line 50
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 51
    .line 52
    .line 53
    move-result-wide v9

    .line 54
    add-double/2addr v7, v9

    .line 55
    mul-double v7, v7, v3

    .line 56
    .line 57
    div-double/2addr v7, v5

    .line 58
    invoke-static {v7, v8}, Ljava/lang/Math;->round(D)J

    .line 59
    .line 60
    .line 61
    move-result-wide v7

    .line 62
    long-to-int v8, v7

    .line 63
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 64
    .line 65
    .line 66
    move-result-wide v9

    .line 67
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 68
    .line 69
    .line 70
    move-result-wide v11

    .line 71
    add-double/2addr v9, v11

    .line 72
    mul-double v9, v9, v3

    .line 73
    .line 74
    div-double/2addr v9, v5

    .line 75
    invoke-static {v9, v10}, Ljava/lang/Math;->round(D)J

    .line 76
    .line 77
    .line 78
    move-result-wide v3

    .line 79
    long-to-int p1, v3

    .line 80
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectX1(I)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectY1(I)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {v0, v8}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectX2(I)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectY2(I)V

    .line 90
    .line 91
    .line 92
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

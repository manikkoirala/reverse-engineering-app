.class public Lcom/intsig/office/fc/hslf/record/TimeNodeAtom;
.super Lcom/intsig/office/fc/hslf/record/PositionDependentRecordAtom;
.source "TimeNodeAtom.java"


# static fields
.field public static final TNT_Behavior:I = 0x2

.field public static final TNT_Parallel:I = 0x0

.field public static final TNT_Sequential:I = 0x1

.field public static final TNT__Media:I = 0x3

.field private static _type:J = 0xf127L


# instance fields
.field private _header:[B

.field private duration:I

.field private fDurationProperty:Z

.field private fFillProperty:Z

.field private fGroupingTypeProperty:Z

.field private fRestartProperty:Z

.field private fill:I

.field private reserved1:I

.field private reserved2:I

.field private reserved3:B

.field private reserved4:Z

.field private reserved5:[B

.field private restart:I

.field private timeNodeType:I

.field private unused:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected constructor <init>([BII)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/PositionDependentRecordAtom;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 p3, 0x8

    .line 5
    .line 6
    new-array v0, p3, [B

    .line 7
    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/TimeNodeAtom;->_header:[B

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 12
    .line 13
    .line 14
    add-int/lit8 p3, p2, 0x8

    .line 15
    .line 16
    invoke-static {p1, p3}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 17
    .line 18
    .line 19
    move-result p3

    .line 20
    iput p3, p0, Lcom/intsig/office/fc/hslf/record/TimeNodeAtom;->reserved1:I

    .line 21
    .line 22
    add-int/lit8 p3, p2, 0xc

    .line 23
    .line 24
    invoke-static {p1, p3}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 25
    .line 26
    .line 27
    move-result p3

    .line 28
    iput p3, p0, Lcom/intsig/office/fc/hslf/record/TimeNodeAtom;->restart:I

    .line 29
    .line 30
    add-int/lit8 p3, p2, 0x10

    .line 31
    .line 32
    invoke-static {p1, p3}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 33
    .line 34
    .line 35
    move-result p3

    .line 36
    iput p3, p0, Lcom/intsig/office/fc/hslf/record/TimeNodeAtom;->timeNodeType:I

    .line 37
    .line 38
    add-int/lit8 p3, p2, 0x14

    .line 39
    .line 40
    invoke-static {p1, p3}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 41
    .line 42
    .line 43
    move-result p3

    .line 44
    iput p3, p0, Lcom/intsig/office/fc/hslf/record/TimeNodeAtom;->fill:I

    .line 45
    .line 46
    add-int/lit8 p3, p2, 0x20

    .line 47
    .line 48
    invoke-static {p1, p3}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 49
    .line 50
    .line 51
    move-result p3

    .line 52
    iput p3, p0, Lcom/intsig/office/fc/hslf/record/TimeNodeAtom;->duration:I

    .line 53
    .line 54
    add-int/lit8 p2, p2, 0x24

    .line 55
    .line 56
    aget-byte p1, p1, p2

    .line 57
    .line 58
    and-int/lit8 p2, p1, 0x10

    .line 59
    .line 60
    shr-int/lit8 p2, p2, 0x4

    .line 61
    .line 62
    const/4 p3, 0x1

    .line 63
    if-lez p2, :cond_0

    .line 64
    .line 65
    const/4 p2, 0x1

    .line 66
    goto :goto_0

    .line 67
    :cond_0
    const/4 p2, 0x0

    .line 68
    :goto_0
    iput-boolean p2, p0, Lcom/intsig/office/fc/hslf/record/TimeNodeAtom;->fDurationProperty:Z

    .line 69
    .line 70
    and-int/lit8 p2, p1, 0x8

    .line 71
    .line 72
    shr-int/lit8 p2, p2, 0x3

    .line 73
    .line 74
    if-lez p2, :cond_1

    .line 75
    .line 76
    const/4 p2, 0x1

    .line 77
    goto :goto_1

    .line 78
    :cond_1
    const/4 p2, 0x0

    .line 79
    :goto_1
    iput-boolean p2, p0, Lcom/intsig/office/fc/hslf/record/TimeNodeAtom;->fGroupingTypeProperty:Z

    .line 80
    .line 81
    and-int/lit8 p2, p1, 0x2

    .line 82
    .line 83
    shr-int/2addr p2, p3

    .line 84
    if-lez p2, :cond_2

    .line 85
    .line 86
    const/4 p2, 0x1

    .line 87
    goto :goto_2

    .line 88
    :cond_2
    const/4 p2, 0x0

    .line 89
    :goto_2
    iput-boolean p2, p0, Lcom/intsig/office/fc/hslf/record/TimeNodeAtom;->fRestartProperty:Z

    .line 90
    .line 91
    and-int/2addr p1, p3

    .line 92
    if-lez p1, :cond_3

    .line 93
    .line 94
    const/4 v1, 0x1

    .line 95
    :cond_3
    iput-boolean v1, p0, Lcom/intsig/office/fc/hslf/record/TimeNodeAtom;->fFillProperty:Z

    .line 96
    .line 97
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/TimeNodeAtom;->_header:[B

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/office/fc/hslf/record/TimeNodeAtom;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public updateOtherRecordReferences(Ljava/util/Hashtable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

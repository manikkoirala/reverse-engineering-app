.class public final Lcom/intsig/office/fc/hslf/record/NotesAtom;
.super Lcom/intsig/office/fc/hslf/record/RecordAtom;
.source "NotesAtom.java"


# static fields
.field private static _type:J = 0x3f1L


# instance fields
.field private _header:[B

.field private followMasterBackground:Z

.field private followMasterObjects:Z

.field private followMasterScheme:Z

.field private reserved:[B

.field private slideID:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected constructor <init>([BII)V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordAtom;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x8

    .line 5
    .line 6
    if-ge p3, v0, :cond_0

    .line 7
    .line 8
    const/16 p3, 0x8

    .line 9
    .line 10
    :cond_0
    new-array v1, v0, [B

    .line 11
    .line 12
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->_header:[B

    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 16
    .line 17
    .line 18
    add-int/lit8 v0, p2, 0x8

    .line 19
    .line 20
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    iput v0, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->slideID:I

    .line 25
    .line 26
    add-int/lit8 v0, p2, 0xc

    .line 27
    .line 28
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getUShort([BI)I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    and-int/lit8 v1, v0, 0x4

    .line 33
    .line 34
    const/4 v3, 0x4

    .line 35
    const/4 v4, 0x1

    .line 36
    if-ne v1, v3, :cond_1

    .line 37
    .line 38
    iput-boolean v4, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->followMasterBackground:Z

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    iput-boolean v2, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->followMasterBackground:Z

    .line 42
    .line 43
    :goto_0
    and-int/lit8 v1, v0, 0x2

    .line 44
    .line 45
    const/4 v3, 0x2

    .line 46
    if-ne v1, v3, :cond_2

    .line 47
    .line 48
    iput-boolean v4, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->followMasterScheme:Z

    .line 49
    .line 50
    goto :goto_1

    .line 51
    :cond_2
    iput-boolean v2, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->followMasterScheme:Z

    .line 52
    .line 53
    :goto_1
    and-int/2addr v0, v4

    .line 54
    if-ne v0, v4, :cond_3

    .line 55
    .line 56
    iput-boolean v4, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->followMasterObjects:Z

    .line 57
    .line 58
    goto :goto_2

    .line 59
    :cond_3
    iput-boolean v2, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->followMasterObjects:Z

    .line 60
    .line 61
    :goto_2
    add-int/lit8 p3, p3, -0xe

    .line 62
    .line 63
    new-array p3, p3, [B

    .line 64
    .line 65
    iput-object p3, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->reserved:[B

    .line 66
    .line 67
    add-int/lit8 p2, p2, 0xe

    .line 68
    .line 69
    array-length v0, p3

    .line 70
    invoke-static {p1, p2, p3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 71
    .line 72
    .line 73
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->_header:[B

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->reserved:[B

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFollowMasterBackground()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->followMasterBackground:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFollowMasterObjects()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->followMasterObjects:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFollowMasterScheme()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->followMasterScheme:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideID()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->slideID:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setFollowMasterBackground(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->followMasterBackground:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFollowMasterObjects(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->followMasterObjects:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFollowMasterScheme(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->followMasterScheme:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSlideID(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->slideID:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->_header:[B

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 4
    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->slideID:I

    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hslf/record/Record;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 9
    .line 10
    .line 11
    iget-boolean v0, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->followMasterObjects:Z

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    int-to-short v0, v0

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    :goto_0
    iget-boolean v1, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->followMasterScheme:Z

    .line 20
    .line 21
    if-eqz v1, :cond_1

    .line 22
    .line 23
    add-int/lit8 v0, v0, 0x2

    .line 24
    .line 25
    int-to-short v0, v0

    .line 26
    :cond_1
    iget-boolean v1, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->followMasterBackground:Z

    .line 27
    .line 28
    if-eqz v1, :cond_2

    .line 29
    .line 30
    add-int/lit8 v0, v0, 0x4

    .line 31
    .line 32
    int-to-short v0, v0

    .line 33
    :cond_2
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hslf/record/Record;->writeLittleEndian(SLjava/io/OutputStream;)V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/NotesAtom;->reserved:[B

    .line 37
    .line 38
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.class public final Lcom/intsig/office/fc/hslf/record/ExControlAtom;
.super Lcom/intsig/office/fc/hslf/record/RecordAtom;
.source "ExControlAtom.java"


# instance fields
.field private _header:[B

.field private _id:I


# direct methods
.method protected constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordAtom;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [B

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExControlAtom;->_header:[B

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/record/ExControlAtom;->getRecordType()J

    move-result-wide v1

    long-to-int v2, v1

    int-to-short v1, v2

    const/4 v2, 0x2

    invoke-static {v0, v2, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExControlAtom;->_header:[B

    const/4 v1, 0x4

    invoke-static {v0, v1, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 2

    .line 5
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordAtom;-><init>()V

    const/16 p3, 0x8

    new-array v0, p3, [B

    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExControlAtom;->_header:[B

    const/4 v1, 0x0

    .line 7
    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, p3

    .line 8
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/hslf/record/ExControlAtom;->_id:I

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExControlAtom;->_header:[B

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExControlAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 4
    .line 5
    int-to-long v0, v0

    .line 6
    return-wide v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/record/ExControlAtom;->_id:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setSlideId(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/record/ExControlAtom;->_id:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExControlAtom;->_header:[B

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x4

    .line 7
    new-array v0, v0, [B

    .line 8
    .line 9
    iget v1, p0, Lcom/intsig/office/fc/hslf/record/ExControlAtom;->_id:I

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BI)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public final Lcom/intsig/office/fc/hslf/record/MainMaster;
.super Lcom/intsig/office/fc/hslf/record/SheetContainer;
.source "MainMaster.java"


# static fields
.field private static _type:J = 0x3f8L


# instance fields
.field private _colorScheme:Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

.field private _header:[B

.field private clrscheme:[Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

.field private ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

.field private slideAtom:Lcom/intsig/office/fc/hslf/record/SlideAtom;

.field private txmasters:[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected constructor <init>([BII)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/SheetContainer;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x8

    .line 5
    .line 6
    new-array v1, v0, [B

    .line 7
    .line 8
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->_header:[B

    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 12
    .line 13
    .line 14
    add-int/2addr p2, v0

    .line 15
    sub-int/2addr p3, v0

    .line 16
    invoke-static {p1, p2, p3}, Lcom/intsig/office/fc/hslf/record/Record;->findChildRecords([BII)[Lcom/intsig/office/fc/hslf/record/Record;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 21
    .line 22
    new-instance p1, Ljava/util/ArrayList;

    .line 23
    .line 24
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 25
    .line 26
    .line 27
    new-instance p2, Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 30
    .line 31
    .line 32
    :goto_0
    iget-object p3, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 33
    .line 34
    array-length v0, p3

    .line 35
    if-ge v2, v0, :cond_5

    .line 36
    .line 37
    aget-object p3, p3, v2

    .line 38
    .line 39
    instance-of v0, p3, Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 40
    .line 41
    if-eqz v0, :cond_0

    .line 42
    .line 43
    check-cast p3, Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 44
    .line 45
    iput-object p3, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->slideAtom:Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 46
    .line 47
    goto :goto_1

    .line 48
    :cond_0
    instance-of v0, p3, Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 49
    .line 50
    if-eqz v0, :cond_1

    .line 51
    .line 52
    check-cast p3, Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 53
    .line 54
    iput-object p3, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 55
    .line 56
    goto :goto_1

    .line 57
    :cond_1
    instance-of v0, p3, Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 58
    .line 59
    if-eqz v0, :cond_2

    .line 60
    .line 61
    check-cast p3, Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 62
    .line 63
    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_2
    instance-of v0, p3, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 68
    .line 69
    if-eqz v0, :cond_3

    .line 70
    .line 71
    check-cast p3, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 72
    .line 73
    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    .line 75
    .line 76
    :cond_3
    :goto_1
    iget-object p3, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 77
    .line 78
    if-eqz p3, :cond_4

    .line 79
    .line 80
    iget-object p3, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 81
    .line 82
    aget-object p3, p3, v2

    .line 83
    .line 84
    instance-of v0, p3, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 85
    .line 86
    if-eqz v0, :cond_4

    .line 87
    .line 88
    check-cast p3, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 89
    .line 90
    iput-object p3, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->_colorScheme:Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 91
    .line 92
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_5
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 96
    .line 97
    .line 98
    move-result p3

    .line 99
    new-array p3, p3, [Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 100
    .line 101
    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 102
    .line 103
    .line 104
    move-result-object p1

    .line 105
    check-cast p1, [Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 106
    .line 107
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->txmasters:[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 108
    .line 109
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    .line 110
    .line 111
    .line 112
    move-result p1

    .line 113
    new-array p1, p1, [Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 114
    .line 115
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 116
    .line 117
    .line 118
    move-result-object p1

    .line 119
    check-cast p1, [Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 120
    .line 121
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->clrscheme:[Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 122
    .line 123
    return-void
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public dispose()V
    .locals 6

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->dispose()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->_header:[B

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->slideAtom:Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 8
    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->dispose()V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->slideAtom:Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 15
    .line 16
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 17
    .line 18
    if-eqz v1, :cond_1

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->dispose()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 24
    .line 25
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->txmasters:[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 26
    .line 27
    const/4 v2, 0x0

    .line 28
    if-eqz v1, :cond_3

    .line 29
    .line 30
    array-length v3, v1

    .line 31
    const/4 v4, 0x0

    .line 32
    :goto_0
    if-ge v4, v3, :cond_2

    .line 33
    .line 34
    aget-object v5, v1, v4

    .line 35
    .line 36
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;->dispose()V

    .line 37
    .line 38
    .line 39
    add-int/lit8 v4, v4, 0x1

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->txmasters:[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 43
    .line 44
    :cond_3
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->clrscheme:[Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 45
    .line 46
    if-eqz v1, :cond_5

    .line 47
    .line 48
    array-length v3, v1

    .line 49
    :goto_1
    if-ge v2, v3, :cond_4

    .line 50
    .line 51
    aget-object v4, v1, v2

    .line 52
    .line 53
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;->dispose()V

    .line 54
    .line 55
    .line 56
    add-int/lit8 v2, v2, 0x1

    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_4
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->clrscheme:[Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 60
    .line 61
    :cond_5
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->_colorScheme:Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 62
    .line 63
    if-eqz v1, :cond_6

    .line 64
    .line 65
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;->dispose()V

    .line 66
    .line 67
    .line 68
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->_colorScheme:Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 69
    .line 70
    :cond_6
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getColorScheme()Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->_colorScheme:Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getColorSchemeAtoms()[Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->clrscheme:[Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPPDrawing()Lcom/intsig/office/fc/hslf/record/PPDrawing;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/office/fc/hslf/record/MainMaster;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->slideAtom:Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTxMasterStyleAtoms()[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/MainMaster;->txmasters:[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

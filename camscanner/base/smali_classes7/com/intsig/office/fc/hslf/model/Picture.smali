.class public Lcom/intsig/office/fc/hslf/model/Picture;
.super Lcom/intsig/office/fc/hslf/model/SimpleShape;
.source "Picture.java"


# static fields
.field public static final DIB:B = 0x7t

.field public static final EMF:I = 0x2

.field public static final JPEG:I = 0x5

.field public static final PICT:I = 0x4

.field public static final PNG:I = 0x6

.field public static final WMF:I = 0x3


# direct methods
.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hslf/model/Picture;-><init>(ILcom/intsig/office/fc/hslf/model/Shape;)V

    return-void
.end method

.method public constructor <init>(ILcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0, p2}, Lcom/intsig/office/fc/hslf/model/SimpleShape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    .line 3
    instance-of p2, p2, Lcom/intsig/office/fc/hslf/model/ShapeGroup;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/Picture;->createSpContainer(IZ)Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 0

    .line 4
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/SimpleShape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    return-void
.end method


# virtual methods
.method protected afterInsert(Lcom/intsig/office/fc/hslf/model/Sheet;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->afterInsert(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Picture;->getEscherBSERecord()Lcom/intsig/office/fc/ddf/EscherBSERecord;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->getRef()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    add-int/lit8 v0, v0, 0x1

    .line 13
    .line 14
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->setRef(I)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor()Lcom/intsig/office/java/awt/Rectangle;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    new-instance v0, Lcom/intsig/office/java/awt/Rectangle;

    .line 22
    .line 23
    invoke-direct {v0}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p1, v0}, Lcom/intsig/office/java/awt/Rectangle;->equals(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    if-eqz p1, :cond_0

    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Picture;->setDefaultSize()V

    .line 33
    .line 34
    .line 35
    :cond_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected createSpContainer(IZ)Lcom/intsig/office/fc/ddf/EscherContainerRecord;
    .locals 2

    .line 1
    invoke-super {p0, p2}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->createSpContainer(Z)Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    iput-object p2, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 6
    .line 7
    const/16 v0, 0xf

    .line 8
    .line 9
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 10
    .line 11
    .line 12
    iget-object p2, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 13
    .line 14
    const/16 v0, -0xff6

    .line 15
    .line 16
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 17
    .line 18
    .line 19
    move-result-object p2

    .line 20
    check-cast p2, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 21
    .line 22
    const/16 v0, 0x4b2

    .line 23
    .line 24
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 25
    .line 26
    .line 27
    iget-object p2, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 28
    .line 29
    const/16 v0, -0xff5

    .line 30
    .line 31
    invoke-static {p2, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    check-cast p2, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 36
    .line 37
    const/16 v0, 0x7f

    .line 38
    .line 39
    const v1, 0x800080

    .line 40
    .line 41
    .line 42
    invoke-static {p2, v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 43
    .line 44
    .line 45
    const/16 v0, 0x4104

    .line 46
    .line 47
    invoke-static {p2, v0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 48
    .line 49
    .line 50
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 51
    .line 52
    return-object p1
    .line 53
.end method

.method protected getEscherBSERecord()Lcom/intsig/office/fc/ddf/EscherBSERecord;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSlideShow()Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getDocumentRecord()Lcom/intsig/office/fc/hslf/record/Document;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/Document;->getPPDrawingGroup()Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;->getDggContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const/16 v1, -0xfff

    .line 22
    .line 23
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 28
    .line 29
    const/4 v1, 0x0

    .line 30
    if-nez v0, :cond_0

    .line 31
    .line 32
    return-object v1

    .line 33
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Picture;->getPictureIndex()I

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    if-nez v2, :cond_1

    .line 42
    .line 43
    return-object v1

    .line 44
    :cond_1
    add-int/lit8 v2, v2, -0x1

    .line 45
    .line 46
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherBSERecord;

    .line 51
    .line 52
    return-object v0
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getEscherOptRecord()Lcom/intsig/office/fc/ddf/EscherOptRecord;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPictureData()Lcom/intsig/office/fc/hslf/usermodel/PictureData;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSlideShow()Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getPictureData()[Lcom/intsig/office/fc/hslf/usermodel/PictureData;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Picture;->getEscherBSERecord()Lcom/intsig/office/fc/ddf/EscherBSERecord;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    if-nez v1, :cond_0

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_0
    const/4 v2, 0x0

    .line 21
    :goto_0
    array-length v3, v0

    .line 22
    if-ge v2, v3, :cond_2

    .line 23
    .line 24
    aget-object v3, v0, v2

    .line 25
    .line 26
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->getOffset()I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->getOffset()I

    .line 31
    .line 32
    .line 33
    move-result v4

    .line 34
    if-ne v3, v4, :cond_1

    .line 35
    .line 36
    aget-object v0, v0, v2

    .line 37
    .line 38
    return-object v0

    .line 39
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_2
    :goto_1
    const/4 v0, 0x0

    .line 43
    return-object v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getPictureIndex()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    const/16 v1, 0x104

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    const/4 v0, 0x0

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    :goto_0
    return v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getPictureName()Ljava/lang/String;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    const/16 v1, 0x105

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherComplexProperty;

    .line 18
    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    :try_start_0
    new-instance v1, Ljava/lang/String;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherComplexProperty;->getComplexData()[B

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const-string v2, "UTF-16LE"

    .line 28
    .line 29
    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 30
    .line 31
    .line 32
    const/4 v0, 0x0

    .line 33
    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(I)I

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    const/4 v3, -0x1

    .line 38
    if-ne v2, v3, :cond_0

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_0
    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :goto_0
    return-object v1

    .line 46
    :catch_0
    move-exception v0

    .line 47
    new-instance v1, Lcom/intsig/office/fc/hslf/exceptions/HSLFException;

    .line 48
    .line 49
    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    .line 50
    .line 51
    .line 52
    throw v1

    .line 53
    :cond_1
    const/4 v0, 0x0

    .line 54
    return-object v0
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public setDefaultSize()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setPictureName(Ljava/lang/String;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    const/4 p1, 0x0

    .line 20
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    const-string v2, "UTF-16LE"

    .line 28
    .line 29
    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    new-instance v2, Lcom/intsig/office/fc/ddf/EscherComplexProperty;

    .line 34
    .line 35
    const/16 v3, 0x105

    .line 36
    .line 37
    invoke-direct {v2, v3, p1, v1}, Lcom/intsig/office/fc/ddf/EscherComplexProperty;-><init>(SZ[B)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    .line 42
    .line 43
    return-void

    .line 44
    :catch_0
    move-exception p1

    .line 45
    new-instance v0, Lcom/intsig/office/fc/hslf/exceptions/HSLFException;

    .line 46
    .line 47
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    .line 48
    .line 49
    .line 50
    throw v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

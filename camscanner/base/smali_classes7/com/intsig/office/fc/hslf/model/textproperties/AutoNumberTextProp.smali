.class public Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;
.super Ljava/lang/Object;
.source "AutoNumberTextProp.java"


# instance fields
.field private numberingType:I

.field private start:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 2
    iput v0, p0, Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;->numberingType:I

    const/4 v0, 0x0

    .line 3
    iput v0, p0, Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;->start:I

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    iput p1, p0, Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;->numberingType:I

    .line 6
    iput p2, p0, Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;->start:I

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNumberingType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;->numberingType:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStart()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;->start:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setNumberingType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;->numberingType:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setStart(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/model/textproperties/AutoNumberTextProp;->start:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

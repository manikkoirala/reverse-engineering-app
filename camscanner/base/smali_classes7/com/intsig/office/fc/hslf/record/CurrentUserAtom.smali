.class public Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;
.super Ljava/lang/Object;
.source "CurrentUserAtom.java"


# static fields
.field public static final atomHeader:[B

.field public static final encHeaderToken:[B

.field public static final headerToken:[B

.field public static final ppt97FileVer:[B


# instance fields
.field private _contents:[B

.field private currentEditOffset:J

.field private docFinalVersion:I

.field private docMajorNo:B

.field private docMinorNo:B

.field private lastEditUser:Ljava/lang/String;

.field private releaseVersion:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const/4 v0, 0x4

    .line 2
    new-array v1, v0, [B

    .line 3
    .line 4
    fill-array-data v1, :array_0

    .line 5
    .line 6
    .line 7
    sput-object v1, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->atomHeader:[B

    .line 8
    .line 9
    new-array v1, v0, [B

    .line 10
    .line 11
    fill-array-data v1, :array_1

    .line 12
    .line 13
    .line 14
    sput-object v1, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->headerToken:[B

    .line 15
    .line 16
    new-array v0, v0, [B

    .line 17
    .line 18
    fill-array-data v0, :array_2

    .line 19
    .line 20
    .line 21
    sput-object v0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->encHeaderToken:[B

    .line 22
    .line 23
    const/4 v0, 0x6

    .line 24
    new-array v0, v0, [B

    .line 25
    .line 26
    fill-array-data v0, :array_3

    .line 27
    .line 28
    .line 29
    sput-object v0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->ppt97FileVer:[B

    .line 30
    .line 31
    return-void

    .line 32
    nop

    .line 33
    :array_0
    .array-data 1
        0x0t
        0x0t
        -0xat
        0xft
    .end array-data

    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    :array_1
    .array-data 1
        0x5ft
        -0x40t
        -0x6ft
        -0x1dt
    .end array-data

    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    :array_2
    .array-data 1
        -0x21t
        -0x3ct
        -0x2ft
        -0xdt
    .end array-data

    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    :array_3
    .array-data 1
        0x8t
        0x0t
        -0xdt
        0x3t
        0x3t
        0x0t
    .end array-data
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    new-array v1, v0, [B

    .line 2
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v1, 0x3f4

    .line 3
    iput v1, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->docFinalVersion:I

    const/4 v1, 0x3

    .line 4
    iput-byte v1, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->docMajorNo:B

    .line 5
    iput-byte v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->docMinorNo:B

    const-wide/16 v0, 0x8

    .line 6
    iput-wide v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->releaseVersion:J

    const-wide/16 v0, 0x0

    .line 7
    iput-wide v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->currentEditOffset:J

    const-string v0, "Apache POI"

    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Current User"

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->getPropertyRawData(Ljava/lang/String;)[B

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->_contents:[B

    if-eqz p1, :cond_2

    .line 12
    array-length v0, p1

    const/high16 v1, 0x20000

    if-gt v0, v1, :cond_2

    .line 13
    array-length v0, p1

    const/16 v1, 0x1c

    if-ge v0, v1, :cond_1

    .line 14
    array-length v0, p1

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    .line 15
    invoke-static {p1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([B)I

    move-result p1

    .line 16
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(I)V

    add-int/2addr p1, v1

    .line 17
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v0, v0

    if-ne p1, v0, :cond_0

    .line 18
    new-instance p1, Lcom/intsig/office/fc/hslf/exceptions/OldPowerPointFormatException;

    const-string v0, "Based on the Current User stream, you seem to have supplied a PowerPoint95 file, which isn\'t supported"

    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hslf/exceptions/OldPowerPointFormatException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 19
    :cond_0
    new-instance p1, Lcom/intsig/office/fc/hslf/exceptions/CorruptPowerPointFileException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The Current User stream must be at least 28 bytes long, but was only "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 20
    :cond_1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->init()V

    return-void

    .line 21
    :cond_2
    new-instance p1, Lcom/intsig/office/fc/hslf/exceptions/CorruptPowerPointFileException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The Current User stream is implausably long. It\'s normally 28-200 bytes long, but was "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " bytes"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>([B)V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->_contents:[B

    .line 24
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->init()V

    return-void
.end method

.method private init()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->_contents:[B

    .line 2
    .line 3
    const/16 v1, 0xc

    .line 4
    .line 5
    aget-byte v1, v0, v1

    .line 6
    .line 7
    sget-object v2, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->encHeaderToken:[B

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    aget-byte v4, v2, v3

    .line 11
    .line 12
    if-ne v1, v4, :cond_1

    .line 13
    .line 14
    const/16 v1, 0xd

    .line 15
    .line 16
    aget-byte v1, v0, v1

    .line 17
    .line 18
    const/4 v4, 0x1

    .line 19
    aget-byte v4, v2, v4

    .line 20
    .line 21
    if-ne v1, v4, :cond_1

    .line 22
    .line 23
    const/16 v1, 0xe

    .line 24
    .line 25
    aget-byte v1, v0, v1

    .line 26
    .line 27
    const/4 v4, 0x2

    .line 28
    aget-byte v4, v2, v4

    .line 29
    .line 30
    if-ne v1, v4, :cond_1

    .line 31
    .line 32
    const/16 v1, 0xf

    .line 33
    .line 34
    aget-byte v1, v0, v1

    .line 35
    .line 36
    const/4 v4, 0x3

    .line 37
    aget-byte v2, v2, v4

    .line 38
    .line 39
    if-eq v1, v2, :cond_0

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hslf/exceptions/EncryptedPowerPointFileException;

    .line 43
    .line 44
    const-string v1, "Cannot process encrypted office files!"

    .line 45
    .line 46
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hslf/exceptions/EncryptedPowerPointFileException;-><init>(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    throw v0

    .line 50
    :cond_1
    :goto_0
    const/16 v1, 0x10

    .line 51
    .line 52
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getUInt([BI)J

    .line 53
    .line 54
    .line 55
    move-result-wide v0

    .line 56
    iput-wide v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->currentEditOffset:J

    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->_contents:[B

    .line 59
    .line 60
    const/16 v1, 0x16

    .line 61
    .line 62
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getUShort([BI)I

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    iput v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->docFinalVersion:I

    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->_contents:[B

    .line 69
    .line 70
    const/16 v1, 0x18

    .line 71
    .line 72
    aget-byte v1, v0, v1

    .line 73
    .line 74
    iput-byte v1, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->docMajorNo:B

    .line 75
    .line 76
    const/16 v1, 0x19

    .line 77
    .line 78
    aget-byte v1, v0, v1

    .line 79
    .line 80
    iput-byte v1, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->docMinorNo:B

    .line 81
    .line 82
    const/16 v1, 0x14

    .line 83
    .line 84
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getUShort([BI)I

    .line 85
    .line 86
    .line 87
    move-result v0

    .line 88
    int-to-long v0, v0

    .line 89
    const-wide/16 v4, 0x200

    .line 90
    .line 91
    const-wide/16 v6, 0x0

    .line 92
    .line 93
    cmp-long v2, v0, v4

    .line 94
    .line 95
    if-lez v2, :cond_2

    .line 96
    .line 97
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    .line 98
    .line 99
    new-instance v4, Ljava/lang/StringBuilder;

    .line 100
    .line 101
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    .line 103
    .line 104
    const-string v5, "Warning - invalid username length "

    .line 105
    .line 106
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    const-string v0, " found, treating as if there was no username set"

    .line 113
    .line 114
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object v0

    .line 121
    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    move-wide v0, v6

    .line 125
    :cond_2
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->_contents:[B

    .line 126
    .line 127
    array-length v4, v2

    .line 128
    long-to-int v1, v0

    .line 129
    add-int/lit8 v0, v1, 0x1c

    .line 130
    .line 131
    add-int/lit8 v5, v0, 0x4

    .line 132
    .line 133
    if-lt v4, v5, :cond_4

    .line 134
    .line 135
    invoke-static {v2, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getUInt([BI)J

    .line 136
    .line 137
    .line 138
    move-result-wide v8

    .line 139
    iput-wide v8, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->releaseVersion:J

    .line 140
    .line 141
    cmp-long v0, v8, v6

    .line 142
    .line 143
    if-eqz v0, :cond_3

    .line 144
    .line 145
    goto :goto_1

    .line 146
    :cond_3
    new-instance v0, Lcom/intsig/office/fc/hwpf/OldWordFileFormatException;

    .line 147
    .line 148
    const-string v1, "The document is too old - Word 95 or older. Try HWPFOldDocument instead?"

    .line 149
    .line 150
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hwpf/OldWordFileFormatException;-><init>(Ljava/lang/String;)V

    .line 151
    .line 152
    .line 153
    throw v0

    .line 154
    :cond_4
    iput-wide v6, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->releaseVersion:J

    .line 155
    .line 156
    :goto_1
    mul-int/lit8 v0, v1, 0x2

    .line 157
    .line 158
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->_contents:[B

    .line 159
    .line 160
    array-length v4, v2

    .line 161
    add-int v6, v5, v0

    .line 162
    .line 163
    if-lt v4, v6, :cond_5

    .line 164
    .line 165
    new-array v1, v0, [B

    .line 166
    .line 167
    invoke-static {v2, v5, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 168
    .line 169
    .line 170
    invoke-static {v1}, Lcom/intsig/office/fc/util/StringUtil;->getFromUnicodeLE([B)Ljava/lang/String;

    .line 171
    .line 172
    .line 173
    move-result-object v0

    .line 174
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    .line 175
    .line 176
    goto :goto_2

    .line 177
    :cond_5
    new-array v0, v1, [B

    .line 178
    .line 179
    const/16 v4, 0x1c

    .line 180
    .line 181
    invoke-static {v2, v4, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 182
    .line 183
    .line 184
    invoke-static {v0, v3, v1}, Lcom/intsig/office/fc/util/StringUtil;->getFromCompressedUnicode([BII)Ljava/lang/String;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    .line 189
    .line 190
    :goto_2
    return-void
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->_contents:[B

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCurrentEditOffset()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->currentEditOffset:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDocFinalVersion()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->docFinalVersion:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDocMajorNo()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->docMajorNo:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDocMinorNo()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->docMinorNo:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLastEditUsername()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getReleaseVersion()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->releaseVersion:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setCurrentEditOffset(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->currentEditOffset:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLastEditUsername(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setReleaseVersion(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/office/fc/hslf/record/CurrentUserAtom;->releaseVersion:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

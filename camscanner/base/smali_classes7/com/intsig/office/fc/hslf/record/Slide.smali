.class public final Lcom/intsig/office/fc/hslf/record/Slide;
.super Lcom/intsig/office/fc/hslf/record/SheetContainer;
.source "Slide.java"


# static fields
.field private static _type:J = 0x3eeL


# instance fields
.field private _colorScheme:Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

.field private _header:[B

.field private headersFootersContainer:Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;

.field private ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

.field private propTagsContainer:Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;

.field private slideAtom:Lcom/intsig/office/fc/hslf/record/SlideAtom;

.field private ssSlideInfoAtom:Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 5

    .line 18
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/SheetContainer;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [B

    .line 19
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->_header:[B

    const/16 v1, 0xf

    const/4 v2, 0x0

    .line 20
    invoke-static {v0, v2, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    .line 21
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->_header:[B

    sget-wide v3, Lcom/intsig/office/fc/hslf/record/Slide;->_type:J

    long-to-int v1, v3

    const/4 v3, 0x2

    invoke-static {v0, v3, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    .line 22
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->_header:[B

    const/4 v1, 0x4

    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 23
    new-instance v0, Lcom/intsig/office/fc/hslf/record/SlideAtom;

    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/record/SlideAtom;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->slideAtom:Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 24
    new-instance v0, Lcom/intsig/office/fc/hslf/record/PPDrawing;

    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/record/PPDrawing;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 25
    new-instance v0, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/intsig/office/fc/hslf/record/Record;

    .line 26
    iget-object v4, p0, Lcom/intsig/office/fc/hslf/record/Slide;->slideAtom:Lcom/intsig/office/fc/hslf/record/SlideAtom;

    aput-object v4, v1, v2

    const/4 v2, 0x1

    iget-object v4, p0, Lcom/intsig/office/fc/hslf/record/Slide;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    aput-object v4, v1, v2

    aput-object v0, v1, v3

    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/SheetContainer;-><init>()V

    const/16 v0, 0x8

    new-array v1, v0, [B

    .line 2
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/Slide;->_header:[B

    const/4 v2, 0x0

    .line 3
    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, v0

    sub-int/2addr p3, v0

    .line 4
    invoke-static {p1, p2, p3}, Lcom/intsig/office/fc/hslf/record/Record;->findChildRecords([BII)[Lcom/intsig/office/fc/hslf/record/Record;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 5
    :goto_0
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    array-length p2, p1

    if-ge v2, p2, :cond_6

    .line 6
    aget-object p1, p1, v2

    instance-of p2, p1, Lcom/intsig/office/fc/hslf/record/SlideAtom;

    if-eqz p2, :cond_0

    .line 7
    move-object p2, p1

    check-cast p2, Lcom/intsig/office/fc/hslf/record/SlideAtom;

    iput-object p2, p0, Lcom/intsig/office/fc/hslf/record/Slide;->slideAtom:Lcom/intsig/office/fc/hslf/record/SlideAtom;

    goto :goto_1

    .line 8
    :cond_0
    instance-of p2, p1, Lcom/intsig/office/fc/hslf/record/PPDrawing;

    if-eqz p2, :cond_1

    .line 9
    move-object p2, p1

    check-cast p2, Lcom/intsig/office/fc/hslf/record/PPDrawing;

    iput-object p2, p0, Lcom/intsig/office/fc/hslf/record/Slide;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    goto :goto_1

    .line 10
    :cond_1
    instance-of p2, p1, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;

    if-eqz p2, :cond_2

    .line 11
    move-object p2, p1

    check-cast p2, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;

    iput-object p2, p0, Lcom/intsig/office/fc/hslf/record/Slide;->ssSlideInfoAtom:Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;

    goto :goto_1

    .line 12
    :cond_2
    instance-of p2, p1, Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;

    if-eqz p2, :cond_3

    .line 13
    move-object p2, p1

    check-cast p2, Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;

    iput-object p2, p0, Lcom/intsig/office/fc/hslf/record/Slide;->propTagsContainer:Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;

    goto :goto_1

    .line 14
    :cond_3
    instance-of p2, p1, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;

    if-eqz p2, :cond_4

    .line 15
    move-object p2, p1

    check-cast p2, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;

    iput-object p2, p0, Lcom/intsig/office/fc/hslf/record/Slide;->headersFootersContainer:Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;

    .line 16
    :cond_4
    :goto_1
    iget-object p2, p0, Lcom/intsig/office/fc/hslf/record/Slide;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    if-eqz p2, :cond_5

    instance-of p2, p1, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    if-eqz p2, :cond_5

    .line 17
    check-cast p1, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/Slide;->_colorScheme:Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_6
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->dispose()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->_header:[B

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Slide;->slideAtom:Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 8
    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->dispose()V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->slideAtom:Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 15
    .line 16
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Slide;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 17
    .line 18
    if-eqz v1, :cond_1

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->dispose()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 24
    .line 25
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Slide;->_colorScheme:Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 26
    .line 27
    if-eqz v1, :cond_2

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;->dispose()V

    .line 30
    .line 31
    .line 32
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->_colorScheme:Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 33
    .line 34
    :cond_2
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Slide;->ssSlideInfoAtom:Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;

    .line 35
    .line 36
    if-eqz v1, :cond_3

    .line 37
    .line 38
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->dispose()V

    .line 39
    .line 40
    .line 41
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->ssSlideInfoAtom:Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;

    .line 42
    .line 43
    :cond_3
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Slide;->propTagsContainer:Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;

    .line 44
    .line 45
    if-eqz v1, :cond_4

    .line 46
    .line 47
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->dispose()V

    .line 48
    .line 49
    .line 50
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->propTagsContainer:Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;

    .line 51
    .line 52
    :cond_4
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getColorScheme()Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->_colorScheme:Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHeadersFootersContainer()Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->headersFootersContainer:Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPPDrawing()Lcom/intsig/office/fc/hslf/record/PPDrawing;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/office/fc/hslf/record/Slide;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->slideAtom:Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideProgTagsContainer()Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->propTagsContainer:Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideShowSlideInfoAtom()Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Slide;->ssSlideInfoAtom:Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

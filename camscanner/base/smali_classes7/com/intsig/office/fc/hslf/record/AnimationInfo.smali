.class public final Lcom/intsig/office/fc/hslf/record/AnimationInfo;
.super Lcom/intsig/office/fc/hslf/record/RecordContainer;
.source "AnimationInfo.java"


# instance fields
.field private _header:[B

.field private animationAtom:Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;


# direct methods
.method public constructor <init>()V
    .locals 5

    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [B

    .line 7
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/AnimationInfo;->_header:[B

    const/16 v1, 0xf

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/record/AnimationInfo;->getRecordType()J

    move-result-wide v3

    long-to-int v1, v3

    int-to-short v1, v1

    const/4 v3, 0x2

    invoke-static {v0, v3, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/intsig/office/fc/hslf/record/Record;

    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 10
    new-instance v1, Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;

    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;-><init>()V

    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/AnimationInfo;->animationAtom:Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;

    aput-object v1, v0, v2

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;-><init>()V

    const/16 v0, 0x8

    new-array v1, v0, [B

    .line 2
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/AnimationInfo;->_header:[B

    const/4 v2, 0x0

    .line 3
    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, v0

    sub-int/2addr p3, v0

    .line 4
    invoke-static {p1, p2, p3}, Lcom/intsig/office/fc/hslf/record/Record;->findChildRecords([BII)[Lcom/intsig/office/fc/hslf/record/Record;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 5
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/AnimationInfo;->findInterestingChildren()V

    return-void
.end method

.method private findInterestingChildren()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    aget-object v0, v0, v1

    .line 5
    .line 6
    instance-of v1, v0, Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;

    .line 7
    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    check-cast v0, Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/AnimationInfo;->animationAtom:Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;

    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->dispose()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/AnimationInfo;->_header:[B

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/AnimationInfo;->animationAtom:Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;

    .line 8
    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;->dispose()V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/AnimationInfo;->animationAtom:Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;

    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAnimationInfoAtom()Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/AnimationInfo;->animationAtom:Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->AnimationInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 4
    .line 5
    int-to-long v0, v0

    .line 6
    return-wide v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public Lcom/intsig/office/fc/hslf/model/AutoShape;
.super Lcom/intsig/office/fc/hslf/model/TextShape;
.source "AutoShape.java"


# direct methods
.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    .line 4
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hslf/model/AutoShape;-><init>(ILcom/intsig/office/fc/hslf/model/Shape;)V

    return-void
.end method

.method public constructor <init>(ILcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0, p2}, Lcom/intsig/office/fc/hslf/model/TextShape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    .line 3
    instance-of p2, p2, Lcom/intsig/office/fc/hslf/model/ShapeGroup;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/AutoShape;->createSpContainer(IZ)Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/TextShape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    return-void
.end method


# virtual methods
.method protected createSpContainer(IZ)Lcom/intsig/office/fc/ddf/EscherContainerRecord;
    .locals 0

    .line 1
    invoke-super {p0, p2}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->createSpContainer(Z)Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    iput-object p2, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 6
    .line 7
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setShapeType(I)V

    .line 8
    .line 9
    .line 10
    const/16 p1, 0x7f

    .line 11
    .line 12
    const/high16 p2, 0x40000

    .line 13
    .line 14
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 15
    .line 16
    .line 17
    const/16 p1, 0x181

    .line 18
    .line 19
    const p2, 0x8000004

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 26
    .line 27
    .line 28
    const/16 p1, 0x183

    .line 29
    .line 30
    const/high16 p2, 0x8000000

    .line 31
    .line 32
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 33
    .line 34
    .line 35
    const/16 p1, 0x1bf

    .line 36
    .line 37
    const p2, 0x100010

    .line 38
    .line 39
    .line 40
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 41
    .line 42
    .line 43
    const/16 p1, 0x1c0

    .line 44
    .line 45
    const p2, 0x8000001

    .line 46
    .line 47
    .line 48
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 49
    .line 50
    .line 51
    const/16 p1, 0x1ff

    .line 52
    .line 53
    const p2, 0x80008

    .line 54
    .line 55
    .line 56
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 57
    .line 58
    .line 59
    const/16 p1, 0x201

    .line 60
    .line 61
    const p2, 0x8000002

    .line 62
    .line 63
    .line 64
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 65
    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 68
    .line 69
    return-object p1
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public dispose()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->dispose()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAdjustmentValue(I)I
    .locals 1

    .line 1
    if-ltz p1, :cond_0

    .line 2
    .line 3
    const/16 v0, 0x9

    .line 4
    .line 5
    if-gt p1, v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 8
    .line 9
    add-int/lit16 p1, p1, 0x147

    .line 10
    .line 11
    int-to-short p1, p1

    .line 12
    invoke-static {v0, p1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherContainerRecord;S)I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    return p1

    .line 17
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 18
    .line 19
    const-string v0, "The index of an adjustment value must be in the [0, 9] range"

    .line 20
    .line 21
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    throw p1
.end method

.method public getOutline()Lcom/intsig/office/java/awt/Shape;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeType()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/hslf/model/AutoShapes;->getShapeOutline(I)Lcom/intsig/office/fc/hslf/model/ShapeOutline;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getLogicalAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    return-object v1

    .line 16
    :cond_0
    invoke-interface {v0, p0}, Lcom/intsig/office/fc/hslf/model/ShapeOutline;->getOutline(Lcom/intsig/office/fc/hslf/model/Shape;)Lcom/intsig/office/java/awt/Shape;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/office/fc/hslf/model/AutoShapes;->transform(Lcom/intsig/office/java/awt/Shape;Lcom/intsig/office/java/awt/geom/Rectangle2D;)Lcom/intsig/office/java/awt/Shape;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    return-object v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public setAdjustmentValue(II)V
    .locals 1

    .line 1
    if-ltz p1, :cond_0

    .line 2
    .line 3
    const/16 v0, 0x9

    .line 4
    .line 5
    if-gt p1, v0, :cond_0

    .line 6
    .line 7
    add-int/lit16 p1, p1, 0x147

    .line 8
    .line 9
    int-to-short p1, p1

    .line 10
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 11
    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 15
    .line 16
    const-string p2, "The index of an adjustment value must be in the [0, 9] range"

    .line 17
    .line 18
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    throw p1
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected setDefaultTextProperties(Lcom/intsig/office/fc/hslf/model/TextRun;)V
    .locals 0

    .line 1
    const/4 p1, 0x1

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/model/TextShape;->setVerticalAlignment(I)V

    .line 3
    .line 4
    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/model/TextShape;->setHorizontalAlignment(I)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x2

    .line 9
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/model/TextShape;->setWordWrap(I)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

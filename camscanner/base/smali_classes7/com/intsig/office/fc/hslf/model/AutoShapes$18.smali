.class Lcom/intsig/office/fc/hslf/model/AutoShapes$18;
.super Ljava/lang/Object;
.source "AutoShapes.java"

# interfaces
.implements Lcom/intsig/office/fc/hslf/model/ShapeOutline;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hslf/model/AutoShapes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public getOutline(Lcom/intsig/office/fc/hslf/model/Shape;)Lcom/intsig/office/java/awt/Shape;
    .locals 22

    .line 1
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/16 v1, 0x147

    .line 6
    .line 7
    const/16 v2, 0x708

    .line 8
    .line 9
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherContainerRecord;SI)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const/16 v2, 0x148

    .line 18
    .line 19
    const/16 v3, 0x2a30

    .line 20
    .line 21
    invoke-static {v1, v2, v3}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherContainerRecord;SI)I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    new-instance v2, Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 26
    .line 27
    invoke-direct {v2}, Lcom/intsig/office/java/awt/geom/GeneralPath;-><init>()V

    .line 28
    .line 29
    .line 30
    const v3, 0x46a8c000    # 21600.0f

    .line 31
    .line 32
    .line 33
    const/4 v4, 0x0

    .line 34
    invoke-virtual {v2, v3, v4}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->moveTo(FF)V

    .line 35
    .line 36
    .line 37
    new-instance v3, Lcom/intsig/office/java/awt/geom/Arc2D$Float;

    .line 38
    .line 39
    const v6, 0x4628c000    # 10800.0f

    .line 40
    .line 41
    .line 42
    const/4 v7, 0x0

    .line 43
    const v8, 0x46a8c000    # 21600.0f

    .line 44
    .line 45
    .line 46
    mul-int/lit8 v13, v0, 0x2

    .line 47
    .line 48
    int-to-float v15, v13

    .line 49
    const/high16 v10, 0x42b40000    # 90.0f

    .line 50
    .line 51
    const/high16 v11, 0x42b40000    # 90.0f

    .line 52
    .line 53
    const/4 v12, 0x0

    .line 54
    move-object v5, v3

    .line 55
    move v9, v15

    .line 56
    invoke-direct/range {v5 .. v12}, Lcom/intsig/office/java/awt/geom/Arc2D$Float;-><init>(FFFFFFI)V

    .line 57
    .line 58
    .line 59
    const/4 v5, 0x0

    .line 60
    invoke-virtual {v2, v3, v5}, Lcom/intsig/office/java/awt/geom/Path2D;->append(Lcom/intsig/office/java/awt/Shape;Z)V

    .line 61
    .line 62
    .line 63
    int-to-float v3, v0

    .line 64
    invoke-virtual {v2, v6, v3}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->moveTo(FF)V

    .line 65
    .line 66
    .line 67
    sub-int v3, v1, v0

    .line 68
    .line 69
    int-to-float v3, v3

    .line 70
    invoke-virtual {v2, v6, v3}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->lineTo(FF)V

    .line 71
    .line 72
    .line 73
    new-instance v3, Lcom/intsig/office/java/awt/geom/Arc2D$Float;

    .line 74
    .line 75
    const v7, -0x39d74000    # -10800.0f

    .line 76
    .line 77
    .line 78
    sub-int v8, v1, v13

    .line 79
    .line 80
    int-to-float v8, v8

    .line 81
    const v17, 0x46a8c000    # 21600.0f

    .line 82
    .line 83
    .line 84
    const/high16 v19, 0x43870000    # 270.0f

    .line 85
    .line 86
    const/high16 v20, 0x42b40000    # 90.0f

    .line 87
    .line 88
    const/16 v21, 0x0

    .line 89
    .line 90
    move-object v14, v3

    .line 91
    move v15, v7

    .line 92
    move/from16 v16, v8

    .line 93
    .line 94
    move/from16 v18, v9

    .line 95
    .line 96
    invoke-direct/range {v14 .. v21}, Lcom/intsig/office/java/awt/geom/Arc2D$Float;-><init>(FFFFFFI)V

    .line 97
    .line 98
    .line 99
    invoke-virtual {v2, v3, v5}, Lcom/intsig/office/java/awt/geom/Path2D;->append(Lcom/intsig/office/java/awt/Shape;Z)V

    .line 100
    .line 101
    .line 102
    int-to-float v3, v1

    .line 103
    invoke-virtual {v2, v4, v3}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->moveTo(FF)V

    .line 104
    .line 105
    .line 106
    new-instance v4, Lcom/intsig/office/java/awt/geom/Arc2D$Float;

    .line 107
    .line 108
    const v15, -0x39d74000    # -10800.0f

    .line 109
    .line 110
    .line 111
    const/16 v19, 0x0

    .line 112
    .line 113
    move-object v14, v4

    .line 114
    move/from16 v16, v3

    .line 115
    .line 116
    invoke-direct/range {v14 .. v21}, Lcom/intsig/office/java/awt/geom/Arc2D$Float;-><init>(FFFFFFI)V

    .line 117
    .line 118
    .line 119
    invoke-virtual {v2, v4, v5}, Lcom/intsig/office/java/awt/geom/Path2D;->append(Lcom/intsig/office/java/awt/Shape;Z)V

    .line 120
    .line 121
    .line 122
    add-int/2addr v1, v0

    .line 123
    int-to-float v1, v1

    .line 124
    invoke-virtual {v2, v6, v1}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->moveTo(FF)V

    .line 125
    .line 126
    .line 127
    rsub-int v0, v0, 0x5460

    .line 128
    .line 129
    int-to-float v0, v0

    .line 130
    invoke-virtual {v2, v6, v0}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->lineTo(FF)V

    .line 131
    .line 132
    .line 133
    new-instance v0, Lcom/intsig/office/java/awt/geom/Arc2D$Float;

    .line 134
    .line 135
    const v15, 0x4628c000    # 10800.0f

    .line 136
    .line 137
    .line 138
    rsub-int v1, v13, 0x5460

    .line 139
    .line 140
    int-to-float v1, v1

    .line 141
    const/high16 v19, 0x43340000    # 180.0f

    .line 142
    .line 143
    move-object v14, v0

    .line 144
    move/from16 v16, v1

    .line 145
    .line 146
    invoke-direct/range {v14 .. v21}, Lcom/intsig/office/java/awt/geom/Arc2D$Float;-><init>(FFFFFFI)V

    .line 147
    .line 148
    .line 149
    invoke-virtual {v2, v0, v5}, Lcom/intsig/office/java/awt/geom/Path2D;->append(Lcom/intsig/office/java/awt/Shape;Z)V

    .line 150
    .line 151
    .line 152
    return-object v2
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

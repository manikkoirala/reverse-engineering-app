.class public final Lcom/intsig/office/fc/hslf/blip/EMF;
.super Lcom/intsig/office/fc/hslf/blip/Metafile;
.source "EMF.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/blip/Metafile;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public getData()[B
    .locals 5

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->getRawData()[B

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 8
    .line 9
    .line 10
    new-instance v2, Ljava/io/ByteArrayInputStream;

    .line 11
    .line 12
    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 13
    .line 14
    .line 15
    const-wide/16 v3, 0x8

    .line 16
    .line 17
    invoke-virtual {v2, v3, v4}, Ljava/io/InputStream;->skip(J)J

    .line 18
    .line 19
    .line 20
    new-instance v3, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;

    .line 21
    .line 22
    invoke-direct {v3}, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;-><init>()V

    .line 23
    .line 24
    .line 25
    const/16 v4, 0x10

    .line 26
    .line 27
    invoke-virtual {v3, v0, v4}, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->read([BI)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->getSize()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    add-int/2addr v0, v4

    .line 35
    int-to-long v3, v0

    .line 36
    invoke-virtual {v2, v3, v4}, Ljava/io/InputStream;->skip(J)J

    .line 37
    .line 38
    .line 39
    new-instance v0, Ljava/util/zip/InflaterInputStream;

    .line 40
    .line 41
    invoke-direct {v0, v2}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 42
    .line 43
    .line 44
    const/16 v2, 0x1000

    .line 45
    .line 46
    new-array v2, v2, [B

    .line 47
    .line 48
    :goto_0
    invoke-virtual {v0, v2}, Ljava/io/InputStream;->read([B)I

    .line 49
    .line 50
    .line 51
    move-result v3

    .line 52
    if-ltz v3, :cond_0

    .line 53
    .line 54
    const/4 v4, 0x0

    .line 55
    invoke-virtual {v1, v2, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_0
    invoke-virtual {v0}, Ljava/util/zip/InflaterInputStream;->close()V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 63
    .line 64
    .line 65
    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    return-object v0

    .line 67
    :catch_0
    move-exception v0

    .line 68
    new-instance v1, Lcom/intsig/office/fc/hslf/exceptions/HSLFException;

    .line 69
    .line 70
    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hslf/exceptions/HSLFException;-><init>(Ljava/lang/Throwable;)V

    .line 71
    .line 72
    .line 73
    throw v1
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getSignature()I
    .locals 1

    .line 1
    const/16 v0, 0x3d40

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getType()I
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setData([B)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    array-length v0, p1

    .line 2
    const/4 v1, 0x0

    .line 3
    invoke-virtual {p0, p1, v1, v0}, Lcom/intsig/office/fc/hslf/blip/Metafile;->compress([BII)[B

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    new-instance v2, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;

    .line 8
    .line 9
    invoke-direct {v2}, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;-><init>()V

    .line 10
    .line 11
    .line 12
    array-length v3, p1

    .line 13
    iput v3, v2, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->wmfsize:I

    .line 14
    .line 15
    new-instance v3, Lcom/intsig/office/java/awt/Rectangle;

    .line 16
    .line 17
    const/16 v4, 0xc8

    .line 18
    .line 19
    invoke-direct {v3, v1, v1, v4, v4}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    .line 20
    .line 21
    .line 22
    iput-object v3, v2, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 23
    .line 24
    new-instance v1, Lcom/intsig/office/java/awt/Dimension;

    .line 25
    .line 26
    iget-object v3, v2, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 27
    .line 28
    iget v4, v3, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 29
    .line 30
    mul-int/lit16 v4, v4, 0x319c

    .line 31
    .line 32
    iget v3, v3, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 33
    .line 34
    mul-int/lit16 v3, v3, 0x319c

    .line 35
    .line 36
    invoke-direct {v1, v4, v3}, Lcom/intsig/office/java/awt/Dimension;-><init>(II)V

    .line 37
    .line 38
    .line 39
    iput-object v1, v2, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->size:Lcom/intsig/office/java/awt/Dimension;

    .line 40
    .line 41
    array-length v1, v0

    .line 42
    iput v1, v2, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->zipsize:I

    .line 43
    .line 44
    invoke-static {p1}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->getChecksum([B)[B

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    .line 49
    .line 50
    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write([B)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->write(Ljava/io/OutputStream;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/usermodel/PictureData;->setRawData([B)V

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

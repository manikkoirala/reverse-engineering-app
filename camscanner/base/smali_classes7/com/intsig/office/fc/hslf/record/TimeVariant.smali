.class public Lcom/intsig/office/fc/hslf/record/TimeVariant;
.super Lcom/intsig/office/fc/hslf/record/PositionDependentRecordAtom;
.source "TimeVariant.java"


# static fields
.field public static final TPID_AfterEffect:B = 0xdt

.field public static final TPID_Display:B = 0x2t

.field public static final TPID_EffectDir:B = 0xat

.field public static final TPID_EffectType:B = 0xbt

.field public static final TPID_HideWhenStopped:B = 0x12t

.field public static final TPID_MasterPos:B = 0x5t

.field public static final TPID_MediaMute:B = 0x17t

.field public static final TPID_PlaceholderNode:B = 0x15t

.field public static final TPID_SlaveType:B = 0x6t

.field public static final TPID_SlideCount:B = 0xft

.field public static final TPID__EffectID:B = 0x9t

.field public static final TPID__EffectNodeType:B = 0x14t

.field public static final TPID__EventFilter:B = 0x11t

.field public static final TPID__GroupID:B = 0x13t

.field public static final TPID__MediaVolume:B = 0x16t

.field public static final TPID__TimeFilter:B = 0x10t

.field public static final TPID__ZoomToFullScreen:B = 0x1at

.field private static final TVT_Bool:B = 0x0t

.field private static final TVT_Int:B = 0x1t

.field private static final TVT_String:B = 0x3t

.field private static final TVT_TVT_Float:B = 0x2t

.field public static final TimeEffectType__ActionVerb:B = 0x5t

.field public static final TimeEffectType__Emphasis:B = 0x3t

.field public static final TimeEffectType__Entrance:B = 0x1t

.field public static final TimeEffectType__Exit:B = 0x2t

.field public static final TimeEffectType__MediaCommand:B = 0x6t

.field public static final TimeEffectType__MotionPath:B = 0x4t

.field private static _type:J = 0xf142L


# instance fields
.field private _header:[B

.field private tpID:I

.field private value:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>([BII)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/PositionDependentRecordAtom;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 p3, 0x8

    .line 5
    .line 6
    new-array v0, p3, [B

    .line 7
    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/TimeVariant;->_header:[B

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 12
    .line 13
    .line 14
    iget-object p3, p0, Lcom/intsig/office/fc/hslf/record/TimeVariant;->_header:[B

    .line 15
    .line 16
    invoke-static {p3, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 17
    .line 18
    .line 19
    move-result p3

    .line 20
    const v0, 0xfff0

    .line 21
    .line 22
    .line 23
    and-int/2addr p3, v0

    .line 24
    const/4 v0, 0x4

    .line 25
    shr-int/2addr p3, v0

    .line 26
    iput p3, p0, Lcom/intsig/office/fc/hslf/record/TimeVariant;->tpID:I

    .line 27
    .line 28
    add-int/lit8 p3, p2, 0x8

    .line 29
    .line 30
    aget-byte p3, p1, p3

    .line 31
    .line 32
    const/4 v2, 0x1

    .line 33
    if-eqz p3, :cond_3

    .line 34
    .line 35
    if-eq p3, v2, :cond_2

    .line 36
    .line 37
    const/4 v3, 0x2

    .line 38
    if-eq p3, v3, :cond_1

    .line 39
    .line 40
    const/4 v3, 0x3

    .line 41
    if-eq p3, v3, :cond_0

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_0
    iget-object p3, p0, Lcom/intsig/office/fc/hslf/record/TimeVariant;->_header:[B

    .line 45
    .line 46
    invoke-static {p3, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 47
    .line 48
    .line 49
    move-result p3

    .line 50
    sub-int/2addr p3, v2

    .line 51
    new-array v0, p3, [B

    .line 52
    .line 53
    add-int/lit8 p2, p2, 0x9

    .line 54
    .line 55
    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56
    .line 57
    .line 58
    invoke-static {v0}, Lcom/intsig/office/fc/util/StringUtil;->getFromUnicodeLE([B)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/TimeVariant;->value:Ljava/lang/Object;

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_1
    add-int/lit8 p2, p2, 0x9

    .line 66
    .line 67
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getFloat([BI)F

    .line 68
    .line 69
    .line 70
    move-result p1

    .line 71
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/TimeVariant;->value:Ljava/lang/Object;

    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_2
    add-int/lit8 p2, p2, 0x9

    .line 79
    .line 80
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 81
    .line 82
    .line 83
    move-result p1

    .line 84
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/TimeVariant;->value:Ljava/lang/Object;

    .line 89
    .line 90
    goto :goto_0

    .line 91
    :cond_3
    add-int/lit8 p2, p2, 0x9

    .line 92
    .line 93
    aget-byte p1, p1, p2

    .line 94
    .line 95
    if-ne p1, v2, :cond_4

    .line 96
    .line 97
    const/4 v1, 0x1

    .line 98
    :cond_4
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 99
    .line 100
    .line 101
    move-result-object p1

    .line 102
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/TimeVariant;->value:Ljava/lang/Object;

    .line 103
    .line 104
    :goto_0
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/TimeVariant;->_header:[B

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAttributeType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/record/TimeVariant;->tpID:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/office/fc/hslf/record/TimeVariant;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/TimeVariant;->value:Ljava/lang/Object;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public updateOtherRecordReferences(Ljava/util/Hashtable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public Lcom/intsig/office/fc/hslf/blip/Metafile$Header;
.super Ljava/lang/Object;
.source "Metafile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hslf/blip/Metafile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Header"
.end annotation


# instance fields
.field public bounds:Lcom/intsig/office/java/awt/Rectangle;

.field public compression:I

.field public filter:I

.field public size:Lcom/intsig/office/java/awt/Dimension;

.field public wmfsize:I

.field public zipsize:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0xfe

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->filter:I

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public getSize()I
    .locals 1

    .line 1
    const/16 v0, 0x22

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public read([BI)V
    .locals 5

    .line 1
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iput v0, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->wmfsize:I

    .line 6
    .line 7
    add-int/lit8 p2, p2, 0x4

    .line 8
    .line 9
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    add-int/lit8 p2, p2, 0x4

    .line 14
    .line 15
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    add-int/lit8 p2, p2, 0x4

    .line 20
    .line 21
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    add-int/lit8 p2, p2, 0x4

    .line 26
    .line 27
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    add-int/lit8 p2, p2, 0x4

    .line 32
    .line 33
    new-instance v4, Lcom/intsig/office/java/awt/Rectangle;

    .line 34
    .line 35
    sub-int/2addr v2, v0

    .line 36
    sub-int/2addr v3, v1

    .line 37
    invoke-direct {v4, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    .line 38
    .line 39
    .line 40
    iput-object v4, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 41
    .line 42
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    add-int/lit8 p2, p2, 0x4

    .line 47
    .line 48
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    add-int/lit8 p2, p2, 0x4

    .line 53
    .line 54
    new-instance v2, Lcom/intsig/office/java/awt/Dimension;

    .line 55
    .line 56
    invoke-direct {v2, v0, v1}, Lcom/intsig/office/java/awt/Dimension;-><init>(II)V

    .line 57
    .line 58
    .line 59
    iput-object v2, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->size:Lcom/intsig/office/java/awt/Dimension;

    .line 60
    .line 61
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    iput v0, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->zipsize:I

    .line 66
    .line 67
    add-int/lit8 p2, p2, 0x4

    .line 68
    .line 69
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getUnsignedByte([BI)I

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    iput v0, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->compression:I

    .line 74
    .line 75
    add-int/lit8 p2, p2, 0x1

    .line 76
    .line 77
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getUnsignedByte([BI)I

    .line 78
    .line 79
    .line 80
    move-result p1

    .line 81
    iput p1, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->filter:I

    .line 82
    .line 83
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/16 v0, 0x22

    .line 2
    .line 3
    new-array v0, v0, [B

    .line 4
    .line 5
    iget v1, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->wmfsize:I

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-static {v0, v2, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 12
    .line 13
    iget v1, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 14
    .line 15
    const/4 v3, 0x4

    .line 16
    invoke-static {v0, v3, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 17
    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 20
    .line 21
    iget v1, v1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 22
    .line 23
    const/16 v3, 0x8

    .line 24
    .line 25
    invoke-static {v0, v3, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 26
    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 29
    .line 30
    iget v3, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 31
    .line 32
    iget v1, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 33
    .line 34
    add-int/2addr v3, v1

    .line 35
    const/16 v1, 0xc

    .line 36
    .line 37
    invoke-static {v0, v1, v3}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 38
    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 41
    .line 42
    iget v3, v1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 43
    .line 44
    iget v1, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 45
    .line 46
    add-int/2addr v3, v1

    .line 47
    const/16 v1, 0x10

    .line 48
    .line 49
    invoke-static {v0, v1, v3}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 50
    .line 51
    .line 52
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->size:Lcom/intsig/office/java/awt/Dimension;

    .line 53
    .line 54
    iget v1, v1, Lcom/intsig/office/java/awt/Dimension;->width:I

    .line 55
    .line 56
    const/16 v3, 0x14

    .line 57
    .line 58
    invoke-static {v0, v3, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 59
    .line 60
    .line 61
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->size:Lcom/intsig/office/java/awt/Dimension;

    .line 62
    .line 63
    iget v1, v1, Lcom/intsig/office/java/awt/Dimension;->height:I

    .line 64
    .line 65
    const/16 v3, 0x18

    .line 66
    .line 67
    invoke-static {v0, v3, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 68
    .line 69
    .line 70
    const/16 v1, 0x1c

    .line 71
    .line 72
    iget v3, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->zipsize:I

    .line 73
    .line 74
    invoke-static {v0, v1, v3}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 75
    .line 76
    .line 77
    const/16 v1, 0x20

    .line 78
    .line 79
    aput-byte v2, v0, v1

    .line 80
    .line 81
    iget v1, p0, Lcom/intsig/office/fc/hslf/blip/Metafile$Header;->filter:I

    .line 82
    .line 83
    int-to-byte v1, v1

    .line 84
    const/16 v2, 0x21

    .line 85
    .line 86
    aput-byte v1, v0, v2

    .line 87
    .line 88
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 89
    .line 90
    .line 91
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.class public final Lcom/intsig/office/fc/hslf/record/TextBytesAtom;
.super Lcom/intsig/office/fc/hslf/record/RecordAtom;
.source "TextBytesAtom.java"


# static fields
.field private static _type:J = 0xfa8L


# instance fields
.field private _header:[B

.field private _text:[B


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 4

    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordAtom;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [B

    .line 7
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;->_header:[B

    const/4 v1, 0x0

    .line 8
    invoke-static {v0, v1, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    .line 9
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;->_header:[B

    sget-wide v2, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;->_type:J

    long-to-int v3, v2

    const/4 v2, 0x2

    invoke-static {v0, v2, v3}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    .line 10
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;->_header:[B

    const/4 v2, 0x4

    invoke-static {v0, v2, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    new-array v0, v1, [B

    .line 11
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;->_text:[B

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordAtom;-><init>()V

    const/16 v0, 0x8

    if-ge p3, v0, :cond_0

    const/16 p3, 0x8

    :cond_0
    new-array v1, v0, [B

    .line 2
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;->_header:[B

    const/4 v2, 0x0

    .line 3
    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sub-int/2addr p3, v0

    .line 4
    new-array v1, p3, [B

    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;->_text:[B

    add-int/2addr p2, v0

    .line 5
    invoke-static {p1, p2, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;->_header:[B

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;->_text:[B

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getText()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;->_text:[B

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    array-length v2, v0

    .line 5
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/util/StringUtil;->getFromCompressedUnicode([BII)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setText([B)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;->_text:[B

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;->_header:[B

    .line 4
    .line 5
    const/4 v1, 0x4

    .line 6
    array-length p1, p1

    .line 7
    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "TextBytesAtom:\n"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;->_text:[B

    .line 12
    .line 13
    const-wide/16 v2, 0x0

    .line 14
    .line 15
    const/4 v4, 0x0

    .line 16
    invoke-static {v1, v2, v3, v4}, Lcom/intsig/office/fc/util/HexDump;->dump([BJI)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    return-object v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.class public abstract Lcom/intsig/office/fc/hslf/model/Sheet;
.super Ljava/lang/Object;
.source "Sheet.java"


# instance fields
.field private _background:Lcom/intsig/office/fc/hslf/model/Background;

.field private _container:Lcom/intsig/office/fc/hslf/record/SheetContainer;

.field private _shapes:[Lcom/intsig/office/fc/hslf/model/Shape;

.field private _sheetNo:I

.field private _slideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hslf/record/SheetContainer;I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_container:Lcom/intsig/office/fc/hslf/record/SheetContainer;

    .line 5
    .line 6
    iput p2, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_sheetNo:I

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected static findTextRuns([Lcom/intsig/office/fc/hslf/record/Record;Ljava/util/Vector;)V
    .locals 9

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 17
    :goto_0
    array-length v2, p0

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 18
    aget-object v2, p0, v0

    instance-of v3, v2, Lcom/intsig/office/fc/hslf/record/TextHeaderAtom;

    if-eqz v3, :cond_8

    .line 19
    check-cast v2, Lcom/intsig/office/fc/hslf/record/TextHeaderAtom;

    .line 20
    array-length v3, p0

    add-int/lit8 v3, v3, -0x2

    const/4 v4, 0x0

    if-ge v0, v3, :cond_0

    add-int/lit8 v3, v0, 0x2

    .line 21
    aget-object v3, p0, v3

    instance-of v5, v3, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;

    if-eqz v5, :cond_0

    .line 22
    check-cast v3, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;

    goto :goto_1

    :cond_0
    move-object v3, v4

    :goto_1
    add-int/lit8 v5, v0, 0x1

    .line 23
    aget-object v6, p0, v5

    instance-of v7, v6, Lcom/intsig/office/fc/hslf/record/TextCharsAtom;

    if-eqz v7, :cond_1

    .line 24
    check-cast v6, Lcom/intsig/office/fc/hslf/record/TextCharsAtom;

    .line 25
    new-instance v4, Lcom/intsig/office/fc/hslf/model/TextRun;

    invoke-direct {v4, v2, v6, v3}, Lcom/intsig/office/fc/hslf/model/TextRun;-><init>(Lcom/intsig/office/fc/hslf/record/TextHeaderAtom;Lcom/intsig/office/fc/hslf/record/TextCharsAtom;Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;)V

    goto :goto_2

    .line 26
    :cond_1
    instance-of v7, v6, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;

    if-eqz v7, :cond_2

    .line 27
    check-cast v6, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;

    .line 28
    new-instance v4, Lcom/intsig/office/fc/hslf/model/TextRun;

    invoke-direct {v4, v2, v6, v3}, Lcom/intsig/office/fc/hslf/model/TextRun;-><init>(Lcom/intsig/office/fc/hslf/record/TextHeaderAtom;Lcom/intsig/office/fc/hslf/record/TextBytesAtom;Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;)V

    goto :goto_2

    .line 29
    :cond_2
    invoke-virtual {v6}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    move-result-wide v2

    const-wide/16 v6, 0xfa1

    cmp-long v8, v2, v6

    if-nez v8, :cond_3

    goto :goto_2

    .line 30
    :cond_3
    aget-object v2, p0, v5

    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    move-result-wide v2

    const-wide/16 v6, 0xfaa

    cmp-long v8, v2, v6

    if-nez v8, :cond_4

    goto :goto_2

    .line 31
    :cond_4
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Found a TextHeaderAtom not followed by a TextBytesAtom or TextCharsAtom: Followed by "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v6, p0, v5

    .line 32
    invoke-virtual {v6}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 33
    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_2
    if-eqz v4, :cond_7

    .line 34
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v3, v0

    .line 35
    :goto_3
    array-length v6, p0

    if-ge v3, v6, :cond_6

    if-le v3, v0, :cond_5

    .line 36
    aget-object v6, p0, v3

    instance-of v6, v6, Lcom/intsig/office/fc/hslf/record/TextHeaderAtom;

    if-eqz v6, :cond_5

    goto :goto_4

    .line 37
    :cond_5
    aget-object v6, p0, v3

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 38
    :cond_6
    :goto_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/intsig/office/fc/hslf/record/Record;

    .line 39
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 40
    iput-object v0, v4, Lcom/intsig/office/fc/hslf/model/TextRun;->_records:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 41
    invoke-virtual {v4, v1}, Lcom/intsig/office/fc/hslf/model/TextRun;->setIndex(I)V

    .line 42
    invoke-virtual {p1, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move v0, v5

    :cond_7
    add-int/lit8 v1, v1, 0x1

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_9
    return-void
.end method

.method public static findTextRuns(Lcom/intsig/office/fc/hslf/record/PPDrawing;)[Lcom/intsig/office/fc/hslf/model/TextRun;
    .locals 10

    .line 1
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->getTextboxWrappers()[Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    move-result-object p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 3
    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_4

    .line 4
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    .line 5
    aget-object v4, p0, v2

    invoke-static {v4}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->handleParentAwareRecords(Lcom/intsig/office/fc/hslf/record/RecordContainer;)V

    .line 6
    aget-object v4, p0, v2

    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/intsig/office/fc/hslf/model/Sheet;->findTextRuns([Lcom/intsig/office/fc/hslf/record/Record;Ljava/util/Vector;)V

    .line 7
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v4

    if-eq v4, v3, :cond_3

    .line 8
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    const/4 v4, 0x1

    sub-int/2addr v3, v4

    invoke-virtual {v0, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 9
    aget-object v5, p0, v2

    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->getShapeId()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/intsig/office/fc/hslf/model/TextRun;->setShapeId(I)V

    add-int/lit8 v5, v2, -0x1

    const/4 v6, 0x0

    :goto_1
    if-ltz v5, :cond_3

    .line 10
    aget-object v7, p0, v5

    invoke-virtual {v7}, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->getShapeId()I

    move-result v7

    const/16 v8, 0x138b

    if-ne v7, v8, :cond_1

    .line 11
    aget-object v7, p0, v5

    invoke-virtual {v7}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    move-result-object v7

    const/4 v8, 0x0

    .line 12
    :goto_2
    array-length v9, v7

    if-ge v8, v9, :cond_1

    .line 13
    aget-object v9, v7, v1

    instance-of v9, v9, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;

    if-eqz v9, :cond_0

    .line 14
    aget-object v6, v7, v8

    check-cast v6, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;

    invoke-virtual {v3, v6}, Lcom/intsig/office/fc/hslf/model/TextRun;->setExtendedParagraphAtom(Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;)V

    const/4 v6, 0x1

    goto :goto_3

    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_1
    :goto_3
    if-eqz v6, :cond_2

    goto :goto_4

    :cond_2
    add-int/lit8 v5, v5, -0x1

    goto :goto_1

    :cond_3
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 15
    :cond_4
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result p0

    new-array v2, p0, [Lcom/intsig/office/fc/hslf/model/TextRun;

    :goto_5
    if-ge v1, p0, :cond_5

    .line 16
    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/intsig/office/fc/hslf/model/TextRun;

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    return-object v2
.end method


# virtual methods
.method public _getSheetNumber()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_sheetNo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public _getSheetRefId()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_container:Lcom/intsig/office/fc/hslf/record/SheetContainer;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/PositionDependentRecordContainer;->getSheetId()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public addShape(Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getPPDrawing()Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->getEscherRecords()[Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    aget-object v0, v0, v1

    .line 11
    .line 12
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 13
    .line 14
    const/16 v1, -0xffd

    .line 15
    .line 16
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hslf/model/Shape;->setSheet(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->allocateShapeId()I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hslf/model/Shape;->setShapeId(I)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hslf/model/Shape;->afterInsert(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public allocateShapeId()I
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_slideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getDocumentRecord()Lcom/intsig/office/fc/hslf/record/Document;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/Document;->getPPDrawingGroup()Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;->getEscherDggRecord()Lcom/intsig/office/fc/ddf/EscherDggRecord;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_container:Lcom/intsig/office/fc/hslf/record/SheetContainer;

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/SheetContainer;->getPPDrawing()Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->getEscherDgRecord()Lcom/intsig/office/fc/ddf/EscherDgRecord;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getNumShapesSaved()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    add-int/lit8 v2, v2, 0x1

    .line 30
    .line 31
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->setNumShapesSaved(I)V

    .line 32
    .line 33
    .line 34
    const/4 v2, 0x0

    .line 35
    const/4 v3, 0x0

    .line 36
    :goto_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getFileIdClusters()[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 37
    .line 38
    .line 39
    move-result-object v4

    .line 40
    array-length v4, v4

    .line 41
    const/16 v5, 0x400

    .line 42
    .line 43
    if-ge v3, v4, :cond_2

    .line 44
    .line 45
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getFileIdClusters()[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 46
    .line 47
    .line 48
    move-result-object v4

    .line 49
    aget-object v4, v4, v3

    .line 50
    .line 51
    invoke-virtual {v4}, Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;->getDrawingGroupId()I

    .line 52
    .line 53
    .line 54
    move-result v6

    .line 55
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherDgRecord;->getDrawingGroupId()S

    .line 56
    .line 57
    .line 58
    move-result v7

    .line 59
    if-ne v6, v7, :cond_1

    .line 60
    .line 61
    invoke-virtual {v4}, Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;->getNumShapeIdsUsed()I

    .line 62
    .line 63
    .line 64
    move-result v6

    .line 65
    if-eq v6, v5, :cond_1

    .line 66
    .line 67
    invoke-virtual {v4}, Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;->getNumShapeIdsUsed()I

    .line 68
    .line 69
    .line 70
    move-result v2

    .line 71
    add-int/lit8 v3, v3, 0x1

    .line 72
    .line 73
    mul-int/lit16 v3, v3, 0x400

    .line 74
    .line 75
    add-int/2addr v2, v3

    .line 76
    invoke-virtual {v4}, Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;->incrementShapeId()V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherDgRecord;->getNumShapes()I

    .line 80
    .line 81
    .line 82
    move-result v3

    .line 83
    add-int/lit8 v3, v3, 0x1

    .line 84
    .line 85
    invoke-virtual {v1, v3}, Lcom/intsig/office/fc/ddf/EscherDgRecord;->setNumShapes(I)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/EscherDgRecord;->setLastMSOSPID(I)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getShapeIdMax()I

    .line 92
    .line 93
    .line 94
    move-result v1

    .line 95
    if-lt v2, v1, :cond_0

    .line 96
    .line 97
    add-int/lit8 v1, v2, 0x1

    .line 98
    .line 99
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->setShapeIdMax(I)V

    .line 100
    .line 101
    .line 102
    :cond_0
    return v2

    .line 103
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 104
    .line 105
    goto :goto_0

    .line 106
    :cond_2
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherDgRecord;->getDrawingGroupId()S

    .line 107
    .line 108
    .line 109
    move-result v3

    .line 110
    invoke-virtual {v0, v3, v2, v2}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->addCluster(IIZ)V

    .line 111
    .line 112
    .line 113
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getFileIdClusters()[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 114
    .line 115
    .line 116
    move-result-object v2

    .line 117
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getFileIdClusters()[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 118
    .line 119
    .line 120
    move-result-object v3

    .line 121
    array-length v3, v3

    .line 122
    add-int/lit8 v3, v3, -0x1

    .line 123
    .line 124
    aget-object v2, v2, v3

    .line 125
    .line 126
    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;->incrementShapeId()V

    .line 127
    .line 128
    .line 129
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherDgRecord;->getNumShapes()I

    .line 130
    .line 131
    .line 132
    move-result v2

    .line 133
    add-int/lit8 v2, v2, 0x1

    .line 134
    .line 135
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/EscherDgRecord;->setNumShapes(I)V

    .line 136
    .line 137
    .line 138
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getFileIdClusters()[Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    .line 139
    .line 140
    .line 141
    move-result-object v2

    .line 142
    array-length v2, v2

    .line 143
    mul-int/lit16 v2, v2, 0x400

    .line 144
    .line 145
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/EscherDgRecord;->setLastMSOSPID(I)V

    .line 146
    .line 147
    .line 148
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getShapeIdMax()I

    .line 149
    .line 150
    .line 151
    move-result v1

    .line 152
    if-lt v2, v1, :cond_3

    .line 153
    .line 154
    add-int/lit8 v1, v2, 0x1

    .line 155
    .line 156
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->setShapeIdMax(I)V

    .line 157
    .line 158
    .line 159
    :cond_3
    return v2
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public dispose()V
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_slideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_background:Lcom/intsig/office/fc/hslf/model/Background;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/Shape;->dispose()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_background:Lcom/intsig/office/fc/hslf/model/Background;

    .line 12
    .line 13
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_shapes:[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 14
    .line 15
    if-eqz v1, :cond_2

    .line 16
    .line 17
    array-length v2, v1

    .line 18
    const/4 v3, 0x0

    .line 19
    :goto_0
    if-ge v3, v2, :cond_1

    .line 20
    .line 21
    aget-object v4, v1, v3

    .line 22
    .line 23
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/model/Shape;->dispose()V

    .line 24
    .line 25
    .line 26
    add-int/lit8 v3, v3, 0x1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_shapes:[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 30
    .line 31
    :cond_2
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_container:Lcom/intsig/office/fc/hslf/record/SheetContainer;

    .line 32
    .line 33
    if-eqz v1, :cond_3

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->dispose()V

    .line 36
    .line 37
    .line 38
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_container:Lcom/intsig/office/fc/hslf/record/SheetContainer;

    .line 39
    .line 40
    :cond_3
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getBackground()Lcom/intsig/office/fc/hslf/model/Background;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_background:Lcom/intsig/office/fc/hslf/model/Background;

    .line 2
    .line 3
    if-nez v0, :cond_2

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getPPDrawing()Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->getEscherRecords()[Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const/4 v1, 0x0

    .line 14
    aget-object v0, v0, v1

    .line 15
    .line 16
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    const/4 v2, 0x0

    .line 27
    if-eqz v1, :cond_1

    .line 28
    .line 29
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    const/16 v4, -0xffc

    .line 40
    .line 41
    if-ne v3, v4, :cond_0

    .line 42
    .line 43
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_1
    move-object v1, v2

    .line 47
    :goto_0
    if-eqz v1, :cond_2

    .line 48
    .line 49
    new-instance v0, Lcom/intsig/office/fc/hslf/model/Background;

    .line 50
    .line 51
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/hslf/model/Background;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    .line 52
    .line 53
    .line 54
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_background:Lcom/intsig/office/fc/hslf/model/Background;

    .line 55
    .line 56
    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/hslf/model/Shape;->setSheet(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    .line 57
    .line 58
    .line 59
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_background:Lcom/intsig/office/fc/hslf/model/Background;

    .line 60
    .line 61
    return-object v0
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getColorScheme()Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_container:Lcom/intsig/office/fc/hslf/record/SheetContainer;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/SheetContainer;->getColorScheme()Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public abstract getMasterSheet()Lcom/intsig/office/fc/hslf/model/MasterSheet;
.end method

.method protected getPPDrawing()Lcom/intsig/office/fc/hslf/record/PPDrawing;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_container:Lcom/intsig/office/fc/hslf/record/SheetContainer;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/SheetContainer;->getPPDrawing()Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPlaceholder(I)Lcom/intsig/office/fc/hslf/model/TextShape;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getShapes()[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    :goto_0
    array-length v3, v0

    .line 8
    if-ge v2, v3, :cond_3

    .line 9
    .line 10
    aget-object v3, v0, v2

    .line 11
    .line 12
    instance-of v4, v3, Lcom/intsig/office/fc/hslf/model/TextShape;

    .line 13
    .line 14
    if-eqz v4, :cond_2

    .line 15
    .line 16
    check-cast v3, Lcom/intsig/office/fc/hslf/model/TextShape;

    .line 17
    .line 18
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/TextShape;->getPlaceholderAtom()Lcom/intsig/office/fc/hslf/record/OEPlaceholderAtom;

    .line 19
    .line 20
    .line 21
    move-result-object v4

    .line 22
    if-eqz v4, :cond_0

    .line 23
    .line 24
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/OEPlaceholderAtom;->getPlaceholderId()I

    .line 25
    .line 26
    .line 27
    move-result v4

    .line 28
    goto :goto_1

    .line 29
    :cond_0
    sget-object v4, Lcom/intsig/office/fc/hslf/record/RecordTypes;->RoundTripHFPlaceholder12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 30
    .line 31
    iget v4, v4, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 32
    .line 33
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getClientDataRecord(I)Lcom/intsig/office/fc/hslf/record/Record;

    .line 34
    .line 35
    .line 36
    move-result-object v4

    .line 37
    check-cast v4, Lcom/intsig/office/fc/hslf/record/RoundTripHFPlaceholder12;

    .line 38
    .line 39
    if-eqz v4, :cond_1

    .line 40
    .line 41
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/RoundTripHFPlaceholder12;->getPlaceholderId()I

    .line 42
    .line 43
    .line 44
    move-result v4

    .line 45
    goto :goto_1

    .line 46
    :cond_1
    const/4 v4, 0x0

    .line 47
    :goto_1
    if-ne v4, p1, :cond_2

    .line 48
    .line 49
    return-object v3

    .line 50
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_3
    const/4 p1, 0x0

    .line 54
    return-object p1
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getPlaceholderByTextType(I)Lcom/intsig/office/fc/hslf/model/TextShape;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getShapes()[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    :goto_0
    array-length v2, v0

    .line 7
    if-ge v1, v2, :cond_1

    .line 8
    .line 9
    aget-object v2, v0, v1

    .line 10
    .line 11
    instance-of v3, v2, Lcom/intsig/office/fc/hslf/model/TextShape;

    .line 12
    .line 13
    if-eqz v3, :cond_0

    .line 14
    .line 15
    check-cast v2, Lcom/intsig/office/fc/hslf/model/TextShape;

    .line 16
    .line 17
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    if-eqz v3, :cond_0

    .line 22
    .line 23
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRunType()I

    .line 24
    .line 25
    .line 26
    move-result v3

    .line 27
    if-ne v3, p1, :cond_0

    .line 28
    .line 29
    return-object v2

    .line 30
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    const/4 p1, 0x0

    .line 34
    return-object p1
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getProgrammableTag()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSheetContainer()Lcom/intsig/office/fc/hslf/record/SheetContainer;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sget-object v1, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlideProgTagsContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 6
    .line 7
    iget v1, v1, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 8
    .line 9
    int-to-long v1, v1

    .line 10
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    check-cast v0, Lcom/intsig/office/fc/hslf/record/RecordContainer;

    .line 15
    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    sget-object v1, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlideProgBinaryTagContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 19
    .line 20
    iget v1, v1, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 21
    .line 22
    int-to-long v1, v1

    .line 23
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    check-cast v0, Lcom/intsig/office/fc/hslf/record/RecordContainer;

    .line 28
    .line 29
    if-eqz v0, :cond_0

    .line 30
    .line 31
    sget-object v1, Lcom/intsig/office/fc/hslf/record/RecordTypes;->CString:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 32
    .line 33
    iget v1, v1, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 34
    .line 35
    int-to-long v1, v1

    .line 36
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    check-cast v0, Lcom/intsig/office/fc/hslf/record/CString;

    .line 41
    .line 42
    if-eqz v0, :cond_0

    .line 43
    .line 44
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/CString;->getText()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    goto :goto_0

    .line 49
    :cond_0
    const/4 v0, 0x0

    .line 50
    :goto_0
    return-object v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getShapes()[Lcom/intsig/office/fc/hslf/model/Shape;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_shapes:[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getPPDrawing()Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->getEscherRecords()[Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const/4 v1, 0x0

    .line 15
    aget-object v0, v0, v1

    .line 16
    .line 17
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    const/4 v2, 0x0

    .line 28
    if-eqz v1, :cond_2

    .line 29
    .line 30
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 35
    .line 36
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    const/16 v4, -0xffd

    .line 41
    .line 42
    if-ne v3, v4, :cond_1

    .line 43
    .line 44
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_2
    move-object v1, v2

    .line 48
    :goto_0
    if-eqz v1, :cond_5

    .line 49
    .line 50
    new-instance v0, Ljava/util/ArrayList;

    .line 51
    .line 52
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 60
    .line 61
    .line 62
    move-result v3

    .line 63
    if-eqz v3, :cond_3

    .line 64
    .line 65
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 66
    .line 67
    .line 68
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 69
    .line 70
    .line 71
    move-result v3

    .line 72
    if-eqz v3, :cond_4

    .line 73
    .line 74
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 75
    .line 76
    .line 77
    move-result-object v3

    .line 78
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 79
    .line 80
    invoke-static {v3, v2}, Lcom/intsig/office/fc/hslf/model/ShapeFactory;->createShape(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)Lcom/intsig/office/fc/hslf/model/Shape;

    .line 81
    .line 82
    .line 83
    move-result-object v3

    .line 84
    invoke-virtual {v3, p0}, Lcom/intsig/office/fc/hslf/model/Shape;->setSheet(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    .line 85
    .line 86
    .line 87
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    .line 89
    .line 90
    goto :goto_1

    .line 91
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 92
    .line 93
    .line 94
    move-result v1

    .line 95
    new-array v1, v1, [Lcom/intsig/office/fc/hslf/model/Shape;

    .line 96
    .line 97
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_shapes:[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 98
    .line 99
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 100
    .line 101
    .line 102
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_shapes:[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 103
    .line 104
    return-object v0

    .line 105
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 106
    .line 107
    const-string v1, "spgr not found"

    .line 108
    .line 109
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    throw v0
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public getSheetContainer()Lcom/intsig/office/fc/hslf/record/SheetContainer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_container:Lcom/intsig/office/fc/hslf/record/SheetContainer;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideShow()Lcom/intsig/office/fc/hslf/usermodel/SlideShow;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_slideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public abstract getTextRuns()[Lcom/intsig/office/fc/hslf/model/TextRun;
.end method

.method protected onAddTextShape(Lcom/intsig/office/fc/hslf/model/TextShape;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onCreate()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeShape(Lcom/intsig/office/fc/hslf/model/Shape;)Z
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getPPDrawing()Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->getEscherRecords()[Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    aget-object v0, v0, v1

    .line 11
    .line 12
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_1

    .line 23
    .line 24
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 29
    .line 30
    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    const/16 v4, -0xffd

    .line 35
    .line 36
    if-ne v3, v4, :cond_0

    .line 37
    .line 38
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    const/4 v2, 0x0

    .line 42
    :goto_0
    if-nez v2, :cond_2

    .line 43
    .line 44
    return v1

    .line 45
    :cond_2
    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 54
    .line 55
    .line 56
    move-result p1

    .line 57
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->setChildRecords(Ljava/util/List;)V

    .line 58
    .line 59
    .line 60
    return p1
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setSlideShow(Lcom/intsig/office/fc/hslf/usermodel/SlideShow;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_slideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getTextRuns()[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    :goto_0
    array-length v1, p1

    .line 11
    if-ge v0, v1, :cond_0

    .line 12
    .line 13
    aget-object v1, p1, v0

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/model/Sheet;->_slideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hslf/model/TextRun;->supplySlideShow(Lcom/intsig/office/fc/hslf/usermodel/SlideShow;)V

    .line 18
    .line 19
    .line 20
    add-int/lit8 v0, v0, 0x1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    return-void
    .line 24
.end method

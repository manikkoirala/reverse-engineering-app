.class public final Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;
.super Ljava/lang/Object;
.source "RichTextRun.java"


# instance fields
.field private _fontname:Ljava/lang/String;

.field private characterStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

.field private length:I

.field private paragraphStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

.field private parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

.field private sharingCharacterStyle:Z

.field private sharingParagraphStyle:Z

.field private slideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

.field private startPos:I


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hslf/model/TextRun;II)V
    .locals 8

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    .line 1
    invoke-direct/range {v0 .. v7}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;-><init>(Lcom/intsig/office/fc/hslf/model/TextRun;IILcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;ZZ)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hslf/model/TextRun;IILcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;ZZ)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 4
    iput p2, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->startPos:I

    .line 5
    iput p3, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->length:I

    .line 6
    iput-object p4, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->paragraphStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 7
    iput-object p5, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->characterStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 8
    iput-boolean p6, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->sharingParagraphStyle:Z

    .line 9
    iput-boolean p7, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->sharingCharacterStyle:Z

    return-void
.end method

.method private fetchOrAddTextProp(Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;Ljava/lang/String;)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;
    .locals 1

    .line 1
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->findByName(Ljava/lang/String;)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->addWithName(Ljava/lang/String;)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    :cond_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private getCharTextPropVal(Ljava/lang/String;)I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->characterStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->findByName(Ljava/lang/String;)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    if-nez v0, :cond_1

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/TextRun;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 20
    .line 21
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRunType()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/Sheet;->getMasterSheet()Lcom/intsig/office/fc/hslf/model/MasterSheet;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    if-eqz v1, :cond_1

    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getIndentLevel()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    const/4 v3, 0x1

    .line 36
    invoke-virtual {v1, v2, v0, p1, v3}, Lcom/intsig/office/fc/hslf/model/MasterSheet;->getStyleAttribute(IILjava/lang/String;Z)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    :cond_1
    if-nez v0, :cond_2

    .line 41
    .line 42
    const-string v1, "font.color"

    .line 43
    .line 44
    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    if-eqz p1, :cond_2

    .line 49
    .line 50
    sget-object p1, Lcom/intsig/office/java/awt/Color;->BLACK:Lcom/intsig/office/java/awt/Color;

    .line 51
    .line 52
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    return p1

    .line 57
    :cond_2
    if-nez v0, :cond_3

    .line 58
    .line 59
    const/4 p1, -0x1

    .line 60
    goto :goto_1

    .line 61
    :cond_3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->getValue()I

    .line 62
    .line 63
    .line 64
    move-result p1

    .line 65
    :goto_1
    return p1
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private getFlag(ZI)Z
    .locals 4

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->characterStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 4
    .line 5
    sget-object v1, Lcom/intsig/office/fc/hslf/model/textproperties/CharFlagsTextProp;->NAME:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->paragraphStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 9
    .line 10
    sget-object v1, Lcom/intsig/office/fc/hslf/model/textproperties/ParagraphFlagsTextProp;->NAME:Ljava/lang/String;

    .line 11
    .line 12
    :goto_0
    if-eqz v0, :cond_1

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->findByName(Ljava/lang/String;)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    check-cast v0, Lcom/intsig/office/fc/hslf/model/textproperties/BitMaskTextProp;

    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_1
    const/4 v0, 0x0

    .line 22
    :goto_1
    if-nez v0, :cond_2

    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 25
    .line 26
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/TextRun;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    if-eqz v2, :cond_2

    .line 31
    .line 32
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 33
    .line 34
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRunType()I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Sheet;->getMasterSheet()Lcom/intsig/office/fc/hslf/model/MasterSheet;

    .line 39
    .line 40
    .line 41
    move-result-object v2

    .line 42
    if-eqz v2, :cond_2

    .line 43
    .line 44
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getIndentLevel()I

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    invoke-virtual {v2, v3, v0, v1, p1}, Lcom/intsig/office/fc/hslf/model/MasterSheet;->getStyleAttribute(IILjava/lang/String;Z)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    move-object v0, p1

    .line 53
    check-cast v0, Lcom/intsig/office/fc/hslf/model/textproperties/BitMaskTextProp;

    .line 54
    .line 55
    :cond_2
    if-nez v0, :cond_3

    .line 56
    .line 57
    const/4 p1, 0x0

    .line 58
    goto :goto_2

    .line 59
    :cond_3
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hslf/model/textproperties/BitMaskTextProp;->getSubValue(I)Z

    .line 60
    .line 61
    .line 62
    move-result p1

    .line 63
    :goto_2
    return p1
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private getParaTextPropVal(Ljava/lang/String;)I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->paragraphStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->findByName(Ljava/lang/String;)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    if-nez v0, :cond_1

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/TextRun;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 20
    .line 21
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRunType()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/Sheet;->getMasterSheet()Lcom/intsig/office/fc/hslf/model/MasterSheet;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    if-eqz v1, :cond_1

    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getIndentLevel()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    const/4 v3, 0x0

    .line 36
    invoke-virtual {v1, v2, v0, p1, v3}, Lcom/intsig/office/fc/hslf/model/MasterSheet;->getStyleAttribute(IILjava/lang/String;Z)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    :cond_1
    if-nez v0, :cond_2

    .line 41
    .line 42
    const/4 p1, -0x1

    .line 43
    goto :goto_1

    .line 44
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->getValue()I

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    :goto_1
    return p1
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private isCharFlagsTextPropVal(I)Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getFlag(ZI)Z

    .line 3
    .line 4
    .line 5
    move-result p1

    .line 6
    return p1
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private setCharFlagsTextPropVal(IZ)V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getFlag(ZI)Z

    .line 3
    .line 4
    .line 5
    move-result v1

    .line 6
    if-eq v1, p2, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0, v0, p1, p2}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setFlag(ZIZ)V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public _getRawCharacterStyle()Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->characterStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public _getRawParagraphStyle()Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->paragraphStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public _isCharacterStyleShared()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->sharingCharacterStyle:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public _isParagraphStyleShared()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->sharingParagraphStyle:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->slideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->_fontname:Ljava/lang/String;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->paragraphStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 9
    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->dispose()V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->paragraphStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 16
    .line 17
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->characterStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 18
    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->dispose()V

    .line 22
    .line 23
    .line 24
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->characterStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 25
    .line 26
    :cond_1
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getAlignment()I
    .locals 1

    .line 1
    const-string v0, "alignment"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBulletChar()C
    .locals 1

    .line 1
    const-string v0, "bullet.char"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-char v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBulletColor()Lcom/intsig/office/java/awt/Color;
    .locals 4

    .line 1
    const-string v0, "bullet.color"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, -0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getFontColor()Lcom/intsig/office/java/awt/Color;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    return-object v0

    .line 15
    :cond_0
    shr-int/lit8 v1, v0, 0x18

    .line 16
    .line 17
    const/high16 v2, 0x1000000

    .line 18
    .line 19
    rem-int v2, v0, v2

    .line 20
    .line 21
    if-nez v2, :cond_1

    .line 22
    .line 23
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 24
    .line 25
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/TextRun;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Sheet;->getColorScheme()Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    if-ltz v1, :cond_1

    .line 34
    .line 35
    const/4 v3, 0x7

    .line 36
    if-gt v1, v3, :cond_1

    .line 37
    .line 38
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;->getColor(I)I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    :cond_1
    new-instance v1, Lcom/intsig/office/java/awt/Color;

    .line 43
    .line 44
    const/4 v2, 0x1

    .line 45
    invoke-direct {v1, v0, v2}, Lcom/intsig/office/java/awt/Color;-><init>(IZ)V

    .line 46
    .line 47
    .line 48
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 49
    .line 50
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Color;->getBlue()I

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Color;->getGreen()I

    .line 55
    .line 56
    .line 57
    move-result v3

    .line 58
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Color;->getRed()I

    .line 59
    .line 60
    .line 61
    move-result v1

    .line 62
    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 63
    .line 64
    .line 65
    return-object v0
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getBulletFont()I
    .locals 1

    .line 1
    const-string v0, "bullet.font"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBulletOffset()I
    .locals 2

    .line 1
    const-string v0, "bullet.offset"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-float v0, v0

    .line 8
    const/high16 v1, 0x42900000    # 72.0f

    .line 9
    .line 10
    mul-float v0, v0, v1

    .line 11
    .line 12
    const/high16 v1, 0x44100000    # 576.0f

    .line 13
    .line 14
    div-float/2addr v0, v1

    .line 15
    float-to-int v0, v0

    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBulletSize()I
    .locals 1

    .line 1
    const-string v0, "bullet.size"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEndIndex()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->startPos:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->length:I

    .line 4
    .line 5
    add-int/2addr v0, v1

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFontColor()Lcom/intsig/office/java/awt/Color;
    .locals 4

    .line 1
    const-string v0, "font.color"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getCharTextPropVal(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    shr-int/lit8 v1, v0, 0x18

    .line 8
    .line 9
    const/high16 v2, 0x1000000

    .line 10
    .line 11
    rem-int v2, v0, v2

    .line 12
    .line 13
    if-nez v2, :cond_0

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 16
    .line 17
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/TextRun;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Sheet;->getColorScheme()Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    if-ltz v1, :cond_0

    .line 26
    .line 27
    const/4 v3, 0x7

    .line 28
    if-gt v1, v3, :cond_0

    .line 29
    .line 30
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;->getColor(I)I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    :cond_0
    new-instance v1, Lcom/intsig/office/java/awt/Color;

    .line 35
    .line 36
    const/4 v2, 0x1

    .line 37
    invoke-direct {v1, v0, v2}, Lcom/intsig/office/java/awt/Color;-><init>(IZ)V

    .line 38
    .line 39
    .line 40
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 41
    .line 42
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Color;->getBlue()I

    .line 43
    .line 44
    .line 45
    move-result v2

    .line 46
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Color;->getGreen()I

    .line 47
    .line 48
    .line 49
    move-result v3

    .line 50
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Color;->getRed()I

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 55
    .line 56
    .line 57
    return-object v0
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getFontIndex()I
    .locals 1

    .line 1
    const-string v0, "font.index"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getCharTextPropVal(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFontName()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->slideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->_fontname:Ljava/lang/String;

    .line 6
    .line 7
    return-object v0

    .line 8
    :cond_0
    const-string v0, "font.index"

    .line 9
    .line 10
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getCharTextPropVal(Ljava/lang/String;)I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    const/4 v1, -0x1

    .line 15
    if-ne v0, v1, :cond_1

    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    return-object v0

    .line 19
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->slideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getFontCollection()Lcom/intsig/office/fc/hslf/record/FontCollection;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hslf/record/FontCollection;->getFontWithId(I)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getFontSize()I
    .locals 1

    .line 1
    const-string v0, "font.size"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getCharTextPropVal(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getIndentLevel()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->paragraphStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->getReservedField()S

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLength()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->length:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLineSpacing()I
    .locals 2

    .line 1
    const-string v0, "linespacing"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, -0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRawText()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRawText()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget v1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->startPos:I

    .line 8
    .line 9
    iget v2, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->length:I

    .line 10
    .line 11
    add-int/2addr v2, v1

    .line 12
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSpaceAfter()I
    .locals 2

    .line 1
    const-string v0, "spaceafter"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, -0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSpaceBefore()I
    .locals 2

    .line 1
    const-string v0, "spacebefore"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, -0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStartIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->startPos:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSuperscript()I
    .locals 2

    .line 1
    const-string v0, "superscript"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getCharTextPropVal(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, -0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getText()Ljava/lang/String;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->getText()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    iget v2, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->startPos:I

    .line 12
    .line 13
    iget v3, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->length:I

    .line 14
    .line 15
    add-int/2addr v2, v3

    .line 16
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    iget v2, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->startPos:I

    .line 21
    .line 22
    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getTextOffset()I
    .locals 2

    .line 1
    const-string v0, "text.offset"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getParaTextPropVal(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-float v0, v0

    .line 8
    const/high16 v1, 0x42900000    # 72.0f

    .line 9
    .line 10
    mul-float v0, v0, v1

    .line 11
    .line 12
    const/high16 v1, 0x44100000    # 576.0f

    .line 13
    .line 14
    div-float/2addr v0, v1

    .line 15
    float-to-int v0, v0

    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isBold()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->isCharFlagsTextPropVal(I)Z

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isBullet()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getFlag(ZI)Z

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isBulletHard()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getFlag(ZI)Z

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isEmbossed()Z
    .locals 1

    .line 1
    const/16 v0, 0x9

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->isCharFlagsTextPropVal(I)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isItalic()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->isCharFlagsTextPropVal(I)Z

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isShadowed()Z
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->isCharFlagsTextPropVal(I)Z

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isStrikethrough()Z
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->isCharFlagsTextPropVal(I)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isUnderlined()Z
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->isCharFlagsTextPropVal(I)Z

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setAlignment(I)V
    .locals 1

    .line 1
    const-string v0, "alignment"

    .line 2
    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBold(Z)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setCharFlagsTextPropVal(IZ)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBullet(Z)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setFlag(ZIZ)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBulletChar(C)V
    .locals 1

    .line 1
    const-string v0, "bullet.char"

    .line 2
    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBulletColor(Lcom/intsig/office/java/awt/Color;)V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getBlue()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getGreen()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getRed()I

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    const/16 v3, 0xfe

    .line 16
    .line 17
    invoke-direct {v0, v1, v2, p1, v3}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    const-string v0, "bullet.color"

    .line 25
    .line 26
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setBulletFont(I)V
    .locals 1

    .line 1
    const-string v0, "bullet.font"

    .line 2
    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x0

    .line 7
    const/4 v0, 0x1

    .line 8
    invoke-virtual {p0, p1, v0, v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setFlag(ZIZ)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBulletOffset(I)V
    .locals 1

    .line 1
    mul-int/lit16 p1, p1, 0x240

    .line 2
    .line 3
    int-to-float p1, p1

    .line 4
    const/high16 v0, 0x42900000    # 72.0f

    .line 5
    .line 6
    div-float/2addr p1, v0

    .line 7
    float-to-int p1, p1

    .line 8
    const-string v0, "bullet.offset"

    .line 9
    .line 10
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBulletSize(I)V
    .locals 1

    .line 1
    const-string v0, "bullet.size"

    .line 2
    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCharTextPropVal(Ljava/lang/String;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->characterStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->ensureStyleAtomPresent()V

    .line 8
    .line 9
    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->characterStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 11
    .line 12
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->fetchOrAddTextProp(Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;Ljava/lang/String;)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->setValue(I)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setEmbossed(Z)V
    .locals 1

    .line 1
    const/16 v0, 0x9

    .line 2
    .line 3
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setCharFlagsTextPropVal(IZ)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFlag(ZIZ)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->characterStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 4
    .line 5
    sget-object v1, Lcom/intsig/office/fc/hslf/model/textproperties/CharFlagsTextProp;->NAME:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->paragraphStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 9
    .line 10
    sget-object v1, Lcom/intsig/office/fc/hslf/model/textproperties/ParagraphFlagsTextProp;->NAME:Ljava/lang/String;

    .line 11
    .line 12
    :goto_0
    if-nez v0, :cond_2

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->ensureStyleAtomPresent()V

    .line 17
    .line 18
    .line 19
    if-eqz p1, :cond_1

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->characterStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_1
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->paragraphStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 25
    .line 26
    :goto_1
    move-object v0, p1

    .line 27
    :cond_2
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->fetchOrAddTextProp(Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;Ljava/lang/String;)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    check-cast p1, Lcom/intsig/office/fc/hslf/model/textproperties/BitMaskTextProp;

    .line 32
    .line 33
    invoke-virtual {p1, p3, p2}, Lcom/intsig/office/fc/hslf/model/textproperties/BitMaskTextProp;->setSubValue(ZI)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public setFontColor(I)V
    .locals 1

    const-string v0, "font.color"

    .line 1
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setCharTextPropVal(Ljava/lang/String;I)V

    return-void
.end method

.method public setFontColor(Lcom/intsig/office/java/awt/Color;)V
    .locals 4

    .line 2
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getBlue()I

    move-result v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getGreen()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getRed()I

    move-result p1

    const/16 v3, 0xfe

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    move-result p1

    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setFontColor(I)V

    return-void
.end method

.method public setFontIndex(I)V
    .locals 1

    .line 1
    const-string v0, "font.index"

    .line 2
    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setCharTextPropVal(Ljava/lang/String;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFontName(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->slideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->_fontname:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getFontCollection()Lcom/intsig/office/fc/hslf/record/FontCollection;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/record/FontCollection;->addFont(Ljava/lang/String;)I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    const-string v0, "font.index"

    .line 17
    .line 18
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setCharTextPropVal(Ljava/lang/String;I)V

    .line 19
    .line 20
    .line 21
    :goto_0
    return-void
    .line 22
    .line 23
    .line 24
.end method

.method public setFontSize(I)V
    .locals 1

    .line 1
    const-string v0, "font.size"

    .line 2
    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setCharTextPropVal(Ljava/lang/String;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setIndentLevel(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->paragraphStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    int-to-short p1, p1

    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->setReservedField(S)V

    .line 7
    .line 8
    .line 9
    :cond_0
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setItalic(Z)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setCharFlagsTextPropVal(IZ)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLineSpacing(I)V
    .locals 1

    .line 1
    const-string v0, "linespacing"

    .line 2
    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setParaTextPropVal(Ljava/lang/String;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->paragraphStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->ensureStyleAtomPresent()V

    .line 8
    .line 9
    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->paragraphStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 11
    .line 12
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->fetchOrAddTextProp(Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;Ljava/lang/String;)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->setValue(I)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setRawText(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iput v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->length:I

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 8
    .line 9
    invoke-virtual {v0, p0, p1}, Lcom/intsig/office/fc/hslf/model/TextRun;->changeTextInRichTextRun(Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setShadowed(Z)V
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setCharFlagsTextPropVal(IZ)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSpaceAfter(I)V
    .locals 1

    .line 1
    const-string v0, "spaceafter"

    .line 2
    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSpaceBefore(I)V
    .locals 1

    .line 1
    const-string v0, "spacebefore"

    .line 2
    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setStrikethrough(Z)V
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setCharFlagsTextPropVal(IZ)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSuperscript(I)V
    .locals 1

    .line 1
    const-string v0, "superscript"

    .line 2
    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setCharTextPropVal(Ljava/lang/String;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->parentRun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/model/TextRun;->normalize(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setRawText(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTextOffset(I)V
    .locals 1

    .line 1
    mul-int/lit16 p1, p1, 0x240

    .line 2
    .line 3
    int-to-float p1, p1

    .line 4
    const/high16 v0, 0x42900000    # 72.0f

    .line 5
    .line 6
    div-float/2addr p1, v0

    .line 7
    float-to-int p1, p1

    .line 8
    const-string v0, "text.offset"

    .line 9
    .line 10
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setParaTextPropVal(Ljava/lang/String;I)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setUnderlined(Z)V
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setCharFlagsTextPropVal(IZ)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public supplySlideShow(Lcom/intsig/office/fc/hslf/usermodel/SlideShow;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->slideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->_fontname:Ljava/lang/String;

    .line 4
    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setFontName(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 p1, 0x0

    .line 11
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->_fontname:Ljava/lang/String;

    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public supplyTextProps(Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;ZZ)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->paragraphStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->characterStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->paragraphStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 10
    .line 11
    iput-object p2, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->characterStyle:Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 12
    .line 13
    iput-boolean p3, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->sharingParagraphStyle:Z

    .line 14
    .line 15
    iput-boolean p4, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->sharingCharacterStyle:Z

    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 19
    .line 20
    const-string p2, "Can\'t call supplyTextProps if run already has some"

    .line 21
    .line 22
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    throw p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public updateStartPosition(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->startPos:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

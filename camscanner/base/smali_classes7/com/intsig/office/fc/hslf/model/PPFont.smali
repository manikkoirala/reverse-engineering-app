.class public final Lcom/intsig/office/fc/hslf/model/PPFont;
.super Ljava/lang/Object;
.source "PPFont.java"


# static fields
.field public static final ANSI_CHARSET:B = 0x0t

.field public static final ARIAL:Lcom/intsig/office/fc/hslf/model/PPFont;

.field public static final COURIER_NEW:Lcom/intsig/office/fc/hslf/model/PPFont;

.field public static final DEFAULT_CHARSET:B = 0x1t

.field public static final DEFAULT_PITCH:B = 0x0t

.field public static final FF_DECORATIVE:B = 0x50t

.field public static final FF_DONTCARE:B = 0x0t

.field public static final FF_MODERN:B = 0x30t

.field public static final FF_ROMAN:B = 0x10t

.field public static final FF_SCRIPT:B = 0x40t

.field public static final FF_SWISS:B = 0x20t

.field public static final FIXED_PITCH:B = 0x1t

.field public static final SYMBOL_CHARSET:B = 0x2t

.field public static final TIMES_NEW_ROMAN:Lcom/intsig/office/fc/hslf/model/PPFont;

.field public static final VARIABLE_PITCH:B = 0x2t

.field public static final WINGDINGS:Lcom/intsig/office/fc/hslf/model/PPFont;


# instance fields
.field protected charset:I

.field protected flags:I

.field protected name:Ljava/lang/String;

.field protected pitch:I

.field protected type:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hslf/model/PPFont;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/model/PPFont;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/hslf/model/PPFont;->ARIAL:Lcom/intsig/office/fc/hslf/model/PPFont;

    .line 7
    .line 8
    const-string v1, "Arial"

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/PPFont;->setFontName(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/PPFont;->setCharSet(I)V

    .line 15
    .line 16
    .line 17
    const/4 v2, 0x4

    .line 18
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hslf/model/PPFont;->setFontType(I)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/PPFont;->setFontFlags(I)V

    .line 22
    .line 23
    .line 24
    const/16 v3, 0x22

    .line 25
    .line 26
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hslf/model/PPFont;->setPitchAndFamily(I)V

    .line 27
    .line 28
    .line 29
    new-instance v0, Lcom/intsig/office/fc/hslf/model/PPFont;

    .line 30
    .line 31
    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/model/PPFont;-><init>()V

    .line 32
    .line 33
    .line 34
    sput-object v0, Lcom/intsig/office/fc/hslf/model/PPFont;->TIMES_NEW_ROMAN:Lcom/intsig/office/fc/hslf/model/PPFont;

    .line 35
    .line 36
    const-string v3, "Times New Roman"

    .line 37
    .line 38
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hslf/model/PPFont;->setFontName(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/PPFont;->setCharSet(I)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hslf/model/PPFont;->setFontType(I)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/PPFont;->setFontFlags(I)V

    .line 48
    .line 49
    .line 50
    const/16 v3, 0x12

    .line 51
    .line 52
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hslf/model/PPFont;->setPitchAndFamily(I)V

    .line 53
    .line 54
    .line 55
    new-instance v0, Lcom/intsig/office/fc/hslf/model/PPFont;

    .line 56
    .line 57
    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/model/PPFont;-><init>()V

    .line 58
    .line 59
    .line 60
    sput-object v0, Lcom/intsig/office/fc/hslf/model/PPFont;->COURIER_NEW:Lcom/intsig/office/fc/hslf/model/PPFont;

    .line 61
    .line 62
    const-string v3, "Courier New"

    .line 63
    .line 64
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hslf/model/PPFont;->setFontName(Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/PPFont;->setCharSet(I)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hslf/model/PPFont;->setFontType(I)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/PPFont;->setFontFlags(I)V

    .line 74
    .line 75
    .line 76
    const/16 v3, 0x31

    .line 77
    .line 78
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hslf/model/PPFont;->setPitchAndFamily(I)V

    .line 79
    .line 80
    .line 81
    new-instance v0, Lcom/intsig/office/fc/hslf/model/PPFont;

    .line 82
    .line 83
    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/model/PPFont;-><init>()V

    .line 84
    .line 85
    .line 86
    sput-object v0, Lcom/intsig/office/fc/hslf/model/PPFont;->WINGDINGS:Lcom/intsig/office/fc/hslf/model/PPFont;

    .line 87
    .line 88
    const-string v3, "Wingdings"

    .line 89
    .line 90
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hslf/model/PPFont;->setFontName(Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    const/4 v3, 0x2

    .line 94
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hslf/model/PPFont;->setCharSet(I)V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hslf/model/PPFont;->setFontType(I)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/PPFont;->setFontFlags(I)V

    .line 101
    .line 102
    .line 103
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hslf/model/PPFont;->setPitchAndFamily(I)V

    .line 104
    .line 105
    .line 106
    return-void
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hslf/record/FontEntityAtom;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/record/FontEntityAtom;->getFontName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/PPFont;->name:Ljava/lang/String;

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/record/FontEntityAtom;->getCharSet()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hslf/model/PPFont;->charset:I

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/record/FontEntityAtom;->getFontType()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hslf/model/PPFont;->type:I

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/record/FontEntityAtom;->getFontFlags()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hslf/model/PPFont;->flags:I

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/record/FontEntityAtom;->getPitchAndFamily()I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/hslf/model/PPFont;->pitch:I

    return-void
.end method


# virtual methods
.method public getCharSet()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/model/PPFont;->charset:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFontFlags()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/model/PPFont;->flags:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFontName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/PPFont;->name:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFontType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/model/PPFont;->type:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPitchAndFamily()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/model/PPFont;->pitch:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setCharSet(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/model/PPFont;->charset:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFontFlags(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/model/PPFont;->flags:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFontName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/PPFont;->name:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFontType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/model/PPFont;->type:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPitchAndFamily(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/model/PPFont;->pitch:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public abstract Lcom/intsig/office/fc/hslf/model/TextShape;
.super Lcom/intsig/office/fc/hslf/model/SimpleShape;
.source "TextShape.java"


# static fields
.field public static final AlignCenter:I = 0x1

.field public static final AlignJustify:I = 0x3

.field public static final AlignLeft:I = 0x0

.field public static final AlignRight:I = 0x2

.field public static final AnchorBottom:I = 0x2

.field public static final AnchorBottomBaseline:I = 0x7

.field public static final AnchorBottomCentered:I = 0x5

.field public static final AnchorBottomCenteredBaseline:I = 0x9

.field public static final AnchorMiddle:I = 0x1

.field public static final AnchorMiddleCentered:I = 0x4

.field public static final AnchorTop:I = 0x0

.field public static final AnchorTopBaseline:I = 0x6

.field public static final AnchorTopCentered:I = 0x3

.field public static final AnchorTopCenteredBaseline:I = 0x8

.field public static final WrapByPoints:I = 0x1

.field public static final WrapNone:I = 0x2

.field public static final WrapSquare:I = 0x0

.field public static final WrapThrough:I = 0x4

.field public static final WrapTopBottom:I = 0x3


# instance fields
.field protected _txtbox:Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

.field protected _txtrun:Lcom/intsig/office/fc/hslf/model/TextRun;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 4
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/model/TextShape;-><init>(Lcom/intsig/office/fc/hslf/model/Shape;)V

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/SimpleShape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/SimpleShape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    .line 3
    instance-of p1, p1, Lcom/intsig/office/fc/hslf/model/ShapeGroup;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->createSpContainer(Z)Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    return-void
.end method


# virtual methods
.method protected afterInsert(Lcom/intsig/office/fc/hslf/model/Sheet;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->afterInsert(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getEscherTextboxWrapper()Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Sheet;->getPPDrawing()Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->addTextboxWrapper(Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor()Lcom/intsig/office/java/awt/Rectangle;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    new-instance v1, Lcom/intsig/office/java/awt/Rectangle;

    .line 22
    .line 23
    invoke-direct {v1}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/office/java/awt/Rectangle;->equals(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eqz v0, :cond_0

    .line 31
    .line 32
    const-string v0, ""

    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getText()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    if-nez v0, :cond_0

    .line 43
    .line 44
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->resizeToFitText()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 45
    .line 46
    .line 47
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtrun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 48
    .line 49
    if-eqz v0, :cond_1

    .line 50
    .line 51
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeId()I

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/TextRun;->setShapeId(I)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->onAddTextShape(Lcom/intsig/office/fc/hslf/model/TextShape;)V

    .line 59
    .line 60
    .line 61
    :cond_1
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public createTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getEscherTextboxWrapper()Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtbox:Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtbox:Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 15
    .line 16
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtrun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 21
    .line 22
    if-nez v0, :cond_1

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/office/fc/hslf/record/TextHeaderAtom;

    .line 25
    .line 26
    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/record/TextHeaderAtom;-><init>()V

    .line 27
    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtbox:Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/record/TextHeaderAtom;->setParentRecord(Lcom/intsig/office/fc/hslf/record/RecordContainer;)V

    .line 32
    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtbox:Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 35
    .line 36
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->appendChildRecord(Lcom/intsig/office/fc/hslf/record/Record;)V

    .line 37
    .line 38
    .line 39
    new-instance v1, Lcom/intsig/office/fc/hslf/record/TextCharsAtom;

    .line 40
    .line 41
    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/record/TextCharsAtom;-><init>()V

    .line 42
    .line 43
    .line 44
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtbox:Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 45
    .line 46
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->appendChildRecord(Lcom/intsig/office/fc/hslf/record/Record;)V

    .line 47
    .line 48
    .line 49
    new-instance v2, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;

    .line 50
    .line 51
    const/4 v3, 0x0

    .line 52
    invoke-direct {v2, v3}, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;-><init>(I)V

    .line 53
    .line 54
    .line 55
    iget-object v4, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtbox:Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 56
    .line 57
    invoke-virtual {v4, v2}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->appendChildRecord(Lcom/intsig/office/fc/hslf/record/Record;)V

    .line 58
    .line 59
    .line 60
    new-instance v4, Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 61
    .line 62
    invoke-direct {v4, v0, v1, v2}, Lcom/intsig/office/fc/hslf/model/TextRun;-><init>(Lcom/intsig/office/fc/hslf/record/TextHeaderAtom;Lcom/intsig/office/fc/hslf/record/TextCharsAtom;Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;)V

    .line 63
    .line 64
    .line 65
    iput-object v4, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtrun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 66
    .line 67
    const/4 v5, 0x3

    .line 68
    new-array v5, v5, [Lcom/intsig/office/fc/hslf/record/Record;

    .line 69
    .line 70
    aput-object v0, v5, v3

    .line 71
    .line 72
    const/4 v0, 0x1

    .line 73
    aput-object v1, v5, v0

    .line 74
    .line 75
    const/4 v0, 0x2

    .line 76
    aput-object v2, v5, v0

    .line 77
    .line 78
    iput-object v5, v4, Lcom/intsig/office/fc/hslf/model/TextRun;->_records:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 79
    .line 80
    const-string v0, ""

    .line 81
    .line 82
    invoke-virtual {v4, v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->setText(Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 86
    .line 87
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtbox:Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 88
    .line 89
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->getEscherRecord()Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 90
    .line 91
    .line 92
    move-result-object v1

    .line 93
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 94
    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtrun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 97
    .line 98
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hslf/model/TextShape;->setDefaultTextProperties(Lcom/intsig/office/fc/hslf/model/TextRun;)V

    .line 99
    .line 100
    .line 101
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtrun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 102
    .line 103
    return-object v0
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public dispose()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->dispose()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtrun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->dispose()V

    .line 10
    .line 11
    .line 12
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtrun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtbox:Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 15
    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->dispose()V

    .line 19
    .line 20
    .line 21
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtbox:Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 22
    .line 23
    :cond_1
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected getEscherTextboxWrapper()Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtbox:Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 6
    .line 7
    const/16 v1, -0xff3

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    new-instance v1, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 18
    .line 19
    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;-><init>(Lcom/intsig/office/fc/ddf/EscherTextboxRecord;)V

    .line 20
    .line 21
    .line 22
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtbox:Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 23
    .line 24
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtbox:Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 25
    .line 26
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getHorizontalAlignment()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, -0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRichTextRuns()[Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const/4 v1, 0x0

    .line 14
    aget-object v0, v0, v1

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getAlignment()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    :goto_0
    return v0
    .line 21
.end method

.method public getMarginBottom()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    const/16 v1, 0x84

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    const v0, 0xb298

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    :goto_0
    int-to-float v0, v0

    .line 30
    const v1, 0x46467000    # 12700.0f

    .line 31
    .line 32
    .line 33
    div-float/2addr v0, v1

    .line 34
    return v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getMarginLeft()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    const/16 v1, 0x81

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    const v0, 0x16530

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    :goto_0
    int-to-float v0, v0

    .line 30
    const v1, 0x46467000    # 12700.0f

    .line 31
    .line 32
    .line 33
    div-float/2addr v0, v1

    .line 34
    return v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getMarginRight()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    const/16 v1, 0x83

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    const v0, 0x16530

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    :goto_0
    int-to-float v0, v0

    .line 30
    const v1, 0x46467000    # 12700.0f

    .line 31
    .line 32
    .line 33
    div-float/2addr v0, v1

    .line 34
    return v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getMarginTop()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    const/16 v1, 0x82

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    const v0, 0xb298

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    :goto_0
    int-to-float v0, v0

    .line 30
    const v1, 0x46467000    # 12700.0f

    .line 31
    .line 32
    .line 33
    div-float/2addr v0, v1

    .line 34
    return v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getMetaCharactersType()B
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getEscherTextboxWrapper()Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_5

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/4 v1, 0x0

    .line 12
    :goto_0
    array-length v2, v0

    .line 13
    if-ge v1, v2, :cond_5

    .line 14
    .line 15
    aget-object v2, v0, v1

    .line 16
    .line 17
    if-eqz v2, :cond_4

    .line 18
    .line 19
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    .line 20
    .line 21
    .line 22
    move-result-wide v2

    .line 23
    sget-object v4, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlideNumberMCAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 24
    .line 25
    iget v4, v4, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 26
    .line 27
    int-to-long v4, v4

    .line 28
    cmp-long v6, v2, v4

    .line 29
    .line 30
    if-nez v6, :cond_0

    .line 31
    .line 32
    const/4 v0, 0x1

    .line 33
    return v0

    .line 34
    :cond_0
    sget-object v4, Lcom/intsig/office/fc/hslf/record/RecordTypes;->DateTimeMCAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 35
    .line 36
    iget v4, v4, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 37
    .line 38
    int-to-long v4, v4

    .line 39
    cmp-long v6, v2, v4

    .line 40
    .line 41
    if-nez v6, :cond_1

    .line 42
    .line 43
    const/4 v0, 0x2

    .line 44
    return v0

    .line 45
    :cond_1
    sget-object v4, Lcom/intsig/office/fc/hslf/record/RecordTypes;->GenericDateMCAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 46
    .line 47
    iget v4, v4, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 48
    .line 49
    int-to-long v4, v4

    .line 50
    cmp-long v6, v2, v4

    .line 51
    .line 52
    if-nez v6, :cond_2

    .line 53
    .line 54
    const/4 v0, 0x3

    .line 55
    return v0

    .line 56
    :cond_2
    sget-object v4, Lcom/intsig/office/fc/hslf/record/RecordTypes;->RTFDateTimeMCAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 57
    .line 58
    iget v4, v4, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 59
    .line 60
    int-to-long v4, v4

    .line 61
    cmp-long v6, v2, v4

    .line 62
    .line 63
    if-nez v6, :cond_3

    .line 64
    .line 65
    const/4 v0, 0x5

    .line 66
    return v0

    .line 67
    :cond_3
    sget-object v4, Lcom/intsig/office/fc/hslf/record/RecordTypes;->FooterMCAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 68
    .line 69
    iget v4, v4, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 70
    .line 71
    int-to-long v4, v4

    .line 72
    cmp-long v6, v2, v4

    .line 73
    .line 74
    if-nez v6, :cond_4

    .line 75
    .line 76
    const/4 v0, 0x4

    .line 77
    return v0

    .line 78
    :cond_4
    add-int/lit8 v1, v1, 0x1

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_5
    const/4 v0, -0x1

    .line 82
    return v0
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getPlaceholderAtom()Lcom/intsig/office/fc/hslf/record/OEPlaceholderAtom;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->OEPlaceholderAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 4
    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getClientDataRecord(I)Lcom/intsig/office/fc/hslf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hslf/record/OEPlaceholderAtom;

    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPlaceholderId()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getPlaceholderAtom()Lcom/intsig/office/fc/hslf/record/OEPlaceholderAtom;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/OEPlaceholderAtom;->getPlaceholderId()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    sget-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->RoundTripHFPlaceholder12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 13
    .line 14
    iget v0, v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 15
    .line 16
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getClientDataRecord(I)Lcom/intsig/office/fc/hslf/record/Record;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    check-cast v0, Lcom/intsig/office/fc/hslf/record/RoundTripHFPlaceholder12;

    .line 21
    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/RoundTripHFPlaceholder12;->getPlaceholderId()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    goto :goto_0

    .line 29
    :cond_1
    const/4 v0, 0x0

    .line 30
    :goto_0
    return v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_sheet:Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->getText()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    :goto_0
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTextId()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    const/16 v1, 0x80

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    const/4 v0, 0x0

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    :goto_0
    return v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtrun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->initTextRun()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtrun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getUnicodeGeoText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getUnicodeGeoText(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getVerticalAlignment()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    const/16 v1, 0x87

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-nez v0, :cond_3

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRunType()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/Sheet;->getMasterSheet()Lcom/intsig/office/fc/hslf/model/MasterSheet;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    if-eqz v1, :cond_0

    .line 38
    .line 39
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getPlaceholderAtom()Lcom/intsig/office/fc/hslf/record/OEPlaceholderAtom;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    if-eqz v2, :cond_0

    .line 44
    .line 45
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getPlaceholderByTextType(I)Lcom/intsig/office/fc/hslf/model/TextShape;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    goto :goto_0

    .line 50
    :cond_0
    const/4 v1, 0x0

    .line 51
    :goto_0
    if-eqz v1, :cond_1

    .line 52
    .line 53
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/TextShape;->getVerticalAlignment()I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    goto :goto_1

    .line 58
    :cond_1
    if-eqz v0, :cond_2

    .line 59
    .line 60
    const/4 v1, 0x6

    .line 61
    if-eq v0, v1, :cond_2

    .line 62
    .line 63
    const/4 v0, 0x0

    .line 64
    goto :goto_1

    .line 65
    :cond_2
    const/4 v0, 0x1

    .line 66
    goto :goto_1

    .line 67
    :cond_3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 68
    .line 69
    .line 70
    move-result v0

    .line 71
    :goto_1
    return v0
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getWordWrap()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    const/16 v1, -0xff5

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 10
    .line 11
    const/16 v1, 0x85

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 18
    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    const/4 v0, 0x0

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    :goto_0
    return v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected initTextRun()V
    .locals 12

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getEscherTextboxWrapper()Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    if-eqz v1, :cond_a

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    goto/16 :goto_7

    .line 14
    .line 15
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const/4 v1, 0x0

    .line 20
    const/4 v2, 0x0

    .line 21
    :goto_0
    array-length v3, v0

    .line 22
    if-ge v2, v3, :cond_2

    .line 23
    .line 24
    aget-object v3, v0, v2

    .line 25
    .line 26
    instance-of v4, v3, Lcom/intsig/office/fc/hslf/record/OutlineTextRefAtom;

    .line 27
    .line 28
    if-eqz v4, :cond_1

    .line 29
    .line 30
    check-cast v3, Lcom/intsig/office/fc/hslf/record/OutlineTextRefAtom;

    .line 31
    .line 32
    goto :goto_1

    .line 33
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_2
    const/4 v3, 0x0

    .line 37
    :goto_1
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_sheet:Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 38
    .line 39
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Sheet;->getTextRuns()[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    if-eqz v3, :cond_4

    .line 44
    .line 45
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/record/OutlineTextRefAtom;->getTextIndex()I

    .line 46
    .line 47
    .line 48
    move-result v3

    .line 49
    const/4 v4, 0x0

    .line 50
    :goto_2
    array-length v5, v2

    .line 51
    if-ge v4, v5, :cond_6

    .line 52
    .line 53
    aget-object v5, v2, v4

    .line 54
    .line 55
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/model/TextRun;->getIndex()I

    .line 56
    .line 57
    .line 58
    move-result v5

    .line 59
    if-ne v5, v3, :cond_3

    .line 60
    .line 61
    aget-object v5, v2, v4

    .line 62
    .line 63
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/model/TextRun;->getShapeId()I

    .line 64
    .line 65
    .line 66
    move-result v5

    .line 67
    if-gez v5, :cond_3

    .line 68
    .line 69
    aget-object v2, v2, v4

    .line 70
    .line 71
    iput-object v2, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtrun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 72
    .line 73
    goto :goto_4

    .line 74
    :cond_3
    add-int/lit8 v4, v4, 0x1

    .line 75
    .line 76
    goto :goto_2

    .line 77
    :cond_4
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 78
    .line 79
    const/16 v4, -0xff6

    .line 80
    .line 81
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 82
    .line 83
    .line 84
    move-result-object v3

    .line 85
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 86
    .line 87
    invoke-virtual {v3}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->getShapeId()I

    .line 88
    .line 89
    .line 90
    move-result v3

    .line 91
    if-eqz v2, :cond_6

    .line 92
    .line 93
    const/4 v4, 0x0

    .line 94
    :goto_3
    array-length v5, v2

    .line 95
    if-ge v4, v5, :cond_6

    .line 96
    .line 97
    aget-object v5, v2, v4

    .line 98
    .line 99
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/model/TextRun;->getShapeId()I

    .line 100
    .line 101
    .line 102
    move-result v5

    .line 103
    if-ne v5, v3, :cond_5

    .line 104
    .line 105
    aget-object v2, v2, v4

    .line 106
    .line 107
    iput-object v2, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtrun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 108
    .line 109
    goto :goto_4

    .line 110
    :cond_5
    add-int/lit8 v4, v4, 0x1

    .line 111
    .line 112
    goto :goto_3

    .line 113
    :cond_6
    :goto_4
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtrun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 114
    .line 115
    if-eqz v2, :cond_a

    .line 116
    .line 117
    const/4 v2, 0x0

    .line 118
    :goto_5
    array-length v3, v0

    .line 119
    if-ge v2, v3, :cond_a

    .line 120
    .line 121
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtrun:Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 122
    .line 123
    iget-object v4, v3, Lcom/intsig/office/fc/hslf/model/TextRun;->_ruler:Lcom/intsig/office/fc/hslf/record/TextRulerAtom;

    .line 124
    .line 125
    if-nez v4, :cond_7

    .line 126
    .line 127
    aget-object v4, v0, v2

    .line 128
    .line 129
    instance-of v5, v4, Lcom/intsig/office/fc/hslf/record/TextRulerAtom;

    .line 130
    .line 131
    if-eqz v5, :cond_7

    .line 132
    .line 133
    check-cast v4, Lcom/intsig/office/fc/hslf/record/TextRulerAtom;

    .line 134
    .line 135
    iput-object v4, v3, Lcom/intsig/office/fc/hslf/model/TextRun;->_ruler:Lcom/intsig/office/fc/hslf/record/TextRulerAtom;

    .line 136
    .line 137
    :cond_7
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 138
    .line 139
    .line 140
    move-result-object v3

    .line 141
    array-length v4, v3

    .line 142
    const/4 v5, 0x0

    .line 143
    :goto_6
    if-ge v5, v4, :cond_9

    .line 144
    .line 145
    aget-object v6, v3, v5

    .line 146
    .line 147
    aget-object v7, v0, v2

    .line 148
    .line 149
    invoke-virtual {v7}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    .line 150
    .line 151
    .line 152
    move-result-wide v7

    .line 153
    invoke-virtual {v6}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    .line 154
    .line 155
    .line 156
    move-result-wide v9

    .line 157
    cmp-long v11, v7, v9

    .line 158
    .line 159
    if-nez v11, :cond_8

    .line 160
    .line 161
    aput-object v6, v0, v2

    .line 162
    .line 163
    :cond_8
    add-int/lit8 v5, v5, 0x1

    .line 164
    .line 165
    goto :goto_6

    .line 166
    :cond_9
    add-int/lit8 v2, v2, 0x1

    .line 167
    .line 168
    goto :goto_5

    .line 169
    :cond_a
    :goto_7
    return-void
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public resizeToFitText()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected setDefaultTextProperties(Lcom/intsig/office/fc/hslf/model/TextRun;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setHorizontalAlignment(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRichTextRuns()[Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/4 v1, 0x0

    .line 12
    aget-object v0, v0, v1

    .line 13
    .line 14
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->setAlignment(I)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setHyperlink(III)V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;->getInteractiveInfoAtom()Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    const/4 v2, 0x4

    .line 11
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setAction(B)V

    .line 12
    .line 13
    .line 14
    const/16 v2, 0x8

    .line 15
    .line 16
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setHyperlinkType(B)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->setHyperlinkID(I)V

    .line 20
    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtbox:Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 23
    .line 24
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->appendChildRecord(Lcom/intsig/office/fc/hslf/record/Record;)V

    .line 25
    .line 26
    .line 27
    new-instance p1, Lcom/intsig/office/fc/hslf/record/TxInteractiveInfoAtom;

    .line 28
    .line 29
    invoke-direct {p1}, Lcom/intsig/office/fc/hslf/record/TxInteractiveInfoAtom;-><init>()V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hslf/record/TxInteractiveInfoAtom;->setStartIndex(I)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p1, p3}, Lcom/intsig/office/fc/hslf/record/TxInteractiveInfoAtom;->setEndIndex(I)V

    .line 36
    .line 37
    .line 38
    iget-object p2, p0, Lcom/intsig/office/fc/hslf/model/TextShape;->_txtbox:Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 39
    .line 40
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->appendChildRecord(Lcom/intsig/office/fc/hslf/record/Record;)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public setMarginBottom(F)V
    .locals 1

    .line 1
    const v0, 0x46467000    # 12700.0f

    .line 2
    .line 3
    .line 4
    mul-float p1, p1, v0

    .line 5
    .line 6
    float-to-int p1, p1

    .line 7
    const/16 v0, 0x84

    .line 8
    .line 9
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMarginLeft(F)V
    .locals 1

    .line 1
    const v0, 0x46467000    # 12700.0f

    .line 2
    .line 3
    .line 4
    mul-float p1, p1, v0

    .line 5
    .line 6
    float-to-int p1, p1

    .line 7
    const/16 v0, 0x81

    .line 8
    .line 9
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMarginRight(F)V
    .locals 1

    .line 1
    const v0, 0x46467000    # 12700.0f

    .line 2
    .line 3
    .line 4
    mul-float p1, p1, v0

    .line 5
    .line 6
    float-to-int p1, p1

    .line 7
    const/16 v0, 0x83

    .line 8
    .line 9
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMarginTop(F)V
    .locals 1

    .line 1
    const v0, 0x46467000    # 12700.0f

    .line 2
    .line 3
    .line 4
    mul-float p1, p1, v0

    .line 5
    .line 6
    float-to-int p1, p1

    .line 7
    const/16 v0, 0x82

    .line 8
    .line 9
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSheet(Lcom/intsig/office/fc/hslf/model/Sheet;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_sheet:Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_sheet:Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 10
    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->setSheet(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRichTextRuns()[Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    const/4 v0, 0x0

    .line 19
    :goto_0
    array-length v1, p1

    .line 20
    if-ge v0, v1, :cond_0

    .line 21
    .line 22
    aget-object v1, p1, v0

    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/model/Shape;->_sheet:Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 25
    .line 26
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSlideShow()Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->supplySlideShow(Lcom/intsig/office/fc/hslf/usermodel/SlideShow;)V

    .line 31
    .line 32
    .line 33
    add-int/lit8 v0, v0, 0x1

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextShape;->createTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    :cond_0
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/model/TextRun;->setText(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/model/TextShape;->setTextId(I)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
.end method

.method public setTextId(I)V
    .locals 1

    .line 1
    const/16 v0, 0x80

    .line 2
    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setVerticalAlignment(I)V
    .locals 1

    .line 1
    const/16 v0, 0x87

    .line 2
    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setWordWrap(I)V
    .locals 1

    .line 1
    const/16 v0, 0x85

    .line 2
    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

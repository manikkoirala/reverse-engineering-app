.class public final Lcom/intsig/office/fc/hslf/record/PPDrawing;
.super Lcom/intsig/office/fc/hslf/record/RecordAtom;
.source "PPDrawing.java"


# instance fields
.field private _header:[B

.field private _type:J

.field private childRecords:[Lcom/intsig/office/fc/ddf/EscherRecord;

.field private dg:Lcom/intsig/office/fc/ddf/EscherDgRecord;

.field private textboxWrappers:[Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 18
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordAtom;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [B

    .line 19
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->_header:[B

    const/16 v1, 0xf

    const/4 v2, 0x0

    .line 20
    invoke-static {v0, v2, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    .line 21
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->_header:[B

    sget-object v1, Lcom/intsig/office/fc/hslf/record/RecordTypes;->PPDrawing:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    iget v1, v1, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    const/4 v3, 0x2

    invoke-static {v0, v3, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    .line 22
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->_header:[B

    const/4 v1, 0x4

    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    new-array v0, v2, [Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 23
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->textboxWrappers:[Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 24
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->create()V

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 11

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordAtom;-><init>()V

    const/16 v0, 0x8

    new-array v1, v0, [B

    .line 2
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->_header:[B

    const/4 v2, 0x0

    .line 3
    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->_header:[B

    const/4 v3, 0x2

    invoke-static {v1, v3}, Lcom/intsig/office/fc/util/LittleEndian;->getUShort([BI)I

    move-result v1

    int-to-long v3, v1

    iput-wide v3, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->_type:J

    .line 5
    new-array v7, p3, [B

    .line 6
    invoke-static {p1, p2, v7, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7
    new-instance v6, Lcom/intsig/office/fc/ddf/DefaultEscherRecordFactory;

    invoke-direct {v6}, Lcom/intsig/office/fc/ddf/DefaultEscherRecordFactory;-><init>()V

    .line 8
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    const/16 v8, 0x8

    add-int/lit8 v9, p3, -0x8

    move-object v5, p0

    move-object v10, p1

    .line 9
    invoke-direct/range {v5 .. v10}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->findEscherChildren(Lcom/intsig/office/fc/ddf/DefaultEscherRecordFactory;[BIILjava/util/Vector;)V

    .line 10
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p2

    new-array p2, p2, [Lcom/intsig/office/fc/ddf/EscherRecord;

    iput-object p2, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->childRecords:[Lcom/intsig/office/fc/ddf/EscherRecord;

    const/4 p2, 0x0

    .line 11
    :goto_0
    iget-object p3, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->childRecords:[Lcom/intsig/office/fc/ddf/EscherRecord;

    array-length v0, p3

    if-ge p2, v0, :cond_0

    .line 12
    invoke-virtual {p1, p2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/ddf/EscherRecord;

    aput-object v0, p3, p2

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 13
    :cond_0
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    .line 14
    iget-object p2, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->childRecords:[Lcom/intsig/office/fc/ddf/EscherRecord;

    invoke-direct {p0, p2, p1}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->findEscherTextboxRecord([Lcom/intsig/office/fc/ddf/EscherRecord;Ljava/util/Vector;)V

    .line 15
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p2

    new-array p2, p2, [Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    iput-object p2, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->textboxWrappers:[Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 16
    :goto_1
    iget-object p2, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->textboxWrappers:[Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    array-length p3, p2

    if-ge v2, p3, :cond_1

    .line 17
    invoke-virtual {p1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    aput-object p3, p2, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method private create()V
    .locals 8

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, -0xffe

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 9
    .line 10
    .line 11
    const/16 v1, 0xf

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 14
    .line 15
    .line 16
    new-instance v2, Lcom/intsig/office/fc/ddf/EscherDgRecord;

    .line 17
    .line 18
    invoke-direct {v2}, Lcom/intsig/office/fc/ddf/EscherDgRecord;-><init>()V

    .line 19
    .line 20
    .line 21
    const/16 v3, 0x10

    .line 22
    .line 23
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 24
    .line 25
    .line 26
    const/4 v3, 0x1

    .line 27
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/ddf/EscherDgRecord;->setNumShapes(I)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 31
    .line 32
    .line 33
    new-instance v2, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 34
    .line 35
    invoke-direct {v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;-><init>()V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 39
    .line 40
    .line 41
    const/16 v4, -0xffd

    .line 42
    .line 43
    invoke-virtual {v2, v4}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 44
    .line 45
    .line 46
    new-instance v4, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 47
    .line 48
    invoke-direct {v4}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;-><init>()V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v4, v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 52
    .line 53
    .line 54
    const/16 v5, -0xffc

    .line 55
    .line 56
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 57
    .line 58
    .line 59
    new-instance v6, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;

    .line 60
    .line 61
    invoke-direct {v6}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;-><init>()V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v6, v3}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v4, v6}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 68
    .line 69
    .line 70
    new-instance v6, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 71
    .line 72
    invoke-direct {v6}, Lcom/intsig/office/fc/ddf/EscherSpRecord;-><init>()V

    .line 73
    .line 74
    .line 75
    const/4 v7, 0x2

    .line 76
    invoke-virtual {v6, v7}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 77
    .line 78
    .line 79
    const/4 v7, 0x5

    .line 80
    invoke-virtual {v6, v7}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setFlags(I)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {v4, v6}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {v2, v4}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 90
    .line 91
    .line 92
    new-instance v2, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 93
    .line 94
    invoke-direct {v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;-><init>()V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 101
    .line 102
    .line 103
    new-instance v1, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 104
    .line 105
    invoke-direct {v1}, Lcom/intsig/office/fc/ddf/EscherSpRecord;-><init>()V

    .line 106
    .line 107
    .line 108
    const/16 v4, 0x12

    .line 109
    .line 110
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 111
    .line 112
    .line 113
    const/16 v4, 0xc00

    .line 114
    .line 115
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setFlags(I)V

    .line 116
    .line 117
    .line 118
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 119
    .line 120
    .line 121
    new-instance v1, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 122
    .line 123
    invoke-direct {v1}, Lcom/intsig/office/fc/ddf/EscherOptRecord;-><init>()V

    .line 124
    .line 125
    .line 126
    const/16 v4, -0xff5

    .line 127
    .line 128
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 129
    .line 130
    .line 131
    new-instance v4, Lcom/intsig/office/fc/ddf/EscherRGBProperty;

    .line 132
    .line 133
    const/16 v5, 0x181

    .line 134
    .line 135
    const/high16 v6, 0x8000000

    .line 136
    .line 137
    invoke-direct {v4, v5, v6}, Lcom/intsig/office/fc/ddf/EscherRGBProperty;-><init>(SI)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 141
    .line 142
    .line 143
    new-instance v4, Lcom/intsig/office/fc/ddf/EscherRGBProperty;

    .line 144
    .line 145
    const/16 v5, 0x183

    .line 146
    .line 147
    const v6, 0x8000005

    .line 148
    .line 149
    .line 150
    invoke-direct {v4, v5, v6}, Lcom/intsig/office/fc/ddf/EscherRGBProperty;-><init>(SI)V

    .line 151
    .line 152
    .line 153
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 154
    .line 155
    .line 156
    new-instance v4, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 157
    .line 158
    const/16 v5, 0x193

    .line 159
    .line 160
    const v6, 0x99936e

    .line 161
    .line 162
    .line 163
    invoke-direct {v4, v5, v6}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 167
    .line 168
    .line 169
    new-instance v4, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 170
    .line 171
    const/16 v5, 0x194

    .line 172
    .line 173
    const v6, 0x76b1be

    .line 174
    .line 175
    .line 176
    invoke-direct {v4, v5, v6}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 177
    .line 178
    .line 179
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 180
    .line 181
    .line 182
    new-instance v4, Lcom/intsig/office/fc/ddf/EscherBoolProperty;

    .line 183
    .line 184
    const/16 v5, 0x1bf

    .line 185
    .line 186
    const v6, 0x120012

    .line 187
    .line 188
    .line 189
    invoke-direct {v4, v5, v6}, Lcom/intsig/office/fc/ddf/EscherBoolProperty;-><init>(SI)V

    .line 190
    .line 191
    .line 192
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 193
    .line 194
    .line 195
    new-instance v4, Lcom/intsig/office/fc/ddf/EscherBoolProperty;

    .line 196
    .line 197
    const/16 v5, 0x1ff

    .line 198
    .line 199
    const/high16 v6, 0x80000

    .line 200
    .line 201
    invoke-direct {v4, v5, v6}, Lcom/intsig/office/fc/ddf/EscherBoolProperty;-><init>(SI)V

    .line 202
    .line 203
    .line 204
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 205
    .line 206
    .line 207
    new-instance v4, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 208
    .line 209
    const/16 v5, 0x304

    .line 210
    .line 211
    const/16 v6, 0x9

    .line 212
    .line 213
    invoke-direct {v4, v5, v6}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 214
    .line 215
    .line 216
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 217
    .line 218
    .line 219
    new-instance v4, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 220
    .line 221
    const/16 v5, 0x33f

    .line 222
    .line 223
    const v6, 0x10001

    .line 224
    .line 225
    .line 226
    invoke-direct {v4, v5, v6}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 227
    .line 228
    .line 229
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 230
    .line 231
    .line 232
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 233
    .line 234
    .line 235
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 236
    .line 237
    .line 238
    new-array v1, v3, [Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 239
    .line 240
    const/4 v2, 0x0

    .line 241
    aput-object v0, v1, v2

    .line 242
    .line 243
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->childRecords:[Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 244
    .line 245
    return-void
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private findEscherChildren(Lcom/intsig/office/fc/ddf/DefaultEscherRecordFactory;[BIILjava/util/Vector;)V
    .locals 8

    .line 1
    add-int/lit8 v0, p3, 0x4

    .line 2
    .line 3
    invoke-static {p2, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0x8

    .line 8
    .line 9
    add-int/2addr v0, v1

    .line 10
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/fc/ddf/DefaultEscherRecordFactory;->createRecord([BI)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    invoke-virtual {v2, p2, p3, p1}, Lcom/intsig/office/fc/ddf/EscherRecord;->fillFields([BILcom/intsig/office/fc/ddf/EscherRecordFactory;)I

    .line 15
    .line 16
    .line 17
    invoke-virtual {p5, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordSize()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    if-eq v2, v0, :cond_0

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    move v0, v2

    .line 28
    :goto_0
    add-int v5, p3, v0

    .line 29
    .line 30
    sub-int v6, p4, v0

    .line 31
    .line 32
    if-lt v6, v1, :cond_1

    .line 33
    .line 34
    move-object v2, p0

    .line 35
    move-object v3, p1

    .line 36
    move-object v4, p2

    .line 37
    move-object v7, p5

    .line 38
    invoke-direct/range {v2 .. v7}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->findEscherChildren(Lcom/intsig/office/fc/ddf/DefaultEscherRecordFactory;[BIILjava/util/Vector;)V

    .line 39
    .line 40
    .line 41
    :cond_1
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method private findEscherTextboxRecord([Lcom/intsig/office/fc/ddf/EscherRecord;Ljava/util/Vector;)V
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    array-length v1, p1

    .line 3
    if-ge v0, v1, :cond_4

    .line 4
    .line 5
    aget-object v1, p1, v0

    .line 6
    .line 7
    instance-of v2, v1, Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 8
    .line 9
    if-eqz v2, :cond_2

    .line 10
    .line 11
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 12
    .line 13
    new-instance v2, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 14
    .line 15
    invoke-direct {v2, v1}, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;-><init>(Lcom/intsig/office/fc/ddf/EscherTextboxRecord;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p2, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    aget-object v1, p1, v0

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordName()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    const-string v3, "BinaryTagData"

    .line 28
    .line 29
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    if-eqz v1, :cond_0

    .line 34
    .line 35
    aget-object v1, p1, v0

    .line 36
    .line 37
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->setShapeId(I)V

    .line 42
    .line 43
    .line 44
    goto :goto_2

    .line 45
    :cond_0
    move v1, v0

    .line 46
    :goto_1
    if-ltz v1, :cond_3

    .line 47
    .line 48
    aget-object v3, p1, v1

    .line 49
    .line 50
    instance-of v4, v3, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 51
    .line 52
    if-eqz v4, :cond_1

    .line 53
    .line 54
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 55
    .line 56
    invoke-virtual {v3}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->getShapeId()I

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->setShapeId(I)V

    .line 61
    .line 62
    .line 63
    goto :goto_2

    .line 64
    :cond_1
    add-int/lit8 v1, v1, -0x1

    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_2
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->isContainerRecord()Z

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    if-eqz v1, :cond_3

    .line 72
    .line 73
    aget-object v1, p1, v0

    .line 74
    .line 75
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->getChildRecords()Ljava/util/List;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 80
    .line 81
    .line 82
    move-result v2

    .line 83
    new-array v2, v2, [Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 84
    .line 85
    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 86
    .line 87
    .line 88
    invoke-direct {p0, v2, p2}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->findEscherTextboxRecord([Lcom/intsig/office/fc/ddf/EscherRecord;Ljava/util/Vector;)V

    .line 89
    .line 90
    .line 91
    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    .line 92
    .line 93
    goto :goto_0

    .line 94
    :cond_4
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method


# virtual methods
.method public addTextboxWrapper(Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->textboxWrappers:[Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    add-int/lit8 v1, v1, 0x1

    .line 5
    .line 6
    new-array v1, v1, [Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    array-length v3, v0

    .line 10
    invoke-static {v0, v2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->textboxWrappers:[Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 14
    .line 15
    array-length v0, v0

    .line 16
    aput-object p1, v1, v0

    .line 17
    .line 18
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->textboxWrappers:[Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public dispose()V
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->_header:[B

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->childRecords:[Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    if-eqz v1, :cond_1

    .line 8
    .line 9
    array-length v3, v1

    .line 10
    const/4 v4, 0x0

    .line 11
    :goto_0
    if-ge v4, v3, :cond_0

    .line 12
    .line 13
    aget-object v5, v1, v4

    .line 14
    .line 15
    invoke-virtual {v5}, Lcom/intsig/office/fc/ddf/EscherRecord;->dispose()V

    .line 16
    .line 17
    .line 18
    add-int/lit8 v4, v4, 0x1

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->childRecords:[Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 22
    .line 23
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->textboxWrappers:[Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 24
    .line 25
    if-eqz v1, :cond_3

    .line 26
    .line 27
    array-length v3, v1

    .line 28
    :goto_1
    if-ge v2, v3, :cond_2

    .line 29
    .line 30
    aget-object v4, v1, v2

    .line 31
    .line 32
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;->dispose()V

    .line 33
    .line 34
    .line 35
    add-int/lit8 v2, v2, 0x1

    .line 36
    .line 37
    goto :goto_1

    .line 38
    :cond_2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->textboxWrappers:[Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 39
    .line 40
    :cond_3
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->dg:Lcom/intsig/office/fc/ddf/EscherDgRecord;

    .line 41
    .line 42
    if-eqz v1, :cond_4

    .line 43
    .line 44
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherDgRecord;->dispose()V

    .line 45
    .line 46
    .line 47
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->dg:Lcom/intsig/office/fc/ddf/EscherDgRecord;

    .line 48
    .line 49
    :cond_4
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEscherDgRecord()Lcom/intsig/office/fc/ddf/EscherDgRecord;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->dg:Lcom/intsig/office/fc/ddf/EscherDgRecord;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->childRecords:[Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    aget-object v0, v0, v1

    .line 9
    .line 10
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_1

    .line 21
    .line 22
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 27
    .line 28
    instance-of v2, v1, Lcom/intsig/office/fc/ddf/EscherDgRecord;

    .line 29
    .line 30
    if-eqz v2, :cond_0

    .line 31
    .line 32
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherDgRecord;

    .line 33
    .line 34
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->dg:Lcom/intsig/office/fc/ddf/EscherDgRecord;

    .line 35
    .line 36
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->dg:Lcom/intsig/office/fc/ddf/EscherDgRecord;

    .line 37
    .line 38
    return-object v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getEscherRecords()[Lcom/intsig/office/fc/ddf/EscherRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->childRecords:[Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTextboxWrappers()[Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->textboxWrappers:[Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->textboxWrappers:[Lcom/intsig/office/fc/hslf/record/EscherTextboxWrapper;

    .line 4
    .line 5
    array-length v2, v2

    .line 6
    if-ge v1, v2, :cond_0

    .line 7
    .line 8
    add-int/lit8 v1, v1, 0x1

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v1, 0x0

    .line 12
    const/4 v2, 0x0

    .line 13
    :goto_1
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->childRecords:[Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 14
    .line 15
    array-length v4, v3

    .line 16
    if-ge v1, v4, :cond_1

    .line 17
    .line 18
    aget-object v3, v3, v1

    .line 19
    .line 20
    invoke-virtual {v3}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordSize()I

    .line 21
    .line 22
    .line 23
    move-result v3

    .line 24
    add-int/2addr v2, v3

    .line 25
    add-int/lit8 v1, v1, 0x1

    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->_header:[B

    .line 29
    .line 30
    const/4 v3, 0x4

    .line 31
    invoke-static {v1, v3, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 32
    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->_header:[B

    .line 35
    .line 36
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 37
    .line 38
    .line 39
    new-array v1, v2, [B

    .line 40
    .line 41
    const/4 v2, 0x0

    .line 42
    :goto_2
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/record/PPDrawing;->childRecords:[Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 43
    .line 44
    array-length v4, v3

    .line 45
    if-ge v0, v4, :cond_2

    .line 46
    .line 47
    aget-object v3, v3, v0

    .line 48
    .line 49
    invoke-virtual {v3, v2, v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->serialize(I[B)I

    .line 50
    .line 51
    .line 52
    move-result v3

    .line 53
    add-int/2addr v2, v3

    .line 54
    add-int/lit8 v0, v0, 0x1

    .line 55
    .line 56
    goto :goto_2

    .line 57
    :cond_2
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.class public final Lcom/intsig/office/fc/hslf/record/ExControl;
.super Lcom/intsig/office/fc/hslf/record/ExEmbed;
.source "ExControl.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/ExEmbed;-><init>()V

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    new-instance v1, Lcom/intsig/office/fc/hslf/record/ExControlAtom;

    invoke-direct {v1}, Lcom/intsig/office/fc/hslf/record/ExControlAtom;-><init>()V

    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/ExEmbed;->embedAtom:Lcom/intsig/office/fc/hslf/record/RecordAtom;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/fc/hslf/record/ExEmbed;-><init>([BII)V

    return-void
.end method


# virtual methods
.method public getExControlAtom()Lcom/intsig/office/fc/hslf/record/ExControlAtom;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    aget-object v0, v0, v1

    .line 5
    .line 6
    check-cast v0, Lcom/intsig/office/fc/hslf/record/ExControlAtom;

    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExControl:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 4
    .line 5
    int-to-long v0, v0

    .line 6
    return-wide v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;
.super Lcom/intsig/office/fc/hslf/record/RecordAtom;
.source "SlidePersistAtom.java"


# static fields
.field private static _type:J = 0x3f3L


# instance fields
.field private _header:[B

.field private hasShapesOtherThanPlaceholders:Z

.field private numPlaceholderTexts:I

.field private refID:I

.field private reservedFields:[B

.field private slideIdentifier:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 3

    .line 12
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordAtom;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [B

    .line 13
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->_header:[B

    const/4 v1, 0x0

    .line 14
    invoke-static {v0, v1, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    .line 15
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->_header:[B

    sget-wide v1, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->_type:J

    long-to-int v2, v1

    const/4 v1, 0x2

    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    .line 16
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->_header:[B

    const/16 v1, 0x14

    const/4 v2, 0x4

    invoke-static {v0, v2, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    const/4 v0, 0x1

    .line 17
    iput-boolean v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->hasShapesOtherThanPlaceholders:Z

    new-array v0, v2, [B

    .line 18
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->reservedFields:[B

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordAtom;-><init>()V

    const/16 v0, 0x8

    if-ge p3, v0, :cond_0

    const/16 p3, 0x8

    :cond_0
    new-array v1, v0, [B

    .line 2
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->_header:[B

    const/4 v2, 0x0

    .line 3
    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v0, p2, 0x8

    .line 4
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->refID:I

    add-int/lit8 v0, p2, 0xc

    .line 5
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->hasShapesOtherThanPlaceholders:Z

    goto :goto_0

    .line 7
    :cond_1
    iput-boolean v2, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->hasShapesOtherThanPlaceholders:Z

    :goto_0
    add-int/lit8 v0, p2, 0x10

    .line 8
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->numPlaceholderTexts:I

    add-int/lit8 v0, p2, 0x14

    .line 9
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->slideIdentifier:I

    add-int/lit8 p3, p3, -0x18

    .line 10
    new-array p3, p3, [B

    iput-object p3, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->reservedFields:[B

    add-int/lit8 p2, p2, 0x18

    .line 11
    array-length v0, p3

    invoke-static {p1, p2, p3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->_header:[B

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->reservedFields:[B

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHasShapesOtherThanPlaceholders()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->hasShapesOtherThanPlaceholders:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNumPlaceholderTexts()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->numPlaceholderTexts:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRefID()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->refID:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideIdentifier()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->slideIdentifier:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setRefID(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->refID:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSlideIdentifier(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->slideIdentifier:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->_header:[B

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->hasShapesOtherThanPlaceholders:Z

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x4

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    :goto_0
    iget v1, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->refID:I

    .line 14
    .line 15
    invoke-static {v1, p1}, Lcom/intsig/office/fc/hslf/record/Record;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 16
    .line 17
    .line 18
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hslf/record/Record;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 19
    .line 20
    .line 21
    iget v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->numPlaceholderTexts:I

    .line 22
    .line 23
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hslf/record/Record;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 24
    .line 25
    .line 26
    iget v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->slideIdentifier:I

    .line 27
    .line 28
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hslf/record/Record;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;->reservedFields:[B

    .line 32
    .line 33
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

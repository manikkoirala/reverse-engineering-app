.class public final Lcom/intsig/office/fc/hslf/model/Hyperlink;
.super Ljava/lang/Object;
.source "Hyperlink.java"


# static fields
.field public static final LINK_FIRSTSLIDE:B = 0x2t

.field public static final LINK_LASTSLIDE:B = 0x3t

.field public static final LINK_NEXTSLIDE:B = 0x0t

.field public static final LINK_NULL:B = -0x1t

.field public static final LINK_PREVIOUSSLIDE:B = 0x1t

.field public static final LINK_URL:B = 0x8t


# instance fields
.field private address:Ljava/lang/String;

.field private endIndex:I

.field private id:I

.field private startIndex:I

.field private title:Ljava/lang/String;

.field private type:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, -0x1

    .line 5
    iput v0, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->id:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected static find(Lcom/intsig/office/fc/hslf/model/Shape;)Lcom/intsig/office/fc/hslf/model/Hyperlink;
    .locals 6

    .line 9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSlideShow()Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    move-result-object v1

    .line 11
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getDocumentRecord()Lcom/intsig/office/fc/hslf/record/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/Document;->getExObjList()Lcom/intsig/office/fc/hslf/record/ExObjList;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return-object v2

    .line 12
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    move-result-object p0

    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 14
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 15
    invoke-virtual {v3}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    move-result v4

    const/16 v5, -0xfef

    if-ne v4, v5, :cond_1

    .line 16
    invoke-virtual {v3}, Lcom/intsig/office/fc/ddf/EscherRecord;->serialize()[B

    move-result-object v3

    .line 17
    array-length v4, v3

    const/16 v5, 0x8

    sub-int/2addr v4, v5

    invoke-static {v3, v5, v4}, Lcom/intsig/office/fc/hslf/record/Record;->findChildRecords([BII)[Lcom/intsig/office/fc/hslf/record/Record;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 18
    invoke-static {v3, v1, v0}, Lcom/intsig/office/fc/hslf/model/Hyperlink;->find([Lcom/intsig/office/fc/hslf/record/Record;Lcom/intsig/office/fc/hslf/record/ExObjList;Ljava/util/List;)V

    goto :goto_0

    .line 19
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p0

    const/4 v1, 0x1

    if-ne p0, v1, :cond_3

    const/4 p0, 0x0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p0

    move-object v2, p0

    check-cast v2, Lcom/intsig/office/fc/hslf/model/Hyperlink;

    :cond_3
    return-object v2
.end method

.method private static find([Lcom/intsig/office/fc/hslf/record/Record;Lcom/intsig/office/fc/hslf/record/ExObjList;Ljava/util/List;)V
    .locals 5

    const/4 v0, 0x0

    .line 20
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_2

    .line 21
    aget-object v1, p0, v0

    instance-of v2, v1, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;

    if-eqz v2, :cond_1

    .line 22
    check-cast v1, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;

    .line 23
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;->getInteractiveInfoAtom()Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;

    move-result-object v1

    .line 24
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->getHyperlinkID()I

    move-result v2

    .line 25
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hslf/record/ExObjList;->get(I)Lcom/intsig/office/fc/hslf/record/ExHyperlink;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 26
    new-instance v3, Lcom/intsig/office/fc/hslf/model/Hyperlink;

    invoke-direct {v3}, Lcom/intsig/office/fc/hslf/model/Hyperlink;-><init>()V

    .line 27
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/record/ExHyperlink;->getLinkTitle()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/intsig/office/fc/hslf/model/Hyperlink;->title:Ljava/lang/String;

    .line 28
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/record/ExHyperlink;->getLinkURL()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/intsig/office/fc/hslf/model/Hyperlink;->address:Ljava/lang/String;

    .line 29
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;->getAction()B

    move-result v1

    iput v1, v3, Lcom/intsig/office/fc/hslf/model/Hyperlink;->type:I

    add-int/lit8 v0, v0, 0x1

    .line 30
    array-length v1, p0

    if-ge v0, v1, :cond_0

    aget-object v1, p0, v0

    instance-of v2, v1, Lcom/intsig/office/fc/hslf/record/TxInteractiveInfoAtom;

    if-eqz v2, :cond_0

    .line 31
    check-cast v1, Lcom/intsig/office/fc/hslf/record/TxInteractiveInfoAtom;

    .line 32
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/TxInteractiveInfoAtom;->getStartIndex()I

    move-result v2

    iput v2, v3, Lcom/intsig/office/fc/hslf/model/Hyperlink;->startIndex:I

    .line 33
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/TxInteractiveInfoAtom;->getEndIndex()I

    move-result v1

    iput v1, v3, Lcom/intsig/office/fc/hslf/model/Hyperlink;->endIndex:I

    .line 34
    :cond_0
    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected static find(Lcom/intsig/office/fc/hslf/model/TextRun;)[Lcom/intsig/office/fc/hslf/model/Hyperlink;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/TextRun;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSlideShow()Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    move-result-object v1

    .line 3
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getDocumentRecord()Lcom/intsig/office/fc/hslf/record/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/Document;->getExObjList()Lcom/intsig/office/fc/hslf/record/ExObjList;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return-object v2

    .line 4
    :cond_0
    iget-object p0, p0, Lcom/intsig/office/fc/hslf/model/TextRun;->_records:[Lcom/intsig/office/fc/hslf/record/Record;

    if-eqz p0, :cond_1

    .line 5
    invoke-static {p0, v1, v0}, Lcom/intsig/office/fc/hslf/model/Hyperlink;->find([Lcom/intsig/office/fc/hslf/record/Record;Lcom/intsig/office/fc/hslf/record/ExObjList;Ljava/util/List;)V

    .line 6
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p0

    if-lez p0, :cond_2

    .line 7
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p0

    new-array v2, p0, [Lcom/intsig/office/fc/hslf/model/Hyperlink;

    .line 8
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    :cond_2
    return-object v2
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->address:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEndIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->endIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->id:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStartIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->startIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->title:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->type:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->address:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setId(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->id:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->title:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setType(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->type:I

    .line 2
    .line 3
    if-eqz p1, :cond_3

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    if-eq p1, v0, :cond_2

    .line 7
    .line 8
    const/4 v0, 0x2

    .line 9
    if-eq p1, v0, :cond_1

    .line 10
    .line 11
    const/4 v0, 0x3

    .line 12
    if-eq p1, v0, :cond_0

    .line 13
    .line 14
    const-string p1, ""

    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->title:Ljava/lang/String;

    .line 17
    .line 18
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->address:Ljava/lang/String;

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const-string p1, "LAST"

    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->title:Ljava/lang/String;

    .line 24
    .line 25
    const-string p1, "1,-1,LAST"

    .line 26
    .line 27
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->address:Ljava/lang/String;

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    const-string p1, "FIRST"

    .line 31
    .line 32
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->title:Ljava/lang/String;

    .line 33
    .line 34
    const-string p1, "1,-1,FIRST"

    .line 35
    .line 36
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->address:Ljava/lang/String;

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_2
    const-string p1, "PREV"

    .line 40
    .line 41
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->title:Ljava/lang/String;

    .line 42
    .line 43
    const-string p1, "1,-1,PREV"

    .line 44
    .line 45
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->address:Ljava/lang/String;

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_3
    const-string p1, "NEXT"

    .line 49
    .line 50
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->title:Ljava/lang/String;

    .line 51
    .line 52
    const-string p1, "1,-1,NEXT"

    .line 53
    .line 54
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Hyperlink;->address:Ljava/lang/String;

    .line 55
    .line 56
    :goto_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.class public final Lcom/intsig/office/fc/hslf/model/Table;
.super Lcom/intsig/office/fc/hslf/model/ShapeGroup;
.source "Table.java"


# static fields
.field protected static final BORDERS_ALL:I = 0x5

.field protected static final BORDERS_INSIDE:I = 0x7

.field protected static final BORDERS_NONE:I = 0x8

.field protected static final BORDERS_OUTSIDE:I = 0x6

.field protected static final BORDER_BOTTOM:I = 0x3

.field protected static final BORDER_LEFT:I = 0x4

.field protected static final BORDER_RIGHT:I = 0x2

.field protected static final BORDER_TOP:I = 0x1


# instance fields
.field protected borders:[Lcom/intsig/office/fc/hslf/model/Line;

.field protected cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;


# direct methods
.method public constructor <init>(II)V
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;-><init>()V

    const/4 v0, 0x1

    if-lt p1, v0, :cond_3

    if-lt p2, v0, :cond_2

    const/4 v1, 0x2

    new-array v1, v1, [I

    aput p2, v1, v0

    const/4 p2, 0x0

    aput p1, v1, p2

    .line 2
    const-class v2, Lcom/intsig/office/fc/hslf/model/TableCell;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[Lcom/intsig/office/fc/hslf/model/TableCell;

    iput-object v1, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 3
    :goto_0
    iget-object v4, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    array-length v4, v4

    if-ge v1, v4, :cond_1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 4
    :goto_1
    iget-object v5, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    aget-object v5, v5, v1

    array-length v6, v5

    if-ge v4, v6, :cond_0

    .line 5
    new-instance v6, Lcom/intsig/office/fc/hslf/model/TableCell;

    invoke-direct {v6, p0}, Lcom/intsig/office/fc/hslf/model/TableCell;-><init>(Lcom/intsig/office/fc/hslf/model/Shape;)V

    aput-object v6, v5, v4

    .line 6
    new-instance v5, Lcom/intsig/office/java/awt/Rectangle;

    const/16 v6, 0x64

    const/16 v7, 0x28

    invoke-direct {v5, v2, v3, v6, v7}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    .line 7
    iget-object v6, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    aget-object v6, v6, v1

    aget-object v6, v6, v4

    invoke-virtual {v6, v5}, Lcom/intsig/office/fc/hslf/model/TableCell;->setAnchor(Lcom/intsig/office/java/awt/Rectangle;)V

    add-int/lit8 v2, v2, 0x64

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x28

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 8
    :cond_1
    new-instance v1, Lcom/intsig/office/java/awt/Rectangle;

    invoke-direct {v1, p2, p2, v2, v3}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->setAnchor(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChild(I)Lcom/intsig/office/fc/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 10
    new-instance v2, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    invoke-direct {v2}, Lcom/intsig/office/fc/ddf/EscherOptRecord;-><init>()V

    const/16 v3, -0xede

    .line 11
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 12
    new-instance v3, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    const/16 v4, 0x39f

    invoke-direct {v3, v4, v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 13
    new-instance v3, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    const/16 v4, 0x43a0

    const/4 v5, 0x0

    invoke-direct {v3, v4, p2, v5}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;-><init>(SZ[B)V

    const/4 p2, 0x4

    .line 14
    invoke-virtual {v3, p2}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->setSizeOfElements(I)V

    .line 15
    invoke-virtual {v3, p1}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->setNumberOfElementsInArray(I)V

    .line 16
    invoke-virtual {v3, p1}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->setNumberOfElementsInMemory(I)V

    .line 17
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 18
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object p1

    .line 19
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v0

    invoke-interface {p1, p2, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 20
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->setChildRecords(Ljava/util/List;)V

    return-void

    .line 21
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The number of columns must be greater than 1"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 22
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The number of rows must be greater than 1"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    return-void
.end method

.method private cloneBorder(Lcom/intsig/office/fc/hslf/model/Line;)Lcom/intsig/office/fc/hslf/model/Line;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Table;->createBorder()Lcom/intsig/office/fc/hslf/model/Line;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getLineWidth()D

    .line 6
    .line 7
    .line 8
    move-result-wide v1

    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->setLineWidth(D)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getLineStyle()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->setLineStyle(I)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getLineDashing()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->setLineDashing(I)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getLineColor()Lcom/intsig/office/java/awt/Color;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->setLineColor(Lcom/intsig/office/java/awt/Color;)V

    .line 31
    .line 32
    .line 33
    return-object v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method protected afterInsert(Lcom/intsig/office/fc/hslf/model/Sheet;)V
    .locals 5

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->afterInsert(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    const/4 v0, 0x0

    .line 9
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChild(I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    check-cast p1, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    add-int/lit8 v1, v1, -0x2

    .line 24
    .line 25
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    check-cast p1, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 30
    .line 31
    const/4 v1, 0x1

    .line 32
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->getEscherProperty(I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    check-cast p1, Lcom/intsig/office/fc/ddf/EscherArrayProperty;

    .line 37
    .line 38
    const/4 v1, 0x0

    .line 39
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 40
    .line 41
    array-length v3, v2

    .line 42
    if-ge v1, v3, :cond_5

    .line 43
    .line 44
    aget-object v2, v2, v1

    .line 45
    .line 46
    aget-object v2, v2, v0

    .line 47
    .line 48
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor()Lcom/intsig/office/java/awt/Rectangle;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    iget v2, v2, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 53
    .line 54
    mul-int/lit16 v2, v2, 0x240

    .line 55
    .line 56
    int-to-float v2, v2

    .line 57
    const/high16 v3, 0x42900000    # 72.0f

    .line 58
    .line 59
    div-float/2addr v2, v3

    .line 60
    float-to-int v2, v2

    .line 61
    const/4 v3, 0x4

    .line 62
    new-array v3, v3, [B

    .line 63
    .line 64
    invoke-static {v3, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BI)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p1, v1, v3}, Lcom/intsig/office/fc/ddf/EscherArrayProperty;->setElement(I[B)V

    .line 68
    .line 69
    .line 70
    const/4 v2, 0x0

    .line 71
    :goto_1
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 72
    .line 73
    aget-object v3, v3, v1

    .line 74
    .line 75
    array-length v4, v3

    .line 76
    if-ge v2, v4, :cond_4

    .line 77
    .line 78
    aget-object v3, v3, v2

    .line 79
    .line 80
    invoke-virtual {p0, v3}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->addShape(Lcom/intsig/office/fc/hslf/model/Shape;)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/TableCell;->getBorderTop()Lcom/intsig/office/fc/hslf/model/Line;

    .line 84
    .line 85
    .line 86
    move-result-object v4

    .line 87
    if-eqz v4, :cond_0

    .line 88
    .line 89
    invoke-virtual {p0, v4}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->addShape(Lcom/intsig/office/fc/hslf/model/Shape;)V

    .line 90
    .line 91
    .line 92
    :cond_0
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/TableCell;->getBorderRight()Lcom/intsig/office/fc/hslf/model/Line;

    .line 93
    .line 94
    .line 95
    move-result-object v4

    .line 96
    if-eqz v4, :cond_1

    .line 97
    .line 98
    invoke-virtual {p0, v4}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->addShape(Lcom/intsig/office/fc/hslf/model/Shape;)V

    .line 99
    .line 100
    .line 101
    :cond_1
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/TableCell;->getBorderBottom()Lcom/intsig/office/fc/hslf/model/Line;

    .line 102
    .line 103
    .line 104
    move-result-object v4

    .line 105
    if-eqz v4, :cond_2

    .line 106
    .line 107
    invoke-virtual {p0, v4}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->addShape(Lcom/intsig/office/fc/hslf/model/Shape;)V

    .line 108
    .line 109
    .line 110
    :cond_2
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/TableCell;->getBorderLeft()Lcom/intsig/office/fc/hslf/model/Line;

    .line 111
    .line 112
    .line 113
    move-result-object v3

    .line 114
    if-eqz v3, :cond_3

    .line 115
    .line 116
    invoke-virtual {p0, v3}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->addShape(Lcom/intsig/office/fc/hslf/model/Shape;)V

    .line 117
    .line 118
    .line 119
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 120
    .line 121
    goto :goto_1

    .line 122
    :cond_4
    add-int/lit8 v1, v1, 0x1

    .line 123
    .line 124
    goto :goto_0

    .line 125
    :cond_5
    return-void
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public createBorder()Lcom/intsig/office/fc/hslf/model/Line;
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hslf/model/Line;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hslf/model/Line;-><init>(Lcom/intsig/office/fc/hslf/model/Shape;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    const/16 v2, -0xff5

    .line 11
    .line 12
    invoke-static {v1, v2}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 17
    .line 18
    const/16 v2, 0x144

    .line 19
    .line 20
    const/4 v3, -0x1

    .line 21
    invoke-static {v1, v2, v3}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 22
    .line 23
    .line 24
    const/16 v2, 0x17f

    .line 25
    .line 26
    invoke-static {v1, v2, v3}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 27
    .line 28
    .line 29
    const/16 v2, 0x23f

    .line 30
    .line 31
    const/high16 v3, 0x20000

    .line 32
    .line 33
    invoke-static {v1, v2, v3}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 34
    .line 35
    .line 36
    const/16 v2, 0x2bf

    .line 37
    .line 38
    const/high16 v3, 0x80000

    .line 39
    .line 40
    invoke-static {v1, v2, v3}, Lcom/intsig/office/fc/hslf/model/Shape;->setEscherProperty(Lcom/intsig/office/fc/ddf/EscherOptRecord;SI)V

    .line 41
    .line 42
    .line 43
    return-object v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getCell(II)Lcom/intsig/office/fc/hslf/model/TableCell;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 2
    .line 3
    aget-object p1, v0, p1

    .line 4
    .line 5
    aget-object p1, p1, p2

    .line 6
    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public getNumberOfColumns()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    aget-object v0, v0, v1

    .line 5
    .line 6
    array-length v0, v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNumberOfRows()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTableBorders()[Lcom/intsig/office/fc/hslf/model/Line;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Table;->borders:[Lcom/intsig/office/fc/hslf/model/Line;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected initTable()V
    .locals 11

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->getShapes()[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/office/fc/hslf/model/Table$1;

    .line 6
    .line 7
    invoke-direct {v1, p0}, Lcom/intsig/office/fc/hslf/model/Table$1;-><init>(Lcom/intsig/office/fc/hslf/model/Table;)V

    .line 8
    .line 9
    .line 10
    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 11
    .line 12
    .line 13
    new-instance v1, Ljava/util/ArrayList;

    .line 14
    .line 15
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 16
    .line 17
    .line 18
    new-instance v2, Ljava/util/ArrayList;

    .line 19
    .line 20
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 21
    .line 22
    .line 23
    const/4 v3, -0x1

    .line 24
    const/4 v4, 0x0

    .line 25
    const/4 v5, 0x0

    .line 26
    const/4 v6, 0x0

    .line 27
    const/4 v7, 0x0

    .line 28
    :goto_0
    array-length v8, v0

    .line 29
    if-ge v6, v8, :cond_3

    .line 30
    .line 31
    aget-object v8, v0, v6

    .line 32
    .line 33
    instance-of v9, v8, Lcom/intsig/office/fc/hslf/model/TextShape;

    .line 34
    .line 35
    if-eqz v9, :cond_1

    .line 36
    .line 37
    invoke-virtual {v8}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor()Lcom/intsig/office/java/awt/Rectangle;

    .line 38
    .line 39
    .line 40
    move-result-object v8

    .line 41
    iget v8, v8, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 42
    .line 43
    if-eq v8, v3, :cond_0

    .line 44
    .line 45
    new-instance v5, Ljava/util/ArrayList;

    .line 46
    .line 47
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    move v3, v8

    .line 54
    :cond_0
    aget-object v8, v0, v6

    .line 55
    .line 56
    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    .line 60
    .line 61
    .line 62
    move-result v8

    .line 63
    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    .line 64
    .line 65
    .line 66
    move-result v7

    .line 67
    goto :goto_1

    .line 68
    :cond_1
    instance-of v9, v8, Lcom/intsig/office/fc/hslf/model/Line;

    .line 69
    .line 70
    if-eqz v9, :cond_2

    .line 71
    .line 72
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    .line 74
    .line 75
    :cond_2
    :goto_1
    add-int/lit8 v6, v6, 0x1

    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 79
    .line 80
    .line 81
    move-result v0

    .line 82
    const/4 v3, 0x2

    .line 83
    new-array v3, v3, [I

    .line 84
    .line 85
    const/4 v5, 0x1

    .line 86
    aput v7, v3, v5

    .line 87
    .line 88
    aput v0, v3, v4

    .line 89
    .line 90
    const-class v0, Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 91
    .line 92
    invoke-static {v0, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    check-cast v0, [[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 97
    .line 98
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 99
    .line 100
    const/4 v0, 0x0

    .line 101
    :goto_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 102
    .line 103
    .line 104
    move-result v3

    .line 105
    if-ge v0, v3, :cond_5

    .line 106
    .line 107
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 108
    .line 109
    .line 110
    move-result-object v3

    .line 111
    check-cast v3, Ljava/util/ArrayList;

    .line 112
    .line 113
    const/4 v5, 0x0

    .line 114
    :goto_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    .line 115
    .line 116
    .line 117
    move-result v6

    .line 118
    if-ge v5, v6, :cond_4

    .line 119
    .line 120
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 121
    .line 122
    .line 123
    move-result-object v6

    .line 124
    check-cast v6, Lcom/intsig/office/fc/hslf/model/TextShape;

    .line 125
    .line 126
    iget-object v7, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 127
    .line 128
    aget-object v7, v7, v0

    .line 129
    .line 130
    new-instance v8, Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 131
    .line 132
    invoke-virtual {v6}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 133
    .line 134
    .line 135
    move-result-object v9

    .line 136
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getParent()Lcom/intsig/office/fc/hslf/model/Shape;

    .line 137
    .line 138
    .line 139
    move-result-object v10

    .line 140
    invoke-direct {v8, v9, v10}, Lcom/intsig/office/fc/hslf/model/TableCell;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hslf/model/Shape;)V

    .line 141
    .line 142
    .line 143
    aput-object v8, v7, v5

    .line 144
    .line 145
    iget-object v7, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 146
    .line 147
    aget-object v7, v7, v0

    .line 148
    .line 149
    aget-object v7, v7, v5

    .line 150
    .line 151
    invoke-virtual {v6}, Lcom/intsig/office/fc/hslf/model/TextShape;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 152
    .line 153
    .line 154
    move-result-object v6

    .line 155
    invoke-virtual {v7, v6}, Lcom/intsig/office/fc/hslf/model/TextShape;->setSheet(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    .line 156
    .line 157
    .line 158
    add-int/lit8 v5, v5, 0x1

    .line 159
    .line 160
    goto :goto_3

    .line 161
    :cond_4
    add-int/lit8 v0, v0, 0x1

    .line 162
    .line 163
    goto :goto_2

    .line 164
    :cond_5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 165
    .line 166
    .line 167
    move-result v0

    .line 168
    new-array v0, v0, [Lcom/intsig/office/fc/hslf/model/Line;

    .line 169
    .line 170
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Table;->borders:[Lcom/intsig/office/fc/hslf/model/Line;

    .line 171
    .line 172
    :goto_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 173
    .line 174
    .line 175
    move-result v0

    .line 176
    if-ge v4, v0, :cond_6

    .line 177
    .line 178
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Table;->borders:[Lcom/intsig/office/fc/hslf/model/Line;

    .line 179
    .line 180
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 181
    .line 182
    .line 183
    move-result-object v1

    .line 184
    check-cast v1, Lcom/intsig/office/fc/hslf/model/Line;

    .line 185
    .line 186
    aput-object v1, v0, v4

    .line 187
    .line 188
    add-int/lit8 v4, v4, 0x1

    .line 189
    .line 190
    goto :goto_4

    .line 191
    :cond_6
    return-void
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public setAllBorders(Lcom/intsig/office/fc/hslf/model/Line;)V
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 4
    .line 5
    array-length v2, v2

    .line 6
    if-ge v1, v2, :cond_3

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    :goto_1
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 10
    .line 11
    aget-object v3, v3, v1

    .line 12
    .line 13
    array-length v4, v3

    .line 14
    if-ge v2, v4, :cond_2

    .line 15
    .line 16
    aget-object v3, v3, v2

    .line 17
    .line 18
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hslf/model/Table;->cloneBorder(Lcom/intsig/office/fc/hslf/model/Line;)Lcom/intsig/office/fc/hslf/model/Line;

    .line 19
    .line 20
    .line 21
    move-result-object v4

    .line 22
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderTop(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hslf/model/Table;->cloneBorder(Lcom/intsig/office/fc/hslf/model/Line;)Lcom/intsig/office/fc/hslf/model/Line;

    .line 26
    .line 27
    .line 28
    move-result-object v4

    .line 29
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderLeft(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 30
    .line 31
    .line 32
    iget-object v4, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 33
    .line 34
    aget-object v4, v4, v1

    .line 35
    .line 36
    array-length v4, v4

    .line 37
    add-int/lit8 v4, v4, -0x1

    .line 38
    .line 39
    if-ne v2, v4, :cond_0

    .line 40
    .line 41
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hslf/model/Table;->cloneBorder(Lcom/intsig/office/fc/hslf/model/Line;)Lcom/intsig/office/fc/hslf/model/Line;

    .line 42
    .line 43
    .line 44
    move-result-object v4

    .line 45
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderRight(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 46
    .line 47
    .line 48
    :cond_0
    iget-object v4, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 49
    .line 50
    array-length v4, v4

    .line 51
    add-int/lit8 v4, v4, -0x1

    .line 52
    .line 53
    if-ne v1, v4, :cond_1

    .line 54
    .line 55
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hslf/model/Table;->cloneBorder(Lcom/intsig/office/fc/hslf/model/Line;)Lcom/intsig/office/fc/hslf/model/Line;

    .line 56
    .line 57
    .line 58
    move-result-object v4

    .line 59
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderBottom(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 60
    .line 61
    .line 62
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 63
    .line 64
    goto :goto_1

    .line 65
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_3
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setColumnWidth(II)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    aget-object v0, v0, v1

    .line 5
    .line 6
    aget-object v0, v0, p1

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor()Lcom/intsig/office/java/awt/Rectangle;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 13
    .line 14
    sub-int v0, p2, v0

    .line 15
    .line 16
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 17
    .line 18
    array-length v3, v2

    .line 19
    if-ge v1, v3, :cond_1

    .line 20
    .line 21
    aget-object v2, v2, v1

    .line 22
    .line 23
    aget-object v2, v2, p1

    .line 24
    .line 25
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor()Lcom/intsig/office/java/awt/Rectangle;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    iput p2, v2, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 30
    .line 31
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 32
    .line 33
    aget-object v3, v3, v1

    .line 34
    .line 35
    aget-object v3, v3, p1

    .line 36
    .line 37
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/hslf/model/TableCell;->setAnchor(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 38
    .line 39
    .line 40
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 41
    .line 42
    aget-object v2, v2, v1

    .line 43
    .line 44
    array-length v2, v2

    .line 45
    add-int/lit8 v2, v2, -0x1

    .line 46
    .line 47
    if-ge p1, v2, :cond_0

    .line 48
    .line 49
    add-int/lit8 v2, p1, 0x1

    .line 50
    .line 51
    :goto_1
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 52
    .line 53
    aget-object v3, v3, v1

    .line 54
    .line 55
    array-length v4, v3

    .line 56
    if-ge v2, v4, :cond_0

    .line 57
    .line 58
    aget-object v3, v3, v2

    .line 59
    .line 60
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor()Lcom/intsig/office/java/awt/Rectangle;

    .line 61
    .line 62
    .line 63
    move-result-object v3

    .line 64
    iget v4, v3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 65
    .line 66
    add-int/2addr v4, v0

    .line 67
    iput v4, v3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 68
    .line 69
    iget-object v4, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 70
    .line 71
    aget-object v4, v4, v1

    .line 72
    .line 73
    aget-object v4, v4, v2

    .line 74
    .line 75
    invoke-virtual {v4, v3}, Lcom/intsig/office/fc/hslf/model/TableCell;->setAnchor(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 76
    .line 77
    .line 78
    add-int/lit8 v2, v2, 0x1

    .line 79
    .line 80
    goto :goto_1

    .line 81
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 82
    .line 83
    goto :goto_0

    .line 84
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor()Lcom/intsig/office/java/awt/Rectangle;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    iget p2, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 89
    .line 90
    add-int/2addr p2, v0

    .line 91
    iput p2, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 92
    .line 93
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->setAnchor(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 94
    .line 95
    .line 96
    return-void
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public setInsideBorders(Lcom/intsig/office/fc/hslf/model/Line;)V
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 4
    .line 5
    array-length v2, v2

    .line 6
    if-ge v1, v2, :cond_3

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    :goto_1
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 10
    .line 11
    aget-object v3, v3, v1

    .line 12
    .line 13
    array-length v4, v3

    .line 14
    if-ge v2, v4, :cond_2

    .line 15
    .line 16
    aget-object v4, v3, v2

    .line 17
    .line 18
    array-length v3, v3

    .line 19
    add-int/lit8 v3, v3, -0x1

    .line 20
    .line 21
    const/4 v5, 0x0

    .line 22
    if-eq v2, v3, :cond_0

    .line 23
    .line 24
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hslf/model/Table;->cloneBorder(Lcom/intsig/office/fc/hslf/model/Line;)Lcom/intsig/office/fc/hslf/model/Line;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    invoke-virtual {v4, v3}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderRight(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 29
    .line 30
    .line 31
    goto :goto_2

    .line 32
    :cond_0
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderLeft(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderLeft(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 36
    .line 37
    .line 38
    :goto_2
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 39
    .line 40
    array-length v3, v3

    .line 41
    add-int/lit8 v3, v3, -0x1

    .line 42
    .line 43
    if-eq v1, v3, :cond_1

    .line 44
    .line 45
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hslf/model/Table;->cloneBorder(Lcom/intsig/office/fc/hslf/model/Line;)Lcom/intsig/office/fc/hslf/model/Line;

    .line 46
    .line 47
    .line 48
    move-result-object v3

    .line 49
    invoke-virtual {v4, v3}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderBottom(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 50
    .line 51
    .line 52
    goto :goto_3

    .line 53
    :cond_1
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderTop(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderBottom(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 57
    .line 58
    .line 59
    :goto_3
    add-int/lit8 v2, v2, 0x1

    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_3
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setOutsideBorders(Lcom/intsig/office/fc/hslf/model/Line;)V
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 4
    .line 5
    array-length v2, v2

    .line 6
    if-ge v1, v2, :cond_5

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    :goto_1
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 10
    .line 11
    aget-object v3, v3, v1

    .line 12
    .line 13
    array-length v4, v3

    .line 14
    if-ge v2, v4, :cond_4

    .line 15
    .line 16
    aget-object v3, v3, v2

    .line 17
    .line 18
    if-nez v2, :cond_0

    .line 19
    .line 20
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hslf/model/Table;->cloneBorder(Lcom/intsig/office/fc/hslf/model/Line;)Lcom/intsig/office/fc/hslf/model/Line;

    .line 21
    .line 22
    .line 23
    move-result-object v4

    .line 24
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderLeft(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 25
    .line 26
    .line 27
    :cond_0
    iget-object v4, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 28
    .line 29
    aget-object v4, v4, v1

    .line 30
    .line 31
    array-length v4, v4

    .line 32
    add-int/lit8 v4, v4, -0x1

    .line 33
    .line 34
    const/4 v5, 0x0

    .line 35
    if-ne v2, v4, :cond_1

    .line 36
    .line 37
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hslf/model/Table;->cloneBorder(Lcom/intsig/office/fc/hslf/model/Line;)Lcom/intsig/office/fc/hslf/model/Line;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderRight(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 42
    .line 43
    .line 44
    goto :goto_2

    .line 45
    :cond_1
    invoke-virtual {v3, v5}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderLeft(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v3, v5}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderLeft(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 49
    .line 50
    .line 51
    :goto_2
    if-nez v1, :cond_2

    .line 52
    .line 53
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hslf/model/Table;->cloneBorder(Lcom/intsig/office/fc/hslf/model/Line;)Lcom/intsig/office/fc/hslf/model/Line;

    .line 54
    .line 55
    .line 56
    move-result-object v4

    .line 57
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderTop(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 58
    .line 59
    .line 60
    goto :goto_3

    .line 61
    :cond_2
    iget-object v4, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 62
    .line 63
    array-length v4, v4

    .line 64
    add-int/lit8 v4, v4, -0x1

    .line 65
    .line 66
    if-ne v1, v4, :cond_3

    .line 67
    .line 68
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hslf/model/Table;->cloneBorder(Lcom/intsig/office/fc/hslf/model/Line;)Lcom/intsig/office/fc/hslf/model/Line;

    .line 69
    .line 70
    .line 71
    move-result-object v4

    .line 72
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderBottom(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 73
    .line 74
    .line 75
    goto :goto_3

    .line 76
    :cond_3
    invoke-virtual {v3, v5}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderTop(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v3, v5}, Lcom/intsig/office/fc/hslf/model/TableCell;->setBorderBottom(Lcom/intsig/office/fc/hslf/model/Line;)V

    .line 80
    .line 81
    .line 82
    :goto_3
    add-int/lit8 v2, v2, 0x1

    .line 83
    .line 84
    goto :goto_1

    .line 85
    :cond_4
    add-int/lit8 v1, v1, 0x1

    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_5
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public setRowHeight(II)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 2
    .line 3
    aget-object v0, v0, p1

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    aget-object v0, v0, v1

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor()Lcom/intsig/office/java/awt/Rectangle;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 13
    .line 14
    sub-int v0, p2, v0

    .line 15
    .line 16
    move v2, p1

    .line 17
    :goto_0
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 18
    .line 19
    array-length v3, v3

    .line 20
    if-ge v2, v3, :cond_2

    .line 21
    .line 22
    const/4 v3, 0x0

    .line 23
    :goto_1
    iget-object v4, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 24
    .line 25
    aget-object v4, v4, v2

    .line 26
    .line 27
    array-length v5, v4

    .line 28
    if-ge v3, v5, :cond_1

    .line 29
    .line 30
    aget-object v4, v4, v3

    .line 31
    .line 32
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor()Lcom/intsig/office/java/awt/Rectangle;

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    if-ne v2, p1, :cond_0

    .line 37
    .line 38
    iput p2, v4, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 39
    .line 40
    goto :goto_2

    .line 41
    :cond_0
    iget v5, v4, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 42
    .line 43
    add-int/2addr v5, v0

    .line 44
    iput v5, v4, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 45
    .line 46
    :goto_2
    iget-object v5, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 47
    .line 48
    aget-object v5, v5, v2

    .line 49
    .line 50
    aget-object v5, v5, v3

    .line 51
    .line 52
    invoke-virtual {v5, v4}, Lcom/intsig/office/fc/hslf/model/TableCell;->setAnchor(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 53
    .line 54
    .line 55
    add-int/lit8 v3, v3, 0x1

    .line 56
    .line 57
    goto :goto_1

    .line 58
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Shape;->getAnchor()Lcom/intsig/office/java/awt/Rectangle;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    iget p2, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 66
    .line 67
    add-int/2addr p2, v0

    .line 68
    iput p2, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 69
    .line 70
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->setAnchor(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 71
    .line 72
    .line 73
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public setSheet(Lcom/intsig/office/fc/hslf/model/Sheet;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hslf/model/Shape;->setSheet(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/model/Table;->cells:[[Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 5
    .line 6
    if-nez p1, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Table;->initTable()V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public final Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;
.super Lcom/intsig/office/fc/hslf/record/RecordAtom;
.source "StyleTextPropAtom.java"


# static fields
.field private static _type:J = 0xfa1L

.field public static characterTextPropTypes:[Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

.field public static paragraphTextPropTypes:[Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;


# instance fields
.field private _header:[B

.field private charStyles:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;",
            ">;"
        }
    .end annotation
.end field

.field private initialised:Z

.field private paraAutoNumberIndexs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private paragraphStyles:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;",
            ">;"
        }
    .end annotation
.end field

.field private rawContents:[B

.field private reserved:[B


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    const/16 v0, 0x16

    .line 2
    .line 3
    new-array v1, v0, [Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 4
    .line 5
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 6
    .line 7
    const-string v3, "hasBullet"

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    const/4 v5, 0x1

    .line 11
    invoke-direct {v2, v4, v5, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 12
    .line 13
    .line 14
    aput-object v2, v1, v4

    .line 15
    .line 16
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 17
    .line 18
    const-string v3, "hasBulletFont"

    .line 19
    .line 20
    const/4 v6, 0x2

    .line 21
    invoke-direct {v2, v4, v6, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 22
    .line 23
    .line 24
    aput-object v2, v1, v5

    .line 25
    .line 26
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 27
    .line 28
    const-string v3, "hasBulletColor"

    .line 29
    .line 30
    const/4 v7, 0x4

    .line 31
    invoke-direct {v2, v4, v7, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 32
    .line 33
    .line 34
    aput-object v2, v1, v6

    .line 35
    .line 36
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 37
    .line 38
    const-string v3, "hasBulletSize"

    .line 39
    .line 40
    const/16 v8, 0x8

    .line 41
    .line 42
    invoke-direct {v2, v4, v8, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 43
    .line 44
    .line 45
    const/4 v3, 0x3

    .line 46
    aput-object v2, v1, v3

    .line 47
    .line 48
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/ParagraphFlagsTextProp;

    .line 49
    .line 50
    invoke-direct {v2}, Lcom/intsig/office/fc/hslf/model/textproperties/ParagraphFlagsTextProp;-><init>()V

    .line 51
    .line 52
    .line 53
    aput-object v2, v1, v7

    .line 54
    .line 55
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 56
    .line 57
    const-string v9, "bullet.char"

    .line 58
    .line 59
    const/16 v10, 0x80

    .line 60
    .line 61
    invoke-direct {v2, v6, v10, v9}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 62
    .line 63
    .line 64
    const/4 v9, 0x5

    .line 65
    aput-object v2, v1, v9

    .line 66
    .line 67
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 68
    .line 69
    const-string v11, "bullet.font"

    .line 70
    .line 71
    const/16 v12, 0x10

    .line 72
    .line 73
    invoke-direct {v2, v6, v12, v11}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 74
    .line 75
    .line 76
    const/4 v11, 0x6

    .line 77
    aput-object v2, v1, v11

    .line 78
    .line 79
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 80
    .line 81
    const-string v13, "bullet.size"

    .line 82
    .line 83
    const/16 v14, 0x40

    .line 84
    .line 85
    invoke-direct {v2, v6, v14, v13}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 86
    .line 87
    .line 88
    const/4 v13, 0x7

    .line 89
    aput-object v2, v1, v13

    .line 90
    .line 91
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 92
    .line 93
    const-string v15, "bullet.color"

    .line 94
    .line 95
    const/16 v0, 0x20

    .line 96
    .line 97
    invoke-direct {v2, v7, v0, v15}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 98
    .line 99
    .line 100
    aput-object v2, v1, v8

    .line 101
    .line 102
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/AlignmentTextProp;

    .line 103
    .line 104
    invoke-direct {v2}, Lcom/intsig/office/fc/hslf/model/textproperties/AlignmentTextProp;-><init>()V

    .line 105
    .line 106
    .line 107
    const/16 v15, 0x9

    .line 108
    .line 109
    aput-object v2, v1, v15

    .line 110
    .line 111
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 112
    .line 113
    const-string v15, "text.offset"

    .line 114
    .line 115
    const/16 v13, 0x100

    .line 116
    .line 117
    invoke-direct {v2, v6, v13, v15}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 118
    .line 119
    .line 120
    const/16 v15, 0xa

    .line 121
    .line 122
    aput-object v2, v1, v15

    .line 123
    .line 124
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 125
    .line 126
    const-string v15, "bullet.offset"

    .line 127
    .line 128
    const/16 v13, 0x400

    .line 129
    .line 130
    invoke-direct {v2, v6, v13, v15}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 131
    .line 132
    .line 133
    const/16 v15, 0xb

    .line 134
    .line 135
    aput-object v2, v1, v15

    .line 136
    .line 137
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 138
    .line 139
    const-string v15, "linespacing"

    .line 140
    .line 141
    const/16 v13, 0x1000

    .line 142
    .line 143
    invoke-direct {v2, v6, v13, v15}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 144
    .line 145
    .line 146
    const/16 v15, 0xc

    .line 147
    .line 148
    aput-object v2, v1, v15

    .line 149
    .line 150
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 151
    .line 152
    const/16 v15, 0x2000

    .line 153
    .line 154
    const-string v13, "spacebefore"

    .line 155
    .line 156
    invoke-direct {v2, v6, v15, v13}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 157
    .line 158
    .line 159
    const/16 v13, 0xd

    .line 160
    .line 161
    aput-object v2, v1, v13

    .line 162
    .line 163
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 164
    .line 165
    const/16 v15, 0x4000

    .line 166
    .line 167
    const-string v13, "spaceafter"

    .line 168
    .line 169
    invoke-direct {v2, v6, v15, v13}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 170
    .line 171
    .line 172
    const/16 v13, 0xe

    .line 173
    .line 174
    aput-object v2, v1, v13

    .line 175
    .line 176
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 177
    .line 178
    const v13, 0x8000

    .line 179
    .line 180
    .line 181
    const-string v15, "defaultTabSize"

    .line 182
    .line 183
    invoke-direct {v2, v6, v13, v15}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 184
    .line 185
    .line 186
    const/16 v13, 0xf

    .line 187
    .line 188
    aput-object v2, v1, v13

    .line 189
    .line 190
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 191
    .line 192
    const/high16 v13, 0x100000

    .line 193
    .line 194
    const-string v15, "tabStops"

    .line 195
    .line 196
    invoke-direct {v2, v6, v13, v15}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 197
    .line 198
    .line 199
    aput-object v2, v1, v12

    .line 200
    .line 201
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 202
    .line 203
    const/high16 v13, 0x10000

    .line 204
    .line 205
    const-string v15, "fontAlign"

    .line 206
    .line 207
    invoke-direct {v2, v6, v13, v15}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 208
    .line 209
    .line 210
    const/16 v13, 0x11

    .line 211
    .line 212
    aput-object v2, v1, v13

    .line 213
    .line 214
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 215
    .line 216
    const/high16 v13, 0xe0000

    .line 217
    .line 218
    const-string v15, "wrapFlags"

    .line 219
    .line 220
    invoke-direct {v2, v6, v13, v15}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 221
    .line 222
    .line 223
    const/16 v13, 0x12

    .line 224
    .line 225
    aput-object v2, v1, v13

    .line 226
    .line 227
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 228
    .line 229
    const/high16 v13, 0x200000

    .line 230
    .line 231
    const-string v15, "textDirection"

    .line 232
    .line 233
    invoke-direct {v2, v6, v13, v15}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 234
    .line 235
    .line 236
    const/16 v13, 0x13

    .line 237
    .line 238
    aput-object v2, v1, v13

    .line 239
    .line 240
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 241
    .line 242
    const/high16 v13, 0x1000000

    .line 243
    .line 244
    const-string v15, "buletScheme"

    .line 245
    .line 246
    invoke-direct {v2, v6, v13, v15}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 247
    .line 248
    .line 249
    const/16 v13, 0x14

    .line 250
    .line 251
    aput-object v2, v1, v13

    .line 252
    .line 253
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 254
    .line 255
    const/high16 v13, 0x2000000

    .line 256
    .line 257
    const-string v15, "bulletHasScheme"

    .line 258
    .line 259
    invoke-direct {v2, v6, v13, v15}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 260
    .line 261
    .line 262
    const/16 v13, 0x15

    .line 263
    .line 264
    aput-object v2, v1, v13

    .line 265
    .line 266
    sput-object v1, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphTextPropTypes:[Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 267
    .line 268
    const/16 v1, 0x19

    .line 269
    .line 270
    new-array v1, v1, [Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 271
    .line 272
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 273
    .line 274
    const-string v13, "bold"

    .line 275
    .line 276
    invoke-direct {v2, v4, v5, v13}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 277
    .line 278
    .line 279
    aput-object v2, v1, v4

    .line 280
    .line 281
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 282
    .line 283
    const-string v13, "italic"

    .line 284
    .line 285
    invoke-direct {v2, v4, v6, v13}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 286
    .line 287
    .line 288
    aput-object v2, v1, v5

    .line 289
    .line 290
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 291
    .line 292
    const-string v5, "underline"

    .line 293
    .line 294
    invoke-direct {v2, v4, v7, v5}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 295
    .line 296
    .line 297
    aput-object v2, v1, v6

    .line 298
    .line 299
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 300
    .line 301
    const-string v5, "unused1"

    .line 302
    .line 303
    invoke-direct {v2, v4, v8, v5}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 304
    .line 305
    .line 306
    aput-object v2, v1, v3

    .line 307
    .line 308
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 309
    .line 310
    const-string v3, "shadow"

    .line 311
    .line 312
    invoke-direct {v2, v4, v12, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 313
    .line 314
    .line 315
    aput-object v2, v1, v7

    .line 316
    .line 317
    new-instance v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 318
    .line 319
    const-string v3, "fehint"

    .line 320
    .line 321
    invoke-direct {v2, v4, v0, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 322
    .line 323
    .line 324
    aput-object v2, v1, v9

    .line 325
    .line 326
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 327
    .line 328
    const-string v2, "unused2"

    .line 329
    .line 330
    invoke-direct {v0, v4, v14, v2}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 331
    .line 332
    .line 333
    aput-object v0, v1, v11

    .line 334
    .line 335
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 336
    .line 337
    const-string v2, "kumi"

    .line 338
    .line 339
    invoke-direct {v0, v4, v10, v2}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 340
    .line 341
    .line 342
    const/4 v2, 0x7

    .line 343
    aput-object v0, v1, v2

    .line 344
    .line 345
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 346
    .line 347
    const-string v2, "unused3"

    .line 348
    .line 349
    const/16 v3, 0x100

    .line 350
    .line 351
    invoke-direct {v0, v4, v3, v2}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 352
    .line 353
    .line 354
    aput-object v0, v1, v8

    .line 355
    .line 356
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 357
    .line 358
    const/16 v2, 0x200

    .line 359
    .line 360
    const-string v3, "emboss"

    .line 361
    .line 362
    invoke-direct {v0, v4, v2, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 363
    .line 364
    .line 365
    const/16 v2, 0x9

    .line 366
    .line 367
    aput-object v0, v1, v2

    .line 368
    .line 369
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 370
    .line 371
    const-string v2, "nibble1"

    .line 372
    .line 373
    const/16 v3, 0x400

    .line 374
    .line 375
    invoke-direct {v0, v4, v3, v2}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 376
    .line 377
    .line 378
    const/16 v2, 0xa

    .line 379
    .line 380
    aput-object v0, v1, v2

    .line 381
    .line 382
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 383
    .line 384
    const/16 v2, 0x800

    .line 385
    .line 386
    const-string v3, "nibble2"

    .line 387
    .line 388
    invoke-direct {v0, v4, v2, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 389
    .line 390
    .line 391
    const/16 v2, 0xb

    .line 392
    .line 393
    aput-object v0, v1, v2

    .line 394
    .line 395
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 396
    .line 397
    const-string v2, "nibble3"

    .line 398
    .line 399
    const/16 v3, 0x1000

    .line 400
    .line 401
    invoke-direct {v0, v4, v3, v2}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 402
    .line 403
    .line 404
    const/16 v2, 0xc

    .line 405
    .line 406
    aput-object v0, v1, v2

    .line 407
    .line 408
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 409
    .line 410
    const/16 v2, 0x2000

    .line 411
    .line 412
    const-string v3, "nibble4"

    .line 413
    .line 414
    invoke-direct {v0, v4, v2, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 415
    .line 416
    .line 417
    const/16 v2, 0xd

    .line 418
    .line 419
    aput-object v0, v1, v2

    .line 420
    .line 421
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 422
    .line 423
    const/16 v2, 0x4000

    .line 424
    .line 425
    const-string v3, "unused4"

    .line 426
    .line 427
    invoke-direct {v0, v4, v2, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 428
    .line 429
    .line 430
    const/16 v2, 0xe

    .line 431
    .line 432
    aput-object v0, v1, v2

    .line 433
    .line 434
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 435
    .line 436
    const v2, 0x8000

    .line 437
    .line 438
    .line 439
    const-string v3, "unused5"

    .line 440
    .line 441
    invoke-direct {v0, v4, v2, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 442
    .line 443
    .line 444
    const/16 v2, 0xf

    .line 445
    .line 446
    aput-object v0, v1, v2

    .line 447
    .line 448
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/CharFlagsTextProp;

    .line 449
    .line 450
    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/model/textproperties/CharFlagsTextProp;-><init>()V

    .line 451
    .line 452
    .line 453
    aput-object v0, v1, v12

    .line 454
    .line 455
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 456
    .line 457
    const/high16 v2, 0x10000

    .line 458
    .line 459
    const-string v3, "font.index"

    .line 460
    .line 461
    invoke-direct {v0, v6, v2, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 462
    .line 463
    .line 464
    const/16 v2, 0x11

    .line 465
    .line 466
    aput-object v0, v1, v2

    .line 467
    .line 468
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 469
    .line 470
    const/high16 v2, 0x100000

    .line 471
    .line 472
    const-string v3, "pp10ext"

    .line 473
    .line 474
    invoke-direct {v0, v4, v2, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 475
    .line 476
    .line 477
    const/16 v2, 0x12

    .line 478
    .line 479
    aput-object v0, v1, v2

    .line 480
    .line 481
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 482
    .line 483
    const/high16 v2, 0x200000

    .line 484
    .line 485
    const-string v3, "asian.font.index"

    .line 486
    .line 487
    invoke-direct {v0, v6, v2, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 488
    .line 489
    .line 490
    const/16 v2, 0x13

    .line 491
    .line 492
    aput-object v0, v1, v2

    .line 493
    .line 494
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 495
    .line 496
    const/high16 v2, 0x400000

    .line 497
    .line 498
    const-string v3, "ansi.font.index"

    .line 499
    .line 500
    invoke-direct {v0, v6, v2, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 501
    .line 502
    .line 503
    const/16 v2, 0x14

    .line 504
    .line 505
    aput-object v0, v1, v2

    .line 506
    .line 507
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 508
    .line 509
    const/high16 v2, 0x800000

    .line 510
    .line 511
    const-string v3, "symbol.font.index"

    .line 512
    .line 513
    invoke-direct {v0, v6, v2, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 514
    .line 515
    .line 516
    const/16 v2, 0x15

    .line 517
    .line 518
    aput-object v0, v1, v2

    .line 519
    .line 520
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 521
    .line 522
    const/high16 v2, 0x20000

    .line 523
    .line 524
    const-string v3, "font.size"

    .line 525
    .line 526
    invoke-direct {v0, v6, v2, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 527
    .line 528
    .line 529
    const/16 v2, 0x16

    .line 530
    .line 531
    aput-object v0, v1, v2

    .line 532
    .line 533
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 534
    .line 535
    const/high16 v2, 0x40000

    .line 536
    .line 537
    const-string v3, "font.color"

    .line 538
    .line 539
    invoke-direct {v0, v7, v2, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 540
    .line 541
    .line 542
    const/16 v2, 0x17

    .line 543
    .line 544
    aput-object v0, v1, v2

    .line 545
    .line 546
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 547
    .line 548
    const/high16 v2, 0x80000

    .line 549
    .line 550
    const-string v3, "superscript"

    .line 551
    .line 552
    invoke-direct {v0, v6, v2, v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;-><init>(IILjava/lang/String;)V

    .line 553
    .line 554
    .line 555
    const/16 v2, 0x18

    .line 556
    .line 557
    aput-object v0, v1, v2

    .line 558
    .line 559
    sput-object v1, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->characterTextPropTypes:[Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 560
    .line 561
    return-void
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
    .line 1825
    .line 1826
    .line 1827
    .line 1828
    .line 1829
    .line 1830
    .line 1831
    .line 1832
    .line 1833
    .line 1834
    .line 1835
    .line 1836
    .line 1837
    .line 1838
    .line 1839
    .line 1840
    .line 1841
    .line 1842
    .line 1843
    .line 1844
    .line 1845
    .line 1846
    .line 1847
    .line 1848
    .line 1849
    .line 1850
    .line 1851
    .line 1852
    .line 1853
    .line 1854
    .line 1855
    .line 1856
    .line 1857
    .line 1858
    .line 1859
    .line 1860
    .line 1861
    .line 1862
    .line 1863
    .line 1864
    .line 1865
    .line 1866
    .line 1867
    .line 1868
    .line 1869
    .line 1870
    .line 1871
    .line 1872
    .line 1873
    .line 1874
    .line 1875
    .line 1876
    .line 1877
    .line 1878
    .line 1879
    .line 1880
    .line 1881
    .line 1882
    .line 1883
    .line 1884
    .line 1885
    .line 1886
    .line 1887
    .line 1888
    .line 1889
    .line 1890
    .line 1891
    .line 1892
    .line 1893
    .line 1894
    .line 1895
    .line 1896
    .line 1897
    .line 1898
    .line 1899
    .line 1900
    .line 1901
    .line 1902
    .line 1903
    .line 1904
    .line 1905
    .line 1906
    .line 1907
    .line 1908
    .line 1909
    .line 1910
    .line 1911
    .line 1912
    .line 1913
    .line 1914
    .line 1915
    .line 1916
    .line 1917
    .line 1918
    .line 1919
    .line 1920
    .line 1921
    .line 1922
    .line 1923
    .line 1924
    .line 1925
    .line 1926
    .line 1927
    .line 1928
    .line 1929
    .line 1930
    .line 1931
    .line 1932
    .line 1933
    .line 1934
    .line 1935
    .line 1936
    .line 1937
    .line 1938
    .line 1939
    .line 1940
    .line 1941
    .line 1942
    .line 1943
    .line 1944
    .line 1945
    .line 1946
    .line 1947
    .line 1948
    .line 1949
    .line 1950
    .line 1951
    .line 1952
    .line 1953
    .line 1954
    .line 1955
    .line 1956
    .line 1957
    .line 1958
    .line 1959
    .line 1960
    .line 1961
    .line 1962
    .line 1963
    .line 1964
    .line 1965
    .line 1966
    .line 1967
    .line 1968
    .line 1969
    .line 1970
    .line 1971
    .line 1972
    .line 1973
    .line 1974
    .line 1975
    .line 1976
    .line 1977
    .line 1978
    .line 1979
    .line 1980
    .line 1981
    .line 1982
    .line 1983
    .line 1984
    .line 1985
    .line 1986
    .line 1987
    .line 1988
    .line 1989
    .line 1990
    .line 1991
    .line 1992
    .line 1993
    .line 1994
    .line 1995
    .line 1996
    .line 1997
    .line 1998
    .line 1999
    .line 2000
    .line 2001
    .line 2002
    .line 2003
    .line 2004
    .line 2005
    .line 2006
    .line 2007
    .line 2008
    .line 2009
    .line 2010
    .line 2011
    .line 2012
    .line 2013
    .line 2014
    .line 2015
    .line 2016
    .line 2017
    .line 2018
    .line 2019
    .line 2020
    .line 2021
    .line 2022
    .line 2023
    .line 2024
    .line 2025
    .line 2026
    .line 2027
    .line 2028
    .line 2029
    .line 2030
    .line 2031
    .line 2032
    .line 2033
    .line 2034
    .line 2035
    .line 2036
    .line 2037
    .line 2038
    .line 2039
    .line 2040
    .line 2041
    .line 2042
    .line 2043
    .line 2044
    .line 2045
    .line 2046
    .line 2047
    .line 2048
    .line 2049
    .line 2050
    .line 2051
    .line 2052
    .line 2053
    .line 2054
    .line 2055
    .line 2056
    .line 2057
    .line 2058
    .line 2059
    .line 2060
    .line 2061
    .line 2062
    .line 2063
    .line 2064
    .line 2065
    .line 2066
    .line 2067
    .line 2068
    .line 2069
    .line 2070
    .line 2071
    .line 2072
    .line 2073
    .line 2074
    .line 2075
    .line 2076
    .line 2077
    .line 2078
    .line 2079
    .line 2080
    .line 2081
    .line 2082
    .line 2083
    .line 2084
    .line 2085
    .line 2086
.end method

.method public constructor <init>(I)V
    .locals 4

    .line 13
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordAtom;-><init>()V

    const/4 v0, 0x0

    .line 14
    iput-boolean v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->initialised:Z

    .line 15
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paraAutoNumberIndexs:Ljava/util/Map;

    const/16 v1, 0x8

    new-array v1, v1, [B

    .line 16
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->_header:[B

    new-array v2, v0, [B

    .line 17
    iput-object v2, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    new-array v2, v0, [B

    .line 18
    iput-object v2, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->reserved:[B

    .line 19
    sget-wide v2, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->_type:J

    long-to-int v3, v2

    int-to-short v2, v3

    const/4 v3, 0x2

    invoke-static {v1, v3, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 20
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->_header:[B

    const/4 v2, 0x4

    const/16 v3, 0xa

    invoke-static {v1, v2, v3}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 21
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    .line 22
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    .line 23
    new-instance v1, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    invoke-direct {v1, p1, v0}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;-><init>(IS)V

    .line 24
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 25
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;-><init>(I)V

    .line 26
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    invoke-virtual {p1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    .line 27
    iput-boolean p1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->initialised:Z

    return-void
.end method

.method public constructor <init>([BII)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/RecordAtom;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->initialised:Z

    .line 3
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paraAutoNumberIndexs:Ljava/util/Map;

    const/16 v1, 0x12

    if-ge p3, v1, :cond_1

    .line 4
    array-length p3, p1

    sub-int/2addr p3, p2

    if-lt p3, v1, :cond_0

    const/16 p3, 0x12

    goto :goto_0

    .line 5
    :cond_0
    new-instance p3, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not enough data to form a StyleTextPropAtom (min size 18 bytes long) - found "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p1, p1

    sub-int/2addr p1, p2

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p3

    :cond_1
    :goto_0
    const/16 v1, 0x8

    new-array v2, v1, [B

    .line 6
    iput-object v2, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->_header:[B

    .line 7
    invoke-static {p1, p2, v2, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sub-int/2addr p3, v1

    .line 8
    new-array p3, p3, [B

    iput-object p3, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    add-int/2addr p2, v1

    .line 9
    array-length v1, p3

    invoke-static {p1, p2, p3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-array p1, v0, [B

    .line 10
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->reserved:[B

    .line 11
    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    .line 12
    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    return-void
.end method

.method private getCharactersCovered(Ljava/util/LinkedList;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList<",
            "Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;",
            ">;)I"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const/4 v0, 0x0

    .line 6
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    check-cast v1, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->getCharactersCovered()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    add-int/2addr v0, v1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    return v0
.end method

.method private updateRawContents()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->initialised:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 9
    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    const/4 v2, 0x0

    .line 13
    :goto_0
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    .line 14
    .line 15
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    if-ge v2, v3, :cond_1

    .line 20
    .line 21
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    .line 22
    .line 23
    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    check-cast v3, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 28
    .line 29
    add-int/lit8 v2, v2, 0x1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    .line 33
    .line 34
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    if-ge v1, v2, :cond_2

    .line 39
    .line 40
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    .line 41
    .line 42
    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    check-cast v2, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 47
    .line 48
    add-int/lit8 v1, v1, 0x1

    .line 49
    .line 50
    goto :goto_1

    .line 51
    :cond_2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method


# virtual methods
.method public addCharacterTextPropCollection(I)Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;-><init>(I)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public addParagraphTextPropCollection(I)Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p1, v1}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;-><init>(IS)V

    .line 5
    .line 6
    .line 7
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    .line 8
    .line 9
    invoke-virtual {p1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->_header:[B

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->reserved:[B

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paraAutoNumberIndexs:Ljava/util/Map;

    .line 9
    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paraAutoNumberIndexs:Ljava/util/Map;

    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAutoNumberIndex(I)I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    const/4 v2, 0x0

    .line 8
    :goto_0
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    .line 9
    .line 10
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    .line 11
    .line 12
    .line 13
    move-result v3

    .line 14
    if-ge v0, v3, :cond_1

    .line 15
    .line 16
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    .line 17
    .line 18
    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v3

    .line 22
    check-cast v3, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 23
    .line 24
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->getCharactersCovered()I

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    add-int/2addr v3, v2

    .line 29
    add-int/lit8 v3, v3, -0x1

    .line 30
    .line 31
    if-lt p1, v2, :cond_0

    .line 32
    .line 33
    if-gt p1, v3, :cond_0

    .line 34
    .line 35
    move v1, v0

    .line 36
    goto :goto_1

    .line 37
    :cond_0
    add-int/lit8 v2, v3, 0x1

    .line 38
    .line 39
    add-int/lit8 v0, v0, 0x1

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    :goto_1
    if-ltz v1, :cond_2

    .line 43
    .line 44
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paraAutoNumberIndexs:Ljava/util/Map;

    .line 45
    .line 46
    invoke-interface {p1}, Ljava/util/Map;->size()I

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    if-ge v1, p1, :cond_2

    .line 51
    .line 52
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paraAutoNumberIndexs:Ljava/util/Map;

    .line 53
    .line 54
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    check-cast p1, Ljava/lang/Integer;

    .line 63
    .line 64
    if-eqz p1, :cond_2

    .line 65
    .line 66
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 67
    .line 68
    .line 69
    move-result p1

    .line 70
    return p1

    .line 71
    :cond_2
    const/4 p1, -0x1

    .line 72
    return p1
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getCharacterStyles()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCharacterTextLengthCovered()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->getCharactersCovered(Ljava/util/LinkedList;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getParagraphStyles()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getParagraphTextLengthCovered()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->getCharactersCovered(Ljava/util/LinkedList;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setCharacterStyles(Ljava/util/LinkedList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList<",
            "Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setParagraphStyles(Ljava/util/LinkedList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList<",
            "Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setParentTextSize(I)V
    .locals 12

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paraAutoNumberIndexs:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    move v3, p1

    .line 8
    const/4 v1, 0x0

    .line 9
    const/4 v2, 0x0

    .line 10
    const/4 v4, 0x0

    .line 11
    const/4 v5, 0x0

    .line 12
    :goto_0
    iget-object v6, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 13
    .line 14
    array-length v7, v6

    .line 15
    const/4 v8, 0x1

    .line 16
    if-ge v1, v7, :cond_7

    .line 17
    .line 18
    if-ge v2, v3, :cond_7

    .line 19
    .line 20
    invoke-static {v6, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 21
    .line 22
    .line 23
    move-result v6

    .line 24
    add-int/2addr v2, v6

    .line 25
    add-int/lit8 v1, v1, 0x4

    .line 26
    .line 27
    iget-object v7, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 28
    .line 29
    invoke-static {v7, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 30
    .line 31
    .line 32
    move-result v7

    .line 33
    const/4 v9, 0x2

    .line 34
    add-int/2addr v1, v9

    .line 35
    iget-object v10, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 36
    .line 37
    invoke-static {v10, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 38
    .line 39
    .line 40
    move-result v10

    .line 41
    add-int/lit8 v1, v1, 0x4

    .line 42
    .line 43
    new-instance v11, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 44
    .line 45
    invoke-direct {v11, v6, v7}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;-><init>(IS)V

    .line 46
    .line 47
    .line 48
    sget-object v6, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphTextPropTypes:[Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 49
    .line 50
    iget-object v7, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 51
    .line 52
    invoke-virtual {v11, v10, v6, v7, v1}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->buildTextPropList(I[Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;[BI)I

    .line 53
    .line 54
    .line 55
    move-result v6

    .line 56
    add-int/2addr v1, v6

    .line 57
    iget-object v6, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    .line 58
    .line 59
    invoke-virtual {v6, v11}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 60
    .line 61
    .line 62
    iget-object v6, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 63
    .line 64
    array-length v6, v6

    .line 65
    if-ge v1, v6, :cond_0

    .line 66
    .line 67
    if-ne v2, p1, :cond_0

    .line 68
    .line 69
    add-int/lit8 v3, v3, 0x1

    .line 70
    .line 71
    :cond_0
    if-lez v4, :cond_6

    .line 72
    .line 73
    const-string v6, "paragraph_flags"

    .line 74
    .line 75
    invoke-virtual {v11, v6}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->findByName(Ljava/lang/String;)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 76
    .line 77
    .line 78
    move-result-object v6

    .line 79
    if-eqz v6, :cond_1

    .line 80
    .line 81
    invoke-virtual {v6}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->getValue()I

    .line 82
    .line 83
    .line 84
    move-result v6

    .line 85
    goto :goto_1

    .line 86
    :cond_1
    const/4 v6, 0x0

    .line 87
    :goto_1
    if-eq v6, v8, :cond_5

    .line 88
    .line 89
    const-string v7, "bullet.char"

    .line 90
    .line 91
    invoke-virtual {v11, v7}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->findByName(Ljava/lang/String;)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 92
    .line 93
    .line 94
    move-result-object v8

    .line 95
    if-eqz v8, :cond_2

    .line 96
    .line 97
    invoke-virtual {v8}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->getValue()I

    .line 98
    .line 99
    .line 100
    move-result v8

    .line 101
    goto :goto_2

    .line 102
    :cond_2
    const/4 v8, 0x0

    .line 103
    :goto_2
    if-eq v6, v9, :cond_6

    .line 104
    .line 105
    const/16 v6, 0x2022

    .line 106
    .line 107
    if-eq v8, v6, :cond_5

    .line 108
    .line 109
    const/16 v9, 0x2013

    .line 110
    .line 111
    if-ne v8, v9, :cond_3

    .line 112
    .line 113
    goto :goto_3

    .line 114
    :cond_3
    iget-object v10, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paragraphStyles:Ljava/util/LinkedList;

    .line 115
    .line 116
    add-int/lit8 v11, v4, -0x1

    .line 117
    .line 118
    invoke-virtual {v10, v11}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    .line 119
    .line 120
    .line 121
    move-result-object v10

    .line 122
    check-cast v10, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 123
    .line 124
    if-eqz v10, :cond_4

    .line 125
    .line 126
    invoke-virtual {v10, v7}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->findByName(Ljava/lang/String;)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 127
    .line 128
    .line 129
    move-result-object v7

    .line 130
    if-eqz v7, :cond_4

    .line 131
    .line 132
    invoke-virtual {v7}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->getValue()I

    .line 133
    .line 134
    .line 135
    move-result v8

    .line 136
    :cond_4
    if-eq v8, v6, :cond_5

    .line 137
    .line 138
    if-ne v8, v9, :cond_6

    .line 139
    .line 140
    :cond_5
    :goto_3
    add-int/lit8 v5, v5, 0x1

    .line 141
    .line 142
    :cond_6
    iget-object v6, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->paraAutoNumberIndexs:Ljava/util/Map;

    .line 143
    .line 144
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 145
    .line 146
    .line 147
    move-result-object v7

    .line 148
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 149
    .line 150
    .line 151
    move-result-object v8

    .line 152
    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    .line 154
    .line 155
    add-int/lit8 v4, v4, 0x1

    .line 156
    .line 157
    goto/16 :goto_0

    .line 158
    .line 159
    :cond_7
    move v3, p1

    .line 160
    const/4 v2, 0x0

    .line 161
    :cond_8
    :goto_4
    iget-object v4, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 162
    .line 163
    array-length v5, v4

    .line 164
    if-ge v1, v5, :cond_9

    .line 165
    .line 166
    if-ge v2, v3, :cond_9

    .line 167
    .line 168
    invoke-static {v4, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 169
    .line 170
    .line 171
    move-result v4

    .line 172
    add-int/2addr v2, v4

    .line 173
    add-int/lit8 v1, v1, 0x4

    .line 174
    .line 175
    iget-object v5, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 176
    .line 177
    invoke-static {v5, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 178
    .line 179
    .line 180
    move-result v5

    .line 181
    add-int/lit8 v1, v1, 0x4

    .line 182
    .line 183
    new-instance v6, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 184
    .line 185
    const/4 v7, -0x1

    .line 186
    invoke-direct {v6, v4, v7}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;-><init>(IS)V

    .line 187
    .line 188
    .line 189
    sget-object v4, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->characterTextPropTypes:[Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 190
    .line 191
    iget-object v7, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 192
    .line 193
    invoke-virtual {v6, v5, v4, v7, v1}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->buildTextPropList(I[Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;[BI)I

    .line 194
    .line 195
    .line 196
    move-result v4

    .line 197
    add-int/2addr v1, v4

    .line 198
    iget-object v4, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->charStyles:Ljava/util/LinkedList;

    .line 199
    .line 200
    invoke-virtual {v4, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 201
    .line 202
    .line 203
    iget-object v4, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 204
    .line 205
    array-length v4, v4

    .line 206
    if-ge v1, v4, :cond_8

    .line 207
    .line 208
    if-ne v2, p1, :cond_8

    .line 209
    .line 210
    add-int/lit8 v3, v3, 0x1

    .line 211
    .line 212
    goto :goto_4

    .line 213
    :cond_9
    array-length p1, v4

    .line 214
    if-ge v1, p1, :cond_a

    .line 215
    .line 216
    array-length p1, v4

    .line 217
    sub-int/2addr p1, v1

    .line 218
    new-array p1, p1, [B

    .line 219
    .line 220
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->reserved:[B

    .line 221
    .line 222
    array-length v2, p1

    .line 223
    invoke-static {v4, v1, p1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 224
    .line 225
    .line 226
    :cond_a
    iput-boolean v8, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->initialised:Z

    .line 227
    .line 228
    return-void
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public setRawContents([B)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    new-array v0, p1, [B

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->reserved:[B

    .line 7
    .line 8
    iput-boolean p1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->initialised:Z

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toString()Ljava/lang/String;
    .locals 16

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    new-instance v2, Ljava/lang/StringBuffer;

    .line 4
    .line 5
    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v0, "StyleTextPropAtom:\n"

    .line 9
    .line 10
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 11
    .line 12
    .line 13
    iget-boolean v0, v1, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->initialised:Z

    .line 14
    .line 15
    const/4 v3, 0x0

    .line 16
    const-wide/16 v4, 0x0

    .line 17
    .line 18
    if-nez v0, :cond_0

    .line 19
    .line 20
    const-string v0, "Uninitialised, dumping Raw Style Data\n"

    .line 21
    .line 22
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 23
    .line 24
    .line 25
    goto/16 :goto_5

    .line 26
    .line 27
    :cond_0
    const-string v0, "Paragraph properties\n"

    .line 28
    .line 29
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 30
    .line 31
    .line 32
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->getParagraphStyles()Ljava/util/LinkedList;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-virtual {v0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 37
    .line 38
    .line 39
    move-result-object v6

    .line 40
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    const-string v7, ")\n"

    .line 45
    .line 46
    const-string v8, " (0x"

    .line 47
    .line 48
    const-string v9, " = "

    .line 49
    .line 50
    const-string v10, "    "

    .line 51
    .line 52
    const-string v11, "\n"

    .line 53
    .line 54
    const-string v12, "  special mask flags: 0x"

    .line 55
    .line 56
    const-string v13, "  chars covered: "

    .line 57
    .line 58
    if-eqz v0, :cond_2

    .line 59
    .line 60
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    check-cast v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 65
    .line 66
    new-instance v14, Ljava/lang/StringBuilder;

    .line 67
    .line 68
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->getCharactersCovered()I

    .line 75
    .line 76
    .line 77
    move-result v13

    .line 78
    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v13

    .line 85
    invoke-virtual {v2, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 86
    .line 87
    .line 88
    new-instance v13, Ljava/lang/StringBuilder;

    .line 89
    .line 90
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    .line 92
    .line 93
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->getSpecialMask()I

    .line 97
    .line 98
    .line 99
    move-result v12

    .line 100
    invoke-static {v12}, Lcom/intsig/office/fc/util/HexDump;->toHex(I)Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object v12

    .line 104
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object v11

    .line 114
    invoke-virtual {v2, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 115
    .line 116
    .line 117
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->getTextPropList()Ljava/util/LinkedList;

    .line 118
    .line 119
    .line 120
    move-result-object v0

    .line 121
    invoke-virtual {v0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 126
    .line 127
    .line 128
    move-result v11

    .line 129
    if-eqz v11, :cond_1

    .line 130
    .line 131
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 132
    .line 133
    .line 134
    move-result-object v11

    .line 135
    check-cast v11, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 136
    .line 137
    new-instance v12, Ljava/lang/StringBuilder;

    .line 138
    .line 139
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 140
    .line 141
    .line 142
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    invoke-virtual {v11}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->getName()Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v13

    .line 149
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    .line 154
    .line 155
    invoke-virtual {v11}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->getValue()I

    .line 156
    .line 157
    .line 158
    move-result v13

    .line 159
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 160
    .line 161
    .line 162
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object v12

    .line 166
    invoke-virtual {v2, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 167
    .line 168
    .line 169
    new-instance v12, Ljava/lang/StringBuilder;

    .line 170
    .line 171
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 172
    .line 173
    .line 174
    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    .line 176
    .line 177
    invoke-virtual {v11}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->getValue()I

    .line 178
    .line 179
    .line 180
    move-result v11

    .line 181
    invoke-static {v11}, Lcom/intsig/office/fc/util/HexDump;->toHex(I)Ljava/lang/String;

    .line 182
    .line 183
    .line 184
    move-result-object v11

    .line 185
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    .line 187
    .line 188
    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    .line 190
    .line 191
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 192
    .line 193
    .line 194
    move-result-object v11

    .line 195
    invoke-virtual {v2, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 196
    .line 197
    .line 198
    goto :goto_1

    .line 199
    :cond_1
    const-string v0, "  para bytes that would be written: \n"

    .line 200
    .line 201
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 202
    .line 203
    .line 204
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    .line 205
    .line 206
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 207
    .line 208
    .line 209
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 210
    .line 211
    .line 212
    move-result-object v0

    .line 213
    invoke-static {v0, v4, v5, v3}, Lcom/intsig/office/fc/util/HexDump;->dump([BJI)Ljava/lang/String;

    .line 214
    .line 215
    .line 216
    move-result-object v0

    .line 217
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    .line 219
    .line 220
    goto/16 :goto_0

    .line 221
    .line 222
    :catch_0
    move-exception v0

    .line 223
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 224
    .line 225
    .line 226
    goto/16 :goto_0

    .line 227
    .line 228
    :cond_2
    const-string v0, "Character properties\n"

    .line 229
    .line 230
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 231
    .line 232
    .line 233
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->getCharacterStyles()Ljava/util/LinkedList;

    .line 234
    .line 235
    .line 236
    move-result-object v0

    .line 237
    invoke-virtual {v0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 238
    .line 239
    .line 240
    move-result-object v6

    .line 241
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    .line 242
    .line 243
    .line 244
    move-result v0

    .line 245
    if-eqz v0, :cond_4

    .line 246
    .line 247
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 248
    .line 249
    .line 250
    move-result-object v0

    .line 251
    check-cast v0, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 252
    .line 253
    new-instance v14, Ljava/lang/StringBuilder;

    .line 254
    .line 255
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 256
    .line 257
    .line 258
    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    .line 260
    .line 261
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->getCharactersCovered()I

    .line 262
    .line 263
    .line 264
    move-result v15

    .line 265
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 266
    .line 267
    .line 268
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 269
    .line 270
    .line 271
    move-result-object v14

    .line 272
    invoke-virtual {v2, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 273
    .line 274
    .line 275
    new-instance v14, Ljava/lang/StringBuilder;

    .line 276
    .line 277
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 278
    .line 279
    .line 280
    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    .line 282
    .line 283
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->getSpecialMask()I

    .line 284
    .line 285
    .line 286
    move-result v15

    .line 287
    invoke-static {v15}, Lcom/intsig/office/fc/util/HexDump;->toHex(I)Ljava/lang/String;

    .line 288
    .line 289
    .line 290
    move-result-object v15

    .line 291
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292
    .line 293
    .line 294
    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    .line 296
    .line 297
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 298
    .line 299
    .line 300
    move-result-object v14

    .line 301
    invoke-virtual {v2, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 302
    .line 303
    .line 304
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->getTextPropList()Ljava/util/LinkedList;

    .line 305
    .line 306
    .line 307
    move-result-object v0

    .line 308
    invoke-virtual {v0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 309
    .line 310
    .line 311
    move-result-object v0

    .line 312
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 313
    .line 314
    .line 315
    move-result v14

    .line 316
    if-eqz v14, :cond_3

    .line 317
    .line 318
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 319
    .line 320
    .line 321
    move-result-object v14

    .line 322
    check-cast v14, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 323
    .line 324
    new-instance v15, Ljava/lang/StringBuilder;

    .line 325
    .line 326
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 327
    .line 328
    .line 329
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    .line 331
    .line 332
    invoke-virtual {v14}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->getName()Ljava/lang/String;

    .line 333
    .line 334
    .line 335
    move-result-object v3

    .line 336
    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    .line 338
    .line 339
    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    .line 341
    .line 342
    invoke-virtual {v14}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->getValue()I

    .line 343
    .line 344
    .line 345
    move-result v3

    .line 346
    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 347
    .line 348
    .line 349
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 350
    .line 351
    .line 352
    move-result-object v3

    .line 353
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 354
    .line 355
    .line 356
    new-instance v3, Ljava/lang/StringBuilder;

    .line 357
    .line 358
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 359
    .line 360
    .line 361
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 362
    .line 363
    .line 364
    invoke-virtual {v14}, Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;->getValue()I

    .line 365
    .line 366
    .line 367
    move-result v14

    .line 368
    invoke-static {v14}, Lcom/intsig/office/fc/util/HexDump;->toHex(I)Ljava/lang/String;

    .line 369
    .line 370
    .line 371
    move-result-object v14

    .line 372
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    .line 374
    .line 375
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376
    .line 377
    .line 378
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 379
    .line 380
    .line 381
    move-result-object v3

    .line 382
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 383
    .line 384
    .line 385
    const/4 v3, 0x0

    .line 386
    goto :goto_3

    .line 387
    :cond_3
    const-string v0, "  char bytes that would be written: \n"

    .line 388
    .line 389
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 390
    .line 391
    .line 392
    :try_start_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    .line 393
    .line 394
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 395
    .line 396
    .line 397
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 398
    .line 399
    .line 400
    move-result-object v0

    .line 401
    const/4 v3, 0x0

    .line 402
    invoke-static {v0, v4, v5, v3}, Lcom/intsig/office/fc/util/HexDump;->dump([BJI)Ljava/lang/String;

    .line 403
    .line 404
    .line 405
    move-result-object v0

    .line 406
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 407
    .line 408
    .line 409
    goto :goto_4

    .line 410
    :catch_1
    move-exception v0

    .line 411
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 412
    .line 413
    .line 414
    :goto_4
    const/4 v3, 0x0

    .line 415
    goto/16 :goto_2

    .line 416
    .line 417
    :cond_4
    :goto_5
    const-string v0, "  original byte stream \n"

    .line 418
    .line 419
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 420
    .line 421
    .line 422
    iget-object v0, v1, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 423
    .line 424
    const/4 v3, 0x0

    .line 425
    invoke-static {v0, v4, v5, v3}, Lcom/intsig/office/fc/util/HexDump;->dump([BJI)Ljava/lang/String;

    .line 426
    .line 427
    .line 428
    move-result-object v0

    .line 429
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 430
    .line 431
    .line 432
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 433
    .line 434
    .line 435
    move-result-object v0

    .line 436
    return-object v0
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
    .line 1825
    .line 1826
    .line 1827
    .line 1828
    .line 1829
    .line 1830
    .line 1831
    .line 1832
    .line 1833
    .line 1834
    .line 1835
    .line 1836
    .line 1837
    .line 1838
    .line 1839
    .line 1840
    .line 1841
    .line 1842
    .line 1843
    .line 1844
    .line 1845
    .line 1846
    .line 1847
    .line 1848
    .line 1849
    .line 1850
    .line 1851
    .line 1852
    .line 1853
    .line 1854
    .line 1855
    .line 1856
    .line 1857
    .line 1858
    .line 1859
    .line 1860
    .line 1861
    .line 1862
    .line 1863
    .line 1864
    .line 1865
    .line 1866
    .line 1867
    .line 1868
    .line 1869
    .line 1870
    .line 1871
    .line 1872
    .line 1873
    .line 1874
    .line 1875
    .line 1876
    .line 1877
    .line 1878
    .line 1879
    .line 1880
    .line 1881
    .line 1882
    .line 1883
    .line 1884
    .line 1885
    .line 1886
    .line 1887
    .line 1888
    .line 1889
    .line 1890
    .line 1891
    .line 1892
    .line 1893
    .line 1894
    .line 1895
    .line 1896
    .line 1897
    .line 1898
    .line 1899
    .line 1900
    .line 1901
    .line 1902
    .line 1903
    .line 1904
    .line 1905
    .line 1906
    .line 1907
    .line 1908
    .line 1909
    .line 1910
    .line 1911
    .line 1912
    .line 1913
    .line 1914
    .line 1915
    .line 1916
    .line 1917
    .line 1918
    .line 1919
    .line 1920
    .line 1921
    .line 1922
    .line 1923
    .line 1924
    .line 1925
    .line 1926
    .line 1927
    .line 1928
    .line 1929
    .line 1930
    .line 1931
    .line 1932
    .line 1933
    .line 1934
    .line 1935
    .line 1936
    .line 1937
    .line 1938
    .line 1939
    .line 1940
    .line 1941
    .line 1942
    .line 1943
    .line 1944
    .line 1945
    .line 1946
    .line 1947
    .line 1948
    .line 1949
    .line 1950
    .line 1951
    .line 1952
    .line 1953
    .line 1954
    .line 1955
    .line 1956
    .line 1957
    .line 1958
    .line 1959
    .line 1960
    .line 1961
    .line 1962
    .line 1963
    .line 1964
    .line 1965
    .line 1966
    .line 1967
    .line 1968
    .line 1969
    .line 1970
    .line 1971
    .line 1972
    .line 1973
    .line 1974
    .line 1975
    .line 1976
    .line 1977
    .line 1978
    .line 1979
    .line 1980
    .line 1981
    .line 1982
    .line 1983
    .line 1984
    .line 1985
    .line 1986
    .line 1987
    .line 1988
    .line 1989
    .line 1990
    .line 1991
    .line 1992
    .line 1993
    .line 1994
    .line 1995
    .line 1996
    .line 1997
    .line 1998
    .line 1999
    .line 2000
    .line 2001
    .line 2002
    .line 2003
    .line 2004
    .line 2005
    .line 2006
    .line 2007
    .line 2008
    .line 2009
    .line 2010
    .line 2011
    .line 2012
    .line 2013
    .line 2014
    .line 2015
    .line 2016
    .line 2017
    .line 2018
    .line 2019
    .line 2020
    .line 2021
    .line 2022
    .line 2023
    .line 2024
    .line 2025
    .line 2026
    .line 2027
    .line 2028
    .line 2029
    .line 2030
    .line 2031
    .line 2032
    .line 2033
    .line 2034
    .line 2035
    .line 2036
    .line 2037
    .line 2038
    .line 2039
    .line 2040
    .line 2041
    .line 2042
    .line 2043
    .line 2044
    .line 2045
    .line 2046
    .line 2047
    .line 2048
    .line 2049
    .line 2050
    .line 2051
    .line 2052
    .line 2053
    .line 2054
    .line 2055
    .line 2056
    .line 2057
    .line 2058
    .line 2059
    .line 2060
    .line 2061
    .line 2062
    .line 2063
    .line 2064
    .line 2065
    .line 2066
    .line 2067
    .line 2068
    .line 2069
    .line 2070
    .line 2071
    .line 2072
    .line 2073
    .line 2074
    .line 2075
    .line 2076
    .line 2077
    .line 2078
    .line 2079
    .line 2080
    .line 2081
    .line 2082
    .line 2083
    .line 2084
    .line 2085
    .line 2086
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->updateRawContents()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 5
    .line 6
    array-length v0, v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->reserved:[B

    .line 8
    .line 9
    array-length v1, v1

    .line 10
    add-int/2addr v0, v1

    .line 11
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->_header:[B

    .line 12
    .line 13
    const/4 v2, 0x4

    .line 14
    invoke-static {v1, v2, v0}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->_header:[B

    .line 18
    .line 19
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->rawContents:[B

    .line 23
    .line 24
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;->reserved:[B

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.class public final Lcom/intsig/office/fc/hslf/model/Slide;
.super Lcom/intsig/office/fc/hslf/model/Sheet;
.source "Slide.java"


# instance fields
.field private _atomSet:Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;

.field private _extendedAtomsSets:[Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

.field private _notes:Lcom/intsig/office/fc/hslf/model/Notes;

.field private _runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

.field private _slideNo:I

.field private propTagsContainer:Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;

.field private ssSlideInfoAtom:Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;


# direct methods
.method public constructor <init>(III)V
    .locals 1

    .line 25
    new-instance v0, Lcom/intsig/office/fc/hslf/record/Slide;

    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/record/Slide;-><init>()V

    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hslf/model/Sheet;-><init>(Lcom/intsig/office/fc/hslf/record/SheetContainer;I)V

    .line 26
    iput p3, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_slideNo:I

    .line 27
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSheetContainer()Lcom/intsig/office/fc/hslf/record/SheetContainer;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hslf/record/PositionDependentRecordContainer;->setSheetId(I)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hslf/record/Slide;Lcom/intsig/office/fc/hslf/model/Notes;Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;[Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p5}, Lcom/intsig/office/fc/hslf/model/Sheet;-><init>(Lcom/intsig/office/fc/hslf/record/SheetContainer;I)V

    .line 2
    iput-object p2, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_notes:Lcom/intsig/office/fc/hslf/model/Notes;

    .line 3
    iput-object p3, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_atomSet:Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;

    .line 4
    iput p6, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_slideNo:I

    .line 5
    iput-object p4, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_extendedAtomsSets:[Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getPPDrawing()Lcom/intsig/office/fc/hslf/record/PPDrawing;

    move-result-object p1

    invoke-static {p1}, Lcom/intsig/office/fc/hslf/model/Sheet;->findTextRuns(Lcom/intsig/office/fc/hslf/record/PPDrawing;)[Lcom/intsig/office/fc/hslf/model/TextRun;

    move-result-object p1

    .line 7
    new-instance p2, Ljava/util/Vector;

    invoke-direct {p2}, Ljava/util/Vector;-><init>()V

    .line 8
    iget-object p3, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_atomSet:Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;

    if-eqz p3, :cond_0

    .line 9
    invoke-virtual {p3}, Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;->getSlideRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    move-result-object p3

    invoke-static {p3, p2}, Lcom/intsig/office/fc/hslf/model/Sheet;->findTextRuns([Lcom/intsig/office/fc/hslf/record/Record;Ljava/util/Vector;)V

    .line 10
    :cond_0
    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result p3

    array-length p4, p1

    add-int/2addr p3, p4

    new-array p3, p3, [Lcom/intsig/office/fc/hslf/model/TextRun;

    iput-object p3, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    const/4 p3, 0x0

    const/4 p4, 0x0

    .line 11
    :goto_0
    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result p5

    if-ge p4, p5, :cond_1

    .line 12
    iget-object p5, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    invoke-virtual {p2, p4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object p6

    check-cast p6, Lcom/intsig/office/fc/hslf/model/TextRun;

    aput-object p6, p5, p4

    .line 13
    iget-object p5, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    aget-object p5, p5, p4

    invoke-virtual {p5, p0}, Lcom/intsig/office/fc/hslf/model/TextRun;->setSheet(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    add-int/lit8 p4, p4, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    .line 14
    :goto_1
    array-length p5, p1

    if-ge p2, p5, :cond_2

    .line 15
    iget-object p5, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    aget-object p6, p1, p2

    aput-object p6, p5, p4

    .line 16
    invoke-virtual {p6, p0}, Lcom/intsig/office/fc/hslf/model/TextRun;->setSheet(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    add-int/lit8 p4, p4, 0x1

    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    .line 17
    :cond_2
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_extendedAtomsSets:[Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

    if-eqz p1, :cond_5

    const/4 p1, 0x0

    .line 18
    :goto_2
    iget-object p2, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    array-length p4, p2

    if-ge p1, p4, :cond_5

    .line 19
    aget-object p2, p2, p1

    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/TextRun;->getExtendedParagraphAtom()Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;

    move-result-object p2

    if-nez p2, :cond_4

    .line 20
    iget-object p2, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    aget-object p2, p2, p1

    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRunType()I

    move-result p2

    const/4 p4, 0x0

    .line 21
    :goto_3
    iget-object p5, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_extendedAtomsSets:[Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

    array-length p6, p5

    if-ge p4, p6, :cond_4

    .line 22
    aget-object p5, p5, p4

    invoke-virtual {p5}, Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;->getExtendedParaHeaderAtom()Lcom/intsig/office/fc/hslf/record/ExtendedParagraphHeaderAtom;

    move-result-object p5

    if-eqz p5, :cond_3

    .line 23
    invoke-virtual {p5}, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphHeaderAtom;->getTextType()I

    move-result p5

    if-ne p5, p2, :cond_3

    .line 24
    iget-object p2, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    aget-object p2, p2, p1

    iget-object p5, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_extendedAtomsSets:[Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

    aget-object p4, p5, p4

    invoke-virtual {p4}, Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;->getExtendedParaAtom()Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;

    move-result-object p4

    invoke-virtual {p2, p4}, Lcom/intsig/office/fc/hslf/model/TextRun;->setExtendedParagraphAtom(Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;)V

    goto :goto_4

    :cond_3
    add-int/lit8 p4, p4, 0x1

    goto :goto_3

    :cond_4
    :goto_4
    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    :cond_5
    return-void
.end method


# virtual methods
.method public addTitle()Lcom/intsig/office/fc/hslf/model/TextBox;
    .locals 6

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hslf/model/Placeholder;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hslf/model/Placeholder;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setShapeType(I)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hslf/model/TextRun;->setRunType(I)V

    .line 16
    .line 17
    .line 18
    const-string v1, "Click to edit title"

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/TextShape;->setText(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    new-instance v1, Lcom/intsig/office/java/awt/Rectangle;

    .line 24
    .line 25
    const/16 v2, 0x264

    .line 26
    .line 27
    const/16 v3, 0x5a

    .line 28
    .line 29
    const/16 v4, 0x36

    .line 30
    .line 31
    const/16 v5, 0x30

    .line 32
    .line 33
    invoke-direct {v1, v4, v5, v2, v3}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/model/Shape;->setAnchor(Lcom/intsig/office/java/awt/geom/Rectangle2D;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hslf/model/Sheet;->addShape(Lcom/intsig/office/fc/hslf/model/Shape;)V

    .line 40
    .line 41
    .line 42
    return-object v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public dispose()V
    .locals 6

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->dispose()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_atomSet:Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;->dispose()V

    .line 10
    .line 11
    .line 12
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_atomSet:Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;

    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 15
    .line 16
    const/4 v2, 0x0

    .line 17
    if-eqz v0, :cond_2

    .line 18
    .line 19
    array-length v3, v0

    .line 20
    const/4 v4, 0x0

    .line 21
    :goto_0
    if-ge v4, v3, :cond_1

    .line 22
    .line 23
    aget-object v5, v0, v4

    .line 24
    .line 25
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/model/TextRun;->dispose()V

    .line 26
    .line 27
    .line 28
    add-int/lit8 v4, v4, 0x1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 32
    .line 33
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_notes:Lcom/intsig/office/fc/hslf/model/Notes;

    .line 34
    .line 35
    if-eqz v0, :cond_3

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Notes;->dispose()V

    .line 38
    .line 39
    .line 40
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_notes:Lcom/intsig/office/fc/hslf/model/Notes;

    .line 41
    .line 42
    :cond_3
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_extendedAtomsSets:[Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

    .line 43
    .line 44
    if-eqz v0, :cond_5

    .line 45
    .line 46
    array-length v3, v0

    .line 47
    :goto_1
    if-ge v2, v3, :cond_4

    .line 48
    .line 49
    aget-object v4, v0, v2

    .line 50
    .line 51
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;->dispose()V

    .line 52
    .line 53
    .line 54
    add-int/lit8 v2, v2, 0x1

    .line 55
    .line 56
    goto :goto_1

    .line 57
    :cond_4
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_extendedAtomsSets:[Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

    .line 58
    .line 59
    :cond_5
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Slide;->ssSlideInfoAtom:Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;

    .line 60
    .line 61
    if-eqz v0, :cond_6

    .line 62
    .line 63
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->dispose()V

    .line 64
    .line 65
    .line 66
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/model/Slide;->ssSlideInfoAtom:Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;

    .line 67
    .line 68
    :cond_6
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Slide;->propTagsContainer:Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;

    .line 69
    .line 70
    if-eqz v0, :cond_7

    .line 71
    .line 72
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->dispose()V

    .line 73
    .line 74
    .line 75
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/model/Slide;->propTagsContainer:Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;

    .line 76
    .line 77
    :cond_7
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getBackground()Lcom/intsig/office/fc/hslf/model/Background;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Slide;->getFollowMasterBackground()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Slide;->getMasterSheet()Lcom/intsig/office/fc/hslf/model/MasterSheet;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getBackground()Lcom/intsig/office/fc/hslf/model/Background;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0

    .line 16
    :cond_0
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getBackground()Lcom/intsig/office/fc/hslf/model/Background;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    return-object v0
    .line 21
.end method

.method public getColorScheme()Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Slide;->getFollowMasterScheme()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Slide;->getMasterSheet()Lcom/intsig/office/fc/hslf/model/MasterSheet;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getColorScheme()Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0

    .line 16
    :cond_0
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getColorScheme()Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    return-object v0
    .line 21
.end method

.method public getComments()[Lcom/intsig/office/fc/hslf/model/Comment;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSheetContainer()Lcom/intsig/office/fc/hslf/record/SheetContainer;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sget-object v1, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlideProgTagsContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 6
    .line 7
    iget v1, v1, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 8
    .line 9
    int-to-long v1, v1

    .line 10
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    check-cast v0, Lcom/intsig/office/fc/hslf/record/RecordContainer;

    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    if-eqz v0, :cond_4

    .line 18
    .line 19
    sget-object v2, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlideProgBinaryTagContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 20
    .line 21
    iget v2, v2, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 22
    .line 23
    int-to-long v2, v2

    .line 24
    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    check-cast v0, Lcom/intsig/office/fc/hslf/record/RecordContainer;

    .line 29
    .line 30
    if-eqz v0, :cond_4

    .line 31
    .line 32
    sget-object v2, Lcom/intsig/office/fc/hslf/record/RecordTypes;->BinaryTagDataBlob:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 33
    .line 34
    iget v2, v2, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 35
    .line 36
    int-to-long v2, v2

    .line 37
    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    check-cast v0, Lcom/intsig/office/fc/hslf/record/RecordContainer;

    .line 42
    .line 43
    if-eqz v0, :cond_4

    .line 44
    .line 45
    const/4 v2, 0x0

    .line 46
    const/4 v3, 0x0

    .line 47
    :goto_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 48
    .line 49
    .line 50
    move-result-object v4

    .line 51
    array-length v4, v4

    .line 52
    if-ge v2, v4, :cond_1

    .line 53
    .line 54
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 55
    .line 56
    .line 57
    move-result-object v4

    .line 58
    aget-object v4, v4, v2

    .line 59
    .line 60
    instance-of v4, v4, Lcom/intsig/office/fc/hslf/record/Comment2000;

    .line 61
    .line 62
    if-eqz v4, :cond_0

    .line 63
    .line 64
    add-int/lit8 v3, v3, 0x1

    .line 65
    .line 66
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_1
    new-array v2, v3, [Lcom/intsig/office/fc/hslf/model/Comment;

    .line 70
    .line 71
    :goto_1
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 72
    .line 73
    .line 74
    move-result-object v3

    .line 75
    array-length v3, v3

    .line 76
    if-ge v1, v3, :cond_3

    .line 77
    .line 78
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 79
    .line 80
    .line 81
    move-result-object v3

    .line 82
    aget-object v3, v3, v1

    .line 83
    .line 84
    instance-of v3, v3, Lcom/intsig/office/fc/hslf/record/Comment2000;

    .line 85
    .line 86
    if-eqz v3, :cond_2

    .line 87
    .line 88
    new-instance v3, Lcom/intsig/office/fc/hslf/model/Comment;

    .line 89
    .line 90
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 91
    .line 92
    .line 93
    move-result-object v4

    .line 94
    aget-object v4, v4, v1

    .line 95
    .line 96
    check-cast v4, Lcom/intsig/office/fc/hslf/record/Comment2000;

    .line 97
    .line 98
    invoke-direct {v3, v4}, Lcom/intsig/office/fc/hslf/model/Comment;-><init>(Lcom/intsig/office/fc/hslf/record/Comment2000;)V

    .line 99
    .line 100
    .line 101
    aput-object v3, v2, v1

    .line 102
    .line 103
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 104
    .line 105
    goto :goto_1

    .line 106
    :cond_3
    return-object v2

    .line 107
    :cond_4
    new-array v0, v1, [Lcom/intsig/office/fc/hslf/model/Comment;

    .line 108
    .line 109
    return-object v0
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public getFollowMasterBackground()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideRecord()Lcom/intsig/office/fc/hslf/record/Slide;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/Slide;->getSlideAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->getFollowMasterBackground()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFollowMasterObjects()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideRecord()Lcom/intsig/office/fc/hslf/record/Slide;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/Slide;->getSlideAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->getFollowMasterObjects()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFollowMasterScheme()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideRecord()Lcom/intsig/office/fc/hslf/record/Slide;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/Slide;->getSlideAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->getFollowMasterScheme()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMasterSheet()Lcom/intsig/office/fc/hslf/model/MasterSheet;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSlideShow()Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getSlidesMasters()[Lcom/intsig/office/fc/hslf/model/SlideMaster;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideRecord()Lcom/intsig/office/fc/hslf/record/Slide;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/Slide;->getSlideAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->getMasterID()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    const/4 v2, 0x0

    .line 22
    const/4 v3, 0x0

    .line 23
    :goto_0
    array-length v4, v0

    .line 24
    if-ge v3, v4, :cond_1

    .line 25
    .line 26
    aget-object v4, v0, v3

    .line 27
    .line 28
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/model/Sheet;->_getSheetNumber()I

    .line 29
    .line 30
    .line 31
    move-result v4

    .line 32
    if-ne v1, v4, :cond_0

    .line 33
    .line 34
    aget-object v0, v0, v3

    .line 35
    .line 36
    goto :goto_1

    .line 37
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    const/4 v0, 0x0

    .line 41
    :goto_1
    if-nez v0, :cond_3

    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSlideShow()Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 44
    .line 45
    .line 46
    move-result-object v3

    .line 47
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getTitleMasters()[Lcom/intsig/office/fc/hslf/model/TitleMaster;

    .line 48
    .line 49
    .line 50
    move-result-object v3

    .line 51
    if-eqz v3, :cond_3

    .line 52
    .line 53
    :goto_2
    array-length v4, v3

    .line 54
    if-ge v2, v4, :cond_3

    .line 55
    .line 56
    aget-object v4, v3, v2

    .line 57
    .line 58
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/model/Sheet;->_getSheetNumber()I

    .line 59
    .line 60
    .line 61
    move-result v4

    .line 62
    if-ne v1, v4, :cond_2

    .line 63
    .line 64
    aget-object v0, v3, v2

    .line 65
    .line 66
    goto :goto_3

    .line 67
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 68
    .line 69
    goto :goto_2

    .line 70
    :cond_3
    :goto_3
    return-object v0
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getNotesSheet()Lcom/intsig/office/fc/hslf/model/Notes;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_notes:Lcom/intsig/office/fc/hslf/model/Notes;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getSlideAtomsSet()Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_atomSet:Lcom/intsig/office/fc/hslf/record/SlideListWithText$SlideAtomsSet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideHeadersFooters()Lcom/intsig/office/fc/hslf/model/HeadersFooters;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideRecord()Lcom/intsig/office/fc/hslf/record/Slide;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/Slide;->getHeadersFootersContainer()Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v1, Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    invoke-direct {v1, v0, p0, v2, v2}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;-><init>(Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;Lcom/intsig/office/fc/hslf/model/Sheet;ZZ)V

    .line 15
    .line 16
    .line 17
    return-object v1

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    return-object v0
    .line 20
    .line 21
.end method

.method public getSlideNumber()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_slideNo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideProgTagsContainer()Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Slide;->propTagsContainer:Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideRecord()Lcom/intsig/office/fc/hslf/record/Slide;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSheetContainer()Lcom/intsig/office/fc/hslf/record/SheetContainer;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hslf/record/Slide;

    .line 6
    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideShowSlideInfoAtom()Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Slide;->ssSlideInfoAtom:Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTextRuns()[Lcom/intsig/office/fc/hslf/model/TextRun;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTitle()Ljava/lang/String;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Slide;->getTextRuns()[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    :goto_0
    array-length v2, v0

    .line 7
    if-ge v1, v2, :cond_2

    .line 8
    .line 9
    aget-object v2, v0, v1

    .line 10
    .line 11
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRunType()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    const/4 v3, 0x6

    .line 16
    if-eq v2, v3, :cond_1

    .line 17
    .line 18
    if-nez v2, :cond_0

    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    :goto_1
    aget-object v0, v0, v1

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->getText()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    return-object v0

    .line 31
    :cond_2
    const/4 v0, 0x0

    .line 32
    return-object v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected onAddTextShape(Lcom/intsig/office/fc/hslf/model/TextShape;)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    const/4 v2, 0x1

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    new-array v0, v2, [Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 12
    .line 13
    aput-object p1, v0, v1

    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    array-length v3, v0

    .line 19
    add-int/2addr v3, v2

    .line 20
    new-array v4, v3, [Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 21
    .line 22
    array-length v5, v0

    .line 23
    invoke-static {v0, v1, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 24
    .line 25
    .line 26
    sub-int/2addr v3, v2

    .line 27
    aput-object p1, v4, v3

    .line 28
    .line 29
    iput-object v4, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 30
    .line 31
    :goto_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public onCreate()V
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSlideShow()Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getDocumentRecord()Lcom/intsig/office/fc/hslf/record/Document;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/Document;->getPPDrawingGroup()Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;->getEscherDggRecord()Lcom/intsig/office/fc/ddf/EscherDggRecord;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSheetContainer()Lcom/intsig/office/fc/hslf/record/SheetContainer;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/SheetContainer;->getPPDrawing()Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/PPDrawing;->getEscherRecords()[Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    const/4 v2, 0x0

    .line 30
    aget-object v1, v1, v2

    .line 31
    .line 32
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 33
    .line 34
    const/16 v3, -0xff8

    .line 35
    .line 36
    invoke-static {v1, v3}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 37
    .line 38
    .line 39
    move-result-object v3

    .line 40
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherDgRecord;

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getMaxDrawingGroupId()I

    .line 43
    .line 44
    .line 45
    move-result v4

    .line 46
    const/4 v5, 0x1

    .line 47
    add-int/2addr v4, v5

    .line 48
    shl-int/lit8 v6, v4, 0x4

    .line 49
    .line 50
    int-to-short v6, v6

    .line 51
    invoke-virtual {v3, v6}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->getDrawingsSaved()I

    .line 55
    .line 56
    .line 57
    move-result v6

    .line 58
    add-int/2addr v6, v5

    .line 59
    invoke-virtual {v0, v6}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->setDrawingsSaved(I)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->setMaxDrawingGroupId(I)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildContainers()Ljava/util/List;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 74
    .line 75
    .line 76
    move-result v1

    .line 77
    if-eqz v1, :cond_3

    .line 78
    .line 79
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 84
    .line 85
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    .line 86
    .line 87
    .line 88
    move-result v4

    .line 89
    const/16 v6, -0xffd

    .line 90
    .line 91
    const/16 v7, -0xff6

    .line 92
    .line 93
    if-eq v4, v6, :cond_2

    .line 94
    .line 95
    const/16 v6, -0xffc

    .line 96
    .line 97
    if-eq v4, v6, :cond_1

    .line 98
    .line 99
    const/4 v1, 0x0

    .line 100
    goto :goto_1

    .line 101
    :cond_1
    invoke-virtual {v1, v7}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 106
    .line 107
    goto :goto_1

    .line 108
    :cond_2
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChild(I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 113
    .line 114
    invoke-virtual {v1, v7}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 115
    .line 116
    .line 117
    move-result-object v1

    .line 118
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 119
    .line 120
    :goto_1
    if-eqz v1, :cond_0

    .line 121
    .line 122
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->allocateShapeId()I

    .line 123
    .line 124
    .line 125
    move-result v4

    .line 126
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setShapeId(I)V

    .line 127
    .line 128
    .line 129
    goto :goto_0

    .line 130
    :cond_3
    invoke-virtual {v3, v5}, Lcom/intsig/office/fc/ddf/EscherDgRecord;->setNumShapes(I)V

    .line 131
    .line 132
    .line 133
    return-void
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public setExtendedAtom([Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_extendedAtomsSets:[Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFollowMasterBackground(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideRecord()Lcom/intsig/office/fc/hslf/record/Slide;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/Slide;->getSlideAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->setFollowMasterBackground(Z)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFollowMasterObjects(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideRecord()Lcom/intsig/office/fc/hslf/record/Slide;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/Slide;->getSlideAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->setFollowMasterObjects(Z)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFollowMasterScheme(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideRecord()Lcom/intsig/office/fc/hslf/record/Slide;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/Slide;->getSlideAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->setFollowMasterScheme(Z)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMasterSheet(Lcom/intsig/office/fc/hslf/model/MasterSheet;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideRecord()Lcom/intsig/office/fc/hslf/record/Slide;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/Slide;->getSlideAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Sheet;->_getSheetNumber()I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->setMasterID(I)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setNotes(Lcom/intsig/office/fc/hslf/model/Notes;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_notes:Lcom/intsig/office/fc/hslf/model/Notes;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideRecord()Lcom/intsig/office/fc/hslf/record/Slide;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/Slide;->getSlideAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-nez p1, :cond_0

    .line 12
    .line 13
    const/4 p1, 0x0

    .line 14
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->setNotesID(I)V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Sheet;->_getSheetNumber()I

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->setNotesID(I)V

    .line 23
    .line 24
    .line 25
    :goto_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setSlideNumber(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hslf/model/Slide;->_slideNo:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSlideProgTagsContainer(Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Slide;->propTagsContainer:Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSlideShowSlideInfoAtom(Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/Slide;->ssSlideInfoAtom:Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

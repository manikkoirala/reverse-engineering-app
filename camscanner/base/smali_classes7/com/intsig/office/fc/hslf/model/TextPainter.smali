.class public final Lcom/intsig/office/fc/hslf/model/TextPainter;
.super Ljava/lang/Object;
.source "TextPainter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hslf/model/TextPainter$TextElement;
    }
.end annotation


# static fields
.field protected static final DEFAULT_BULLET_CHAR:C = '\u25a0'


# instance fields
.field protected _shape:Lcom/intsig/office/fc/hslf/model/TextShape;

.field protected logger:Lcom/intsig/office/fc/util/POILogger;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hslf/model/TextShape;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-class v0, Lcom/intsig/office/fc/hslf/model/TextPainter;

    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/office/fc/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lcom/intsig/office/fc/util/POILogger;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/TextPainter;->logger:Lcom/intsig/office/fc/util/POILogger;

    .line 11
    .line 12
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/TextPainter;->_shape:Lcom/intsig/office/fc/hslf/model/TextShape;

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public getAttributedString(Lcom/intsig/office/fc/hslf/model/TextRun;)Ljava/text/AttributedString;
    .locals 7

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/TextRun;->getText()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/16 v1, 0x9

    .line 6
    .line 7
    const/16 v2, 0x20

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const/16 v1, 0xa0

    .line 14
    .line 15
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    new-instance v1, Ljava/text/AttributedString;

    .line 20
    .line 21
    invoke-direct {v1, v0}, Ljava/text/AttributedString;-><init>(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRichTextRuns()[Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const/4 v0, 0x0

    .line 29
    :goto_0
    array-length v2, p1

    .line 30
    if-ge v0, v2, :cond_7

    .line 31
    .line 32
    aget-object v2, p1, v0

    .line 33
    .line 34
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getStartIndex()I

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    aget-object v3, p1, v0

    .line 39
    .line 40
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getEndIndex()I

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    if-ne v2, v3, :cond_0

    .line 45
    .line 46
    iget-object v2, p0, Lcom/intsig/office/fc/hslf/model/TextPainter;->logger:Lcom/intsig/office/fc/util/POILogger;

    .line 47
    .line 48
    sget v3, Lcom/intsig/office/fc/util/POILogger;->INFO:I

    .line 49
    .line 50
    const-string v4, "Skipping RichTextRun with zero length"

    .line 51
    .line 52
    invoke-virtual {v2, v3, v4}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 53
    .line 54
    .line 55
    goto/16 :goto_2

    .line 56
    .line 57
    :cond_0
    sget-object v4, Ljava/awt/font/TextAttribute;->FAMILY:Ljava/awt/font/TextAttribute;

    .line 58
    .line 59
    aget-object v5, p1, v0

    .line 60
    .line 61
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getFontName()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v5

    .line 65
    invoke-virtual {v1, v4, v5, v2, v3}, Ljava/text/AttributedString;->addAttribute(Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;II)V

    .line 66
    .line 67
    .line 68
    sget-object v4, Ljava/awt/font/TextAttribute;->SIZE:Ljava/awt/font/TextAttribute;

    .line 69
    .line 70
    new-instance v5, Ljava/lang/Float;

    .line 71
    .line 72
    aget-object v6, p1, v0

    .line 73
    .line 74
    invoke-virtual {v6}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getFontSize()I

    .line 75
    .line 76
    .line 77
    move-result v6

    .line 78
    int-to-float v6, v6

    .line 79
    invoke-direct {v5, v6}, Ljava/lang/Float;-><init>(F)V

    .line 80
    .line 81
    .line 82
    invoke-virtual {v1, v4, v5, v2, v3}, Ljava/text/AttributedString;->addAttribute(Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;II)V

    .line 83
    .line 84
    .line 85
    sget-object v4, Ljava/awt/font/TextAttribute;->FOREGROUND:Ljava/awt/font/TextAttribute;

    .line 86
    .line 87
    aget-object v5, p1, v0

    .line 88
    .line 89
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getFontColor()Lcom/intsig/office/java/awt/Color;

    .line 90
    .line 91
    .line 92
    move-result-object v5

    .line 93
    invoke-virtual {v1, v4, v5, v2, v3}, Ljava/text/AttributedString;->addAttribute(Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;II)V

    .line 94
    .line 95
    .line 96
    aget-object v4, p1, v0

    .line 97
    .line 98
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->isBold()Z

    .line 99
    .line 100
    .line 101
    move-result v4

    .line 102
    if-eqz v4, :cond_1

    .line 103
    .line 104
    sget-object v4, Ljava/awt/font/TextAttribute;->WEIGHT:Ljava/awt/font/TextAttribute;

    .line 105
    .line 106
    sget-object v5, Ljava/awt/font/TextAttribute;->WEIGHT_BOLD:Ljava/lang/Float;

    .line 107
    .line 108
    invoke-virtual {v1, v4, v5, v2, v3}, Ljava/text/AttributedString;->addAttribute(Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;II)V

    .line 109
    .line 110
    .line 111
    :cond_1
    aget-object v4, p1, v0

    .line 112
    .line 113
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->isItalic()Z

    .line 114
    .line 115
    .line 116
    move-result v4

    .line 117
    if-eqz v4, :cond_2

    .line 118
    .line 119
    sget-object v4, Ljava/awt/font/TextAttribute;->POSTURE:Ljava/awt/font/TextAttribute;

    .line 120
    .line 121
    sget-object v5, Ljava/awt/font/TextAttribute;->POSTURE_OBLIQUE:Ljava/lang/Float;

    .line 122
    .line 123
    invoke-virtual {v1, v4, v5, v2, v3}, Ljava/text/AttributedString;->addAttribute(Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;II)V

    .line 124
    .line 125
    .line 126
    :cond_2
    aget-object v4, p1, v0

    .line 127
    .line 128
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->isUnderlined()Z

    .line 129
    .line 130
    .line 131
    move-result v4

    .line 132
    if-eqz v4, :cond_3

    .line 133
    .line 134
    sget-object v4, Ljava/awt/font/TextAttribute;->UNDERLINE:Ljava/awt/font/TextAttribute;

    .line 135
    .line 136
    sget-object v5, Ljava/awt/font/TextAttribute;->UNDERLINE_ON:Ljava/lang/Integer;

    .line 137
    .line 138
    invoke-virtual {v1, v4, v5, v2, v3}, Ljava/text/AttributedString;->addAttribute(Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;II)V

    .line 139
    .line 140
    .line 141
    sget-object v4, Ljava/awt/font/TextAttribute;->INPUT_METHOD_UNDERLINE:Ljava/awt/font/TextAttribute;

    .line 142
    .line 143
    sget-object v5, Ljava/awt/font/TextAttribute;->UNDERLINE_LOW_TWO_PIXEL:Ljava/lang/Integer;

    .line 144
    .line 145
    invoke-virtual {v1, v4, v5, v2, v3}, Ljava/text/AttributedString;->addAttribute(Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;II)V

    .line 146
    .line 147
    .line 148
    :cond_3
    aget-object v4, p1, v0

    .line 149
    .line 150
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->isStrikethrough()Z

    .line 151
    .line 152
    .line 153
    move-result v4

    .line 154
    if-eqz v4, :cond_4

    .line 155
    .line 156
    sget-object v4, Ljava/awt/font/TextAttribute;->STRIKETHROUGH:Ljava/awt/font/TextAttribute;

    .line 157
    .line 158
    sget-object v5, Ljava/awt/font/TextAttribute;->STRIKETHROUGH_ON:Ljava/lang/Boolean;

    .line 159
    .line 160
    invoke-virtual {v1, v4, v5, v2, v3}, Ljava/text/AttributedString;->addAttribute(Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;II)V

    .line 161
    .line 162
    .line 163
    :cond_4
    aget-object v4, p1, v0

    .line 164
    .line 165
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getSuperscript()I

    .line 166
    .line 167
    .line 168
    move-result v4

    .line 169
    if-eqz v4, :cond_6

    .line 170
    .line 171
    sget-object v5, Ljava/awt/font/TextAttribute;->SUPERSCRIPT:Ljava/awt/font/TextAttribute;

    .line 172
    .line 173
    if-lez v4, :cond_5

    .line 174
    .line 175
    sget-object v4, Ljava/awt/font/TextAttribute;->SUPERSCRIPT_SUPER:Ljava/lang/Integer;

    .line 176
    .line 177
    goto :goto_1

    .line 178
    :cond_5
    sget-object v4, Ljava/awt/font/TextAttribute;->SUPERSCRIPT_SUB:Ljava/lang/Integer;

    .line 179
    .line 180
    :goto_1
    invoke-virtual {v1, v5, v4, v2, v3}, Ljava/text/AttributedString;->addAttribute(Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;II)V

    .line 181
    .line 182
    .line 183
    :cond_6
    :goto_2
    add-int/lit8 v0, v0, 0x1

    .line 184
    .line 185
    goto/16 :goto_0

    .line 186
    .line 187
    :cond_7
    return-object v1
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

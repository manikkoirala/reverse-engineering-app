.class public final Lcom/intsig/office/fc/hslf/record/Document;
.super Lcom/intsig/office/fc/hslf/record/PositionDependentRecordContainer;
.source "Document.java"


# static fields
.field private static _type:J = 0x3e8L


# instance fields
.field private _header:[B

.field private documentAtom:Lcom/intsig/office/fc/hslf/record/DocumentAtom;

.field private environment:Lcom/intsig/office/fc/hslf/record/Environment;

.field private exObjList:Lcom/intsig/office/fc/hslf/record/ExObjList;

.field private list:Lcom/intsig/office/fc/hslf/record/List;

.field private ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;

.field private slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected constructor <init>([BII)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/PositionDependentRecordContainer;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x8

    .line 5
    .line 6
    new-array v1, v0, [B

    .line 7
    .line 8
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/Document;->_header:[B

    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 12
    .line 13
    .line 14
    add-int/2addr p2, v0

    .line 15
    sub-int/2addr p3, v0

    .line 16
    invoke-static {p1, p2, p3}, Lcom/intsig/office/fc/hslf/record/Record;->findChildRecords([BII)[Lcom/intsig/office/fc/hslf/record/Record;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 21
    .line 22
    aget-object p1, p1, v2

    .line 23
    .line 24
    instance-of p2, p1, Lcom/intsig/office/fc/hslf/record/DocumentAtom;

    .line 25
    .line 26
    if-eqz p2, :cond_8

    .line 27
    .line 28
    check-cast p1, Lcom/intsig/office/fc/hslf/record/DocumentAtom;

    .line 29
    .line 30
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/Document;->documentAtom:Lcom/intsig/office/fc/hslf/record/DocumentAtom;

    .line 31
    .line 32
    const/4 p1, 0x1

    .line 33
    const/4 p2, 0x1

    .line 34
    const/4 p3, 0x0

    .line 35
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 36
    .line 37
    array-length v1, v0

    .line 38
    if-ge p2, v1, :cond_5

    .line 39
    .line 40
    aget-object v0, v0, p2

    .line 41
    .line 42
    instance-of v1, v0, Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 43
    .line 44
    if-eqz v1, :cond_0

    .line 45
    .line 46
    add-int/lit8 p3, p3, 0x1

    .line 47
    .line 48
    goto :goto_1

    .line 49
    :cond_0
    instance-of v1, v0, Lcom/intsig/office/fc/hslf/record/Environment;

    .line 50
    .line 51
    if-eqz v1, :cond_1

    .line 52
    .line 53
    check-cast v0, Lcom/intsig/office/fc/hslf/record/Environment;

    .line 54
    .line 55
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->environment:Lcom/intsig/office/fc/hslf/record/Environment;

    .line 56
    .line 57
    goto :goto_1

    .line 58
    :cond_1
    instance-of v1, v0, Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;

    .line 59
    .line 60
    if-eqz v1, :cond_2

    .line 61
    .line 62
    check-cast v0, Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;

    .line 63
    .line 64
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;

    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_2
    instance-of v1, v0, Lcom/intsig/office/fc/hslf/record/ExObjList;

    .line 68
    .line 69
    if-eqz v1, :cond_3

    .line 70
    .line 71
    check-cast v0, Lcom/intsig/office/fc/hslf/record/ExObjList;

    .line 72
    .line 73
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->exObjList:Lcom/intsig/office/fc/hslf/record/ExObjList;

    .line 74
    .line 75
    goto :goto_1

    .line 76
    :cond_3
    instance-of v1, v0, Lcom/intsig/office/fc/hslf/record/List;

    .line 77
    .line 78
    if-eqz v1, :cond_4

    .line 79
    .line 80
    check-cast v0, Lcom/intsig/office/fc/hslf/record/List;

    .line 81
    .line 82
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->list:Lcom/intsig/office/fc/hslf/record/List;

    .line 83
    .line 84
    :cond_4
    :goto_1
    add-int/lit8 p2, p2, 0x1

    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_5
    new-array p2, p3, [Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 88
    .line 89
    iput-object p2, p0, Lcom/intsig/office/fc/hslf/record/Document;->slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 90
    .line 91
    :goto_2
    iget-object p2, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 92
    .line 93
    array-length p3, p2

    .line 94
    if-ge p1, p3, :cond_7

    .line 95
    .line 96
    aget-object p2, p2, p1

    .line 97
    .line 98
    instance-of p3, p2, Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 99
    .line 100
    if-eqz p3, :cond_6

    .line 101
    .line 102
    iget-object p3, p0, Lcom/intsig/office/fc/hslf/record/Document;->slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 103
    .line 104
    check-cast p2, Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 105
    .line 106
    aput-object p2, p3, v2

    .line 107
    .line 108
    add-int/lit8 v2, v2, 0x1

    .line 109
    .line 110
    :cond_6
    add-int/lit8 p1, p1, 0x1

    .line 111
    .line 112
    goto :goto_2

    .line 113
    :cond_7
    return-void

    .line 114
    :cond_8
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 115
    .line 116
    const-string p2, "The first child of a Document must be a DocumentAtom"

    .line 117
    .line 118
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    throw p1
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public addSlideListWithText(Lcom/intsig/office/fc/hslf/record/SlideListWithText;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    add-int/lit8 v1, v1, -0x1

    .line 5
    .line 6
    aget-object v0, v0, v1

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    .line 9
    .line 10
    .line 11
    move-result-wide v1

    .line 12
    sget-object v3, Lcom/intsig/office/fc/hslf/record/RecordTypes;->EndDocument:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 13
    .line 14
    iget v3, v3, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 15
    .line 16
    int-to-long v3, v3

    .line 17
    cmp-long v5, v1, v3

    .line 18
    .line 19
    if-nez v5, :cond_0

    .line 20
    .line 21
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->addChildBefore(Lcom/intsig/office/fc/hslf/record/Record;Lcom/intsig/office/fc/hslf/record/Record;)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 25
    .line 26
    array-length v1, v0

    .line 27
    add-int/lit8 v1, v1, 0x1

    .line 28
    .line 29
    new-array v2, v1, [Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 30
    .line 31
    array-length v3, v0

    .line 32
    const/4 v4, 0x0

    .line 33
    invoke-static {v0, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 34
    .line 35
    .line 36
    add-int/lit8 v1, v1, -0x1

    .line 37
    .line 38
    aput-object p1, v2, v1

    .line 39
    .line 40
    iput-object v2, p0, Lcom/intsig/office/fc/hslf/record/Document;->slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 41
    .line 42
    return-void

    .line 43
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 44
    .line 45
    new-instance v1, Ljava/lang/StringBuilder;

    .line 46
    .line 47
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 48
    .line 49
    .line 50
    const-string v2, "The last child record of a Document should be EndDocument, but it was "

    .line 51
    .line 52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    throw p1
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public dispose()V
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->_header:[B

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Document;->documentAtom:Lcom/intsig/office/fc/hslf/record/DocumentAtom;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/DocumentAtom;->dispose()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->documentAtom:Lcom/intsig/office/fc/hslf/record/DocumentAtom;

    .line 12
    .line 13
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Document;->environment:Lcom/intsig/office/fc/hslf/record/Environment;

    .line 14
    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/Environment;->dispose()V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->environment:Lcom/intsig/office/fc/hslf/record/Environment;

    .line 21
    .line 22
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Document;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;

    .line 23
    .line 24
    if-eqz v1, :cond_2

    .line 25
    .line 26
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;->dispose()V

    .line 27
    .line 28
    .line 29
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;

    .line 30
    .line 31
    :cond_2
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Document;->slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 32
    .line 33
    if-eqz v1, :cond_4

    .line 34
    .line 35
    array-length v2, v1

    .line 36
    const/4 v3, 0x0

    .line 37
    :goto_0
    if-ge v3, v2, :cond_3

    .line 38
    .line 39
    aget-object v4, v1, v3

    .line 40
    .line 41
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/SlideListWithText;->dispose()V

    .line 42
    .line 43
    .line 44
    add-int/lit8 v3, v3, 0x1

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_3
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 48
    .line 49
    :cond_4
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Document;->exObjList:Lcom/intsig/office/fc/hslf/record/ExObjList;

    .line 50
    .line 51
    if-eqz v1, :cond_5

    .line 52
    .line 53
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/ExObjList;->dispose()V

    .line 54
    .line 55
    .line 56
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->exObjList:Lcom/intsig/office/fc/hslf/record/ExObjList;

    .line 57
    .line 58
    :cond_5
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Document;->list:Lcom/intsig/office/fc/hslf/record/List;

    .line 59
    .line 60
    if-eqz v1, :cond_6

    .line 61
    .line 62
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/List;->dispose()V

    .line 63
    .line 64
    .line 65
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->list:Lcom/intsig/office/fc/hslf/record/List;

    .line 66
    .line 67
    :cond_6
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getDocumentAtom()Lcom/intsig/office/fc/hslf/record/DocumentAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->documentAtom:Lcom/intsig/office/fc/hslf/record/DocumentAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEnvironment()Lcom/intsig/office/fc/hslf/record/Environment;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->environment:Lcom/intsig/office/fc/hslf/record/Environment;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getExObjList()Lcom/intsig/office/fc/hslf/record/ExObjList;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->exObjList:Lcom/intsig/office/fc/hslf/record/ExObjList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getList()Lcom/intsig/office/fc/hslf/record/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->list:Lcom/intsig/office/fc/hslf/record/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMasterSlideListWithText()Lcom/intsig/office/fc/hslf/record/SlideListWithText;
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Document;->slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 3
    .line 4
    array-length v2, v1

    .line 5
    if-ge v0, v2, :cond_1

    .line 6
    .line 7
    aget-object v1, v1, v0

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/SlideListWithText;->getInstance()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, 0x1

    .line 14
    if-ne v1, v2, :cond_0

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Document;->slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 17
    .line 18
    aget-object v0, v1, v0

    .line 19
    .line 20
    return-object v0

    .line 21
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    const/4 v0, 0x0

    .line 25
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getNotesSlideListWithText()Lcom/intsig/office/fc/hslf/record/SlideListWithText;
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Document;->slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 3
    .line 4
    array-length v2, v1

    .line 5
    if-ge v0, v2, :cond_1

    .line 6
    .line 7
    aget-object v1, v1, v0

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/SlideListWithText;->getInstance()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, 0x2

    .line 14
    if-ne v1, v2, :cond_0

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Document;->slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 17
    .line 18
    aget-object v0, v1, v0

    .line 19
    .line 20
    return-object v0

    .line 21
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    const/4 v0, 0x0

    .line 25
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getPPDrawingGroup()Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->ppDrawing:Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/office/fc/hslf/record/Document;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideListWithTexts()[Lcom/intsig/office/fc/hslf/record/SlideListWithText;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/Document;->slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSlideSlideListWithText()Lcom/intsig/office/fc/hslf/record/SlideListWithText;
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Document;->slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 3
    .line 4
    array-length v2, v1

    .line 5
    if-ge v0, v2, :cond_1

    .line 6
    .line 7
    aget-object v1, v1, v0

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/SlideListWithText;->getInstance()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Document;->slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 16
    .line 17
    aget-object v0, v1, v0

    .line 18
    .line 19
    return-object v0

    .line 20
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    const/4 v0, 0x0

    .line 24
    return-object v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public removeSlideListWithText(Lcom/intsig/office/fc/hslf/record/SlideListWithText;)V
    .locals 5

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/Document;->slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 7
    .line 8
    array-length v2, v1

    .line 9
    const/4 v3, 0x0

    .line 10
    :goto_0
    if-ge v3, v2, :cond_1

    .line 11
    .line 12
    aget-object v4, v1, v3

    .line 13
    .line 14
    if-eq v4, p1, :cond_0

    .line 15
    .line 16
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 17
    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->removeChild(Lcom/intsig/office/fc/hslf/record/Record;)Lcom/intsig/office/fc/hslf/record/Record;

    .line 21
    .line 22
    .line 23
    :goto_1
    add-int/lit8 v3, v3, 0x1

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    new-array p1, p1, [Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 31
    .line 32
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    check-cast p1, [Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 37
    .line 38
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/Document;->slwts:[Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

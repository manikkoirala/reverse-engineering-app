.class public Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer;
.super Lcom/intsig/office/fc/hslf/record/PositionDependentRecordContainer;
.source "ExtendedPresRuleContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;
    }
.end annotation


# static fields
.field private static _type:J = 0xfaeL


# instance fields
.field private _extendedAtomsSets:[Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

.field private _header:[B


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected constructor <init>([BII)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/PositionDependentRecordContainer;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x8

    .line 5
    .line 6
    new-array v1, v0, [B

    .line 7
    .line 8
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer;->_header:[B

    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 12
    .line 13
    .line 14
    add-int/2addr p2, v0

    .line 15
    sub-int/2addr p3, v0

    .line 16
    invoke-static {p1, p2, p3}, Lcom/intsig/office/fc/hslf/record/Record;->findChildRecords([BII)[Lcom/intsig/office/fc/hslf/record/Record;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 21
    .line 22
    new-instance p1, Ljava/util/Vector;

    .line 23
    .line 24
    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    .line 25
    .line 26
    .line 27
    :goto_0
    iget-object p2, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 28
    .line 29
    array-length p3, p2

    .line 30
    if-ge v2, p3, :cond_2

    .line 31
    .line 32
    aget-object p2, p2, v2

    .line 33
    .line 34
    instance-of p2, p2, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;

    .line 35
    .line 36
    if-eqz p2, :cond_1

    .line 37
    .line 38
    add-int/lit8 p2, v2, -0x1

    .line 39
    .line 40
    :goto_1
    if-ltz p2, :cond_1

    .line 41
    .line 42
    iget-object p3, p0, Lcom/intsig/office/fc/hslf/record/RecordContainer;->_children:[Lcom/intsig/office/fc/hslf/record/Record;

    .line 43
    .line 44
    aget-object v0, p3, p2

    .line 45
    .line 46
    instance-of v1, v0, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphHeaderAtom;

    .line 47
    .line 48
    if-eqz v1, :cond_0

    .line 49
    .line 50
    new-instance p2, Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

    .line 51
    .line 52
    check-cast v0, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphHeaderAtom;

    .line 53
    .line 54
    aget-object p3, p3, v2

    .line 55
    .line 56
    check-cast p3, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;

    .line 57
    .line 58
    invoke-direct {p2, p0, v0, p3}, Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;-><init>(Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer;Lcom/intsig/office/fc/hslf/record/ExtendedParagraphHeaderAtom;Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {p1, p2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    goto :goto_2

    .line 65
    :cond_0
    add-int/lit8 p2, p2, -0x1

    .line 66
    .line 67
    goto :goto_1

    .line 68
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_2
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    .line 72
    .line 73
    .line 74
    move-result p2

    .line 75
    new-array p2, p2, [Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

    .line 76
    .line 77
    invoke-virtual {p1, p2}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 78
    .line 79
    .line 80
    move-result-object p1

    .line 81
    check-cast p1, [Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

    .line 82
    .line 83
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer;->_extendedAtomsSets:[Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

    .line 84
    .line 85
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public dispose()V
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer;->_header:[B

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer;->_extendedAtomsSets:[Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

    .line 5
    .line 6
    if-eqz v1, :cond_1

    .line 7
    .line 8
    array-length v2, v1

    .line 9
    const/4 v3, 0x0

    .line 10
    :goto_0
    if-ge v3, v2, :cond_0

    .line 11
    .line 12
    aget-object v4, v1, v3

    .line 13
    .line 14
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;->dispose()V

    .line 15
    .line 16
    .line 17
    add-int/lit8 v3, v3, 0x1

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer;->_extendedAtomsSets:[Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

    .line 21
    .line 22
    :cond_1
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getExtendedParaAtomsSets()[Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer;->_extendedAtomsSets:[Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer$ExtendedParaAtomsSet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

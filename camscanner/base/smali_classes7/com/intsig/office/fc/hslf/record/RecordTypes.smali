.class public final Lcom/intsig/office/fc/hslf/record/RecordTypes;
.super Ljava/lang/Object;
.source "RecordTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;
    }
.end annotation


# static fields
.field public static final AnimationInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final AnimationInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final BaseTextPropAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final BinaryTagDataBlob:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final BookmarkCollection:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final BookmarkEntityAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final BookmarkSeedAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final CString:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final CharFormatAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ClientVisualElementContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ColorSchemeAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final Comment2000:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final Comment2000Atom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final Comment2000Summary:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final Comment2000SummaryAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final CompositeMasterId:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final CurrentUserAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final DateTimeMCAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final DefaultRulerAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final DocRoutingSlip:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final Document:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final DocumentAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final DocumentEncryptionAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final EndDocument:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final Environment:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final EscherAlignRule:I = 0xf013

.field public static final EscherAnchor:I = 0xf00e

.field public static final EscherArcRule:I = 0xf014

.field public static final EscherBSE:I = 0xf007

.field public static final EscherBStoreContainer:I = 0xf001

.field public static final EscherBlip_END:I = 0xf117

.field public static final EscherBlip_START:I = 0xf018

.field public static final EscherCLSID:I = 0xf016

.field public static final EscherCalloutRule:I = 0xf017

.field public static final EscherChildAnchor:I = 0xf00f

.field public static final EscherClientAnchor:I = 0xf010

.field public static final EscherClientData:I = 0xf011

.field public static final EscherClientRule:I = 0xf015

.field public static final EscherClientTextbox:I = 0xf00d

.field public static final EscherColorMRU:I = 0xf11a

.field public static final EscherColorScheme:I = 0xf120

.field public static final EscherConnectorRule:I = 0xf012

.field public static final EscherDeletedPspl:I = 0xf11d

.field public static final EscherDg:I = 0xf008

.field public static final EscherDgContainer:I = 0xf002

.field public static final EscherDgg:I = 0xf006

.field public static final EscherDggContainer:I = 0xf000

.field public static final EscherOPT:I = 0xf00b

.field public static final EscherOleObject:I = 0xf11f

.field public static final EscherRegroupItems:I = 0xf118

.field public static final EscherSelection:I = 0xf119

.field public static final EscherSolverContainer:I = 0xf005

.field public static final EscherSp:I = 0xf00a

.field public static final EscherSpContainer:I = 0xf004

.field public static final EscherSpgr:I = 0xf009

.field public static final EscherSpgrContainer:I = 0xf003

.field public static final EscherSplitMenuColors:I = 0xf11e

.field public static final EscherTextbox:I = 0xf00c

.field public static final EscherUserDefined:I = 0xf122

.field public static final ExAviMovie:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExCDAudio:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExCDAudioAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExControl:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExControlAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExEmbed:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExEmbedAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExHyperlink:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExHyperlinkAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExLink:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExLinkAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExMCIMovie:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExMIDIAudio:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExMediaAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExObjList:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExObjListAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExObjRefAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExOleObjAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExOleObjStg:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExQuickTimeMovie:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExQuickTimeMovieData:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExVideoContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExWAVAudioEmbedded:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExWAVAudioEmbeddedAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExWAVAudioLink:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExtendedParagraphAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExtendedParagraphHeaderAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ExtendedPreRuleContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final FontCollection:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final FontEmbeddedData:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final FontEntityAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final FooterMCAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final GPopublicintAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final GRColorAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final GRatioAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final GScalingAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final GenericDateMCAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final GuideAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final HandOut:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final HeadersFooters:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final HeadersFootersAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final InteractiveInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final InteractiveInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final List:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final MainMaster:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final MetaFile:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final NamedShow:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final NamedShowSlides:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final NamedShows:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final Notes:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final NotesAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final OEPlaceholderAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final OEShapeAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final OriginalMainMasterId:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final OutlineTextRefAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final OutlineViewInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final PPDrawing:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final PPDrawingGroup:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ParaFormatAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final PersistPtrFullBlock:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final PersistPtrIncrementalBlock:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ProgStringTag:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final PrpublicintOptions:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final RTFDateTimeMCAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final RecolorInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final RoundTripContentMasterId:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final RoundTripContentMasterInfo12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final RoundTripCustomTableStyles12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final RoundTripHFPlaceholder12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final RoundTripNotesMasterTextStyles12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final RoundTripOArtTextStyles12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final RoundTripShapeCheckSumForCustomLayouts12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final RoundTripShapeId12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SSDocInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SSlideLayoutAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SheetProperties:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SlaveContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final Slide:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SlideAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SlideListWithText:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SlideNumberMCAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SlidePersistAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SlideProgBinaryTagContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SlideProgTagsContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SlideShowSlideInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SlideTimeAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SlideViewInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SlideViewInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SorterViewInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final Sound:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SoundCollAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SoundCollection:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SoundData:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SrKinsoku:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final SrKinsokuAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final StyleTextPropAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final Summary:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TextBookmarkAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TextBytesAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TextCharsAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TextHeaderAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TextRulerAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TextSpecInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeAnimateBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeColorBehaviorAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeColorBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeCommandBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeConditionAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeConditionContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeEffectBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeIterateDataAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeMotionBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeNodeAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeNodeAttributeContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeNodeContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeRotationBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeScaleBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeSequenceDataAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeSetBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TimeVariant:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TxCFStyleAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TxInteractiveInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TxMasterStyleAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TxPFStyleAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final TxSIStyleAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final Unknown:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final UserEditAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final VBAInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final VBAInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ViewInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final ViewInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static final VisualShapeAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

.field public static typeToClass:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/intsig/office/fc/hslf/record/Record;",
            ">;>;"
        }
    .end annotation
.end field

.field public static typeToName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x0

    .line 5
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->Unknown:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 9
    .line 10
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 11
    .line 12
    const/16 v3, 0x3e8

    .line 13
    .line 14
    const-class v4, Lcom/intsig/office/fc/hslf/record/Document;

    .line 15
    .line 16
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->Document:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 20
    .line 21
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 22
    .line 23
    const/16 v3, 0x3e9

    .line 24
    .line 25
    const-class v4, Lcom/intsig/office/fc/hslf/record/DocumentAtom;

    .line 26
    .line 27
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->DocumentAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 31
    .line 32
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 33
    .line 34
    const/16 v3, 0x3ea

    .line 35
    .line 36
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 37
    .line 38
    .line 39
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->EndDocument:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 40
    .line 41
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 42
    .line 43
    const/16 v3, 0x3ee

    .line 44
    .line 45
    const-class v4, Lcom/intsig/office/fc/hslf/record/Slide;

    .line 46
    .line 47
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 48
    .line 49
    .line 50
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->Slide:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 51
    .line 52
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 53
    .line 54
    const/16 v3, 0x3ef

    .line 55
    .line 56
    const-class v4, Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 57
    .line 58
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 59
    .line 60
    .line 61
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlideAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 62
    .line 63
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 64
    .line 65
    const/16 v3, 0x3f0

    .line 66
    .line 67
    const-class v4, Lcom/intsig/office/fc/hslf/record/Notes;

    .line 68
    .line 69
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 70
    .line 71
    .line 72
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->Notes:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 73
    .line 74
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 75
    .line 76
    const/16 v3, 0x3f1

    .line 77
    .line 78
    const-class v4, Lcom/intsig/office/fc/hslf/record/NotesAtom;

    .line 79
    .line 80
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 81
    .line 82
    .line 83
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->NotesAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 84
    .line 85
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 86
    .line 87
    const/16 v3, 0x3f2

    .line 88
    .line 89
    const-class v4, Lcom/intsig/office/fc/hslf/record/Environment;

    .line 90
    .line 91
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 92
    .line 93
    .line 94
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->Environment:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 95
    .line 96
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 97
    .line 98
    const/16 v3, 0x3f3

    .line 99
    .line 100
    const-class v4, Lcom/intsig/office/fc/hslf/record/SlidePersistAtom;

    .line 101
    .line 102
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 103
    .line 104
    .line 105
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlidePersistAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 106
    .line 107
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 108
    .line 109
    const/16 v3, 0x3f7

    .line 110
    .line 111
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 112
    .line 113
    .line 114
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SSlideLayoutAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 115
    .line 116
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 117
    .line 118
    const/16 v3, 0x3f8

    .line 119
    .line 120
    const-class v4, Lcom/intsig/office/fc/hslf/record/MainMaster;

    .line 121
    .line 122
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 123
    .line 124
    .line 125
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->MainMaster:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 126
    .line 127
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 128
    .line 129
    const/16 v3, 0x3fa

    .line 130
    .line 131
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 132
    .line 133
    .line 134
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlideViewInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 135
    .line 136
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 137
    .line 138
    const/16 v3, 0x3fb

    .line 139
    .line 140
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 141
    .line 142
    .line 143
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->GuideAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 144
    .line 145
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 146
    .line 147
    const/16 v3, 0x3fc

    .line 148
    .line 149
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 150
    .line 151
    .line 152
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ViewInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 153
    .line 154
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 155
    .line 156
    const/16 v3, 0x3fd

    .line 157
    .line 158
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 159
    .line 160
    .line 161
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ViewInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 162
    .line 163
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 164
    .line 165
    const/16 v3, 0x3fe

    .line 166
    .line 167
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 168
    .line 169
    .line 170
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlideViewInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 171
    .line 172
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 173
    .line 174
    const/16 v3, 0x3ff

    .line 175
    .line 176
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 177
    .line 178
    .line 179
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->VBAInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 180
    .line 181
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 182
    .line 183
    const/16 v3, 0x400

    .line 184
    .line 185
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 186
    .line 187
    .line 188
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->VBAInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 189
    .line 190
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 191
    .line 192
    const/16 v3, 0x401

    .line 193
    .line 194
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 195
    .line 196
    .line 197
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SSDocInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 198
    .line 199
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 200
    .line 201
    const/16 v3, 0x402

    .line 202
    .line 203
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 204
    .line 205
    .line 206
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->Summary:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 207
    .line 208
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 209
    .line 210
    const/16 v3, 0x406

    .line 211
    .line 212
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 213
    .line 214
    .line 215
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->DocRoutingSlip:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 216
    .line 217
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 218
    .line 219
    const/16 v3, 0x407

    .line 220
    .line 221
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 222
    .line 223
    .line 224
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->OutlineViewInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 225
    .line 226
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 227
    .line 228
    const/16 v3, 0x408

    .line 229
    .line 230
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 231
    .line 232
    .line 233
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SorterViewInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 234
    .line 235
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 236
    .line 237
    const/16 v3, 0x409

    .line 238
    .line 239
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExObjList;

    .line 240
    .line 241
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 242
    .line 243
    .line 244
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExObjList:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 245
    .line 246
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 247
    .line 248
    const/16 v3, 0x40a

    .line 249
    .line 250
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExObjListAtom;

    .line 251
    .line 252
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 253
    .line 254
    .line 255
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExObjListAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 256
    .line 257
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 258
    .line 259
    const/16 v3, 0x40b

    .line 260
    .line 261
    const-class v4, Lcom/intsig/office/fc/hslf/record/PPDrawingGroup;

    .line 262
    .line 263
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 264
    .line 265
    .line 266
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->PPDrawingGroup:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 267
    .line 268
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 269
    .line 270
    const/16 v3, 0x40c

    .line 271
    .line 272
    const-class v4, Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 273
    .line 274
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 275
    .line 276
    .line 277
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->PPDrawing:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 278
    .line 279
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 280
    .line 281
    const/16 v3, 0x410

    .line 282
    .line 283
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 284
    .line 285
    .line 286
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->NamedShows:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 287
    .line 288
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 289
    .line 290
    const/16 v3, 0x411

    .line 291
    .line 292
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 293
    .line 294
    .line 295
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->NamedShow:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 296
    .line 297
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 298
    .line 299
    const/16 v3, 0x412

    .line 300
    .line 301
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 302
    .line 303
    .line 304
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->NamedShowSlides:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 305
    .line 306
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 307
    .line 308
    const/16 v3, 0x414

    .line 309
    .line 310
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 311
    .line 312
    .line 313
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SheetProperties:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 314
    .line 315
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 316
    .line 317
    const/16 v3, 0x7d0

    .line 318
    .line 319
    const-class v4, Lcom/intsig/office/fc/hslf/record/List;

    .line 320
    .line 321
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 322
    .line 323
    .line 324
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->List:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 325
    .line 326
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 327
    .line 328
    const/16 v3, 0x7d5

    .line 329
    .line 330
    const-class v4, Lcom/intsig/office/fc/hslf/record/FontCollection;

    .line 331
    .line 332
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 333
    .line 334
    .line 335
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->FontCollection:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 336
    .line 337
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 338
    .line 339
    const/16 v3, 0x7e3

    .line 340
    .line 341
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 342
    .line 343
    .line 344
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->BookmarkCollection:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 345
    .line 346
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 347
    .line 348
    const/16 v3, 0x7e4

    .line 349
    .line 350
    const-class v4, Lcom/intsig/office/fc/hslf/record/SoundCollection;

    .line 351
    .line 352
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 353
    .line 354
    .line 355
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SoundCollection:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 356
    .line 357
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 358
    .line 359
    const/16 v3, 0x7e5

    .line 360
    .line 361
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 362
    .line 363
    .line 364
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SoundCollAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 365
    .line 366
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 367
    .line 368
    const/16 v3, 0x7e6

    .line 369
    .line 370
    const-class v4, Lcom/intsig/office/fc/hslf/record/Sound;

    .line 371
    .line 372
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 373
    .line 374
    .line 375
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->Sound:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 376
    .line 377
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 378
    .line 379
    const/16 v3, 0x7e7

    .line 380
    .line 381
    const-class v4, Lcom/intsig/office/fc/hslf/record/SoundData;

    .line 382
    .line 383
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 384
    .line 385
    .line 386
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SoundData:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 387
    .line 388
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 389
    .line 390
    const/16 v3, 0x7e9

    .line 391
    .line 392
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 393
    .line 394
    .line 395
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->BookmarkSeedAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 396
    .line 397
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 398
    .line 399
    const/16 v3, 0x7f0

    .line 400
    .line 401
    const-class v4, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 402
    .line 403
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 404
    .line 405
    .line 406
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ColorSchemeAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 407
    .line 408
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 409
    .line 410
    const/16 v3, 0xbc1

    .line 411
    .line 412
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 413
    .line 414
    .line 415
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExObjRefAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 416
    .line 417
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 418
    .line 419
    const-class v4, Lcom/intsig/office/fc/hslf/record/OEShapeAtom;

    .line 420
    .line 421
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 422
    .line 423
    .line 424
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->OEShapeAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 425
    .line 426
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 427
    .line 428
    const/16 v3, 0xbc3

    .line 429
    .line 430
    const-class v4, Lcom/intsig/office/fc/hslf/record/OEPlaceholderAtom;

    .line 431
    .line 432
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 433
    .line 434
    .line 435
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->OEPlaceholderAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 436
    .line 437
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 438
    .line 439
    const/16 v3, 0xbd0

    .line 440
    .line 441
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 442
    .line 443
    .line 444
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->GPopublicintAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 445
    .line 446
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 447
    .line 448
    const/16 v3, 0xbd7

    .line 449
    .line 450
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 451
    .line 452
    .line 453
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->GRatioAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 454
    .line 455
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 456
    .line 457
    const/16 v3, 0xf9e

    .line 458
    .line 459
    const-class v4, Lcom/intsig/office/fc/hslf/record/OutlineTextRefAtom;

    .line 460
    .line 461
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 462
    .line 463
    .line 464
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->OutlineTextRefAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 465
    .line 466
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 467
    .line 468
    const/16 v3, 0xf9f

    .line 469
    .line 470
    const-class v4, Lcom/intsig/office/fc/hslf/record/TextHeaderAtom;

    .line 471
    .line 472
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 473
    .line 474
    .line 475
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TextHeaderAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 476
    .line 477
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 478
    .line 479
    const/16 v3, 0xfa0

    .line 480
    .line 481
    const-class v4, Lcom/intsig/office/fc/hslf/record/TextCharsAtom;

    .line 482
    .line 483
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 484
    .line 485
    .line 486
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TextCharsAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 487
    .line 488
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 489
    .line 490
    const/16 v3, 0xfa1

    .line 491
    .line 492
    const-class v4, Lcom/intsig/office/fc/hslf/record/StyleTextPropAtom;

    .line 493
    .line 494
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 495
    .line 496
    .line 497
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->StyleTextPropAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 498
    .line 499
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 500
    .line 501
    const/16 v3, 0xfa2

    .line 502
    .line 503
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 504
    .line 505
    .line 506
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->BaseTextPropAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 507
    .line 508
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 509
    .line 510
    const/16 v3, 0xfa3

    .line 511
    .line 512
    const-class v4, Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 513
    .line 514
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 515
    .line 516
    .line 517
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TxMasterStyleAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 518
    .line 519
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 520
    .line 521
    const/16 v3, 0xfa4

    .line 522
    .line 523
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 524
    .line 525
    .line 526
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TxCFStyleAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 527
    .line 528
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 529
    .line 530
    const/16 v3, 0xfa5

    .line 531
    .line 532
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 533
    .line 534
    .line 535
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TxPFStyleAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 536
    .line 537
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 538
    .line 539
    const/16 v3, 0xfa6

    .line 540
    .line 541
    const-class v4, Lcom/intsig/office/fc/hslf/record/TextRulerAtom;

    .line 542
    .line 543
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 544
    .line 545
    .line 546
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TextRulerAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 547
    .line 548
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 549
    .line 550
    const/16 v3, 0xfa7

    .line 551
    .line 552
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 553
    .line 554
    .line 555
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TextBookmarkAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 556
    .line 557
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 558
    .line 559
    const/16 v3, 0xfa8

    .line 560
    .line 561
    const-class v4, Lcom/intsig/office/fc/hslf/record/TextBytesAtom;

    .line 562
    .line 563
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 564
    .line 565
    .line 566
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TextBytesAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 567
    .line 568
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 569
    .line 570
    const/16 v3, 0xfa9

    .line 571
    .line 572
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 573
    .line 574
    .line 575
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TxSIStyleAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 576
    .line 577
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 578
    .line 579
    const/16 v3, 0xfaa

    .line 580
    .line 581
    const-class v4, Lcom/intsig/office/fc/hslf/record/TextSpecInfoAtom;

    .line 582
    .line 583
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 584
    .line 585
    .line 586
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TextSpecInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 587
    .line 588
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 589
    .line 590
    const/16 v3, 0xfab

    .line 591
    .line 592
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 593
    .line 594
    .line 595
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->DefaultRulerAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 596
    .line 597
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 598
    .line 599
    const/16 v3, 0xfac

    .line 600
    .line 601
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphAtom;

    .line 602
    .line 603
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 604
    .line 605
    .line 606
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExtendedParagraphAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 607
    .line 608
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 609
    .line 610
    const/16 v3, 0xfae

    .line 611
    .line 612
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExtendedPresRuleContainer;

    .line 613
    .line 614
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 615
    .line 616
    .line 617
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExtendedPreRuleContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 618
    .line 619
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 620
    .line 621
    const/16 v3, 0xfaf

    .line 622
    .line 623
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExtendedParagraphHeaderAtom;

    .line 624
    .line 625
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 626
    .line 627
    .line 628
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExtendedParagraphHeaderAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 629
    .line 630
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 631
    .line 632
    const/16 v3, 0xfb7

    .line 633
    .line 634
    const-class v4, Lcom/intsig/office/fc/hslf/record/FontEntityAtom;

    .line 635
    .line 636
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 637
    .line 638
    .line 639
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->FontEntityAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 640
    .line 641
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 642
    .line 643
    const/16 v3, 0xfb8

    .line 644
    .line 645
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 646
    .line 647
    .line 648
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->FontEmbeddedData:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 649
    .line 650
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 651
    .line 652
    const/16 v3, 0xfba

    .line 653
    .line 654
    const-class v4, Lcom/intsig/office/fc/hslf/record/CString;

    .line 655
    .line 656
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 657
    .line 658
    .line 659
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->CString:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 660
    .line 661
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 662
    .line 663
    const/16 v3, 0xfc1

    .line 664
    .line 665
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 666
    .line 667
    .line 668
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->MetaFile:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 669
    .line 670
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 671
    .line 672
    const/16 v3, 0xfc3

    .line 673
    .line 674
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExOleObjAtom;

    .line 675
    .line 676
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 677
    .line 678
    .line 679
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExOleObjAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 680
    .line 681
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 682
    .line 683
    const/16 v3, 0xfc8

    .line 684
    .line 685
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 686
    .line 687
    .line 688
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SrKinsoku:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 689
    .line 690
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 691
    .line 692
    const/16 v3, 0xfc9

    .line 693
    .line 694
    const-class v4, Lcom/intsig/office/fc/hslf/record/DummyPositionSensitiveRecordWithChildren;

    .line 695
    .line 696
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 697
    .line 698
    .line 699
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->HandOut:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 700
    .line 701
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 702
    .line 703
    const/16 v3, 0xfcc

    .line 704
    .line 705
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExEmbed;

    .line 706
    .line 707
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 708
    .line 709
    .line 710
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExEmbed:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 711
    .line 712
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 713
    .line 714
    const/16 v3, 0xfcd

    .line 715
    .line 716
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExEmbedAtom;

    .line 717
    .line 718
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 719
    .line 720
    .line 721
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExEmbedAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 722
    .line 723
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 724
    .line 725
    const/16 v3, 0xfce

    .line 726
    .line 727
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 728
    .line 729
    .line 730
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExLink:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 731
    .line 732
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 733
    .line 734
    const/16 v3, 0xfd0

    .line 735
    .line 736
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 737
    .line 738
    .line 739
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->BookmarkEntityAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 740
    .line 741
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 742
    .line 743
    const/16 v3, 0xfd1

    .line 744
    .line 745
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 746
    .line 747
    .line 748
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExLinkAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 749
    .line 750
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 751
    .line 752
    const/16 v3, 0xfd2

    .line 753
    .line 754
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 755
    .line 756
    .line 757
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SrKinsokuAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 758
    .line 759
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 760
    .line 761
    const/16 v3, 0xfd3

    .line 762
    .line 763
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExHyperlinkAtom;

    .line 764
    .line 765
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 766
    .line 767
    .line 768
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExHyperlinkAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 769
    .line 770
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 771
    .line 772
    const/16 v3, 0xfd7

    .line 773
    .line 774
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExHyperlink;

    .line 775
    .line 776
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 777
    .line 778
    .line 779
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExHyperlink:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 780
    .line 781
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 782
    .line 783
    const/16 v3, 0xfd8

    .line 784
    .line 785
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 786
    .line 787
    .line 788
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlideNumberMCAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 789
    .line 790
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 791
    .line 792
    const/16 v3, 0xfd9

    .line 793
    .line 794
    const-class v4, Lcom/intsig/office/fc/hslf/record/HeadersFootersContainer;

    .line 795
    .line 796
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 797
    .line 798
    .line 799
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->HeadersFooters:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 800
    .line 801
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 802
    .line 803
    const/16 v3, 0xfda

    .line 804
    .line 805
    const-class v4, Lcom/intsig/office/fc/hslf/record/HeadersFootersAtom;

    .line 806
    .line 807
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 808
    .line 809
    .line 810
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->HeadersFootersAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 811
    .line 812
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 813
    .line 814
    const/16 v3, 0xfdf

    .line 815
    .line 816
    const-class v4, Lcom/intsig/office/fc/hslf/record/TxInteractiveInfoAtom;

    .line 817
    .line 818
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 819
    .line 820
    .line 821
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TxInteractiveInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 822
    .line 823
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 824
    .line 825
    const/16 v3, 0xfe2

    .line 826
    .line 827
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 828
    .line 829
    .line 830
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->CharFormatAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 831
    .line 832
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 833
    .line 834
    const/16 v3, 0xfe3

    .line 835
    .line 836
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 837
    .line 838
    .line 839
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ParaFormatAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 840
    .line 841
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 842
    .line 843
    const/16 v3, 0xfe7

    .line 844
    .line 845
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 846
    .line 847
    .line 848
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->RecolorInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 849
    .line 850
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 851
    .line 852
    const/16 v3, 0xfea

    .line 853
    .line 854
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 855
    .line 856
    .line 857
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExQuickTimeMovie:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 858
    .line 859
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 860
    .line 861
    const/16 v3, 0xfeb

    .line 862
    .line 863
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 864
    .line 865
    .line 866
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExQuickTimeMovieData:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 867
    .line 868
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 869
    .line 870
    const/16 v3, 0xfee

    .line 871
    .line 872
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExControl;

    .line 873
    .line 874
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 875
    .line 876
    .line 877
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExControl:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 878
    .line 879
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 880
    .line 881
    const/16 v3, 0xff0

    .line 882
    .line 883
    const-class v4, Lcom/intsig/office/fc/hslf/record/SlideListWithText;

    .line 884
    .line 885
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 886
    .line 887
    .line 888
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlideListWithText:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 889
    .line 890
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 891
    .line 892
    const/16 v3, 0xff2

    .line 893
    .line 894
    const-class v4, Lcom/intsig/office/fc/hslf/record/InteractiveInfo;

    .line 895
    .line 896
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 897
    .line 898
    .line 899
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->InteractiveInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 900
    .line 901
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 902
    .line 903
    const/16 v3, 0xff3

    .line 904
    .line 905
    const-class v4, Lcom/intsig/office/fc/hslf/record/InteractiveInfoAtom;

    .line 906
    .line 907
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 908
    .line 909
    .line 910
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->InteractiveInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 911
    .line 912
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 913
    .line 914
    const/16 v3, 0xff5

    .line 915
    .line 916
    const-class v4, Lcom/intsig/office/fc/hslf/record/UserEditAtom;

    .line 917
    .line 918
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 919
    .line 920
    .line 921
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->UserEditAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 922
    .line 923
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 924
    .line 925
    const/16 v3, 0xff6

    .line 926
    .line 927
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 928
    .line 929
    .line 930
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->CurrentUserAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 931
    .line 932
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 933
    .line 934
    const/16 v3, 0xff7

    .line 935
    .line 936
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 937
    .line 938
    .line 939
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->DateTimeMCAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 940
    .line 941
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 942
    .line 943
    const/16 v3, 0xff8

    .line 944
    .line 945
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 946
    .line 947
    .line 948
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->GenericDateMCAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 949
    .line 950
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 951
    .line 952
    const/16 v3, 0xffa

    .line 953
    .line 954
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 955
    .line 956
    .line 957
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->FooterMCAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 958
    .line 959
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 960
    .line 961
    const/16 v3, 0xffb

    .line 962
    .line 963
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExControlAtom;

    .line 964
    .line 965
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 966
    .line 967
    .line 968
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExControlAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 969
    .line 970
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 971
    .line 972
    const/16 v3, 0x1004

    .line 973
    .line 974
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExMediaAtom;

    .line 975
    .line 976
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 977
    .line 978
    .line 979
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExMediaAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 980
    .line 981
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 982
    .line 983
    const/16 v3, 0x1005

    .line 984
    .line 985
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExVideoContainer;

    .line 986
    .line 987
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 988
    .line 989
    .line 990
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExVideoContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 991
    .line 992
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 993
    .line 994
    const/16 v3, 0x1006

    .line 995
    .line 996
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExAviMovie;

    .line 997
    .line 998
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 999
    .line 1000
    .line 1001
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExAviMovie:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1002
    .line 1003
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1004
    .line 1005
    const/16 v3, 0x1007

    .line 1006
    .line 1007
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExMCIMovie;

    .line 1008
    .line 1009
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1010
    .line 1011
    .line 1012
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExMCIMovie:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1013
    .line 1014
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1015
    .line 1016
    const/16 v3, 0x100d

    .line 1017
    .line 1018
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1019
    .line 1020
    .line 1021
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExMIDIAudio:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1022
    .line 1023
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1024
    .line 1025
    const/16 v3, 0x100e

    .line 1026
    .line 1027
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1028
    .line 1029
    .line 1030
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExCDAudio:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1031
    .line 1032
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1033
    .line 1034
    const/16 v3, 0x100f

    .line 1035
    .line 1036
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1037
    .line 1038
    .line 1039
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExWAVAudioEmbedded:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1040
    .line 1041
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1042
    .line 1043
    const/16 v3, 0x1010

    .line 1044
    .line 1045
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1046
    .line 1047
    .line 1048
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExWAVAudioLink:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1049
    .line 1050
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1051
    .line 1052
    const/16 v3, 0x1011

    .line 1053
    .line 1054
    const-class v4, Lcom/intsig/office/fc/hslf/record/ExOleObjStg;

    .line 1055
    .line 1056
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1057
    .line 1058
    .line 1059
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExOleObjStg:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1060
    .line 1061
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1062
    .line 1063
    const/16 v3, 0x1012

    .line 1064
    .line 1065
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1066
    .line 1067
    .line 1068
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExCDAudioAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1069
    .line 1070
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1071
    .line 1072
    const/16 v3, 0x1013

    .line 1073
    .line 1074
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1075
    .line 1076
    .line 1077
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ExWAVAudioEmbeddedAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1078
    .line 1079
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1080
    .line 1081
    const/16 v3, 0x1014

    .line 1082
    .line 1083
    const-class v4, Lcom/intsig/office/fc/hslf/record/AnimationInfo;

    .line 1084
    .line 1085
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1086
    .line 1087
    .line 1088
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->AnimationInfo:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1089
    .line 1090
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1091
    .line 1092
    const/16 v3, 0xff1

    .line 1093
    .line 1094
    const-class v4, Lcom/intsig/office/fc/hslf/record/AnimationInfoAtom;

    .line 1095
    .line 1096
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1097
    .line 1098
    .line 1099
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->AnimationInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1100
    .line 1101
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1102
    .line 1103
    const/16 v3, 0x1015

    .line 1104
    .line 1105
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1106
    .line 1107
    .line 1108
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->RTFDateTimeMCAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1109
    .line 1110
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1111
    .line 1112
    const/16 v3, 0x1389

    .line 1113
    .line 1114
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1115
    .line 1116
    .line 1117
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ProgStringTag:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1118
    .line 1119
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1120
    .line 1121
    const/16 v3, 0x1770

    .line 1122
    .line 1123
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1124
    .line 1125
    .line 1126
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->PrpublicintOptions:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1127
    .line 1128
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1129
    .line 1130
    const/16 v3, 0x1771

    .line 1131
    .line 1132
    const-class v4, Lcom/intsig/office/fc/hslf/record/PersistPtrHolder;

    .line 1133
    .line 1134
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1135
    .line 1136
    .line 1137
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->PersistPtrFullBlock:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1138
    .line 1139
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1140
    .line 1141
    const/16 v3, 0x1772

    .line 1142
    .line 1143
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1144
    .line 1145
    .line 1146
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->PersistPtrIncrementalBlock:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1147
    .line 1148
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1149
    .line 1150
    const/16 v3, 0x2711

    .line 1151
    .line 1152
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1153
    .line 1154
    .line 1155
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->GScalingAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1156
    .line 1157
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1158
    .line 1159
    const/16 v3, 0x2712

    .line 1160
    .line 1161
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1162
    .line 1163
    .line 1164
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->GRColorAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1165
    .line 1166
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1167
    .line 1168
    const/16 v3, 0x2ee0

    .line 1169
    .line 1170
    const-class v4, Lcom/intsig/office/fc/hslf/record/Comment2000;

    .line 1171
    .line 1172
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1173
    .line 1174
    .line 1175
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->Comment2000:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1176
    .line 1177
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1178
    .line 1179
    const/16 v3, 0x2ee1

    .line 1180
    .line 1181
    const-class v4, Lcom/intsig/office/fc/hslf/record/Comment2000Atom;

    .line 1182
    .line 1183
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1184
    .line 1185
    .line 1186
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->Comment2000Atom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1187
    .line 1188
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1189
    .line 1190
    const/16 v3, 0x2ee4

    .line 1191
    .line 1192
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1193
    .line 1194
    .line 1195
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->Comment2000Summary:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1196
    .line 1197
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1198
    .line 1199
    const/16 v3, 0x2ee5

    .line 1200
    .line 1201
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1202
    .line 1203
    .line 1204
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->Comment2000SummaryAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1205
    .line 1206
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1207
    .line 1208
    const/16 v3, 0x1388

    .line 1209
    .line 1210
    const-class v4, Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;

    .line 1211
    .line 1212
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1213
    .line 1214
    .line 1215
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlideProgTagsContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1216
    .line 1217
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1218
    .line 1219
    const/16 v3, 0x138a

    .line 1220
    .line 1221
    const-class v4, Lcom/intsig/office/fc/hslf/record/SlideProgBinaryTagContainer;

    .line 1222
    .line 1223
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1224
    .line 1225
    .line 1226
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlideProgBinaryTagContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1227
    .line 1228
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1229
    .line 1230
    const/16 v3, 0x138b

    .line 1231
    .line 1232
    const-class v4, Lcom/intsig/office/fc/hslf/record/BinaryTagDataBlob;

    .line 1233
    .line 1234
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1235
    .line 1236
    .line 1237
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->BinaryTagDataBlob:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1238
    .line 1239
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1240
    .line 1241
    const/16 v3, 0x3f9

    .line 1242
    .line 1243
    const-class v4, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;

    .line 1244
    .line 1245
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1246
    .line 1247
    .line 1248
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlideShowSlideInfoAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1249
    .line 1250
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1251
    .line 1252
    const/16 v3, 0x2eeb

    .line 1253
    .line 1254
    const-class v4, Lcom/intsig/office/fc/hslf/record/SlideTimeAtom;

    .line 1255
    .line 1256
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1257
    .line 1258
    .line 1259
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlideTimeAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1260
    .line 1261
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1262
    .line 1263
    const v3, 0xf144

    .line 1264
    .line 1265
    .line 1266
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;

    .line 1267
    .line 1268
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1269
    .line 1270
    .line 1271
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeNodeContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1272
    .line 1273
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1274
    .line 1275
    const v3, 0xf127

    .line 1276
    .line 1277
    .line 1278
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeNodeAtom;

    .line 1279
    .line 1280
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1281
    .line 1282
    .line 1283
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeNodeAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1284
    .line 1285
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1286
    .line 1287
    const v3, 0xf13d

    .line 1288
    .line 1289
    .line 1290
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeNodeAttributeContainer;

    .line 1291
    .line 1292
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1293
    .line 1294
    .line 1295
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeNodeAttributeContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1296
    .line 1297
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1298
    .line 1299
    const v3, 0xf125

    .line 1300
    .line 1301
    .line 1302
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeConditionContainer;

    .line 1303
    .line 1304
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1305
    .line 1306
    .line 1307
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeConditionContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1308
    .line 1309
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1310
    .line 1311
    const v3, 0xf128

    .line 1312
    .line 1313
    .line 1314
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeConditionAtom;

    .line 1315
    .line 1316
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1317
    .line 1318
    .line 1319
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeConditionAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1320
    .line 1321
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1322
    .line 1323
    const v3, 0xf142

    .line 1324
    .line 1325
    .line 1326
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeVariant;

    .line 1327
    .line 1328
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1329
    .line 1330
    .line 1331
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeVariant:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1332
    .line 1333
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1334
    .line 1335
    const v3, 0xf131

    .line 1336
    .line 1337
    .line 1338
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeSetBehaviorContainer;

    .line 1339
    .line 1340
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1341
    .line 1342
    .line 1343
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeSetBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1344
    .line 1345
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1346
    .line 1347
    const v3, 0xf12b

    .line 1348
    .line 1349
    .line 1350
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeAnimateBehaviorContainer;

    .line 1351
    .line 1352
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1353
    .line 1354
    .line 1355
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeAnimateBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1356
    .line 1357
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1358
    .line 1359
    const v3, 0xf12a

    .line 1360
    .line 1361
    .line 1362
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeBehaviorContainer;

    .line 1363
    .line 1364
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1365
    .line 1366
    .line 1367
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1368
    .line 1369
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1370
    .line 1371
    const v3, 0xf13c

    .line 1372
    .line 1373
    .line 1374
    const-class v4, Lcom/intsig/office/fc/hslf/record/ClientVisualElementContainer;

    .line 1375
    .line 1376
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1377
    .line 1378
    .line 1379
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->ClientVisualElementContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1380
    .line 1381
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1382
    .line 1383
    const/16 v3, 0x2afb

    .line 1384
    .line 1385
    const-class v4, Lcom/intsig/office/fc/hslf/record/VisualShapeAtom;

    .line 1386
    .line 1387
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1388
    .line 1389
    .line 1390
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->VisualShapeAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1391
    .line 1392
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1393
    .line 1394
    const v3, 0xf145

    .line 1395
    .line 1396
    .line 1397
    const-class v4, Lcom/intsig/office/fc/hslf/record/SlaveContainer;

    .line 1398
    .line 1399
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1400
    .line 1401
    .line 1402
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->SlaveContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1403
    .line 1404
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1405
    .line 1406
    const v3, 0xf135

    .line 1407
    .line 1408
    .line 1409
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeColorBehaviorAtom;

    .line 1410
    .line 1411
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1412
    .line 1413
    .line 1414
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeColorBehaviorAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1415
    .line 1416
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1417
    .line 1418
    const v3, 0xf12c

    .line 1419
    .line 1420
    .line 1421
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeColorBehaviorContainer;

    .line 1422
    .line 1423
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1424
    .line 1425
    .line 1426
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeColorBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1427
    .line 1428
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1429
    .line 1430
    const v3, 0xf132

    .line 1431
    .line 1432
    .line 1433
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeCommandBehaviorContainer;

    .line 1434
    .line 1435
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1436
    .line 1437
    .line 1438
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeCommandBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1439
    .line 1440
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1441
    .line 1442
    const v3, 0xf12d

    .line 1443
    .line 1444
    .line 1445
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeEffectBehaviorContainer;

    .line 1446
    .line 1447
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1448
    .line 1449
    .line 1450
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeEffectBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1451
    .line 1452
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1453
    .line 1454
    const v3, 0xf140

    .line 1455
    .line 1456
    .line 1457
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeIterateDataAtom;

    .line 1458
    .line 1459
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1460
    .line 1461
    .line 1462
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeIterateDataAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1463
    .line 1464
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1465
    .line 1466
    const v3, 0xf12e

    .line 1467
    .line 1468
    .line 1469
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeMotionBehaviorContainer;

    .line 1470
    .line 1471
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1472
    .line 1473
    .line 1474
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeMotionBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1475
    .line 1476
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1477
    .line 1478
    const v3, 0xf12f

    .line 1479
    .line 1480
    .line 1481
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeRotationBehaviorContainer;

    .line 1482
    .line 1483
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1484
    .line 1485
    .line 1486
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeRotationBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1487
    .line 1488
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1489
    .line 1490
    const v3, 0xf130

    .line 1491
    .line 1492
    .line 1493
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeScaleBehaviorContainer;

    .line 1494
    .line 1495
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1496
    .line 1497
    .line 1498
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeScaleBehaviorContainer:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1499
    .line 1500
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1501
    .line 1502
    const v3, 0xf141

    .line 1503
    .line 1504
    .line 1505
    const-class v4, Lcom/intsig/office/fc/hslf/record/TimeSequenceDataAtom;

    .line 1506
    .line 1507
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1508
    .line 1509
    .line 1510
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->TimeSequenceDataAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1511
    .line 1512
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1513
    .line 1514
    const/16 v3, 0x2f14

    .line 1515
    .line 1516
    const-class v4, Lcom/intsig/office/fc/hslf/record/DocumentEncryptionAtom;

    .line 1517
    .line 1518
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1519
    .line 1520
    .line 1521
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->DocumentEncryptionAtom:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1522
    .line 1523
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1524
    .line 1525
    const/16 v3, 0x41c

    .line 1526
    .line 1527
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1528
    .line 1529
    .line 1530
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->OriginalMainMasterId:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1531
    .line 1532
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1533
    .line 1534
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1535
    .line 1536
    .line 1537
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->CompositeMasterId:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1538
    .line 1539
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1540
    .line 1541
    const/16 v3, 0x41e

    .line 1542
    .line 1543
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1544
    .line 1545
    .line 1546
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->RoundTripContentMasterInfo12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1547
    .line 1548
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1549
    .line 1550
    const/16 v3, 0x41f

    .line 1551
    .line 1552
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1553
    .line 1554
    .line 1555
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->RoundTripShapeId12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1556
    .line 1557
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1558
    .line 1559
    const/16 v3, 0x420

    .line 1560
    .line 1561
    const-class v4, Lcom/intsig/office/fc/hslf/record/RoundTripHFPlaceholder12;

    .line 1562
    .line 1563
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1564
    .line 1565
    .line 1566
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->RoundTripHFPlaceholder12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1567
    .line 1568
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1569
    .line 1570
    const/16 v3, 0x422

    .line 1571
    .line 1572
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1573
    .line 1574
    .line 1575
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->RoundTripContentMasterId:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1576
    .line 1577
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1578
    .line 1579
    const/16 v3, 0x423

    .line 1580
    .line 1581
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1582
    .line 1583
    .line 1584
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->RoundTripOArtTextStyles12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1585
    .line 1586
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1587
    .line 1588
    const/16 v3, 0x426

    .line 1589
    .line 1590
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1591
    .line 1592
    .line 1593
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->RoundTripShapeCheckSumForCustomLayouts12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1594
    .line 1595
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1596
    .line 1597
    const/16 v3, 0x427

    .line 1598
    .line 1599
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1600
    .line 1601
    .line 1602
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->RoundTripNotesMasterTextStyles12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1603
    .line 1604
    new-instance v0, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1605
    .line 1606
    const/16 v3, 0x428

    .line 1607
    .line 1608
    invoke-direct {v0, v3, v2}, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    .line 1609
    .line 1610
    .line 1611
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->RoundTripCustomTableStyles12:Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1612
    .line 1613
    new-instance v0, Ljava/util/HashMap;

    .line 1614
    .line 1615
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1616
    .line 1617
    .line 1618
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->typeToName:Ljava/util/HashMap;

    .line 1619
    .line 1620
    new-instance v0, Ljava/util/HashMap;

    .line 1621
    .line 1622
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1623
    .line 1624
    .line 1625
    sput-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->typeToClass:Ljava/util/HashMap;

    .line 1626
    .line 1627
    :try_start_0
    const-class v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;

    .line 1628
    .line 1629
    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    .line 1630
    .line 1631
    .line 1632
    move-result-object v0

    .line 1633
    :goto_0
    array-length v3, v0

    .line 1634
    if-ge v1, v3, :cond_3

    .line 1635
    .line 1636
    aget-object v3, v0, v1

    .line 1637
    .line 1638
    invoke-virtual {v3, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1639
    .line 1640
    .line 1641
    move-result-object v3

    .line 1642
    instance-of v4, v3, Ljava/lang/Integer;

    .line 1643
    .line 1644
    if-eqz v4, :cond_0

    .line 1645
    .line 1646
    sget-object v4, Lcom/intsig/office/fc/hslf/record/RecordTypes;->typeToName:Ljava/util/HashMap;

    .line 1647
    .line 1648
    move-object v5, v3

    .line 1649
    check-cast v5, Ljava/lang/Integer;

    .line 1650
    .line 1651
    aget-object v6, v0, v1

    .line 1652
    .line 1653
    invoke-virtual {v6}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    .line 1654
    .line 1655
    .line 1656
    move-result-object v6

    .line 1657
    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1658
    .line 1659
    .line 1660
    :cond_0
    instance-of v4, v3, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1661
    .line 1662
    if-eqz v4, :cond_2

    .line 1663
    .line 1664
    check-cast v3, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;

    .line 1665
    .line 1666
    iget-object v4, v3, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->handlingClass:Ljava/lang/Class;

    .line 1667
    .line 1668
    iget v3, v3, Lcom/intsig/office/fc/hslf/record/RecordTypes$Type;->typeID:I

    .line 1669
    .line 1670
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 1671
    .line 1672
    .line 1673
    move-result-object v3

    .line 1674
    if-nez v4, :cond_1

    .line 1675
    .line 1676
    const-class v4, Lcom/intsig/office/fc/hslf/record/UnknownRecordPlaceholder;

    .line 1677
    .line 1678
    :cond_1
    sget-object v5, Lcom/intsig/office/fc/hslf/record/RecordTypes;->typeToName:Ljava/util/HashMap;

    .line 1679
    .line 1680
    aget-object v6, v0, v1

    .line 1681
    .line 1682
    invoke-virtual {v6}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    .line 1683
    .line 1684
    .line 1685
    move-result-object v6

    .line 1686
    invoke-virtual {v5, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1687
    .line 1688
    .line 1689
    sget-object v5, Lcom/intsig/office/fc/hslf/record/RecordTypes;->typeToClass:Ljava/util/HashMap;

    .line 1690
    .line 1691
    invoke-virtual {v5, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1692
    .line 1693
    .line 1694
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 1695
    .line 1696
    goto :goto_0

    .line 1697
    :cond_3
    return-void

    .line 1698
    :catch_0
    new-instance v0, Ljava/lang/RuntimeException;

    .line 1699
    .line 1700
    const-string v1, "Failed to initialize records types"

    .line 1701
    .line 1702
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 1703
    .line 1704
    .line 1705
    throw v0
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
    .line 1825
    .line 1826
    .line 1827
    .line 1828
    .line 1829
    .line 1830
    .line 1831
    .line 1832
    .line 1833
    .line 1834
    .line 1835
    .line 1836
    .line 1837
    .line 1838
    .line 1839
    .line 1840
    .line 1841
    .line 1842
    .line 1843
    .line 1844
    .line 1845
    .line 1846
    .line 1847
    .line 1848
    .line 1849
    .line 1850
    .line 1851
    .line 1852
    .line 1853
    .line 1854
    .line 1855
    .line 1856
    .line 1857
    .line 1858
    .line 1859
    .line 1860
    .line 1861
    .line 1862
    .line 1863
    .line 1864
    .line 1865
    .line 1866
    .line 1867
    .line 1868
    .line 1869
    .line 1870
    .line 1871
    .line 1872
    .line 1873
    .line 1874
    .line 1875
    .line 1876
    .line 1877
    .line 1878
    .line 1879
    .line 1880
    .line 1881
    .line 1882
    .line 1883
    .line 1884
    .line 1885
    .line 1886
    .line 1887
    .line 1888
    .line 1889
    .line 1890
    .line 1891
    .line 1892
    .line 1893
    .line 1894
    .line 1895
    .line 1896
    .line 1897
    .line 1898
    .line 1899
    .line 1900
    .line 1901
    .line 1902
    .line 1903
    .line 1904
    .line 1905
    .line 1906
    .line 1907
    .line 1908
    .line 1909
    .line 1910
    .line 1911
    .line 1912
    .line 1913
    .line 1914
    .line 1915
    .line 1916
    .line 1917
    .line 1918
    .line 1919
    .line 1920
    .line 1921
    .line 1922
    .line 1923
    .line 1924
    .line 1925
    .line 1926
    .line 1927
    .line 1928
    .line 1929
    .line 1930
    .line 1931
    .line 1932
    .line 1933
    .line 1934
    .line 1935
    .line 1936
    .line 1937
    .line 1938
    .line 1939
    .line 1940
    .line 1941
    .line 1942
    .line 1943
    .line 1944
    .line 1945
    .line 1946
    .line 1947
    .line 1948
    .line 1949
    .line 1950
    .line 1951
    .line 1952
    .line 1953
    .line 1954
    .line 1955
    .line 1956
    .line 1957
    .line 1958
    .line 1959
    .line 1960
    .line 1961
    .line 1962
    .line 1963
    .line 1964
    .line 1965
    .line 1966
    .line 1967
    .line 1968
    .line 1969
    .line 1970
    .line 1971
    .line 1972
    .line 1973
    .line 1974
    .line 1975
    .line 1976
    .line 1977
    .line 1978
    .line 1979
    .line 1980
    .line 1981
    .line 1982
    .line 1983
    .line 1984
    .line 1985
    .line 1986
    .line 1987
    .line 1988
    .line 1989
    .line 1990
    .line 1991
    .line 1992
    .line 1993
    .line 1994
    .line 1995
    .line 1996
    .line 1997
    .line 1998
    .line 1999
    .line 2000
    .line 2001
    .line 2002
    .line 2003
    .line 2004
    .line 2005
    .line 2006
    .line 2007
    .line 2008
    .line 2009
    .line 2010
    .line 2011
    .line 2012
    .line 2013
    .line 2014
    .line 2015
    .line 2016
    .line 2017
    .line 2018
    .line 2019
    .line 2020
    .line 2021
    .line 2022
    .line 2023
    .line 2024
    .line 2025
    .line 2026
    .line 2027
    .line 2028
    .line 2029
    .line 2030
    .line 2031
    .line 2032
    .line 2033
    .line 2034
    .line 2035
    .line 2036
    .line 2037
    .line 2038
    .line 2039
    .line 2040
    .line 2041
    .line 2042
    .line 2043
    .line 2044
    .line 2045
    .line 2046
    .line 2047
    .line 2048
    .line 2049
    .line 2050
    .line 2051
    .line 2052
    .line 2053
    .line 2054
    .line 2055
    .line 2056
    .line 2057
    .line 2058
    .line 2059
    .line 2060
    .line 2061
    .line 2062
    .line 2063
    .line 2064
    .line 2065
    .line 2066
    .line 2067
    .line 2068
    .line 2069
    .line 2070
    .line 2071
    .line 2072
    .line 2073
    .line 2074
    .line 2075
    .line 2076
    .line 2077
    .line 2078
    .line 2079
    .line 2080
    .line 2081
    .line 2082
    .line 2083
    .line 2084
    .line 2085
    .line 2086
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static recordHandlingClass(I)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/lang/Class<",
            "+",
            "Lcom/intsig/office/fc/hslf/record/Record;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->typeToClass:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    check-cast p0, Ljava/lang/Class;

    .line 12
    .line 13
    return-object p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static recordName(I)Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hslf/record/RecordTypes;->typeToName:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Ljava/lang/String;

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v1, "Unknown"

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    :cond_0
    return-object v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.class public Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;
.super Lcom/intsig/office/fc/hslf/record/PositionDependentRecordAtom;
.source "SlideShowSlideInfoAtom.java"


# static fields
.field private static _type:J = 0x3f9L


# instance fields
.field private _header:[B

.field private effectDirection:B

.field private effectType:B

.field private fAutoAdvance:Z

.field private fCursorVisible:Z

.field private fHidden:Z

.field private fLoopSound:Z

.field private fManualAdvance:Z

.field private fSound:Z

.field private fStopSound:Z

.field private reserved1:Z

.field private reserved2:Z

.field private reserved3:Z

.field private reserved4:Z

.field private reserved5:Z

.field private reserved6:Z

.field private reserved7:B

.field private slideTime:I

.field private soundIdRef:I

.field private speed:B

.field private unused:[B


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected constructor <init>([BII)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hslf/record/PositionDependentRecordAtom;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 p3, 0x8

    .line 5
    .line 6
    new-array v0, p3, [B

    .line 7
    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->_header:[B

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 12
    .line 13
    .line 14
    add-int/lit8 p3, p2, 0x8

    .line 15
    .line 16
    invoke-static {p1, p3}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 17
    .line 18
    .line 19
    move-result p3

    .line 20
    iput p3, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->slideTime:I

    .line 21
    .line 22
    add-int/lit8 p3, p2, 0xc

    .line 23
    .line 24
    invoke-static {p1, p3}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 25
    .line 26
    .line 27
    move-result p3

    .line 28
    iput p3, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->soundIdRef:I

    .line 29
    .line 30
    add-int/lit8 p3, p2, 0x10

    .line 31
    .line 32
    aget-byte p3, p1, p3

    .line 33
    .line 34
    iput-byte p3, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 35
    .line 36
    add-int/lit8 p3, p2, 0x11

    .line 37
    .line 38
    aget-byte p3, p1, p3

    .line 39
    .line 40
    iput-byte p3, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectType:B

    .line 41
    .line 42
    add-int/lit8 p2, p2, 0x14

    .line 43
    .line 44
    aget-byte p1, p1, p2

    .line 45
    .line 46
    iput-byte p1, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->speed:B

    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->_header:[B

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordType()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->_type:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isValidateTransition()Z
    .locals 6

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectType:B

    .line 2
    .line 3
    const/4 v1, 0x4

    .line 4
    const/4 v2, 0x3

    .line 5
    const/4 v3, 0x7

    .line 6
    const/4 v4, 0x0

    .line 7
    const/4 v5, 0x1

    .line 8
    packed-switch v0, :pswitch_data_0

    .line 9
    .line 10
    .line 11
    :pswitch_0
    return v4

    .line 12
    :pswitch_1
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 13
    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    const/4 v4, 0x1

    .line 17
    :cond_0
    return v4

    .line 18
    :pswitch_2
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 19
    .line 20
    if-lt v0, v5, :cond_1

    .line 21
    .line 22
    if-le v0, v1, :cond_2

    .line 23
    .line 24
    :cond_1
    const/16 v1, 0x8

    .line 25
    .line 26
    if-ne v0, v1, :cond_3

    .line 27
    .line 28
    :cond_2
    const/4 v4, 0x1

    .line 29
    :cond_3
    return v4

    .line 30
    :pswitch_3
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 31
    .line 32
    if-nez v0, :cond_4

    .line 33
    .line 34
    const/4 v4, 0x1

    .line 35
    :cond_4
    return v4

    .line 36
    :pswitch_4
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 37
    .line 38
    if-ltz v0, :cond_5

    .line 39
    .line 40
    if-gt v0, v5, :cond_5

    .line 41
    .line 42
    const/4 v4, 0x1

    .line 43
    :cond_5
    return v4

    .line 44
    :pswitch_5
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 45
    .line 46
    if-ltz v0, :cond_6

    .line 47
    .line 48
    if-gt v0, v2, :cond_6

    .line 49
    .line 50
    const/4 v4, 0x1

    .line 51
    :cond_6
    return v4

    .line 52
    :pswitch_6
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 53
    .line 54
    if-nez v0, :cond_7

    .line 55
    .line 56
    const/4 v4, 0x1

    .line 57
    :cond_7
    return v4

    .line 58
    :pswitch_7
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 59
    .line 60
    if-ltz v0, :cond_8

    .line 61
    .line 62
    if-gt v0, v2, :cond_8

    .line 63
    .line 64
    const/4 v4, 0x1

    .line 65
    :cond_8
    return v4

    .line 66
    :pswitch_8
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 67
    .line 68
    if-ltz v0, :cond_9

    .line 69
    .line 70
    if-gt v0, v5, :cond_9

    .line 71
    .line 72
    const/4 v4, 0x1

    .line 73
    :cond_9
    return v4

    .line 74
    :pswitch_9
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 75
    .line 76
    if-ltz v0, :cond_a

    .line 77
    .line 78
    if-gt v0, v2, :cond_a

    .line 79
    .line 80
    const/4 v4, 0x1

    .line 81
    :cond_a
    return v4

    .line 82
    :pswitch_a
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 83
    .line 84
    if-lt v0, v1, :cond_b

    .line 85
    .line 86
    if-gt v0, v3, :cond_b

    .line 87
    .line 88
    const/4 v4, 0x1

    .line 89
    :cond_b
    return v4

    .line 90
    :pswitch_b
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 91
    .line 92
    if-ltz v0, :cond_c

    .line 93
    .line 94
    if-gt v0, v5, :cond_c

    .line 95
    .line 96
    const/4 v4, 0x1

    .line 97
    :cond_c
    return v4

    .line 98
    :pswitch_c
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 99
    .line 100
    if-ltz v0, :cond_d

    .line 101
    .line 102
    if-gt v0, v3, :cond_d

    .line 103
    .line 104
    const/4 v4, 0x1

    .line 105
    :cond_d
    return v4

    .line 106
    :pswitch_d
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 107
    .line 108
    if-nez v0, :cond_e

    .line 109
    .line 110
    const/4 v4, 0x1

    .line 111
    :cond_e
    return v4

    .line 112
    :pswitch_e
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 113
    .line 114
    if-nez v0, :cond_f

    .line 115
    .line 116
    const/4 v4, 0x1

    .line 117
    :cond_f
    return v4

    .line 118
    :pswitch_f
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 119
    .line 120
    if-ltz v0, :cond_10

    .line 121
    .line 122
    if-gt v0, v3, :cond_10

    .line 123
    .line 124
    const/4 v4, 0x1

    .line 125
    :cond_10
    return v4

    .line 126
    :pswitch_10
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 127
    .line 128
    if-ltz v0, :cond_11

    .line 129
    .line 130
    if-gt v0, v5, :cond_11

    .line 131
    .line 132
    const/4 v4, 0x1

    .line 133
    :cond_11
    return v4

    .line 134
    :pswitch_11
    return v5

    .line 135
    :pswitch_12
    iget-byte v0, p0, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->effectDirection:B

    .line 136
    .line 137
    if-ltz v0, :cond_12

    .line 138
    .line 139
    if-gt v0, v5, :cond_12

    .line 140
    .line 141
    const/4 v4, 0x1

    .line 142
    :cond_12
    return v4

    .line 143
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public updateOtherRecordReferences(Ljava/util/Hashtable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public final Lcom/intsig/office/fc/hslf/model/SlideMaster;
.super Lcom/intsig/office/fc/hslf/model/MasterSheet;
.source "SlideMaster.java"


# instance fields
.field private _runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

.field private _txmaster:[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hslf/record/MainMaster;I)V
    .locals 1

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hslf/model/MasterSheet;-><init>(Lcom/intsig/office/fc/hslf/record/SheetContainer;I)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getPPDrawing()Lcom/intsig/office/fc/hslf/record/PPDrawing;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    invoke-static {p1}, Lcom/intsig/office/fc/hslf/model/Sheet;->findTextRuns(Lcom/intsig/office/fc/hslf/record/PPDrawing;)[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 13
    .line 14
    const/4 p1, 0x0

    .line 15
    :goto_0
    iget-object p2, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 16
    .line 17
    array-length v0, p2

    .line 18
    if-ge p1, v0, :cond_0

    .line 19
    .line 20
    aget-object p2, p2, p1

    .line 21
    .line 22
    invoke-virtual {p2, p0}, Lcom/intsig/office/fc/hslf/model/TextRun;->setSheet(Lcom/intsig/office/fc/hslf/model/Sheet;)V

    .line 23
    .line 24
    .line 25
    add-int/lit8 p1, p1, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public dispose()V
    .locals 6

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->dispose()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    const/4 v2, 0x0

    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    array-length v3, v0

    .line 11
    const/4 v4, 0x0

    .line 12
    :goto_0
    if-ge v4, v3, :cond_0

    .line 13
    .line 14
    aget-object v5, v0, v4

    .line 15
    .line 16
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/model/TextRun;->dispose()V

    .line 17
    .line 18
    .line 19
    add-int/lit8 v4, v4, 0x1

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 23
    .line 24
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_txmaster:[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 25
    .line 26
    if-eqz v0, :cond_4

    .line 27
    .line 28
    array-length v3, v0

    .line 29
    :goto_1
    if-ge v2, v3, :cond_3

    .line 30
    .line 31
    aget-object v4, v0, v2

    .line 32
    .line 33
    if-eqz v4, :cond_2

    .line 34
    .line 35
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;->dispose()V

    .line 36
    .line 37
    .line 38
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_3
    iput-object v1, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_txmaster:[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 42
    .line 43
    :cond_4
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getMasterSheet()Lcom/intsig/office/fc/hslf/model/MasterSheet;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStyleAttribute(IILjava/lang/String;Z)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;
    .locals 7

    .line 1
    const/4 v0, 0x0

    .line 2
    move v1, p2

    .line 3
    move-object v2, v0

    .line 4
    :goto_0
    if-ltz v1, :cond_3

    .line 5
    .line 6
    if-eqz p4, :cond_0

    .line 7
    .line 8
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_txmaster:[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 9
    .line 10
    aget-object v3, v3, p1

    .line 11
    .line 12
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;->getCharacterStyles()[Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    goto :goto_1

    .line 17
    :cond_0
    iget-object v3, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_txmaster:[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 18
    .line 19
    aget-object v3, v3, p1

    .line 20
    .line 21
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;->getParagraphStyles()[Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    :goto_1
    array-length v4, v3

    .line 26
    if-ge v1, v4, :cond_1

    .line 27
    .line 28
    aget-object v2, v3, v1

    .line 29
    .line 30
    invoke-virtual {v2, p3}, Lcom/intsig/office/fc/hslf/model/textproperties/TextPropCollection;->findByName(Ljava/lang/String;)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    :cond_1
    if-eqz v2, :cond_2

    .line 35
    .line 36
    goto :goto_2

    .line 37
    :cond_2
    add-int/lit8 v1, v1, -0x1

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_3
    :goto_2
    if-nez v2, :cond_7

    .line 41
    .line 42
    const/4 v1, 0x0

    .line 43
    const/4 v2, 0x1

    .line 44
    const/16 v3, 0x8

    .line 45
    .line 46
    const/4 v4, 0x7

    .line 47
    const/4 v5, 0x6

    .line 48
    const/4 v6, 0x5

    .line 49
    if-eqz p4, :cond_4

    .line 50
    .line 51
    if-eq p1, v6, :cond_5

    .line 52
    .line 53
    if-eq p1, v5, :cond_6

    .line 54
    .line 55
    if-eq p1, v4, :cond_5

    .line 56
    .line 57
    if-eq p1, v3, :cond_5

    .line 58
    .line 59
    return-object v0

    .line 60
    :cond_4
    if-eq p1, v6, :cond_5

    .line 61
    .line 62
    if-eq p1, v5, :cond_6

    .line 63
    .line 64
    if-eq p1, v4, :cond_5

    .line 65
    .line 66
    if-eq p1, v3, :cond_5

    .line 67
    .line 68
    return-object v0

    .line 69
    :cond_5
    const/4 v1, 0x1

    .line 70
    :cond_6
    invoke-virtual {p0, v1, p2, p3, p4}, Lcom/intsig/office/fc/hslf/model/SlideMaster;->getStyleAttribute(IILjava/lang/String;Z)Lcom/intsig/office/fc/hslf/model/textproperties/TextProp;

    .line 71
    .line 72
    .line 73
    move-result-object v2

    .line 74
    :cond_7
    return-object v2
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public getTextRuns()[Lcom/intsig/office/fc/hslf/model/TextRun;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTxMasterStyleAtoms()[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_txmaster:[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onAddTextShape(Lcom/intsig/office/fc/hslf/model/TextShape;)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    const/4 v2, 0x1

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    new-array v0, v2, [Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 12
    .line 13
    aput-object p1, v0, v1

    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    array-length v3, v0

    .line 19
    add-int/2addr v3, v2

    .line 20
    new-array v4, v3, [Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 21
    .line 22
    array-length v5, v0

    .line 23
    invoke-static {v0, v1, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 24
    .line 25
    .line 26
    sub-int/2addr v3, v2

    .line 27
    aput-object p1, v4, v3

    .line 28
    .line 29
    iput-object v4, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_runs:[Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 30
    .line 31
    :goto_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setSlideShow(Lcom/intsig/office/fc/hslf/usermodel/SlideShow;)V
    .locals 4

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hslf/model/Sheet;->setSlideShow(Lcom/intsig/office/fc/hslf/usermodel/SlideShow;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_txmaster:[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 5
    .line 6
    if-nez p1, :cond_1

    .line 7
    .line 8
    const/16 p1, 0x9

    .line 9
    .line 10
    new-array p1, p1, [Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 11
    .line 12
    iput-object p1, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_txmaster:[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSheetContainer()Lcom/intsig/office/fc/hslf/record/SheetContainer;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    check-cast p1, Lcom/intsig/office/fc/hslf/record/MainMaster;

    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/record/MainMaster;->getTxMasterStyleAtoms()[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    const/4 v0, 0x0

    .line 25
    :goto_0
    array-length v1, p1

    .line 26
    if-ge v0, v1, :cond_0

    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_txmaster:[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 29
    .line 30
    aget-object v2, p1, v0

    .line 31
    .line 32
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;->getTextType()I

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    aget-object v3, p1, v0

    .line 37
    .line 38
    aput-object v3, v1, v2

    .line 39
    .line 40
    add-int/lit8 v0, v0, 0x1

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getSlideShow()Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getDocumentRecord()Lcom/intsig/office/fc/hslf/record/Document;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/record/Document;->getEnvironment()Lcom/intsig/office/fc/hslf/record/Environment;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/record/Environment;->getTxMasterStyleAtom()Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    iget-object v0, p0, Lcom/intsig/office/fc/hslf/model/SlideMaster;->_txmaster:[Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;

    .line 60
    .line 61
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/record/TxMasterStyleAtom;->getTextType()I

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    aput-object p1, v0, v1

    .line 66
    .line 67
    :cond_1
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

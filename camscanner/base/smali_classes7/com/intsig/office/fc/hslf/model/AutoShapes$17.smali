.class Lcom/intsig/office/fc/hslf/model/AutoShapes$17;
.super Ljava/lang/Object;
.source "AutoShapes.java"

# interfaces
.implements Lcom/intsig/office/fc/hslf/model/ShapeOutline;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hslf/model/AutoShapes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public getOutline(Lcom/intsig/office/fc/hslf/model/Shape;)Lcom/intsig/office/java/awt/Shape;
    .locals 19

    .line 1
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/16 v1, 0x147

    .line 6
    .line 7
    const/16 v2, 0x1518

    .line 8
    .line 9
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/EscherContainerRecord;SI)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    new-instance v1, Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 14
    .line 15
    invoke-direct {v1}, Lcom/intsig/office/java/awt/geom/GeneralPath;-><init>()V

    .line 16
    .line 17
    .line 18
    new-instance v10, Lcom/intsig/office/java/awt/geom/Arc2D$Float;

    .line 19
    .line 20
    const/4 v3, 0x0

    .line 21
    const/4 v4, 0x0

    .line 22
    const v5, 0x46a8c000    # 21600.0f

    .line 23
    .line 24
    .line 25
    int-to-float v15, v0

    .line 26
    const/4 v7, 0x0

    .line 27
    const/high16 v8, 0x43340000    # 180.0f

    .line 28
    .line 29
    const/4 v9, 0x0

    .line 30
    move-object v2, v10

    .line 31
    move v6, v15

    .line 32
    invoke-direct/range {v2 .. v9}, Lcom/intsig/office/java/awt/geom/Arc2D$Float;-><init>(FFFFFFI)V

    .line 33
    .line 34
    .line 35
    const/4 v2, 0x0

    .line 36
    invoke-virtual {v1, v10, v2}, Lcom/intsig/office/java/awt/geom/Path2D;->append(Lcom/intsig/office/java/awt/Shape;Z)V

    .line 37
    .line 38
    .line 39
    div-int/lit8 v3, v0, 0x2

    .line 40
    .line 41
    int-to-float v4, v3

    .line 42
    const/4 v5, 0x0

    .line 43
    invoke-virtual {v1, v5, v4}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->moveTo(FF)V

    .line 44
    .line 45
    .line 46
    rsub-int v3, v3, 0x5460

    .line 47
    .line 48
    int-to-float v3, v3

    .line 49
    invoke-virtual {v1, v5, v3}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->lineTo(FF)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/Path2D;->closePath()V

    .line 53
    .line 54
    .line 55
    new-instance v6, Lcom/intsig/office/java/awt/geom/Arc2D$Float;

    .line 56
    .line 57
    const/4 v12, 0x0

    .line 58
    rsub-int v0, v0, 0x5460

    .line 59
    .line 60
    int-to-float v13, v0

    .line 61
    const v14, 0x46a8c000    # 21600.0f

    .line 62
    .line 63
    .line 64
    const/high16 v16, 0x43340000    # 180.0f

    .line 65
    .line 66
    const/high16 v17, 0x43340000    # 180.0f

    .line 67
    .line 68
    const/16 v18, 0x0

    .line 69
    .line 70
    move-object v11, v6

    .line 71
    move v0, v15

    .line 72
    invoke-direct/range {v11 .. v18}, Lcom/intsig/office/java/awt/geom/Arc2D$Float;-><init>(FFFFFFI)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {v1, v6, v2}, Lcom/intsig/office/java/awt/geom/Path2D;->append(Lcom/intsig/office/java/awt/Shape;Z)V

    .line 76
    .line 77
    .line 78
    const v6, 0x46a8c000    # 21600.0f

    .line 79
    .line 80
    .line 81
    invoke-virtual {v1, v6, v3}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->moveTo(FF)V

    .line 82
    .line 83
    .line 84
    invoke-virtual {v1, v6, v4}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->lineTo(FF)V

    .line 85
    .line 86
    .line 87
    new-instance v3, Lcom/intsig/office/java/awt/geom/Arc2D$Float;

    .line 88
    .line 89
    const/4 v13, 0x0

    .line 90
    move-object v11, v3

    .line 91
    invoke-direct/range {v11 .. v18}, Lcom/intsig/office/java/awt/geom/Arc2D$Float;-><init>(FFFFFFI)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {v1, v3, v2}, Lcom/intsig/office/java/awt/geom/Path2D;->append(Lcom/intsig/office/java/awt/Shape;Z)V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v1, v5, v4}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->moveTo(FF)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/Path2D;->closePath()V

    .line 101
    .line 102
    .line 103
    return-object v1
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

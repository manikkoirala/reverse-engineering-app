.class public Lcom/intsig/office/fc/codec/Base32InputStream;
.super Lcom/intsig/office/fc/codec/BaseNCodecInputStream;
.source "Base32InputStream.java"


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/codec/Base32InputStream;-><init>(Ljava/io/InputStream;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Z)V
    .locals 2

    .line 2
    new-instance v0, Lcom/intsig/office/fc/codec/Base32;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/intsig/office/fc/codec/Base32;-><init>(Z)V

    invoke-direct {p0, p1, v0, p2}, Lcom/intsig/office/fc/codec/BaseNCodecInputStream;-><init>(Ljava/io/InputStream;Lcom/intsig/office/fc/codec/BaseNCodec;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;ZI[B)V
    .locals 1

    .line 3
    new-instance v0, Lcom/intsig/office/fc/codec/Base32;

    invoke-direct {v0, p3, p4}, Lcom/intsig/office/fc/codec/Base32;-><init>(I[B)V

    invoke-direct {p0, p1, v0, p2}, Lcom/intsig/office/fc/codec/BaseNCodecInputStream;-><init>(Ljava/io/InputStream;Lcom/intsig/office/fc/codec/BaseNCodec;Z)V

    return-void
.end method

.class public Lcom/intsig/office/fc/codec/Base32;
.super Lcom/intsig/office/fc/codec/BaseNCodec;
.source "Base32.java"


# static fields
.field private static final BITS_PER_ENCODED_BYTE:I = 0x5

.field private static final BYTES_PER_ENCODED_BLOCK:I = 0x8

.field private static final BYTES_PER_UNENCODED_BLOCK:I = 0x5

.field private static final CHUNK_SEPARATOR:[B

.field private static final DECODE_TABLE:[B

.field private static final ENCODE_TABLE:[B

.field private static final HEX_DECODE_TABLE:[B

.field private static final HEX_ENCODE_TABLE:[B

.field private static final MASK_5BITS:I = 0x1f


# instance fields
.field private bitWorkArea:J

.field private final decodeSize:I

.field private final decodeTable:[B

.field private final encodeSize:I

.field private final encodeTable:[B

.field private final lineSeparator:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [B

    .line 3
    .line 4
    fill-array-data v0, :array_0

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/office/fc/codec/Base32;->CHUNK_SEPARATOR:[B

    .line 8
    .line 9
    const/16 v0, 0x5b

    .line 10
    .line 11
    new-array v0, v0, [B

    .line 12
    .line 13
    fill-array-data v0, :array_1

    .line 14
    .line 15
    .line 16
    sput-object v0, Lcom/intsig/office/fc/codec/Base32;->DECODE_TABLE:[B

    .line 17
    .line 18
    const/16 v0, 0x20

    .line 19
    .line 20
    new-array v1, v0, [B

    .line 21
    .line 22
    fill-array-data v1, :array_2

    .line 23
    .line 24
    .line 25
    sput-object v1, Lcom/intsig/office/fc/codec/Base32;->ENCODE_TABLE:[B

    .line 26
    .line 27
    const/16 v1, 0x58

    .line 28
    .line 29
    new-array v1, v1, [B

    .line 30
    .line 31
    fill-array-data v1, :array_3

    .line 32
    .line 33
    .line 34
    sput-object v1, Lcom/intsig/office/fc/codec/Base32;->HEX_DECODE_TABLE:[B

    .line 35
    .line 36
    new-array v0, v0, [B

    .line 37
    .line 38
    fill-array-data v0, :array_4

    .line 39
    .line 40
    .line 41
    sput-object v0, Lcom/intsig/office/fc/codec/Base32;->HEX_ENCODE_TABLE:[B

    .line 42
    .line 43
    return-void

    .line 44
    nop

    .line 45
    :array_0
    .array-data 1
        0xdt
        0xat
    .end array-data

    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    nop

    .line 51
    :array_1
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0x3ft
        -0x1t
        -0x1t
        0x1at
        0x1bt
        0x1ct
        0x1dt
        0x1et
        0x1ft
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0x0t
        0x1t
        0x2t
        0x3t
        0x4t
        0x5t
        0x6t
        0x7t
        0x8t
        0x9t
        0xat
        0xbt
        0xct
        0xdt
        0xet
        0xft
        0x10t
        0x11t
        0x12t
        0x13t
        0x14t
        0x15t
        0x16t
        0x17t
        0x18t
        0x19t
    .end array-data

    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    :array_2
    .array-data 1
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
        0x47t
        0x48t
        0x49t
        0x4at
        0x4bt
        0x4ct
        0x4dt
        0x4et
        0x4ft
        0x50t
        0x51t
        0x52t
        0x53t
        0x54t
        0x55t
        0x56t
        0x57t
        0x58t
        0x59t
        0x5at
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
    .end array-data

    :array_3
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0x3ft
        0x0t
        0x1t
        0x2t
        0x3t
        0x4t
        0x5t
        0x6t
        0x7t
        0x8t
        0x9t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0xat
        0xbt
        0xct
        0xdt
        0xet
        0xft
        0x10t
        0x11t
        0x12t
        0x13t
        0x14t
        0x15t
        0x16t
        0x17t
        0x18t
        0x19t
        0x1at
        0x1bt
        0x1ct
        0x1dt
        0x1et
        0x1ft
        0x20t
    .end array-data

    :array_4
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
        0x47t
        0x48t
        0x49t
        0x4at
        0x4bt
        0x4ct
        0x4dt
        0x4et
        0x4ft
        0x50t
        0x51t
        0x52t
        0x53t
        0x54t
        0x55t
        0x56t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/codec/Base32;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .line 3
    sget-object v0, Lcom/intsig/office/fc/codec/Base32;->CHUNK_SEPARATOR:[B

    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/codec/Base32;-><init>(I[B)V

    return-void
.end method

.method public constructor <init>(I[B)V
    .locals 1

    const/4 v0, 0x0

    .line 4
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/fc/codec/Base32;-><init>(I[BZ)V

    return-void
.end method

.method public constructor <init>(I[BZ)V
    .locals 4

    const/4 v0, 0x0

    if-nez p2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    .line 5
    :cond_0
    array-length v1, p2

    :goto_0
    const/4 v2, 0x5

    const/16 v3, 0x8

    .line 6
    invoke-direct {p0, v2, v3, p1, v1}, Lcom/intsig/office/fc/codec/BaseNCodec;-><init>(IIII)V

    if-eqz p3, :cond_1

    .line 7
    sget-object p3, Lcom/intsig/office/fc/codec/Base32;->HEX_ENCODE_TABLE:[B

    iput-object p3, p0, Lcom/intsig/office/fc/codec/Base32;->encodeTable:[B

    .line 8
    sget-object p3, Lcom/intsig/office/fc/codec/Base32;->HEX_DECODE_TABLE:[B

    iput-object p3, p0, Lcom/intsig/office/fc/codec/Base32;->decodeTable:[B

    goto :goto_1

    .line 9
    :cond_1
    sget-object p3, Lcom/intsig/office/fc/codec/Base32;->ENCODE_TABLE:[B

    iput-object p3, p0, Lcom/intsig/office/fc/codec/Base32;->encodeTable:[B

    .line 10
    sget-object p3, Lcom/intsig/office/fc/codec/Base32;->DECODE_TABLE:[B

    iput-object p3, p0, Lcom/intsig/office/fc/codec/Base32;->decodeTable:[B

    :goto_1
    if-lez p1, :cond_4

    if-eqz p2, :cond_3

    .line 11
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/codec/BaseNCodec;->containsAlphabetOrPad([B)Z

    move-result p1

    if-nez p1, :cond_2

    .line 12
    array-length p1, p2

    add-int/2addr p1, v3

    iput p1, p0, Lcom/intsig/office/fc/codec/Base32;->encodeSize:I

    .line 13
    array-length p1, p2

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/intsig/office/fc/codec/Base32;->lineSeparator:[B

    .line 14
    array-length p3, p2

    invoke-static {p2, v0, p1, v0, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2

    .line 15
    :cond_2
    invoke-static {p2}, Lcom/intsig/office/fc/codec/StringUtils;->newStringUtf8([B)Ljava/lang/String;

    move-result-object p1

    .line 16
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "lineSeparator must not contain Base32 characters: ["

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 17
    :cond_3
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "lineLength "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " > 0, but lineSeparator is null"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 18
    :cond_4
    iput v3, p0, Lcom/intsig/office/fc/codec/Base32;->encodeSize:I

    const/4 p1, 0x0

    .line 19
    iput-object p1, p0, Lcom/intsig/office/fc/codec/Base32;->lineSeparator:[B

    .line 20
    :goto_2
    iget p1, p0, Lcom/intsig/office/fc/codec/Base32;->encodeSize:I

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lcom/intsig/office/fc/codec/Base32;->decodeSize:I

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 2
    invoke-direct {p0, v0, v1, p1}, Lcom/intsig/office/fc/codec/Base32;-><init>(I[BZ)V

    return-void
.end method


# virtual methods
.method decode([BII)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move/from16 v1, p3

    .line 4
    .line 5
    iget-boolean v2, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->eof:Z

    .line 6
    .line 7
    if-eqz v2, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    const/4 v2, 0x1

    .line 11
    if-gez v1, :cond_1

    .line 12
    .line 13
    iput-boolean v2, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->eof:Z

    .line 14
    .line 15
    :cond_1
    const/4 v3, 0x0

    .line 16
    move/from16 v3, p2

    .line 17
    .line 18
    const/4 v4, 0x0

    .line 19
    :goto_0
    const/16 v5, 0x18

    .line 20
    .line 21
    const/16 v7, 0x8

    .line 22
    .line 23
    const-wide/16 v8, 0xff

    .line 24
    .line 25
    if-ge v4, v1, :cond_4

    .line 26
    .line 27
    add-int/lit8 v10, v3, 0x1

    .line 28
    .line 29
    aget-byte v3, p1, v3

    .line 30
    .line 31
    const/16 v11, 0x3d

    .line 32
    .line 33
    if-ne v3, v11, :cond_2

    .line 34
    .line 35
    iput-boolean v2, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->eof:Z

    .line 36
    .line 37
    goto :goto_1

    .line 38
    :cond_2
    iget v11, v0, Lcom/intsig/office/fc/codec/Base32;->decodeSize:I

    .line 39
    .line 40
    invoke-virtual {v0, v11}, Lcom/intsig/office/fc/codec/BaseNCodec;->ensureBufferSize(I)V

    .line 41
    .line 42
    .line 43
    if-ltz v3, :cond_3

    .line 44
    .line 45
    iget-object v11, v0, Lcom/intsig/office/fc/codec/Base32;->decodeTable:[B

    .line 46
    .line 47
    array-length v12, v11

    .line 48
    if-ge v3, v12, :cond_3

    .line 49
    .line 50
    aget-byte v3, v11, v3

    .line 51
    .line 52
    if-ltz v3, :cond_3

    .line 53
    .line 54
    iget v11, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->modulus:I

    .line 55
    .line 56
    add-int/2addr v11, v2

    .line 57
    rem-int/2addr v11, v7

    .line 58
    iput v11, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->modulus:I

    .line 59
    .line 60
    iget-wide v12, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 61
    .line 62
    const/4 v14, 0x5

    .line 63
    shl-long/2addr v12, v14

    .line 64
    int-to-long v14, v3

    .line 65
    add-long/2addr v12, v14

    .line 66
    iput-wide v12, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 67
    .line 68
    if-nez v11, :cond_3

    .line 69
    .line 70
    iget-object v3, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->buffer:[B

    .line 71
    .line 72
    iget v11, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 73
    .line 74
    add-int/lit8 v14, v11, 0x1

    .line 75
    .line 76
    const/16 v15, 0x20

    .line 77
    .line 78
    shr-long v15, v12, v15

    .line 79
    .line 80
    and-long v6, v15, v8

    .line 81
    .line 82
    long-to-int v7, v6

    .line 83
    int-to-byte v6, v7

    .line 84
    aput-byte v6, v3, v11

    .line 85
    .line 86
    add-int/lit8 v6, v14, 0x1

    .line 87
    .line 88
    shr-long v15, v12, v5

    .line 89
    .line 90
    move-object v11, v3

    .line 91
    and-long v2, v15, v8

    .line 92
    .line 93
    long-to-int v3, v2

    .line 94
    int-to-byte v2, v3

    .line 95
    aput-byte v2, v11, v14

    .line 96
    .line 97
    add-int/lit8 v2, v6, 0x1

    .line 98
    .line 99
    const/16 v3, 0x10

    .line 100
    .line 101
    shr-long v14, v12, v3

    .line 102
    .line 103
    and-long/2addr v14, v8

    .line 104
    long-to-int v3, v14

    .line 105
    int-to-byte v3, v3

    .line 106
    aput-byte v3, v11, v6

    .line 107
    .line 108
    add-int/lit8 v3, v2, 0x1

    .line 109
    .line 110
    const/16 v5, 0x8

    .line 111
    .line 112
    shr-long v5, v12, v5

    .line 113
    .line 114
    and-long/2addr v5, v8

    .line 115
    long-to-int v6, v5

    .line 116
    int-to-byte v5, v6

    .line 117
    aput-byte v5, v11, v2

    .line 118
    .line 119
    add-int/lit8 v2, v3, 0x1

    .line 120
    .line 121
    iput v2, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 122
    .line 123
    and-long v5, v12, v8

    .line 124
    .line 125
    long-to-int v2, v5

    .line 126
    int-to-byte v2, v2

    .line 127
    aput-byte v2, v11, v3

    .line 128
    .line 129
    :cond_3
    add-int/lit8 v4, v4, 0x1

    .line 130
    .line 131
    move v3, v10

    .line 132
    const/4 v2, 0x1

    .line 133
    goto :goto_0

    .line 134
    :cond_4
    :goto_1
    iget-boolean v1, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->eof:Z

    .line 135
    .line 136
    if-eqz v1, :cond_5

    .line 137
    .line 138
    iget v1, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->modulus:I

    .line 139
    .line 140
    const/4 v2, 0x2

    .line 141
    if-lt v1, v2, :cond_5

    .line 142
    .line 143
    iget v1, v0, Lcom/intsig/office/fc/codec/Base32;->decodeSize:I

    .line 144
    .line 145
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/codec/BaseNCodec;->ensureBufferSize(I)V

    .line 146
    .line 147
    .line 148
    iget v1, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->modulus:I

    .line 149
    .line 150
    packed-switch v1, :pswitch_data_0

    .line 151
    .line 152
    .line 153
    goto/16 :goto_2

    .line 154
    .line 155
    :pswitch_0
    iget-wide v1, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 156
    .line 157
    const/4 v3, 0x3

    .line 158
    shr-long/2addr v1, v3

    .line 159
    iput-wide v1, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 160
    .line 161
    iget-object v3, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->buffer:[B

    .line 162
    .line 163
    iget v4, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 164
    .line 165
    add-int/lit8 v6, v4, 0x1

    .line 166
    .line 167
    shr-long v10, v1, v5

    .line 168
    .line 169
    and-long/2addr v10, v8

    .line 170
    long-to-int v5, v10

    .line 171
    int-to-byte v5, v5

    .line 172
    aput-byte v5, v3, v4

    .line 173
    .line 174
    add-int/lit8 v4, v6, 0x1

    .line 175
    .line 176
    const/16 v5, 0x10

    .line 177
    .line 178
    shr-long v10, v1, v5

    .line 179
    .line 180
    and-long/2addr v10, v8

    .line 181
    long-to-int v5, v10

    .line 182
    int-to-byte v5, v5

    .line 183
    aput-byte v5, v3, v6

    .line 184
    .line 185
    add-int/lit8 v5, v4, 0x1

    .line 186
    .line 187
    const/16 v6, 0x8

    .line 188
    .line 189
    shr-long v6, v1, v6

    .line 190
    .line 191
    and-long/2addr v6, v8

    .line 192
    long-to-int v7, v6

    .line 193
    int-to-byte v6, v7

    .line 194
    aput-byte v6, v3, v4

    .line 195
    .line 196
    add-int/lit8 v4, v5, 0x1

    .line 197
    .line 198
    iput v4, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 199
    .line 200
    and-long/2addr v1, v8

    .line 201
    long-to-int v2, v1

    .line 202
    int-to-byte v1, v2

    .line 203
    aput-byte v1, v3, v5

    .line 204
    .line 205
    goto/16 :goto_2

    .line 206
    .line 207
    :pswitch_1
    iget-wide v1, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 208
    .line 209
    const/4 v3, 0x6

    .line 210
    shr-long/2addr v1, v3

    .line 211
    iput-wide v1, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 212
    .line 213
    iget-object v3, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->buffer:[B

    .line 214
    .line 215
    iget v4, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 216
    .line 217
    add-int/lit8 v5, v4, 0x1

    .line 218
    .line 219
    const/16 v6, 0x10

    .line 220
    .line 221
    shr-long v6, v1, v6

    .line 222
    .line 223
    and-long/2addr v6, v8

    .line 224
    long-to-int v7, v6

    .line 225
    int-to-byte v6, v7

    .line 226
    aput-byte v6, v3, v4

    .line 227
    .line 228
    add-int/lit8 v4, v5, 0x1

    .line 229
    .line 230
    const/16 v6, 0x8

    .line 231
    .line 232
    shr-long v6, v1, v6

    .line 233
    .line 234
    and-long/2addr v6, v8

    .line 235
    long-to-int v7, v6

    .line 236
    int-to-byte v6, v7

    .line 237
    aput-byte v6, v3, v5

    .line 238
    .line 239
    add-int/lit8 v5, v4, 0x1

    .line 240
    .line 241
    iput v5, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 242
    .line 243
    and-long/2addr v1, v8

    .line 244
    long-to-int v2, v1

    .line 245
    int-to-byte v1, v2

    .line 246
    aput-byte v1, v3, v4

    .line 247
    .line 248
    goto :goto_2

    .line 249
    :pswitch_2
    iget-wide v1, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 250
    .line 251
    const/4 v3, 0x1

    .line 252
    shr-long/2addr v1, v3

    .line 253
    iput-wide v1, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 254
    .line 255
    iget-object v3, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->buffer:[B

    .line 256
    .line 257
    iget v4, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 258
    .line 259
    add-int/lit8 v5, v4, 0x1

    .line 260
    .line 261
    const/16 v6, 0x10

    .line 262
    .line 263
    shr-long v6, v1, v6

    .line 264
    .line 265
    and-long/2addr v6, v8

    .line 266
    long-to-int v7, v6

    .line 267
    int-to-byte v6, v7

    .line 268
    aput-byte v6, v3, v4

    .line 269
    .line 270
    add-int/lit8 v4, v5, 0x1

    .line 271
    .line 272
    const/16 v6, 0x8

    .line 273
    .line 274
    shr-long v6, v1, v6

    .line 275
    .line 276
    and-long/2addr v6, v8

    .line 277
    long-to-int v7, v6

    .line 278
    int-to-byte v6, v7

    .line 279
    aput-byte v6, v3, v5

    .line 280
    .line 281
    add-int/lit8 v5, v4, 0x1

    .line 282
    .line 283
    iput v5, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 284
    .line 285
    and-long/2addr v1, v8

    .line 286
    long-to-int v2, v1

    .line 287
    int-to-byte v1, v2

    .line 288
    aput-byte v1, v3, v4

    .line 289
    .line 290
    goto :goto_2

    .line 291
    :pswitch_3
    iget-wide v1, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 292
    .line 293
    const/4 v3, 0x4

    .line 294
    shr-long/2addr v1, v3

    .line 295
    iput-wide v1, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 296
    .line 297
    iget-object v3, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->buffer:[B

    .line 298
    .line 299
    iget v4, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 300
    .line 301
    add-int/lit8 v5, v4, 0x1

    .line 302
    .line 303
    const/16 v6, 0x8

    .line 304
    .line 305
    shr-long v6, v1, v6

    .line 306
    .line 307
    and-long/2addr v6, v8

    .line 308
    long-to-int v7, v6

    .line 309
    int-to-byte v6, v7

    .line 310
    aput-byte v6, v3, v4

    .line 311
    .line 312
    add-int/lit8 v4, v5, 0x1

    .line 313
    .line 314
    iput v4, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 315
    .line 316
    and-long/2addr v1, v8

    .line 317
    long-to-int v2, v1

    .line 318
    int-to-byte v1, v2

    .line 319
    aput-byte v1, v3, v5

    .line 320
    .line 321
    goto :goto_2

    .line 322
    :pswitch_4
    iget-object v1, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->buffer:[B

    .line 323
    .line 324
    iget v2, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 325
    .line 326
    add-int/lit8 v3, v2, 0x1

    .line 327
    .line 328
    iput v3, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 329
    .line 330
    iget-wide v3, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 331
    .line 332
    const/4 v5, 0x7

    .line 333
    shr-long/2addr v3, v5

    .line 334
    and-long/2addr v3, v8

    .line 335
    long-to-int v4, v3

    .line 336
    int-to-byte v3, v4

    .line 337
    aput-byte v3, v1, v2

    .line 338
    .line 339
    goto :goto_2

    .line 340
    :pswitch_5
    iget-object v1, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->buffer:[B

    .line 341
    .line 342
    iget v3, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 343
    .line 344
    add-int/lit8 v4, v3, 0x1

    .line 345
    .line 346
    iput v4, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 347
    .line 348
    iget-wide v4, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 349
    .line 350
    shr-long/2addr v4, v2

    .line 351
    and-long/2addr v4, v8

    .line 352
    long-to-int v2, v4

    .line 353
    int-to-byte v2, v2

    .line 354
    aput-byte v2, v1, v3

    .line 355
    .line 356
    :cond_5
    :goto_2
    return-void

    .line 357
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method encode([BII)V
    .locals 16

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move/from16 v1, p3

    .line 4
    .line 5
    iget-boolean v2, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->eof:Z

    .line 6
    .line 7
    if-eqz v2, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    const/4 v2, 0x0

    .line 11
    const/4 v3, 0x1

    .line 12
    if-gez v1, :cond_6

    .line 13
    .line 14
    iput-boolean v3, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->eof:Z

    .line 15
    .line 16
    iget v1, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->modulus:I

    .line 17
    .line 18
    if-nez v1, :cond_1

    .line 19
    .line 20
    iget v1, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->lineLength:I

    .line 21
    .line 22
    if-nez v1, :cond_1

    .line 23
    .line 24
    return-void

    .line 25
    :cond_1
    iget v1, v0, Lcom/intsig/office/fc/codec/Base32;->encodeSize:I

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/codec/BaseNCodec;->ensureBufferSize(I)V

    .line 28
    .line 29
    .line 30
    iget v1, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 31
    .line 32
    iget v4, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->modulus:I

    .line 33
    .line 34
    const/4 v5, 0x3

    .line 35
    const/4 v6, 0x2

    .line 36
    const/16 v7, 0x3d

    .line 37
    .line 38
    if-eq v4, v3, :cond_5

    .line 39
    .line 40
    const/4 v8, 0x4

    .line 41
    if-eq v4, v6, :cond_4

    .line 42
    .line 43
    if-eq v4, v5, :cond_3

    .line 44
    .line 45
    if-eq v4, v8, :cond_2

    .line 46
    .line 47
    goto/16 :goto_0

    .line 48
    .line 49
    :cond_2
    iget-object v3, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->buffer:[B

    .line 50
    .line 51
    add-int/lit8 v4, v1, 0x1

    .line 52
    .line 53
    iget-object v8, v0, Lcom/intsig/office/fc/codec/Base32;->encodeTable:[B

    .line 54
    .line 55
    iget-wide v9, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 56
    .line 57
    const/16 v11, 0x1b

    .line 58
    .line 59
    shr-long v11, v9, v11

    .line 60
    .line 61
    long-to-int v12, v11

    .line 62
    and-int/lit8 v11, v12, 0x1f

    .line 63
    .line 64
    aget-byte v11, v8, v11

    .line 65
    .line 66
    aput-byte v11, v3, v1

    .line 67
    .line 68
    add-int/lit8 v11, v4, 0x1

    .line 69
    .line 70
    const/16 v12, 0x16

    .line 71
    .line 72
    shr-long v12, v9, v12

    .line 73
    .line 74
    long-to-int v13, v12

    .line 75
    and-int/lit8 v12, v13, 0x1f

    .line 76
    .line 77
    aget-byte v12, v8, v12

    .line 78
    .line 79
    aput-byte v12, v3, v4

    .line 80
    .line 81
    add-int/lit8 v4, v11, 0x1

    .line 82
    .line 83
    const/16 v12, 0x11

    .line 84
    .line 85
    shr-long v12, v9, v12

    .line 86
    .line 87
    long-to-int v13, v12

    .line 88
    and-int/lit8 v12, v13, 0x1f

    .line 89
    .line 90
    aget-byte v12, v8, v12

    .line 91
    .line 92
    aput-byte v12, v3, v11

    .line 93
    .line 94
    add-int/lit8 v11, v4, 0x1

    .line 95
    .line 96
    const/16 v12, 0xc

    .line 97
    .line 98
    shr-long v12, v9, v12

    .line 99
    .line 100
    long-to-int v13, v12

    .line 101
    and-int/lit8 v12, v13, 0x1f

    .line 102
    .line 103
    aget-byte v12, v8, v12

    .line 104
    .line 105
    aput-byte v12, v3, v4

    .line 106
    .line 107
    add-int/lit8 v4, v11, 0x1

    .line 108
    .line 109
    const/4 v12, 0x7

    .line 110
    shr-long v12, v9, v12

    .line 111
    .line 112
    long-to-int v13, v12

    .line 113
    and-int/lit8 v12, v13, 0x1f

    .line 114
    .line 115
    aget-byte v12, v8, v12

    .line 116
    .line 117
    aput-byte v12, v3, v11

    .line 118
    .line 119
    add-int/lit8 v11, v4, 0x1

    .line 120
    .line 121
    shr-long v12, v9, v6

    .line 122
    .line 123
    long-to-int v6, v12

    .line 124
    and-int/lit8 v6, v6, 0x1f

    .line 125
    .line 126
    aget-byte v6, v8, v6

    .line 127
    .line 128
    aput-byte v6, v3, v4

    .line 129
    .line 130
    add-int/lit8 v4, v11, 0x1

    .line 131
    .line 132
    shl-long v5, v9, v5

    .line 133
    .line 134
    long-to-int v6, v5

    .line 135
    and-int/lit8 v5, v6, 0x1f

    .line 136
    .line 137
    aget-byte v5, v8, v5

    .line 138
    .line 139
    aput-byte v5, v3, v11

    .line 140
    .line 141
    add-int/lit8 v5, v4, 0x1

    .line 142
    .line 143
    iput v5, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 144
    .line 145
    aput-byte v7, v3, v4

    .line 146
    .line 147
    goto/16 :goto_0

    .line 148
    .line 149
    :cond_3
    iget-object v4, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->buffer:[B

    .line 150
    .line 151
    add-int/lit8 v5, v1, 0x1

    .line 152
    .line 153
    iget-object v6, v0, Lcom/intsig/office/fc/codec/Base32;->encodeTable:[B

    .line 154
    .line 155
    iget-wide v9, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 156
    .line 157
    const/16 v11, 0x13

    .line 158
    .line 159
    shr-long v11, v9, v11

    .line 160
    .line 161
    long-to-int v12, v11

    .line 162
    and-int/lit8 v11, v12, 0x1f

    .line 163
    .line 164
    aget-byte v11, v6, v11

    .line 165
    .line 166
    aput-byte v11, v4, v1

    .line 167
    .line 168
    add-int/lit8 v11, v5, 0x1

    .line 169
    .line 170
    const/16 v12, 0xe

    .line 171
    .line 172
    shr-long v12, v9, v12

    .line 173
    .line 174
    long-to-int v13, v12

    .line 175
    and-int/lit8 v12, v13, 0x1f

    .line 176
    .line 177
    aget-byte v12, v6, v12

    .line 178
    .line 179
    aput-byte v12, v4, v5

    .line 180
    .line 181
    add-int/lit8 v5, v11, 0x1

    .line 182
    .line 183
    const/16 v12, 0x9

    .line 184
    .line 185
    shr-long v12, v9, v12

    .line 186
    .line 187
    long-to-int v13, v12

    .line 188
    and-int/lit8 v12, v13, 0x1f

    .line 189
    .line 190
    aget-byte v12, v6, v12

    .line 191
    .line 192
    aput-byte v12, v4, v11

    .line 193
    .line 194
    add-int/lit8 v11, v5, 0x1

    .line 195
    .line 196
    shr-long v12, v9, v8

    .line 197
    .line 198
    long-to-int v8, v12

    .line 199
    and-int/lit8 v8, v8, 0x1f

    .line 200
    .line 201
    aget-byte v8, v6, v8

    .line 202
    .line 203
    aput-byte v8, v4, v5

    .line 204
    .line 205
    add-int/lit8 v5, v11, 0x1

    .line 206
    .line 207
    shl-long v8, v9, v3

    .line 208
    .line 209
    long-to-int v3, v8

    .line 210
    and-int/lit8 v3, v3, 0x1f

    .line 211
    .line 212
    aget-byte v3, v6, v3

    .line 213
    .line 214
    aput-byte v3, v4, v11

    .line 215
    .line 216
    add-int/lit8 v3, v5, 0x1

    .line 217
    .line 218
    aput-byte v7, v4, v5

    .line 219
    .line 220
    add-int/lit8 v5, v3, 0x1

    .line 221
    .line 222
    aput-byte v7, v4, v3

    .line 223
    .line 224
    add-int/lit8 v3, v5, 0x1

    .line 225
    .line 226
    iput v3, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 227
    .line 228
    aput-byte v7, v4, v5

    .line 229
    .line 230
    goto/16 :goto_0

    .line 231
    .line 232
    :cond_4
    iget-object v4, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->buffer:[B

    .line 233
    .line 234
    add-int/lit8 v5, v1, 0x1

    .line 235
    .line 236
    iget-object v6, v0, Lcom/intsig/office/fc/codec/Base32;->encodeTable:[B

    .line 237
    .line 238
    iget-wide v9, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 239
    .line 240
    const/16 v11, 0xb

    .line 241
    .line 242
    shr-long v11, v9, v11

    .line 243
    .line 244
    long-to-int v12, v11

    .line 245
    and-int/lit8 v11, v12, 0x1f

    .line 246
    .line 247
    aget-byte v11, v6, v11

    .line 248
    .line 249
    aput-byte v11, v4, v1

    .line 250
    .line 251
    add-int/lit8 v11, v5, 0x1

    .line 252
    .line 253
    const/4 v12, 0x6

    .line 254
    shr-long v12, v9, v12

    .line 255
    .line 256
    long-to-int v13, v12

    .line 257
    and-int/lit8 v12, v13, 0x1f

    .line 258
    .line 259
    aget-byte v12, v6, v12

    .line 260
    .line 261
    aput-byte v12, v4, v5

    .line 262
    .line 263
    add-int/lit8 v5, v11, 0x1

    .line 264
    .line 265
    shr-long v12, v9, v3

    .line 266
    .line 267
    long-to-int v3, v12

    .line 268
    and-int/lit8 v3, v3, 0x1f

    .line 269
    .line 270
    aget-byte v3, v6, v3

    .line 271
    .line 272
    aput-byte v3, v4, v11

    .line 273
    .line 274
    add-int/lit8 v3, v5, 0x1

    .line 275
    .line 276
    shl-long v8, v9, v8

    .line 277
    .line 278
    long-to-int v9, v8

    .line 279
    and-int/lit8 v8, v9, 0x1f

    .line 280
    .line 281
    aget-byte v6, v6, v8

    .line 282
    .line 283
    aput-byte v6, v4, v5

    .line 284
    .line 285
    add-int/lit8 v5, v3, 0x1

    .line 286
    .line 287
    aput-byte v7, v4, v3

    .line 288
    .line 289
    add-int/lit8 v3, v5, 0x1

    .line 290
    .line 291
    aput-byte v7, v4, v5

    .line 292
    .line 293
    add-int/lit8 v5, v3, 0x1

    .line 294
    .line 295
    aput-byte v7, v4, v3

    .line 296
    .line 297
    add-int/lit8 v3, v5, 0x1

    .line 298
    .line 299
    iput v3, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 300
    .line 301
    aput-byte v7, v4, v5

    .line 302
    .line 303
    goto :goto_0

    .line 304
    :cond_5
    iget-object v3, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->buffer:[B

    .line 305
    .line 306
    add-int/lit8 v4, v1, 0x1

    .line 307
    .line 308
    iget-object v8, v0, Lcom/intsig/office/fc/codec/Base32;->encodeTable:[B

    .line 309
    .line 310
    iget-wide v9, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 311
    .line 312
    shr-long v11, v9, v5

    .line 313
    .line 314
    long-to-int v5, v11

    .line 315
    and-int/lit8 v5, v5, 0x1f

    .line 316
    .line 317
    aget-byte v5, v8, v5

    .line 318
    .line 319
    aput-byte v5, v3, v1

    .line 320
    .line 321
    add-int/lit8 v5, v4, 0x1

    .line 322
    .line 323
    shl-long/2addr v9, v6

    .line 324
    long-to-int v6, v9

    .line 325
    and-int/lit8 v6, v6, 0x1f

    .line 326
    .line 327
    aget-byte v6, v8, v6

    .line 328
    .line 329
    aput-byte v6, v3, v4

    .line 330
    .line 331
    add-int/lit8 v4, v5, 0x1

    .line 332
    .line 333
    aput-byte v7, v3, v5

    .line 334
    .line 335
    add-int/lit8 v5, v4, 0x1

    .line 336
    .line 337
    aput-byte v7, v3, v4

    .line 338
    .line 339
    add-int/lit8 v4, v5, 0x1

    .line 340
    .line 341
    aput-byte v7, v3, v5

    .line 342
    .line 343
    add-int/lit8 v5, v4, 0x1

    .line 344
    .line 345
    aput-byte v7, v3, v4

    .line 346
    .line 347
    add-int/lit8 v4, v5, 0x1

    .line 348
    .line 349
    aput-byte v7, v3, v5

    .line 350
    .line 351
    add-int/lit8 v5, v4, 0x1

    .line 352
    .line 353
    iput v5, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 354
    .line 355
    aput-byte v7, v3, v4

    .line 356
    .line 357
    :goto_0
    iget v3, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->currentLinePos:I

    .line 358
    .line 359
    iget v4, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 360
    .line 361
    sub-int v1, v4, v1

    .line 362
    .line 363
    add-int/2addr v3, v1

    .line 364
    iput v3, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->currentLinePos:I

    .line 365
    .line 366
    iget v1, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->lineLength:I

    .line 367
    .line 368
    if-lez v1, :cond_9

    .line 369
    .line 370
    if-lez v3, :cond_9

    .line 371
    .line 372
    iget-object v1, v0, Lcom/intsig/office/fc/codec/Base32;->lineSeparator:[B

    .line 373
    .line 374
    iget-object v3, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->buffer:[B

    .line 375
    .line 376
    array-length v5, v1

    .line 377
    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 378
    .line 379
    .line 380
    iget v1, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 381
    .line 382
    iget-object v2, v0, Lcom/intsig/office/fc/codec/Base32;->lineSeparator:[B

    .line 383
    .line 384
    array-length v2, v2

    .line 385
    add-int/2addr v1, v2

    .line 386
    iput v1, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 387
    .line 388
    goto/16 :goto_2

    .line 389
    .line 390
    :cond_6
    move/from16 v4, p2

    .line 391
    .line 392
    const/4 v5, 0x0

    .line 393
    :goto_1
    if-ge v5, v1, :cond_9

    .line 394
    .line 395
    iget v6, v0, Lcom/intsig/office/fc/codec/Base32;->encodeSize:I

    .line 396
    .line 397
    invoke-virtual {v0, v6}, Lcom/intsig/office/fc/codec/BaseNCodec;->ensureBufferSize(I)V

    .line 398
    .line 399
    .line 400
    iget v6, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->modulus:I

    .line 401
    .line 402
    add-int/2addr v6, v3

    .line 403
    const/4 v7, 0x5

    .line 404
    rem-int/2addr v6, v7

    .line 405
    iput v6, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->modulus:I

    .line 406
    .line 407
    add-int/lit8 v8, v4, 0x1

    .line 408
    .line 409
    aget-byte v4, p1, v4

    .line 410
    .line 411
    if-gez v4, :cond_7

    .line 412
    .line 413
    add-int/lit16 v4, v4, 0x100

    .line 414
    .line 415
    :cond_7
    iget-wide v9, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 416
    .line 417
    const/16 v11, 0x8

    .line 418
    .line 419
    shl-long/2addr v9, v11

    .line 420
    int-to-long v12, v4

    .line 421
    add-long/2addr v9, v12

    .line 422
    iput-wide v9, v0, Lcom/intsig/office/fc/codec/Base32;->bitWorkArea:J

    .line 423
    .line 424
    if-nez v6, :cond_8

    .line 425
    .line 426
    iget-object v4, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->buffer:[B

    .line 427
    .line 428
    iget v6, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 429
    .line 430
    add-int/lit8 v12, v6, 0x1

    .line 431
    .line 432
    iget-object v13, v0, Lcom/intsig/office/fc/codec/Base32;->encodeTable:[B

    .line 433
    .line 434
    const/16 v14, 0x23

    .line 435
    .line 436
    shr-long v14, v9, v14

    .line 437
    .line 438
    long-to-int v15, v14

    .line 439
    and-int/lit8 v14, v15, 0x1f

    .line 440
    .line 441
    aget-byte v14, v13, v14

    .line 442
    .line 443
    aput-byte v14, v4, v6

    .line 444
    .line 445
    add-int/lit8 v6, v12, 0x1

    .line 446
    .line 447
    const/16 v14, 0x1e

    .line 448
    .line 449
    shr-long v14, v9, v14

    .line 450
    .line 451
    long-to-int v15, v14

    .line 452
    and-int/lit8 v14, v15, 0x1f

    .line 453
    .line 454
    aget-byte v14, v13, v14

    .line 455
    .line 456
    aput-byte v14, v4, v12

    .line 457
    .line 458
    add-int/lit8 v12, v6, 0x1

    .line 459
    .line 460
    const/16 v14, 0x19

    .line 461
    .line 462
    shr-long v14, v9, v14

    .line 463
    .line 464
    long-to-int v15, v14

    .line 465
    and-int/lit8 v14, v15, 0x1f

    .line 466
    .line 467
    aget-byte v14, v13, v14

    .line 468
    .line 469
    aput-byte v14, v4, v6

    .line 470
    .line 471
    add-int/lit8 v6, v12, 0x1

    .line 472
    .line 473
    const/16 v14, 0x14

    .line 474
    .line 475
    shr-long v14, v9, v14

    .line 476
    .line 477
    long-to-int v15, v14

    .line 478
    and-int/lit8 v14, v15, 0x1f

    .line 479
    .line 480
    aget-byte v14, v13, v14

    .line 481
    .line 482
    aput-byte v14, v4, v12

    .line 483
    .line 484
    add-int/lit8 v12, v6, 0x1

    .line 485
    .line 486
    const/16 v14, 0xf

    .line 487
    .line 488
    shr-long v14, v9, v14

    .line 489
    .line 490
    long-to-int v15, v14

    .line 491
    and-int/lit8 v14, v15, 0x1f

    .line 492
    .line 493
    aget-byte v14, v13, v14

    .line 494
    .line 495
    aput-byte v14, v4, v6

    .line 496
    .line 497
    add-int/lit8 v6, v12, 0x1

    .line 498
    .line 499
    const/16 v14, 0xa

    .line 500
    .line 501
    shr-long v14, v9, v14

    .line 502
    .line 503
    long-to-int v15, v14

    .line 504
    and-int/lit8 v14, v15, 0x1f

    .line 505
    .line 506
    aget-byte v14, v13, v14

    .line 507
    .line 508
    aput-byte v14, v4, v12

    .line 509
    .line 510
    add-int/lit8 v12, v6, 0x1

    .line 511
    .line 512
    shr-long v14, v9, v7

    .line 513
    .line 514
    long-to-int v7, v14

    .line 515
    and-int/lit8 v7, v7, 0x1f

    .line 516
    .line 517
    aget-byte v7, v13, v7

    .line 518
    .line 519
    aput-byte v7, v4, v6

    .line 520
    .line 521
    add-int/lit8 v6, v12, 0x1

    .line 522
    .line 523
    iput v6, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 524
    .line 525
    long-to-int v7, v9

    .line 526
    and-int/lit8 v7, v7, 0x1f

    .line 527
    .line 528
    aget-byte v7, v13, v7

    .line 529
    .line 530
    aput-byte v7, v4, v12

    .line 531
    .line 532
    iget v7, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->currentLinePos:I

    .line 533
    .line 534
    add-int/2addr v7, v11

    .line 535
    iput v7, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->currentLinePos:I

    .line 536
    .line 537
    iget v9, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->lineLength:I

    .line 538
    .line 539
    if-lez v9, :cond_8

    .line 540
    .line 541
    if-gt v9, v7, :cond_8

    .line 542
    .line 543
    iget-object v7, v0, Lcom/intsig/office/fc/codec/Base32;->lineSeparator:[B

    .line 544
    .line 545
    array-length v9, v7

    .line 546
    invoke-static {v7, v2, v4, v6, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 547
    .line 548
    .line 549
    iget v4, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 550
    .line 551
    iget-object v6, v0, Lcom/intsig/office/fc/codec/Base32;->lineSeparator:[B

    .line 552
    .line 553
    array-length v6, v6

    .line 554
    add-int/2addr v4, v6

    .line 555
    iput v4, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->pos:I

    .line 556
    .line 557
    iput v2, v0, Lcom/intsig/office/fc/codec/BaseNCodec;->currentLinePos:I

    .line 558
    .line 559
    :cond_8
    add-int/lit8 v5, v5, 0x1

    .line 560
    .line 561
    move v4, v8

    .line 562
    goto/16 :goto_1

    .line 563
    .line 564
    :cond_9
    :goto_2
    return-void
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method public isInAlphabet(B)Z
    .locals 2

    .line 1
    if-ltz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/codec/Base32;->decodeTable:[B

    .line 4
    .line 5
    array-length v1, v0

    .line 6
    if-ge p1, v1, :cond_0

    .line 7
    .line 8
    aget-byte p1, v0, p1

    .line 9
    .line 10
    const/4 v0, -0x1

    .line 11
    if-eq p1, v0, :cond_0

    .line 12
    .line 13
    const/4 p1, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    :goto_0
    return p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

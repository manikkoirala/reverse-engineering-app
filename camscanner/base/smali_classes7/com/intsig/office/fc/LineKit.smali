.class public Lcom/intsig/office/fc/LineKit;
.super Ljava/lang/Object;
.source "LineKit.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static createChartLine(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;)Lcom/intsig/office/common/borders/Line;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/system/IControl;",
            "Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;",
            "Lcom/intsig/office/fc/openxml4j/opc/PackagePart;",
            "Lcom/intsig/office/fc/dom4j/Element;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/intsig/office/common/borders/Line;"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x1

    .line 3
    if-eqz p3, :cond_3

    .line 4
    .line 5
    const-string v2, "w"

    .line 6
    .line 7
    invoke-interface {p3, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v3

    .line 11
    if-eqz v3, :cond_0

    .line 12
    .line 13
    invoke-interface {p3, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    int-to-float v2, v2

    .line 22
    const/high16 v3, 0x42c00000    # 96.0f

    .line 23
    .line 24
    mul-float v2, v2, v3

    .line 25
    .line 26
    const v3, 0x495f3e00    # 914400.0f

    .line 27
    .line 28
    .line 29
    div-float/2addr v2, v3

    .line 30
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    goto :goto_0

    .line 35
    :cond_0
    const/4 v2, 0x1

    .line 36
    :goto_0
    const-string v3, "prstDash"

    .line 37
    .line 38
    invoke-interface {p3, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 39
    .line 40
    .line 41
    move-result-object v3

    .line 42
    if-eqz v3, :cond_1

    .line 43
    .line 44
    const-string v4, "val"

    .line 45
    .line 46
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    const-string v4, "solid"

    .line 51
    .line 52
    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    if-nez v3, :cond_1

    .line 57
    .line 58
    const/4 v0, 0x1

    .line 59
    :cond_1
    const-string v1, "noFill"

    .line 60
    .line 61
    invoke-interface {p3, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    if-nez v1, :cond_2

    .line 66
    .line 67
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/office/common/autoshape/AutoShapeDataKit;->processBackground(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 68
    .line 69
    .line 70
    move-result-object p0

    .line 71
    if-eqz p0, :cond_2

    .line 72
    .line 73
    new-instance p1, Lcom/intsig/office/common/borders/Line;

    .line 74
    .line 75
    invoke-direct {p1}, Lcom/intsig/office/common/borders/Line;-><init>()V

    .line 76
    .line 77
    .line 78
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/borders/Line;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {p1, v2}, Lcom/intsig/office/common/borders/Border;->setLineWidth(I)V

    .line 82
    .line 83
    .line 84
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/borders/Line;->setDash(Z)V

    .line 85
    .line 86
    .line 87
    return-object p1

    .line 88
    :cond_2
    const/4 p0, 0x0

    .line 89
    return-object p0

    .line 90
    :cond_3
    new-instance p0, Lcom/intsig/office/common/borders/Line;

    .line 91
    .line 92
    invoke-direct {p0}, Lcom/intsig/office/common/borders/Line;-><init>()V

    .line 93
    .line 94
    .line 95
    new-instance p1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 96
    .line 97
    invoke-direct {p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 98
    .line 99
    .line 100
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 101
    .line 102
    .line 103
    const p2, -0x8b8b8c

    .line 104
    .line 105
    .line 106
    invoke-virtual {p1, p2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {p0, p1}, Lcom/intsig/office/common/borders/Line;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 110
    .line 111
    .line 112
    invoke-virtual {p0, v1}, Lcom/intsig/office/common/borders/Border;->setLineWidth(I)V

    .line 113
    .line 114
    .line 115
    return-object p0
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method public static createLine(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;)Lcom/intsig/office/common/borders/Line;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/system/IControl;",
            "Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;",
            "Lcom/intsig/office/fc/openxml4j/opc/PackagePart;",
            "Lcom/intsig/office/fc/dom4j/Element;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/intsig/office/common/borders/Line;"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz p3, :cond_3

    const-string v3, "w"

    .line 11
    invoke-interface {p3, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 12
    invoke-interface {p3, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x42c00000    # 96.0f

    mul-float v3, v3, v4

    const v4, 0x495f3e00    # 914400.0f

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    goto :goto_0

    :cond_0
    const/4 v3, 0x1

    :goto_0
    const-string v4, "prstDash"

    .line 13
    invoke-interface {p3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v4

    if-eqz v4, :cond_1

    const-string v5, "val"

    .line 14
    invoke-interface {v4, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "solid"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    const-string v1, "noFill"

    .line 15
    invoke-interface {p3, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    if-nez v1, :cond_2

    .line 16
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/office/common/autoshape/AutoShapeDataKit;->processBackground(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object p0

    move v1, v0

    goto :goto_2

    :cond_2
    move v1, v0

    move-object p0, v2

    :goto_2
    move v0, v3

    goto :goto_3

    :cond_3
    move-object p0, v2

    :goto_3
    if-eqz p0, :cond_4

    .line 17
    new-instance p1, Lcom/intsig/office/common/borders/Line;

    invoke-direct {p1}, Lcom/intsig/office/common/borders/Line;-><init>()V

    .line 18
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/borders/Line;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 19
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/borders/Border;->setLineWidth(I)V

    .line 20
    invoke-virtual {p1, v1}, Lcom/intsig/office/common/borders/Line;->setDash(Z)V

    return-object p1

    :cond_4
    return-object v2
.end method

.method public static createLine(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/borders/Line;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-eqz p4, :cond_2

    const-string v0, "noFill"

    .line 1
    invoke-interface {p4, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "w"

    .line 2
    invoke-interface {p4, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    .line 3
    invoke-interface {p4, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c00000    # 96.0f

    mul-float v0, v0, v1

    const v1, 0x495f3e00    # 914400.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "prstDash"

    .line 4
    invoke-interface {p4, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v3, "val"

    .line 5
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "solid"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    .line 6
    :goto_1
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->instance()Lcom/intsig/office/fc/ppt/reader/BackgroundReader;

    move-result-object v3

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    invoke-virtual/range {v3 .. v8}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->processBackground(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 7
    new-instance p1, Lcom/intsig/office/common/borders/Line;

    invoke-direct {p1}, Lcom/intsig/office/common/borders/Line;-><init>()V

    .line 8
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/borders/Line;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 9
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/borders/Border;->setLineWidth(I)V

    .line 10
    invoke-virtual {p1, v2}, Lcom/intsig/office/common/borders/Line;->setDash(Z)V

    return-object p1

    :cond_2
    const/4 p0, 0x0

    return-object p0
.end method

.method public static createShapeLine(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;)Lcom/intsig/office/common/borders/Line;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/system/IControl;",
            "Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;",
            "Lcom/intsig/office/fc/openxml4j/opc/PackagePart;",
            "Lcom/intsig/office/fc/dom4j/Element;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/intsig/office/common/borders/Line;"
        }
    .end annotation

    const-string v0, "spPr"

    .line 22
    invoke-interface {p3, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    const-string v1, "ln"

    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    const-string v1, "style"

    .line 23
    invoke-interface {p3, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p3

    const-string v1, "lnRef"

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    if-eqz v0, :cond_4

    const-string v5, "w"

    .line 24
    invoke-interface {v0, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 25
    invoke-interface {v0, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x42c00000    # 96.0f

    mul-float v5, v5, v6

    const v6, 0x495f3e00    # 914400.0f

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    goto :goto_0

    :cond_0
    const/4 v5, 0x1

    :goto_0
    const-string v6, "prstDash"

    .line 26
    invoke-interface {v0, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v6

    if-eqz v6, :cond_1

    const-string v7, "val"

    .line 27
    invoke-interface {v6, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "solid"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    const-string v6, "noFill"

    .line 28
    invoke-interface {v0, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v6

    if-nez v6, :cond_2

    .line 29
    invoke-static {p0, p1, p2, v0, p4}, Lcom/intsig/office/common/autoshape/AutoShapeDataKit;->processBackground(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object p0

    if-nez p0, :cond_3

    if-eqz p3, :cond_3

    .line 30
    invoke-interface {p3, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 31
    new-instance p0, Lcom/intsig/office/common/bg/BackgroundAndFill;

    invoke-direct {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 32
    invoke-virtual {p0, v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 33
    invoke-interface {p3, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    invoke-static {p4, p1}, Lcom/intsig/office/common/autoshape/AutoShapeDataKit;->getColor(Ljava/util/Map;Lcom/intsig/office/fc/dom4j/Element;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    goto :goto_2

    :cond_2
    move-object p0, v4

    :cond_3
    :goto_2
    move v3, v2

    move v2, v5

    goto :goto_3

    :cond_4
    if-eqz p3, :cond_5

    .line 34
    invoke-interface {p3, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p0

    if-eqz p0, :cond_5

    .line 35
    new-instance p0, Lcom/intsig/office/common/bg/BackgroundAndFill;

    invoke-direct {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 36
    invoke-virtual {p0, v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 37
    invoke-interface {p3, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    invoke-static {p4, p1}, Lcom/intsig/office/common/autoshape/AutoShapeDataKit;->getColor(Ljava/util/Map;Lcom/intsig/office/fc/dom4j/Element;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    goto :goto_3

    :cond_5
    move-object p0, v4

    :goto_3
    if-eqz p0, :cond_6

    .line 38
    new-instance p1, Lcom/intsig/office/common/borders/Line;

    invoke-direct {p1}, Lcom/intsig/office/common/borders/Line;-><init>()V

    .line 39
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/borders/Line;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 40
    invoke-virtual {p1, v2}, Lcom/intsig/office/common/borders/Border;->setLineWidth(I)V

    .line 41
    invoke-virtual {p1, v3}, Lcom/intsig/office/common/borders/Line;->setDash(Z)V

    return-object p1

    :cond_6
    return-object v4
.end method

.method public static createShapeLine(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/borders/Line;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "spPr"

    .line 1
    invoke-interface {p4, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    const-string v1, "ln"

    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v7

    const-string v0, "style"

    .line 2
    invoke-interface {p4, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p4

    const-string v0, "lnRef"

    const/4 v1, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    if-eqz v7, :cond_3

    const-string v2, "noFill"

    .line 3
    invoke-interface {v7, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v2

    if-nez v2, :cond_4

    const-string v2, "w"

    .line 4
    invoke-interface {v7, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 5
    invoke-interface {v7, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x42c00000    # 96.0f

    mul-float v2, v2, v3

    const v3, 0x495f3e00    # 914400.0f

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    move v10, v2

    goto :goto_0

    :cond_0
    const/4 v10, 0x1

    :goto_0
    const-string v2, "prstDash"

    .line 6
    invoke-interface {v7, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v3, "val"

    .line 7
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "solid"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 8
    :goto_1
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->instance()Lcom/intsig/office/fc/ppt/reader/BackgroundReader;

    move-result-object v2

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-virtual/range {v2 .. v7}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->processBackground(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object p0

    if-nez p0, :cond_2

    if-eqz p4, :cond_2

    .line 9
    invoke-interface {p4, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 10
    new-instance p0, Lcom/intsig/office/common/bg/BackgroundAndFill;

    invoke-direct {p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 11
    invoke-virtual {p0, v8}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 12
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object p1

    invoke-interface {p4, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p2

    invoke-virtual {p1, p3, p2}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getColor(Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    :cond_2
    move v8, v1

    move v1, v10

    goto :goto_2

    :cond_3
    if-eqz p4, :cond_4

    .line 13
    invoke-interface {p4, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p0

    if-eqz p0, :cond_4

    .line 14
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object p0

    invoke-interface {p4, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    invoke-virtual {p0, p3, p1}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getColor(Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)I

    move-result p0

    const p1, 0xffffff

    and-int/2addr p1, p0

    if-eqz p1, :cond_4

    .line 15
    new-instance p1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    invoke-direct {p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 16
    invoke-virtual {p1, v8}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 17
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    move-object p0, p1

    goto :goto_2

    :cond_4
    move-object p0, v9

    :goto_2
    if-eqz p0, :cond_5

    .line 18
    new-instance p1, Lcom/intsig/office/common/borders/Line;

    invoke-direct {p1}, Lcom/intsig/office/common/borders/Line;-><init>()V

    .line 19
    invoke-virtual {p1, p0}, Lcom/intsig/office/common/borders/Line;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 20
    invoke-virtual {p1, v1}, Lcom/intsig/office/common/borders/Border;->setLineWidth(I)V

    .line 21
    invoke-virtual {p1, v8}, Lcom/intsig/office/common/borders/Line;->setDash(Z)V

    return-object p1

    :cond_5
    return-object v9
.end method

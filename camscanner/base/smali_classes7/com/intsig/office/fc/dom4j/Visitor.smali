.class public interface abstract Lcom/intsig/office/fc/dom4j/Visitor;
.super Ljava/lang/Object;
.source "Visitor.java"


# virtual methods
.method public abstract visit(Lcom/intsig/office/fc/dom4j/Attribute;)V
.end method

.method public abstract visit(Lcom/intsig/office/fc/dom4j/CDATA;)V
.end method

.method public abstract visit(Lcom/intsig/office/fc/dom4j/Comment;)V
.end method

.method public abstract visit(Lcom/intsig/office/fc/dom4j/Document;)V
.end method

.method public abstract visit(Lcom/intsig/office/fc/dom4j/DocumentType;)V
.end method

.method public abstract visit(Lcom/intsig/office/fc/dom4j/Element;)V
.end method

.method public abstract visit(Lcom/intsig/office/fc/dom4j/Entity;)V
.end method

.method public abstract visit(Lcom/intsig/office/fc/dom4j/Namespace;)V
.end method

.method public abstract visit(Lcom/intsig/office/fc/dom4j/ProcessingInstruction;)V
.end method

.method public abstract visit(Lcom/intsig/office/fc/dom4j/Text;)V
.end method

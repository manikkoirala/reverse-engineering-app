.class public Lcom/intsig/office/fc/dom4j/io/DOMWriter;
.super Ljava/lang/Object;
.source "DOMWriter.java"


# static fields
.field private static final DEFAULT_DOM_DOCUMENT_CLASSES:[Ljava/lang/String;

.field private static loggedWarning:Z = false


# instance fields
.field private domDocumentClass:Ljava/lang/Class;

.field private namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 1
    const-string v0, "org.apache.xerces.dom.DocumentImpl"

    .line 2
    .line 3
    const-string v1, "gnu.xml.dom.DomDocument"

    .line 4
    .line 5
    const-string v2, "org.apache.crimson.tree.XmlDocument"

    .line 6
    .line 7
    const-string v3, "com.sun.xml.tree.XmlDocument"

    .line 8
    .line 9
    const-string v4, "oracle.xml.parser.v2.XMLDocument"

    .line 10
    .line 11
    const-string v5, "oracle.xml.parser.XMLDocument"

    .line 12
    .line 13
    const-string v6, "org.dom4j.dom.DOMDocument"

    .line 14
    .line 15
    filled-new-array/range {v0 .. v6}, [Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    sput-object v0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->DEFAULT_DOM_DOCUMENT_CLASSES:[Ljava/lang/String;

    .line 20
    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->domDocumentClass:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method protected appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Lcom/intsig/office/fc/dom4j/CDATA;)V
    .locals 0

    .line 42
    invoke-interface {p3}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3}, Lorg/w3c/dom/Document;->createCDATASection(Ljava/lang/String;)Lorg/w3c/dom/CDATASection;

    move-result-object p1

    .line 43
    invoke-interface {p2, p1}, Lorg/w3c/dom/Node;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    return-void
.end method

.method protected appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Lcom/intsig/office/fc/dom4j/Comment;)V
    .locals 0

    .line 44
    invoke-interface {p3}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3}, Lorg/w3c/dom/Document;->createComment(Ljava/lang/String;)Lorg/w3c/dom/Comment;

    move-result-object p1

    .line 45
    invoke-interface {p2, p1}, Lorg/w3c/dom/Node;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    return-void
.end method

.method protected appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 8

    .line 18
    invoke-interface {p3}, Lcom/intsig/office/fc/dom4j/Element;->getNamespaceURI()Ljava/lang/String;

    move-result-object v0

    .line 19
    invoke-interface {p3}, Lcom/intsig/office/fc/dom4j/Element;->getQualifiedName()Ljava/lang/String;

    move-result-object v1

    .line 20
    invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-virtual {v1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->size()I

    move-result v1

    .line 22
    invoke-interface {p3}, Lcom/intsig/office/fc/dom4j/Element;->getNamespace()Lcom/intsig/office/fc/dom4j/Namespace;

    move-result-object v2

    .line 23
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->isNamespaceDeclaration(Lcom/intsig/office/fc/dom4j/Namespace;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 24
    iget-object v3, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->push(Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 25
    invoke-virtual {p0, v0, v2}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->writeNamespace(Lorg/w3c/dom/Element;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 26
    :cond_0
    invoke-interface {p3}, Lcom/intsig/office/fc/dom4j/Element;->declaredNamespaces()Ljava/util/List;

    move-result-object v2

    .line 27
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_2

    .line 28
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 29
    invoke-virtual {p0, v6}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->isNamespaceDeclaration(Lcom/intsig/office/fc/dom4j/Namespace;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 30
    iget-object v7, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-virtual {v7, v6}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->push(Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 31
    invoke-virtual {p0, v0, v6}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->writeNamespace(Lorg/w3c/dom/Element;Lcom/intsig/office/fc/dom4j/Namespace;)V

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 32
    :cond_2
    invoke-interface {p3}, Lcom/intsig/office/fc/dom4j/Element;->attributeCount()I

    move-result v2

    :goto_1
    if-ge v4, v2, :cond_3

    .line 33
    invoke-interface {p3, v4}, Lcom/intsig/office/fc/dom4j/Element;->attribute(I)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v3

    .line 34
    invoke-interface {v3}, Lcom/intsig/office/fc/dom4j/Attribute;->getNamespaceURI()Ljava/lang/String;

    move-result-object v5

    .line 35
    invoke-interface {v3}, Lcom/intsig/office/fc/dom4j/Attribute;->getQualifiedName()Ljava/lang/String;

    move-result-object v6

    .line 36
    invoke-interface {v3}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 37
    invoke-interface {v0, v5, v6, v3}, Lorg/w3c/dom/Element;->setAttributeNS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 38
    :cond_3
    invoke-interface {p3}, Lcom/intsig/office/fc/dom4j/Branch;->content()Ljava/util/List;

    move-result-object p3

    invoke-virtual {p0, p1, v0, p3}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Ljava/util/List;)V

    .line 39
    invoke-interface {p2, v0}, Lorg/w3c/dom/Node;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 40
    :goto_2
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->size()I

    move-result p1

    if-le p1, v1, :cond_4

    .line 41
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->pop()Lcom/intsig/office/fc/dom4j/Namespace;

    goto :goto_2

    :cond_4
    return-void
.end method

.method protected appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Lcom/intsig/office/fc/dom4j/Entity;)V
    .locals 0

    .line 48
    invoke-interface {p3}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3}, Lorg/w3c/dom/Document;->createEntityReference(Ljava/lang/String;)Lorg/w3c/dom/EntityReference;

    move-result-object p1

    .line 49
    invoke-interface {p2, p1}, Lorg/w3c/dom/Node;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    return-void
.end method

.method protected appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Lcom/intsig/office/fc/dom4j/ProcessingInstruction;)V
    .locals 1

    .line 50
    invoke-interface {p3}, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;->getTarget()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3}, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;->getText()Ljava/lang/String;

    move-result-object p3

    .line 51
    invoke-interface {p1, v0, p3}, Lorg/w3c/dom/Document;->createProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/ProcessingInstruction;

    move-result-object p1

    .line 52
    invoke-interface {p2, p1}, Lorg/w3c/dom/Node;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    return-void
.end method

.method protected appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Ljava/lang/String;)V
    .locals 0

    .line 46
    invoke-interface {p1, p3}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object p1

    .line 47
    invoke-interface {p2, p1}, Lorg/w3c/dom/Node;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    return-void
.end method

.method protected appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Ljava/util/List;)V
    .locals 4

    .line 1
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_7

    .line 2
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 3
    instance-of v3, v2, Lcom/intsig/office/fc/dom4j/Element;

    if-eqz v3, :cond_0

    .line 4
    check-cast v2, Lcom/intsig/office/fc/dom4j/Element;

    invoke-virtual {p0, p1, p2, v2}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Lcom/intsig/office/fc/dom4j/Element;)V

    goto :goto_1

    .line 5
    :cond_0
    instance-of v3, v2, Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 6
    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v2}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Ljava/lang/String;)V

    goto :goto_1

    .line 7
    :cond_1
    instance-of v3, v2, Lcom/intsig/office/fc/dom4j/Text;

    if-eqz v3, :cond_2

    .line 8
    check-cast v2, Lcom/intsig/office/fc/dom4j/Text;

    .line 9
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, p2, v2}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Ljava/lang/String;)V

    goto :goto_1

    .line 10
    :cond_2
    instance-of v3, v2, Lcom/intsig/office/fc/dom4j/CDATA;

    if-eqz v3, :cond_3

    .line 11
    check-cast v2, Lcom/intsig/office/fc/dom4j/CDATA;

    invoke-virtual {p0, p1, p2, v2}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Lcom/intsig/office/fc/dom4j/CDATA;)V

    goto :goto_1

    .line 12
    :cond_3
    instance-of v3, v2, Lcom/intsig/office/fc/dom4j/Comment;

    if-eqz v3, :cond_4

    .line 13
    check-cast v2, Lcom/intsig/office/fc/dom4j/Comment;

    invoke-virtual {p0, p1, p2, v2}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Lcom/intsig/office/fc/dom4j/Comment;)V

    goto :goto_1

    .line 14
    :cond_4
    instance-of v3, v2, Lcom/intsig/office/fc/dom4j/Entity;

    if-eqz v3, :cond_5

    .line 15
    check-cast v2, Lcom/intsig/office/fc/dom4j/Entity;

    invoke-virtual {p0, p1, p2, v2}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Lcom/intsig/office/fc/dom4j/Entity;)V

    goto :goto_1

    .line 16
    :cond_5
    instance-of v3, v2, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    if-eqz v3, :cond_6

    .line 17
    check-cast v2, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    invoke-virtual {p0, p1, p2, v2}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Lcom/intsig/office/fc/dom4j/ProcessingInstruction;)V

    :cond_6
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_7
    return-void
.end method

.method protected attributeNameForNamespace(Lcom/intsig/office/fc/dom4j/Namespace;)Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/Namespace;->getPrefix()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const-string v1, "xmlns"

    .line 10
    .line 11
    if-lez v0, :cond_0

    .line 12
    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    const-string v1, ":"

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    return-object p1

    .line 34
    :cond_0
    return-object v1
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected createDomDocument(Lcom/intsig/office/fc/dom4j/Document;)Lorg/w3c/dom/Document;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->domDocumentClass:Ljava/lang/Class;

    const-string v0, "Could not instantiate an instance of DOM Document with class: "

    if-eqz p1, :cond_0

    .line 2
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/w3c/dom/Document;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 3
    new-instance v1, Lcom/intsig/office/fc/dom4j/DocumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->domDocumentClass:Ljava/lang/Class;

    .line 4
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 5
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->createDomDocumentViaJAXP()Lorg/w3c/dom/Document;

    move-result-object p1

    if-nez p1, :cond_1

    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->getDomDocumentClass()Ljava/lang/Class;

    move-result-object p1

    .line 7
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Document;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object p1, v1

    goto :goto_0

    :catch_1
    move-exception v1

    .line 8
    new-instance v2, Lcom/intsig/office/fc/dom4j/DocumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1, v1}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_1
    :goto_0
    return-object p1
.end method

.method protected createDomDocument(Lcom/intsig/office/fc/dom4j/Document;Lorg/w3c/dom/DOMImplementation;)Lorg/w3c/dom/Document;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    const/4 p1, 0x0

    .line 10
    invoke-interface {p2, p1, p1, p1}, Lorg/w3c/dom/DOMImplementation;->createDocument(Ljava/lang/String;Ljava/lang/String;Lorg/w3c/dom/DocumentType;)Lorg/w3c/dom/Document;

    move-result-object p1

    return-object p1
.end method

.method protected createDomDocumentViaJAXP()Lorg/w3c/dom/Document;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x1

    .line 3
    :try_start_0
    invoke-static {v0, v1}, Lcom/intsig/office/fc/dom4j/io/JAXPHelper;->〇080(ZZ)Lorg/w3c/dom/Document;

    .line 4
    .line 5
    .line 6
    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7
    return-object v0

    .line 8
    :catchall_0
    move-exception v0

    .line 9
    sget-boolean v2, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->loggedWarning:Z

    .line 10
    .line 11
    if-nez v2, :cond_0

    .line 12
    .line 13
    sput-boolean v1, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->loggedWarning:Z

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/office/fc/dom4j/io/SAXHelper;->〇o〇()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 22
    .line 23
    .line 24
    :cond_0
    const/4 v0, 0x0

    .line 25
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getDomDocumentClass()Ljava/lang/Class;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->domDocumentClass:Ljava/lang/Class;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    sget-object v1, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->DEFAULT_DOM_DOCUMENT_CLASSES:[Ljava/lang/String;

    .line 6
    .line 7
    array-length v1, v1

    .line 8
    const/4 v2, 0x0

    .line 9
    :goto_0
    if-ge v2, v1, :cond_1

    .line 10
    .line 11
    :try_start_0
    sget-object v3, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->DEFAULT_DOM_DOCUMENT_CLASSES:[Ljava/lang/String;

    .line 12
    .line 13
    aget-object v3, v3, v2

    .line 14
    .line 15
    const-class v4, Lcom/intsig/office/fc/dom4j/io/DOMWriter;

    .line 16
    .line 17
    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    .line 18
    .line 19
    .line 20
    move-result-object v4

    .line 21
    const/4 v5, 0x1

    .line 22
    invoke-static {v3, v5, v4}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    .line 23
    .line 24
    .line 25
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    if-eqz v0, :cond_0

    .line 27
    .line 28
    goto :goto_1

    .line 29
    :catch_0
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    :goto_1
    return-object v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected isNamespaceDeclaration(Lcom/intsig/office/fc/dom4j/Namespace;)Z
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    sget-object v0, Lcom/intsig/office/fc/dom4j/Namespace;->NO_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 4
    .line 5
    if-eq p1, v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/office/fc/dom4j/Namespace;->XML_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 8
    .line 9
    if-eq p1, v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/Namespace;->getURI()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-lez v0, :cond_0

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 24
    .line 25
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->contains(Lcom/intsig/office/fc/dom4j/Namespace;)Z

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    if-nez p1, :cond_0

    .line 30
    .line 31
    const/4 p1, 0x1

    .line 32
    return p1

    .line 33
    :cond_0
    const/4 p1, 0x0

    .line 34
    return p1
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected resetNamespaceStack()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 7
    .line 8
    sget-object v1, Lcom/intsig/office/fc/dom4j/Namespace;->XML_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->push(Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setDomDocumentClass(Ljava/lang/Class;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->domDocumentClass:Ljava/lang/Class;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDomDocumentClassName(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 1
    :try_start_0
    const-class v0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x1

    .line 8
    invoke-static {p1, v1, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->domDocumentClass:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 13
    .line 14
    return-void

    .line 15
    :catch_0
    move-exception v0

    .line 16
    new-instance v1, Lcom/intsig/office/fc/dom4j/DocumentException;

    .line 17
    .line 18
    new-instance v2, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v3, "Could not load the DOM Document class: "

    .line 24
    .line 25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-direct {v1, p1, v0}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 36
    .line 37
    .line 38
    throw v1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public write(Lcom/intsig/office/fc/dom4j/Document;)Lorg/w3c/dom/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 1
    instance-of v0, p1, Lorg/w3c/dom/Document;

    if-eqz v0, :cond_0

    .line 2
    check-cast p1, Lorg/w3c/dom/Document;

    return-object p1

    .line 3
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->resetNamespaceStack()V

    .line 4
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->createDomDocument(Lcom/intsig/office/fc/dom4j/Document;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 5
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Branch;->content()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, v0, v0, p1}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Ljava/util/List;)V

    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->clear()V

    return-object v0
.end method

.method public write(Lcom/intsig/office/fc/dom4j/Document;Lorg/w3c/dom/DOMImplementation;)Lorg/w3c/dom/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 7
    instance-of v0, p1, Lorg/w3c/dom/Document;

    if-eqz v0, :cond_0

    .line 8
    check-cast p1, Lorg/w3c/dom/Document;

    return-object p1

    .line 9
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->resetNamespaceStack()V

    .line 10
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->createDomDocument(Lcom/intsig/office/fc/dom4j/Document;Lorg/w3c/dom/DOMImplementation;)Lorg/w3c/dom/Document;

    move-result-object p2

    .line 11
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Branch;->content()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p2, p2, p1}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->appendDOMTree(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Ljava/util/List;)V

    .line 12
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->clear()V

    return-object p2
.end method

.method protected writeNamespace(Lorg/w3c/dom/Element;Lcom/intsig/office/fc/dom4j/Namespace;)V
    .locals 1

    .line 1
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/dom4j/io/DOMWriter;->attributeNameForNamespace(Lcom/intsig/office/fc/dom4j/Namespace;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/fc/dom4j/Namespace;->getURI()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p2

    .line 9
    invoke-interface {p1, v0, p2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

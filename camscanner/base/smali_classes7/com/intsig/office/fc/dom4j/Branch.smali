.class public interface abstract Lcom/intsig/office/fc/dom4j/Branch;
.super Ljava/lang/Object;
.source "Branch.java"

# interfaces
.implements Lcom/intsig/office/fc/dom4j/Node;


# virtual methods
.method public abstract add(Lcom/intsig/office/fc/dom4j/Comment;)V
.end method

.method public abstract add(Lcom/intsig/office/fc/dom4j/Element;)V
.end method

.method public abstract add(Lcom/intsig/office/fc/dom4j/Node;)V
.end method

.method public abstract add(Lcom/intsig/office/fc/dom4j/ProcessingInstruction;)V
.end method

.method public abstract addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract addElement(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract addElement(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract appendContent(Lcom/intsig/office/fc/dom4j/Branch;)V
.end method

.method public abstract clearContent()V
.end method

.method public abstract content()Ljava/util/List;
.end method

.method public abstract elementByID(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract indexOf(Lcom/intsig/office/fc/dom4j/Node;)I
.end method

.method public abstract node(I)Lcom/intsig/office/fc/dom4j/Node;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;
        }
    .end annotation
.end method

.method public abstract nodeCount()I
.end method

.method public abstract nodeIterator()Ljava/util/Iterator;
.end method

.method public abstract normalize()V
.end method

.method public abstract processingInstruction(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/ProcessingInstruction;
.end method

.method public abstract processingInstructions()Ljava/util/List;
.end method

.method public abstract processingInstructions(Ljava/lang/String;)Ljava/util/List;
.end method

.method public abstract remove(Lcom/intsig/office/fc/dom4j/Comment;)Z
.end method

.method public abstract remove(Lcom/intsig/office/fc/dom4j/Element;)Z
.end method

.method public abstract remove(Lcom/intsig/office/fc/dom4j/Node;)Z
.end method

.method public abstract remove(Lcom/intsig/office/fc/dom4j/ProcessingInstruction;)Z
.end method

.method public abstract removeProcessingInstruction(Ljava/lang/String;)Z
.end method

.method public abstract setContent(Ljava/util/List;)V
.end method

.method public abstract setProcessingInstructions(Ljava/util/List;)V
.end method

.class public Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;
.super Lcom/intsig/office/fc/dom4j/tree/AbstractDocument;
.source "DefaultDocument.java"


# static fields
.field protected static final EMPTY_ITERATOR:Ljava/util/Iterator;

.field protected static final EMPTY_LIST:Ljava/util/List;


# instance fields
.field private content:Ljava/util/List;

.field private docType:Lcom/intsig/office/fc/dom4j/DocumentType;

.field private documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

.field private transient entityResolver:Lorg/xml/sax/EntityResolver;

.field private name:Ljava/lang/String;

.field private rootElement:Lcom/intsig/office/fc/dom4j/Element;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 2
    .line 3
    sput-object v0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->EMPTY_LIST:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    sput-object v0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->EMPTY_ITERATOR:Ljava/util/Iterator;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractDocument;-><init>()V

    .line 2
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/DocumentType;)V
    .locals 1

    .line 9
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractDocument;-><init>()V

    .line 10
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 11
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->docType:Lcom/intsig/office/fc/dom4j/DocumentType;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 1

    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractDocument;-><init>()V

    .line 7
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 8
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->rootElement:Lcom/intsig/office/fc/dom4j/Element;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/DocumentType;)V
    .locals 1

    .line 12
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractDocument;-><init>()V

    .line 13
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 14
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->rootElement:Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    iput-object p2, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->docType:Lcom/intsig/office/fc/dom4j/DocumentType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractDocument;-><init>()V

    .line 4
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->name:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/DocumentType;)V
    .locals 1

    .line 16
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractDocument;-><init>()V

    .line 17
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 18
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->name:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->rootElement:Lcom/intsig/office/fc/dom4j/Element;

    .line 20
    iput-object p3, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->docType:Lcom/intsig/office/fc/dom4j/DocumentType;

    return-void
.end method


# virtual methods
.method public addDocType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createDocType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/DocumentType;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->setDocType(Lcom/intsig/office/fc/dom4j/DocumentType;)V

    .line 10
    .line 11
    .line 12
    return-object p0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method protected addNode(ILcom/intsig/office/fc/dom4j/Node;)V
    .locals 2

    if-eqz p2, :cond_2

    .line 6
    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Node;->getDocument()Lcom/intsig/office/fc/dom4j/Document;

    move-result-object v0

    if-eqz v0, :cond_1

    if-ne v0, p0, :cond_0

    goto :goto_0

    .line 7
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The Node already has an existing document: "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 8
    new-instance v0, Lcom/intsig/office/fc/dom4j/IllegalAddException;

    invoke-direct {v0, p0, p2, p1}, Lcom/intsig/office/fc/dom4j/IllegalAddException;-><init>(Lcom/intsig/office/fc/dom4j/Branch;Lcom/intsig/office/fc/dom4j/Node;Ljava/lang/String;)V

    throw v0

    .line 9
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->contentList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 10
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/dom4j/tree/AbstractDocument;->childAdded(Lcom/intsig/office/fc/dom4j/Node;)V

    :cond_2
    return-void
.end method

.method protected addNode(Lcom/intsig/office/fc/dom4j/Node;)V
    .locals 3

    if-eqz p1, :cond_2

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getDocument()Lcom/intsig/office/fc/dom4j/Document;

    move-result-object v0

    if-eqz v0, :cond_1

    if-ne v0, p0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The Node already has an existing document: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3
    new-instance v1, Lcom/intsig/office/fc/dom4j/IllegalAddException;

    invoke-direct {v1, p0, p1, v0}, Lcom/intsig/office/fc/dom4j/IllegalAddException;-><init>(Lcom/intsig/office/fc/dom4j/Branch;Lcom/intsig/office/fc/dom4j/Node;Ljava/lang/String;)V

    throw v1

    .line 4
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->contentList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractDocument;->childAdded(Lcom/intsig/office/fc/dom4j/Node;)V

    :cond_2
    return-void
.end method

.method public clearContent()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentRemoved()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->content:Ljava/util/List;

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->rootElement:Lcom/intsig/office/fc/dom4j/Element;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractNode;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    iput-object v1, v0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->rootElement:Lcom/intsig/office/fc/dom4j/Element;

    .line 9
    .line 10
    iput-object v1, v0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->content:Ljava/util/List;

    .line 11
    .line 12
    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->appendContent(Lcom/intsig/office/fc/dom4j/Branch;)V

    .line 13
    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected contentList()Ljava/util/List;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->content:Ljava/util/List;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createContentList()Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->content:Ljava/util/List;

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->rootElement:Lcom/intsig/office/fc/dom4j/Element;

    .line 12
    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->content:Ljava/util/List;

    .line 19
    .line 20
    return-object v0
    .line 21
.end method

.method public getDocType()Lcom/intsig/office/fc/dom4j/DocumentType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->docType:Lcom/intsig/office/fc/dom4j/DocumentType;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEntityResolver()Lorg/xml/sax/EntityResolver;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->entityResolver:Lorg/xml/sax/EntityResolver;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->name:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRootElement()Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->rootElement:Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getXMLEncoding()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/AbstractDocument;->encoding:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public processingInstruction(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/ProcessingInstruction;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->contentList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x0

    .line 10
    :goto_0
    if-ge v2, v1, :cond_1

    .line 11
    .line 12
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    instance-of v4, v3, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    .line 17
    .line 18
    if-eqz v4, :cond_0

    .line 19
    .line 20
    check-cast v3, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    .line 21
    .line 22
    invoke-interface {v3}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v4

    .line 26
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    move-result v4

    .line 30
    if-eqz v4, :cond_0

    .line 31
    .line 32
    return-object v3

    .line 33
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    const/4 p1, 0x0

    .line 37
    return-object p1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public processingInstructions()Ljava/util/List;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->contentList()Ljava/util/List;

    move-result-object v0

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createResultList()Lcom/intsig/office/fc/dom4j/tree/BackedList;

    move-result-object v1

    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 4
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 5
    instance-of v5, v4, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    if-eqz v5, :cond_0

    .line 6
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public processingInstructions(Ljava/lang/String;)Ljava/util/List;
    .locals 6

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->contentList()Ljava/util/List;

    move-result-object v0

    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createResultList()Lcom/intsig/office/fc/dom4j/tree/BackedList;

    move-result-object v1

    .line 9
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 10
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 11
    instance-of v5, v4, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    if-eqz v5, :cond_0

    .line 12
    check-cast v4, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    .line 13
    invoke-interface {v4}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 14
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method protected removeNode(Lcom/intsig/office/fc/dom4j/Node;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->rootElement:Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    if-ne p1, v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->rootElement:Lcom/intsig/office/fc/dom4j/Element;

    .line 7
    .line 8
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->contentList()Ljava/util/List;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractDocument;->childRemoved(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 19
    .line 20
    .line 21
    const/4 p1, 0x1

    .line 22
    return p1

    .line 23
    :cond_1
    const/4 p1, 0x0

    .line 24
    return p1
.end method

.method public removeProcessingInstruction(Ljava/lang/String;)Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->contentList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_1

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    instance-of v2, v1, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    .line 20
    .line 21
    if-eqz v2, :cond_0

    .line 22
    .line 23
    check-cast v1, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    .line 24
    .line 25
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    if-eqz v1, :cond_0

    .line 34
    .line 35
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 36
    .line 37
    .line 38
    const/4 p1, 0x1

    .line 39
    return p1

    .line 40
    :cond_1
    const/4 p1, 0x0

    .line 41
    return p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected rootElementAdded(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->rootElement:Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/dom4j/Node;->setDocument(Lcom/intsig/office/fc/dom4j/Document;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setContent(Ljava/util/List;)V
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->rootElement:Lcom/intsig/office/fc/dom4j/Element;

    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentRemoved()V

    .line 5
    .line 6
    .line 7
    instance-of v1, p1, Lcom/intsig/office/fc/dom4j/tree/ContentListFacade;

    .line 8
    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    check-cast p1, Lcom/intsig/office/fc/dom4j/tree/ContentListFacade;

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/tree/ContentListFacade;->getBackingList()Ljava/util/List;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    :cond_0
    if-nez p1, :cond_1

    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->content:Ljava/util/List;

    .line 20
    .line 21
    goto :goto_2

    .line 22
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createContentList(I)Ljava/util/List;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    const/4 v2, 0x0

    .line 31
    :goto_0
    if-ge v2, v0, :cond_6

    .line 32
    .line 33
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    instance-of v4, v3, Lcom/intsig/office/fc/dom4j/Node;

    .line 38
    .line 39
    if-eqz v4, :cond_5

    .line 40
    .line 41
    check-cast v3, Lcom/intsig/office/fc/dom4j/Node;

    .line 42
    .line 43
    invoke-interface {v3}, Lcom/intsig/office/fc/dom4j/Node;->getDocument()Lcom/intsig/office/fc/dom4j/Document;

    .line 44
    .line 45
    .line 46
    move-result-object v4

    .line 47
    if-eqz v4, :cond_2

    .line 48
    .line 49
    if-eq v4, p0, :cond_2

    .line 50
    .line 51
    invoke-interface {v3}, Lcom/intsig/office/fc/dom4j/Node;->clone()Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object v3

    .line 55
    check-cast v3, Lcom/intsig/office/fc/dom4j/Node;

    .line 56
    .line 57
    :cond_2
    instance-of v4, v3, Lcom/intsig/office/fc/dom4j/Element;

    .line 58
    .line 59
    if-eqz v4, :cond_4

    .line 60
    .line 61
    iget-object v4, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->rootElement:Lcom/intsig/office/fc/dom4j/Element;

    .line 62
    .line 63
    if-nez v4, :cond_3

    .line 64
    .line 65
    move-object v4, v3

    .line 66
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    iput-object v4, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->rootElement:Lcom/intsig/office/fc/dom4j/Element;

    .line 69
    .line 70
    goto :goto_1

    .line 71
    :cond_3
    new-instance v0, Lcom/intsig/office/fc/dom4j/IllegalAddException;

    .line 72
    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    .line 74
    .line 75
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    .line 77
    .line 78
    const-string v2, "A document may only contain one root element: "

    .line 79
    .line 80
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object p1

    .line 90
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/dom4j/IllegalAddException;-><init>(Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    throw v0

    .line 94
    :cond_4
    :goto_1
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    .line 96
    .line 97
    invoke-virtual {p0, v3}, Lcom/intsig/office/fc/dom4j/tree/AbstractDocument;->childAdded(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 98
    .line 99
    .line 100
    :cond_5
    add-int/lit8 v2, v2, 0x1

    .line 101
    .line 102
    goto :goto_0

    .line 103
    :cond_6
    iput-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->content:Ljava/util/List;

    .line 104
    .line 105
    :goto_2
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public setDocType(Lcom/intsig/office/fc/dom4j/DocumentType;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->docType:Lcom/intsig/office/fc/dom4j/DocumentType;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDocumentFactory(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setEntityResolver(Lorg/xml/sax/EntityResolver;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->entityResolver:Lorg/xml/sax/EntityResolver;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->name:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public abstract Lcom/intsig/office/fc/dom4j/tree/AbstractElement;
.super Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;
.source "AbstractElement.java"

# interfaces
.implements Lcom/intsig/office/fc/dom4j/Element;


# static fields
.field private static final DOCUMENT_FACTORY:Lcom/intsig/office/fc/dom4j/DocumentFactory;

.field protected static final EMPTY_ITERATOR:Ljava/util/Iterator;

.field protected static final EMPTY_LIST:Ljava/util/List;

.field protected static final USE_STRINGVALUE_SEPARATOR:Z = false

.field protected static final VERBOSE_TOSTRING:Z = false


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sput-object v0, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->DOCUMENT_FACTORY:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 6
    .line 7
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 8
    .line 9
    sput-object v0, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->EMPTY_LIST:Ljava/util/List;

    .line 10
    .line 11
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sput-object v0, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->EMPTY_ITERATOR:Ljava/util/Iterator;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public accept(Lcom/intsig/office/fc/dom4j/Visitor;)V
    .locals 4

    .line 1
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/dom4j/Visitor;->visit(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attributeCount()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    const/4 v1, 0x0

    .line 9
    const/4 v2, 0x0

    .line 10
    :goto_0
    if-ge v2, v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attribute(I)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/dom4j/Visitor;->visit(Lcom/intsig/office/fc/dom4j/Attribute;)V

    .line 17
    .line 18
    .line 19
    add-int/lit8 v2, v2, 0x1

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->nodeCount()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    :goto_1
    if-ge v1, v0, :cond_1

    .line 27
    .line 28
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->node(I)Lcom/intsig/office/fc/dom4j/Node;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    invoke-interface {v2, p1}, Lcom/intsig/office/fc/dom4j/Node;->accept(Lcom/intsig/office/fc/dom4j/Visitor;)V

    .line 33
    .line 34
    .line 35
    add-int/lit8 v1, v1, 0x1

    .line 36
    .line 37
    goto :goto_1

    .line 38
    :cond_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public add(Lcom/intsig/office/fc/dom4j/Attribute;)V
    .locals 2

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getParent()Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Attribute;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attribute(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 4
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->remove(Lcom/intsig/office/fc/dom4j/Attribute;)Z

    goto :goto_0

    .line 5
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attributeList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->childAdded(Lcom/intsig/office/fc/dom4j/Node;)V

    :cond_1
    :goto_0
    return-void

    .line 7
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The Attribute already has an existing parent \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getParent()Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->getQualifiedName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 9
    new-instance v1, Lcom/intsig/office/fc/dom4j/IllegalAddException;

    invoke-direct {v1, p0, p1, v0}, Lcom/intsig/office/fc/dom4j/IllegalAddException;-><init>(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/Node;Ljava/lang/String;)V

    throw v1
.end method

.method public add(Lcom/intsig/office/fc/dom4j/CDATA;)V
    .locals 0

    .line 20
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNode(Lcom/intsig/office/fc/dom4j/Node;)V

    return-void
.end method

.method public add(Lcom/intsig/office/fc/dom4j/Comment;)V
    .locals 0

    .line 21
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNode(Lcom/intsig/office/fc/dom4j/Node;)V

    return-void
.end method

.method public add(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 0

    .line 22
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNode(Lcom/intsig/office/fc/dom4j/Node;)V

    return-void
.end method

.method public add(Lcom/intsig/office/fc/dom4j/Entity;)V
    .locals 0

    .line 23
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNode(Lcom/intsig/office/fc/dom4j/Node;)V

    return-void
.end method

.method public add(Lcom/intsig/office/fc/dom4j/Namespace;)V
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNode(Lcom/intsig/office/fc/dom4j/Node;)V

    return-void
.end method

.method public add(Lcom/intsig/office/fc/dom4j/Node;)V
    .locals 2

    .line 10
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getNodeType()S

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_7

    const/4 v1, 0x2

    if-eq v0, v1, :cond_6

    const/4 v1, 0x3

    if-eq v0, v1, :cond_5

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    .line 11
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->invalidNodeTypeAddException(Lcom/intsig/office/fc/dom4j/Node;)V

    goto :goto_0

    .line 12
    :cond_0
    check-cast p1, Lcom/intsig/office/fc/dom4j/Namespace;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->add(Lcom/intsig/office/fc/dom4j/Namespace;)V

    goto :goto_0

    .line 13
    :cond_1
    check-cast p1, Lcom/intsig/office/fc/dom4j/Comment;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->add(Lcom/intsig/office/fc/dom4j/Comment;)V

    goto :goto_0

    .line 14
    :cond_2
    check-cast p1, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->add(Lcom/intsig/office/fc/dom4j/ProcessingInstruction;)V

    goto :goto_0

    .line 15
    :cond_3
    check-cast p1, Lcom/intsig/office/fc/dom4j/Entity;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->add(Lcom/intsig/office/fc/dom4j/Entity;)V

    goto :goto_0

    .line 16
    :cond_4
    check-cast p1, Lcom/intsig/office/fc/dom4j/CDATA;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->add(Lcom/intsig/office/fc/dom4j/CDATA;)V

    goto :goto_0

    .line 17
    :cond_5
    check-cast p1, Lcom/intsig/office/fc/dom4j/Text;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->add(Lcom/intsig/office/fc/dom4j/Text;)V

    goto :goto_0

    .line 18
    :cond_6
    check-cast p1, Lcom/intsig/office/fc/dom4j/Attribute;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->add(Lcom/intsig/office/fc/dom4j/Attribute;)V

    goto :goto_0

    .line 19
    :cond_7
    check-cast p1, Lcom/intsig/office/fc/dom4j/Element;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->add(Lcom/intsig/office/fc/dom4j/Element;)V

    :goto_0
    return-void
.end method

.method public add(Lcom/intsig/office/fc/dom4j/ProcessingInstruction;)V
    .locals 0

    .line 25
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNode(Lcom/intsig/office/fc/dom4j/Node;)V

    return-void
.end method

.method public add(Lcom/intsig/office/fc/dom4j/Text;)V
    .locals 0

    .line 26
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNode(Lcom/intsig/office/fc/dom4j/Node;)V

    return-void
.end method

.method public addAttribute(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 2

    .line 8
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attribute(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v0

    if-eqz p2, :cond_2

    if-nez v0, :cond_0

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->add(Lcom/intsig/office/fc/dom4j/Attribute;)V

    goto :goto_0

    .line 10
    :cond_0
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Node;->isReadOnly()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 11
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->remove(Lcom/intsig/office/fc/dom4j/Attribute;)Z

    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->add(Lcom/intsig/office/fc/dom4j/Attribute;)V

    goto :goto_0

    .line 13
    :cond_1
    invoke-interface {v0, p2}, Lcom/intsig/office/fc/dom4j/Attribute;->setValue(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    .line 14
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->remove(Lcom/intsig/office/fc/dom4j/Attribute;)Z

    :cond_3
    :goto_0
    return-object p0
.end method

.method public addAttribute(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v0

    if-eqz p2, :cond_2

    if-nez v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createAttribute(Lcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->add(Lcom/intsig/office/fc/dom4j/Attribute;)V

    goto :goto_0

    .line 3
    :cond_0
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Node;->isReadOnly()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->remove(Lcom/intsig/office/fc/dom4j/Attribute;)Z

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createAttribute(Lcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->add(Lcom/intsig/office/fc/dom4j/Attribute;)V

    goto :goto_0

    .line 6
    :cond_1
    invoke-interface {v0, p2}, Lcom/intsig/office/fc/dom4j/Attribute;->setValue(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    .line 7
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->remove(Lcom/intsig/office/fc/dom4j/Attribute;)Z

    :cond_3
    :goto_0
    return-object p0
.end method

.method public addCDATA(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createCDATA(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/CDATA;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNewNode(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 10
    .line 11
    .line 12
    return-object p0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public addComment(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createComment(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Comment;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNewNode(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 10
    .line 11
    .line 12
    return-object p0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public addElement(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, ":"

    .line 6
    .line 7
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-lez v1, :cond_1

    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    add-int/lit8 v1, v1, 0x1

    .line 19
    .line 20
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getNamespaceForPrefix(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    if-eqz v3, :cond_0

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/dom4j/IllegalAddException;

    .line 32
    .line 33
    new-instance v1, Ljava/lang/StringBuilder;

    .line 34
    .line 35
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .line 37
    .line 38
    const-string v3, "No such namespace prefix: "

    .line 39
    .line 40
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v2, " is in scope on: "

    .line 47
    .line 48
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    const-string v2, " so cannot add element: "

    .line 55
    .line 56
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/dom4j/IllegalAddException;-><init>(Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    throw v0

    .line 70
    :cond_1
    const-string v1, ""

    .line 71
    .line 72
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getNamespaceForPrefix(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    .line 73
    .line 74
    .line 75
    move-result-object v3

    .line 76
    move-object v1, p1

    .line 77
    :goto_0
    if-eqz v3, :cond_2

    .line 78
    .line 79
    invoke-virtual {v0, v1, v3}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    goto :goto_1

    .line 88
    :cond_2
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createElement(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    :goto_1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNewNode(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 93
    .line 94
    .line 95
    return-object p1
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public addEntity(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createEntity(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Entity;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNewNode(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 10
    .line 11
    .line 12
    return-object p0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public addNamespace(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createNamespace(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNewNode(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 10
    .line 11
    .line 12
    return-object p0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected addNewNode(ILcom/intsig/office/fc/dom4j/Node;)V
    .locals 1

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 4
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->childAdded(Lcom/intsig/office/fc/dom4j/Node;)V

    return-void
.end method

.method protected addNewNode(Lcom/intsig/office/fc/dom4j/Node;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->childAdded(Lcom/intsig/office/fc/dom4j/Node;)V

    return-void
.end method

.method protected addNode(ILcom/intsig/office/fc/dom4j/Node;)V
    .locals 1

    .line 6
    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Node;->getParent()Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-nez v0, :cond_0

    .line 7
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNewNode(ILcom/intsig/office/fc/dom4j/Node;)V

    return-void

    .line 8
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "The Node already has an existing parent of \""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Node;->getParent()Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->getQualifiedName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 10
    new-instance v0, Lcom/intsig/office/fc/dom4j/IllegalAddException;

    invoke-direct {v0, p0, p2, p1}, Lcom/intsig/office/fc/dom4j/IllegalAddException;-><init>(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/Node;Ljava/lang/String;)V

    throw v0
.end method

.method protected addNode(Lcom/intsig/office/fc/dom4j/Node;)V
    .locals 2

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getParent()Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNewNode(Lcom/intsig/office/fc/dom4j/Node;)V

    return-void

    .line 3
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The Node already has an existing parent of \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getParent()Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->getQualifiedName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/office/fc/dom4j/IllegalAddException;

    invoke-direct {v1, p0, p1, v0}, Lcom/intsig/office/fc/dom4j/IllegalAddException;-><init>(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/Node;Ljava/lang/String;)V

    throw v1
.end method

.method public addProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    move-result-object p1

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNewNode(Lcom/intsig/office/fc/dom4j/Node;)V

    return-object p0
.end method

.method public addProcessingInstruction(Ljava/lang/String;Ljava/util/Map;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createProcessingInstruction(Ljava/lang/String;Ljava/util/Map;)Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    move-result-object p1

    .line 4
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNewNode(Lcom/intsig/office/fc/dom4j/Node;)V

    return-object p0
.end method

.method public addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Text;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNewNode(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 10
    .line 11
    .line 12
    return-object p0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public additionalNamespaces()Ljava/util/List;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    move-result-object v0

    .line 2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createResultList()Lcom/intsig/office/fc/dom4j/tree/BackedList;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    .line 4
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 5
    instance-of v5, v4, Lcom/intsig/office/fc/dom4j/Namespace;

    if-eqz v5, :cond_0

    .line 6
    check-cast v4, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getNamespace()Lcom/intsig/office/fc/dom4j/Namespace;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/dom4j/Namespace;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 8
    invoke-virtual {v2, v4}, Lcom/intsig/office/fc/dom4j/tree/BackedList;->addLocal(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public additionalNamespaces(Ljava/lang/String;)Ljava/util/List;
    .locals 6

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    move-result-object v0

    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createResultList()Lcom/intsig/office/fc/dom4j/tree/BackedList;

    move-result-object v1

    .line 11
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 12
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 13
    instance-of v5, v4, Lcom/intsig/office/fc/dom4j/Namespace;

    if-eqz v5, :cond_0

    .line 14
    check-cast v4, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 15
    invoke-virtual {v4}, Lcom/intsig/office/fc/dom4j/Namespace;->getURI()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 16
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/dom4j/tree/BackedList;->addLocal(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public appendAttributes(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 4

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->attributeCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    :goto_0
    if-ge v1, v0, :cond_1

    .line 7
    .line 8
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attribute(I)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Node;->supportsParent()Z

    .line 13
    .line 14
    .line 15
    move-result v3

    .line 16
    if-eqz v3, :cond_0

    .line 17
    .line 18
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Attribute;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    .line 19
    .line 20
    .line 21
    move-result-object v3

    .line 22
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    invoke-virtual {p0, v3, v2}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addAttribute(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 27
    .line 28
    .line 29
    goto :goto_1

    .line 30
    :cond_0
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->add(Lcom/intsig/office/fc/dom4j/Attribute;)V

    .line 31
    .line 32
    .line 33
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public asXML()Ljava/lang/String;
    .locals 4

    .line 1
    :try_start_0
    new-instance v0, Ljava/io/StringWriter;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 7
    .line 8
    new-instance v2, Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 9
    .line 10
    invoke-direct {v2}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;-><init>()V

    .line 11
    .line 12
    .line 13
    invoke-direct {v1, v0, v2}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>(Ljava/io/Writer;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->write(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->flush()V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    return-object v0

    .line 27
    :catch_0
    move-exception v0

    .line 28
    new-instance v1, Ljava/lang/RuntimeException;

    .line 29
    .line 30
    new-instance v2, Ljava/lang/StringBuilder;

    .line 31
    .line 32
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    const-string v3, "IOException while generating textual representation: "

    .line 36
    .line 37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    throw v1
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public attribute(I)Lcom/intsig/office/fc/dom4j/Attribute;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attributeList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/dom4j/Attribute;

    return-object p1
.end method

.method public attribute(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Attribute;
    .locals 5

    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attributeList()Ljava/util/List;

    move-result-object v0

    .line 7
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 8
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/intsig/office/fc/dom4j/Attribute;

    .line 9
    invoke-interface {v3}, Lcom/intsig/office/fc/dom4j/Attribute;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/intsig/office/fc/dom4j/QName;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;
    .locals 5

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attributeList()Ljava/util/List;

    move-result-object v0

    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 4
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/intsig/office/fc/dom4j/Attribute;

    .line 5
    invoke-interface {v3}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public attribute(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/Attribute;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attribute(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object p1

    return-object p1
.end method

.method public attributeCount()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attributeList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public attributeIterator()Ljava/util/Iterator;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attributeList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected abstract attributeList()Ljava/util/List;
.end method

.method protected abstract attributeList(I)Ljava/util/List;
.end method

.method public attributeValue(Lcom/intsig/office/fc/dom4j/QName;)Ljava/lang/String;
    .locals 0

    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attribute(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 4
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public attributeValue(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 6
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attributeValue(Lcom/intsig/office/fc/dom4j/QName;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    move-object p2, p1

    :cond_0
    return-object p2
.end method

.method public attributeValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 2
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public attributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    move-object p2, p1

    :cond_0
    return-object p2
.end method

.method public attributes()Ljava/util/List;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/ContentListFacade;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attributeList()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-direct {v0, p0, v1}, Lcom/intsig/office/fc/dom4j/tree/ContentListFacade;-><init>(Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;Ljava/util/List;)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected childAdded(Lcom/intsig/office/fc/dom4j/Node;)V
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/dom4j/Node;->setParent(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 4
    .line 5
    .line 6
    :cond_0
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected childRemoved(Lcom/intsig/office/fc/dom4j/Node;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Node;->setParent(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 5
    .line 6
    .line 7
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Node;->setDocument(Lcom/intsig/office/fc/dom4j/Document;)V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected createAttributeList()Ljava/util/List;
    .locals 1

    const/4 v0, 0x5

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->createAttributeList(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected createAttributeList(I)Ljava/util/List;
    .locals 1

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0
.end method

.method public createCopy()Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/dom4j/Element;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->createElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    .line 2
    invoke-interface {v0, p0}, Lcom/intsig/office/fc/dom4j/Element;->appendAttributes(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 3
    invoke-interface {v0, p0}, Lcom/intsig/office/fc/dom4j/Branch;->appendContent(Lcom/intsig/office/fc/dom4j/Branch;)V

    return-object v0
.end method

.method public createCopy(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 0

    .line 7
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->createElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    .line 8
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/dom4j/Element;->appendAttributes(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 9
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/dom4j/Branch;->appendContent(Lcom/intsig/office/fc/dom4j/Branch;)V

    return-object p1
.end method

.method public createCopy(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 0

    .line 4
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->createElement(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    .line 5
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/dom4j/Element;->appendAttributes(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 6
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/dom4j/Branch;->appendContent(Lcom/intsig/office/fc/dom4j/Branch;)V

    return-object p1
.end method

.method protected createElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    return-object p1
.end method

.method protected createElement(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createElement(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    return-object p1
.end method

.method protected createSingleIterator(Ljava/lang/Object;)Ljava/util/Iterator;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/SingleIterator;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/dom4j/tree/SingleIterator;-><init>(Ljava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public declaredNamespaces()Ljava/util/List;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createResultList()Lcom/intsig/office/fc/dom4j/tree/BackedList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    const/4 v3, 0x0

    .line 14
    :goto_0
    if-ge v3, v2, :cond_1

    .line 15
    .line 16
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v4

    .line 20
    instance-of v5, v4, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 21
    .line 22
    if-eqz v5, :cond_0

    .line 23
    .line 24
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/dom4j/tree/BackedList;->addLocal(Ljava/lang/Object;)V

    .line 25
    .line 26
    .line 27
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    return-object v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 5

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    move-result-object v0

    .line 8
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 9
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 10
    instance-of v4, v3, Lcom/intsig/office/fc/dom4j/Element;

    if-eqz v4, :cond_0

    .line 11
    check-cast v3, Lcom/intsig/office/fc/dom4j/Element;

    .line 12
    invoke-interface {v3}, Lcom/intsig/office/fc/dom4j/Element;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/intsig/office/fc/dom4j/QName;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    move-result-object v0

    .line 2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 3
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 4
    instance-of v4, v3, Lcom/intsig/office/fc/dom4j/Element;

    if-eqz v4, :cond_0

    .line 5
    check-cast v3, Lcom/intsig/office/fc/dom4j/Element;

    .line 6
    invoke-interface {v3}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public element(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    return-object p1
.end method

.method public elementIterator()Ljava/util/Iterator;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->elements()Ljava/util/List;

    move-result-object v0

    .line 2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public elementIterator(Lcom/intsig/office/fc/dom4j/QName;)Ljava/util/Iterator;
    .locals 0

    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->elements(Lcom/intsig/office/fc/dom4j/QName;)Ljava/util/List;

    move-result-object p1

    .line 6
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    return-object p1
.end method

.method public elementIterator(Ljava/lang/String;)Ljava/util/Iterator;
    .locals 0

    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->elements(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    .line 4
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    return-object p1
.end method

.method public elementIterator(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Ljava/util/Iterator;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->elementIterator(Lcom/intsig/office/fc/dom4j/QName;)Ljava/util/Iterator;

    move-result-object p1

    return-object p1
.end method

.method public elementText(Lcom/intsig/office/fc/dom4j/QName;)Ljava/lang/String;
    .locals 0

    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 4
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public elementText(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public elementTextTrim(Lcom/intsig/office/fc/dom4j/QName;)Ljava/lang/String;
    .locals 0

    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 4
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getTextTrim()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public elementTextTrim(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getTextTrim()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public elements()Ljava/util/List;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    move-result-object v0

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createResultList()Lcom/intsig/office/fc/dom4j/tree/BackedList;

    move-result-object v1

    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 4
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 5
    instance-of v5, v4, Lcom/intsig/office/fc/dom4j/Element;

    if-eqz v5, :cond_0

    .line 6
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/dom4j/tree/BackedList;->addLocal(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public elements(Lcom/intsig/office/fc/dom4j/QName;)Ljava/util/List;
    .locals 6

    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    move-result-object v0

    .line 16
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createResultList()Lcom/intsig/office/fc/dom4j/tree/BackedList;

    move-result-object v1

    .line 17
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 18
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 19
    instance-of v5, v4, Lcom/intsig/office/fc/dom4j/Element;

    if-eqz v5, :cond_0

    .line 20
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 21
    invoke-interface {v4}, Lcom/intsig/office/fc/dom4j/Element;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/intsig/office/fc/dom4j/QName;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 22
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/dom4j/tree/BackedList;->addLocal(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public elements(Ljava/lang/String;)Ljava/util/List;
    .locals 6

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    move-result-object v0

    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createResultList()Lcom/intsig/office/fc/dom4j/tree/BackedList;

    move-result-object v1

    .line 9
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 10
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 11
    instance-of v5, v4, Lcom/intsig/office/fc/dom4j/Element;

    if-eqz v5, :cond_0

    .line 12
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 13
    invoke-interface {v4}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 14
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/dom4j/tree/BackedList;->addLocal(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public elements(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Ljava/util/List;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->elements(Lcom/intsig/office/fc/dom4j/QName;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public ensureAttributesCapacity(I)V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    if-le p1, v0, :cond_0

    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attributeList()Ljava/util/List;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    instance-of v1, v0, Ljava/util/ArrayList;

    .line 9
    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    check-cast v0, Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getData()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->getText()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;
    .locals 1

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/dom4j/Element;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/QName;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    return-object v0

    .line 14
    :cond_0
    sget-object v0, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->DOCUMENT_FACTORY:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 15
    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/dom4j/Element;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/QName;->getName()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNamespace()Lcom/intsig/office/fc/dom4j/Namespace;
    .locals 1

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/dom4j/Element;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/QName;->getNamespace()Lcom/intsig/office/fc/dom4j/Namespace;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNamespaceForPrefix(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;
    .locals 5

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const-string p1, ""

    .line 4
    .line 5
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getNamespacePrefix()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getNamespace()Lcom/intsig/office/fc/dom4j/Namespace;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    return-object p1

    .line 20
    :cond_1
    const-string v0, "xml"

    .line 21
    .line 22
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-eqz v0, :cond_2

    .line 27
    .line 28
    sget-object p1, Lcom/intsig/office/fc/dom4j/Namespace;->XML_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 29
    .line 30
    return-object p1

    .line 31
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    const/4 v2, 0x0

    .line 40
    :goto_0
    if-ge v2, v1, :cond_4

    .line 41
    .line 42
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    instance-of v4, v3, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 47
    .line 48
    if-eqz v4, :cond_3

    .line 49
    .line 50
    check-cast v3, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 51
    .line 52
    invoke-virtual {v3}, Lcom/intsig/office/fc/dom4j/Namespace;->getPrefix()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v4

    .line 56
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    move-result v4

    .line 60
    if-eqz v4, :cond_3

    .line 61
    .line 62
    return-object v3

    .line 63
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractNode;->getParent()Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    if-eqz v0, :cond_5

    .line 71
    .line 72
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/dom4j/Element;->getNamespaceForPrefix(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    if-eqz v0, :cond_5

    .line 77
    .line 78
    return-object v0

    .line 79
    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 80
    .line 81
    .line 82
    move-result p1

    .line 83
    if-gtz p1, :cond_6

    .line 84
    .line 85
    sget-object p1, Lcom/intsig/office/fc/dom4j/Namespace;->NO_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 86
    .line 87
    return-object p1

    .line 88
    :cond_6
    const/4 p1, 0x0

    .line 89
    return-object p1
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public getNamespaceForURI(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;
    .locals 5

    .line 1
    if-eqz p1, :cond_4

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-gtz v0, :cond_0

    .line 8
    .line 9
    goto :goto_1

    .line 10
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getNamespaceURI()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getNamespace()Lcom/intsig/office/fc/dom4j/Namespace;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    return-object p1

    .line 25
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    const/4 v2, 0x0

    .line 34
    :goto_0
    if-ge v2, v1, :cond_3

    .line 35
    .line 36
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v3

    .line 40
    instance-of v4, v3, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 41
    .line 42
    if-eqz v4, :cond_2

    .line 43
    .line 44
    check-cast v3, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 45
    .line 46
    invoke-virtual {v3}, Lcom/intsig/office/fc/dom4j/Namespace;->getURI()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v4

    .line 50
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    move-result v4

    .line 54
    if-eqz v4, :cond_2

    .line 55
    .line 56
    return-object v3

    .line 57
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_3
    const/4 p1, 0x0

    .line 61
    return-object p1

    .line 62
    :cond_4
    :goto_1
    sget-object p1, Lcom/intsig/office/fc/dom4j/Namespace;->NO_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 63
    .line 64
    return-object p1
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getNamespacePrefix()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/dom4j/Element;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/QName;->getNamespacePrefix()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNamespaceURI()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/dom4j/Element;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/QName;->getNamespaceURI()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNamespacesForURI(Ljava/lang/String;)Ljava/util/List;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createResultList()Lcom/intsig/office/fc/dom4j/tree/BackedList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    const/4 v3, 0x0

    .line 14
    :goto_0
    if-ge v3, v2, :cond_1

    .line 15
    .line 16
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v4

    .line 20
    instance-of v5, v4, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 21
    .line 22
    if-eqz v5, :cond_0

    .line 23
    .line 24
    move-object v5, v4

    .line 25
    check-cast v5, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 26
    .line 27
    invoke-virtual {v5}, Lcom/intsig/office/fc/dom4j/Namespace;->getURI()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v5

    .line 31
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 32
    .line 33
    .line 34
    move-result v5

    .line 35
    if-eqz v5, :cond_0

    .line 36
    .line 37
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/dom4j/tree/BackedList;->addLocal(Ljava/lang/Object;)V

    .line 38
    .line 39
    .line 40
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    return-object v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getNodeType()S
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPath(Lcom/intsig/office/fc/dom4j/Element;)Ljava/lang/String;
    .locals 3

    .line 1
    if-ne p0, p1, :cond_0

    .line 2
    .line 3
    const-string p1, "."

    .line 4
    .line 5
    return-object p1

    .line 6
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractNode;->getParent()Lcom/intsig/office/fc/dom4j/Element;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const-string v1, "/"

    .line 11
    .line 12
    if-nez v0, :cond_1

    .line 13
    .line 14
    new-instance p1, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getXPathNameStep()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    return-object p1

    .line 34
    :cond_1
    if-ne v0, p1, :cond_2

    .line 35
    .line 36
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getXPathNameStep()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    return-object p1

    .line 41
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    .line 42
    .line 43
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/dom4j/Node;->getPath(Lcom/intsig/office/fc/dom4j/Element;)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getXPathNameStep()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    return-object p1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 2

    .line 1
    const-string v0, ":"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_0

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    add-int/lit8 v0, v0, 0x1

    .line 15
    .line 16
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const-string v1, ""

    .line 22
    .line 23
    :goto_0
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getNamespaceForPrefix(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-virtual {v1, p1, v0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    return-object p1

    .line 38
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    return-object p1
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getQualifiedName()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/dom4j/Element;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/QName;->getQualifiedName()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStringValue()Ljava/lang/String;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-lez v1, :cond_3

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    const/4 v3, 0x1

    .line 13
    if-ne v1, v3, :cond_0

    .line 14
    .line 15
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->getContentAsStringValue(Ljava/lang/Object;)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    return-object v0

    .line 24
    :cond_0
    new-instance v3, Ljava/lang/StringBuffer;

    .line 25
    .line 26
    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 27
    .line 28
    .line 29
    :goto_0
    if-ge v2, v1, :cond_2

    .line 30
    .line 31
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v4

    .line 35
    invoke-virtual {p0, v4}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->getContentAsStringValue(Ljava/lang/Object;)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v4

    .line 39
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 40
    .line 41
    .line 42
    move-result v5

    .line 43
    if-lez v5, :cond_1

    .line 44
    .line 45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 46
    .line 47
    .line 48
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    return-object v0

    .line 56
    :cond_3
    const-string v0, ""

    .line 57
    .line 58
    return-object v0
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getUniquePath(Lcom/intsig/office/fc/dom4j/Element;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractNode;->getParent()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "/"

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    new-instance p1, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getXPathNameStep()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    return-object p1

    .line 29
    :cond_0
    new-instance v2, Ljava/lang/StringBuffer;

    .line 30
    .line 31
    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 32
    .line 33
    .line 34
    if-eq v0, p1, :cond_1

    .line 35
    .line 36
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/dom4j/Node;->getUniquePath(Lcom/intsig/office/fc/dom4j/Element;)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 44
    .line 45
    .line 46
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getXPathNameStep()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 51
    .line 52
    .line 53
    invoke-interface {p0}, Lcom/intsig/office/fc/dom4j/Element;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/dom4j/Element;->elements(Lcom/intsig/office/fc/dom4j/QName;)Ljava/util/List;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    const/4 v1, 0x1

    .line 66
    if-le v0, v1, :cond_2

    .line 67
    .line 68
    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 69
    .line 70
    .line 71
    move-result p1

    .line 72
    if-ltz p1, :cond_2

    .line 73
    .line 74
    const-string v0, "["

    .line 75
    .line 76
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 77
    .line 78
    .line 79
    add-int/2addr p1, v1

    .line 80
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object p1

    .line 84
    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 85
    .line 86
    .line 87
    const-string p1, "]"

    .line 88
    .line 89
    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 90
    .line 91
    .line 92
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object p1

    .line 96
    return-object p1
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public getXPathNameStep()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getNamespaceURI()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    goto :goto_1

    .line 14
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getNamespacePrefix()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    if-eqz v0, :cond_2

    .line 19
    .line 20
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-nez v0, :cond_1

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getQualifiedName()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    return-object v0

    .line 32
    :cond_2
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v1, "*[name()=\'"

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getName()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    const-string v1, "\']"

    .line 50
    .line 51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    return-object v0

    .line 59
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getName()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    return-object v0
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getXPathResult(I)Lcom/intsig/office/fc/dom4j/Node;
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->node(I)Lcom/intsig/office/fc/dom4j/Node;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->supportsParent()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/dom4j/Node;->asXPathResult(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/fc/dom4j/Node;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    :cond_0
    return-object p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public hasMixedContent()Z
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_3

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-nez v2, :cond_3

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    const/4 v3, 0x2

    .line 19
    if-ge v2, v3, :cond_0

    .line 20
    .line 21
    goto :goto_1

    .line 22
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const/4 v2, 0x0

    .line 27
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    if-eqz v3, :cond_3

    .line 32
    .line 33
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    if-eq v3, v2, :cond_1

    .line 42
    .line 43
    if-eqz v2, :cond_2

    .line 44
    .line 45
    const/4 v0, 0x1

    .line 46
    return v0

    .line 47
    :cond_2
    move-object v2, v3

    .line 48
    goto :goto_0

    .line 49
    :cond_3
    :goto_1
    return v1
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public indexOf(Lcom/intsig/office/fc/dom4j/Node;)I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public isRootElement()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractNode;->getDocument()Lcom/intsig/office/fc/dom4j/Document;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-ne v0, p0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    return v0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isTextOnly()Z
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_2

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz v2, :cond_2

    .line 24
    .line 25
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    instance-of v3, v2, Lcom/intsig/office/fc/dom4j/CharacterData;

    .line 30
    .line 31
    if-nez v3, :cond_1

    .line 32
    .line 33
    instance-of v2, v2, Ljava/lang/String;

    .line 34
    .line 35
    if-nez v2, :cond_1

    .line 36
    .line 37
    const/4 v0, 0x0

    .line 38
    return v0

    .line 39
    :cond_2
    :goto_0
    return v1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public node(I)Lcom/intsig/office/fc/dom4j/Node;
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-ltz p1, :cond_2

    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 5
    .line 6
    .line 7
    move-result-object v1

    .line 8
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-lt p1, v2, :cond_0

    .line 13
    .line 14
    return-object v0

    .line 15
    :cond_0
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    if-eqz p1, :cond_2

    .line 20
    .line 21
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/Node;

    .line 22
    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    check-cast p1, Lcom/intsig/office/fc/dom4j/Node;

    .line 26
    .line 27
    return-object p1

    .line 28
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Text;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    return-object p1

    .line 41
    :cond_2
    return-object v0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public nodeCount()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public nodeIterator()Ljava/util/Iterator;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public normalize()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    :goto_0
    move-object v3, v1

    .line 8
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result v4

    .line 12
    if-ge v2, v4, :cond_5

    .line 13
    .line 14
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v4

    .line 18
    check-cast v4, Lcom/intsig/office/fc/dom4j/Node;

    .line 19
    .line 20
    instance-of v5, v4, Lcom/intsig/office/fc/dom4j/Text;

    .line 21
    .line 22
    if-eqz v5, :cond_3

    .line 23
    .line 24
    check-cast v4, Lcom/intsig/office/fc/dom4j/Text;

    .line 25
    .line 26
    if-eqz v3, :cond_0

    .line 27
    .line 28
    invoke-interface {v4}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v5

    .line 32
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/CharacterData;->appendText(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0, v4}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->remove(Lcom/intsig/office/fc/dom4j/Text;)Z

    .line 36
    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_0
    invoke-interface {v4}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v5

    .line 43
    if-eqz v5, :cond_2

    .line 44
    .line 45
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 46
    .line 47
    .line 48
    move-result v5

    .line 49
    if-gtz v5, :cond_1

    .line 50
    .line 51
    goto :goto_2

    .line 52
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 53
    .line 54
    move-object v3, v4

    .line 55
    goto :goto_1

    .line 56
    :cond_2
    :goto_2
    invoke-virtual {p0, v4}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->remove(Lcom/intsig/office/fc/dom4j/Text;)Z

    .line 57
    .line 58
    .line 59
    goto :goto_1

    .line 60
    :cond_3
    instance-of v3, v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 61
    .line 62
    if-eqz v3, :cond_4

    .line 63
    .line 64
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 65
    .line 66
    invoke-interface {v4}, Lcom/intsig/office/fc/dom4j/Branch;->normalize()V

    .line 67
    .line 68
    .line 69
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_5
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public processingInstruction(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/ProcessingInstruction;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x0

    .line 10
    :goto_0
    if-ge v2, v1, :cond_1

    .line 11
    .line 12
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    instance-of v4, v3, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    .line 17
    .line 18
    if-eqz v4, :cond_0

    .line 19
    .line 20
    check-cast v3, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    .line 21
    .line 22
    invoke-interface {v3}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v4

    .line 26
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    move-result v4

    .line 30
    if-eqz v4, :cond_0

    .line 31
    .line 32
    return-object v3

    .line 33
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    const/4 p1, 0x0

    .line 37
    return-object p1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public processingInstructions()Ljava/util/List;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    move-result-object v0

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createResultList()Lcom/intsig/office/fc/dom4j/tree/BackedList;

    move-result-object v1

    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 4
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 5
    instance-of v5, v4, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    if-eqz v5, :cond_0

    .line 6
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/dom4j/tree/BackedList;->addLocal(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public processingInstructions(Ljava/lang/String;)Ljava/util/List;
    .locals 6

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    move-result-object v0

    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createResultList()Lcom/intsig/office/fc/dom4j/tree/BackedList;

    move-result-object v1

    .line 9
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 10
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 11
    instance-of v5, v4, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    if-eqz v5, :cond_0

    .line 12
    check-cast v4, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    .line 13
    invoke-interface {v4}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 14
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/dom4j/tree/BackedList;->addLocal(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public remove(Lcom/intsig/office/fc/dom4j/Attribute;)Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attributeList()Ljava/util/List;

    move-result-object v0

    .line 2
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->childRemoved(Lcom/intsig/office/fc/dom4j/Node;)V

    goto :goto_0

    .line 4
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Attribute;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attribute(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 5
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    :cond_1
    :goto_0
    return v1
.end method

.method public remove(Lcom/intsig/office/fc/dom4j/CDATA;)Z
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->removeNode(Lcom/intsig/office/fc/dom4j/Node;)Z

    move-result p1

    return p1
.end method

.method public remove(Lcom/intsig/office/fc/dom4j/Comment;)Z
    .locals 0

    .line 16
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->removeNode(Lcom/intsig/office/fc/dom4j/Node;)Z

    move-result p1

    return p1
.end method

.method public remove(Lcom/intsig/office/fc/dom4j/Element;)Z
    .locals 0

    .line 17
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->removeNode(Lcom/intsig/office/fc/dom4j/Node;)Z

    move-result p1

    return p1
.end method

.method public remove(Lcom/intsig/office/fc/dom4j/Entity;)Z
    .locals 0

    .line 18
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->removeNode(Lcom/intsig/office/fc/dom4j/Node;)Z

    move-result p1

    return p1
.end method

.method public remove(Lcom/intsig/office/fc/dom4j/Namespace;)Z
    .locals 0

    .line 19
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->removeNode(Lcom/intsig/office/fc/dom4j/Node;)Z

    move-result p1

    return p1
.end method

.method public remove(Lcom/intsig/office/fc/dom4j/Node;)Z
    .locals 2

    .line 6
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getNodeType()S

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_7

    const/4 v1, 0x2

    if-eq v0, v1, :cond_6

    const/4 v1, 0x3

    if-eq v0, v1, :cond_5

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 7
    :cond_0
    check-cast p1, Lcom/intsig/office/fc/dom4j/Namespace;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->remove(Lcom/intsig/office/fc/dom4j/Namespace;)Z

    move-result p1

    return p1

    .line 8
    :cond_1
    check-cast p1, Lcom/intsig/office/fc/dom4j/Comment;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->remove(Lcom/intsig/office/fc/dom4j/Comment;)Z

    move-result p1

    return p1

    .line 9
    :cond_2
    check-cast p1, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->remove(Lcom/intsig/office/fc/dom4j/ProcessingInstruction;)Z

    move-result p1

    return p1

    .line 10
    :cond_3
    check-cast p1, Lcom/intsig/office/fc/dom4j/Entity;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->remove(Lcom/intsig/office/fc/dom4j/Entity;)Z

    move-result p1

    return p1

    .line 11
    :cond_4
    check-cast p1, Lcom/intsig/office/fc/dom4j/CDATA;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->remove(Lcom/intsig/office/fc/dom4j/CDATA;)Z

    move-result p1

    return p1

    .line 12
    :cond_5
    check-cast p1, Lcom/intsig/office/fc/dom4j/Text;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->remove(Lcom/intsig/office/fc/dom4j/Text;)Z

    move-result p1

    return p1

    .line 13
    :cond_6
    check-cast p1, Lcom/intsig/office/fc/dom4j/Attribute;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->remove(Lcom/intsig/office/fc/dom4j/Attribute;)Z

    move-result p1

    return p1

    .line 14
    :cond_7
    check-cast p1, Lcom/intsig/office/fc/dom4j/Element;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->remove(Lcom/intsig/office/fc/dom4j/Element;)Z

    move-result p1

    return p1
.end method

.method public remove(Lcom/intsig/office/fc/dom4j/ProcessingInstruction;)Z
    .locals 0

    .line 20
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->removeNode(Lcom/intsig/office/fc/dom4j/Node;)Z

    move-result p1

    return p1
.end method

.method public remove(Lcom/intsig/office/fc/dom4j/Text;)Z
    .locals 0

    .line 21
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->removeNode(Lcom/intsig/office/fc/dom4j/Node;)Z

    move-result p1

    return p1
.end method

.method protected removeNode(Lcom/intsig/office/fc/dom4j/Node;)Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->childRemoved(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public removeProcessingInstruction(Ljava/lang/String;)Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_1

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    instance-of v2, v1, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    .line 20
    .line 21
    if-eqz v2, :cond_0

    .line 22
    .line 23
    check-cast v1, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    .line 24
    .line 25
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    if-eqz v1, :cond_0

    .line 34
    .line 35
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 36
    .line 37
    .line 38
    const/4 p1, 0x1

    .line 39
    return p1

    .line 40
    :cond_1
    const/4 p1, 0x0

    .line 41
    return p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setAttributeValue(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 2
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addAttribute(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    return-void
.end method

.method public setAttributeValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addAttribute(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    return-void
.end method

.method public setAttributes(Lorg/xml/sax/Attributes;Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;Z)V
    .locals 9

    .line 1
    invoke-interface {p1}, Lorg/xml/sax/Attributes;->getLength()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-lez v0, :cond_4

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const-string v2, "xmlns"

    .line 12
    .line 13
    const/4 v3, 0x1

    .line 14
    const/4 v4, 0x0

    .line 15
    if-ne v0, v3, :cond_1

    .line 16
    .line 17
    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getQName(I)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    if-nez p3, :cond_0

    .line 22
    .line 23
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 24
    .line 25
    .line 26
    move-result p3

    .line 27
    if-nez p3, :cond_4

    .line 28
    .line 29
    :cond_0
    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getURI(I)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p3

    .line 33
    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getLocalName(I)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-virtual {p2, p3, v2, v0}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->getAttributeQName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    .line 44
    move-result-object p2

    .line 45
    invoke-virtual {v1, p0, p2, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->add(Lcom/intsig/office/fc/dom4j/Attribute;)V

    .line 50
    .line 51
    .line 52
    goto :goto_1

    .line 53
    :cond_1
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attributeList(I)Ljava/util/List;

    .line 54
    .line 55
    .line 56
    move-result-object v3

    .line 57
    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 58
    .line 59
    .line 60
    :goto_0
    if-ge v4, v0, :cond_4

    .line 61
    .line 62
    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getQName(I)Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v5

    .line 66
    if-nez p3, :cond_2

    .line 67
    .line 68
    invoke-virtual {v5, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 69
    .line 70
    .line 71
    move-result v6

    .line 72
    if-nez v6, :cond_3

    .line 73
    .line 74
    :cond_2
    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getURI(I)Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v6

    .line 78
    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getLocalName(I)Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v7

    .line 82
    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v8

    .line 86
    invoke-virtual {p2, v6, v7, v5}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->getAttributeQName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    .line 87
    .line 88
    .line 89
    move-result-object v5

    .line 90
    invoke-virtual {v1, p0, v5, v8}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 91
    .line 92
    .line 93
    move-result-object v5

    .line 94
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    .line 96
    .line 97
    invoke-virtual {p0, v5}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->childAdded(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 98
    .line 99
    .line 100
    :cond_3
    add-int/lit8 v4, v4, 0x1

    .line 101
    .line 102
    goto :goto_0

    .line 103
    :cond_4
    :goto_1
    return-void
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public setData(Ljava/lang/Object;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-interface {p0, p1}, Lcom/intsig/office/fc/dom4j/Element;->setQName(Lcom/intsig/office/fc/dom4j/QName;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setNamespace(Lcom/intsig/office/fc/dom4j/Namespace;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getName()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    invoke-interface {p0, p1}, Lcom/intsig/office/fc/dom4j/Element;->setQName(Lcom/intsig/office/fc/dom4j/QName;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setText(Ljava/lang/String;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->contentList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    check-cast v1, Lcom/intsig/office/fc/dom4j/Node;

    .line 22
    .line 23
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getNodeType()S

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    const/4 v2, 0x3

    .line 28
    if-eq v1, v2, :cond_0

    .line 29
    .line 30
    const/4 v2, 0x4

    .line 31
    if-eq v1, v2, :cond_0

    .line 32
    .line 33
    const/4 v2, 0x5

    .line 34
    if-eq v1, v2, :cond_0

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getNamespaceURI()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "/>]"

    .line 6
    .line 7
    const-string v2, " attributes: "

    .line 8
    .line 9
    const-string v3, " [Element: <"

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 14
    .line 15
    .line 16
    move-result v4

    .line 17
    if-lez v4, :cond_0

    .line 18
    .line 19
    new-instance v4, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v5

    .line 28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getQualifiedName()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v3

    .line 38
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    const-string v3, " uri: "

    .line 42
    .line 43
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attributeList()Ljava/util/List;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    return-object v0

    .line 67
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 68
    .line 69
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 70
    .line 71
    .line 72
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v4

    .line 76
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getQualifiedName()Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v3

    .line 86
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->attributeList()Ljava/util/List;

    .line 93
    .line 94
    .line 95
    move-result-object v2

    .line 96
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    return-object v0
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public write(Ljava/io/Writer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 4
    .line 5
    invoke-direct {v1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;-><init>()V

    .line 6
    .line 7
    .line 8
    invoke-direct {v0, p1, v1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>(Ljava/io/Writer;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->write(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

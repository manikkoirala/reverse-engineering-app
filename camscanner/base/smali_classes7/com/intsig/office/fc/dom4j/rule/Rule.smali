.class public Lcom/intsig/office/fc/dom4j/rule/Rule;
.super Ljava/lang/Object;
.source "Rule.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private action:Lcom/intsig/office/fc/dom4j/rule/Action;

.field private appearenceCount:I

.field private importPrecedence:I

.field private mode:Ljava/lang/String;

.field private pattern:Lcom/intsig/office/fc/dom4j/rule/Pattern;

.field private priority:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    .line 2
    iput-wide v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->priority:D

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/rule/Pattern;)V
    .locals 2

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->pattern:Lcom/intsig/office/fc/dom4j/rule/Pattern;

    .line 5
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/rule/Pattern;->getPriority()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->priority:D

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/rule/Pattern;Lcom/intsig/office/fc/dom4j/rule/Action;)V
    .locals 0

    .line 6
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/dom4j/rule/Rule;-><init>(Lcom/intsig/office/fc/dom4j/rule/Pattern;)V

    .line 7
    iput-object p2, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->action:Lcom/intsig/office/fc/dom4j/rule/Action;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/rule/Rule;Lcom/intsig/office/fc/dom4j/rule/Pattern;)V
    .locals 2

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iget-object v0, p1, Lcom/intsig/office/fc/dom4j/rule/Rule;->mode:Ljava/lang/String;

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->mode:Ljava/lang/String;

    .line 10
    iget v0, p1, Lcom/intsig/office/fc/dom4j/rule/Rule;->importPrecedence:I

    iput v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->importPrecedence:I

    .line 11
    iget-wide v0, p1, Lcom/intsig/office/fc/dom4j/rule/Rule;->priority:D

    iput-wide v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->priority:D

    .line 12
    iget v0, p1, Lcom/intsig/office/fc/dom4j/rule/Rule;->appearenceCount:I

    iput v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->appearenceCount:I

    .line 13
    iget-object p1, p1, Lcom/intsig/office/fc/dom4j/rule/Rule;->action:Lcom/intsig/office/fc/dom4j/rule/Action;

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->action:Lcom/intsig/office/fc/dom4j/rule/Action;

    .line 14
    iput-object p2, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->pattern:Lcom/intsig/office/fc/dom4j/rule/Pattern;

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/intsig/office/fc/dom4j/rule/Rule;)I
    .locals 4

    .line 4
    iget v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->importPrecedence:I

    iget v1, p1, Lcom/intsig/office/fc/dom4j/rule/Rule;->importPrecedence:I

    sub-int/2addr v0, v1

    if-nez v0, :cond_0

    .line 5
    iget-wide v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->priority:D

    iget-wide v2, p1, Lcom/intsig/office/fc/dom4j/rule/Rule;->priority:D

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    if-nez v0, :cond_0

    .line 6
    iget v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->appearenceCount:I

    iget p1, p1, Lcom/intsig/office/fc/dom4j/rule/Rule;->appearenceCount:I

    sub-int/2addr v0, p1

    :cond_0
    return v0
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/rule/Rule;

    if-eqz v0, :cond_0

    .line 2
    check-cast p1, Lcom/intsig/office/fc/dom4j/rule/Rule;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/rule/Rule;->compareTo(Lcom/intsig/office/fc/dom4j/rule/Rule;)I

    move-result p1

    return p1

    .line 3
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/rule/Rule;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    check-cast p1, Lcom/intsig/office/fc/dom4j/rule/Rule;

    .line 7
    .line 8
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/rule/Rule;->compareTo(Lcom/intsig/office/fc/dom4j/rule/Rule;)I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    if-nez p1, :cond_0

    .line 13
    .line 14
    const/4 v1, 0x1

    .line 15
    :cond_0
    return v1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getAction()Lcom/intsig/office/fc/dom4j/rule/Action;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->action:Lcom/intsig/office/fc/dom4j/rule/Action;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAppearenceCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->appearenceCount:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getImportPrecedence()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->importPrecedence:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMatchType()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->pattern:Lcom/intsig/office/fc/dom4j/rule/Pattern;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/rule/Pattern;->getMatchType()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMatchesNodeName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->pattern:Lcom/intsig/office/fc/dom4j/rule/Pattern;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/rule/Pattern;->getMatchesNodeName()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMode()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->mode:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPattern()Lcom/intsig/office/fc/dom4j/rule/Pattern;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->pattern:Lcom/intsig/office/fc/dom4j/rule/Pattern;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPriority()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->priority:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getUnionRules()[Lcom/intsig/office/fc/dom4j/rule/Rule;
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->pattern:Lcom/intsig/office/fc/dom4j/rule/Pattern;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/rule/Pattern;->getUnionPatterns()[Lcom/intsig/office/fc/dom4j/rule/Pattern;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    array-length v1, v0

    .line 12
    new-array v2, v1, [Lcom/intsig/office/fc/dom4j/rule/Rule;

    .line 13
    .line 14
    const/4 v3, 0x0

    .line 15
    :goto_0
    if-ge v3, v1, :cond_1

    .line 16
    .line 17
    new-instance v4, Lcom/intsig/office/fc/dom4j/rule/Rule;

    .line 18
    .line 19
    aget-object v5, v0, v3

    .line 20
    .line 21
    invoke-direct {v4, p0, v5}, Lcom/intsig/office/fc/dom4j/rule/Rule;-><init>(Lcom/intsig/office/fc/dom4j/rule/Rule;Lcom/intsig/office/fc/dom4j/rule/Pattern;)V

    .line 22
    .line 23
    .line 24
    aput-object v4, v2, v3

    .line 25
    .line 26
    add-int/lit8 v3, v3, 0x1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    return-object v2
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->importPrecedence:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->appearenceCount:I

    .line 4
    .line 5
    add-int/2addr v0, v1

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final matches(Lcom/intsig/office/fc/dom4j/Node;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->pattern:Lcom/intsig/office/fc/dom4j/rule/Pattern;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/dom4j/rule/Pattern;->matches(Lcom/intsig/office/fc/dom4j/Node;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setAction(Lcom/intsig/office/fc/dom4j/rule/Action;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->action:Lcom/intsig/office/fc/dom4j/rule/Action;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setAppearenceCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->appearenceCount:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setImportPrecedence(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->importPrecedence:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMode(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->mode:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPattern(Lcom/intsig/office/fc/dom4j/rule/Pattern;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->pattern:Lcom/intsig/office/fc/dom4j/rule/Pattern;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPriority(D)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/office/fc/dom4j/rule/Rule;->priority:D

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const-string v1, "[ pattern: "

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/rule/Rule;->getPattern()Lcom/intsig/office/fc/dom4j/rule/Pattern;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const-string v1, " action: "

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/rule/Rule;->getAction()Lcom/intsig/office/fc/dom4j/rule/Action;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    const-string v1, " ]"

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    return-object v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

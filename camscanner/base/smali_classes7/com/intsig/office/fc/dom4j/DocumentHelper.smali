.class public final Lcom/intsig/office/fc/dom4j/DocumentHelper;
.super Ljava/lang/Object;
.source "DocumentHelper.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static createAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object p0

    return-object p0
.end method

.method public static createAttribute(Lcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;
    .locals 1

    .line 2
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createAttribute(Lcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object p0

    return-object p0
.end method

.method public static createCDATA(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/CDATA;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createCDATA(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/CDATA;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static createComment(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Comment;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createComment(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Comment;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static createDocument()Lcom/intsig/office/fc/dom4j/Document;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createDocument()Lcom/intsig/office/fc/dom4j/Document;

    move-result-object v0

    return-object v0
.end method

.method public static createDocument(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1

    .line 2
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createDocument(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p0

    return-object p0
.end method

.method public static createElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p0

    return-object p0
.end method

.method public static createElement(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 2
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createElement(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p0

    return-object p0
.end method

.method public static createEntity(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Entity;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createEntity(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Entity;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static createNamespace(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createNamespace(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static createProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/ProcessingInstruction;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    move-result-object p0

    return-object p0
.end method

.method public static createProcessingInstruction(Ljava/lang/String;Ljava/util/Map;)Lcom/intsig/office/fc/dom4j/ProcessingInstruction;
    .locals 1

    .line 2
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createProcessingInstruction(Ljava/lang/String;Ljava/util/Map;)Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    move-result-object p0

    return-object p0
.end method

.method public static createQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 2
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p0

    return-object p0
.end method

.method public static createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p0

    return-object p0
.end method

.method public static createText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Text;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Text;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static createXPath(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/XPath;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/InvalidXPathException;
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createXPath(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/XPath;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static createXPathFilter(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/NodeFilter;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createXPathFilter(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/NodeFilter;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static getEncoding(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const-string v0, "<?xml"

    .line 6
    .line 7
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    const-string v0, "?>"

    .line 14
    .line 15
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    const/4 v1, 0x0

    .line 20
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p0

    .line 24
    new-instance v0, Ljava/util/StringTokenizer;

    .line 25
    .line 26
    const-string v1, " =\"\'"

    .line 27
    .line 28
    invoke-direct {v0, p0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    :cond_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    .line 32
    .line 33
    .line 34
    move-result p0

    .line 35
    if-eqz p0, :cond_1

    .line 36
    .line 37
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p0

    .line 41
    const-string v1, "encoding"

    .line 42
    .line 43
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    move-result p0

    .line 47
    if-eqz p0, :cond_0

    .line 48
    .line 49
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    .line 50
    .line 51
    .line 52
    move-result p0

    .line 53
    if-eqz p0, :cond_1

    .line 54
    .line 55
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p0

    .line 59
    goto :goto_0

    .line 60
    :cond_1
    const/4 p0, 0x0

    .line 61
    :goto_0
    return-object p0
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static makeElement(Lcom/intsig/office/fc/dom4j/Branch;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 2

    .line 1
    new-instance v0, Ljava/util/StringTokenizer;

    .line 2
    .line 3
    const-string v1, "/"

    .line 4
    .line 5
    invoke-direct {v0, p1, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    instance-of p1, p0, Lcom/intsig/office/fc/dom4j/Document;

    .line 9
    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    check-cast p0, Lcom/intsig/office/fc/dom4j/Document;

    .line 13
    .line 14
    invoke-interface {p0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    if-nez p1, :cond_1

    .line 23
    .line 24
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    goto :goto_0

    .line 29
    :cond_0
    move-object p1, p0

    .line 30
    check-cast p1, Lcom/intsig/office/fc/dom4j/Element;

    .line 31
    .line 32
    :cond_1
    :goto_0
    const/4 p0, 0x0

    .line 33
    :goto_1
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    if-eqz v1, :cond_4

    .line 38
    .line 39
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object p0

    .line 43
    const/16 v1, 0x3a

    .line 44
    .line 45
    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    if-lez v1, :cond_2

    .line 50
    .line 51
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/dom4j/Element;->getQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    goto :goto_2

    .line 60
    :cond_2
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    :goto_2
    if-nez v1, :cond_3

    .line 65
    .line 66
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    move-result-object p0

    .line 70
    move-object p1, p0

    .line 71
    goto :goto_3

    .line 72
    :cond_3
    move-object p1, v1

    .line 73
    :goto_3
    move-object p0, p1

    .line 74
    goto :goto_1

    .line 75
    :cond_4
    return-object p0
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static parseText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-static {p0}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->getEncoding(Ljava/lang/String;)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    new-instance v2, Lorg/xml/sax/InputSource;

    .line 11
    .line 12
    new-instance v3, Ljava/io/StringReader;

    .line 13
    .line 14
    invoke-direct {v3, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {v2, v3}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v2, v1}, Lorg/xml/sax/InputSource;->setEncoding(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Lorg/xml/sax/InputSource;)Lcom/intsig/office/fc/dom4j/Document;

    .line 24
    .line 25
    .line 26
    move-result-object p0

    .line 27
    invoke-interface {p0}, Lcom/intsig/office/fc/dom4j/Document;->getXMLEncoding()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    if-nez v0, :cond_0

    .line 32
    .line 33
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Document;->setXMLEncoding(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    :cond_0
    return-object p0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static selectNodes(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Node;)Ljava/util/List;
    .locals 0

    .line 3
    invoke-static {p0}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->createXPath(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/XPath;

    move-result-object p0

    .line 4
    invoke-interface {p0, p1}, Lcom/intsig/office/fc/dom4j/XPath;->selectNodes(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static selectNodes(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->createXPath(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/XPath;

    move-result-object p0

    .line 2
    invoke-interface {p0, p1}, Lcom/intsig/office/fc/dom4j/XPath;->selectNodes(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static sort(Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->createXPath(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/XPath;

    move-result-object p1

    .line 2
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/dom4j/XPath;->sort(Ljava/util/List;)V

    return-void
.end method

.method public static sort(Ljava/util/List;Ljava/lang/String;Z)V
    .locals 0

    .line 3
    invoke-static {p1}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->createXPath(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/XPath;

    move-result-object p1

    .line 4
    invoke-interface {p1, p0, p2}, Lcom/intsig/office/fc/dom4j/XPath;->sort(Ljava/util/List;Z)V

    return-void
.end method

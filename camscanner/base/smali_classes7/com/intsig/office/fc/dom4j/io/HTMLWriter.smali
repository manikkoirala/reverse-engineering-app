.class public Lcom/intsig/office/fc/dom4j/io/HTMLWriter;
.super Lcom/intsig/office/fc/dom4j/io/XMLWriter;
.source "HTMLWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/dom4j/io/HTMLWriter$FormatState;
    }
.end annotation


# static fields
.field protected static final DEFAULT_HTML_FORMAT:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

.field protected static final DEFAULT_PREFORMATTED_TAGS:Ljava/util/HashSet;

.field private static lineSeparator:Ljava/lang/String;


# instance fields
.field private formatStack:Ljava/util/Stack;

.field private lastText:Ljava/lang/String;

.field private newLineAfterNTags:I

.field private omitElementCloseSet:Ljava/util/HashSet;

.field private preformattedTags:Ljava/util/HashSet;

.field private tagsOuput:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    const-string v0, "line.separator"

    .line 2
    .line 3
    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->lineSeparator:Ljava/lang/String;

    .line 8
    .line 9
    new-instance v0, Ljava/util/HashSet;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 12
    .line 13
    .line 14
    sput-object v0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->DEFAULT_PREFORMATTED_TAGS:Ljava/util/HashSet;

    .line 15
    .line 16
    const-string v1, "PRE"

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    const-string v1, "SCRIPT"

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    const-string v1, "STYLE"

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    const-string v1, "TEXTAREA"

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 37
    .line 38
    const-string v1, "  "

    .line 39
    .line 40
    const/4 v2, 0x1

    .line 41
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;-><init>(Ljava/lang/String;Z)V

    .line 42
    .line 43
    .line 44
    sput-object v0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->DEFAULT_HTML_FORMAT:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 45
    .line 46
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->setTrimText(Z)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->setSuppressDeclaration(Z)V

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 13
    sget-object v0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->DEFAULT_HTML_FORMAT:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>(Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V

    .line 14
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->formatStack:Ljava/util/Stack;

    const-string v0, ""

    .line 15
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->lastText:Ljava/lang/String;

    const/4 v0, 0x0

    .line 16
    iput v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->tagsOuput:I

    const/4 v0, -0x1

    .line 17
    iput v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->newLineAfterNTags:I

    .line 18
    sget-object v0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->DEFAULT_PREFORMATTED_TAGS:Ljava/util/HashSet;

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->preformattedTags:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>(Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V

    .line 20
    new-instance p1, Ljava/util/Stack;

    invoke-direct {p1}, Ljava/util/Stack;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->formatStack:Ljava/util/Stack;

    const-string p1, ""

    .line 21
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->lastText:Ljava/lang/String;

    const/4 p1, 0x0

    .line 22
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->tagsOuput:I

    const/4 p1, -0x1

    .line 23
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->newLineAfterNTags:I

    .line 24
    sget-object p1, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->DEFAULT_PREFORMATTED_TAGS:Ljava/util/HashSet;

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->preformattedTags:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 25
    sget-object v0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->DEFAULT_HTML_FORMAT:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>(Ljava/io/OutputStream;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V

    .line 26
    new-instance p1, Ljava/util/Stack;

    invoke-direct {p1}, Ljava/util/Stack;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->formatStack:Ljava/util/Stack;

    const-string p1, ""

    .line 27
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->lastText:Ljava/lang/String;

    const/4 p1, 0x0

    .line 28
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->tagsOuput:I

    const/4 p1, -0x1

    .line 29
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->newLineAfterNTags:I

    .line 30
    sget-object p1, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->DEFAULT_PREFORMATTED_TAGS:Ljava/util/HashSet;

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->preformattedTags:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>(Ljava/io/OutputStream;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V

    .line 32
    new-instance p1, Ljava/util/Stack;

    invoke-direct {p1}, Ljava/util/Stack;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->formatStack:Ljava/util/Stack;

    const-string p1, ""

    .line 33
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->lastText:Ljava/lang/String;

    const/4 p1, 0x0

    .line 34
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->tagsOuput:I

    const/4 p1, -0x1

    .line 35
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->newLineAfterNTags:I

    .line 36
    sget-object p1, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->DEFAULT_PREFORMATTED_TAGS:Ljava/util/HashSet;

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->preformattedTags:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->DEFAULT_HTML_FORMAT:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>(Ljava/io/Writer;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V

    .line 2
    new-instance p1, Ljava/util/Stack;

    invoke-direct {p1}, Ljava/util/Stack;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->formatStack:Ljava/util/Stack;

    const-string p1, ""

    .line 3
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->lastText:Ljava/lang/String;

    const/4 p1, 0x0

    .line 4
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->tagsOuput:I

    const/4 p1, -0x1

    .line 5
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->newLineAfterNTags:I

    .line 6
    sget-object p1, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->DEFAULT_PREFORMATTED_TAGS:Ljava/util/HashSet;

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->preformattedTags:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V
    .locals 0

    .line 7
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>(Ljava/io/Writer;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V

    .line 8
    new-instance p1, Ljava/util/Stack;

    invoke-direct {p1}, Ljava/util/Stack;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->formatStack:Ljava/util/Stack;

    const-string p1, ""

    .line 9
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->lastText:Ljava/lang/String;

    const/4 p1, 0x0

    .line 10
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->tagsOuput:I

    const/4 p1, -0x1

    .line 11
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->newLineAfterNTags:I

    .line 12
    sget-object p1, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->DEFAULT_PREFORMATTED_TAGS:Ljava/util/HashSet;

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->preformattedTags:Ljava/util/HashSet;

    return-void
.end method

.method private internalGetOmitElementCloseSet()Ljava/util/HashSet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->omitElementCloseSet:Ljava/util/HashSet;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/HashSet;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->omitElementCloseSet:Ljava/util/HashSet;

    .line 11
    .line 12
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->loadOmitElementCloseSet(Ljava/util/Set;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->omitElementCloseSet:Ljava/util/HashSet;

    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private justSpaces(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Ljava/lang/StringBuffer;

    .line 6
    .line 7
    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    :goto_0
    if-ge v2, v0, :cond_1

    .line 12
    .line 13
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    .line 14
    .line 15
    .line 16
    move-result v3

    .line 17
    const/16 v4, 0xa

    .line 18
    .line 19
    if-eq v3, v4, :cond_0

    .line 20
    .line 21
    const/16 v4, 0xd

    .line 22
    .line 23
    if-eq v3, v4, :cond_0

    .line 24
    .line 25
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 26
    .line 27
    .line 28
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    return-object p1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private lazyInitNewLinesAfterNTags()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->getOutputFormat()Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isNewlines()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->newLineAfterNTags:I

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->getOutputFormat()Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getNewLineAfterNTags()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    iput v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->newLineAfterNTags:I

    .line 24
    .line 25
    :goto_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static prettyPrintHTML(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/io/UnsupportedEncodingException;,
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1
    invoke-static {p0, v0, v0, v1, v0}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->prettyPrintHTML(Ljava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static prettyPrintHTML(Ljava/lang/String;ZZZZ)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/io/UnsupportedEncodingException;,
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 2
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 3
    invoke-static {}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->createPrettyPrint()Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    move-result-object v1

    .line 4
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->setNewlines(Z)V

    .line 5
    invoke-virtual {v1, p2}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->setTrimText(Z)V

    .line 6
    invoke-virtual {v1, p3}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->setXHTML(Z)V

    .line 7
    invoke-virtual {v1, p4}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->setExpandEmptyElements(Z)V

    .line 8
    new-instance p1, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;

    invoke-direct {p1, v0, v1}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;-><init>(Ljava/io/Writer;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V

    .line 9
    invoke-static {p0}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->parseText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p0

    .line 10
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->write(Lcom/intsig/office/fc/dom4j/Document;)V

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->flush()V

    .line 12
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static prettyPrintXHTML(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/io/UnsupportedEncodingException;,
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x1

    .line 2
    const/4 v1, 0x0

    .line 3
    invoke-static {p0, v0, v0, v0, v1}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->prettyPrintHTML(Ljava/lang/String;ZZZZ)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    return-object p0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public endCDATA()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOmitElementCloseSet()Ljava/util/Set;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->internalGetOmitElementCloseSet()Ljava/util/HashSet;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/util/HashSet;->clone()Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Ljava/util/Set;

    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPreformattedTags()Ljava/util/Set;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->preformattedTags:Ljava/util/HashSet;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/HashSet;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/util/Set;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isPreformattedTag(Ljava/lang/String;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->preformattedTags:Ljava/util/HashSet;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    const/4 p1, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 p1, 0x0

    .line 18
    :goto_0
    return p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected loadOmitElementCloseSet(Ljava/util/Set;)V
    .locals 1

    .line 1
    const-string v0, "AREA"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    const-string v0, "BASE"

    .line 7
    .line 8
    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    const-string v0, "BR"

    .line 12
    .line 13
    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    const-string v0, "COL"

    .line 17
    .line 18
    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    const-string v0, "HR"

    .line 22
    .line 23
    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    const-string v0, "IMG"

    .line 27
    .line 28
    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    const-string v0, "INPUT"

    .line 32
    .line 33
    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    const-string v0, "LINK"

    .line 37
    .line 38
    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    const-string v0, "META"

    .line 42
    .line 43
    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    const-string v0, "P"

    .line 47
    .line 48
    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 49
    .line 50
    .line 51
    const-string v0, "PARAM"

    .line 52
    .line 53
    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected omitElementClose(Ljava/lang/String;)Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->internalGetOmitElementCloseSet()Ljava/util/HashSet;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOmitElementCloseSet(Ljava/util/Set;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/HashSet;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->omitElementCloseSet:Ljava/util/HashSet;

    .line 7
    .line 8
    if-eqz p1, :cond_1

    .line 9
    .line 10
    new-instance v0, Ljava/util/HashSet;

    .line 11
    .line 12
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->omitElementCloseSet:Ljava/util/HashSet;

    .line 16
    .line 17
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    if-eqz v0, :cond_0

    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->omitElementCloseSet:Ljava/util/HashSet;

    .line 34
    .line 35
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_1
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setPreformattedTags(Ljava/util/Set;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/HashSet;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->preformattedTags:Ljava/util/HashSet;

    .line 7
    .line 8
    if-eqz p1, :cond_1

    .line 9
    .line 10
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    if-eqz v0, :cond_0

    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->preformattedTags:Ljava/util/HashSet;

    .line 27
    .line 28
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public startCDATA()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected writeCDATA(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->getOutputFormat()Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isXHTML()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-super {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeCDATA(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 16
    .line 17
    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :goto_0
    const/4 p1, 0x4

    .line 21
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 22
    .line 23
    return-void
    .line 24
.end method

.method protected writeClose(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->omitElementClose(Ljava/lang/String;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-super {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeClose(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected writeDeclaration()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected writeElement(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->newLineAfterNTags:I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->lazyInitNewLinesAfterNTags()V

    .line 7
    .line 8
    .line 9
    :cond_0
    iget v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->newLineAfterNTags:I

    .line 10
    .line 11
    if-lez v0, :cond_1

    .line 12
    .line 13
    iget v1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->tagsOuput:I

    .line 14
    .line 15
    if-lez v1, :cond_1

    .line 16
    .line 17
    rem-int/2addr v1, v0

    .line 18
    if-nez v1, :cond_1

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 21
    .line 22
    sget-object v1, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->lineSeparator:Ljava/lang/String;

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    :cond_1
    iget v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->tagsOuput:I

    .line 28
    .line 29
    add-int/lit8 v0, v0, 0x1

    .line 30
    .line 31
    iput v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->tagsOuput:I

    .line 32
    .line 33
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getQualifiedName()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->lastText:Ljava/lang/String;

    .line 38
    .line 39
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Branch;->nodeCount()I

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->isPreformattedTag(Ljava/lang/String;)Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-eqz v0, :cond_3

    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->getOutputFormat()Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isNewlines()Z

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isTrimText()Z

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getIndent()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v4

    .line 64
    iget-object v5, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->formatStack:Ljava/util/Stack;

    .line 65
    .line 66
    new-instance v6, Lcom/intsig/office/fc/dom4j/io/HTMLWriter$FormatState;

    .line 67
    .line 68
    invoke-direct {v6, p0, v2, v3, v4}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter$FormatState;-><init>(Lcom/intsig/office/fc/dom4j/io/HTMLWriter;ZZLjava/lang/String;)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v5, v6}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    .line 73
    .line 74
    :try_start_0
    invoke-super {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writePrintln()V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 82
    .line 83
    .line 84
    move-result v2

    .line 85
    if-nez v2, :cond_2

    .line 86
    .line 87
    if-eqz v4, :cond_2

    .line 88
    .line 89
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 90
    .line 91
    .line 92
    move-result v2

    .line 93
    if-lez v2, :cond_2

    .line 94
    .line 95
    iget-object v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 96
    .line 97
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->justSpaces(Ljava/lang/String;)Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v1

    .line 101
    invoke-virtual {v2, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    :cond_2
    const/4 v1, 0x0

    .line 105
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->setNewlines(Z)V

    .line 106
    .line 107
    .line 108
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->setTrimText(Z)V

    .line 109
    .line 110
    .line 111
    const-string v1, ""

    .line 112
    .line 113
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->setIndent(Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    invoke-super {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeElement(Lcom/intsig/office/fc/dom4j/Element;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    .line 118
    .line 119
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->formatStack:Ljava/util/Stack;

    .line 120
    .line 121
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 122
    .line 123
    .line 124
    move-result-object p1

    .line 125
    check-cast p1, Lcom/intsig/office/fc/dom4j/io/HTMLWriter$FormatState;

    .line 126
    .line 127
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter$FormatState;->〇o00〇〇Oo()Z

    .line 128
    .line 129
    .line 130
    move-result v1

    .line 131
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->setNewlines(Z)V

    .line 132
    .line 133
    .line 134
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter$FormatState;->〇o〇()Z

    .line 135
    .line 136
    .line 137
    move-result v1

    .line 138
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->setTrimText(Z)V

    .line 139
    .line 140
    .line 141
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter$FormatState;->〇080()Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object p1

    .line 145
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->setIndent(Ljava/lang/String;)V

    .line 146
    .line 147
    .line 148
    goto :goto_0

    .line 149
    :catchall_0
    move-exception p1

    .line 150
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->formatStack:Ljava/util/Stack;

    .line 151
    .line 152
    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 153
    .line 154
    .line 155
    move-result-object v1

    .line 156
    check-cast v1, Lcom/intsig/office/fc/dom4j/io/HTMLWriter$FormatState;

    .line 157
    .line 158
    invoke-virtual {v1}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter$FormatState;->〇o00〇〇Oo()Z

    .line 159
    .line 160
    .line 161
    move-result v2

    .line 162
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->setNewlines(Z)V

    .line 163
    .line 164
    .line 165
    invoke-virtual {v1}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter$FormatState;->〇o〇()Z

    .line 166
    .line 167
    .line 168
    move-result v2

    .line 169
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->setTrimText(Z)V

    .line 170
    .line 171
    .line 172
    invoke-virtual {v1}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter$FormatState;->〇080()Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object v1

    .line 176
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->setIndent(Ljava/lang/String;)V

    .line 177
    .line 178
    .line 179
    throw p1

    .line 180
    :cond_3
    invoke-super {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeElement(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 181
    .line 182
    .line 183
    :goto_0
    return-void
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method protected writeEmptyElementClose(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->getOutputFormat()Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isXHTML()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->omitElementClose(Ljava/lang/String;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 18
    .line 19
    const-string v0, " />"

    .line 20
    .line 21
    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-super {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeEmptyElementClose(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->omitElementClose(Ljava/lang/String;)Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-eqz v0, :cond_2

    .line 34
    .line 35
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 36
    .line 37
    const-string v0, ">"

    .line 38
    .line 39
    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_2
    invoke-super {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeEmptyElementClose(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    :goto_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected writeEntity(Lcom/intsig/office/fc/dom4j/Entity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 p1, 0x5

    .line 11
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected writeString(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const-string v0, "\n"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->formatStack:Ljava/util/Stack;

    .line 10
    .line 11
    invoke-virtual {p1}, Ljava/util/Stack;->empty()Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-nez p1, :cond_0

    .line 16
    .line 17
    sget-object p1, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->lineSeparator:Ljava/lang/String;

    .line 18
    .line 19
    invoke-super {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeString(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void

    .line 23
    :cond_1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->lastText:Ljava/lang/String;

    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/HTMLWriter;->formatStack:Ljava/util/Stack;

    .line 26
    .line 27
    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-eqz v0, :cond_2

    .line 32
    .line 33
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-super {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeString(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_2
    invoke-super {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeString(Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    :goto_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

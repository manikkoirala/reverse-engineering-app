.class public interface abstract Lcom/intsig/office/fc/dom4j/Attribute;
.super Ljava/lang/Object;
.source "Attribute.java"

# interfaces
.implements Lcom/intsig/office/fc/dom4j/Node;


# virtual methods
.method public abstract getData()Ljava/lang/Object;
.end method

.method public abstract getNamespace()Lcom/intsig/office/fc/dom4j/Namespace;
.end method

.method public abstract getNamespacePrefix()Ljava/lang/String;
.end method

.method public abstract getNamespaceURI()Ljava/lang/String;
.end method

.method public abstract getQName()Lcom/intsig/office/fc/dom4j/QName;
.end method

.method public abstract getQualifiedName()Ljava/lang/String;
.end method

.method public abstract getValue()Ljava/lang/String;
.end method

.method public abstract setData(Ljava/lang/Object;)V
.end method

.method public abstract setNamespace(Lcom/intsig/office/fc/dom4j/Namespace;)V
.end method

.method public abstract setValue(Ljava/lang/String;)V
.end method

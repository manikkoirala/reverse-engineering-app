.class public Lcom/intsig/office/fc/dom4j/rule/RuleManager;
.super Ljava/lang/Object;
.source "RuleManager.java"


# instance fields
.field private appearenceCount:I

.field private modes:Ljava/util/HashMap;

.field private valueOfAction:Lcom/intsig/office/fc/dom4j/rule/Action;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/HashMap;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->modes:Ljava/util/HashMap;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method protected addDefaultRule(Lcom/intsig/office/fc/dom4j/rule/Mode;Lcom/intsig/office/fc/dom4j/rule/Pattern;Lcom/intsig/office/fc/dom4j/rule/Action;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->createDefaultRule(Lcom/intsig/office/fc/dom4j/rule/Pattern;Lcom/intsig/office/fc/dom4j/rule/Action;)Lcom/intsig/office/fc/dom4j/rule/Rule;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/dom4j/rule/Mode;->addRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method protected addDefaultRules(Lcom/intsig/office/fc/dom4j/rule/Mode;)V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/rule/RuleManager$1;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/fc/dom4j/rule/RuleManager$1;-><init>(Lcom/intsig/office/fc/dom4j/rule/RuleManager;Lcom/intsig/office/fc/dom4j/rule/Mode;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->getValueOfAction()Lcom/intsig/office/fc/dom4j/rule/Action;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    sget-object v2, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;->ANY_DOCUMENT:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 11
    .line 12
    invoke-virtual {p0, p1, v2, v0}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->addDefaultRule(Lcom/intsig/office/fc/dom4j/rule/Mode;Lcom/intsig/office/fc/dom4j/rule/Pattern;Lcom/intsig/office/fc/dom4j/rule/Action;)V

    .line 13
    .line 14
    .line 15
    sget-object v2, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;->ANY_ELEMENT:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 16
    .line 17
    invoke-virtual {p0, p1, v2, v0}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->addDefaultRule(Lcom/intsig/office/fc/dom4j/rule/Mode;Lcom/intsig/office/fc/dom4j/rule/Pattern;Lcom/intsig/office/fc/dom4j/rule/Action;)V

    .line 18
    .line 19
    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    sget-object v0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;->ANY_ATTRIBUTE:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 23
    .line 24
    invoke-virtual {p0, p1, v0, v1}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->addDefaultRule(Lcom/intsig/office/fc/dom4j/rule/Mode;Lcom/intsig/office/fc/dom4j/rule/Pattern;Lcom/intsig/office/fc/dom4j/rule/Action;)V

    .line 25
    .line 26
    .line 27
    sget-object v0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;->ANY_TEXT:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 28
    .line 29
    invoke-virtual {p0, p1, v0, v1}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->addDefaultRule(Lcom/intsig/office/fc/dom4j/rule/Mode;Lcom/intsig/office/fc/dom4j/rule/Pattern;Lcom/intsig/office/fc/dom4j/rule/Action;)V

    .line 30
    .line 31
    .line 32
    :cond_0
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public addRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->appearenceCount:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    iput v0, p0, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->appearenceCount:I

    .line 6
    .line 7
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/dom4j/rule/Rule;->setAppearenceCount(I)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/rule/Rule;->getMode()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->getMode(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/rule/Mode;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/rule/Rule;->getUnionRules()[Lcom/intsig/office/fc/dom4j/rule/Rule;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    if-eqz v1, :cond_0

    .line 23
    .line 24
    array-length p1, v1

    .line 25
    const/4 v2, 0x0

    .line 26
    :goto_0
    if-ge v2, p1, :cond_1

    .line 27
    .line 28
    aget-object v3, v1, v2

    .line 29
    .line 30
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/dom4j/rule/Mode;->addRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V

    .line 31
    .line 32
    .line 33
    add-int/lit8 v2, v2, 0x1

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_0
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/rule/Mode;->addRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V

    .line 37
    .line 38
    .line 39
    :cond_1
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public clear()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->modes:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    iput v0, p0, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->appearenceCount:I

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected createDefaultRule(Lcom/intsig/office/fc/dom4j/rule/Pattern;Lcom/intsig/office/fc/dom4j/rule/Action;)Lcom/intsig/office/fc/dom4j/rule/Rule;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/rule/Rule;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/rule/Rule;-><init>(Lcom/intsig/office/fc/dom4j/rule/Pattern;Lcom/intsig/office/fc/dom4j/rule/Action;)V

    .line 4
    .line 5
    .line 6
    const/4 p1, -0x1

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/rule/Rule;->setImportPrecedence(I)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected createMode()Lcom/intsig/office/fc/dom4j/rule/Mode;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/rule/Mode;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/rule/Mode;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->addDefaultRules(Lcom/intsig/office/fc/dom4j/rule/Mode;)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMatchingRule(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Node;)Lcom/intsig/office/fc/dom4j/rule/Rule;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->modes:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/office/fc/dom4j/rule/Mode;

    .line 8
    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/dom4j/rule/Mode;->getMatchingRule(Lcom/intsig/office/fc/dom4j/Node;)Lcom/intsig/office/fc/dom4j/rule/Rule;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1

    .line 16
    :cond_0
    sget-object p2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 17
    .line 18
    new-instance v0, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v1, "Warning: No Mode for mode: "

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-virtual {p2, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    const/4 p1, 0x0

    .line 39
    return-object p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public getMode(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/rule/Mode;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->modes:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/office/fc/dom4j/rule/Mode;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->createMode()Lcom/intsig/office/fc/dom4j/rule/Mode;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->modes:Ljava/util/HashMap;

    .line 16
    .line 17
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    :cond_0
    return-object v0
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getValueOfAction()Lcom/intsig/office/fc/dom4j/rule/Action;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->valueOfAction:Lcom/intsig/office/fc/dom4j/rule/Action;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/rule/Rule;->getMode()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->getMode(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/rule/Mode;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/rule/Rule;->getUnionRules()[Lcom/intsig/office/fc/dom4j/rule/Rule;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    array-length p1, v1

    .line 16
    const/4 v2, 0x0

    .line 17
    :goto_0
    if-ge v2, p1, :cond_1

    .line 18
    .line 19
    aget-object v3, v1, v2

    .line 20
    .line 21
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/dom4j/rule/Mode;->removeRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V

    .line 22
    .line 23
    .line 24
    add-int/lit8 v2, v2, 0x1

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/rule/Mode;->removeRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V

    .line 28
    .line 29
    .line 30
    :cond_1
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setValueOfAction(Lcom/intsig/office/fc/dom4j/rule/Action;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->valueOfAction:Lcom/intsig/office/fc/dom4j/rule/Action;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

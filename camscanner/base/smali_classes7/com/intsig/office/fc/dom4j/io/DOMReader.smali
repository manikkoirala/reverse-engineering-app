.class public Lcom/intsig/office/fc/dom4j/io/DOMReader;
.super Ljava/lang/Object;
.source "DOMReader.java"


# instance fields
.field private factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

.field private namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 3
    new-instance v1, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-direct {v1, v0}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;-><init>(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V

    iput-object v1, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V
    .locals 1

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 6
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;-><init>(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    return-void
.end method

.method private getPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 1
    const/16 v0, 0x3a

    .line 2
    .line 3
    const/4 v1, 0x5

    .line 4
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->indexOf(II)I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    const/4 v1, -0x1

    .line 9
    if-eq v0, v1, :cond_0

    .line 10
    .line 11
    add-int/lit8 v0, v0, 0x1

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    return-object p1

    .line 18
    :cond_0
    const-string p1, ""

    .line 19
    .line 20
    return-object p1
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method protected clearNamespaceStack()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 7
    .line 8
    sget-object v1, Lcom/intsig/office/fc/dom4j/Namespace;->XML_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->contains(Lcom/intsig/office/fc/dom4j/Namespace;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->push(Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    return-void
.end method

.method protected createDocument()Lcom/intsig/office/fc/dom4j/Document;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/DOMReader;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createDocument()Lcom/intsig/office/fc/dom4j/Document;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getNamespace(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/DOMReader;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createNamespace(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public read(Lorg/w3c/dom/Document;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 4

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/Document;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/fc/dom4j/Document;

    .line 6
    .line 7
    return-object p1

    .line 8
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/DOMReader;->createDocument()Lcom/intsig/office/fc/dom4j/Document;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/DOMReader;->clearNamespaceStack()V

    .line 13
    .line 14
    .line 15
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    const/4 v2, 0x0

    .line 24
    :goto_0
    if-ge v2, v1, :cond_1

    .line 25
    .line 26
    invoke-interface {p1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    invoke-virtual {p0, v3, v0}, Lcom/intsig/office/fc/dom4j/io/DOMReader;->readTree(Lorg/w3c/dom/Node;Lcom/intsig/office/fc/dom4j/Branch;)V

    .line 31
    .line 32
    .line 33
    add-int/lit8 v2, v2, 0x1

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected readElement(Lorg/w3c/dom/Node;Lcom/intsig/office/fc/dom4j/Branch;)V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNamespaceURI()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getPrefix()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    const-string v3, "xmlns"

    .line 19
    .line 20
    if-eqz v2, :cond_0

    .line 21
    .line 22
    if-nez v1, :cond_0

    .line 23
    .line 24
    invoke-interface {v2, v3}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    .line 25
    .line 26
    .line 27
    move-result-object v4

    .line 28
    if-eqz v4, :cond_0

    .line 29
    .line 30
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    :cond_0
    iget-object v4, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 35
    .line 36
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getLocalName()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v5

    .line 40
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v6

    .line 44
    invoke-virtual {v4, v1, v5, v6}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->getQName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-interface {p2, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 49
    .line 50
    .line 51
    move-result-object p2

    .line 52
    const/4 v1, 0x0

    .line 53
    if-eqz v2, :cond_3

    .line 54
    .line 55
    invoke-interface {v2}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    .line 56
    .line 57
    .line 58
    move-result v4

    .line 59
    new-instance v5, Ljava/util/ArrayList;

    .line 60
    .line 61
    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 62
    .line 63
    .line 64
    const/4 v6, 0x0

    .line 65
    :goto_0
    if-ge v6, v4, :cond_2

    .line 66
    .line 67
    invoke-interface {v2, v6}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    .line 68
    .line 69
    .line 70
    move-result-object v7

    .line 71
    invoke-interface {v7}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v8

    .line 75
    invoke-virtual {v8, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 76
    .line 77
    .line 78
    move-result v9

    .line 79
    if-eqz v9, :cond_1

    .line 80
    .line 81
    invoke-direct {p0, v8}, Lcom/intsig/office/fc/dom4j/io/DOMReader;->getPrefix(Ljava/lang/String;)Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v8

    .line 85
    invoke-interface {v7}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v7

    .line 89
    iget-object v9, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 90
    .line 91
    invoke-virtual {v9, v8, v7}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->addNamespace(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    .line 92
    .line 93
    .line 94
    move-result-object v7

    .line 95
    invoke-interface {p2, v7}, Lcom/intsig/office/fc/dom4j/Element;->add(Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 96
    .line 97
    .line 98
    goto :goto_1

    .line 99
    :cond_1
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    .line 101
    .line 102
    :goto_1
    add-int/lit8 v6, v6, 0x1

    .line 103
    .line 104
    goto :goto_0

    .line 105
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    .line 106
    .line 107
    .line 108
    move-result v2

    .line 109
    const/4 v3, 0x0

    .line 110
    :goto_2
    if-ge v3, v2, :cond_3

    .line 111
    .line 112
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 113
    .line 114
    .line 115
    move-result-object v4

    .line 116
    check-cast v4, Lorg/w3c/dom/Node;

    .line 117
    .line 118
    iget-object v6, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 119
    .line 120
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNamespaceURI()Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v7

    .line 124
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getLocalName()Ljava/lang/String;

    .line 125
    .line 126
    .line 127
    move-result-object v8

    .line 128
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v9

    .line 132
    invoke-virtual {v6, v7, v8, v9}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->getQName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    .line 133
    .line 134
    .line 135
    move-result-object v6

    .line 136
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v4

    .line 140
    invoke-interface {p2, v6, v4}, Lcom/intsig/office/fc/dom4j/Element;->addAttribute(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 141
    .line 142
    .line 143
    add-int/lit8 v3, v3, 0x1

    .line 144
    .line 145
    goto :goto_2

    .line 146
    :cond_3
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    .line 147
    .line 148
    .line 149
    move-result-object p1

    .line 150
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    .line 151
    .line 152
    .line 153
    move-result v2

    .line 154
    :goto_3
    if-ge v1, v2, :cond_4

    .line 155
    .line 156
    invoke-interface {p1, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    .line 157
    .line 158
    .line 159
    move-result-object v3

    .line 160
    invoke-virtual {p0, v3, p2}, Lcom/intsig/office/fc/dom4j/io/DOMReader;->readTree(Lorg/w3c/dom/Node;Lcom/intsig/office/fc/dom4j/Branch;)V

    .line 161
    .line 162
    .line 163
    add-int/lit8 v1, v1, 0x1

    .line 164
    .line 165
    goto :goto_3

    .line 166
    :cond_4
    :goto_4
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 167
    .line 168
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->size()I

    .line 169
    .line 170
    .line 171
    move-result p1

    .line 172
    if-le p1, v0, :cond_5

    .line 173
    .line 174
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 175
    .line 176
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->pop()Lcom/intsig/office/fc/dom4j/Namespace;

    .line 177
    .line 178
    .line 179
    goto :goto_4

    .line 180
    :cond_5
    return-void
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method protected readTree(Lorg/w3c/dom/Node;Lcom/intsig/office/fc/dom4j/Branch;)V
    .locals 5

    .line 1
    instance-of v0, p2, Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    move-object v2, p2

    .line 7
    check-cast v2, Lcom/intsig/office/fc/dom4j/Element;

    .line 8
    .line 9
    move-object v4, v2

    .line 10
    move-object v2, v1

    .line 11
    move-object v1, v4

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object v2, p2

    .line 14
    check-cast v2, Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    :goto_0
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeType()S

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    packed-switch v3, :pswitch_data_0

    .line 21
    .line 22
    .line 23
    :pswitch_0
    goto/16 :goto_1

    .line 24
    .line 25
    :pswitch_1
    check-cast p1, Lorg/w3c/dom/DocumentType;

    .line 26
    .line 27
    invoke-interface {p1}, Lorg/w3c/dom/DocumentType;->getName()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    invoke-interface {p1}, Lorg/w3c/dom/DocumentType;->getPublicId()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-interface {p1}, Lorg/w3c/dom/DocumentType;->getSystemId()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-interface {v2, p2, v0, p1}, Lcom/intsig/office/fc/dom4j/Document;->addDocType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;

    .line 40
    .line 41
    .line 42
    goto/16 :goto_1

    .line 43
    .line 44
    :pswitch_2
    if-eqz v0, :cond_1

    .line 45
    .line 46
    check-cast p2, Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    invoke-interface {p2, p1}, Lcom/intsig/office/fc/dom4j/Element;->addComment(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 53
    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_1
    check-cast p2, Lcom/intsig/office/fc/dom4j/Document;

    .line 57
    .line 58
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    invoke-interface {p2, p1}, Lcom/intsig/office/fc/dom4j/Document;->addComment(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;

    .line 63
    .line 64
    .line 65
    goto :goto_1

    .line 66
    :pswitch_3
    if-eqz v0, :cond_2

    .line 67
    .line 68
    check-cast p2, Lcom/intsig/office/fc/dom4j/Element;

    .line 69
    .line 70
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object p1

    .line 78
    invoke-interface {p2, v0, p1}, Lcom/intsig/office/fc/dom4j/Element;->addProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 79
    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_2
    check-cast p2, Lcom/intsig/office/fc/dom4j/Document;

    .line 83
    .line 84
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    invoke-interface {p2, v0, p1}, Lcom/intsig/office/fc/dom4j/Document;->addProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;

    .line 93
    .line 94
    .line 95
    goto :goto_1

    .line 96
    :pswitch_4
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object p2

    .line 100
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    invoke-interface {v1, p2, p1}, Lcom/intsig/office/fc/dom4j/Element;->addEntity(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 105
    .line 106
    .line 107
    goto :goto_1

    .line 108
    :pswitch_5
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    .line 109
    .line 110
    .line 111
    move-result-object p2

    .line 112
    if-eqz p2, :cond_3

    .line 113
    .line 114
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object p1

    .line 118
    invoke-interface {p2}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object p2

    .line 122
    invoke-interface {v1, p1, p2}, Lcom/intsig/office/fc/dom4j/Element;->addEntity(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 123
    .line 124
    .line 125
    goto :goto_1

    .line 126
    :cond_3
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    .line 127
    .line 128
    .line 129
    move-result-object p1

    .line 130
    const-string p2, ""

    .line 131
    .line 132
    invoke-interface {v1, p1, p2}, Lcom/intsig/office/fc/dom4j/Element;->addEntity(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 133
    .line 134
    .line 135
    goto :goto_1

    .line 136
    :pswitch_6
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object p1

    .line 140
    invoke-interface {v1, p1}, Lcom/intsig/office/fc/dom4j/Element;->addCDATA(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 141
    .line 142
    .line 143
    goto :goto_1

    .line 144
    :pswitch_7
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object p1

    .line 148
    invoke-interface {v1, p1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 149
    .line 150
    .line 151
    goto :goto_1

    .line 152
    :pswitch_8
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/io/DOMReader;->readElement(Lorg/w3c/dom/Node;Lcom/intsig/office/fc/dom4j/Branch;)V

    .line 153
    .line 154
    .line 155
    :goto_1
    return-void

    .line 156
    nop

    .line 157
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public setDocumentFactory(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DOMReader;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->setDocumentFactory(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

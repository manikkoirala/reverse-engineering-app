.class public Lcom/intsig/office/fc/dom4j/io/XPPReader;
.super Ljava/lang/Object;
.source "XPPReader.java"


# instance fields
.field private dispatchHandler:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

.field private factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XPPReader;->factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    return-void
.end method


# virtual methods
.method public addHandler(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XPPReader;->getDispatchHandler()Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;->〇080(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected createReader(Ljava/io/InputStream;)Ljava/io/Reader;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/io/BufferedReader;

    .line 2
    .line 3
    new-instance v1, Ljava/io/InputStreamReader;

    .line 4
    .line 5
    invoke-direct {v1, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 9
    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected getDispatchHandler()Lcom/intsig/office/fc/dom4j/io/DispatchHandler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XPPReader;->dispatchHandler:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XPPReader;->dispatchHandler:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XPPReader;->dispatchHandler:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XPPReader;->factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XPPReader;->factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XPPReader;->factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 12
    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeHandler(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XPPReader;->getDispatchHandler()Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;->Oo08(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/ElementHandler;

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDefaultHandler(Lcom/intsig/office/fc/dom4j/ElementHandler;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XPPReader;->getDispatchHandler()Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;->〇〇888(Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected setDispatchHandler(Lcom/intsig/office/fc/dom4j/io/DispatchHandler;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XPPReader;->dispatchHandler:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDocumentFactory(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XPPReader;->factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

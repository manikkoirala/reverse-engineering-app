.class public interface abstract Lcom/intsig/office/fc/dom4j/Document;
.super Ljava/lang/Object;
.source "Document.java"

# interfaces
.implements Lcom/intsig/office/fc/dom4j/Branch;


# virtual methods
.method public abstract addComment(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
.end method

.method public abstract addDocType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
.end method

.method public abstract addProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
.end method

.method public abstract addProcessingInstruction(Ljava/lang/String;Ljava/util/Map;)Lcom/intsig/office/fc/dom4j/Document;
.end method

.method public abstract getDocType()Lcom/intsig/office/fc/dom4j/DocumentType;
.end method

.method public abstract getEntityResolver()Lorg/xml/sax/EntityResolver;
.end method

.method public abstract getRootElement()Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract getXMLEncoding()Ljava/lang/String;
.end method

.method public abstract setDocType(Lcom/intsig/office/fc/dom4j/DocumentType;)V
.end method

.method public abstract setEntityResolver(Lorg/xml/sax/EntityResolver;)V
.end method

.method public abstract setRootElement(Lcom/intsig/office/fc/dom4j/Element;)V
.end method

.method public abstract setXMLEncoding(Ljava/lang/String;)V
.end method

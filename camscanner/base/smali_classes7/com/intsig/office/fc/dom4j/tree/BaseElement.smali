.class public Lcom/intsig/office/fc/dom4j/tree/BaseElement;
.super Lcom/intsig/office/fc/dom4j/tree/AbstractElement;
.source "BaseElement.java"


# instance fields
.field protected attributes:Ljava/util/List;

.field protected content:Ljava/util/List;

.field private parentBranch:Lcom/intsig/office/fc/dom4j/Branch;

.field private qname:Lcom/intsig/office/fc/dom4j/QName;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/dom4j/QName;)V
    .locals 0

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->qname:Lcom/intsig/office/fc/dom4j/QName;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;-><init>()V

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->qname:Lcom/intsig/office/fc/dom4j/QName;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V
    .locals 1

    .line 5
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;-><init>()V

    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->qname:Lcom/intsig/office/fc/dom4j/QName;

    return-void
.end method


# virtual methods
.method protected attributeList()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->attributes:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->createAttributeList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->attributes:Ljava/util/List;

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->attributes:Ljava/util/List;

    return-object v0
.end method

.method protected attributeList(I)Ljava/util/List;
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->attributes:Ljava/util/List;

    if-nez v0, :cond_0

    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->createAttributeList(I)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->attributes:Ljava/util/List;

    .line 6
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->attributes:Ljava/util/List;

    return-object p1
.end method

.method public clearContent()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->contentList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected contentList()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->content:Ljava/util/List;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createContentList()Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->content:Ljava/util/List;

    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->content:Ljava/util/List;

    .line 12
    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDocument()Lcom/intsig/office/fc/dom4j/Document;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->parentBranch:Lcom/intsig/office/fc/dom4j/Branch;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/office/fc/dom4j/Document;

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    check-cast v0, Lcom/intsig/office/fc/dom4j/Document;

    .line 8
    .line 9
    return-object v0

    .line 10
    :cond_0
    instance-of v1, v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 11
    .line 12
    if-eqz v1, :cond_1

    .line 13
    .line 14
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Node;->getDocument()Lcom/intsig/office/fc/dom4j/Document;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    return-object v0

    .line 21
    :cond_1
    const/4 v0, 0x0

    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getParent()Lcom/intsig/office/fc/dom4j/Element;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->parentBranch:Lcom/intsig/office/fc/dom4j/Branch;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getQName()Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->qname:Lcom/intsig/office/fc/dom4j/QName;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected setAttributeList(Ljava/util/List;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->attributes:Ljava/util/List;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setAttributes(Ljava/util/List;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->attributes:Ljava/util/List;

    .line 2
    .line 3
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/tree/ContentListFacade;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    check-cast p1, Lcom/intsig/office/fc/dom4j/tree/ContentListFacade;

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/tree/ContentListFacade;->getBackingList()Ljava/util/List;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->attributes:Ljava/util/List;

    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setContent(Ljava/util/List;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->content:Ljava/util/List;

    .line 2
    .line 3
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/tree/ContentListFacade;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    check-cast p1, Lcom/intsig/office/fc/dom4j/tree/ContentListFacade;

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/tree/ContentListFacade;->getBackingList()Ljava/util/List;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->content:Ljava/util/List;

    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDocument(Lcom/intsig/office/fc/dom4j/Document;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->parentBranch:Lcom/intsig/office/fc/dom4j/Branch;

    .line 2
    .line 3
    instance-of v0, v0, Lcom/intsig/office/fc/dom4j/Document;

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    if-eqz p1, :cond_1

    .line 8
    .line 9
    :cond_0
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->parentBranch:Lcom/intsig/office/fc/dom4j/Branch;

    .line 10
    .line 11
    :cond_1
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setParent(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->parentBranch:Lcom/intsig/office/fc/dom4j/Branch;

    .line 2
    .line 3
    instance-of v0, v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    if-eqz p1, :cond_1

    .line 8
    .line 9
    :cond_0
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->parentBranch:Lcom/intsig/office/fc/dom4j/Branch;

    .line 10
    .line 11
    :cond_1
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setQName(Lcom/intsig/office/fc/dom4j/QName;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/BaseElement;->qname:Lcom/intsig/office/fc/dom4j/QName;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public supportsParent()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

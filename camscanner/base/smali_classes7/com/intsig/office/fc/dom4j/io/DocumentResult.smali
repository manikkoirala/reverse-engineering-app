.class public Lcom/intsig/office/fc/dom4j/io/DocumentResult;
.super Ljavax/xml/transform/sax/SAXResult;
.source "DocumentResult.java"


# instance fields
.field private contentHandler:Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;

    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;-><init>()V

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/dom4j/io/DocumentResult;-><init>(Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljavax/xml/transform/sax/SAXResult;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DocumentResult;->contentHandler:Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;

    .line 4
    invoke-super {p0, p1}, Ljavax/xml/transform/sax/SAXResult;->setHandler(Lorg/xml/sax/ContentHandler;)V

    .line 5
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DocumentResult;->contentHandler:Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;

    invoke-super {p0, p1}, Ljavax/xml/transform/sax/SAXResult;->setLexicalHandler(Lorg/xml/sax/ext/LexicalHandler;)V

    return-void
.end method


# virtual methods
.method public getDocument()Lcom/intsig/office/fc/dom4j/Document;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DocumentResult;->contentHandler:Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;->getDocument()Lcom/intsig/office/fc/dom4j/Document;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setHandler(Lorg/xml/sax/ContentHandler;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;

    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DocumentResult;->contentHandler:Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;

    .line 8
    .line 9
    invoke-super {p0, p1}, Ljavax/xml/transform/sax/SAXResult;->setHandler(Lorg/xml/sax/ContentHandler;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLexicalHandler(Lorg/xml/sax/ext/LexicalHandler;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;

    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DocumentResult;->contentHandler:Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;

    .line 8
    .line 9
    invoke-super {p0, p1}, Ljavax/xml/transform/sax/SAXResult;->setLexicalHandler(Lorg/xml/sax/ext/LexicalHandler;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public interface abstract Lcom/intsig/office/fc/dom4j/ElementPath;
.super Ljava/lang/Object;
.source "ElementPath.java"


# virtual methods
.method public abstract addHandler(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V
.end method

.method public abstract getCurrent()Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract getElement(I)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract getPath()Ljava/lang/String;
.end method

.method public abstract removeHandler(Ljava/lang/String;)V
.end method

.method public abstract size()I
.end method

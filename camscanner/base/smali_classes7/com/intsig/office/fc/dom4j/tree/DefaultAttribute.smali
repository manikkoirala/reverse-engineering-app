.class public Lcom/intsig/office/fc/dom4j/tree/DefaultAttribute;
.super Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;
.source "DefaultAttribute.java"


# instance fields
.field private parent:Lcom/intsig/office/fc/dom4j/Element;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)V
    .locals 0

    .line 3
    invoke-direct {p0, p2, p3}, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;-><init>(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)V

    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultAttribute;->parent:Lcom/intsig/office/fc/dom4j/Element;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V
    .locals 0

    .line 7
    invoke-direct {p0, p2, p3, p4}, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 8
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultAttribute;->parent:Lcom/intsig/office/fc/dom4j/Element;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/QName;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;-><init>(Lcom/intsig/office/fc/dom4j/QName;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;-><init>(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 5
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V
    .locals 0

    .line 6
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    return-void
.end method


# virtual methods
.method public getParent()Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultAttribute;->parent:Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isReadOnly()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setParent(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/DefaultAttribute;->parent:Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;->value:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public supportsParent()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

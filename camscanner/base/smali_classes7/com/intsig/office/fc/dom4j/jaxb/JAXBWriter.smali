.class public Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;
.super Lcom/intsig/office/fc/dom4j/jaxb/JAXBSupport;
.source "JAXBWriter.java"


# instance fields
.field private outputFormat:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

.field private xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBSupport;-><init>(Ljava/lang/String;)V

    .line 2
    new-instance p1, Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    invoke-direct {p1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->outputFormat:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBSupport;-><init>(Ljava/lang/String;)V

    .line 4
    iput-object p2, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->outputFormat:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V
    .locals 0

    .line 5
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBSupport;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/ClassLoader;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V
    .locals 0

    .line 6
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBSupport;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 7
    iput-object p3, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->outputFormat:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    return-void
.end method

.method private getWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->outputFormat:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->outputFormat:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 12
    .line 13
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>(Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 20
    .line 21
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>()V

    .line 22
    .line 23
    .line 24
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 25
    .line 26
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 27
    .line 28
    return-object v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method


# virtual methods
.method public endDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->getWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->endDocument()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOutputFormat()Lcom/intsig/office/fc/dom4j/io/OutputFormat;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->outputFormat:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setOutput(Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->getWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    move-result-object v0

    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->setOutputStream(Ljava/io/OutputStream;)V

    return-void
.end method

.method public setOutput(Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->getWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->setOutputStream(Ljava/io/OutputStream;)V

    return-void
.end method

.method public setOutput(Ljava/io/Writer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->getWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->setWriter(Ljava/io/Writer;)V

    return-void
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->getWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->startDocument()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public writeCloseElement(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->getWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeClose(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public writeElement(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->getWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->write(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public writeOpenElement(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBWriter;->getWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeOpen(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;
.super Ljava/util/AbstractMap;
.source "ConcurrentReaderHashMap.java"

# interfaces
.implements Ljava/util/Map;
.implements Ljava/lang/Cloneable;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$ValueIterator;,
        Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$KeyIterator;,
        Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;,
        Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;,
        Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$EntrySet;,
        Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Values;,
        Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$KeySet;,
        Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$BarrierLock;
    }
.end annotation


# static fields
.field public static oOo0:I = 0x20


# instance fields
.field protected O8o08O8O:F

.field protected transient OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

.field protected final o0:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$BarrierLock;

.field protected transient oOo〇8o008:Ljava/util/Collection;

.field protected o〇00O:I

.field protected transient 〇080OO8〇0:Ljava/util/Set;

.field protected transient 〇08O〇00〇o:I

.field protected transient 〇0O:Ljava/util/Set;

.field protected transient 〇OOo8〇0:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 2

    .line 11
    sget v0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->oOo0:I

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-direct {p0, v0, v1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;-><init>(IF)V

    return-void
.end method

.method public constructor <init>(IF)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 2
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$BarrierLock;

    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$BarrierLock;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->o0:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$BarrierLock;

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇080OO8〇0:Ljava/util/Set;

    .line 4
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇0O:Ljava/util/Set;

    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->oOo〇8o008:Ljava/util/Collection;

    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-lez v0, :cond_0

    .line 6
    iput p2, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->O8o08O8O:F

    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇〇888(I)I

    move-result p1

    .line 8
    new-array v0, p1, [Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    int-to-float p1, p1

    mul-float p1, p1, p2

    float-to-int p1, p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->o〇00O:I

    return-void

    .line 10
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal Load factor: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static o〇0(Ljava/lang/Object;)I
    .locals 2

    .line 1
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    shl-int/lit8 v0, p0, 0x7

    .line 6
    .line 7
    sub-int/2addr v0, p0

    .line 8
    ushr-int/lit8 v1, p0, 0x9

    .line 9
    .line 10
    add-int/2addr v0, v1

    .line 11
    ushr-int/lit8 p0, p0, 0x11

    .line 12
    .line 13
    add-int/2addr v0, p0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private declared-synchronized readObject(Ljava/io/ObjectInputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 3
    .line 4
    .line 5
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    new-array v0, v0, [Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 12
    .line 13
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, 0x0

    .line 18
    :goto_0
    if-ge v1, v0, :cond_0

    .line 19
    .line 20
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    invoke-virtual {p0, v2, v3}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    .line 30
    .line 31
    add-int/lit8 v1, v1, 0x1

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    monitor-exit p0

    .line 35
    return-void

    .line 36
    :catchall_0
    move-exception p1

    .line 37
    monitor-exit p0

    .line 38
    throw p1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private declared-synchronized writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 3
    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 6
    .line 7
    array-length v0, v0

    .line 8
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 9
    .line 10
    .line 11
    iget v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇08O〇00〇o:I

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 17
    .line 18
    array-length v0, v0

    .line 19
    add-int/lit8 v0, v0, -0x1

    .line 20
    .line 21
    :goto_0
    if-ltz v0, :cond_1

    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 24
    .line 25
    aget-object v1, v1, v0

    .line 26
    .line 27
    :goto_1
    if-eqz v1, :cond_0

    .line 28
    .line 29
    iget-object v2, v1, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->key:Ljava/lang/Object;

    .line 30
    .line 31
    invoke-virtual {p1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 32
    .line 33
    .line 34
    iget-object v2, v1, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 35
    .line 36
    invoke-virtual {p1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 37
    .line 38
    .line 39
    iget-object v1, v1, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    .line 41
    goto :goto_1

    .line 42
    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    monitor-exit p0

    .line 46
    return-void

    .line 47
    :catchall_0
    move-exception p1

    .line 48
    monitor-exit p0

    .line 49
    throw p1
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private 〇〇888(I)I
    .locals 1

    .line 1
    const/high16 v0, 0x40000000    # 2.0f

    .line 2
    .line 3
    if-gt p1, v0, :cond_1

    .line 4
    .line 5
    if-gez p1, :cond_0

    .line 6
    .line 7
    goto :goto_1

    .line 8
    :cond_0
    const/4 v0, 0x4

    .line 9
    :goto_0
    if-ge v0, p1, :cond_1

    .line 10
    .line 11
    shl-int/lit8 v0, v0, 0x1

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_1
    :goto_1
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method protected final O8()[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->o0:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$BarrierLock;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 5
    .line 6
    monitor-exit v0

    .line 7
    return-object v1

    .line 8
    :catchall_0
    move-exception v1

    .line 9
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    throw v1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected OO0o〇〇〇〇0(Ljava/lang/Object;Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    add-int/lit8 v1, v1, -0x1

    .line 5
    .line 6
    and-int/2addr v1, p3

    .line 7
    aget-object v2, v0, v1

    .line 8
    .line 9
    move-object v3, v2

    .line 10
    :goto_0
    if-nez v3, :cond_1

    .line 11
    .line 12
    new-instance v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 13
    .line 14
    invoke-direct {v3, p3, p1, p2, v2}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;-><init>(ILjava/lang/Object;Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;)V

    .line 15
    .line 16
    .line 17
    aput-object v3, v0, v1

    .line 18
    .line 19
    iget p1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇08O〇00〇o:I

    .line 20
    .line 21
    add-int/lit8 p1, p1, 0x1

    .line 22
    .line 23
    iput p1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇08O〇00〇o:I

    .line 24
    .line 25
    iget p2, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->o〇00O:I

    .line 26
    .line 27
    if-lt p1, p2, :cond_0

    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇80〇808〇O()V

    .line 30
    .line 31
    .line 32
    goto :goto_1

    .line 33
    :cond_0
    invoke-virtual {p0, v3}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->oO80(Ljava/lang/Object;)V

    .line 34
    .line 35
    .line 36
    :goto_1
    const/4 p1, 0x0

    .line 37
    return-object p1

    .line 38
    :cond_1
    iget v4, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->hash:I

    .line 39
    .line 40
    if-ne v4, p3, :cond_2

    .line 41
    .line 42
    iget-object v4, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->key:Ljava/lang/Object;

    .line 43
    .line 44
    invoke-virtual {p0, p1, v4}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    if-eqz v4, :cond_2

    .line 49
    .line 50
    iget-object p1, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 51
    .line 52
    iput-object p2, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 53
    .line 54
    return-object p1

    .line 55
    :cond_2
    iget-object v3, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 56
    .line 57
    goto :goto_0
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public declared-synchronized clear()V
    .locals 5

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    const/4 v2, 0x0

    .line 6
    :goto_0
    array-length v3, v0

    .line 7
    if-ge v2, v3, :cond_1

    .line 8
    .line 9
    aget-object v3, v0, v2

    .line 10
    .line 11
    :goto_1
    const/4 v4, 0x0

    .line 12
    if-eqz v3, :cond_0

    .line 13
    .line 14
    iput-object v4, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 15
    .line 16
    iget-object v3, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_0
    aput-object v4, v0, v2

    .line 20
    .line 21
    add-int/lit8 v2, v2, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    iput v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇08O〇00〇o:I

    .line 25
    .line 26
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->oO80(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    .line 28
    .line 29
    monitor-exit p0

    .line 30
    return-void

    .line 31
    :catchall_0
    move-exception v0

    .line 32
    monitor-exit p0

    .line 33
    throw v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public declared-synchronized clone()Ljava/lang/Object;
    .locals 11

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-super {p0}, Ljava/util/AbstractMap;->clone()Ljava/lang/Object;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    check-cast v0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    iput-object v1, v0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇080OO8〇0:Ljava/util/Set;

    .line 10
    .line 11
    iput-object v1, v0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇0O:Ljava/util/Set;

    .line 12
    .line 13
    iput-object v1, v0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->oOo〇8o008:Ljava/util/Collection;

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 16
    .line 17
    array-length v3, v2

    .line 18
    new-array v3, v3, [Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 19
    .line 20
    iput-object v3, v0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 21
    .line 22
    const/4 v4, 0x0

    .line 23
    :goto_0
    array-length v5, v2

    .line 24
    if-ge v4, v5, :cond_1

    .line 25
    .line 26
    aget-object v5, v2, v4

    .line 27
    .line 28
    move-object v6, v1

    .line 29
    :goto_1
    if-eqz v5, :cond_0

    .line 30
    .line 31
    new-instance v7, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 32
    .line 33
    iget v8, v5, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->hash:I

    .line 34
    .line 35
    iget-object v9, v5, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->key:Ljava/lang/Object;

    .line 36
    .line 37
    iget-object v10, v5, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 38
    .line 39
    invoke-direct {v7, v8, v9, v10, v6}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;-><init>(ILjava/lang/Object;Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;)V

    .line 40
    .line 41
    .line 42
    iget-object v5, v5, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 43
    .line 44
    move-object v6, v7

    .line 45
    goto :goto_1

    .line 46
    :cond_0
    aput-object v6, v3, v4
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    .line 48
    add-int/lit8 v4, v4, 0x1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    monitor-exit p0

    .line 52
    return-object v0

    .line 53
    :catchall_0
    move-exception v0

    .line 54
    goto :goto_2

    .line 55
    :catch_0
    :try_start_1
    new-instance v0, Ljava/lang/InternalError;

    .line 56
    .line 57
    invoke-direct {v0}, Ljava/lang/InternalError;-><init>()V

    .line 58
    .line 59
    .line 60
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61
    :goto_2
    monitor-exit p0

    .line 62
    throw v0
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    const/4 p1, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 p1, 0x0

    .line 10
    :goto_0
    return p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 5

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->O8()[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    const/4 v1, 0x0

    .line 9
    const/4 v2, 0x0

    .line 10
    :goto_0
    array-length v3, v0

    .line 11
    if-ge v2, v3, :cond_2

    .line 12
    .line 13
    aget-object v3, v0, v2

    .line 14
    .line 15
    :goto_1
    if-eqz v3, :cond_1

    .line 16
    .line 17
    iget-object v4, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 18
    .line 19
    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    if-eqz v4, :cond_0

    .line 24
    .line 25
    const/4 p1, 0x1

    .line 26
    return p1

    .line 27
    :cond_0
    iget-object v3, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 28
    .line 29
    goto :goto_1

    .line 30
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_2
    return v1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public entrySet()Ljava/util/Set;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇0O:Ljava/util/Set;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$EntrySet;

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    invoke-direct {v0, p0, v1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$EntrySet;-><init>(Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;Lcom/intsig/office/fc/dom4j/tree/〇080;)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇0O:Ljava/util/Set;

    .line 13
    .line 14
    :goto_0
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->o〇0(Ljava/lang/Object;)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 6
    .line 7
    array-length v2, v1

    .line 8
    add-int/lit8 v2, v2, -0x1

    .line 9
    .line 10
    and-int/2addr v2, v0

    .line 11
    aget-object v3, v1, v2

    .line 12
    .line 13
    move-object v4, v3

    .line 14
    :goto_0
    if-nez v3, :cond_1

    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->O8()[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 17
    .line 18
    .line 19
    move-result-object v3

    .line 20
    if-ne v1, v3, :cond_0

    .line 21
    .line 22
    aget-object v1, v1, v2

    .line 23
    .line 24
    if-ne v4, v1, :cond_0

    .line 25
    .line 26
    const/4 p1, 0x0

    .line 27
    return-object p1

    .line 28
    :cond_0
    array-length v1, v3

    .line 29
    add-int/lit8 v1, v1, -0x1

    .line 30
    .line 31
    and-int v2, v0, v1

    .line 32
    .line 33
    aget-object v4, v3, v2

    .line 34
    .line 35
    move-object v1, v3

    .line 36
    :goto_1
    move-object v3, v4

    .line 37
    goto :goto_0

    .line 38
    :cond_1
    iget v5, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->hash:I

    .line 39
    .line 40
    if-ne v5, v0, :cond_3

    .line 41
    .line 42
    iget-object v5, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->key:Ljava/lang/Object;

    .line 43
    .line 44
    invoke-virtual {p0, p1, v5}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 45
    .line 46
    .line 47
    move-result v5

    .line 48
    if-eqz v5, :cond_3

    .line 49
    .line 50
    iget-object v1, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 51
    .line 52
    if-eqz v1, :cond_2

    .line 53
    .line 54
    return-object v1

    .line 55
    :cond_2
    monitor-enter p0

    .line 56
    :try_start_0
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 57
    .line 58
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    array-length v2, v1

    .line 60
    add-int/lit8 v2, v2, -0x1

    .line 61
    .line 62
    and-int/2addr v2, v0

    .line 63
    aget-object v4, v1, v2

    .line 64
    .line 65
    goto :goto_1

    .line 66
    :catchall_0
    move-exception p1

    .line 67
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    throw p1

    .line 69
    :cond_3
    iget-object v3, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 70
    .line 71
    goto :goto_0
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public declared-synchronized isEmpty()Z
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇08O〇00〇o:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3
    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    monitor-exit p0

    .line 10
    return v0

    .line 11
    :catchall_0
    move-exception v0

    .line 12
    monitor-exit p0

    .line 13
    throw v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public keySet()Ljava/util/Set;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇080OO8〇0:Ljava/util/Set;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$KeySet;

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    invoke-direct {v0, p0, v1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$KeySet;-><init>(Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;Lcom/intsig/office/fc/dom4j/tree/〇o00〇〇Oo;)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇080OO8〇0:Ljava/util/Set;

    .line 13
    .line 14
    :goto_0
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected final oO80(Ljava/lang/Object;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->o0:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$BarrierLock;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇OOo8〇0:Ljava/lang/Object;

    .line 5
    .line 6
    monitor-exit v0

    .line 7
    return-void

    .line 8
    :catchall_0
    move-exception p1

    .line 9
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    throw p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .line 1
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    .line 3
    .line 4
    invoke-static {p1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->o〇0(Ljava/lang/Object;)I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 9
    .line 10
    array-length v2, v1

    .line 11
    add-int/lit8 v2, v2, -0x1

    .line 12
    .line 13
    and-int/2addr v2, v0

    .line 14
    aget-object v3, v1, v2

    .line 15
    .line 16
    move-object v4, v3

    .line 17
    :goto_0
    if-eqz v4, :cond_1

    .line 18
    .line 19
    iget v5, v4, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->hash:I

    .line 20
    .line 21
    if-ne v5, v0, :cond_0

    .line 22
    .line 23
    iget-object v5, v4, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->key:Ljava/lang/Object;

    .line 24
    .line 25
    invoke-virtual {p0, p1, v5}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    move-result v5

    .line 29
    if-eqz v5, :cond_0

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_0
    iget-object v4, v4, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    :goto_1
    monitor-enter p0

    .line 36
    :try_start_0
    iget-object v5, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 37
    .line 38
    if-ne v1, v5, :cond_4

    .line 39
    .line 40
    if-nez v4, :cond_3

    .line 41
    .line 42
    aget-object v4, v1, v2

    .line 43
    .line 44
    if-ne v3, v4, :cond_4

    .line 45
    .line 46
    new-instance v4, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 47
    .line 48
    invoke-direct {v4, v0, p1, p2, v3}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;-><init>(ILjava/lang/Object;Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;)V

    .line 49
    .line 50
    .line 51
    aput-object v4, v1, v2

    .line 52
    .line 53
    iget p1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇08O〇00〇o:I

    .line 54
    .line 55
    add-int/lit8 p1, p1, 0x1

    .line 56
    .line 57
    iput p1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇08O〇00〇o:I

    .line 58
    .line 59
    iget p2, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->o〇00O:I

    .line 60
    .line 61
    if-lt p1, p2, :cond_2

    .line 62
    .line 63
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇80〇808〇O()V

    .line 64
    .line 65
    .line 66
    goto :goto_2

    .line 67
    :cond_2
    invoke-virtual {p0, v4}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->oO80(Ljava/lang/Object;)V

    .line 68
    .line 69
    .line 70
    :goto_2
    monitor-exit p0

    .line 71
    const/4 p1, 0x0

    .line 72
    return-object p1

    .line 73
    :cond_3
    iget-object v5, v4, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 74
    .line 75
    aget-object v1, v1, v2

    .line 76
    .line 77
    if-ne v3, v1, :cond_4

    .line 78
    .line 79
    if-eqz v5, :cond_4

    .line 80
    .line 81
    iput-object p2, v4, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 82
    .line 83
    monitor-exit p0

    .line 84
    return-object v5

    .line 85
    :cond_4
    invoke-virtual {p0, p1, p2, v0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO0o〇〇〇〇0(Ljava/lang/Object;Ljava/lang/Object;I)Ljava/lang/Object;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    monitor-exit p0

    .line 90
    return-object p1

    .line 91
    :catchall_0
    move-exception p1

    .line 92
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    throw p1
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public declared-synchronized putAll(Ljava/util/Map;)V
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-interface {p1}, Ljava/util/Map;->size()I

    .line 3
    .line 4
    .line 5
    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    monitor-exit p0

    .line 9
    return-void

    .line 10
    :cond_0
    :goto_0
    :try_start_1
    iget v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->o〇00O:I

    .line 11
    .line 12
    if-lt v0, v1, :cond_1

    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇80〇808〇O()V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eqz v0, :cond_2

    .line 31
    .line 32
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    check-cast v0, Ljava/util/Map$Entry;

    .line 37
    .line 38
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-virtual {p0, v1, v0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 47
    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_2
    monitor-exit p0

    .line 51
    return-void

    .line 52
    :catchall_0
    move-exception p1

    .line 53
    monitor-exit p0

    .line 54
    throw p1
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->o〇0(Ljava/lang/Object;)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 6
    .line 7
    array-length v2, v1

    .line 8
    add-int/lit8 v2, v2, -0x1

    .line 9
    .line 10
    and-int/2addr v2, v0

    .line 11
    aget-object v3, v1, v2

    .line 12
    .line 13
    move-object v4, v3

    .line 14
    :goto_0
    if-eqz v4, :cond_1

    .line 15
    .line 16
    iget v5, v4, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->hash:I

    .line 17
    .line 18
    if-ne v5, v0, :cond_0

    .line 19
    .line 20
    iget-object v5, v4, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->key:Ljava/lang/Object;

    .line 21
    .line 22
    invoke-virtual {p0, p1, v5}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v5

    .line 26
    if-eqz v5, :cond_0

    .line 27
    .line 28
    goto :goto_1

    .line 29
    :cond_0
    iget-object v4, v4, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    :goto_1
    monitor-enter p0

    .line 33
    :try_start_0
    iget-object v5, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 34
    .line 35
    if-ne v1, v5, :cond_4

    .line 36
    .line 37
    const/4 v5, 0x0

    .line 38
    if-nez v4, :cond_2

    .line 39
    .line 40
    aget-object v1, v1, v2

    .line 41
    .line 42
    if-ne v3, v1, :cond_4

    .line 43
    .line 44
    monitor-exit p0

    .line 45
    return-object v5

    .line 46
    :cond_2
    iget-object v6, v4, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 47
    .line 48
    aget-object v7, v1, v2

    .line 49
    .line 50
    if-ne v3, v7, :cond_4

    .line 51
    .line 52
    if-eqz v6, :cond_4

    .line 53
    .line 54
    iput-object v5, v4, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 55
    .line 56
    iget p1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇08O〇00〇o:I

    .line 57
    .line 58
    add-int/lit8 p1, p1, -0x1

    .line 59
    .line 60
    iput p1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇08O〇00〇o:I

    .line 61
    .line 62
    iget-object p1, v4, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 63
    .line 64
    :goto_2
    if-eq v3, v4, :cond_3

    .line 65
    .line 66
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 67
    .line 68
    iget v5, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->hash:I

    .line 69
    .line 70
    iget-object v7, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->key:Ljava/lang/Object;

    .line 71
    .line 72
    iget-object v8, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 73
    .line 74
    invoke-direct {v0, v5, v7, v8, p1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;-><init>(ILjava/lang/Object;Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;)V

    .line 75
    .line 76
    .line 77
    iget-object v3, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 78
    .line 79
    move-object p1, v0

    .line 80
    goto :goto_2

    .line 81
    :cond_3
    aput-object p1, v1, v2

    .line 82
    .line 83
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->oO80(Ljava/lang/Object;)V

    .line 84
    .line 85
    .line 86
    monitor-exit p0

    .line 87
    return-object v6

    .line 88
    :cond_4
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇8o8o〇(Ljava/lang/Object;I)Ljava/lang/Object;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    monitor-exit p0

    .line 93
    return-object p1

    .line 94
    :catchall_0
    move-exception p1

    .line 95
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    throw p1
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public declared-synchronized size()I
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇08O〇00〇o:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3
    .line 4
    monitor-exit p0

    .line 5
    return v0

    .line 6
    :catchall_0
    move-exception v0

    .line 7
    monitor-exit p0

    .line 8
    throw v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public values()Ljava/util/Collection;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->oOo〇8o008:Ljava/util/Collection;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Values;

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    invoke-direct {v0, p0, v1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Values;-><init>(Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;Lcom/intsig/office/fc/dom4j/tree/〇o〇;)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->oOo〇8o008:Ljava/util/Collection;

    .line 13
    .line 14
    :goto_0
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected 〇080(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 1
    if-eq p1, p2, :cond_1

    .line 2
    .line 3
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p1, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 13
    :goto_1
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected 〇80〇808〇O()V
    .locals 13

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    const/high16 v2, 0x40000000    # 2.0f

    .line 5
    .line 6
    if-lt v1, v2, :cond_0

    .line 7
    .line 8
    const v0, 0x7fffffff

    .line 9
    .line 10
    .line 11
    iput v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->o〇00O:I

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    shl-int/lit8 v2, v1, 0x1

    .line 15
    .line 16
    add-int/lit8 v3, v2, -0x1

    .line 17
    .line 18
    int-to-float v4, v2

    .line 19
    iget v5, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->O8o08O8O:F

    .line 20
    .line 21
    mul-float v4, v4, v5

    .line 22
    .line 23
    float-to-int v4, v4

    .line 24
    iput v4, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->o〇00O:I

    .line 25
    .line 26
    new-array v2, v2, [Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 27
    .line 28
    const/4 v4, 0x0

    .line 29
    :goto_0
    if-ge v4, v1, :cond_5

    .line 30
    .line 31
    aget-object v5, v0, v4

    .line 32
    .line 33
    if-eqz v5, :cond_4

    .line 34
    .line 35
    iget v6, v5, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->hash:I

    .line 36
    .line 37
    and-int/2addr v6, v3

    .line 38
    iget-object v7, v5, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 39
    .line 40
    if-nez v7, :cond_1

    .line 41
    .line 42
    aput-object v5, v2, v6

    .line 43
    .line 44
    goto :goto_3

    .line 45
    :cond_1
    move-object v8, v5

    .line 46
    :goto_1
    if-eqz v7, :cond_3

    .line 47
    .line 48
    iget v9, v7, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->hash:I

    .line 49
    .line 50
    and-int/2addr v9, v3

    .line 51
    if-eq v9, v6, :cond_2

    .line 52
    .line 53
    move-object v8, v7

    .line 54
    move v6, v9

    .line 55
    :cond_2
    iget-object v7, v7, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 56
    .line 57
    goto :goto_1

    .line 58
    :cond_3
    aput-object v8, v2, v6

    .line 59
    .line 60
    :goto_2
    if-eq v5, v8, :cond_4

    .line 61
    .line 62
    iget v6, v5, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->hash:I

    .line 63
    .line 64
    and-int v7, v6, v3

    .line 65
    .line 66
    new-instance v9, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 67
    .line 68
    iget-object v10, v5, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->key:Ljava/lang/Object;

    .line 69
    .line 70
    iget-object v11, v5, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 71
    .line 72
    aget-object v12, v2, v7

    .line 73
    .line 74
    invoke-direct {v9, v6, v10, v11, v12}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;-><init>(ILjava/lang/Object;Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;)V

    .line 75
    .line 76
    .line 77
    aput-object v9, v2, v7

    .line 78
    .line 79
    iget-object v5, v5, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 80
    .line 81
    goto :goto_2

    .line 82
    :cond_4
    :goto_3
    add-int/lit8 v4, v4, 0x1

    .line 83
    .line 84
    goto :goto_0

    .line 85
    :cond_5
    iput-object v2, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 86
    .line 87
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->oO80(Ljava/lang/Object;)V

    .line 88
    .line 89
    .line 90
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method protected 〇8o8o〇(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->OO:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    add-int/lit8 v1, v1, -0x1

    .line 5
    .line 6
    and-int/2addr v1, p2

    .line 7
    aget-object v2, v0, v1

    .line 8
    .line 9
    move-object v3, v2

    .line 10
    :goto_0
    const/4 v4, 0x0

    .line 11
    if-eqz v3, :cond_2

    .line 12
    .line 13
    iget v5, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->hash:I

    .line 14
    .line 15
    if-ne v5, p2, :cond_1

    .line 16
    .line 17
    iget-object v5, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->key:Ljava/lang/Object;

    .line 18
    .line 19
    invoke-virtual {p0, p1, v5}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    move-result v5

    .line 23
    if-eqz v5, :cond_1

    .line 24
    .line 25
    iget-object p1, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 26
    .line 27
    iput-object v4, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 28
    .line 29
    iget p2, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇08O〇00〇o:I

    .line 30
    .line 31
    add-int/lit8 p2, p2, -0x1

    .line 32
    .line 33
    iput p2, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->〇08O〇00〇o:I

    .line 34
    .line 35
    iget-object p2, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 36
    .line 37
    :goto_1
    if-eq v2, v3, :cond_0

    .line 38
    .line 39
    new-instance v4, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 40
    .line 41
    iget v5, v2, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->hash:I

    .line 42
    .line 43
    iget-object v6, v2, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->key:Ljava/lang/Object;

    .line 44
    .line 45
    iget-object v7, v2, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 46
    .line 47
    invoke-direct {v4, v5, v6, v7, p2}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;-><init>(ILjava/lang/Object;Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;)V

    .line 48
    .line 49
    .line 50
    iget-object v2, v2, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 51
    .line 52
    move-object p2, v4

    .line 53
    goto :goto_1

    .line 54
    :cond_0
    aput-object p2, v0, v1

    .line 55
    .line 56
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->oO80(Ljava/lang/Object;)V

    .line 57
    .line 58
    .line 59
    return-object p1

    .line 60
    :cond_1
    iget-object v3, v3, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_2
    return-object v4
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method protected declared-synchronized 〇o〇(Ljava/util/Map$Entry;)Z
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    if-eqz p1, :cond_0

    .line 21
    .line 22
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    .line 24
    .line 25
    monitor-exit p0

    .line 26
    const/4 p1, 0x1

    .line 27
    return p1

    .line 28
    :cond_0
    monitor-exit p0

    .line 29
    const/4 p1, 0x0

    .line 30
    return p1

    .line 31
    :catchall_0
    move-exception p1

    .line 32
    monitor-exit p0

    .line 33
    throw p1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.class public Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;
.super Lcom/intsig/office/fc/dom4j/jaxb/JAXBSupport;
.source "JAXBModifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier$JAXBElementModifier;
    }
.end annotation


# instance fields
.field private modifier:Lcom/intsig/office/fc/dom4j/io/SAXModifier;

.field private modifiers:Ljava/util/HashMap;

.field private outputFormat:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

.field private pruneElements:Z

.field private xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBSupport;-><init>(Ljava/lang/String;)V

    .line 2
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->modifiers:Ljava/util/HashMap;

    .line 3
    new-instance p1, Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    invoke-direct {p1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->outputFormat:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V
    .locals 0

    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBSupport;-><init>(Ljava/lang/String;)V

    .line 8
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->modifiers:Ljava/util/HashMap;

    .line 9
    iput-object p2, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->outputFormat:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V
    .locals 0

    .line 4
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBSupport;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 5
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->modifiers:Ljava/util/HashMap;

    .line 6
    new-instance p1, Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    invoke-direct {p1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->outputFormat:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/ClassLoader;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V
    .locals 0

    .line 10
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBSupport;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 11
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->modifiers:Ljava/util/HashMap;

    .line 12
    iput-object p3, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->outputFormat:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    return-void
.end method

.method private createXMLWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->outputFormat:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 8
    .line 9
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>(Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 15
    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private getModifier()Lcom/intsig/office/fc/dom4j/io/SAXModifier;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->modifier:Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->isPruneElements()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;-><init>(Z)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->modifier:Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    .line 15
    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->modifier:Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
.end method

.method private getXMLWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private installModifier()Lcom/intsig/office/fc/dom4j/io/SAXModifier;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->isPruneElements()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;-><init>(Z)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->modifier:Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->resetModifiers()V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->modifiers:Ljava/util/HashMap;

    .line 16
    .line 17
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    if-eqz v1, :cond_0

    .line 30
    .line 31
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    check-cast v1, Ljava/util/Map$Entry;

    .line 36
    .line 37
    new-instance v2, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier$JAXBElementModifier;

    .line 38
    .line 39
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    check-cast v3, Lcom/intsig/office/fc/dom4j/jaxb/JAXBObjectModifier;

    .line 44
    .line 45
    invoke-direct {v2, p0, p0, v3}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier$JAXBElementModifier;-><init>(Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;Lcom/intsig/office/fc/dom4j/jaxb/JAXBObjectModifier;)V

    .line 46
    .line 47
    .line 48
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->getModifier()Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    .line 49
    .line 50
    .line 51
    move-result-object v3

    .line 52
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    check-cast v1, Ljava/lang/String;

    .line 57
    .line 58
    invoke-virtual {v3, v1, v2}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->addModifier(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/io/ElementModifier;)V

    .line 59
    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->modifier:Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    .line 63
    .line 64
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->getXMLWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->setXMLWriter(Lcom/intsig/office/fc/dom4j/io/XMLWriter;)V

    .line 69
    .line 70
    .line 71
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->modifier:Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    .line 72
    .line 73
    return-object v0
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method


# virtual methods
.method public addObjectModifier(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/jaxb/JAXBObjectModifier;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->modifiers:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public isPruneElements()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->pruneElements:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public modify(Ljava/io/File;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->installModifier()Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modify(Ljava/io/File;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public modify(Ljava/io/File;Ljava/nio/charset/Charset;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    :try_start_0
    new-instance v0, Ljava/io/InputStreamReader;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->installModifier()Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modify(Ljava/io/Reader;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 4
    new-instance p2, Lcom/intsig/office/fc/dom4j/DocumentException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public modify(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->installModifier()Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modify(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public modify(Ljava/io/InputStream;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 7
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->installModifier()Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modify(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public modify(Ljava/io/Reader;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 8
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->installModifier()Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modify(Ljava/io/Reader;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public modify(Ljava/io/Reader;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 9
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->installModifier()Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modify(Ljava/io/Reader;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public modify(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 10
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->installModifier()Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modify(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public modify(Ljava/net/URL;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 11
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->installModifier()Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modify(Ljava/net/URL;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public modify(Lorg/xml/sax/InputSource;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 5
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->installModifier()Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modify(Lorg/xml/sax/InputSource;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public removeObjectModifier(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->modifiers:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->getModifier()Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->removeModifier(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public resetObjectModifiers()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->modifiers:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->getModifier()Lcom/intsig/office/fc/dom4j/io/SAXModifier;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->resetModifiers()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setOutput(Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->createXMLWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    move-result-object v0

    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->setOutputStream(Ljava/io/OutputStream;)V

    return-void
.end method

.method public setOutput(Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->createXMLWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->setOutputStream(Ljava/io/OutputStream;)V

    return-void
.end method

.method public setOutput(Ljava/io/Writer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->createXMLWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->setWriter(Ljava/io/Writer;)V

    return-void
.end method

.method public setPruneElements(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBModifier;->pruneElements:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

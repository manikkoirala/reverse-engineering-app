.class public Lcom/intsig/office/fc/dom4j/rule/Stylesheet;
.super Ljava/lang/Object;
.source "Stylesheet.java"


# instance fields
.field private modeName:Ljava/lang/String;

.field private ruleManager:Lcom/intsig/office/fc/dom4j/rule/RuleManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/fc/dom4j/rule/RuleManager;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->ruleManager:Lcom/intsig/office/fc/dom4j/rule/RuleManager;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public addRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->ruleManager:Lcom/intsig/office/fc/dom4j/rule/RuleManager;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->addRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public applyTemplates(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->modeName:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->applyTemplates(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public applyTemplates(Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/XPath;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->modeName:Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0}, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->applyTemplates(Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/XPath;Ljava/lang/String;)V

    return-void
.end method

.method public applyTemplates(Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/XPath;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->ruleManager:Lcom/intsig/office/fc/dom4j/rule/RuleManager;

    invoke-virtual {v0, p3}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->getMode(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/rule/Mode;

    move-result-object p3

    .line 3
    invoke-interface {p2, p1}, Lcom/intsig/office/fc/dom4j/XPath;->selectNodes(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 4
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 5
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 6
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/intsig/office/fc/dom4j/Node;

    .line 7
    invoke-virtual {p3, p2}, Lcom/intsig/office/fc/dom4j/rule/Mode;->fireRule(Lcom/intsig/office/fc/dom4j/Node;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public applyTemplates(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->ruleManager:Lcom/intsig/office/fc/dom4j/rule/RuleManager;

    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->getMode(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/rule/Mode;

    move-result-object v0

    .line 10
    instance-of v1, p1, Lcom/intsig/office/fc/dom4j/Element;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 11
    check-cast p1, Lcom/intsig/office/fc/dom4j/Element;

    .line 12
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Branch;->nodeCount()I

    move-result p2

    :goto_0
    if-ge v2, p2, :cond_4

    .line 13
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Branch;->node(I)Lcom/intsig/office/fc/dom4j/Node;

    move-result-object v1

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/rule/Mode;->fireRule(Lcom/intsig/office/fc/dom4j/Node;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 15
    :cond_0
    instance-of v1, p1, Lcom/intsig/office/fc/dom4j/Document;

    if-eqz v1, :cond_1

    .line 16
    check-cast p1, Lcom/intsig/office/fc/dom4j/Document;

    .line 17
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Branch;->nodeCount()I

    move-result p2

    :goto_1
    if-ge v2, p2, :cond_4

    .line 18
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Branch;->node(I)Lcom/intsig/office/fc/dom4j/Node;

    move-result-object v1

    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/rule/Mode;->fireRule(Lcom/intsig/office/fc/dom4j/Node;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 20
    :cond_1
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_4

    .line 21
    check-cast p1, Ljava/util/List;

    .line 22
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    :goto_2
    if-ge v2, v0, :cond_4

    .line 23
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 24
    instance-of v3, v1, Lcom/intsig/office/fc/dom4j/Element;

    if-eqz v3, :cond_2

    .line 25
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    invoke-virtual {p0, v1, p2}, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->applyTemplates(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    .line 26
    :cond_2
    instance-of v3, v1, Lcom/intsig/office/fc/dom4j/Document;

    if-eqz v3, :cond_3

    .line 27
    check-cast v1, Lcom/intsig/office/fc/dom4j/Document;

    invoke-virtual {p0, v1, p2}, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->applyTemplates(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_3
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    return-void
.end method

.method public clear()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->ruleManager:Lcom/intsig/office/fc/dom4j/rule/RuleManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->clear()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getModeName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->modeName:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getValueOfAction()Lcom/intsig/office/fc/dom4j/rule/Action;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->ruleManager:Lcom/intsig/office/fc/dom4j/rule/RuleManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->getValueOfAction()Lcom/intsig/office/fc/dom4j/rule/Action;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->ruleManager:Lcom/intsig/office/fc/dom4j/rule/RuleManager;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->removeRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public run(Lcom/intsig/office/fc/dom4j/Node;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->modeName:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->run(Lcom/intsig/office/fc/dom4j/Node;Ljava/lang/String;)V

    return-void
.end method

.method public run(Lcom/intsig/office/fc/dom4j/Node;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->ruleManager:Lcom/intsig/office/fc/dom4j/rule/RuleManager;

    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->getMode(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/rule/Mode;

    move-result-object p2

    .line 13
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/dom4j/rule/Mode;->fireRule(Lcom/intsig/office/fc/dom4j/Node;)V

    return-void
.end method

.method public run(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->modeName:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->run(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public run(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 2
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/Node;

    if-eqz v0, :cond_0

    .line 3
    check-cast p1, Lcom/intsig/office/fc/dom4j/Node;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->run(Lcom/intsig/office/fc/dom4j/Node;Ljava/lang/String;)V

    goto :goto_0

    .line 4
    :cond_0
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_1

    .line 5
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->run(Ljava/util/List;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public run(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->modeName:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->run(Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method public run(Ljava/util/List;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 7
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 8
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 9
    instance-of v3, v2, Lcom/intsig/office/fc/dom4j/Node;

    if-eqz v3, :cond_0

    .line 10
    check-cast v2, Lcom/intsig/office/fc/dom4j/Node;

    invoke-virtual {p0, v2, p2}, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->run(Lcom/intsig/office/fc/dom4j/Node;Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setModeName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->modeName:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setValueOfAction(Lcom/intsig/office/fc/dom4j/rule/Action;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Stylesheet;->ruleManager:Lcom/intsig/office/fc/dom4j/rule/RuleManager;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/rule/RuleManager;->setValueOfAction(Lcom/intsig/office/fc/dom4j/rule/Action;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

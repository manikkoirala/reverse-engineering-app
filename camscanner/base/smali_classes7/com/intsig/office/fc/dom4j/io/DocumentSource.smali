.class public Lcom/intsig/office/fc/dom4j/io/DocumentSource;
.super Ljavax/xml/transform/sax/SAXSource;
.source "DocumentSource.java"


# static fields
.field public static final DOM4J_FEATURE:Ljava/lang/String; = "http://org.dom4j.io.DoucmentSource/feature"


# instance fields
.field private xmlReader:Lorg/xml/sax/XMLReader;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/dom4j/Document;)V
    .locals 1

    .line 4
    invoke-direct {p0}, Ljavax/xml/transform/sax/SAXSource;-><init>()V

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/SAXWriter;

    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/SAXWriter;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DocumentSource;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 6
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/DocumentSource;->setDocument(Lcom/intsig/office/fc/dom4j/Document;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/Node;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljavax/xml/transform/sax/SAXSource;-><init>()V

    .line 2
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/SAXWriter;

    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/SAXWriter;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DocumentSource;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 3
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getDocument()Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/DocumentSource;->setDocument(Lcom/intsig/office/fc/dom4j/Document;)V

    return-void
.end method


# virtual methods
.method public getDocument()Lcom/intsig/office/fc/dom4j/Document;
    .locals 1

    .line 1
    invoke-virtual {p0}, Ljavax/xml/transform/sax/SAXSource;->getInputSource()Lorg/xml/sax/InputSource;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/dom4j/io/DocumentInputSource;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/DocumentInputSource;->〇080()Lcom/intsig/office/fc/dom4j/Document;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getXMLReader()Lorg/xml/sax/XMLReader;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DocumentSource;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setDocument(Lcom/intsig/office/fc/dom4j/Document;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/DocumentInputSource;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/dom4j/io/DocumentInputSource;-><init>(Lcom/intsig/office/fc/dom4j/Document;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, v0}, Ljavax/xml/transform/sax/SAXSource;->setInputSource(Lorg/xml/sax/InputSource;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setInputSource(Lorg/xml/sax/InputSource;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/io/DocumentInputSource;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/fc/dom4j/io/DocumentInputSource;

    .line 6
    .line 7
    invoke-super {p0, p1}, Ljavax/xml/transform/sax/SAXSource;->setInputSource(Lorg/xml/sax/InputSource;)V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    .line 12
    .line 13
    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 14
    .line 15
    .line 16
    throw p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setXMLReader(Lorg/xml/sax/XMLReader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/io/SAXWriter;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/fc/dom4j/io/SAXWriter;

    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DocumentSource;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 8
    .line 9
    goto :goto_1

    .line 10
    :cond_0
    instance-of v0, p1, Lorg/xml/sax/XMLFilter;

    .line 11
    .line 12
    if-eqz v0, :cond_2

    .line 13
    .line 14
    check-cast p1, Lorg/xml/sax/XMLFilter;

    .line 15
    .line 16
    :goto_0
    invoke-interface {p1}, Lorg/xml/sax/XMLFilter;->getParent()Lorg/xml/sax/XMLReader;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    instance-of v1, v0, Lorg/xml/sax/XMLFilter;

    .line 21
    .line 22
    if-eqz v1, :cond_1

    .line 23
    .line 24
    move-object p1, v0

    .line 25
    check-cast p1, Lorg/xml/sax/XMLFilter;

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/DocumentSource;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 29
    .line 30
    invoke-interface {p1, v0}, Lorg/xml/sax/XMLFilter;->setParent(Lorg/xml/sax/XMLReader;)V

    .line 31
    .line 32
    .line 33
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/DocumentSource;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 34
    .line 35
    :goto_1
    return-void

    .line 36
    :cond_2
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    .line 37
    .line 38
    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 39
    .line 40
    .line 41
    throw p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.class public Lcom/intsig/office/fc/dom4j/DocumentFactory;
.super Ljava/lang/Object;
.source "DocumentFactory.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static singleton:Lcom/intsig/office/fc/dom4j/util/SingletonStrategy;


# instance fields
.field protected transient cache:Lcom/intsig/office/fc/dom4j/tree/QNameCache;

.field private xpathNamespaceURIs:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->init()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected static createSingleton(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/DocumentFactory;
    .locals 3

    .line 7
    :try_start_0
    const-class v0, Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v1, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    .line 8
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/dom4j/DocumentFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 9
    :catchall_0
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WARNING: Cannot load DocumentFactory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 10
    new-instance p0, Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;-><init>()V

    return-object p0
.end method

.method private static createSingleton()Lcom/intsig/office/fc/dom4j/util/SingletonStrategy;
    .locals 3

    const-string v0, "com.intsig.office.fc.dom4j.DocumentFactory"

    :try_start_0
    const-string v1, "com.intsig.office.fc.dom4j.factory"

    .line 1
    invoke-static {v1, v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    const-string v1, "com.intsig.office.fc.dom4j.DocumentFactory.singleton.strategy"

    const-string v2, "com.intsig.office.fc.dom4j.util.SimpleSingleton"

    .line 2
    invoke-static {v1, v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 4
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/fc/dom4j/util/SingletonStrategy;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 5
    :catch_1
    new-instance v1, Lcom/intsig/office/fc/dom4j/util/SimpleSingleton;

    invoke-direct {v1}, Lcom/intsig/office/fc/dom4j/util/SimpleSingleton;-><init>()V

    .line 6
    :goto_0
    invoke-interface {v1, v0}, Lcom/intsig/office/fc/dom4j/util/SingletonStrategy;->setSingletonClassName(Ljava/lang/String;)V

    return-object v1
.end method

.method public static declared-synchronized getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;
    .locals 2

    .line 1
    const-class v0, Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    sget-object v1, Lcom/intsig/office/fc/dom4j/DocumentFactory;->singleton:Lcom/intsig/office/fc/dom4j/util/SingletonStrategy;

    .line 5
    .line 6
    if-nez v1, :cond_0

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createSingleton()Lcom/intsig/office/fc/dom4j/util/SingletonStrategy;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    sput-object v1, Lcom/intsig/office/fc/dom4j/DocumentFactory;->singleton:Lcom/intsig/office/fc/dom4j/util/SingletonStrategy;

    .line 13
    .line 14
    :cond_0
    sget-object v1, Lcom/intsig/office/fc/dom4j/DocumentFactory;->singleton:Lcom/intsig/office/fc/dom4j/util/SingletonStrategy;

    .line 15
    .line 16
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/util/SingletonStrategy;->instance()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    check-cast v1, Lcom/intsig/office/fc/dom4j/DocumentFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 21
    .line 22
    monitor-exit v0

    .line 23
    return-object v1

    .line 24
    :catchall_0
    move-exception v1

    .line 25
    monitor-exit v0

    .line 26
    throw v1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->init()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public createAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;
    .locals 0

    .line 1
    new-instance p1, Lcom/intsig/office/fc/dom4j/tree/DefaultAttribute;

    invoke-direct {p1, p2, p3}, Lcom/intsig/office/fc/dom4j/tree/DefaultAttribute;-><init>(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)V

    return-object p1
.end method

.method public createAttribute(Lcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;
    .locals 0

    .line 2
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p2

    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object p1

    return-object p1
.end method

.method public createCDATA(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/CDATA;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/DefaultCDATA;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/dom4j/tree/DefaultCDATA;-><init>(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public createComment(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Comment;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/DefaultComment;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/dom4j/tree/DefaultComment;-><init>(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public createDocType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/DocumentType;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocumentType;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2, p3}, Lcom/intsig/office/fc/dom4j/tree/DefaultDocumentType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public createDocument()Lcom/intsig/office/fc/dom4j/Document;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;

    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;-><init>()V

    .line 2
    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/dom4j/tree/DefaultDocument;->setDocumentFactory(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V

    return-object v0
.end method

.method public createDocument(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createDocument()Lcom/intsig/office/fc/dom4j/Document;

    move-result-object v0

    .line 7
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/dom4j/Document;->setRootElement(Lcom/intsig/office/fc/dom4j/Element;)V

    return-object v0
.end method

.method public createDocument(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 2

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createDocument()Lcom/intsig/office/fc/dom4j/Document;

    move-result-object v0

    .line 4
    instance-of v1, v0, Lcom/intsig/office/fc/dom4j/tree/AbstractDocument;

    if-eqz v1, :cond_0

    .line 5
    move-object v1, v0

    check-cast v1, Lcom/intsig/office/fc/dom4j/tree/AbstractDocument;

    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractDocument;->setXMLEncoding(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public createElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/DefaultElement;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/dom4j/tree/DefaultElement;-><init>(Lcom/intsig/office/fc/dom4j/QName;)V

    return-object v0
.end method

.method public createElement(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 0

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    return-object p1
.end method

.method public createElement(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 0

    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    return-object p1
.end method

.method public createEntity(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Entity;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/DefaultEntity;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/tree/DefaultEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public createNamespace(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;
    .locals 0

    .line 1
    invoke-static {p1, p2}, Lcom/intsig/office/fc/dom4j/Namespace;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public createPattern(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/rule/Pattern;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/xpath/XPathPattern;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/dom4j/xpath/XPathPattern;-><init>(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public createProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/ProcessingInstruction;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/DefaultProcessingInstruction;

    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/tree/DefaultProcessingInstruction;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public createProcessingInstruction(Ljava/lang/String;Ljava/util/Map;)Lcom/intsig/office/fc/dom4j/ProcessingInstruction;
    .locals 1

    .line 2
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/DefaultProcessingInstruction;

    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/tree/DefaultProcessingInstruction;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    return-object v0
.end method

.method public createQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/DocumentFactory;->cache:Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->get(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    return-object p1
.end method

.method public createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/DocumentFactory;->cache:Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->get(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    return-object p1
.end method

.method public createQName(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/DocumentFactory;->cache:Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    return-object p1
.end method

.method public createQName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/DocumentFactory;->cache:Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    invoke-static {p2, p3}, Lcom/intsig/office/fc/dom4j/Namespace;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->get(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    return-object p1
.end method

.method protected createQNameCache()Lcom/intsig/office/fc/dom4j/tree/QNameCache;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;-><init>(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public createText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Text;
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/DefaultText;

    .line 4
    .line 5
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/dom4j/tree/DefaultText;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-object v0

    .line 9
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 10
    .line 11
    const-string v0, "Adding text to an XML document must not be null"

    .line 12
    .line 13
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    throw p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public createXPath(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/XPath;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/InvalidXPathException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/xpath/DefaultXPath;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/dom4j/xpath/DefaultXPath;-><init>(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/DocumentFactory;->xpathNamespaceURIs:Ljava/util/Map;

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/xpath/DefaultXPath;->setNamespaceURIs(Ljava/util/Map;)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public createXPathFilter(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/NodeFilter;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createXPath(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/XPath;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getQNames()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/DocumentFactory;->cache:Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->getQNames()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getXPathNamespaceURIs()Ljava/util/Map;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/DocumentFactory;->xpathNamespaceURIs:Ljava/util/Map;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected init()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQNameCache()Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/DocumentFactory;->cache:Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected intern(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/DocumentFactory;->cache:Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->intern(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/QName;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setXPathNamespaceURIs(Ljava/util/Map;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/DocumentFactory;->xpathNamespaceURIs:Ljava/util/Map;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

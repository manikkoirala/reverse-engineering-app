.class public Lcom/intsig/office/fc/dom4j/io/SAXReader;
.super Ljava/lang/Object;
.source "SAXReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/dom4j/io/SAXReader$SAXEntityResolver;
    }
.end annotation


# static fields
.field private static final SAX_DECL_HANDLER:Ljava/lang/String; = "http://xml.org/sax/properties/declaration-handler"

.field private static final SAX_LEXICALHANDLER:Ljava/lang/String; = "http://xml.org/sax/handlers/LexicalHandler"

.field private static final SAX_LEXICAL_HANDLER:Ljava/lang/String; = "http://xml.org/sax/properties/lexical-handler"

.field private static final SAX_NAMESPACES:Ljava/lang/String; = "http://xml.org/sax/features/namespaces"

.field private static final SAX_NAMESPACE_PREFIXES:Ljava/lang/String; = "http://xml.org/sax/features/namespace-prefixes"

.field private static final SAX_STRING_INTERNING:Ljava/lang/String; = "http://xml.org/sax/features/string-interning"


# instance fields
.field private dispatchHandler:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

.field private encoding:Ljava/lang/String;

.field private entityResolver:Lorg/xml/sax/EntityResolver;

.field private errorHandler:Lorg/xml/sax/ErrorHandler;

.field private factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

.field private ignoreComments:Z

.field private includeExternalDTDDeclarations:Z

.field private includeInternalDTDDeclarations:Z

.field private mergeAdjacentText:Z

.field private stringInternEnabled:Z

.field private stripWhitespaceText:Z

.field private validating:Z

.field private xmlFilter:Lorg/xml/sax/XMLFilter;

.field private xmlReader:Lorg/xml/sax/XMLReader;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stringInternEnabled:Z

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeInternalDTDDeclarations:Z

    .line 4
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeExternalDTDDeclarations:Z

    .line 5
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->mergeAdjacentText:Z

    .line 6
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stripWhitespaceText:Z

    .line 7
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->ignoreComments:Z

    const/4 v0, 0x0

    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V
    .locals 1

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 19
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stringInternEnabled:Z

    const/4 v0, 0x0

    .line 20
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeInternalDTDDeclarations:Z

    .line 21
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeExternalDTDDeclarations:Z

    .line 22
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->mergeAdjacentText:Z

    .line 23
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stripWhitespaceText:Z

    .line 24
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->ignoreComments:Z

    const/4 v0, 0x0

    .line 25
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/DocumentFactory;Z)V
    .locals 1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 28
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stringInternEnabled:Z

    const/4 v0, 0x0

    .line 29
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeInternalDTDDeclarations:Z

    .line 30
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeExternalDTDDeclarations:Z

    .line 31
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->mergeAdjacentText:Z

    .line 32
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stripWhitespaceText:Z

    .line 33
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->ignoreComments:Z

    const/4 v0, 0x0

    .line 34
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    .line 35
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 36
    iput-boolean p2, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->validating:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 57
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stringInternEnabled:Z

    const/4 v0, 0x0

    .line 58
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeInternalDTDDeclarations:Z

    .line 59
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeExternalDTDDeclarations:Z

    .line 60
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->mergeAdjacentText:Z

    .line 61
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stripWhitespaceText:Z

    .line 62
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->ignoreComments:Z

    const/4 v0, 0x0

    .line 63
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 64
    invoke-static {p1}, Lorg/xml/sax/helpers/XMLReaderFactory;->createXMLReader(Ljava/lang/String;)Lorg/xml/sax/XMLReader;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->xmlReader:Lorg/xml/sax/XMLReader;

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 66
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stringInternEnabled:Z

    const/4 v0, 0x0

    .line 67
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeInternalDTDDeclarations:Z

    .line 68
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeExternalDTDDeclarations:Z

    .line 69
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->mergeAdjacentText:Z

    .line 70
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stripWhitespaceText:Z

    .line 71
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->ignoreComments:Z

    const/4 v0, 0x0

    .line 72
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 73
    invoke-static {p1}, Lorg/xml/sax/helpers/XMLReaderFactory;->createXMLReader(Ljava/lang/String;)Lorg/xml/sax/XMLReader;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 74
    :cond_0
    iput-boolean p2, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->validating:Z

    return-void
.end method

.method public constructor <init>(Lorg/xml/sax/XMLReader;)V
    .locals 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 38
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stringInternEnabled:Z

    const/4 v0, 0x0

    .line 39
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeInternalDTDDeclarations:Z

    .line 40
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeExternalDTDDeclarations:Z

    .line 41
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->mergeAdjacentText:Z

    .line 42
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stripWhitespaceText:Z

    .line 43
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->ignoreComments:Z

    const/4 v0, 0x0

    .line 44
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    .line 45
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->xmlReader:Lorg/xml/sax/XMLReader;

    return-void
.end method

.method public constructor <init>(Lorg/xml/sax/XMLReader;Z)V
    .locals 1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 47
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stringInternEnabled:Z

    const/4 v0, 0x0

    .line 48
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeInternalDTDDeclarations:Z

    .line 49
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeExternalDTDDeclarations:Z

    .line 50
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->mergeAdjacentText:Z

    .line 51
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stripWhitespaceText:Z

    .line 52
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->ignoreComments:Z

    const/4 v0, 0x0

    .line 53
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    .line 54
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 55
    iput-boolean p2, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->validating:Z

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 10
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stringInternEnabled:Z

    const/4 v0, 0x0

    .line 11
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeInternalDTDDeclarations:Z

    .line 12
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeExternalDTDDeclarations:Z

    .line 13
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->mergeAdjacentText:Z

    .line 14
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stripWhitespaceText:Z

    .line 15
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->ignoreComments:Z

    const/4 v0, 0x0

    .line 16
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    .line 17
    iput-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->validating:Z

    return-void
.end method


# virtual methods
.method public addHandler(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->getDispatchHandler()Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;->〇080(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected configureReader(Lorg/xml/sax/XMLReader;Lorg/xml/sax/helpers/DefaultHandler;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 1
    const-string v0, "http://xml.org/sax/handlers/LexicalHandler"

    .line 2
    .line 3
    invoke-static {p1, v0, p2}, Lcom/intsig/office/fc/dom4j/io/SAXHelper;->Oo08(Lorg/xml/sax/XMLReader;Ljava/lang/String;Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    const-string v0, "http://xml.org/sax/properties/lexical-handler"

    .line 7
    .line 8
    invoke-static {p1, v0, p2}, Lcom/intsig/office/fc/dom4j/io/SAXHelper;->Oo08(Lorg/xml/sax/XMLReader;Ljava/lang/String;Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeInternalDTDDeclarations:Z

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeExternalDTDDeclarations:Z

    .line 16
    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    :cond_0
    const-string v0, "http://xml.org/sax/properties/declaration-handler"

    .line 20
    .line 21
    invoke-static {p1, v0, p2}, Lcom/intsig/office/fc/dom4j/io/SAXHelper;->Oo08(Lorg/xml/sax/XMLReader;Ljava/lang/String;Ljava/lang/Object;)Z

    .line 22
    .line 23
    .line 24
    :cond_1
    const-string v0, "http://xml.org/sax/features/namespaces"

    .line 25
    .line 26
    const/4 v1, 0x1

    .line 27
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/dom4j/io/SAXHelper;->O8(Lorg/xml/sax/XMLReader;Ljava/lang/String;Z)Z

    .line 28
    .line 29
    .line 30
    const-string v0, "http://xml.org/sax/features/namespace-prefixes"

    .line 31
    .line 32
    const/4 v2, 0x0

    .line 33
    invoke-static {p1, v0, v2}, Lcom/intsig/office/fc/dom4j/io/SAXHelper;->O8(Lorg/xml/sax/XMLReader;Ljava/lang/String;Z)Z

    .line 34
    .line 35
    .line 36
    const-string v0, "http://xml.org/sax/features/string-interning"

    .line 37
    .line 38
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->isStringInternEnabled()Z

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    invoke-static {p1, v0, v2}, Lcom/intsig/office/fc/dom4j/io/SAXHelper;->O8(Lorg/xml/sax/XMLReader;Ljava/lang/String;Z)Z

    .line 43
    .line 44
    .line 45
    const-string v0, "http://xml.org/sax/features/use-locator2"

    .line 46
    .line 47
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/dom4j/io/SAXHelper;->O8(Lorg/xml/sax/XMLReader;Ljava/lang/String;Z)Z

    .line 48
    .line 49
    .line 50
    :try_start_0
    const-string v0, "http://xml.org/sax/features/validation"

    .line 51
    .line 52
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->isValidating()Z

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    invoke-interface {p1, v0, v1}, Lorg/xml/sax/XMLReader;->setFeature(Ljava/lang/String;Z)V

    .line 57
    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->errorHandler:Lorg/xml/sax/ErrorHandler;

    .line 60
    .line 61
    if-eqz v0, :cond_2

    .line 62
    .line 63
    invoke-interface {p1, v0}, Lorg/xml/sax/XMLReader;->setErrorHandler(Lorg/xml/sax/ErrorHandler;)V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_2
    invoke-interface {p1, p2}, Lorg/xml/sax/XMLReader;->setErrorHandler(Lorg/xml/sax/ErrorHandler;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    .line 69
    .line 70
    goto :goto_0

    .line 71
    :catch_0
    move-exception p2

    .line 72
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->isValidating()Z

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    if-nez v0, :cond_3

    .line 77
    .line 78
    :goto_0
    return-void

    .line 79
    :cond_3
    new-instance v0, Lcom/intsig/office/fc/dom4j/DocumentException;

    .line 80
    .line 81
    new-instance v1, Ljava/lang/StringBuilder;

    .line 82
    .line 83
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    .line 85
    .line 86
    const-string v2, "Validation not supported for XMLReader: "

    .line 87
    .line 88
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 99
    .line 100
    .line 101
    throw v0
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method protected createContentHandler(Lorg/xml/sax/XMLReader;)Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;
    .locals 2

    .line 1
    new-instance p1, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->dispatchHandler:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 8
    .line 9
    invoke-direct {p1, v0, v1}, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;-><init>(Lcom/intsig/office/fc/dom4j/DocumentFactory;Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 10
    .line 11
    .line 12
    return-object p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected createDefaultEntityResolver(Ljava/lang/String;)Lorg/xml/sax/EntityResolver;
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_0

    .line 8
    .line 9
    const/16 v0, 0x2f

    .line 10
    .line 11
    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-lez v0, :cond_0

    .line 16
    .line 17
    add-int/lit8 v0, v0, 0x1

    .line 18
    .line 19
    const/4 v1, 0x0

    .line 20
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 p1, 0x0

    .line 26
    :goto_0
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/SAXReader$SAXEntityResolver;

    .line 27
    .line 28
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader$SAXEntityResolver;-><init>(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    return-object v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected createXMLReader()Lorg/xml/sax/XMLReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->isValidating()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/dom4j/io/SAXHelper;->〇080(Z)Lorg/xml/sax/XMLReader;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getDispatchHandler()Lcom/intsig/office/fc/dom4j/io/DispatchHandler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->dispatchHandler:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->dispatchHandler:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->dispatchHandler:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 12
    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEntityResolver()Lorg/xml/sax/EntityResolver;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->entityResolver:Lorg/xml/sax/EntityResolver;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getErrorHandler()Lorg/xml/sax/ErrorHandler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->errorHandler:Lorg/xml/sax/ErrorHandler;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getXMLFilter()Lorg/xml/sax/XMLFilter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->xmlFilter:Lorg/xml/sax/XMLFilter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getXMLReader()Lorg/xml/sax/XMLReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->createXMLReader()Lorg/xml/sax/XMLReader;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 12
    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected installXMLFilter(Lorg/xml/sax/XMLReader;)Lorg/xml/sax/XMLReader;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->getXMLFilter()Lorg/xml/sax/XMLFilter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    move-object v1, v0

    .line 8
    :goto_0
    invoke-interface {v1}, Lorg/xml/sax/XMLFilter;->getParent()Lorg/xml/sax/XMLReader;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    instance-of v3, v2, Lorg/xml/sax/XMLFilter;

    .line 13
    .line 14
    if-eqz v3, :cond_0

    .line 15
    .line 16
    move-object v1, v2

    .line 17
    check-cast v1, Lorg/xml/sax/XMLFilter;

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-interface {v1, p1}, Lorg/xml/sax/XMLFilter;->setParent(Lorg/xml/sax/XMLReader;)V

    .line 21
    .line 22
    .line 23
    return-object v0

    .line 24
    :cond_1
    return-object p1
.end method

.method public isIgnoreComments()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->ignoreComments:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isIncludeExternalDTDDeclarations()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeExternalDTDDeclarations:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isIncludeInternalDTDDeclarations()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeInternalDTDDeclarations:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isMergeAdjacentText()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->mergeAdjacentText:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isStringInternEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stringInternEnabled:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isStripWhitespaceText()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stripWhitespaceText:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isValidating()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->validating:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public read(Ljava/io/File;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 1
    :try_start_0
    new-instance v0, Lorg/xml/sax/InputSource;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    .line 2
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3
    invoke-virtual {v0, v1}, Lorg/xml/sax/InputSource;->setEncoding(Ljava/lang/String;)V

    .line 4
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 5
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "file://"

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 6
    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "/"

    .line 7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    const/16 v2, 0x5c

    const/16 v3, 0x2f

    .line 8
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    .line 9
    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 10
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/xml/sax/InputSource;->setSystemId(Ljava/lang/String;)V

    .line 11
    :cond_2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Lorg/xml/sax/InputSource;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 12
    new-instance v0, Lcom/intsig/office/fc/dom4j/DocumentException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 22
    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-direct {v0, p1}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    .line 23
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 24
    invoke-virtual {v0, p1}, Lorg/xml/sax/InputSource;->setEncoding(Ljava/lang/String;)V

    .line 25
    :cond_0
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Lorg/xml/sax/InputSource;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public read(Ljava/io/InputStream;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 30
    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-direct {v0, p1}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    .line 31
    invoke-virtual {v0, p2}, Lorg/xml/sax/InputSource;->setSystemId(Ljava/lang/String;)V

    .line 32
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 33
    invoke-virtual {v0, p1}, Lorg/xml/sax/InputSource;->setEncoding(Ljava/lang/String;)V

    .line 34
    :cond_0
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Lorg/xml/sax/InputSource;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public read(Ljava/io/Reader;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 26
    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-direct {v0, p1}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    .line 27
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 28
    invoke-virtual {v0, p1}, Lorg/xml/sax/InputSource;->setEncoding(Ljava/lang/String;)V

    .line 29
    :cond_0
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Lorg/xml/sax/InputSource;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public read(Ljava/io/Reader;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 35
    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-direct {v0, p1}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    .line 36
    invoke-virtual {v0, p2}, Lorg/xml/sax/InputSource;->setSystemId(Ljava/lang/String;)V

    .line 37
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 38
    invoke-virtual {v0, p1}, Lorg/xml/sax/InputSource;->setEncoding(Ljava/lang/String;)V

    .line 39
    :cond_0
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Lorg/xml/sax/InputSource;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public read(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 18
    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-direct {v0, p1}, Lorg/xml/sax/InputSource;-><init>(Ljava/lang/String;)V

    .line 19
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 20
    invoke-virtual {v0, p1}, Lorg/xml/sax/InputSource;->setEncoding(Ljava/lang/String;)V

    .line 21
    :cond_0
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Lorg/xml/sax/InputSource;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public read(Ljava/net/URL;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 13
    invoke-virtual {p1}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object p1

    .line 14
    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-direct {v0, p1}, Lorg/xml/sax/InputSource;-><init>(Ljava/lang/String;)V

    .line 15
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 16
    invoke-virtual {v0, p1}, Lorg/xml/sax/InputSource;->setEncoding(Ljava/lang/String;)V

    .line 17
    :cond_0
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Lorg/xml/sax/InputSource;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public read(Lorg/xml/sax/InputSource;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 40
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v0

    .line 41
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->installXMLFilter(Lorg/xml/sax/XMLReader;)Lorg/xml/sax/XMLReader;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->entityResolver:Lorg/xml/sax/EntityResolver;

    if-nez v1, :cond_0

    .line 43
    invoke-virtual {p1}, Lorg/xml/sax/InputSource;->getSystemId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->createDefaultEntityResolver(Ljava/lang/String;)Lorg/xml/sax/EntityResolver;

    move-result-object v1

    .line 44
    iput-object v1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->entityResolver:Lorg/xml/sax/EntityResolver;

    .line 45
    :cond_0
    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setEntityResolver(Lorg/xml/sax/EntityResolver;)V

    .line 46
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->createContentHandler(Lorg/xml/sax/XMLReader;)Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;

    move-result-object v2

    .line 47
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;->setEntityResolver(Lorg/xml/sax/EntityResolver;)V

    .line 48
    invoke-virtual {v2, p1}, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;->setInputSource(Lorg/xml/sax/InputSource;)V

    .line 49
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->isIncludeInternalDTDDeclarations()Z

    move-result v1

    .line 50
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->isIncludeExternalDTDDeclarations()Z

    move-result v3

    .line 51
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;->setIncludeInternalDTDDeclarations(Z)V

    .line 52
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;->setIncludeExternalDTDDeclarations(Z)V

    .line 53
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->isMergeAdjacentText()Z

    move-result v1

    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;->setMergeAdjacentText(Z)V

    .line 54
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->isStripWhitespaceText()Z

    move-result v1

    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;->setStripWhitespaceText(Z)V

    .line 55
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->isIgnoreComments()Z

    move-result v1

    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;->setIgnoreComments(Z)V

    .line 56
    invoke-interface {v0, v2}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 57
    invoke-virtual {p0, v0, v2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->configureReader(Lorg/xml/sax/XMLReader;Lorg/xml/sax/helpers/DefaultHandler;)V

    .line 58
    invoke-interface {v0, p1}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 59
    invoke-virtual {v2}, Lcom/intsig/office/fc/dom4j/io/SAXContentHandler;->getDocument()Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 60
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 61
    instance-of v0, p1, Lorg/xml/sax/SAXParseException;

    if-eqz v0, :cond_2

    .line 62
    move-object v0, p1

    check-cast v0, Lorg/xml/sax/SAXParseException;

    .line 63
    invoke-virtual {v0}, Lorg/xml/sax/SAXParseException;->getSystemId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, ""

    .line 64
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error on line "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/xml/sax/SAXParseException;->getLineNumber()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " of document "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " : "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 66
    new-instance v1, Lcom/intsig/office/fc/dom4j/DocumentException;

    invoke-direct {v1, v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 67
    :cond_2
    new-instance v0, Lcom/intsig/office/fc/dom4j/DocumentException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public removeHandler(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->getDispatchHandler()Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;->Oo08(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/ElementHandler;

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public resetHandlers()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->getDispatchHandler()Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;->o〇0()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setDefaultHandler(Lcom/intsig/office/fc/dom4j/ElementHandler;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->getDispatchHandler()Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;->〇〇888(Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected setDispatchHandler(Lcom/intsig/office/fc/dom4j/io/DispatchHandler;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->dispatchHandler:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDocumentFactory(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->factory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->encoding:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setEntityResolver(Lorg/xml/sax/EntityResolver;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->entityResolver:Lorg/xml/sax/EntityResolver;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setErrorHandler(Lorg/xml/sax/ErrorHandler;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->errorHandler:Lorg/xml/sax/ErrorHandler;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFeature(Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->getXMLReader()Lorg/xml/sax/XMLReader;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0, p1, p2}, Lorg/xml/sax/XMLReader;->setFeature(Ljava/lang/String;Z)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setIgnoreComments(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->ignoreComments:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setIncludeExternalDTDDeclarations(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeExternalDTDDeclarations:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setIncludeInternalDTDDeclarations(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->includeInternalDTDDeclarations:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMergeAdjacentText(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->mergeAdjacentText:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->getXMLReader()Lorg/xml/sax/XMLReader;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0, p1, p2}, Lorg/xml/sax/XMLReader;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setStringInternEnabled(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stringInternEnabled:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setStripWhitespaceText(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->stripWhitespaceText:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setValidation(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->validating:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setXMLFilter(Lorg/xml/sax/XMLFilter;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->xmlFilter:Lorg/xml/sax/XMLFilter;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setXMLReader(Lorg/xml/sax/XMLReader;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXReader;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setXMLReaderClassName(Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lorg/xml/sax/helpers/XMLReaderFactory;->createXMLReader(Ljava/lang/String;)Lorg/xml/sax/XMLReader;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->setXMLReader(Lorg/xml/sax/XMLReader;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

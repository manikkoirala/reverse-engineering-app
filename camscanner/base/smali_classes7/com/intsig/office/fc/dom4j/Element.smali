.class public interface abstract Lcom/intsig/office/fc/dom4j/Element;
.super Ljava/lang/Object;
.source "Element.java"

# interfaces
.implements Lcom/intsig/office/fc/dom4j/Branch;


# virtual methods
.method public abstract add(Lcom/intsig/office/fc/dom4j/Attribute;)V
.end method

.method public abstract add(Lcom/intsig/office/fc/dom4j/CDATA;)V
.end method

.method public abstract add(Lcom/intsig/office/fc/dom4j/Entity;)V
.end method

.method public abstract add(Lcom/intsig/office/fc/dom4j/Namespace;)V
.end method

.method public abstract add(Lcom/intsig/office/fc/dom4j/Text;)V
.end method

.method public abstract addAttribute(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract addAttribute(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract addCDATA(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract addComment(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract addEntity(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract addNamespace(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract addProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract addProcessingInstruction(Ljava/lang/String;Ljava/util/Map;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract additionalNamespaces()Ljava/util/List;
.end method

.method public abstract appendAttributes(Lcom/intsig/office/fc/dom4j/Element;)V
.end method

.method public abstract attribute(I)Lcom/intsig/office/fc/dom4j/Attribute;
.end method

.method public abstract attribute(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Attribute;
.end method

.method public abstract attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;
.end method

.method public abstract attributeCount()I
.end method

.method public abstract attributeIterator()Ljava/util/Iterator;
.end method

.method public abstract attributeValue(Lcom/intsig/office/fc/dom4j/QName;)Ljava/lang/String;
.end method

.method public abstract attributeValue(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract attributeValue(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract attributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract attributes()Ljava/util/List;
.end method

.method public abstract createCopy()Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract createCopy(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract createCopy(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract declaredNamespaces()Ljava/util/List;
.end method

.method public abstract element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
.end method

.method public abstract elementIterator()Ljava/util/Iterator;
.end method

.method public abstract elementIterator(Lcom/intsig/office/fc/dom4j/QName;)Ljava/util/Iterator;
.end method

.method public abstract elementIterator(Ljava/lang/String;)Ljava/util/Iterator;
.end method

.method public abstract elementText(Lcom/intsig/office/fc/dom4j/QName;)Ljava/lang/String;
.end method

.method public abstract elementText(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract elementTextTrim(Lcom/intsig/office/fc/dom4j/QName;)Ljava/lang/String;
.end method

.method public abstract elementTextTrim(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract elements()Ljava/util/List;
.end method

.method public abstract elements(Lcom/intsig/office/fc/dom4j/QName;)Ljava/util/List;
.end method

.method public abstract elements(Ljava/lang/String;)Ljava/util/List;
.end method

.method public abstract getData()Ljava/lang/Object;
.end method

.method public abstract getNamespace()Lcom/intsig/office/fc/dom4j/Namespace;
.end method

.method public abstract getNamespaceForPrefix(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;
.end method

.method public abstract getNamespaceForURI(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;
.end method

.method public abstract getNamespacePrefix()Ljava/lang/String;
.end method

.method public abstract getNamespaceURI()Ljava/lang/String;
.end method

.method public abstract getNamespacesForURI(Ljava/lang/String;)Ljava/util/List;
.end method

.method public abstract getQName()Lcom/intsig/office/fc/dom4j/QName;
.end method

.method public abstract getQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
.end method

.method public abstract getQualifiedName()Ljava/lang/String;
.end method

.method public abstract getStringValue()Ljava/lang/String;
.end method

.method public abstract getText()Ljava/lang/String;
.end method

.method public abstract getTextTrim()Ljava/lang/String;
.end method

.method public abstract getXPathResult(I)Lcom/intsig/office/fc/dom4j/Node;
.end method

.method public abstract hasMixedContent()Z
.end method

.method public abstract isRootElement()Z
.end method

.method public abstract isTextOnly()Z
.end method

.method public abstract remove(Lcom/intsig/office/fc/dom4j/Attribute;)Z
.end method

.method public abstract remove(Lcom/intsig/office/fc/dom4j/CDATA;)Z
.end method

.method public abstract remove(Lcom/intsig/office/fc/dom4j/Entity;)Z
.end method

.method public abstract remove(Lcom/intsig/office/fc/dom4j/Namespace;)Z
.end method

.method public abstract remove(Lcom/intsig/office/fc/dom4j/Text;)Z
.end method

.method public abstract setAttributeValue(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract setAttributeValue(Ljava/lang/String;Ljava/lang/String;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract setAttributes(Ljava/util/List;)V
.end method

.method public abstract setData(Ljava/lang/Object;)V
.end method

.method public abstract setQName(Lcom/intsig/office/fc/dom4j/QName;)V
.end method

.class public Lcom/intsig/office/fc/dom4j/rule/Mode;
.super Ljava/lang/Object;
.source "Mode.java"


# instance fields
.field private attributeNameRuleSets:Ljava/util/Map;

.field private elementNameRuleSets:Ljava/util/Map;

.field private ruleSets:[Lcom/intsig/office/fc/dom4j/rule/RuleSet;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0xe

    .line 5
    .line 6
    new-array v0, v0, [Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 7
    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->ruleSets:[Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public addRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/rule/Rule;->getMatchType()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/rule/Rule;->getMatchesNodeName()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const/4 v2, 0x1

    .line 10
    if-eqz v1, :cond_1

    .line 11
    .line 12
    if-ne v0, v2, :cond_0

    .line 13
    .line 14
    iget-object v3, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->elementNameRuleSets:Ljava/util/Map;

    .line 15
    .line 16
    invoke-virtual {p0, v3, v1, p1}, Lcom/intsig/office/fc/dom4j/rule/Mode;->addToNameMap(Ljava/util/Map;Ljava/lang/String;Lcom/intsig/office/fc/dom4j/rule/Rule;)Ljava/util/Map;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    iput-object v1, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->elementNameRuleSets:Ljava/util/Map;

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v3, 0x2

    .line 24
    if-ne v0, v3, :cond_1

    .line 25
    .line 26
    iget-object v3, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->attributeNameRuleSets:Ljava/util/Map;

    .line 27
    .line 28
    invoke-virtual {p0, v3, v1, p1}, Lcom/intsig/office/fc/dom4j/rule/Mode;->addToNameMap(Ljava/util/Map;Ljava/lang/String;Lcom/intsig/office/fc/dom4j/rule/Rule;)Ljava/util/Map;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    iput-object v1, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->attributeNameRuleSets:Ljava/util/Map;

    .line 33
    .line 34
    :cond_1
    :goto_0
    const/16 v1, 0xe

    .line 35
    .line 36
    if-lt v0, v1, :cond_2

    .line 37
    .line 38
    const/4 v0, 0x0

    .line 39
    :cond_2
    if-nez v0, :cond_4

    .line 40
    .line 41
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->ruleSets:[Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 42
    .line 43
    array-length v1, v1

    .line 44
    :goto_1
    if-ge v2, v1, :cond_4

    .line 45
    .line 46
    iget-object v3, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->ruleSets:[Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 47
    .line 48
    aget-object v3, v3, v2

    .line 49
    .line 50
    if-eqz v3, :cond_3

    .line 51
    .line 52
    invoke-virtual {v3, p1}, Lcom/intsig/office/fc/dom4j/rule/RuleSet;->addRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V

    .line 53
    .line 54
    .line 55
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 56
    .line 57
    goto :goto_1

    .line 58
    :cond_4
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/rule/Mode;->getRuleSet(I)Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/rule/RuleSet;->addRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V

    .line 63
    .line 64
    .line 65
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected addToNameMap(Ljava/util/Map;Ljava/lang/String;Lcom/intsig/office/fc/dom4j/rule/Rule;)Ljava/util/Map;
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    new-instance p1, Ljava/util/HashMap;

    .line 4
    .line 5
    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 6
    .line 7
    .line 8
    :cond_0
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 13
    .line 14
    if-nez v0, :cond_1

    .line 15
    .line 16
    new-instance v0, Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 17
    .line 18
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/rule/RuleSet;-><init>()V

    .line 19
    .line 20
    .line 21
    invoke-interface {p1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    :cond_1
    invoke-virtual {v0, p3}, Lcom/intsig/office/fc/dom4j/rule/RuleSet;->addRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V

    .line 25
    .line 26
    .line 27
    return-object p1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public applyTemplates(Lcom/intsig/office/fc/dom4j/Document;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 7
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Branch;->nodeCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 8
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Branch;->node(I)Lcom/intsig/office/fc/dom4j/Node;

    move-result-object v2

    .line 9
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/dom4j/rule/Mode;->fireRule(Lcom/intsig/office/fc/dom4j/Node;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public applyTemplates(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->attributeCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 2
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attribute(I)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v3

    .line 3
    invoke-virtual {p0, v3}, Lcom/intsig/office/fc/dom4j/rule/Mode;->fireRule(Lcom/intsig/office/fc/dom4j/Node;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Branch;->nodeCount()I

    move-result v0

    :goto_1
    if-ge v1, v0, :cond_1

    .line 5
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Branch;->node(I)Lcom/intsig/office/fc/dom4j/Node;

    move-result-object v2

    .line 6
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/dom4j/rule/Mode;->fireRule(Lcom/intsig/office/fc/dom4j/Node;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public fireRule(Lcom/intsig/office/fc/dom4j/Node;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/rule/Mode;->getMatchingRule(Lcom/intsig/office/fc/dom4j/Node;)Lcom/intsig/office/fc/dom4j/rule/Rule;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/rule/Rule;->getAction()Lcom/intsig/office/fc/dom4j/rule/Action;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/dom4j/rule/Action;->run(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 16
    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getMatchingRule(Lcom/intsig/office/fc/dom4j/Node;)Lcom/intsig/office/fc/dom4j/rule/Rule;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getNodeType()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->elementNameRuleSets:Ljava/util/Map;

    .line 9
    .line 10
    if-eqz v1, :cond_1

    .line 11
    .line 12
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    iget-object v2, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->elementNameRuleSets:Ljava/util/Map;

    .line 17
    .line 18
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 23
    .line 24
    if-eqz v1, :cond_1

    .line 25
    .line 26
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/dom4j/rule/RuleSet;->getMatchingRule(Lcom/intsig/office/fc/dom4j/Node;)Lcom/intsig/office/fc/dom4j/rule/Rule;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    if-eqz v1, :cond_1

    .line 31
    .line 32
    return-object v1

    .line 33
    :cond_0
    const/4 v1, 0x2

    .line 34
    if-ne v0, v1, :cond_1

    .line 35
    .line 36
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->attributeNameRuleSets:Ljava/util/Map;

    .line 37
    .line 38
    if-eqz v1, :cond_1

    .line 39
    .line 40
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    iget-object v2, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->attributeNameRuleSets:Ljava/util/Map;

    .line 45
    .line 46
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    check-cast v1, Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 51
    .line 52
    if-eqz v1, :cond_1

    .line 53
    .line 54
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/dom4j/rule/RuleSet;->getMatchingRule(Lcom/intsig/office/fc/dom4j/Node;)Lcom/intsig/office/fc/dom4j/rule/Rule;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    if-eqz v1, :cond_1

    .line 59
    .line 60
    return-object v1

    .line 61
    :cond_1
    const/4 v1, 0x0

    .line 62
    if-ltz v0, :cond_2

    .line 63
    .line 64
    iget-object v2, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->ruleSets:[Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 65
    .line 66
    array-length v2, v2

    .line 67
    if-lt v0, v2, :cond_3

    .line 68
    .line 69
    :cond_2
    const/4 v0, 0x0

    .line 70
    :cond_3
    iget-object v2, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->ruleSets:[Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 71
    .line 72
    aget-object v2, v2, v0

    .line 73
    .line 74
    if-eqz v2, :cond_4

    .line 75
    .line 76
    invoke-virtual {v2, p1}, Lcom/intsig/office/fc/dom4j/rule/RuleSet;->getMatchingRule(Lcom/intsig/office/fc/dom4j/Node;)Lcom/intsig/office/fc/dom4j/rule/Rule;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    goto :goto_0

    .line 81
    :cond_4
    const/4 v2, 0x0

    .line 82
    :goto_0
    if-nez v2, :cond_5

    .line 83
    .line 84
    if-eqz v0, :cond_5

    .line 85
    .line 86
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->ruleSets:[Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 87
    .line 88
    aget-object v0, v0, v1

    .line 89
    .line 90
    if-eqz v0, :cond_5

    .line 91
    .line 92
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/rule/RuleSet;->getMatchingRule(Lcom/intsig/office/fc/dom4j/Node;)Lcom/intsig/office/fc/dom4j/rule/Rule;

    .line 93
    .line 94
    .line 95
    move-result-object v2

    .line 96
    :cond_5
    return-object v2
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method protected getRuleSet(I)Lcom/intsig/office/fc/dom4j/rule/RuleSet;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->ruleSets:[Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 2
    .line 3
    aget-object v0, v0, p1

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 8
    .line 9
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/rule/RuleSet;-><init>()V

    .line 10
    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->ruleSets:[Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 13
    .line 14
    aput-object v0, v1, p1

    .line 15
    .line 16
    if-eqz p1, :cond_0

    .line 17
    .line 18
    const/4 p1, 0x0

    .line 19
    aget-object p1, v1, p1

    .line 20
    .line 21
    if-eqz p1, :cond_0

    .line 22
    .line 23
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/rule/RuleSet;->addAll(Lcom/intsig/office/fc/dom4j/rule/RuleSet;)V

    .line 24
    .line 25
    .line 26
    :cond_0
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected removeFromNameMap(Ljava/util/Map;Ljava/lang/String;Lcom/intsig/office/fc/dom4j/rule/Rule;)V
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 8
    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1, p3}, Lcom/intsig/office/fc/dom4j/rule/RuleSet;->removeRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public removeRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/rule/Rule;->getMatchType()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/rule/Rule;->getMatchesNodeName()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    if-eqz v1, :cond_1

    .line 10
    .line 11
    const/4 v2, 0x1

    .line 12
    if-ne v0, v2, :cond_0

    .line 13
    .line 14
    iget-object v2, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->elementNameRuleSets:Ljava/util/Map;

    .line 15
    .line 16
    invoke-virtual {p0, v2, v1, p1}, Lcom/intsig/office/fc/dom4j/rule/Mode;->removeFromNameMap(Ljava/util/Map;Ljava/lang/String;Lcom/intsig/office/fc/dom4j/rule/Rule;)V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v2, 0x2

    .line 21
    if-ne v0, v2, :cond_1

    .line 22
    .line 23
    iget-object v2, p0, Lcom/intsig/office/fc/dom4j/rule/Mode;->attributeNameRuleSets:Ljava/util/Map;

    .line 24
    .line 25
    invoke-virtual {p0, v2, v1, p1}, Lcom/intsig/office/fc/dom4j/rule/Mode;->removeFromNameMap(Ljava/util/Map;Ljava/lang/String;Lcom/intsig/office/fc/dom4j/rule/Rule;)V

    .line 26
    .line 27
    .line 28
    :cond_1
    :goto_0
    const/16 v1, 0xe

    .line 29
    .line 30
    const/4 v2, 0x0

    .line 31
    if-lt v0, v1, :cond_2

    .line 32
    .line 33
    const/4 v0, 0x0

    .line 34
    :cond_2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/rule/Mode;->getRuleSet(I)Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/dom4j/rule/RuleSet;->removeRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V

    .line 39
    .line 40
    .line 41
    if-eqz v0, :cond_3

    .line 42
    .line 43
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/dom4j/rule/Mode;->getRuleSet(I)Lcom/intsig/office/fc/dom4j/rule/RuleSet;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/rule/RuleSet;->removeRule(Lcom/intsig/office/fc/dom4j/rule/Rule;)V

    .line 48
    .line 49
    .line 50
    :cond_3
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.class public Lcom/intsig/office/fc/dom4j/QName;
.super Ljava/lang/Object;
.source "QName.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static singleton:Lcom/intsig/office/fc/dom4j/util/SingletonStrategy;


# instance fields
.field private documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

.field private hashCode:I

.field private name:Ljava/lang/String;

.field private transient namespace:Lcom/intsig/office/fc/dom4j/Namespace;

.field private qualifiedName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    :try_start_0
    const-string v0, "com.intsig.office.fc.dom4j.util.SimpleSingleton"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 2
    .line 3
    :try_start_1
    const-string v1, "com.intsig.office.fc.dom4j.QName.singleton.strategy"

    .line 4
    .line 5
    invoke-static {v1, v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 10
    .line 11
    .line 12
    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 13
    goto :goto_0

    .line 14
    :catch_0
    :try_start_2
    const-class v0, Lcom/intsig/office/fc/dom4j/util/SimpleSingleton;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :catch_1
    const/4 v0, 0x0

    .line 18
    :goto_0
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    check-cast v0, Lcom/intsig/office/fc/dom4j/util/SingletonStrategy;

    .line 23
    .line 24
    sput-object v0, Lcom/intsig/office/fc/dom4j/QName;->singleton:Lcom/intsig/office/fc/dom4j/util/SingletonStrategy;

    .line 25
    .line 26
    const-class v1, Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    .line 27
    .line 28
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/util/SingletonStrategy;->setSingletonClassName(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 33
    .line 34
    .line 35
    :catch_2
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/dom4j/Namespace;->NO_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    const-string p1, ""

    .line 3
    :cond_0
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/QName;->name:Ljava/lang/String;

    if-nez p2, :cond_1

    .line 4
    sget-object p2, Lcom/intsig/office/fc/dom4j/Namespace;->NO_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    :cond_1
    iput-object p2, p0, Lcom/intsig/office/fc/dom4j/QName;->namespace:Lcom/intsig/office/fc/dom4j/Namespace;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;Ljava/lang/String;)V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    const-string p1, ""

    .line 6
    :cond_0
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/QName;->name:Ljava/lang/String;

    .line 7
    iput-object p3, p0, Lcom/intsig/office/fc/dom4j/QName;->qualifiedName:Ljava/lang/String;

    if-nez p2, :cond_1

    .line 8
    sget-object p2, Lcom/intsig/office/fc/dom4j/Namespace;->NO_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    :cond_1
    iput-object p2, p0, Lcom/intsig/office/fc/dom4j/QName;->namespace:Lcom/intsig/office/fc/dom4j/Namespace;

    return-void
.end method

.method public static get(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/dom4j/QName;->getCache()Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->get(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p0

    return-object p0
.end method

.method public static get(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 2
    invoke-static {}, Lcom/intsig/office/fc/dom4j/QName;->getCache()Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->get(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p0

    return-object p0
.end method

.method public static get(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 11
    invoke-static {}, Lcom/intsig/office/fc/dom4j/QName;->getCache()Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->get(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p0

    return-object p0
.end method

.method public static get(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    if-nez p1, :cond_0

    .line 9
    invoke-static {}, Lcom/intsig/office/fc/dom4j/QName;->getCache()Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->get(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p0

    return-object p0

    .line 10
    :cond_0
    invoke-static {}, Lcom/intsig/office/fc/dom4j/QName;->getCache()Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p0

    return-object p0
.end method

.method public static get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    if-eqz p1, :cond_0

    .line 3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    if-nez p2, :cond_1

    .line 4
    invoke-static {p0}, Lcom/intsig/office/fc/dom4j/QName;->get(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p0

    return-object p0

    :cond_1
    if-eqz p1, :cond_4

    .line 5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    if-nez p2, :cond_3

    .line 6
    invoke-static {p0}, Lcom/intsig/office/fc/dom4j/QName;->get(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p0

    return-object p0

    .line 7
    :cond_3
    invoke-static {}, Lcom/intsig/office/fc/dom4j/QName;->getCache()Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/intsig/office/fc/dom4j/Namespace;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->get(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p0

    return-object p0

    .line 8
    :cond_4
    :goto_0
    invoke-static {}, Lcom/intsig/office/fc/dom4j/QName;->getCache()Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    move-result-object p1

    invoke-static {p2}, Lcom/intsig/office/fc/dom4j/Namespace;->get(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    move-result-object p2

    invoke-virtual {p1, p0, p2}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->get(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p0

    return-object p0
.end method

.method private static getCache()Lcom/intsig/office/fc/dom4j/tree/QNameCache;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/dom4j/QName;->singleton:Lcom/intsig/office/fc/dom4j/util/SingletonStrategy;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/util/SingletonStrategy;->instance()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Ljava/lang/String;

    .line 6
    .line 7
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    check-cast v1, Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 14
    .line 15
    .line 16
    invoke-static {v0, v1}, Lcom/intsig/office/fc/dom4j/Namespace;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/QName;->namespace:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/QName;->namespace:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/Namespace;->getPrefix()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/QName;->namespace:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/Namespace;->getURI()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    instance-of v1, p1, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-eqz v1, :cond_2

    .line 9
    .line 10
    check-cast p1, Lcom/intsig/office/fc/dom4j/QName;

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/QName;->hashCode()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/QName;->hashCode()I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    if-ne v1, v3, :cond_2

    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/QName;->getName()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/QName;->getName()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    if-eqz v1, :cond_1

    .line 35
    .line 36
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/QName;->getNamespaceURI()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/QName;->getNamespaceURI()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    if-eqz p1, :cond_1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    const/4 v0, 0x0

    .line 52
    :goto_0
    return v0

    .line 53
    :cond_2
    return v2
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/QName;->documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/QName;->name:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNamespace()Lcom/intsig/office/fc/dom4j/Namespace;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/QName;->namespace:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNamespacePrefix()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/QName;->namespace:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, ""

    .line 6
    .line 7
    return-object v0

    .line 8
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/Namespace;->getPrefix()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNamespaceURI()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/QName;->namespace:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, ""

    .line 6
    .line 7
    return-object v0

    .line 8
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/Namespace;->getURI()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getQualifiedName()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/QName;->qualifiedName:Ljava/lang/String;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/QName;->getNamespacePrefix()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-lez v1, :cond_0

    .line 16
    .line 17
    new-instance v1, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const-string v0, ":"

    .line 26
    .line 27
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/QName;->name:Ljava/lang/String;

    .line 31
    .line 32
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/QName;->qualifiedName:Ljava/lang/String;

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/QName;->name:Ljava/lang/String;

    .line 43
    .line 44
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/QName;->qualifiedName:Ljava/lang/String;

    .line 45
    .line 46
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/QName;->qualifiedName:Ljava/lang/String;

    .line 47
    .line 48
    return-object v0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/dom4j/QName;->hashCode:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/QName;->getName()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/QName;->getNamespaceURI()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    xor-int/2addr v0, v1

    .line 22
    iput v0, p0, Lcom/intsig/office/fc/dom4j/QName;->hashCode:I

    .line 23
    .line 24
    if-nez v0, :cond_0

    .line 25
    .line 26
    const v0, 0xbabe

    .line 27
    .line 28
    .line 29
    iput v0, p0, Lcom/intsig/office/fc/dom4j/QName;->hashCode:I

    .line 30
    .line 31
    :cond_0
    iget v0, p0, Lcom/intsig/office/fc/dom4j/QName;->hashCode:I

    .line 32
    .line 33
    return v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public setDocumentFactory(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/QName;->documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const-string v1, " [name: "

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/QName;->getName()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const-string v1, " namespace: \""

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/QName;->getNamespace()Lcom/intsig/office/fc/dom4j/Namespace;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    const-string v1, "\"]"

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    return-object v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.class public Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;
.super Ljava/lang/Object;
.source "ConcurrentReaderHashMap.java"

# interfaces
.implements Ljava/util/Iterator;
.implements Ljava/util/Enumeration;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "HashIterator"
.end annotation


# instance fields
.field protected currentKey:Ljava/lang/Object;

.field protected currentValue:Ljava/lang/Object;

.field protected entry:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

.field protected index:I

.field protected lastReturned:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

.field protected final tab:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

.field final synthetic this$0:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;


# direct methods
.method protected constructor <init>(Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->this$0:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->entry:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->lastReturned:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->O8()[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->tab:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 16
    .line 17
    array-length p1, p1

    .line 18
    add-int/lit8 p1, p1, -0x1

    .line 19
    .line 20
    iput p1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->index:I

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
.end method


# virtual methods
.method public hasMoreElements()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->hasNext()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public hasNext()Z
    .locals 3

    .line 1
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->entry:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->value:Ljava/lang/Object;

    .line 6
    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->entry:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 10
    .line 11
    iget-object v1, v1, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->key:Ljava/lang/Object;

    .line 12
    .line 13
    iput-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->currentKey:Ljava/lang/Object;

    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->currentValue:Ljava/lang/Object;

    .line 16
    .line 17
    const/4 v0, 0x1

    .line 18
    return v0

    .line 19
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->entry:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 20
    .line 21
    iget-object v0, v0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->entry:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 24
    .line 25
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->entry:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 26
    .line 27
    if-nez v0, :cond_3

    .line 28
    .line 29
    iget v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->index:I

    .line 30
    .line 31
    if-ltz v1, :cond_3

    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->tab:[Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 34
    .line 35
    add-int/lit8 v2, v1, -0x1

    .line 36
    .line 37
    iput v2, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->index:I

    .line 38
    .line 39
    aget-object v0, v0, v1

    .line 40
    .line 41
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->entry:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_3
    if-nez v0, :cond_0

    .line 45
    .line 46
    const/4 v0, 0x0

    .line 47
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->currentValue:Ljava/lang/Object;

    .line 48
    .line 49
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->currentKey:Ljava/lang/Object;

    .line 50
    .line 51
    const/4 v0, 0x0

    .line 52
    return v0
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public next()Ljava/lang/Object;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->currentKey:Ljava/lang/Object;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->hasNext()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    .line 13
    .line 14
    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    .line 15
    .line 16
    .line 17
    throw v0

    .line 18
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->returnValueOfNext()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->entry:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 23
    .line 24
    iput-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->lastReturned:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 25
    .line 26
    const/4 v2, 0x0

    .line 27
    iput-object v2, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->currentValue:Ljava/lang/Object;

    .line 28
    .line 29
    iput-object v2, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->currentKey:Ljava/lang/Object;

    .line 30
    .line 31
    iget-object v1, v1, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->next:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 32
    .line 33
    iput-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->entry:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 34
    .line 35
    return-object v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public nextElement()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->next()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public remove()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->lastReturned:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->this$0:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;->key:Ljava/lang/Object;

    .line 8
    .line 9
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->lastReturned:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 17
    .line 18
    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    .line 19
    .line 20
    .line 21
    throw v0
.end method

.method protected returnValueOfNext()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$HashIterator;->entry:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$Entry;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

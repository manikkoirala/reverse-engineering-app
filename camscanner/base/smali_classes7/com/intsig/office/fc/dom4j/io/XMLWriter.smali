.class public Lcom/intsig/office/fc/dom4j/io/XMLWriter;
.super Lorg/xml/sax/helpers/XMLFilterImpl;
.source "XMLWriter.java"

# interfaces
.implements Lorg/xml/sax/ext/LexicalHandler;


# static fields
.field protected static final DEFAULT_FORMAT:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

.field protected static final LEXICAL_HANDLER_NAMES:[Ljava/lang/String;

.field private static final PAD_TEXT:Ljava/lang/String; = " "


# instance fields
.field private autoFlush:Z

.field private buffer:Ljava/lang/StringBuffer;

.field private charsAdded:Z

.field private escapeText:Z

.field private format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

.field private inDTD:Z

.field private indentLevel:I

.field private lastChar:C

.field private lastElementClosed:Z

.field protected lastOutputNodeType:I

.field private lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

.field private maximumAllowedCharacter:I

.field private namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

.field private namespacesMap:Ljava/util/Map;

.field protected preserve:Z

.field private resolveEntityRefs:Z

.field private showCommentsInDTDs:Z

.field protected writer:Ljava/io/Writer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const-string v0, "http://xml.org/sax/properties/lexical-handler"

    .line 2
    .line 3
    const-string v1, "http://xml.org/sax/handlers/LexicalHandler"

    .line 4
    .line 5
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    sput-object v0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->LEXICAL_HANDLER_NAMES:[Ljava/lang/String;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 12
    .line 13
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;-><init>()V

    .line 14
    .line 15
    .line 16
    sput-object v0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->DEFAULT_FORMAT:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 4

    .line 14
    invoke-direct {p0}, Lorg/xml/sax/helpers/XMLFilterImpl;-><init>()V

    const/4 v0, 0x1

    .line 15
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->resolveEntityRefs:Z

    const/4 v1, 0x0

    .line 16
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastElementClosed:Z

    .line 17
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->preserve:Z

    .line 18
    new-instance v2, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-direct {v2}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;-><init>()V

    iput-object v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 19
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->escapeText:Z

    .line 20
    iput v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indentLevel:I

    .line 21
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->buffer:Ljava/lang/StringBuffer;

    .line 22
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->charsAdded:Z

    .line 23
    sget-object v1, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->DEFAULT_FORMAT:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    iput-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 24
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/OutputStreamWriter;

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-direct {v2, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    iput-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 25
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    .line 26
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    sget-object v1, Lcom/intsig/office/fc/dom4j/Namespace;->NO_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->push(Lcom/intsig/office/fc/dom4j/Namespace;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Lorg/xml/sax/helpers/XMLFilterImpl;-><init>()V

    const/4 v0, 0x1

    .line 54
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->resolveEntityRefs:Z

    const/4 v1, 0x0

    .line 55
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastElementClosed:Z

    .line 56
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->preserve:Z

    .line 57
    new-instance v2, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-direct {v2}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;-><init>()V

    iput-object v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 58
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->escapeText:Z

    .line 59
    iput v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indentLevel:I

    .line 60
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->buffer:Ljava/lang/StringBuffer;

    .line 61
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->charsAdded:Z

    .line 62
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 63
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getEncoding()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v1, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->createWriter(Ljava/io/OutputStream;Ljava/lang/String;)Ljava/io/Writer;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 64
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    .line 65
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    sget-object v0, Lcom/intsig/office/fc/dom4j/Namespace;->NO_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->push(Lcom/intsig/office/fc/dom4j/Namespace;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Lorg/xml/sax/helpers/XMLFilterImpl;-><init>()V

    const/4 v0, 0x1

    .line 28
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->resolveEntityRefs:Z

    const/4 v1, 0x0

    .line 29
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastElementClosed:Z

    .line 30
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->preserve:Z

    .line 31
    new-instance v2, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-direct {v2}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;-><init>()V

    iput-object v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 32
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->escapeText:Z

    .line 33
    iput v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indentLevel:I

    .line 34
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->buffer:Ljava/lang/StringBuffer;

    .line 35
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->charsAdded:Z

    .line 36
    sget-object v1, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->DEFAULT_FORMAT:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    iput-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 37
    invoke-virtual {v1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getEncoding()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->createWriter(Ljava/io/OutputStream;Ljava/lang/String;)Ljava/io/Writer;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 38
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    .line 39
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    sget-object v0, Lcom/intsig/office/fc/dom4j/Namespace;->NO_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->push(Lcom/intsig/office/fc/dom4j/Namespace;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Lorg/xml/sax/helpers/XMLFilterImpl;-><init>()V

    const/4 v0, 0x1

    .line 41
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->resolveEntityRefs:Z

    const/4 v1, 0x0

    .line 42
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastElementClosed:Z

    .line 43
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->preserve:Z

    .line 44
    new-instance v2, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-direct {v2}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;-><init>()V

    iput-object v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 45
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->escapeText:Z

    .line 46
    iput v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indentLevel:I

    .line 47
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->buffer:Ljava/lang/StringBuffer;

    .line 48
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->charsAdded:Z

    .line 49
    iput-object p2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 50
    invoke-virtual {p2}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getEncoding()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->createWriter(Ljava/io/OutputStream;Ljava/lang/String;)Ljava/io/Writer;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 51
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    .line 52
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    sget-object p2, Lcom/intsig/office/fc/dom4j/Namespace;->NO_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->push(Lcom/intsig/office/fc/dom4j/Namespace;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->DEFAULT_FORMAT:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>(Ljava/io/Writer;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V
    .locals 3

    .line 2
    invoke-direct {p0}, Lorg/xml/sax/helpers/XMLFilterImpl;-><init>()V

    const/4 v0, 0x1

    .line 3
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->resolveEntityRefs:Z

    const/4 v1, 0x0

    .line 4
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastElementClosed:Z

    .line 5
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->preserve:Z

    .line 6
    new-instance v2, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-direct {v2}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;-><init>()V

    iput-object v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 7
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->escapeText:Z

    .line 8
    iput v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indentLevel:I

    .line 9
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->buffer:Ljava/lang/StringBuffer;

    .line 10
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->charsAdded:Z

    .line 11
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 12
    iput-object p2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 13
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    sget-object p2, Lcom/intsig/office/fc/dom4j/Namespace;->NO_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->push(Lcom/intsig/office/fc/dom4j/Namespace;)V

    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_7

    .line 2
    .line 3
    array-length v0, p1

    .line 4
    if-eqz v0, :cond_7

    .line 5
    .line 6
    if-gtz p3, :cond_0

    .line 7
    .line 8
    goto/16 :goto_2

    .line 9
    .line 10
    :cond_0
    :try_start_0
    invoke-static {p1, p2, p3}, Ljava/lang/String;->valueOf([CII)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iget-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->escapeText:Z

    .line 15
    .line 16
    if-eqz v1, :cond_1

    .line 17
    .line 18
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->escapeElementEntities(Ljava/lang/String;)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 23
    .line 24
    invoke-virtual {v1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isTrimText()Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    const/4 v2, 0x3

    .line 29
    const/4 v3, 0x1

    .line 30
    if-eqz v1, :cond_5

    .line 31
    .line 32
    iget v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    .line 34
    const-string v4, " "

    .line 35
    .line 36
    const/16 v5, 0x20

    .line 37
    .line 38
    if-ne v1, v2, :cond_2

    .line 39
    .line 40
    :try_start_1
    iget-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->charsAdded:Z

    .line 41
    .line 42
    if-nez v1, :cond_2

    .line 43
    .line 44
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 45
    .line 46
    invoke-virtual {v1, v5}, Ljava/io/Writer;->write(I)V

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_2
    iget-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->charsAdded:Z

    .line 51
    .line 52
    if-eqz v1, :cond_3

    .line 53
    .line 54
    iget-char v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastChar:C

    .line 55
    .line 56
    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    if-eqz v1, :cond_3

    .line 61
    .line 62
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 63
    .line 64
    invoke-virtual {v1, v5}, Ljava/io/Writer;->write(I)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_3
    iget v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 69
    .line 70
    if-ne v1, v3, :cond_4

    .line 71
    .line 72
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 73
    .line 74
    invoke-virtual {v1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isPadText()Z

    .line 75
    .line 76
    .line 77
    move-result v1

    .line 78
    if-eqz v1, :cond_4

    .line 79
    .line 80
    iget-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastElementClosed:Z

    .line 81
    .line 82
    if-eqz v1, :cond_4

    .line 83
    .line 84
    const/4 v1, 0x0

    .line 85
    aget-char v1, p1, v1

    .line 86
    .line 87
    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    .line 88
    .line 89
    .line 90
    move-result v1

    .line 91
    if-eqz v1, :cond_4

    .line 92
    .line 93
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 94
    .line 95
    invoke-virtual {v1, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    :cond_4
    :goto_0
    const-string v1, ""

    .line 99
    .line 100
    new-instance v5, Ljava/util/StringTokenizer;

    .line 101
    .line 102
    invoke-direct {v5, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    :goto_1
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    .line 106
    .line 107
    .line 108
    move-result v0

    .line 109
    if-eqz v0, :cond_6

    .line 110
    .line 111
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 112
    .line 113
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 117
    .line 118
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object v1

    .line 122
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    move-object v1, v4

    .line 126
    goto :goto_1

    .line 127
    :cond_5
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 128
    .line 129
    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    :cond_6
    iput-boolean v3, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->charsAdded:Z

    .line 133
    .line 134
    add-int v0, p2, p3

    .line 135
    .line 136
    sub-int/2addr v0, v3

    .line 137
    aget-char v0, p1, v0

    .line 138
    .line 139
    iput-char v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastChar:C

    .line 140
    .line 141
    iput v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 142
    .line 143
    invoke-super {p0, p1, p2, p3}, Lorg/xml/sax/helpers/XMLFilterImpl;->characters([CII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 144
    .line 145
    .line 146
    goto :goto_2

    .line 147
    :catch_0
    move-exception p1

    .line 148
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->handleException(Ljava/io/IOException;)V

    .line 149
    .line 150
    .line 151
    :cond_7
    :goto_2
    return-void
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/io/Writer;->close()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public comment([CII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->showCommentsInDTDs:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->inDTD:Z

    .line 6
    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :try_start_0
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->charsAdded:Z

    .line 11
    .line 12
    new-instance v0, Ljava/lang/String;

    .line 13
    .line 14
    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeComment(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :catch_0
    move-exception v0

    .line 22
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->handleException(Ljava/io/IOException;)V

    .line 23
    .line 24
    .line 25
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    .line 26
    .line 27
    if-eqz v0, :cond_2

    .line 28
    .line 29
    invoke-interface {v0, p1, p2, p3}, Lorg/xml/sax/ext/LexicalHandler;->comment([CII)V

    .line 30
    .line 31
    .line 32
    :cond_2
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method protected createWriter(Ljava/io/OutputStream;Ljava/lang/String;)Ljava/io/Writer;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/io/BufferedWriter;

    .line 2
    .line 3
    new-instance v1, Ljava/io/OutputStreamWriter;

    .line 4
    .line 5
    invoke-direct {v1, p1, p2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 9
    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected defaultMaximumAllowedCharacter()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getEncoding()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v1, "US-ASCII"

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    const/16 v0, 0x7f

    .line 18
    .line 19
    return v0

    .line 20
    :cond_0
    const/4 v0, -0x1

    .line 21
    return v0
.end method

.method public endCDATA()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 2
    .line 3
    const-string v1, "]]>"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :catch_0
    move-exception v0

    .line 10
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->handleException(Ljava/io/IOException;)V

    .line 11
    .line 12
    .line 13
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    invoke-interface {v0}, Lorg/xml/sax/ext/LexicalHandler;->endCDATA()V

    .line 18
    .line 19
    .line 20
    :cond_0
    return-void
    .line 21
.end method

.method public endDTD()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->inDTD:Z

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-interface {v0}, Lorg/xml/sax/ext/LexicalHandler;->endDTD()V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public endDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Lorg/xml/sax/helpers/XMLFilterImpl;->endDocument()V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 9
    .line 10
    .line 11
    :catch_0
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->charsAdded:Z

    .line 3
    .line 4
    iget v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indentLevel:I

    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    sub-int/2addr v0, v1

    .line 8
    iput v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indentLevel:I

    .line 9
    .line 10
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastElementClosed:Z

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writePrintln()V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indent()V

    .line 18
    .line 19
    .line 20
    :cond_0
    invoke-virtual {p0, p3}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeClose(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iput v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 24
    .line 25
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastElementClosed:Z

    .line 26
    .line 27
    invoke-super {p0, p1, p2, p3}, Lorg/xml/sax/helpers/XMLFilterImpl;->endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :catch_0
    move-exception p1

    .line 32
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->handleException(Ljava/io/IOException;)V

    .line 33
    .line 34
    .line 35
    :goto_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public endEntity(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lorg/xml/sax/ext/LexicalHandler;->endEntity(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public endPrefixMapping(Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    invoke-super {p0, p1}, Lorg/xml/sax/helpers/XMLFilterImpl;->endPrefixMapping(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected escapeAttributeEntities(Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getAttributeQuoteCharacter()C

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const/4 v2, 0x0

    .line 12
    const/4 v3, 0x0

    .line 13
    move-object v6, v2

    .line 14
    const/4 v4, 0x0

    .line 15
    const/4 v5, 0x0

    .line 16
    :goto_0
    if-ge v4, v1, :cond_9

    .line 17
    .line 18
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    .line 19
    .line 20
    .line 21
    move-result v7

    .line 22
    const/16 v8, 0x9

    .line 23
    .line 24
    if-eq v7, v8, :cond_6

    .line 25
    .line 26
    const/16 v8, 0xa

    .line 27
    .line 28
    if-eq v7, v8, :cond_6

    .line 29
    .line 30
    const/16 v8, 0xd

    .line 31
    .line 32
    if-eq v7, v8, :cond_6

    .line 33
    .line 34
    const/16 v8, 0x22

    .line 35
    .line 36
    if-eq v7, v8, :cond_5

    .line 37
    .line 38
    const/16 v8, 0x3c

    .line 39
    .line 40
    if-eq v7, v8, :cond_4

    .line 41
    .line 42
    const/16 v8, 0x3e

    .line 43
    .line 44
    if-eq v7, v8, :cond_3

    .line 45
    .line 46
    const/16 v8, 0x26

    .line 47
    .line 48
    if-eq v7, v8, :cond_2

    .line 49
    .line 50
    const/16 v8, 0x27

    .line 51
    .line 52
    if-eq v7, v8, :cond_1

    .line 53
    .line 54
    const/16 v8, 0x20

    .line 55
    .line 56
    if-lt v7, v8, :cond_0

    .line 57
    .line 58
    invoke-virtual {p0, v7}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->shouldEncodeChar(C)Z

    .line 59
    .line 60
    .line 61
    move-result v8

    .line 62
    if-eqz v8, :cond_6

    .line 63
    .line 64
    :cond_0
    new-instance v8, Ljava/lang/StringBuilder;

    .line 65
    .line 66
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    const-string v9, "&#"

    .line 70
    .line 71
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    const-string v7, ";"

    .line 78
    .line 79
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v7

    .line 86
    goto :goto_1

    .line 87
    :cond_1
    if-ne v0, v8, :cond_6

    .line 88
    .line 89
    const-string v7, "&apos;"

    .line 90
    .line 91
    goto :goto_1

    .line 92
    :cond_2
    const-string v7, "&amp;"

    .line 93
    .line 94
    goto :goto_1

    .line 95
    :cond_3
    const-string v7, "&gt;"

    .line 96
    .line 97
    goto :goto_1

    .line 98
    :cond_4
    const-string v7, "&lt;"

    .line 99
    .line 100
    goto :goto_1

    .line 101
    :cond_5
    if-ne v0, v8, :cond_6

    .line 102
    .line 103
    const-string v7, "&quot;"

    .line 104
    .line 105
    goto :goto_1

    .line 106
    :cond_6
    move-object v7, v2

    .line 107
    :goto_1
    if-eqz v7, :cond_8

    .line 108
    .line 109
    if-nez v6, :cond_7

    .line 110
    .line 111
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    .line 112
    .line 113
    .line 114
    move-result-object v6

    .line 115
    :cond_7
    iget-object v8, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->buffer:Ljava/lang/StringBuffer;

    .line 116
    .line 117
    sub-int v9, v4, v5

    .line 118
    .line 119
    invoke-virtual {v8, v6, v5, v9}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 120
    .line 121
    .line 122
    iget-object v5, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->buffer:Ljava/lang/StringBuffer;

    .line 123
    .line 124
    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 125
    .line 126
    .line 127
    add-int/lit8 v5, v4, 0x1

    .line 128
    .line 129
    :cond_8
    add-int/lit8 v4, v4, 0x1

    .line 130
    .line 131
    goto :goto_0

    .line 132
    :cond_9
    if-nez v5, :cond_a

    .line 133
    .line 134
    return-object p1

    .line 135
    :cond_a
    if-ge v5, v1, :cond_c

    .line 136
    .line 137
    if-nez v6, :cond_b

    .line 138
    .line 139
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    .line 140
    .line 141
    .line 142
    move-result-object v6

    .line 143
    :cond_b
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->buffer:Ljava/lang/StringBuffer;

    .line 144
    .line 145
    sub-int/2addr v4, v5

    .line 146
    invoke-virtual {p1, v6, v5, v4}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 147
    .line 148
    .line 149
    :cond_c
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->buffer:Ljava/lang/StringBuffer;

    .line 150
    .line 151
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 152
    .line 153
    .line 154
    move-result-object p1

    .line 155
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->buffer:Ljava/lang/StringBuffer;

    .line 156
    .line 157
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 158
    .line 159
    .line 160
    return-object p1
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method protected escapeElementEntities(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    move-object v5, v1

    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x0

    .line 10
    :goto_0
    if-ge v3, v0, :cond_8

    .line 11
    .line 12
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    .line 13
    .line 14
    .line 15
    move-result v6

    .line 16
    const/16 v7, 0x9

    .line 17
    .line 18
    if-eq v6, v7, :cond_4

    .line 19
    .line 20
    const/16 v7, 0xa

    .line 21
    .line 22
    if-eq v6, v7, :cond_4

    .line 23
    .line 24
    const/16 v7, 0xd

    .line 25
    .line 26
    if-eq v6, v7, :cond_4

    .line 27
    .line 28
    const/16 v7, 0x26

    .line 29
    .line 30
    if-eq v6, v7, :cond_3

    .line 31
    .line 32
    const/16 v7, 0x3c

    .line 33
    .line 34
    if-eq v6, v7, :cond_2

    .line 35
    .line 36
    const/16 v7, 0x3e

    .line 37
    .line 38
    if-eq v6, v7, :cond_1

    .line 39
    .line 40
    const/16 v7, 0x20

    .line 41
    .line 42
    if-lt v6, v7, :cond_0

    .line 43
    .line 44
    invoke-virtual {p0, v6}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->shouldEncodeChar(C)Z

    .line 45
    .line 46
    .line 47
    move-result v7

    .line 48
    if-eqz v7, :cond_5

    .line 49
    .line 50
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    .line 51
    .line 52
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 53
    .line 54
    .line 55
    const-string v8, "&#"

    .line 56
    .line 57
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    const-string v6, ";"

    .line 64
    .line 65
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v6

    .line 72
    goto :goto_1

    .line 73
    :cond_1
    const-string v6, "&gt;"

    .line 74
    .line 75
    goto :goto_1

    .line 76
    :cond_2
    const-string v6, "&lt;"

    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_3
    const-string v6, "&amp;"

    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_4
    iget-boolean v7, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->preserve:Z

    .line 83
    .line 84
    if-eqz v7, :cond_5

    .line 85
    .line 86
    invoke-static {v6}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v6

    .line 90
    goto :goto_1

    .line 91
    :cond_5
    move-object v6, v1

    .line 92
    :goto_1
    if-eqz v6, :cond_7

    .line 93
    .line 94
    if-nez v5, :cond_6

    .line 95
    .line 96
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    .line 97
    .line 98
    .line 99
    move-result-object v5

    .line 100
    :cond_6
    iget-object v7, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->buffer:Ljava/lang/StringBuffer;

    .line 101
    .line 102
    sub-int v8, v3, v4

    .line 103
    .line 104
    invoke-virtual {v7, v5, v4, v8}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 105
    .line 106
    .line 107
    iget-object v4, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->buffer:Ljava/lang/StringBuffer;

    .line 108
    .line 109
    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 110
    .line 111
    .line 112
    add-int/lit8 v4, v3, 0x1

    .line 113
    .line 114
    :cond_7
    add-int/lit8 v3, v3, 0x1

    .line 115
    .line 116
    goto :goto_0

    .line 117
    :cond_8
    if-nez v4, :cond_9

    .line 118
    .line 119
    return-object p1

    .line 120
    :cond_9
    if-ge v4, v0, :cond_b

    .line 121
    .line 122
    if-nez v5, :cond_a

    .line 123
    .line 124
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    .line 125
    .line 126
    .line 127
    move-result-object v5

    .line 128
    :cond_a
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->buffer:Ljava/lang/StringBuffer;

    .line 129
    .line 130
    sub-int/2addr v3, v4

    .line 131
    invoke-virtual {p1, v5, v4, v3}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 132
    .line 133
    .line 134
    :cond_b
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->buffer:Ljava/lang/StringBuffer;

    .line 135
    .line 136
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object p1

    .line 140
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->buffer:Ljava/lang/StringBuffer;

    .line 141
    .line 142
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 143
    .line 144
    .line 145
    return-object p1
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/io/Writer;->flush()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLexicalHandler()Lorg/xml/sax/ext/LexicalHandler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMaximumAllowedCharacter()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->maximumAllowedCharacter:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->defaultMaximumAllowedCharacter()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iput v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->maximumAllowedCharacter:I

    .line 10
    .line 11
    :cond_0
    iget v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->maximumAllowedCharacter:I

    .line 12
    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getOutputFormat()Lcom/intsig/office/fc/dom4j/io/OutputFormat;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXNotRecognizedException;,
            Lorg/xml/sax/SAXNotSupportedException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    sget-object v1, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->LEXICAL_HANDLER_NAMES:[Ljava/lang/String;

    .line 3
    .line 4
    array-length v2, v1

    .line 5
    if-ge v0, v2, :cond_1

    .line 6
    .line 7
    aget-object v1, v1, v0

    .line 8
    .line 9
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->getLexicalHandler()Lorg/xml/sax/ext/LexicalHandler;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    return-object p1

    .line 20
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    invoke-super {p0, p1}, Lorg/xml/sax/helpers/XMLFilterImpl;->getProperty(Ljava/lang/String;)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    return-object p1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected handleException(Ljava/io/IOException;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    new-instance v0, Lorg/xml/sax/SAXException;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/Exception;)V

    .line 4
    .line 5
    .line 6
    throw v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public ignorableWhitespace([CII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    invoke-super {p0, p1, p2, p3}, Lorg/xml/sax/helpers/XMLFilterImpl;->ignorableWhitespace([CII)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method protected indent()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getIndent()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-lez v1, :cond_0

    .line 14
    .line 15
    const/4 v1, 0x0

    .line 16
    :goto_0
    iget v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indentLevel:I

    .line 17
    .line 18
    if-ge v1, v2, :cond_0

    .line 19
    .line 20
    iget-object v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 21
    .line 22
    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    add-int/lit8 v1, v1, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected installLexicalHandler()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lorg/xml/sax/helpers/XMLFilterImpl;->getParent()Lorg/xml/sax/XMLReader;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    sget-object v2, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->LEXICAL_HANDLER_NAMES:[Ljava/lang/String;

    .line 9
    .line 10
    array-length v3, v2

    .line 11
    if-ge v1, v3, :cond_0

    .line 12
    .line 13
    :try_start_0
    aget-object v2, v2, v1

    .line 14
    .line 15
    invoke-interface {v0, v2, p0}, Lorg/xml/sax/XMLReader;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/xml/sax/SAXNotRecognizedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 16
    .line 17
    .line 18
    goto :goto_1

    .line 19
    :catch_0
    add-int/lit8 v1, v1, 0x1

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    :goto_1
    return-void

    .line 23
    :cond_1
    new-instance v0, Ljava/lang/NullPointerException;

    .line 24
    .line 25
    const-string v1, "No parent for filter"

    .line 26
    .line 27
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    throw v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected final isElementSpacePreserved(Lcom/intsig/office/fc/dom4j/Element;)Z
    .locals 2

    .line 1
    const-string v0, "space"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->preserve:Z

    .line 8
    .line 9
    if-eqz p1, :cond_1

    .line 10
    .line 11
    const-string v0, "xml"

    .line 12
    .line 13
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Attribute;->getNamespacePrefix()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    const-string v0, "preserve"

    .line 24
    .line 25
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    if-eqz p1, :cond_0

    .line 34
    .line 35
    const/4 v0, 0x1

    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const/4 v0, 0x0

    .line 38
    :cond_1
    :goto_0
    return v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public isEscapeText()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->escapeText:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected isExpandEmptyElements()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isExpandEmptyElements()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected isNamespaceDeclaration(Lcom/intsig/office/fc/dom4j/Namespace;)Z
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    sget-object v0, Lcom/intsig/office/fc/dom4j/Namespace;->XML_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 4
    .line 5
    if-eq p1, v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/Namespace;->getURI()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 14
    .line 15
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->contains(Lcom/intsig/office/fc/dom4j/Namespace;)Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-nez p1, :cond_0

    .line 20
    .line 21
    const/4 p1, 0x1

    .line 22
    return p1

    .line 23
    :cond_0
    const/4 p1, 0x0

    .line 24
    return p1
.end method

.method public notationDecl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    invoke-super {p0, p1, p2, p3}, Lorg/xml/sax/helpers/XMLFilterImpl;->notationDecl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public parse(Lorg/xml/sax/InputSource;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->installLexicalHandler()V

    .line 2
    .line 3
    .line 4
    invoke-super {p0, p1}, Lorg/xml/sax/helpers/XMLFilterImpl;->parse(Lorg/xml/sax/InputSource;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public println()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getLineSeparator()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public processingInstruction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indent()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 5
    .line 6
    const-string v1, "<?"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 17
    .line 18
    const-string v1, " "

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 24
    .line 25
    invoke-virtual {v0, p2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 29
    .line 30
    const-string v1, "?>"

    .line 31
    .line 32
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writePrintln()V

    .line 36
    .line 37
    .line 38
    const/4 v0, 0x7

    .line 39
    iput v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 40
    .line 41
    invoke-super {p0, p1, p2}, Lorg/xml/sax/helpers/XMLFilterImpl;->processingInstruction(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :catch_0
    move-exception p1

    .line 46
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->handleException(Ljava/io/IOException;)V

    .line 47
    .line 48
    .line 49
    :goto_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public resolveEntityRefs()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->resolveEntityRefs:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setDocumentLocator(Lorg/xml/sax/Locator;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lorg/xml/sax/helpers/XMLFilterImpl;->setDocumentLocator(Lorg/xml/sax/Locator;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setEscapeText(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->escapeText:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setIndentLevel(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indentLevel:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLexicalHandler(Lorg/xml/sax/ext/LexicalHandler;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    .line 7
    .line 8
    const-string v0, "Null lexical handler"

    .line 9
    .line 10
    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    throw p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMaximumAllowedCharacter(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->maximumAllowedCharacter:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOutputStream(Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getEncoding()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->createWriter(Ljava/io/OutputStream;Ljava/lang/String;)Ljava/io/Writer;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 12
    .line 13
    const/4 p1, 0x1

    .line 14
    iput-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXNotRecognizedException;,
            Lorg/xml/sax/SAXNotSupportedException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    sget-object v1, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->LEXICAL_HANDLER_NAMES:[Ljava/lang/String;

    .line 3
    .line 4
    array-length v2, v1

    .line 5
    if-ge v0, v2, :cond_1

    .line 6
    .line 7
    aget-object v1, v1, v0

    .line 8
    .line 9
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    check-cast p2, Lorg/xml/sax/ext/LexicalHandler;

    .line 16
    .line 17
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->setLexicalHandler(Lorg/xml/sax/ext/LexicalHandler;)V

    .line 18
    .line 19
    .line 20
    return-void

    .line 21
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    invoke-super {p0, p1, p2}, Lorg/xml/sax/helpers/XMLFilterImpl;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setResolveEntityRefs(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->resolveEntityRefs:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setWriter(Ljava/io/Writer;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    iput-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected shouldEncodeChar(C)Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->getMaximumAllowedCharacter()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-lez v0, :cond_0

    .line 6
    .line 7
    if-le p1, v0, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 p1, 0x0

    .line 12
    :goto_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public startCDATA()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 2
    .line 3
    const-string v1, "<![CDATA["

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :catch_0
    move-exception v0

    .line 10
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->handleException(Ljava/io/IOException;)V

    .line 11
    .line 12
    .line 13
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    invoke-interface {v0}, Lorg/xml/sax/ext/LexicalHandler;->startCDATA()V

    .line 18
    .line 19
    .line 20
    :cond_0
    return-void
    .line 21
.end method

.method public startDTD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->inDTD:Z

    .line 3
    .line 4
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeDocType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5
    .line 6
    .line 7
    goto :goto_0

    .line 8
    :catch_0
    move-exception v0

    .line 9
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->handleException(Ljava/io/IOException;)V

    .line 10
    .line 11
    .line 12
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    .line 13
    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    invoke-interface {v0, p1, p2, p3}, Lorg/xml/sax/ext/LexicalHandler;->startDTD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeDeclaration()V

    .line 2
    .line 3
    .line 4
    invoke-super {p0}, Lorg/xml/sax/helpers/XMLFilterImpl;->startDocument()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5
    .line 6
    .line 7
    goto :goto_0

    .line 8
    :catch_0
    move-exception v0

    .line 9
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->handleException(Ljava/io/IOException;)V

    .line 10
    .line 11
    .line 12
    :goto_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->charsAdded:Z

    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writePrintln()V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indent()V

    .line 8
    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 11
    .line 12
    const-string v2, "<"

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 18
    .line 19
    invoke-virtual {v1, p3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeNamespaces()V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0, p4}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeAttributes(Lorg/xml/sax/Attributes;)V

    .line 26
    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 29
    .line 30
    const-string v2, ">"

    .line 31
    .line 32
    invoke-virtual {v1, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    iget v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indentLevel:I

    .line 36
    .line 37
    const/4 v2, 0x1

    .line 38
    add-int/2addr v1, v2

    .line 39
    iput v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indentLevel:I

    .line 40
    .line 41
    iput v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 42
    .line 43
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastElementClosed:Z

    .line 44
    .line 45
    invoke-super {p0, p1, p2, p3, p4}, Lorg/xml/sax/helpers/XMLFilterImpl;->startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :catch_0
    move-exception p1

    .line 50
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->handleException(Ljava/io/IOException;)V

    .line 51
    .line 52
    .line 53
    :goto_0
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public startEntity(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeEntityRef(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    goto :goto_0

    .line 5
    :catch_0
    move-exception v0

    .line 6
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->handleException(Ljava/io/IOException;)V

    .line 7
    .line 8
    .line 9
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-interface {v0, p1}, Lorg/xml/sax/ext/LexicalHandler;->startEntity(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public startPrefixMapping(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespacesMap:Ljava/util/Map;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/HashMap;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespacesMap:Ljava/util/Map;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespacesMap:Ljava/util/Map;

    .line 13
    .line 14
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    invoke-super {p0, p1, p2}, Lorg/xml/sax/helpers/XMLFilterImpl;->startPrefixMapping(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public unparsedEntityDecl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Lorg/xml/sax/helpers/XMLFilterImpl;->unparsedEntityDecl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public write(Lcom/intsig/office/fc/dom4j/Attribute;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeAttribute(Lcom/intsig/office/fc/dom4j/Attribute;)V

    .line 2
    iget-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    if-eqz p1, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->flush()V

    :cond_0
    return-void
.end method

.method public write(Lcom/intsig/office/fc/dom4j/CDATA;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 17
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeCDATA(Ljava/lang/String;)V

    .line 18
    iget-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    if-eqz p1, :cond_0

    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->flush()V

    :cond_0
    return-void
.end method

.method public write(Lcom/intsig/office/fc/dom4j/Comment;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 20
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeComment(Ljava/lang/String;)V

    .line 21
    iget-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    if-eqz p1, :cond_0

    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->flush()V

    :cond_0
    return-void
.end method

.method public write(Lcom/intsig/office/fc/dom4j/Document;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeDeclaration()V

    .line 5
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getDocType()Lcom/intsig/office/fc/dom4j/DocumentType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indent()V

    .line 7
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getDocType()Lcom/intsig/office/fc/dom4j/DocumentType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeDocType(Lcom/intsig/office/fc/dom4j/DocumentType;)V

    .line 8
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Branch;->nodeCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 9
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Branch;->node(I)Lcom/intsig/office/fc/dom4j/Node;

    move-result-object v2

    .line 10
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeNode(Lcom/intsig/office/fc/dom4j/Node;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 11
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writePrintln()V

    .line 12
    iget-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    if-eqz p1, :cond_2

    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->flush()V

    :cond_2
    return-void
.end method

.method public write(Lcom/intsig/office/fc/dom4j/DocumentType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 23
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeDocType(Lcom/intsig/office/fc/dom4j/DocumentType;)V

    .line 24
    iget-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    if-eqz p1, :cond_0

    .line 25
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->flush()V

    :cond_0
    return-void
.end method

.method public write(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 14
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeElement(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 15
    iget-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    if-eqz p1, :cond_0

    .line 16
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->flush()V

    :cond_0
    return-void
.end method

.method public write(Lcom/intsig/office/fc/dom4j/Entity;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 26
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeEntity(Lcom/intsig/office/fc/dom4j/Entity;)V

    .line 27
    iget-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    if-eqz p1, :cond_0

    .line 28
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->flush()V

    :cond_0
    return-void
.end method

.method public write(Lcom/intsig/office/fc/dom4j/Namespace;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 29
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeNamespace(Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 30
    iget-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    if-eqz p1, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->flush()V

    :cond_0
    return-void
.end method

.method public write(Lcom/intsig/office/fc/dom4j/Node;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 41
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeNode(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 42
    iget-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    if-eqz p1, :cond_0

    .line 43
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->flush()V

    :cond_0
    return-void
.end method

.method public write(Lcom/intsig/office/fc/dom4j/ProcessingInstruction;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 32
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeProcessingInstruction(Lcom/intsig/office/fc/dom4j/ProcessingInstruction;)V

    .line 33
    iget-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    if-eqz p1, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->flush()V

    :cond_0
    return-void
.end method

.method public write(Lcom/intsig/office/fc/dom4j/Text;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 38
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeString(Ljava/lang/String;)V

    .line 39
    iget-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    if-eqz p1, :cond_0

    .line 40
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->flush()V

    :cond_0
    return-void
.end method

.method public write(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 44
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/Node;

    if-eqz v0, :cond_0

    .line 45
    check-cast p1, Lcom/intsig/office/fc/dom4j/Node;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->write(Lcom/intsig/office/fc/dom4j/Node;)V

    goto :goto_1

    .line 46
    :cond_0
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 47
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->write(Ljava/lang/String;)V

    goto :goto_1

    .line 48
    :cond_1
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_2

    .line 49
    check-cast p1, Ljava/util/List;

    .line 50
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_3

    .line 51
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->write(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    if-nez p1, :cond_4

    :cond_3
    :goto_1
    return-void

    .line 52
    :cond_4
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid object: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public write(Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 35
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeString(Ljava/lang/String;)V

    .line 36
    iget-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->autoFlush:Z

    if-eqz p1, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->flush()V

    :cond_0
    return-void
.end method

.method protected writeAttribute(Lcom/intsig/office/fc/dom4j/Attribute;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Attribute;->getQualifiedName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getAttributeQuoteCharacter()C

    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(I)V

    .line 6
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeEscapeAttributeEntities(Ljava/lang/String;)V

    .line 7
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(I)V

    const/4 p1, 0x2

    .line 8
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    return-void
.end method

.method protected writeAttribute(Lorg/xml/sax/Attributes;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getAttributeQuoteCharacter()C

    move-result v0

    .line 10
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 11
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-interface {p1, p2}, Lorg/xml/sax/Attributes;->getQName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 12
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 13
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(I)V

    .line 14
    invoke-interface {p1, p2}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeEscapeAttributeEntities(Ljava/lang/String;)V

    .line 15
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(I)V

    return-void
.end method

.method protected writeAttributes(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->attributeCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_4

    .line 2
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attribute(I)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v2

    .line 3
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Attribute;->getNamespace()Lcom/intsig/office/fc/dom4j/Namespace;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 4
    sget-object v4, Lcom/intsig/office/fc/dom4j/Namespace;->NO_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    if-eq v3, v4, :cond_0

    sget-object v4, Lcom/intsig/office/fc/dom4j/Namespace;->XML_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    if-eq v3, v4, :cond_0

    .line 5
    invoke-virtual {v3}, Lcom/intsig/office/fc/dom4j/Namespace;->getPrefix()Ljava/lang/String;

    move-result-object v4

    .line 6
    iget-object v5, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-virtual {v5, v4}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->getURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 7
    invoke-virtual {v3}, Lcom/intsig/office/fc/dom4j/Namespace;->getURI()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 8
    invoke-virtual {p0, v3}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeNamespace(Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 9
    iget-object v4, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-virtual {v4, v3}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->push(Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 10
    :cond_0
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "xmlns:"

    .line 11
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x6

    .line 12
    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 13
    iget-object v4, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-virtual {v4, v3}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->getNamespaceForPrefix(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    move-result-object v4

    if-nez v4, :cond_3

    .line 14
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 15
    iget-object v4, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-virtual {v4, v3, v2}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->push(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    invoke-virtual {p0, v3, v2}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeNamespace(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const-string v4, "xmlns"

    .line 17
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 18
    iget-object v3, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    invoke-virtual {v3}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->getDefaultNamespace()Lcom/intsig/office/fc/dom4j/Namespace;

    move-result-object v3

    if-nez v3, :cond_3

    .line 19
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 20
    iget-object v3, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v2}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->push(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    invoke-virtual {p0, v4, v2}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeNamespace(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 22
    :cond_2
    iget-object v3, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    invoke-virtual {v3}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getAttributeQuoteCharacter()C

    move-result v3

    .line 23
    iget-object v4, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 24
    iget-object v4, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Attribute;->getQualifiedName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 25
    iget-object v4, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 26
    iget-object v4, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-virtual {v4, v3}, Ljava/io/Writer;->write(I)V

    .line 27
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeEscapeAttributeEntities(Ljava/lang/String;)V

    .line 28
    iget-object v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(I)V

    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_4
    return-void
.end method

.method protected writeAttributes(Lorg/xml/sax/Attributes;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 29
    invoke-interface {p1}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 30
    invoke-virtual {p0, p1, v1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeAttribute(Lorg/xml/sax/Attributes;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected writeCDATA(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 2
    .line 3
    const-string v1, "<![CDATA["

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 11
    .line 12
    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 16
    .line 17
    const-string v0, "]]>"

    .line 18
    .line 19
    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    const/4 p1, 0x4

    .line 23
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public writeClose(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getQualifiedName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeClose(Ljava/lang/String;)V

    return-void
.end method

.method protected writeClose(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string v1, "</"

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 4
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string v0, ">"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    return-void
.end method

.method protected writeComment(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isNewlines()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->println()V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indent()V

    .line 13
    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 16
    .line 17
    const-string v1, "<!--"

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 23
    .line 24
    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 28
    .line 29
    const-string v0, "-->"

    .line 30
    .line 31
    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const/16 p1, 0x8

    .line 35
    .line 36
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected writeDeclaration()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getEncoding()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isSuppressDeclaration()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-nez v1, :cond_3

    .line 14
    .line 15
    const-string v1, "UTF8"

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    const-string v2, "?>"

    .line 22
    .line 23
    const-string v3, "<?xml version=\"1.0\""

    .line 24
    .line 25
    if-eqz v1, :cond_1

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 28
    .line 29
    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isOmitEncoding()Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    if-nez v0, :cond_0

    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 41
    .line 42
    const-string v1, " encoding=\"UTF-8\""

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 48
    .line 49
    invoke-virtual {v0, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 54
    .line 55
    invoke-virtual {v1, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 59
    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isOmitEncoding()Z

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    if-nez v1, :cond_2

    .line 65
    .line 66
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 67
    .line 68
    new-instance v3, Ljava/lang/StringBuilder;

    .line 69
    .line 70
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 71
    .line 72
    .line 73
    const-string v4, " encoding=\""

    .line 74
    .line 75
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    const-string v0, "\""

    .line 82
    .line 83
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 94
    .line 95
    invoke-virtual {v0, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 99
    .line 100
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isNewLineAfterDeclaration()Z

    .line 101
    .line 102
    .line 103
    move-result v0

    .line 104
    if-eqz v0, :cond_3

    .line 105
    .line 106
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->println()V

    .line 107
    .line 108
    .line 109
    :cond_3
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method protected writeDocType(Lcom/intsig/office/fc/dom4j/DocumentType;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Node;->write(Ljava/io/Writer;)V

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writePrintln()V

    :cond_0
    return-void
.end method

.method protected writeDocType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string v1, "<!DOCTYPE "

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string p1, "\""

    const-string v0, ""

    if-eqz p2, :cond_0

    .line 5
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string v2, " PUBLIC \""

    invoke-virtual {v1, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-virtual {v1, p2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 8
    iget-object p2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-virtual {p2, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p3, :cond_2

    .line 9
    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    if-nez p2, :cond_1

    .line 10
    iget-object p2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string v0, " SYSTEM"

    invoke-virtual {p2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 11
    :cond_1
    iget-object p2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string v0, " \""

    invoke-virtual {p2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 12
    iget-object p2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-virtual {p2, p3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 13
    iget-object p2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-virtual {p2, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 14
    :cond_2
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string p2, ">"

    invoke-virtual {p1, p2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writePrintln()V

    return-void
.end method

.method protected writeElement(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Branch;->nodeCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getQualifiedName()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writePrintln()V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indent()V

    .line 13
    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 16
    .line 17
    const-string v3, "<"

    .line 18
    .line 19
    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    iget-object v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 23
    .line 24
    invoke-virtual {v2, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-object v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 28
    .line 29
    invoke-virtual {v2}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->size()I

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getNamespace()Lcom/intsig/office/fc/dom4j/Namespace;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    invoke-virtual {p0, v3}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->isNamespaceDeclaration(Lcom/intsig/office/fc/dom4j/Namespace;)Z

    .line 38
    .line 39
    .line 40
    move-result v4

    .line 41
    if-eqz v4, :cond_0

    .line 42
    .line 43
    iget-object v4, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 44
    .line 45
    invoke-virtual {v4, v3}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->push(Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p0, v3}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeNamespace(Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 49
    .line 50
    .line 51
    :cond_0
    const/4 v3, 0x0

    .line 52
    const/4 v4, 0x1

    .line 53
    const/4 v5, 0x0

    .line 54
    const/4 v6, 0x1

    .line 55
    :goto_0
    if-ge v5, v0, :cond_4

    .line 56
    .line 57
    invoke-interface {p1, v5}, Lcom/intsig/office/fc/dom4j/Branch;->node(I)Lcom/intsig/office/fc/dom4j/Node;

    .line 58
    .line 59
    .line 60
    move-result-object v7

    .line 61
    instance-of v8, v7, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 62
    .line 63
    if-eqz v8, :cond_1

    .line 64
    .line 65
    check-cast v7, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 66
    .line 67
    invoke-virtual {p0, v7}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->isNamespaceDeclaration(Lcom/intsig/office/fc/dom4j/Namespace;)Z

    .line 68
    .line 69
    .line 70
    move-result v8

    .line 71
    if-eqz v8, :cond_3

    .line 72
    .line 73
    iget-object v8, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 74
    .line 75
    invoke-virtual {v8, v7}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->push(Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 76
    .line 77
    .line 78
    invoke-virtual {p0, v7}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeNamespace(Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 79
    .line 80
    .line 81
    goto :goto_2

    .line 82
    :cond_1
    instance-of v8, v7, Lcom/intsig/office/fc/dom4j/Element;

    .line 83
    .line 84
    if-eqz v8, :cond_2

    .line 85
    .line 86
    :goto_1
    const/4 v6, 0x0

    .line 87
    goto :goto_2

    .line 88
    :cond_2
    instance-of v7, v7, Lcom/intsig/office/fc/dom4j/Comment;

    .line 89
    .line 90
    if-eqz v7, :cond_3

    .line 91
    .line 92
    goto :goto_1

    .line 93
    :cond_3
    :goto_2
    add-int/lit8 v5, v5, 0x1

    .line 94
    .line 95
    goto :goto_0

    .line 96
    :cond_4
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeAttributes(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 97
    .line 98
    .line 99
    iput v4, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 100
    .line 101
    if-gtz v0, :cond_5

    .line 102
    .line 103
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeEmptyElementClose(Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    goto :goto_4

    .line 107
    :cond_5
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 108
    .line 109
    const-string v3, ">"

    .line 110
    .line 111
    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    if-eqz v6, :cond_6

    .line 115
    .line 116
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeElementContent(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 117
    .line 118
    .line 119
    goto :goto_3

    .line 120
    :cond_6
    iget v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indentLevel:I

    .line 121
    .line 122
    add-int/2addr v0, v4

    .line 123
    iput v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indentLevel:I

    .line 124
    .line 125
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeElementContent(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 126
    .line 127
    .line 128
    iget p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indentLevel:I

    .line 129
    .line 130
    sub-int/2addr p1, v4

    .line 131
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indentLevel:I

    .line 132
    .line 133
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writePrintln()V

    .line 134
    .line 135
    .line 136
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->indent()V

    .line 137
    .line 138
    .line 139
    :goto_3
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 140
    .line 141
    const-string v0, "</"

    .line 142
    .line 143
    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 144
    .line 145
    .line 146
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 147
    .line 148
    invoke-virtual {p1, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 152
    .line 153
    invoke-virtual {p1, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 154
    .line 155
    .line 156
    :goto_4
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 157
    .line 158
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->size()I

    .line 159
    .line 160
    .line 161
    move-result p1

    .line 162
    if-le p1, v2, :cond_7

    .line 163
    .line 164
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespaceStack:Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;

    .line 165
    .line 166
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/tree/NamespaceStack;->pop()Lcom/intsig/office/fc/dom4j/Namespace;

    .line 167
    .line 168
    .line 169
    goto :goto_4

    .line 170
    :cond_7
    iput v4, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 171
    .line 172
    return-void
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method protected writeElementContent(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isTrimText()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->preserve:Z

    .line 8
    .line 9
    const/4 v2, 0x1

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->isElementSpacePreserved(Lcom/intsig/office/fc/dom4j/Element;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    iput-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->preserve:Z

    .line 17
    .line 18
    xor-int/2addr v0, v2

    .line 19
    :cond_0
    const-string v3, " "

    .line 20
    .line 21
    const/4 v4, 0x0

    .line 22
    const/4 v5, 0x0

    .line 23
    if-eqz v0, :cond_f

    .line 24
    .line 25
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Branch;->nodeCount()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    move-object v7, v4

    .line 30
    move-object v9, v7

    .line 31
    const/4 v6, 0x0

    .line 32
    const/4 v8, 0x1

    .line 33
    :goto_0
    if-ge v6, v0, :cond_b

    .line 34
    .line 35
    invoke-interface {p1, v6}, Lcom/intsig/office/fc/dom4j/Branch;->node(I)Lcom/intsig/office/fc/dom4j/Node;

    .line 36
    .line 37
    .line 38
    move-result-object v10

    .line 39
    instance-of v11, v10, Lcom/intsig/office/fc/dom4j/Text;

    .line 40
    .line 41
    if-eqz v11, :cond_3

    .line 42
    .line 43
    if-nez v7, :cond_1

    .line 44
    .line 45
    move-object v7, v10

    .line 46
    check-cast v7, Lcom/intsig/office/fc/dom4j/Text;

    .line 47
    .line 48
    goto/16 :goto_4

    .line 49
    .line 50
    :cond_1
    if-nez v9, :cond_2

    .line 51
    .line 52
    new-instance v9, Ljava/lang/StringBuffer;

    .line 53
    .line 54
    invoke-interface {v7}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v11

    .line 58
    invoke-direct {v9, v11}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    :cond_2
    check-cast v10, Lcom/intsig/office/fc/dom4j/Text;

    .line 62
    .line 63
    invoke-interface {v10}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v10

    .line 67
    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 68
    .line 69
    .line 70
    goto/16 :goto_4

    .line 71
    .line 72
    :cond_3
    if-nez v8, :cond_6

    .line 73
    .line 74
    iget-object v8, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 75
    .line 76
    invoke-virtual {v8}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isPadText()Z

    .line 77
    .line 78
    .line 79
    move-result v8

    .line 80
    if-eqz v8, :cond_6

    .line 81
    .line 82
    if-eqz v9, :cond_4

    .line 83
    .line 84
    invoke-virtual {v9, v5}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 85
    .line 86
    .line 87
    move-result v8

    .line 88
    goto :goto_1

    .line 89
    :cond_4
    if-eqz v7, :cond_5

    .line 90
    .line 91
    invoke-interface {v7}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v8

    .line 95
    invoke-virtual {v8, v5}, Ljava/lang/String;->charAt(I)C

    .line 96
    .line 97
    .line 98
    move-result v8

    .line 99
    goto :goto_1

    .line 100
    :cond_5
    const/16 v8, 0x61

    .line 101
    .line 102
    :goto_1
    invoke-static {v8}, Ljava/lang/Character;->isWhitespace(C)Z

    .line 103
    .line 104
    .line 105
    move-result v8

    .line 106
    if-eqz v8, :cond_6

    .line 107
    .line 108
    iget-object v8, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 109
    .line 110
    invoke-virtual {v8, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    :cond_6
    if-eqz v7, :cond_a

    .line 114
    .line 115
    if-eqz v9, :cond_7

    .line 116
    .line 117
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object v8

    .line 121
    invoke-virtual {p0, v8}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeString(Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    move-object v9, v4

    .line 125
    goto :goto_2

    .line 126
    :cond_7
    invoke-interface {v7}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 127
    .line 128
    .line 129
    move-result-object v8

    .line 130
    invoke-virtual {p0, v8}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeString(Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    :goto_2
    iget-object v8, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 134
    .line 135
    invoke-virtual {v8}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isPadText()Z

    .line 136
    .line 137
    .line 138
    move-result v8

    .line 139
    if-eqz v8, :cond_9

    .line 140
    .line 141
    if-eqz v9, :cond_8

    .line 142
    .line 143
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    .line 144
    .line 145
    .line 146
    move-result v7

    .line 147
    sub-int/2addr v7, v2

    .line 148
    invoke-virtual {v9, v7}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 149
    .line 150
    .line 151
    move-result v7

    .line 152
    goto :goto_3

    .line 153
    :cond_8
    invoke-interface {v7}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 154
    .line 155
    .line 156
    move-result-object v7

    .line 157
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    .line 158
    .line 159
    .line 160
    move-result v8

    .line 161
    sub-int/2addr v8, v2

    .line 162
    invoke-virtual {v7, v8}, Ljava/lang/String;->charAt(I)C

    .line 163
    .line 164
    .line 165
    move-result v7

    .line 166
    :goto_3
    invoke-static {v7}, Ljava/lang/Character;->isWhitespace(C)Z

    .line 167
    .line 168
    .line 169
    move-result v7

    .line 170
    if-eqz v7, :cond_9

    .line 171
    .line 172
    iget-object v7, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 173
    .line 174
    invoke-virtual {v7, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 175
    .line 176
    .line 177
    :cond_9
    move-object v7, v4

    .line 178
    :cond_a
    invoke-virtual {p0, v10}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeNode(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 179
    .line 180
    .line 181
    const/4 v8, 0x0

    .line 182
    :goto_4
    add-int/lit8 v6, v6, 0x1

    .line 183
    .line 184
    goto/16 :goto_0

    .line 185
    .line 186
    :cond_b
    if-eqz v7, :cond_12

    .line 187
    .line 188
    if-nez v8, :cond_d

    .line 189
    .line 190
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 191
    .line 192
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isPadText()Z

    .line 193
    .line 194
    .line 195
    move-result p1

    .line 196
    if-eqz p1, :cond_d

    .line 197
    .line 198
    if-eqz v9, :cond_c

    .line 199
    .line 200
    invoke-virtual {v9, v5}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 201
    .line 202
    .line 203
    move-result p1

    .line 204
    goto :goto_5

    .line 205
    :cond_c
    invoke-interface {v7}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 206
    .line 207
    .line 208
    move-result-object p1

    .line 209
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    .line 210
    .line 211
    .line 212
    move-result p1

    .line 213
    :goto_5
    invoke-static {p1}, Ljava/lang/Character;->isWhitespace(C)Z

    .line 214
    .line 215
    .line 216
    move-result p1

    .line 217
    if-eqz p1, :cond_d

    .line 218
    .line 219
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 220
    .line 221
    invoke-virtual {p1, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 222
    .line 223
    .line 224
    :cond_d
    if-eqz v9, :cond_e

    .line 225
    .line 226
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 227
    .line 228
    .line 229
    move-result-object p1

    .line 230
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeString(Ljava/lang/String;)V

    .line 231
    .line 232
    .line 233
    goto :goto_8

    .line 234
    :cond_e
    invoke-interface {v7}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 235
    .line 236
    .line 237
    move-result-object p1

    .line 238
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeString(Ljava/lang/String;)V

    .line 239
    .line 240
    .line 241
    goto :goto_8

    .line 242
    :cond_f
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Branch;->nodeCount()I

    .line 243
    .line 244
    .line 245
    move-result v0

    .line 246
    move-object v6, v4

    .line 247
    :goto_6
    if-ge v5, v0, :cond_12

    .line 248
    .line 249
    invoke-interface {p1, v5}, Lcom/intsig/office/fc/dom4j/Branch;->node(I)Lcom/intsig/office/fc/dom4j/Node;

    .line 250
    .line 251
    .line 252
    move-result-object v7

    .line 253
    instance-of v8, v7, Lcom/intsig/office/fc/dom4j/Text;

    .line 254
    .line 255
    if-eqz v8, :cond_10

    .line 256
    .line 257
    invoke-virtual {p0, v7}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeNode(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 258
    .line 259
    .line 260
    move-object v6, v7

    .line 261
    goto :goto_7

    .line 262
    :cond_10
    if-eqz v6, :cond_11

    .line 263
    .line 264
    iget-object v8, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 265
    .line 266
    invoke-virtual {v8}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isPadText()Z

    .line 267
    .line 268
    .line 269
    move-result v8

    .line 270
    if-eqz v8, :cond_11

    .line 271
    .line 272
    invoke-interface {v6}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 273
    .line 274
    .line 275
    move-result-object v6

    .line 276
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    .line 277
    .line 278
    .line 279
    move-result v8

    .line 280
    sub-int/2addr v8, v2

    .line 281
    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    .line 282
    .line 283
    .line 284
    move-result v6

    .line 285
    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    .line 286
    .line 287
    .line 288
    move-result v6

    .line 289
    if-eqz v6, :cond_11

    .line 290
    .line 291
    iget-object v6, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 292
    .line 293
    invoke-virtual {v6, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 294
    .line 295
    .line 296
    :cond_11
    invoke-virtual {p0, v7}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeNode(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 297
    .line 298
    .line 299
    move-object v6, v4

    .line 300
    :goto_7
    add-int/lit8 v5, v5, 0x1

    .line 301
    .line 302
    goto :goto_6

    .line 303
    :cond_12
    :goto_8
    iput-boolean v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->preserve:Z

    .line 304
    .line 305
    return-void
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method protected writeEmptyElementClose(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isExpandEmptyElements()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 10
    .line 11
    const-string v0, "/>"

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 18
    .line 19
    const-string v1, "></"

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 25
    .line 26
    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 30
    .line 31
    const-string v0, ">"

    .line 32
    .line 33
    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    :goto_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected writeEntity(Lcom/intsig/office/fc/dom4j/Entity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->resolveEntityRefs()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeEntityRef(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 16
    .line 17
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    :goto_0
    return-void
.end method

.method protected writeEntityRef(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 2
    .line 3
    const-string v1, "&"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 14
    .line 15
    const-string v0, ";"

    .line 16
    .line 17
    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    const/4 p1, 0x5

    .line 21
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 22
    .line 23
    return-void
    .line 24
.end method

.method protected writeEscapeAttributeEntities(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->escapeAttributeEntities(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 8
    .line 9
    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected writeNamespace(Lcom/intsig/office/fc/dom4j/Namespace;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/Namespace;->getPrefix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/Namespace;->getURI()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeNamespace(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected writeNamespace(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string v1, " xmlns:"

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 5
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string v0, "=\""

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_0

    .line 6
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string v0, " xmlns=\""

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 7
    :goto_0
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    invoke-virtual {p1, p2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    const-string p2, "\""

    invoke-virtual {p1, p2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    return-void
.end method

.method protected writeNamespaces()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespacesMap:Ljava/util/Map;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    check-cast v1, Ljava/util/Map$Entry;

    .line 24
    .line 25
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    check-cast v2, Ljava/lang/String;

    .line 30
    .line 31
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    check-cast v1, Ljava/lang/String;

    .line 36
    .line 37
    invoke-virtual {p0, v2, v1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeNamespace(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_0
    const/4 v0, 0x0

    .line 42
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->namespacesMap:Ljava/util/Map;

    .line 43
    .line 44
    :cond_1
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected writeNode(Lcom/intsig/office/fc/dom4j/Node;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getNodeType()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    packed-switch v0, :pswitch_data_0

    .line 6
    .line 7
    .line 8
    :pswitch_0
    new-instance v0, Ljava/io/IOException;

    .line 9
    .line 10
    new-instance v1, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v2, "Invalid node type: "

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    throw v0

    .line 31
    :pswitch_1
    check-cast p1, Lcom/intsig/office/fc/dom4j/DocumentType;

    .line 32
    .line 33
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeDocType(Lcom/intsig/office/fc/dom4j/DocumentType;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :pswitch_2
    check-cast p1, Lcom/intsig/office/fc/dom4j/Document;

    .line 38
    .line 39
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->write(Lcom/intsig/office/fc/dom4j/Document;)V

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :pswitch_3
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeComment(Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :pswitch_4
    check-cast p1, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    .line 52
    .line 53
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeProcessingInstruction(Lcom/intsig/office/fc/dom4j/ProcessingInstruction;)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :pswitch_5
    check-cast p1, Lcom/intsig/office/fc/dom4j/Entity;

    .line 58
    .line 59
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeEntity(Lcom/intsig/office/fc/dom4j/Entity;)V

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :pswitch_6
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeCDATA(Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    goto :goto_0

    .line 71
    :pswitch_7
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeNodeText(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 72
    .line 73
    .line 74
    goto :goto_0

    .line 75
    :pswitch_8
    check-cast p1, Lcom/intsig/office/fc/dom4j/Attribute;

    .line 76
    .line 77
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeAttribute(Lcom/intsig/office/fc/dom4j/Attribute;)V

    .line 78
    .line 79
    .line 80
    goto :goto_0

    .line 81
    :pswitch_9
    check-cast p1, Lcom/intsig/office/fc/dom4j/Element;

    .line 82
    .line 83
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeElement(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 84
    .line 85
    .line 86
    :goto_0
    :pswitch_a
    return-void

    .line 87
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method protected writeNodeText(Lcom/intsig/office/fc/dom4j/Node;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-eqz p1, :cond_1

    .line 6
    .line 7
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-lez v0, :cond_1

    .line 12
    .line 13
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->escapeText:Z

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->escapeElementEntities(Ljava/lang/String;)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    :cond_0
    const/4 v0, 0x3

    .line 22
    iput v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 25
    .line 26
    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    add-int/lit8 v0, v0, -0x1

    .line 34
    .line 35
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    iput-char p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastChar:C

    .line 40
    .line 41
    :cond_1
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public writeOpen(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 2
    .line 3
    const-string v1, "<"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 9
    .line 10
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getQualifiedName()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writeAttributes(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 18
    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 21
    .line 22
    const-string v0, ">"

    .line 23
    .line 24
    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected writePrintln()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isNewlines()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getLineSeparator()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iget-char v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastChar:C

    .line 16
    .line 17
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    add-int/lit8 v2, v2, -0x1

    .line 22
    .line 23
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eq v1, v0, :cond_0

    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 32
    .line 33
    invoke-virtual {v1}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->getLineSeparator()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    :cond_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected writeProcessingInstruction(Lcom/intsig/office/fc/dom4j/ProcessingInstruction;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 2
    .line 3
    const-string v1, "<?"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 9
    .line 10
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 18
    .line 19
    const-string v1, " "

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 25
    .line 26
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;->getText()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 34
    .line 35
    const-string v0, "?>"

    .line 36
    .line 37
    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writePrintln()V

    .line 41
    .line 42
    .line 43
    const/4 p1, 0x7

    .line 44
    iput p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected writeString(Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_4

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_4

    .line 8
    .line 9
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->escapeText:Z

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->escapeElementEntities(Ljava/lang/String;)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->format:Lcom/intsig/office/fc/dom4j/io/OutputFormat;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/OutputFormat;->isTrimText()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    const/4 v1, 0x1

    .line 24
    const/4 v2, 0x3

    .line 25
    if-eqz v0, :cond_3

    .line 26
    .line 27
    new-instance v0, Ljava/util/StringTokenizer;

    .line 28
    .line 29
    invoke-direct {v0, p1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    const/4 p1, 0x1

    .line 33
    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    .line 34
    .line 35
    .line 36
    move-result v3

    .line 37
    if-eqz v3, :cond_4

    .line 38
    .line 39
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    const-string v4, " "

    .line 44
    .line 45
    if-eqz p1, :cond_2

    .line 46
    .line 47
    iget p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 48
    .line 49
    if-ne p1, v2, :cond_1

    .line 50
    .line 51
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 52
    .line 53
    invoke-virtual {p1, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    :cond_1
    const/4 p1, 0x0

    .line 57
    goto :goto_1

    .line 58
    :cond_2
    iget-object v5, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 59
    .line 60
    invoke-virtual {v5, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    :goto_1
    iget-object v4, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 64
    .line 65
    invoke-virtual {v4, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    iput v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 69
    .line 70
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    .line 71
    .line 72
    .line 73
    move-result v4

    .line 74
    sub-int/2addr v4, v1

    .line 75
    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    .line 76
    .line 77
    .line 78
    move-result v3

    .line 79
    iput-char v3, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastChar:C

    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_3
    iput v2, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastOutputNodeType:I

    .line 83
    .line 84
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->writer:Ljava/io/Writer;

    .line 85
    .line 86
    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    sub-int/2addr v0, v1

    .line 94
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    .line 95
    .line 96
    .line 97
    move-result p1

    .line 98
    iput-char p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;->lastChar:C

    .line 99
    .line 100
    :cond_4
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

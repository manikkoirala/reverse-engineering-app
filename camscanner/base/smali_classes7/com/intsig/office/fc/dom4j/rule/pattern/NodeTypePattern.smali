.class public Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;
.super Ljava/lang/Object;
.source "NodeTypePattern.java"

# interfaces
.implements Lcom/intsig/office/fc/dom4j/rule/Pattern;


# static fields
.field public static final ANY_ATTRIBUTE:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

.field public static final ANY_COMMENT:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

.field public static final ANY_DOCUMENT:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

.field public static final ANY_ELEMENT:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

.field public static final ANY_PROCESSING_INSTRUCTION:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

.field public static final ANY_TEXT:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;


# instance fields
.field private nodeType:S


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;-><init>(S)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;->ANY_ATTRIBUTE:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 10
    .line 11
    const/16 v1, 0x8

    .line 12
    .line 13
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;-><init>(S)V

    .line 14
    .line 15
    .line 16
    sput-object v0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;->ANY_COMMENT:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 19
    .line 20
    const/16 v1, 0x9

    .line 21
    .line 22
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;-><init>(S)V

    .line 23
    .line 24
    .line 25
    sput-object v0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;->ANY_DOCUMENT:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 26
    .line 27
    new-instance v0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 28
    .line 29
    const/4 v1, 0x1

    .line 30
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;-><init>(S)V

    .line 31
    .line 32
    .line 33
    sput-object v0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;->ANY_ELEMENT:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 34
    .line 35
    new-instance v0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 36
    .line 37
    const/4 v1, 0x7

    .line 38
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;-><init>(S)V

    .line 39
    .line 40
    .line 41
    sput-object v0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;->ANY_PROCESSING_INSTRUCTION:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 42
    .line 43
    new-instance v0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 44
    .line 45
    const/4 v1, 0x3

    .line 46
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;-><init>(S)V

    .line 47
    .line 48
    .line 49
    sput-object v0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;->ANY_TEXT:Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;

    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>(S)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-short p1, p0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;->nodeType:S

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public getMatchType()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;->nodeType:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMatchesNodeName()Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPriority()D
    .locals 2

    .line 1
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getUnionPatterns()[Lcom/intsig/office/fc/dom4j/rule/Pattern;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public matches(Lcom/intsig/office/fc/dom4j/Node;)Z
    .locals 1

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getNodeType()S

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    iget-short v0, p0, Lcom/intsig/office/fc/dom4j/rule/pattern/NodeTypePattern;->nodeType:S

    .line 6
    .line 7
    if-ne p1, v0, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 p1, 0x0

    .line 12
    :goto_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

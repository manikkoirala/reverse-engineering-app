.class public abstract Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;
.super Ljava/lang/Object;
.source "ProxyDocumentFactory.java"


# instance fields
.field private proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    return-void
.end method


# virtual methods
.method public createAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object p1

    return-object p1
.end method

.method public createAttribute(Lcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createAttribute(Lcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object p1

    return-object p1
.end method

.method public createCDATA(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/CDATA;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createCDATA(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/CDATA;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public createComment(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Comment;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createComment(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Comment;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public createDocType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/DocumentType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createDocType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/DocumentType;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public createDocument()Lcom/intsig/office/fc/dom4j/Document;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createDocument()Lcom/intsig/office/fc/dom4j/Document;

    move-result-object v0

    return-object v0
.end method

.method public createDocument(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createDocument(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public createElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    return-object p1
.end method

.method public createElement(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createElement(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    return-object p1
.end method

.method public createEntity(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Entity;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createEntity(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Entity;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public createNamespace(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createNamespace(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public createPattern(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/rule/Pattern;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createPattern(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/rule/Pattern;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public createProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/ProcessingInstruction;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    move-result-object p1

    return-object p1
.end method

.method public createProcessingInstruction(Ljava/lang/String;Ljava/util/Map;)Lcom/intsig/office/fc/dom4j/ProcessingInstruction;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createProcessingInstruction(Ljava/lang/String;Ljava/util/Map;)Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    move-result-object p1

    return-object p1
.end method

.method public createQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    return-object p1
.end method

.method public createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    return-object p1
.end method

.method public createQName(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    return-object p1
.end method

.method public createQName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    return-object p1
.end method

.method public createText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Text;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Text;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public createXPath(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/XPath;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createXPath(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/XPath;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public createXPathFilter(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/NodeFilter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createXPathFilter(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/NodeFilter;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected getProxy()Lcom/intsig/office/fc/dom4j/DocumentFactory;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected setProxy(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V
    .locals 0

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->getInstance()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    :cond_0
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/util/ProxyDocumentFactory;->proxy:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class Lcom/intsig/office/fc/dom4j/io/ElementStack;
.super Ljava/lang/Object;
.source "ElementStack.java"

# interfaces
.implements Lcom/intsig/office/fc/dom4j/ElementPath;


# instance fields
.field protected 〇080:[Lcom/intsig/office/fc/dom4j/Element;

.field protected 〇o00〇〇Oo:I

.field private 〇o〇:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x32

    .line 1
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/dom4j/io/ElementStack;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 3
    iput v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o00〇〇Oo:I

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o〇:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 5
    new-array p1, p1, [Lcom/intsig/office/fc/dom4j/Element;

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇080:[Lcom/intsig/office/fc/dom4j/Element;

    return-void
.end method

.method private 〇o〇(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o〇:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;-><init>()V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/ElementStack;->oO80(Lcom/intsig/office/fc/dom4j/io/DispatchHandler;)V

    .line 11
    .line 12
    .line 13
    :cond_0
    const-string v0, "/"

    .line 14
    .line 15
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/ElementStack;->getPath()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    if-eqz v1, :cond_2

    .line 31
    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/ElementStack;->getPath()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    goto :goto_0

    .line 52
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    .line 53
    .line 54
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 55
    .line 56
    .line 57
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/ElementStack;->getPath()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    :goto_0
    return-object p1
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public O8()Lcom/intsig/office/fc/dom4j/Element;
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o00〇〇Oo:I

    .line 2
    .line 3
    if-gez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇080:[Lcom/intsig/office/fc/dom4j/Element;

    .line 8
    .line 9
    aget-object v0, v1, v0

    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oo08()Lcom/intsig/office/fc/dom4j/Element;
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o00〇〇Oo:I

    .line 2
    .line 3
    if-gez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇080:[Lcom/intsig/office/fc/dom4j/Element;

    .line 8
    .line 9
    add-int/lit8 v2, v0, -0x1

    .line 10
    .line 11
    iput v2, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o00〇〇Oo:I

    .line 12
    .line 13
    aget-object v0, v1, v0

    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public addHandler(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o〇:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o〇(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;->〇080(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public getCurrent()Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/ElementStack;->O8()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getElement(I)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇080:[Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    aget-object p1, v0, p1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :catch_0
    const/4 p1, 0x0

    .line 7
    :goto_0
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o〇:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;-><init>()V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/ElementStack;->oO80(Lcom/intsig/office/fc/dom4j/io/DispatchHandler;)V

    .line 11
    .line 12
    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o〇:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;->O8()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    return-object v0
    .line 20
    .line 21
.end method

.method public oO80(Lcom/intsig/office/fc/dom4j/io/DispatchHandler;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o〇:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public o〇0(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇080:[Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    iget v1, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o00〇〇Oo:I

    .line 5
    .line 6
    add-int/lit8 v1, v1, 0x1

    .line 7
    .line 8
    iput v1, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o00〇〇Oo:I

    .line 9
    .line 10
    if-lt v1, v0, :cond_0

    .line 11
    .line 12
    mul-int/lit8 v0, v0, 0x2

    .line 13
    .line 14
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇〇888(I)V

    .line 15
    .line 16
    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇080:[Lcom/intsig/office/fc/dom4j/Element;

    .line 18
    .line 19
    iget v1, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o00〇〇Oo:I

    .line 20
    .line 21
    aput-object p1, v0, v1

    .line 22
    .line 23
    return-void
    .line 24
.end method

.method public removeHandler(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o〇:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o〇(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/DispatchHandler;->Oo08(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/ElementHandler;

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public size()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o00〇〇Oo:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080()V
    .locals 1

    .line 1
    const/4 v0, -0x1

    .line 2
    iput v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o00〇〇Oo:I

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo()Lcom/intsig/office/fc/dom4j/io/DispatchHandler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇o〇:Lcom/intsig/office/fc/dom4j/io/DispatchHandler;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected 〇〇888(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇080:[Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    new-array p1, p1, [Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/ElementStack;->〇080:[Lcom/intsig/office/fc/dom4j/Element;

    .line 6
    .line 7
    array-length v1, v0

    .line 8
    const/4 v2, 0x0

    .line 9
    invoke-static {v0, v2, p1, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public Lcom/intsig/office/fc/dom4j/xpath/XPathPattern;
.super Ljava/lang/Object;
.source "XPathPattern.java"

# interfaces
.implements Lcom/intsig/office/fc/dom4j/rule/Pattern;


# instance fields
.field private pattern:Lcom/intsig/office/fc/dom4j/rule/Pattern;

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/dom4j/rule/Pattern;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMatchType()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/xpath/XPathPattern;->pattern:Lcom/intsig/office/fc/dom4j/rule/Pattern;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/rule/Pattern;->getMatchType()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMatchesNodeName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/xpath/XPathPattern;->pattern:Lcom/intsig/office/fc/dom4j/rule/Pattern;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/rule/Pattern;->getMatchesNodeName()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPriority()D
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/xpath/XPathPattern;->pattern:Lcom/intsig/office/fc/dom4j/rule/Pattern;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/rule/Pattern;->getPriority()D

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    return-wide v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/xpath/XPathPattern;->text:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getUnionPatterns()[Lcom/intsig/office/fc/dom4j/rule/Pattern;
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/xpath/XPathPattern;->pattern:Lcom/intsig/office/fc/dom4j/rule/Pattern;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/rule/Pattern;->getUnionPatterns()[Lcom/intsig/office/fc/dom4j/rule/Pattern;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    array-length v1, v0

    .line 10
    new-array v2, v1, [Lcom/intsig/office/fc/dom4j/xpath/XPathPattern;

    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    :goto_0
    if-ge v3, v1, :cond_0

    .line 14
    .line 15
    new-instance v4, Lcom/intsig/office/fc/dom4j/xpath/XPathPattern;

    .line 16
    .line 17
    aget-object v5, v0, v3

    .line 18
    .line 19
    invoke-direct {v4, v5}, Lcom/intsig/office/fc/dom4j/xpath/XPathPattern;-><init>(Lcom/intsig/office/fc/dom4j/rule/Pattern;)V

    .line 20
    .line 21
    .line 22
    aput-object v4, v2, v3

    .line 23
    .line 24
    add-int/lit8 v3, v3, 0x1

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    return-object v2

    .line 28
    :cond_1
    const/4 v0, 0x0

    .line 29
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public matches(Lcom/intsig/office/fc/dom4j/Node;)Z
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    return p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

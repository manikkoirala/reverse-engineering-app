.class public Lcom/intsig/office/fc/dom4j/io/XMLResult;
.super Ljavax/xml/transform/sax/SAXResult;
.source "XMLResult.java"


# instance fields
.field private xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>()V

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/dom4j/io/XMLResult;-><init>(Lcom/intsig/office/fc/dom4j/io/XMLWriter;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/io/XMLWriter;)V
    .locals 0

    .line 6
    invoke-direct {p0, p1}, Ljavax/xml/transform/sax/SAXResult;-><init>(Lorg/xml/sax/ContentHandler;)V

    .line 7
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLResult;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 8
    invoke-virtual {p0, p1}, Ljavax/xml/transform/sax/SAXResult;->setLexicalHandler(Lorg/xml/sax/ext/LexicalHandler;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 4
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/dom4j/io/XMLResult;-><init>(Lcom/intsig/office/fc/dom4j/io/XMLWriter;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>(Ljava/io/OutputStream;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/dom4j/io/XMLResult;-><init>(Lcom/intsig/office/fc/dom4j/io/XMLWriter;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;)V
    .locals 1

    .line 2
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>(Ljava/io/Writer;)V

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/dom4j/io/XMLResult;-><init>(Lcom/intsig/office/fc/dom4j/io/XMLWriter;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V
    .locals 1

    .line 3
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/io/XMLWriter;-><init>(Ljava/io/Writer;Lcom/intsig/office/fc/dom4j/io/OutputFormat;)V

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/dom4j/io/XMLResult;-><init>(Lcom/intsig/office/fc/dom4j/io/XMLWriter;)V

    return-void
.end method


# virtual methods
.method public getHandler()Lorg/xml/sax/ContentHandler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLResult;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLexicalHandler()Lorg/xml/sax/ext/LexicalHandler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLResult;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getXMLWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/XMLResult;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setXMLWriter(Lcom/intsig/office/fc/dom4j/io/XMLWriter;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLResult;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Ljavax/xml/transform/sax/SAXResult;->setHandler(Lorg/xml/sax/ContentHandler;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/io/XMLResult;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 7
    .line 8
    invoke-virtual {p0, p1}, Ljavax/xml/transform/sax/SAXResult;->setLexicalHandler(Lorg/xml/sax/ext/LexicalHandler;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

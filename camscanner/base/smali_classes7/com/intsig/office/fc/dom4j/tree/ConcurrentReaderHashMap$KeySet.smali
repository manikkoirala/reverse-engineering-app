.class Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$KeySet;
.super Ljava/util/AbstractSet;
.source "ConcurrentReaderHashMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "KeySet"
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;


# direct methods
.method private constructor <init>(Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$KeySet;->o0:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;Lcom/intsig/office/fc/dom4j/tree/〇o00〇〇Oo;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$KeySet;-><init>(Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;)V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$KeySet;->o0:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->clear()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$KeySet;->o0:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->containsKey(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$KeyIterator;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$KeySet;->o0:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$KeyIterator;-><init>(Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$KeySet;->o0:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 p1, 0x0

    .line 12
    :goto_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public size()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap$KeySet;->o0:Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/tree/ConcurrentReaderHashMap;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

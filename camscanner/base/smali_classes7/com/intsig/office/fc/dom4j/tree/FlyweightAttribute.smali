.class public Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;
.super Lcom/intsig/office/fc/dom4j/tree/AbstractAttribute;
.source "FlyweightAttribute.java"


# instance fields
.field private qname:Lcom/intsig/office/fc/dom4j/QName;

.field protected value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/dom4j/QName;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractAttribute;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;->qname:Lcom/intsig/office/fc/dom4j/QName;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)V
    .locals 0

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractAttribute;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;->qname:Lcom/intsig/office/fc/dom4j/QName;

    .line 5
    iput-object p2, p0, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;->value:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractAttribute;-><init>()V

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractNode;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;->qname:Lcom/intsig/office/fc/dom4j/QName;

    .line 8
    iput-object p2, p0, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;->value:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V
    .locals 1

    .line 9
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractAttribute;-><init>()V

    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractNode;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p3}, Lcom/intsig/office/fc/dom4j/DocumentFactory;->createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;->qname:Lcom/intsig/office/fc/dom4j/QName;

    .line 11
    iput-object p2, p0, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;->value:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getQName()Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;->qname:Lcom/intsig/office/fc/dom4j/QName;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/FlyweightAttribute;->value:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public Lcom/intsig/office/fc/dom4j/util/NodeComparator;
.super Ljava/lang/Object;
.source "NodeComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public compare(Lcom/intsig/office/fc/dom4j/Attribute;Lcom/intsig/office/fc/dom4j/Attribute;)I
    .locals 2

    .line 33
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Attribute;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v0

    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Attribute;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/QName;Lcom/intsig/office/fc/dom4j/QName;)I

    move-result v0

    if-nez v0, :cond_0

    .line 34
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    :cond_0
    return v0
.end method

.method public compare(Lcom/intsig/office/fc/dom4j/CharacterData;Lcom/intsig/office/fc/dom4j/CharacterData;)I
    .locals 0

    .line 39
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public compare(Lcom/intsig/office/fc/dom4j/Document;Lcom/intsig/office/fc/dom4j/Document;)I
    .locals 2

    .line 24
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getDocType()Lcom/intsig/office/fc/dom4j/DocumentType;

    move-result-object v0

    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Document;->getDocType()Lcom/intsig/office/fc/dom4j/DocumentType;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/DocumentType;Lcom/intsig/office/fc/dom4j/DocumentType;)I

    move-result v0

    if-nez v0, :cond_0

    .line 25
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compareContent(Lcom/intsig/office/fc/dom4j/Branch;Lcom/intsig/office/fc/dom4j/Branch;)I

    move-result v0

    :cond_0
    return v0
.end method

.method public compare(Lcom/intsig/office/fc/dom4j/DocumentType;Lcom/intsig/office/fc/dom4j/DocumentType;)I
    .locals 2

    if-ne p1, p2, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    if-nez p1, :cond_1

    const/4 p1, -0x1

    return p1

    :cond_1
    if-nez p2, :cond_2

    const/4 p1, 0x1

    return p1

    .line 40
    :cond_2
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/DocumentType;->getPublicID()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/DocumentType;->getPublicID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 41
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/DocumentType;->getSystemID()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/DocumentType;->getSystemID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 42
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    :cond_3
    return v0
.end method

.method public compare(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/Element;)I
    .locals 4

    .line 26
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v0

    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Element;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/QName;Lcom/intsig/office/fc/dom4j/QName;)I

    move-result v0

    if-nez v0, :cond_3

    .line 27
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->attributeCount()I

    move-result v0

    .line 28
    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Element;->attributeCount()I

    move-result v1

    sub-int v1, v0, v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 29
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attribute(I)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v2

    .line 30
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Attribute;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v3

    invoke-interface {p2, v3}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v3

    .line 31
    invoke-virtual {p0, v2, v3}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/Attribute;Lcom/intsig/office/fc/dom4j/Attribute;)I

    move-result v2

    if-eqz v2, :cond_0

    return v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 32
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compareContent(Lcom/intsig/office/fc/dom4j/Branch;Lcom/intsig/office/fc/dom4j/Branch;)I

    move-result v0

    goto :goto_1

    :cond_2
    move v0, v1

    :cond_3
    :goto_1
    return v0
.end method

.method public compare(Lcom/intsig/office/fc/dom4j/Entity;Lcom/intsig/office/fc/dom4j/Entity;)I
    .locals 2

    .line 43
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 44
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Node;->getText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    :cond_0
    return v0
.end method

.method public compare(Lcom/intsig/office/fc/dom4j/Namespace;Lcom/intsig/office/fc/dom4j/Namespace;)I
    .locals 2

    .line 37
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/Namespace;->getURI()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/intsig/office/fc/dom4j/Namespace;->getURI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 38
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/Namespace;->getPrefix()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/intsig/office/fc/dom4j/Namespace;->getPrefix()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    :cond_0
    return v0
.end method

.method public compare(Lcom/intsig/office/fc/dom4j/Node;Lcom/intsig/office/fc/dom4j/Node;)I
    .locals 3

    .line 11
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getNodeType()S

    move-result v0

    .line 12
    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Node;->getNodeType()S

    move-result v1

    sub-int v1, v0, v1

    if-eqz v1, :cond_0

    return v1

    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 13
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid node types. node1: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " and node2: "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 14
    :pswitch_1
    check-cast p1, Lcom/intsig/office/fc/dom4j/Namespace;

    check-cast p2, Lcom/intsig/office/fc/dom4j/Namespace;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/Namespace;Lcom/intsig/office/fc/dom4j/Namespace;)I

    move-result p1

    return p1

    .line 15
    :pswitch_2
    check-cast p1, Lcom/intsig/office/fc/dom4j/DocumentType;

    check-cast p2, Lcom/intsig/office/fc/dom4j/DocumentType;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/DocumentType;Lcom/intsig/office/fc/dom4j/DocumentType;)I

    move-result p1

    return p1

    .line 16
    :pswitch_3
    check-cast p1, Lcom/intsig/office/fc/dom4j/Document;

    check-cast p2, Lcom/intsig/office/fc/dom4j/Document;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/Document;Lcom/intsig/office/fc/dom4j/Document;)I

    move-result p1

    return p1

    .line 17
    :pswitch_4
    check-cast p1, Lcom/intsig/office/fc/dom4j/Comment;

    check-cast p2, Lcom/intsig/office/fc/dom4j/Comment;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/CharacterData;Lcom/intsig/office/fc/dom4j/CharacterData;)I

    move-result p1

    return p1

    .line 18
    :pswitch_5
    check-cast p1, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    check-cast p2, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/ProcessingInstruction;Lcom/intsig/office/fc/dom4j/ProcessingInstruction;)I

    move-result p1

    return p1

    .line 19
    :pswitch_6
    check-cast p1, Lcom/intsig/office/fc/dom4j/Entity;

    check-cast p2, Lcom/intsig/office/fc/dom4j/Entity;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/Entity;Lcom/intsig/office/fc/dom4j/Entity;)I

    move-result p1

    return p1

    .line 20
    :pswitch_7
    check-cast p1, Lcom/intsig/office/fc/dom4j/CDATA;

    check-cast p2, Lcom/intsig/office/fc/dom4j/CDATA;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/CharacterData;Lcom/intsig/office/fc/dom4j/CharacterData;)I

    move-result p1

    return p1

    .line 21
    :pswitch_8
    check-cast p1, Lcom/intsig/office/fc/dom4j/Text;

    check-cast p2, Lcom/intsig/office/fc/dom4j/Text;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/CharacterData;Lcom/intsig/office/fc/dom4j/CharacterData;)I

    move-result p1

    return p1

    .line 22
    :pswitch_9
    check-cast p1, Lcom/intsig/office/fc/dom4j/Attribute;

    check-cast p2, Lcom/intsig/office/fc/dom4j/Attribute;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/Attribute;Lcom/intsig/office/fc/dom4j/Attribute;)I

    move-result p1

    return p1

    .line 23
    :pswitch_a
    check-cast p1, Lcom/intsig/office/fc/dom4j/Element;

    check-cast p2, Lcom/intsig/office/fc/dom4j/Element;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/Element;)I

    move-result p1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public compare(Lcom/intsig/office/fc/dom4j/ProcessingInstruction;Lcom/intsig/office/fc/dom4j/ProcessingInstruction;)I
    .locals 2

    .line 45
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;->getTarget()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;->getTarget()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 46
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;->getText()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/ProcessingInstruction;->getText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    :cond_0
    return v0
.end method

.method public compare(Lcom/intsig/office/fc/dom4j/QName;Lcom/intsig/office/fc/dom4j/QName;)I
    .locals 2

    .line 35
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/QName;->getNamespaceURI()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/intsig/office/fc/dom4j/QName;->getNamespaceURI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 36
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/QName;->getQualifiedName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/intsig/office/fc/dom4j/QName;->getQualifiedName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    :cond_0
    return v0
.end method

.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    if-ne p1, p2, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 v0, -0x1

    if-nez p1, :cond_1

    return v0

    :cond_1
    const/4 v1, 0x1

    if-nez p2, :cond_2

    return v1

    .line 1
    :cond_2
    instance-of v2, p1, Lcom/intsig/office/fc/dom4j/Node;

    if-eqz v2, :cond_4

    .line 2
    instance-of v0, p2, Lcom/intsig/office/fc/dom4j/Node;

    if-eqz v0, :cond_3

    .line 3
    check-cast p1, Lcom/intsig/office/fc/dom4j/Node;

    check-cast p2, Lcom/intsig/office/fc/dom4j/Node;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/Node;Lcom/intsig/office/fc/dom4j/Node;)I

    move-result p1

    return p1

    :cond_3
    return v1

    .line 4
    :cond_4
    instance-of v1, p2, Lcom/intsig/office/fc/dom4j/Node;

    if-eqz v1, :cond_5

    return v0

    .line 5
    :cond_5
    instance-of v0, p1, Ljava/lang/Comparable;

    if-eqz v0, :cond_6

    .line 6
    check-cast p1, Ljava/lang/Comparable;

    .line 7
    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result p1

    return p1

    .line 8
    :cond_6
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    .line 9
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    .line 10
    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public compare(Ljava/lang/String;Ljava/lang/String;)I
    .locals 0

    if-ne p1, p2, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    if-nez p1, :cond_1

    const/4 p1, -0x1

    return p1

    :cond_1
    if-nez p2, :cond_2

    const/4 p1, 0x1

    return p1

    .line 47
    :cond_2
    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public compareContent(Lcom/intsig/office/fc/dom4j/Branch;Lcom/intsig/office/fc/dom4j/Branch;)I
    .locals 4

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Branch;->nodeCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Branch;->nodeCount()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    sub-int v1, v0, v1

    .line 10
    .line 11
    if-nez v1, :cond_1

    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    :goto_0
    if-ge v2, v0, :cond_1

    .line 15
    .line 16
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Branch;->node(I)Lcom/intsig/office/fc/dom4j/Node;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-interface {p2, v2}, Lcom/intsig/office/fc/dom4j/Branch;->node(I)Lcom/intsig/office/fc/dom4j/Node;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    invoke-virtual {p0, v1, v3}, Lcom/intsig/office/fc/dom4j/util/NodeComparator;->compare(Lcom/intsig/office/fc/dom4j/Node;Lcom/intsig/office/fc/dom4j/Node;)I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-eqz v1, :cond_0

    .line 29
    .line 30
    goto :goto_1

    .line 31
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_1
    :goto_1
    return v1
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

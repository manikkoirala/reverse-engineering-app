.class public Lcom/intsig/office/fc/dom4j/tree/QNameCache;
.super Ljava/lang/Object;
.source "QNameCache.java"


# instance fields
.field private documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

.field protected namespaceCache:Ljava/util/Map;

.field protected noNamespaceCache:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->noNamespaceCache:Ljava/util/Map;

    .line 3
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->namespaceCache:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V
    .locals 1

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->noNamespaceCache:Ljava/util/Map;

    .line 6
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->namespaceCache:Ljava/util/Map;

    .line 7
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    return-void
.end method


# virtual methods
.method protected createMap()Ljava/util/Map;
    .locals 1

    .line 1
    new-instance v0, Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected createQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method protected createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 2
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    return-object v0
.end method

.method protected createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 1

    .line 3
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    invoke-direct {v0, p1, p2, p3}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;Ljava/lang/String;)V

    return-object v0
.end method

.method public get(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 2

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->noNamespaceCache:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/dom4j/QName;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const-string p1, ""

    :goto_0
    if-nez v0, :cond_1

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->createQName(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/QName;->setDocumentFactory(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V

    .line 4
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->noNamespaceCache:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public get(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 2

    .line 5
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->getNamespaceCache(Lcom/intsig/office/fc/dom4j/Namespace;)Ljava/util/Map;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 6
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/fc/dom4j/QName;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const-string p1, ""

    :goto_0
    if-nez v1, :cond_1

    .line 7
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v1

    .line 8
    iget-object p2, p0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v1, p2}, Lcom/intsig/office/fc/dom4j/QName;->setDocumentFactory(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V

    .line 9
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v1
.end method

.method public get(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 2

    .line 10
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->getNamespaceCache(Lcom/intsig/office/fc/dom4j/Namespace;)Ljava/util/Map;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 11
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/fc/dom4j/QName;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const-string p1, ""

    :goto_0
    if-nez v1, :cond_1

    .line 12
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->createQName(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v1

    .line 13
    iget-object p2, p0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->documentFactory:Lcom/intsig/office/fc/dom4j/DocumentFactory;

    invoke-virtual {v1, p2}, Lcom/intsig/office/fc/dom4j/QName;->setDocumentFactory(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V

    .line 14
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v1
.end method

.method public get(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 3

    const/16 v0, 0x3a

    .line 15
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gez v0, :cond_0

    .line 16
    invoke-static {p2}, Lcom/intsig/office/fc/dom4j/Namespace;->get(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->get(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    return-object p1

    :cond_0
    add-int/lit8 v1, v0, 0x1

    .line 17
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 18
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 19
    invoke-static {p1, p2}, Lcom/intsig/office/fc/dom4j/Namespace;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Namespace;

    move-result-object p1

    invoke-virtual {p0, v1, p1}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->get(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)Lcom/intsig/office/fc/dom4j/QName;

    move-result-object p1

    return-object p1
.end method

.method protected getNamespaceCache(Lcom/intsig/office/fc/dom4j/Namespace;)Ljava/util/Map;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/dom4j/Namespace;->NO_NAMESPACE:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 2
    .line 3
    if-ne p1, v0, :cond_0

    .line 4
    .line 5
    iget-object p1, p0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->noNamespaceCache:Ljava/util/Map;

    .line 6
    .line 7
    return-object p1

    .line 8
    :cond_0
    if-eqz p1, :cond_1

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->namespaceCache:Ljava/util/Map;

    .line 11
    .line 12
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    check-cast v0, Ljava/util/Map;

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_1
    const/4 v0, 0x0

    .line 20
    :goto_0
    if-nez v0, :cond_2

    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->createMap()Ljava/util/Map;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->namespaceCache:Ljava/util/Map;

    .line 27
    .line 28
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    :cond_2
    return-object v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getQNames()Ljava/util/List;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->noNamespaceCache:Ljava/util/Map;

    .line 7
    .line 8
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 13
    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->namespaceCache:Ljava/util/Map;

    .line 16
    .line 17
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    if-eqz v2, :cond_0

    .line 30
    .line 31
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    check-cast v2, Ljava/util/Map;

    .line 36
    .line 37
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_0
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public intern(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/QName;
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/QName;->getName()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/QName;->getNamespace()Lcom/intsig/office/fc/dom4j/Namespace;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/QName;->getQualifiedName()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    invoke-virtual {p0, v0, v1, p1}, Lcom/intsig/office/fc/dom4j/tree/QNameCache;->get(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/QName;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    return-object p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

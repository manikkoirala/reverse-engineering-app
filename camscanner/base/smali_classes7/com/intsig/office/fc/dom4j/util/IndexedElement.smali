.class public Lcom/intsig/office/fc/dom4j/util/IndexedElement;
.super Lcom/intsig/office/fc/dom4j/tree/DefaultElement;
.source "IndexedElement.java"


# instance fields
.field private attributeIndex:Ljava/util/Map;

.field private elementIndex:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/dom4j/QName;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/DefaultElement;-><init>(Lcom/intsig/office/fc/dom4j/QName;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/dom4j/QName;I)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/tree/DefaultElement;-><init>(Lcom/intsig/office/fc/dom4j/QName;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/DefaultElement;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected addNode(Lcom/intsig/office/fc/dom4j/Node;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->addNode(Lcom/intsig/office/fc/dom4j/Node;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->elementIndex:Ljava/util/Map;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/Element;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    check-cast p1, Lcom/intsig/office/fc/dom4j/Element;

    .line 13
    .line 14
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->addToElementIndex(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->attributeIndex:Ljava/util/Map;

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/Attribute;

    .line 23
    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    check-cast p1, Lcom/intsig/office/fc/dom4j/Attribute;

    .line 27
    .line 28
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->addToAttributeIndex(Lcom/intsig/office/fc/dom4j/Attribute;)V

    .line 29
    .line 30
    .line 31
    :cond_1
    :goto_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected addToAttributeIndex(Lcom/intsig/office/fc/dom4j/Attribute;)V
    .locals 2

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Attribute;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/QName;->getName()Ljava/lang/String;

    move-result-object v1

    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->addToAttributeIndex(Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/Attribute;)V

    .line 4
    invoke-virtual {p0, v1, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->addToAttributeIndex(Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/Attribute;)V

    return-void
.end method

.method protected addToAttributeIndex(Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/Attribute;)V
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->attributeIndex:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->attributeIndex:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method protected addToElementIndex(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 2

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/QName;->getName()Ljava/lang/String;

    move-result-object v1

    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->addToElementIndex(Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 4
    invoke-virtual {p0, v1, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->addToElementIndex(Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/Element;)V

    return-void
.end method

.method protected addToElementIndex(Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 2

    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->elementIndex:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->elementIndex:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 7
    :cond_0
    instance-of v1, v0, Ljava/util/List;

    if-eqz v1, :cond_1

    .line 8
    check-cast v0, Ljava/util/List;

    .line 9
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 10
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->createList()Ljava/util/List;

    move-result-object v1

    .line 11
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 12
    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 13
    iget-object p2, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->elementIndex:Ljava/util/Map;

    invoke-interface {p2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method protected asElement(Ljava/lang/Object;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/fc/dom4j/Element;

    .line 6
    .line 7
    return-object p1

    .line 8
    :cond_0
    if-eqz p1, :cond_1

    .line 9
    .line 10
    check-cast p1, Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/4 v1, 0x1

    .line 17
    if-lt v0, v1, :cond_1

    .line 18
    .line 19
    const/4 v0, 0x0

    .line 20
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    check-cast p1, Lcom/intsig/office/fc/dom4j/Element;

    .line 25
    .line 26
    return-object p1

    .line 27
    :cond_1
    const/4 p1, 0x0

    .line 28
    return-object p1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected asElementIterator(Ljava/lang/Object;)Ljava/util/Iterator;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->asElementList(Ljava/lang/Object;)Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected asElementList(Ljava/lang/Object;)Ljava/util/List;
    .locals 4

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createSingleResultList(Ljava/lang/Object;)Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1

    .line 10
    :cond_0
    if-eqz p1, :cond_2

    .line 11
    .line 12
    check-cast p1, Ljava/util/List;

    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createResultList()Lcom/intsig/office/fc/dom4j/tree/BackedList;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    const/4 v2, 0x0

    .line 23
    :goto_0
    if-ge v2, v1, :cond_1

    .line 24
    .line 25
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/dom4j/tree/BackedList;->addLocal(Ljava/lang/Object;)V

    .line 30
    .line 31
    .line 32
    add-int/lit8 v2, v2, 0x1

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    return-object v0

    .line 36
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractBranch;->createEmptyList()Ljava/util/List;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    return-object p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public attribute(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Attribute;
    .locals 1

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->attributeIndex()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/dom4j/Attribute;

    return-object p1
.end method

.method public attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->attributeIndex()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/dom4j/Attribute;

    return-object p1
.end method

.method protected attributeIndex()Ljava/util/Map;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->attributeIndex:Ljava/util/Map;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->createAttributeIndex()Ljava/util/Map;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->attributeIndex:Ljava/util/Map;

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/DefaultElement;->attributeIterator()Ljava/util/Iterator;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    check-cast v1, Lcom/intsig/office/fc/dom4j/Attribute;

    .line 26
    .line 27
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->addToAttributeIndex(Lcom/intsig/office/fc/dom4j/Attribute;)V

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->attributeIndex:Ljava/util/Map;

    .line 32
    .line 33
    return-object v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected createAttributeIndex()Ljava/util/Map;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->createIndex()Ljava/util/Map;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected createElementIndex()Ljava/util/Map;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->createIndex()Ljava/util/Map;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected createIndex()Ljava/util/Map;
    .locals 1

    .line 1
    new-instance v0, Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected createList()Ljava/util/List;
    .locals 1

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->elementIndex()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->asElement(Ljava/lang/Object;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    return-object p1
.end method

.method public element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->elementIndex()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->asElement(Ljava/lang/Object;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object p1

    return-object p1
.end method

.method protected elementIndex()Ljava/util/Map;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->elementIndex:Ljava/util/Map;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->createElementIndex()Ljava/util/Map;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->elementIndex:Ljava/util/Map;

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/tree/AbstractElement;->elementIterator()Ljava/util/Iterator;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 26
    .line 27
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->addToElementIndex(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->elementIndex:Ljava/util/Map;

    .line 32
    .line 33
    return-object v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public elements(Lcom/intsig/office/fc/dom4j/QName;)Ljava/util/List;
    .locals 1

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->elementIndex()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->asElementList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public elements(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->elementIndex()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->asElementList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method protected removeFromAttributeIndex(Lcom/intsig/office/fc/dom4j/Attribute;)V
    .locals 2

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Attribute;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/QName;->getName()Ljava/lang/String;

    move-result-object v1

    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->removeFromAttributeIndex(Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/Attribute;)V

    .line 4
    invoke-virtual {p0, v1, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->removeFromAttributeIndex(Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/Attribute;)V

    return-void
.end method

.method protected removeFromAttributeIndex(Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/Attribute;)V
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->attributeIndex:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6
    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 7
    iget-object p2, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->attributeIndex:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method protected removeFromElementIndex(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 2

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getQName()Lcom/intsig/office/fc/dom4j/QName;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/QName;->getName()Ljava/lang/String;

    move-result-object v1

    .line 3
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->removeFromElementIndex(Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 4
    invoke-virtual {p0, v1, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->removeFromElementIndex(Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/Element;)V

    return-void
.end method

.method protected removeFromElementIndex(Ljava/lang/Object;Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 2

    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->elementIndex:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 6
    instance-of v1, v0, Ljava/util/List;

    if-eqz v1, :cond_0

    .line 7
    check-cast v0, Ljava/util/List;

    .line 8
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 9
    :cond_0
    iget-object p2, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->elementIndex:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method protected removeNode(Lcom/intsig/office/fc/dom4j/Node;)Z
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/dom4j/tree/DefaultElement;->removeNode(Lcom/intsig/office/fc/dom4j/Node;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->elementIndex:Ljava/util/Map;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/Element;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    check-cast p1, Lcom/intsig/office/fc/dom4j/Element;

    .line 16
    .line 17
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->removeFromElementIndex(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->attributeIndex:Ljava/util/Map;

    .line 22
    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    instance-of v0, p1, Lcom/intsig/office/fc/dom4j/Attribute;

    .line 26
    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    check-cast p1, Lcom/intsig/office/fc/dom4j/Attribute;

    .line 30
    .line 31
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/dom4j/util/IndexedElement;->removeFromAttributeIndex(Lcom/intsig/office/fc/dom4j/Attribute;)V

    .line 32
    .line 33
    .line 34
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 35
    return p1

    .line 36
    :cond_2
    const/4 p1, 0x0

    .line 37
    return p1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

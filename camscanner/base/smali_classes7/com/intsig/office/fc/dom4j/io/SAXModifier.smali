.class public Lcom/intsig/office/fc/dom4j/io/SAXModifier;
.super Ljava/lang/Object;
.source "SAXModifier.java"


# instance fields
.field private modifiers:Ljava/util/HashMap;

.field private modifyReader:Lcom/intsig/office/fc/dom4j/io/SAXModifyReader;

.field private pruneElements:Z

.field private xmlReader:Lorg/xml/sax/XMLReader;

.field private xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modifiers:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Lorg/xml/sax/XMLReader;)V
    .locals 1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modifiers:Ljava/util/HashMap;

    .line 8
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->xmlReader:Lorg/xml/sax/XMLReader;

    return-void
.end method

.method public constructor <init>(Lorg/xml/sax/XMLReader;Z)V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    iput-object p2, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modifiers:Ljava/util/HashMap;

    .line 11
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->xmlReader:Lorg/xml/sax/XMLReader;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modifiers:Ljava/util/HashMap;

    .line 5
    iput-boolean p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->pruneElements:Z

    return-void
.end method

.method private getSAXModifyReader()Lcom/intsig/office/fc/dom4j/io/SAXModifyReader;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modifyReader:Lcom/intsig/office/fc/dom4j/io/SAXModifyReader;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/SAXModifyReader;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/SAXModifyReader;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modifyReader:Lcom/intsig/office/fc/dom4j/io/SAXModifyReader;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modifyReader:Lcom/intsig/office/fc/dom4j/io/SAXModifyReader;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private getXMLReader()Lorg/xml/sax/XMLReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    invoke-static {v0}, Lcom/intsig/office/fc/dom4j/io/SAXHelper;->〇080(Z)Lorg/xml/sax/XMLReader;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->xmlReader:Lorg/xml/sax/XMLReader;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private installModifyReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 1
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->getSAXModifyReader()Lcom/intsig/office/fc/dom4j/io/SAXModifyReader;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->isPruneElements()Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modifyReader:Lcom/intsig/office/fc/dom4j/io/SAXModifyReader;

    .line 12
    .line 13
    new-instance v2, Lcom/intsig/office/fc/dom4j/io/PruningDispatchHandler;

    .line 14
    .line 15
    invoke-direct {v2}, Lcom/intsig/office/fc/dom4j/io/PruningDispatchHandler;-><init>()V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->setDispatchHandler(Lcom/intsig/office/fc/dom4j/io/DispatchHandler;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->resetHandlers()V

    .line 22
    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modifiers:Ljava/util/HashMap;

    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    if-eqz v2, :cond_1

    .line 39
    .line 40
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    move-result-object v2

    .line 44
    check-cast v2, Ljava/util/Map$Entry;

    .line 45
    .line 46
    new-instance v3, Lcom/intsig/office/fc/dom4j/io/SAXModifyElementHandler;

    .line 47
    .line 48
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    move-result-object v4

    .line 52
    check-cast v4, Lcom/intsig/office/fc/dom4j/io/ElementModifier;

    .line 53
    .line 54
    invoke-direct {v3, v4}, Lcom/intsig/office/fc/dom4j/io/SAXModifyElementHandler;-><init>(Lcom/intsig/office/fc/dom4j/io/ElementModifier;)V

    .line 55
    .line 56
    .line 57
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    check-cast v2, Ljava/lang/String;

    .line 62
    .line 63
    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->addHandler(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->getXMLWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/io/SAXModifyReader;->〇080(Lcom/intsig/office/fc/dom4j/io/XMLWriter;)V

    .line 72
    .line 73
    .line 74
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->getXMLReader()Lorg/xml/sax/XMLReader;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->setXMLReader(Lorg/xml/sax/XMLReader;)V
    :try_end_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    .line 80
    .line 81
    return-object v0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    new-instance v1, Lcom/intsig/office/fc/dom4j/DocumentException;

    .line 84
    .line 85
    invoke-virtual {v0}, Lorg/xml/sax/SAXException;->getMessage()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v2

    .line 89
    invoke-direct {v1, v2, v0}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 90
    .line 91
    .line 92
    throw v1
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method


# virtual methods
.method public addModifier(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/io/ElementModifier;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modifiers:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->getSAXModifyReader()Lcom/intsig/office/fc/dom4j/io/SAXModifyReader;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->getDocumentFactory()Lcom/intsig/office/fc/dom4j/DocumentFactory;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getXMLWriter()Lcom/intsig/office/fc/dom4j/io/XMLWriter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isPruneElements()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->pruneElements:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public modify(Ljava/io/File;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 1
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->installModifyReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/File;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1
    :try_end_0
    .catch Lcom/intsig/office/fc/dom4j/io/SAXModifyException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 2
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    .line 3
    new-instance v0, Lcom/intsig/office/fc/dom4j/DocumentException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public modify(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 7
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->installModifyReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1
    :try_end_0
    .catch Lcom/intsig/office/fc/dom4j/io/SAXModifyException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 8
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    .line 9
    new-instance v0, Lcom/intsig/office/fc/dom4j/DocumentException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public modify(Ljava/io/InputStream;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 10
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->installModifyReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1
    :try_end_0
    .catch Lcom/intsig/office/fc/dom4j/io/SAXModifyException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 11
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    .line 12
    new-instance p2, Lcom/intsig/office/fc/dom4j/DocumentException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public modify(Ljava/io/Reader;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 13
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->installModifyReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/Reader;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1
    :try_end_0
    .catch Lcom/intsig/office/fc/dom4j/io/SAXModifyException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 14
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    .line 15
    new-instance v0, Lcom/intsig/office/fc/dom4j/DocumentException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public modify(Ljava/io/Reader;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 16
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->installModifyReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/Reader;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1
    :try_end_0
    .catch Lcom/intsig/office/fc/dom4j/io/SAXModifyException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 17
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    .line 18
    new-instance p2, Lcom/intsig/office/fc/dom4j/DocumentException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public modify(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 22
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->installModifyReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1
    :try_end_0
    .catch Lcom/intsig/office/fc/dom4j/io/SAXModifyException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 23
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    .line 24
    new-instance v0, Lcom/intsig/office/fc/dom4j/DocumentException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public modify(Ljava/net/URL;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 19
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->installModifyReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/net/URL;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1
    :try_end_0
    .catch Lcom/intsig/office/fc/dom4j/io/SAXModifyException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 20
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    .line 21
    new-instance v0, Lcom/intsig/office/fc/dom4j/DocumentException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public modify(Lorg/xml/sax/InputSource;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 4
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->installModifyReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Lorg/xml/sax/InputSource;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1
    :try_end_0
    .catch Lcom/intsig/office/fc/dom4j/io/SAXModifyException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 5
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    .line 6
    new-instance v0, Lcom/intsig/office/fc/dom4j/DocumentException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public removeModifier(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modifiers:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->getSAXModifyReader()Lcom/intsig/office/fc/dom4j/io/SAXModifyReader;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->removeHandler(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public resetModifiers()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->modifiers:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->getSAXModifyReader()Lcom/intsig/office/fc/dom4j/io/SAXModifyReader;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->resetHandlers()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setDocumentFactory(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->getSAXModifyReader()Lcom/intsig/office/fc/dom4j/io/SAXModifyReader;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->setDocumentFactory(Lcom/intsig/office/fc/dom4j/DocumentFactory;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setXMLWriter(Lcom/intsig/office/fc/dom4j/io/XMLWriter;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/dom4j/io/SAXModifier;->xmlWriter:Lcom/intsig/office/fc/dom4j/io/XMLWriter;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

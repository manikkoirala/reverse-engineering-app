.class public Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;
.super Lcom/intsig/office/fc/dom4j/jaxb/JAXBSupport;
.source "JAXBReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader$PruningElementHandler;,
        Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader$UnmarshalElementHandler;
    }
.end annotation


# instance fields
.field private pruneElements:Z

.field private reader:Lcom/intsig/office/fc/dom4j/io/SAXReader;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBSupport;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBSupport;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    return-void
.end method

.method private getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->reader:Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->reader:Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->reader:Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public addHandler(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->addHandler(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public addObjectHandler(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/jaxb/JAXBObjectHandler;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader$UnmarshalElementHandler;

    .line 2
    .line 3
    invoke-direct {v0, p0, p0, p2}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader$UnmarshalElementHandler;-><init>(Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;Lcom/intsig/office/fc/dom4j/jaxb/JAXBObjectHandler;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 7
    .line 8
    .line 9
    move-result-object p2

    .line 10
    invoke-virtual {p2, p1, v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->addHandler(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public isPruneElements()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->pruneElements:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public read(Ljava/io/File;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/File;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public read(Ljava/io/File;Ljava/nio/charset/Charset;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 2
    :try_start_0
    new-instance v0, Ljava/io/InputStreamReader;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/Reader;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 4
    new-instance p2, Lcom/intsig/office/fc/dom4j/DocumentException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lcom/intsig/office/fc/dom4j/DocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public read(Ljava/io/InputStream;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 7
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public read(Ljava/io/Reader;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 8
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/Reader;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public read(Ljava/io/Reader;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 9
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/Reader;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public read(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 10
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public read(Ljava/net/URL;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 11
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/net/URL;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public read(Lorg/xml/sax/InputSource;)Lcom/intsig/office/fc/dom4j/Document;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/dom4j/DocumentException;
        }
    .end annotation

    .line 5
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Lorg/xml/sax/InputSource;)Lcom/intsig/office/fc/dom4j/Document;

    move-result-object p1

    return-object p1
.end method

.method public removeHandler(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->removeHandler(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public removeObjectHandler(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->removeHandler(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public resetHandlers()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->resetHandlers()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setPruneElements(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->pruneElements:Z

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;->getReader()Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    new-instance v0, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader$PruningElementHandler;

    .line 10
    .line 11
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader$PruningElementHandler;-><init>(Lcom/intsig/office/fc/dom4j/jaxb/JAXBReader;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->setDefaultHandler(Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

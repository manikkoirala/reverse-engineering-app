.class public abstract Lcom/intsig/office/fc/POIDocument;
.super Ljava/lang/Object;
.source "POIDocument.java"


# static fields
.field private static final logger:Lcom/intsig/office/fc/util/POILogger;


# instance fields
.field protected directory:Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

.field private dsInf:Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;

.field private initialized:Z

.field private sInf:Lcom/intsig/office/fc/hpsf/SummaryInformation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/POIDocument;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lcom/intsig/office/fc/util/POILogger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/POIDocument;->logger:Lcom/intsig/office/fc/util/POILogger;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/fc/POIDocument;->initialized:Z

    .line 3
    iput-object p1, p0, Lcom/intsig/office/fc/POIDocument;->directory:Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p2, 0x0

    .line 5
    iput-boolean p2, p0, Lcom/intsig/office/fc/POIDocument;->initialized:Z

    .line 6
    iput-object p1, p0, Lcom/intsig/office/fc/POIDocument;->directory:Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;)V
    .locals 0

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/intsig/office/fc/POIDocument;-><init>(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;)V
    .locals 0

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/intsig/office/fc/POIDocument;-><init>(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V

    return-void
.end method


# virtual methods
.method protected copyNodeRecursively(Lcom/intsig/office/fc/poifs/filesystem/Entry;Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/POIUtils;->copyNodeRecursively(Lcom/intsig/office/fc/poifs/filesystem/Entry;Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected copyNodes(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;",
            "Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 2
    invoke-static {p1, p2, p3}, Lcom/intsig/office/fc/util/POIUtils;->copyNodes(Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;Ljava/util/List;)V

    return-void
.end method

.method protected copyNodes(Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;",
            "Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-static {p1, p2, p3}, Lcom/intsig/office/fc/util/POIUtils;->copyNodes(Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Ljava/util/List;)V

    return-void
.end method

.method public createInformationProperties()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/POIDocument;->initialized:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/POIDocument;->readProperties()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/POIDocument;->sInf:Lcom/intsig/office/fc/hpsf/SummaryInformation;

    .line 9
    .line 10
    if-nez v0, :cond_1

    .line 11
    .line 12
    invoke-static {}, Lcom/intsig/office/fc/hpsf/PropertySetFactory;->newSummaryInformation()Lcom/intsig/office/fc/hpsf/SummaryInformation;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iput-object v0, p0, Lcom/intsig/office/fc/POIDocument;->sInf:Lcom/intsig/office/fc/hpsf/SummaryInformation;

    .line 17
    .line 18
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/POIDocument;->dsInf:Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;

    .line 19
    .line 20
    if-nez v0, :cond_2

    .line 21
    .line 22
    invoke-static {}, Lcom/intsig/office/fc/hpsf/PropertySetFactory;->newDocumentSummaryInformation()Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    iput-object v0, p0, Lcom/intsig/office/fc/POIDocument;->dsInf:Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;

    .line 27
    .line 28
    :cond_2
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getDocumentSummaryInformation()Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/POIDocument;->initialized:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/POIDocument;->readProperties()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/POIDocument;->dsInf:Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;

    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getPropertySet(Ljava/lang/String;)Lcom/intsig/office/fc/hpsf/PropertySet;
    .locals 7

    .line 1
    const-string v0, "Error creating property set with name "

    .line 2
    .line 3
    const-string v1, "\n"

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/office/fc/POIDocument;->directory:Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    if-nez v2, :cond_0

    .line 9
    .line 10
    return-object v3

    .line 11
    :cond_0
    :try_start_0
    invoke-virtual {v2, p1}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/Entry;

    .line 12
    .line 13
    .line 14
    move-result-object v4

    .line 15
    invoke-virtual {v2, v4}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Lcom/intsig/office/fc/poifs/filesystem/Entry;)Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 16
    .line 17
    .line 18
    move-result-object v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 19
    :try_start_1
    invoke-static {v2}, Lcom/intsig/office/fc/hpsf/PropertySetFactory;->create(Ljava/io/InputStream;)Lcom/intsig/office/fc/hpsf/PropertySet;

    .line 20
    .line 21
    .line 22
    move-result-object p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/intsig/office/fc/hpsf/HPSFException; {:try_start_1 .. :try_end_1} :catch_0

    .line 23
    return-object p1

    .line 24
    :catch_0
    move-exception v2

    .line 25
    sget-object v4, Lcom/intsig/office/fc/POIDocument;->logger:Lcom/intsig/office/fc/util/POILogger;

    .line 26
    .line 27
    sget v5, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    .line 28
    .line 29
    new-instance v6, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-virtual {v4, v5, p1}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :catch_1
    move-exception v2

    .line 55
    sget-object v4, Lcom/intsig/office/fc/POIDocument;->logger:Lcom/intsig/office/fc/util/POILogger;

    .line 56
    .line 57
    sget v5, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    .line 58
    .line 59
    new-instance v6, Ljava/lang/StringBuilder;

    .line 60
    .line 61
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    invoke-virtual {v4, v5, p1}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 81
    .line 82
    .line 83
    :goto_0
    return-object v3

    .line 84
    :catch_2
    move-exception v0

    .line 85
    sget-object v2, Lcom/intsig/office/fc/POIDocument;->logger:Lcom/intsig/office/fc/util/POILogger;

    .line 86
    .line 87
    sget v4, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    .line 88
    .line 89
    new-instance v5, Ljava/lang/StringBuilder;

    .line 90
    .line 91
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    .line 93
    .line 94
    const-string v6, "Error getting property set with name "

    .line 95
    .line 96
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object p1

    .line 112
    invoke-virtual {v2, v4, p1}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 113
    .line 114
    .line 115
    return-object v3
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public getSummaryInformation()Lcom/intsig/office/fc/hpsf/SummaryInformation;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/POIDocument;->initialized:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/POIDocument;->readProperties()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/POIDocument;->sInf:Lcom/intsig/office/fc/hpsf/SummaryInformation;

    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected readProperties()V
    .locals 4

    .line 1
    const-string v0, "\u0005DocumentSummaryInformation"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/POIDocument;->getPropertySet(Ljava/lang/String;)Lcom/intsig/office/fc/hpsf/PropertySet;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    instance-of v1, v0, Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;

    .line 10
    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    check-cast v0, Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;

    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/fc/POIDocument;->dsInf:Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    if-eqz v0, :cond_1

    .line 19
    .line 20
    sget-object v1, Lcom/intsig/office/fc/POIDocument;->logger:Lcom/intsig/office/fc/util/POILogger;

    .line 21
    .line 22
    sget v2, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const-string v3, "DocumentSummaryInformation property set came back with wrong class - "

    .line 29
    .line 30
    invoke-virtual {v1, v2, v3, v0}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    :goto_0
    const-string v0, "\u0005SummaryInformation"

    .line 34
    .line 35
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/POIDocument;->getPropertySet(Ljava/lang/String;)Lcom/intsig/office/fc/hpsf/PropertySet;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    instance-of v1, v0, Lcom/intsig/office/fc/hpsf/SummaryInformation;

    .line 40
    .line 41
    if-eqz v1, :cond_2

    .line 42
    .line 43
    check-cast v0, Lcom/intsig/office/fc/hpsf/SummaryInformation;

    .line 44
    .line 45
    iput-object v0, p0, Lcom/intsig/office/fc/POIDocument;->sInf:Lcom/intsig/office/fc/hpsf/SummaryInformation;

    .line 46
    .line 47
    goto :goto_1

    .line 48
    :cond_2
    if-eqz v0, :cond_3

    .line 49
    .line 50
    sget-object v1, Lcom/intsig/office/fc/POIDocument;->logger:Lcom/intsig/office/fc/util/POILogger;

    .line 51
    .line 52
    sget v2, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    .line 53
    .line 54
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    const-string v3, "SummaryInformation property set came back with wrong class - "

    .line 59
    .line 60
    invoke-virtual {v1, v2, v3, v0}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 61
    .line 62
    .line 63
    :cond_3
    :goto_1
    const/4 v0, 0x1

    .line 64
    iput-boolean v0, p0, Lcom/intsig/office/fc/POIDocument;->initialized:Z

    .line 65
    .line 66
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public abstract write(Ljava/io/OutputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected writeProperties(Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/POIDocument;->writeProperties(Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Ljava/util/List;)V

    return-void
.end method

.method protected writeProperties(Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/POIDocument;->getSummaryInformation()Lcom/intsig/office/fc/hpsf/SummaryInformation;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "\u0005SummaryInformation"

    .line 3
    invoke-virtual {p0, v1, v0, p1}, Lcom/intsig/office/fc/POIDocument;->writePropertySet(Ljava/lang/String;Lcom/intsig/office/fc/hpsf/PropertySet;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;)V

    if-eqz p2, :cond_0

    .line 4
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/POIDocument;->getDocumentSummaryInformation()Lcom/intsig/office/fc/hpsf/DocumentSummaryInformation;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "\u0005DocumentSummaryInformation"

    .line 6
    invoke-virtual {p0, v1, v0, p1}, Lcom/intsig/office/fc/POIDocument;->writePropertySet(Ljava/lang/String;Lcom/intsig/office/fc/hpsf/PropertySet;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;)V

    if-eqz p2, :cond_1

    .line 7
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method protected writePropertySet(Ljava/lang/String;Lcom/intsig/office/fc/hpsf/PropertySet;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    :try_start_0
    new-instance v0, Lcom/intsig/office/fc/hpsf/MutablePropertySet;

    .line 2
    .line 3
    invoke-direct {v0, p2}, Lcom/intsig/office/fc/hpsf/MutablePropertySet;-><init>(Lcom/intsig/office/fc/hpsf/PropertySet;)V

    .line 4
    .line 5
    .line 6
    new-instance p2, Ljava/io/ByteArrayOutputStream;

    .line 7
    .line 8
    invoke-direct {p2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hpsf/MutablePropertySet;->write(Ljava/io/OutputStream;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 15
    .line 16
    .line 17
    move-result-object p2

    .line 18
    new-instance v0, Ljava/io/ByteArrayInputStream;

    .line 19
    .line 20
    invoke-direct {v0, p2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p3, v0, p1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->createDocument(Ljava/io/InputStream;Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;

    .line 24
    .line 25
    .line 26
    sget-object p3, Lcom/intsig/office/fc/POIDocument;->logger:Lcom/intsig/office/fc/util/POILogger;

    .line 27
    .line 28
    sget v0, Lcom/intsig/office/fc/util/POILogger;->INFO:I

    .line 29
    .line 30
    new-instance v1, Ljava/lang/StringBuilder;

    .line 31
    .line 32
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    const-string v2, "Wrote property set "

    .line 36
    .line 37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    const-string v2, " of size "

    .line 44
    .line 45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    array-length p2, p2

    .line 49
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p2

    .line 56
    invoke-virtual {p3, v0, p2}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V
    :try_end_0
    .catch Lcom/intsig/office/fc/hpsf/WritingNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :catch_0
    sget-object p2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    .line 61
    .line 62
    new-instance p3, Ljava/lang/StringBuilder;

    .line 63
    .line 64
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .line 66
    .line 67
    const-string v0, "Couldn\'t write property set with name "

    .line 68
    .line 69
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    const-string p1, " as not supported by HPSF yet"

    .line 76
    .line 77
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object p1

    .line 84
    invoke-virtual {p2, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    :goto_0
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

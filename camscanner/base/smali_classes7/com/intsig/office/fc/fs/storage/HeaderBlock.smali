.class public final Lcom/intsig/office/fc/fs/storage/HeaderBlock;
.super Ljava/lang/Object;
.source "HeaderBlock.java"


# static fields
.field public static final _bat_array_offset:I = 0x4c

.field public static final _bat_count_offset:I = 0x2c

.field public static final _max_bats_in_header:I = 0x6d

.field public static final _property_start_offset:I = 0x30

.field public static final _sbat_block_count_offset:I = 0x40

.field public static final _sbat_start_offset:I = 0x3c

.field public static final _signature:J = -0x1ee54e5e1fee3030L

.field public static final _signature_offset:I = 0x0

.field public static final _xbat_count_offset:I = 0x48

.field public static final _xbat_start_offset:I = 0x44


# instance fields
.field private _bat_count:I

.field private _data:[B

.field private _property_start:I

.field private _sbat_count:I

.field private _sbat_start:I

.field private _xbat_count:I

.field private _xbat_start:I

.field private bigBlockSize:Lcom/intsig/office/fc/fs/filesystem/BlockSize;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x200

    .line 5
    .line 6
    new-array v0, v0, [B

    .line 7
    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_data:[B

    .line 9
    .line 10
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_data:[B

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    invoke-static {p1, v0}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getLong([BI)J

    .line 17
    .line 18
    .line 19
    move-result-wide v0

    .line 20
    const-wide v2, -0x1ee54e5e1fee3030L    # -5.8639378995972355E159

    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    cmp-long p1, v0, v2

    .line 26
    .line 27
    if-nez p1, :cond_2

    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_data:[B

    .line 30
    .line 31
    const/16 v0, 0x1e

    .line 32
    .line 33
    aget-byte v1, p1, v0

    .line 34
    .line 35
    const/16 v2, 0xc

    .line 36
    .line 37
    if-ne v1, v2, :cond_0

    .line 38
    .line 39
    sget-object v0, Lcom/intsig/office/fc/fs/filesystem/CFBConstants;->LARGER_BIG_BLOCK_SIZE_DETAILS:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 40
    .line 41
    iput-object v0, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->bigBlockSize:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_0
    const/16 v2, 0x9

    .line 45
    .line 46
    if-ne v1, v2, :cond_1

    .line 47
    .line 48
    sget-object v0, Lcom/intsig/office/fc/fs/filesystem/CFBConstants;->SMALLER_BIG_BLOCK_SIZE_DETAILS:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 49
    .line 50
    iput-object v0, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->bigBlockSize:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 51
    .line 52
    :goto_0
    const/16 v0, 0x2c

    .line 53
    .line 54
    invoke-static {p1, v0}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getInt([BI)I

    .line 55
    .line 56
    .line 57
    move-result p1

    .line 58
    iput p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_bat_count:I

    .line 59
    .line 60
    iget-object p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_data:[B

    .line 61
    .line 62
    const/16 v0, 0x30

    .line 63
    .line 64
    invoke-static {p1, v0}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getInt([BI)I

    .line 65
    .line 66
    .line 67
    move-result p1

    .line 68
    iput p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_property_start:I

    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_data:[B

    .line 71
    .line 72
    const/16 v0, 0x3c

    .line 73
    .line 74
    invoke-static {p1, v0}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getInt([BI)I

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    iput p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_sbat_start:I

    .line 79
    .line 80
    iget-object p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_data:[B

    .line 81
    .line 82
    const/16 v0, 0x40

    .line 83
    .line 84
    invoke-static {p1, v0}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getInt([BI)I

    .line 85
    .line 86
    .line 87
    move-result p1

    .line 88
    iput p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_sbat_count:I

    .line 89
    .line 90
    iget-object p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_data:[B

    .line 91
    .line 92
    const/16 v0, 0x44

    .line 93
    .line 94
    invoke-static {p1, v0}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getInt([BI)I

    .line 95
    .line 96
    .line 97
    move-result p1

    .line 98
    iput p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_xbat_start:I

    .line 99
    .line 100
    iget-object p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_data:[B

    .line 101
    .line 102
    const/16 v0, 0x48

    .line 103
    .line 104
    invoke-static {p1, v0}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getInt([BI)I

    .line 105
    .line 106
    .line 107
    move-result p1

    .line 108
    iput p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_xbat_count:I

    .line 109
    .line 110
    return-void

    .line 111
    :cond_1
    new-instance p1, Ljava/io/IOException;

    .line 112
    .line 113
    new-instance v1, Ljava/lang/StringBuilder;

    .line 114
    .line 115
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    .line 117
    .line 118
    const-string v2, "Unsupported blocksize  (2^"

    .line 119
    .line 120
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    iget-object v2, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_data:[B

    .line 124
    .line 125
    aget-byte v0, v2, v0

    .line 126
    .line 127
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 128
    .line 129
    .line 130
    const-string v0, "). Expected 2^9 or 2^12."

    .line 131
    .line 132
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    .line 134
    .line 135
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v0

    .line 139
    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 140
    .line 141
    .line 142
    throw p1

    .line 143
    :cond_2
    new-instance p1, Ljava/io/IOException;

    .line 144
    .line 145
    new-instance v4, Ljava/lang/StringBuilder;

    .line 146
    .line 147
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 148
    .line 149
    .line 150
    const-string v5, "Invalid header signature; read "

    .line 151
    .line 152
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    .line 154
    .line 155
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->longToHex(J)Ljava/lang/String;

    .line 156
    .line 157
    .line 158
    move-result-object v0

    .line 159
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    .line 161
    .line 162
    const-string v0, ", expected "

    .line 163
    .line 164
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    .line 166
    .line 167
    invoke-direct {p0, v2, v3}, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->longToHex(J)Ljava/lang/String;

    .line 168
    .line 169
    .line 170
    move-result-object v0

    .line 171
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    .line 173
    .line 174
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 175
    .line 176
    .line 177
    move-result-object v0

    .line 178
    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 179
    .line 180
    .line 181
    throw p1
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private longToHex(J)Ljava/lang/String;
    .locals 1

    .line 1
    new-instance v0, Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/HexDump;->longToHex(J)[C

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([C)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_data:[B

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->bigBlockSize:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBATArray()[I
    .locals 5

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_bat_count:I

    .line 2
    .line 3
    const/16 v1, 0x6d

    .line 4
    .line 5
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    new-array v1, v0, [I

    .line 10
    .line 11
    const/16 v2, 0x4c

    .line 12
    .line 13
    const/4 v3, 0x0

    .line 14
    :goto_0
    if-ge v3, v0, :cond_0

    .line 15
    .line 16
    iget-object v4, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_data:[B

    .line 17
    .line 18
    invoke-static {v4, v2}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getInt([BI)I

    .line 19
    .line 20
    .line 21
    move-result v4

    .line 22
    aput v4, v1, v3

    .line 23
    .line 24
    add-int/lit8 v2, v2, 0x4

    .line 25
    .line 26
    add-int/lit8 v3, v3, 0x1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    return-object v1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getBATCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_bat_count:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBigBlockSize()Lcom/intsig/office/fc/fs/filesystem/BlockSize;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->bigBlockSize:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPropertyStart()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_property_start:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSBATCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_sbat_count:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSBATStart()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_sbat_start:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getXBATCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_xbat_count:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getXBATIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_xbat_start:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setBATCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_bat_count:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPropertyStart(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_property_start:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSBATBlockCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_sbat_count:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSBATStart(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->_sbat_start:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public interface abstract Lcom/intsig/office/fc/fs/filesystem/CFBConstants;
.super Ljava/lang/Object;
.source "CFBConstants.java"


# static fields
.field public static final BIG_BLOCK_MINIMUM_DOCUMENT_SIZE:I = 0x1000

.field public static final DIFAT_SECTOR_BLOCK:I = -0x4

.field public static final END_OF_CHAIN:I = -0x2

.field public static final FAT_SECTOR_BLOCK:I = -0x3

.field public static final LARGER_BIG_BLOCK_SIZE:I = 0x1000

.field public static final LARGER_BIG_BLOCK_SIZE_DETAILS:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

.field public static final LARGEST_REGULAR_SECTOR_NUMBER:I = -0x5

.field public static final PROPERTY_SIZE:I = 0x80

.field public static final SMALLER_BIG_BLOCK_SIZE:I = 0x200

.field public static final SMALLER_BIG_BLOCK_SIZE_DETAILS:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

.field public static final SMALL_BLOCK_SIZE:I = 0x40

.field public static final UNUSED_BLOCK:I = -0x1


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 2
    .line 3
    const/16 v1, 0x200

    .line 4
    .line 5
    const/16 v2, 0x9

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/fs/filesystem/BlockSize;-><init>(IS)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/intsig/office/fc/fs/filesystem/CFBConstants;->SMALLER_BIG_BLOCK_SIZE_DETAILS:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 11
    .line 12
    new-instance v0, Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 13
    .line 14
    const/16 v1, 0x1000

    .line 15
    .line 16
    const/16 v2, 0xc

    .line 17
    .line 18
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/fs/filesystem/BlockSize;-><init>(IS)V

    .line 19
    .line 20
    .line 21
    sput-object v0, Lcom/intsig/office/fc/fs/filesystem/CFBConstants;->LARGER_BIG_BLOCK_SIZE_DETAILS:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.class public Lcom/intsig/office/fc/fs/filesystem/BlockSize;
.super Ljava/lang/Object;
.source "BlockSize.java"


# instance fields
.field private bigBlockSize:I

.field private headerValue:S


# direct methods
.method protected constructor <init>(IS)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput p1, p0, Lcom/intsig/office/fc/fs/filesystem/BlockSize;->bigBlockSize:I

    .line 5
    .line 6
    iput-short p2, p0, Lcom/intsig/office/fc/fs/filesystem/BlockSize;->headerValue:S

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public getBATEntriesPerBlock()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/filesystem/BlockSize;->bigBlockSize:I

    .line 2
    .line 3
    div-int/lit8 v0, v0, 0x4

    .line 4
    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBigBlockSize()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/filesystem/BlockSize;->bigBlockSize:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHeaderValue()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/fs/filesystem/BlockSize;->headerValue:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNextXBATChainOffset()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/fs/filesystem/BlockSize;->getXBATEntriesPerBlock()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    mul-int/lit8 v0, v0, 0x4

    .line 6
    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPropertiesPerBlock()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/filesystem/BlockSize;->bigBlockSize:I

    .line 2
    .line 3
    div-int/lit16 v0, v0, 0x80

    .line 4
    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getXBATEntriesPerBlock()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/fs/filesystem/BlockSize;->getBATEntriesPerBlock()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    add-int/lit8 v0, v0, -0x1

    .line 6
    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

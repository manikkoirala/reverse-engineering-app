.class public Lcom/intsig/office/fc/fs/filesystem/Property;
.super Ljava/lang/Object;
.source "Property.java"


# static fields
.field private static final CHILD_PROPERTY_OFFSET:I = 0x4c

.field public static final DIRECTORY_TYPE:B = 0x1t

.field public static final DOCUMENT_TYPE:B = 0x2t

.field private static final NAME_SIZE_OFFSET:I = 0x40

.field private static final NEXT_PROPERTY_OFFSET:I = 0x48

.field private static final PREVIOUS_PROPERTY_OFFSET:I = 0x44

.field public static final PROPERTY_TYPE_OFFSET:I = 0x42

.field public static final ROOT_TYPE:B = 0x5t

.field private static final SIZE_OFFSET:I = 0x78

.field private static final START_BLOCK_OFFSET:I = 0x74

.field protected static final _NODE_BLACK:B = 0x1t

.field protected static final _NODE_RED:B = 0x0t

.field protected static final _NO_INDEX:I = -0x1

.field private static _big_block_minimum_bytes:I = 0x1000


# instance fields
.field private _chlid_property:I

.field private _name:Ljava/lang/String;

.field private _name_size:S

.field private _next_property:I

.field private _previous_property:I

.field private _property_type:B

.field private _size:I

.field private _start_block:I

.field private blockSize:I

.field private blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

.field private documentRawData:[B

.field protected properties:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/intsig/office/fc/fs/filesystem/Property;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(I[BI)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance p1, Ljava/util/HashMap;

    .line 5
    .line 6
    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->properties:Ljava/util/Map;

    .line 10
    .line 11
    add-int/lit8 p1, p3, 0x40

    .line 12
    .line 13
    invoke-static {p2, p1}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getShort([BI)S

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    iput-short p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_name_size:S

    .line 18
    .line 19
    add-int/lit8 p1, p3, 0x44

    .line 20
    .line 21
    invoke-static {p2, p1}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getShort([BI)S

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    iput p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_previous_property:I

    .line 26
    .line 27
    add-int/lit8 p1, p3, 0x48

    .line 28
    .line 29
    invoke-static {p2, p1}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getShort([BI)S

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    iput p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_next_property:I

    .line 34
    .line 35
    add-int/lit8 p1, p3, 0x4c

    .line 36
    .line 37
    invoke-static {p2, p1}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getShort([BI)S

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    iput p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_chlid_property:I

    .line 42
    .line 43
    add-int/lit8 p1, p3, 0x74

    .line 44
    .line 45
    invoke-static {p2, p1}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getInt([BI)I

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    iput p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_start_block:I

    .line 50
    .line 51
    add-int/lit8 p1, p3, 0x78

    .line 52
    .line 53
    invoke-static {p2, p1}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getInt([BI)I

    .line 54
    .line 55
    .line 56
    move-result p1

    .line 57
    iput p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_size:I

    .line 58
    .line 59
    add-int/lit8 p1, p3, 0x42

    .line 60
    .line 61
    aget-byte p1, p2, p1

    .line 62
    .line 63
    iput-byte p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_property_type:B

    .line 64
    .line 65
    iget-short v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_name_size:S

    .line 66
    .line 67
    div-int/lit8 v0, v0, 0x2

    .line 68
    .line 69
    const/4 v1, 0x1

    .line 70
    sub-int/2addr v0, v1

    .line 71
    if-ge v0, v1, :cond_1

    .line 72
    .line 73
    const/4 p2, 0x5

    .line 74
    if-ne p1, p2, :cond_0

    .line 75
    .line 76
    const-string p1, "Root Entry"

    .line 77
    .line 78
    iput-object p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_name:Ljava/lang/String;

    .line 79
    .line 80
    goto :goto_1

    .line 81
    :cond_0
    const-string p1, "aaa"

    .line 82
    .line 83
    iput-object p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_name:Ljava/lang/String;

    .line 84
    .line 85
    goto :goto_1

    .line 86
    :cond_1
    new-array p1, v0, [C

    .line 87
    .line 88
    const/4 v1, 0x0

    .line 89
    const/4 v2, 0x0

    .line 90
    const/4 v3, 0x0

    .line 91
    :goto_0
    if-ge v2, v0, :cond_2

    .line 92
    .line 93
    add-int v4, v3, p3

    .line 94
    .line 95
    invoke-static {p2, v4}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getShort([BI)S

    .line 96
    .line 97
    .line 98
    move-result v4

    .line 99
    int-to-char v4, v4

    .line 100
    aput-char v4, p1, v2

    .line 101
    .line 102
    add-int/lit8 v3, v3, 0x2

    .line 103
    .line 104
    add-int/lit8 v2, v2, 0x1

    .line 105
    .line 106
    goto :goto_0

    .line 107
    :cond_2
    new-instance p2, Ljava/lang/String;

    .line 108
    .line 109
    invoke-direct {p2, p1, v1, v0}, Ljava/lang/String;-><init>([CII)V

    .line 110
    .line 111
    .line 112
    iput-object p2, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_name:Ljava/lang/String;

    .line 113
    .line 114
    :goto_1
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private getBlockIndexForOffset(I)I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blockSize:I

    .line 2
    .line 3
    div-int/2addr p1, v0

    .line 4
    return p1
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private getByteForOffset(I)I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blockSize:I

    .line 2
    .line 3
    div-int v1, p1, v0

    .line 4
    .line 5
    mul-int v0, v0, v1

    .line 6
    .line 7
    sub-int/2addr p1, v0

    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 9
    .line 10
    aget-object v0, v0, v1

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    aget-byte p1, v0, p1

    .line 17
    .line 18
    and-int/lit16 p1, p1, 0xff

    .line 19
    .line 20
    return p1
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public addChildProperty(Lcom/intsig/office/fc/fs/filesystem/Property;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->properties:Ljava/util/Map;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/filesystem/Property;->getName()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public dispose()V
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->documentRawData:[B

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_name:Ljava/lang/String;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->properties:Ljava/util/Map;

    .line 9
    .line 10
    if-eqz v1, :cond_1

    .line 11
    .line 12
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    if-eqz v2, :cond_0

    .line 25
    .line 26
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    check-cast v2, Ljava/lang/String;

    .line 31
    .line 32
    iget-object v3, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->properties:Ljava/util/Map;

    .line 33
    .line 34
    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    check-cast v2, Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 39
    .line 40
    invoke-virtual {v2}, Lcom/intsig/office/fc/fs/filesystem/Property;->dispose()V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->properties:Ljava/util/Map;

    .line 45
    .line 46
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 47
    .line 48
    .line 49
    iput-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->properties:Ljava/util/Map;

    .line 50
    .line 51
    :cond_1
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getBlocks()[Lcom/intsig/office/fc/fs/storage/RawDataBlock;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getChildPropertyIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_chlid_property:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getChlidProperty(Ljava/lang/String;)Lcom/intsig/office/fc/fs/filesystem/Property;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->properties:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 8
    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getDocumentRawData()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->documentRawData:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getInt(I)I
    .locals 3

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/fs/filesystem/Property;->getByteForOffset(I)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    add-int/lit8 v1, p1, 0x1

    .line 6
    .line 7
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/fs/filesystem/Property;->getByteForOffset(I)I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    add-int/lit8 v2, p1, 0x2

    .line 12
    .line 13
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/fs/filesystem/Property;->getByteForOffset(I)I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    add-int/lit8 p1, p1, 0x3

    .line 18
    .line 19
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/fs/filesystem/Property;->getByteForOffset(I)I

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    shl-int/lit8 p1, p1, 0x18

    .line 24
    .line 25
    shl-int/lit8 v2, v2, 0x10

    .line 26
    .line 27
    add-int/2addr p1, v2

    .line 28
    shl-int/lit8 v1, v1, 0x8

    .line 29
    .line 30
    add-int/2addr p1, v1

    .line 31
    shl-int/lit8 v0, v0, 0x0

    .line 32
    .line 33
    add-int/2addr p1, v0

    .line 34
    return p1
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getLong(I)J
    .locals 5

    .line 1
    add-int/lit8 v0, p1, 0x8

    .line 2
    .line 3
    add-int/lit8 v0, v0, -0x1

    .line 4
    .line 5
    const-wide/16 v1, 0x0

    .line 6
    .line 7
    :goto_0
    if-lt v0, p1, :cond_0

    .line 8
    .line 9
    const/16 v3, 0x8

    .line 10
    .line 11
    shl-long/2addr v1, v3

    .line 12
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/fs/filesystem/Property;->getByteForOffset(I)I

    .line 13
    .line 14
    .line 15
    move-result v3

    .line 16
    and-int/lit16 v3, v3, 0xff

    .line 17
    .line 18
    int-to-long v3, v3

    .line 19
    or-long/2addr v1, v3

    .line 20
    add-int/lit8 v0, v0, -0x1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    return-wide v1
    .line 24
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_name:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNextPropertyIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_next_property:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPreviousPropertyIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_previous_property:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPropertyRawDataSize()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    aget-object v0, v0, v1

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    array-length v0, v0

    .line 13
    iget-object v1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 14
    .line 15
    array-length v1, v1

    .line 16
    mul-int v0, v0, v1

    .line 17
    .line 18
    :goto_0
    int-to-long v0, v0

    .line 19
    return-wide v0

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->documentRawData:[B

    .line 21
    .line 22
    array-length v0, v0

    .line 23
    goto :goto_0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getRecordData(I)[B
    .locals 7

    .line 1
    add-int/lit8 v0, p1, 0x4

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/fs/filesystem/Property;->getUInt(I)J

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    long-to-int v1, v0

    .line 8
    add-int/lit8 v1, v1, 0x8

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    if-gez v1, :cond_0

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    :cond_0
    iget-object v2, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->documentRawData:[B

    .line 15
    .line 16
    if-eqz v2, :cond_1

    .line 17
    .line 18
    array-length v2, v2

    .line 19
    if-ge v2, v1, :cond_2

    .line 20
    .line 21
    :cond_1
    iget v2, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blockSize:I

    .line 22
    .line 23
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    new-array v2, v2, [B

    .line 28
    .line 29
    iput-object v2, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->documentRawData:[B

    .line 30
    .line 31
    :cond_2
    iget v2, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blockSize:I

    .line 32
    .line 33
    div-int v3, p1, v2

    .line 34
    .line 35
    add-int v4, p1, v1

    .line 36
    .line 37
    div-int v5, v4, v2

    .line 38
    .line 39
    if-le v5, v3, :cond_4

    .line 40
    .line 41
    rem-int/2addr p1, v2

    .line 42
    iget-object v1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 43
    .line 44
    aget-object v1, v1, v3

    .line 45
    .line 46
    invoke-virtual {v1}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    iget-object v2, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->documentRawData:[B

    .line 51
    .line 52
    iget v6, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blockSize:I

    .line 53
    .line 54
    sub-int/2addr v6, p1

    .line 55
    invoke-static {v1, p1, v2, v0, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56
    .line 57
    .line 58
    iget v1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blockSize:I

    .line 59
    .line 60
    sub-int/2addr v1, p1

    .line 61
    add-int/lit8 v3, v3, 0x1

    .line 62
    .line 63
    if-ge v3, v5, :cond_3

    .line 64
    .line 65
    :goto_0
    if-ge v3, v5, :cond_3

    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 68
    .line 69
    aget-object p1, p1, v3

    .line 70
    .line 71
    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    iget-object v2, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->documentRawData:[B

    .line 76
    .line 77
    iget v6, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blockSize:I

    .line 78
    .line 79
    invoke-static {p1, v0, v2, v1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 80
    .line 81
    .line 82
    iget p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blockSize:I

    .line 83
    .line 84
    add-int/2addr v1, p1

    .line 85
    add-int/lit8 v3, v3, 0x1

    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_3
    iget-object p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 89
    .line 90
    array-length v2, p1

    .line 91
    if-ge v5, v2, :cond_5

    .line 92
    .line 93
    aget-object p1, p1, v5

    .line 94
    .line 95
    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    iget-object v2, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->documentRawData:[B

    .line 100
    .line 101
    iget v3, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blockSize:I

    .line 102
    .line 103
    rem-int/2addr v4, v3

    .line 104
    invoke-static {p1, v0, v2, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 105
    .line 106
    .line 107
    goto :goto_1

    .line 108
    :cond_4
    rem-int/2addr p1, v2

    .line 109
    iget-object v2, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 110
    .line 111
    aget-object v2, v2, v3

    .line 112
    .line 113
    invoke-virtual {v2}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    .line 114
    .line 115
    .line 116
    move-result-object v2

    .line 117
    iget-object v3, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->documentRawData:[B

    .line 118
    .line 119
    invoke-static {v2, p1, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 120
    .line 121
    .line 122
    :cond_5
    :goto_1
    iget-object p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->documentRawData:[B

    .line 123
    .line 124
    return-object p1
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public getSize()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_size:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStartBlock()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_start_block:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getUInt(I)J
    .locals 4

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/fs/filesystem/Property;->getInt(I)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    int-to-long v0, p1

    .line 6
    const-wide v2, 0xffffffffL

    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    and-long/2addr v0, v2

    .line 12
    return-wide v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getUShort(I)I
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/fs/filesystem/Property;->getByteForOffset(I)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    add-int/lit8 p1, p1, 0x1

    .line 6
    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/fs/filesystem/Property;->getByteForOffset(I)I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    shl-int/lit8 p1, p1, 0x8

    .line 12
    .line 13
    shl-int/lit8 v0, v0, 0x0

    .line 14
    .line 15
    add-int/2addr p1, v0

    .line 16
    return p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public isDirectory()Z
    .locals 2

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_property_type:B

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isDocument()Z
    .locals 2

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_property_type:B

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isRoot()Z
    .locals 2

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->_property_type:B

    .line 2
    .line 3
    const/4 v1, 0x5

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setBlocks([Lcom/intsig/office/fc/fs/storage/RawDataBlock;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    aget-object p1, p1, v0

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    array-length p1, p1

    .line 11
    iput p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blockSize:I

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDocumentRawData([B)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->documentRawData:[B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public shouldUseSmallBlocks()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/fs/filesystem/Property;->getSize()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    sget v1, Lcom/intsig/office/fc/fs/filesystem/Property;->_big_block_minimum_bytes:I

    .line 6
    .line 7
    if-ge v0, v1, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public writeByte(Ljava/io/OutputStream;II)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blockSize:I

    .line 2
    .line 3
    const/16 v1, 0x10

    .line 4
    .line 5
    mul-int/lit8 v0, v0, 0x10

    .line 6
    .line 7
    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    new-array v0, v0, [B

    .line 12
    .line 13
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/fs/filesystem/Property;->getBlockIndexForOffset(I)I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    iget v3, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blockSize:I

    .line 18
    .line 19
    mul-int v4, v3, v2

    .line 20
    .line 21
    sub-int/2addr p2, v4

    .line 22
    sub-int/2addr v3, p2

    .line 23
    invoke-static {p3, v3}, Ljava/lang/Math;->min(II)I

    .line 24
    .line 25
    .line 26
    move-result v3

    .line 27
    iget-object v4, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 28
    .line 29
    aget-object v4, v4, v2

    .line 30
    .line 31
    invoke-virtual {v4}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    .line 32
    .line 33
    .line 34
    move-result-object v4

    .line 35
    const/4 v5, 0x0

    .line 36
    invoke-static {v4, p2, v0, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 37
    .line 38
    .line 39
    const/4 p2, 0x1

    .line 40
    :goto_0
    if-gt v3, p3, :cond_3

    .line 41
    .line 42
    iget-object v4, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 43
    .line 44
    array-length v6, v4

    .line 45
    if-ge v2, v6, :cond_3

    .line 46
    .line 47
    if-ge p2, v1, :cond_2

    .line 48
    .line 49
    add-int/lit8 v2, v2, 0x1

    .line 50
    .line 51
    add-int/lit8 p2, p2, 0x1

    .line 52
    .line 53
    iget v6, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blockSize:I

    .line 54
    .line 55
    add-int/2addr v6, v3

    .line 56
    if-le v6, p3, :cond_1

    .line 57
    .line 58
    if-le p3, v3, :cond_0

    .line 59
    .line 60
    array-length p2, v4

    .line 61
    if-ge v2, p2, :cond_0

    .line 62
    .line 63
    aget-object p2, v4, v2

    .line 64
    .line 65
    invoke-virtual {p2}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    .line 66
    .line 67
    .line 68
    move-result-object p2

    .line 69
    sub-int v1, p3, v3

    .line 70
    .line 71
    invoke-static {p2, v5, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 72
    .line 73
    .line 74
    :cond_0
    invoke-virtual {p1, v0, v5, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 75
    .line 76
    .line 77
    goto :goto_1

    .line 78
    :cond_1
    aget-object v4, v4, v2

    .line 79
    .line 80
    invoke-virtual {v4}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    .line 81
    .line 82
    .line 83
    move-result-object v4

    .line 84
    iget v6, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blockSize:I

    .line 85
    .line 86
    invoke-static {v4, v5, v0, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 87
    .line 88
    .line 89
    iget v4, p0, Lcom/intsig/office/fc/fs/filesystem/Property;->blockSize:I

    .line 90
    .line 91
    add-int/2addr v3, v4

    .line 92
    goto :goto_0

    .line 93
    :cond_2
    invoke-virtual {p1, v0, v5, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 94
    .line 95
    .line 96
    sub-int/2addr p3, v3

    .line 97
    const/4 p2, 0x0

    .line 98
    const/4 v3, 0x0

    .line 99
    goto :goto_0

    .line 100
    :cond_3
    :goto_1
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

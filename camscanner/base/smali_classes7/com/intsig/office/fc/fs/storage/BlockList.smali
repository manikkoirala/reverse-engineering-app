.class public Lcom/intsig/office/fc/fs/storage/BlockList;
.super Ljava/lang/Object;
.source "BlockList.java"


# instance fields
.field private _bat:Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;

.field private _blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/intsig/office/fc/fs/filesystem/BlockSize;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    invoke-virtual {p2}, Lcom/intsig/office/fc/fs/filesystem/BlockSize;->getBigBlockSize()I

    move-result p2

    .line 4
    :cond_0
    new-array v1, p2, [B

    .line 5
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-gtz v2, :cond_1

    goto :goto_0

    .line 6
    :cond_1
    new-instance v3, Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    invoke-direct {v3, v1}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;-><init>([B)V

    .line 7
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eq v2, p2, :cond_0

    .line 8
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    iput-object p1, p0, Lcom/intsig/office/fc/fs/storage/BlockList;->_blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    return-void
.end method

.method public constructor <init>([Lcom/intsig/office/fc/fs/storage/RawDataBlock;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/intsig/office/fc/fs/storage/BlockList;->_blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    return-void
.end method


# virtual methods
.method public blockCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/storage/BlockList;->_blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public fetchBlocks(II)[Lcom/intsig/office/fc/fs/storage/RawDataBlock;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/storage/BlockList;->_bat:Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1, p2, p0}, Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;->fetchBlocks(IILcom/intsig/office/fc/fs/storage/BlockList;)[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1

    .line 10
    :cond_0
    new-instance p1, Ljava/io/IOException;

    .line 11
    .line 12
    const-string p2, "Improperly initialized list: no block allocation table provided"

    .line 13
    .line 14
    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    throw p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected get(I)Lcom/intsig/office/fc/fs/storage/RawDataBlock;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/storage/BlockList;->_blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 2
    .line 3
    aget-object p1, v0, p1

    .line 4
    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public remove(I)Lcom/intsig/office/fc/fs/storage/RawDataBlock;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    if-ltz p1, :cond_1

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/fc/fs/storage/BlockList;->_blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 5
    .line 6
    array-length v2, v1

    .line 7
    if-lt p1, v2, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    aget-object v2, v1, p1

    .line 11
    .line 12
    aput-object v0, v1, p1

    .line 13
    .line 14
    return-object v2

    .line 15
    :cond_1
    :goto_0
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBAT(Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/fs/storage/BlockList;->_bat:Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public zap(I)V
    .locals 2

    .line 1
    if-ltz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/fs/storage/BlockList;->_blocks:[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 4
    .line 5
    array-length v1, v0

    .line 6
    if-ge p1, v1, :cond_0

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    aput-object v1, v0, p1

    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

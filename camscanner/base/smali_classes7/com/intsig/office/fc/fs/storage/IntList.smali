.class public Lcom/intsig/office/fc/fs/storage/IntList;
.super Ljava/lang/Object;
.source "IntList.java"


# static fields
.field private static final _default_size:I = 0x80


# instance fields
.field private _array:[I

.field private _limit:I

.field private fillval:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v0, 0x80

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/fc/fs/storage/IntList;-><init>(II)V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p2, 0x0

    .line 3
    iput p2, p0, Lcom/intsig/office/fc/fs/storage/IntList;->fillval:I

    .line 4
    new-array p1, p1, [I

    iput-object p1, p0, Lcom/intsig/office/fc/fs/storage/IntList;->_array:[I

    .line 5
    iput p2, p0, Lcom/intsig/office/fc/fs/storage/IntList;->_limit:I

    return-void
.end method

.method private fillArray(I[II)V
    .locals 1

    .line 1
    :goto_0
    array-length v0, p2

    .line 2
    if-ge p3, v0, :cond_0

    .line 3
    .line 4
    aput p1, p2, p3

    .line 5
    .line 6
    add-int/lit8 p3, p3, 0x1

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private growArray(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/storage/IntList;->_array:[I

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    if-ne p1, v1, :cond_0

    .line 5
    .line 6
    add-int/lit8 p1, p1, 0x1

    .line 7
    .line 8
    :cond_0
    new-array p1, p1, [I

    .line 9
    .line 10
    iget v1, p0, Lcom/intsig/office/fc/fs/storage/IntList;->fillval:I

    .line 11
    .line 12
    if-eqz v1, :cond_1

    .line 13
    .line 14
    array-length v0, v0

    .line 15
    invoke-direct {p0, v1, p1, v0}, Lcom/intsig/office/fc/fs/storage/IntList;->fillArray(I[II)V

    .line 16
    .line 17
    .line 18
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/storage/IntList;->_array:[I

    .line 19
    .line 20
    iget v1, p0, Lcom/intsig/office/fc/fs/storage/IntList;->_limit:I

    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    invoke-static {v0, v2, p1, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 24
    .line 25
    .line 26
    iput-object p1, p0, Lcom/intsig/office/fc/fs/storage/IntList;->_array:[I

    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public add(I)Z
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/storage/IntList;->_limit:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/fs/storage/IntList;->_array:[I

    .line 4
    .line 5
    array-length v1, v1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    mul-int/lit8 v0, v0, 0x2

    .line 9
    .line 10
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/fs/storage/IntList;->growArray(I)V

    .line 11
    .line 12
    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/fs/storage/IntList;->_array:[I

    .line 14
    .line 15
    iget v1, p0, Lcom/intsig/office/fc/fs/storage/IntList;->_limit:I

    .line 16
    .line 17
    add-int/lit8 v2, v1, 0x1

    .line 18
    .line 19
    iput v2, p0, Lcom/intsig/office/fc/fs/storage/IntList;->_limit:I

    .line 20
    .line 21
    aput p1, v0, v1

    .line 22
    .line 23
    const/4 p1, 0x1

    .line 24
    return p1
.end method

.method public get(I)I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/storage/IntList;->_limit:I

    .line 2
    .line 3
    if-lt p1, v0, :cond_0

    .line 4
    .line 5
    const/4 p1, -0x2

    .line 6
    return p1

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/fs/storage/IntList;->_array:[I

    .line 8
    .line 9
    aget p1, v0, p1

    .line 10
    .line 11
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public size()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/fs/storage/IntList;->_limit:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

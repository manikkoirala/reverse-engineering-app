.class public final Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;
.super Ljava/lang/Object;
.source "BlockAllocationTableReader.java"


# instance fields
.field private final _entries:Lcom/intsig/office/fc/fs/storage/IntList;

.field private bigBlockSize:Lcom/intsig/office/fc/fs/filesystem/BlockSize;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/fs/filesystem/BlockSize;I[IIILcom/intsig/office/fc/fs/storage/BlockList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;->bigBlockSize:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 3
    new-instance v0, Lcom/intsig/office/fc/fs/storage/IntList;

    invoke-direct {v0}, Lcom/intsig/office/fc/fs/storage/IntList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;->_entries:Lcom/intsig/office/fc/fs/storage/IntList;

    .line 4
    array-length v0, p3

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 5
    new-array v1, p2, [Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    .line 6
    aget v4, p3, v3

    .line 7
    invoke-virtual {p6}, Lcom/intsig/office/fc/fs/storage/BlockList;->blockCount()I

    move-result v5

    if-gt v4, v5, :cond_0

    .line 8
    invoke-virtual {p6, v4}, Lcom/intsig/office/fc/fs/storage/BlockList;->remove(I)Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    move-result-object v4

    aput-object v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 9
    :cond_0
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Your file contains "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p6}, Lcom/intsig/office/fc/fs/storage/BlockList;->blockCount()I

    move-result p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, " sectors, but the initial DIFAT array at index "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, " referenced block # "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, ". This isn\'t allowed and  your file is corrupt"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    if-ge v3, p2, :cond_5

    if-ltz p5, :cond_4

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/filesystem/BlockSize;->getXBATEntriesPerBlock()I

    move-result p3

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/filesystem/BlockSize;->getNextXBATChainOffset()I

    move-result p1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, p4, :cond_5

    sub-int v4, p2, v3

    .line 12
    invoke-static {v4, p3}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 13
    invoke-virtual {p6, p5}, Lcom/intsig/office/fc/fs/storage/BlockList;->remove(I)Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    move-result-object p5

    invoke-virtual {p5}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    move-result-object p5

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_2
    if-ge v5, v4, :cond_2

    add-int/lit8 v7, v3, 0x1

    .line 14
    invoke-static {p5, v6}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getInt([BI)I

    move-result v8

    invoke-virtual {p6, v8}, Lcom/intsig/office/fc/fs/storage/BlockList;->remove(I)Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    move-result-object v8

    aput-object v8, v1, v3

    add-int/lit8 v6, v6, 0x4

    add-int/lit8 v5, v5, 0x1

    move v3, v7

    goto :goto_2

    .line 15
    :cond_2
    invoke-static {p5, p1}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getInt([BI)I

    move-result p5

    const/4 v4, -0x2

    if-ne p5, v4, :cond_3

    goto :goto_3

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 16
    :cond_4
    new-instance p1, Ljava/io/IOException;

    const-string p2, "BAT count exceeds limit, yet XBAT index indicates no valid entries"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    :goto_3
    if-ne v3, p2, :cond_6

    .line 17
    invoke-direct {p0, v1, p6}, Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;->setEntries([Lcom/intsig/office/fc/fs/storage/RawDataBlock;Lcom/intsig/office/fc/fs/storage/BlockList;)V

    return-void

    .line 18
    :cond_6
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Could not find all blocks"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Lcom/intsig/office/fc/fs/filesystem/BlockSize;[Lcom/intsig/office/fc/fs/storage/RawDataBlock;Lcom/intsig/office/fc/fs/storage/BlockList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;->bigBlockSize:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 21
    new-instance p1, Lcom/intsig/office/fc/fs/storage/IntList;

    invoke-direct {p1}, Lcom/intsig/office/fc/fs/storage/IntList;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;->_entries:Lcom/intsig/office/fc/fs/storage/IntList;

    .line 22
    invoke-direct {p0, p2, p3}, Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;->setEntries([Lcom/intsig/office/fc/fs/storage/RawDataBlock;Lcom/intsig/office/fc/fs/storage/BlockList;)V

    return-void
.end method

.method private setEntries([Lcom/intsig/office/fc/fs/storage/RawDataBlock;Lcom/intsig/office/fc/fs/storage/BlockList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;->bigBlockSize:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/fs/filesystem/BlockSize;->getBATEntriesPerBlock()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    const/4 v2, 0x0

    .line 9
    :goto_0
    array-length v3, p1

    .line 10
    if-ge v2, v3, :cond_2

    .line 11
    .line 12
    aget-object v3, p1, v2

    .line 13
    .line 14
    invoke-virtual {v3}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    .line 15
    .line 16
    .line 17
    move-result-object v3

    .line 18
    const/4 v4, 0x0

    .line 19
    const/4 v5, 0x0

    .line 20
    :goto_1
    if-ge v4, v0, :cond_1

    .line 21
    .line 22
    invoke-static {v3, v5}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getInt([BI)I

    .line 23
    .line 24
    .line 25
    move-result v6

    .line 26
    const/4 v7, -0x1

    .line 27
    if-ne v6, v7, :cond_0

    .line 28
    .line 29
    iget-object v7, p0, Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;->_entries:Lcom/intsig/office/fc/fs/storage/IntList;

    .line 30
    .line 31
    invoke-virtual {v7}, Lcom/intsig/office/fc/fs/storage/IntList;->size()I

    .line 32
    .line 33
    .line 34
    move-result v7

    .line 35
    invoke-virtual {p2, v7}, Lcom/intsig/office/fc/fs/storage/BlockList;->zap(I)V

    .line 36
    .line 37
    .line 38
    :cond_0
    iget-object v7, p0, Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;->_entries:Lcom/intsig/office/fc/fs/storage/IntList;

    .line 39
    .line 40
    invoke-virtual {v7, v6}, Lcom/intsig/office/fc/fs/storage/IntList;->add(I)Z

    .line 41
    .line 42
    .line 43
    add-int/lit8 v5, v5, 0x4

    .line 44
    .line 45
    add-int/lit8 v4, v4, 0x1

    .line 46
    .line 47
    goto :goto_1

    .line 48
    :cond_1
    const/4 v3, 0x0

    .line 49
    aput-object v3, p1, v2

    .line 50
    .line 51
    add-int/lit8 v2, v2, 0x1

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_2
    invoke-virtual {p2, p0}, Lcom/intsig/office/fc/fs/storage/BlockList;->setBAT(Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method


# virtual methods
.method public fetchBlocks(IILcom/intsig/office/fc/fs/storage/BlockList;)[Lcom/intsig/office/fc/fs/storage/RawDataBlock;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    :goto_0
    const/4 v2, -0x2

    .line 8
    if-eq p1, v2, :cond_2

    .line 9
    .line 10
    :try_start_0
    invoke-virtual {p3, p1}, Lcom/intsig/office/fc/fs/storage/BlockList;->remove(I)Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 11
    .line 12
    .line 13
    move-result-object v3

    .line 14
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    iget-object v3, p0, Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;->_entries:Lcom/intsig/office/fc/fs/storage/IntList;

    .line 18
    .line 19
    invoke-virtual {v3, p1}, Lcom/intsig/office/fc/fs/storage/IntList;->get(I)I

    .line 20
    .line 21
    .line 22
    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    const/4 v1, 0x0

    .line 24
    goto :goto_0

    .line 25
    :catch_0
    move-exception v3

    .line 26
    if-ne p1, p2, :cond_0

    .line 27
    .line 28
    goto :goto_1

    .line 29
    :cond_0
    if-nez p1, :cond_1

    .line 30
    .line 31
    if-eqz v1, :cond_1

    .line 32
    .line 33
    :goto_1
    const/4 p1, -0x2

    .line 34
    goto :goto_0

    .line 35
    :cond_1
    throw v3

    .line 36
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    new-array p1, p1, [Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 41
    .line 42
    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    check-cast p1, [Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 47
    .line 48
    return-object p1
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

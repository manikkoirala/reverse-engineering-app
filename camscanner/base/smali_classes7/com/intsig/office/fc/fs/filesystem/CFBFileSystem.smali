.class public Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;
.super Ljava/lang/Object;
.source "CFBFileSystem.java"


# instance fields
.field private bigBlockSize:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

.field private headerBlock:Lcom/intsig/office/fc/fs/storage/HeaderBlock;

.field private isGetThumbnail:Z

.field private root:Lcom/intsig/office/fc/fs/filesystem/Property;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;-><init>(Ljava/io/InputStream;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    sget-object v0, Lcom/intsig/office/fc/fs/filesystem/CFBConstants;->SMALLER_BIG_BLOCK_SIZE_DETAILS:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    iput-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->bigBlockSize:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 4
    iput-boolean p2, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->isGetThumbnail:Z

    .line 5
    :try_start_0
    new-instance p2, Lcom/intsig/office/fc/fs/storage/HeaderBlock;

    invoke-direct {p2, p1}, Lcom/intsig/office/fc/fs/storage/HeaderBlock;-><init>(Ljava/io/InputStream;)V

    iput-object p2, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->headerBlock:Lcom/intsig/office/fc/fs/storage/HeaderBlock;

    .line 6
    invoke-virtual {p2}, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->getBigBlockSize()Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->bigBlockSize:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 7
    new-instance v7, Lcom/intsig/office/fc/fs/storage/BlockList;

    invoke-direct {v7, p1, p2}, Lcom/intsig/office/fc/fs/storage/BlockList;-><init>(Ljava/io/InputStream;Lcom/intsig/office/fc/fs/filesystem/BlockSize;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 9
    new-instance v0, Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;

    iget-object p1, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->headerBlock:Lcom/intsig/office/fc/fs/storage/HeaderBlock;

    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->getBigBlockSize()Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    move-result-object v1

    iget-object p1, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->headerBlock:Lcom/intsig/office/fc/fs/storage/HeaderBlock;

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->getBATCount()I

    move-result v2

    iget-object p1, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->headerBlock:Lcom/intsig/office/fc/fs/storage/HeaderBlock;

    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->getBATArray()[I

    move-result-object v3

    iget-object p1, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->headerBlock:Lcom/intsig/office/fc/fs/storage/HeaderBlock;

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->getXBATCount()I

    move-result v4

    iget-object p1, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->headerBlock:Lcom/intsig/office/fc/fs/storage/HeaderBlock;

    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->getXBATIndex()I

    move-result v5

    move-object v6, v7

    invoke-direct/range {v0 .. v6}, Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;-><init>(Lcom/intsig/office/fc/fs/filesystem/BlockSize;I[IIILcom/intsig/office/fc/fs/storage/BlockList;)V

    .line 12
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 13
    iget-object p2, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->headerBlock:Lcom/intsig/office/fc/fs/storage/HeaderBlock;

    invoke-virtual {p2}, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->getPropertyStart()I

    move-result p2

    const/4 v0, -0x1

    invoke-virtual {v7, p2, v0}, Lcom/intsig/office/fc/fs/storage/BlockList;->fetchBlocks(II)[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    move-result-object p2

    invoke-direct {p0, p2, v7, p1}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->readProperties([Lcom/intsig/office/fc/fs/storage/RawDataBlock;Lcom/intsig/office/fc/fs/storage/BlockList;Ljava/util/List;)V

    .line 14
    iget-object p2, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->root:Lcom/intsig/office/fc/fs/filesystem/Property;

    invoke-direct {p0, p2, p1}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->createPropertyTree(Lcom/intsig/office/fc/fs/filesystem/Property;Ljava/util/List;)V

    .line 15
    invoke-direct {p0, v7}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->readSmallRawDataBlock(Lcom/intsig/office/fc/fs/storage/BlockList;)Lcom/intsig/office/fc/fs/storage/BlockList;

    move-result-object p1

    .line 16
    iget-object p2, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->root:Lcom/intsig/office/fc/fs/filesystem/Property;

    invoke-direct {p0, p1, v7, p2}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->readPrepertiesRawData(Lcom/intsig/office/fc/fs/storage/BlockList;Lcom/intsig/office/fc/fs/storage/BlockList;Lcom/intsig/office/fc/fs/filesystem/Property;)V

    return-void

    :catchall_0
    move-exception p2

    .line 17
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 18
    throw p2
.end method

.method private createPropertyTree(Lcom/intsig/office/fc/fs/filesystem/Property;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/fs/filesystem/Property;",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/fs/filesystem/Property;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/filesystem/Property;->getChildPropertyIndex()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-gez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    new-instance v1, Ljava/util/Stack;

    .line 9
    .line 10
    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    .line 11
    .line 12
    .line 13
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 18
    .line 19
    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/util/AbstractCollection;->isEmpty()Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-nez v0, :cond_4

    .line 27
    .line 28
    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    check-cast v0, Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 33
    .line 34
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/fs/filesystem/Property;->addChildProperty(Lcom/intsig/office/fc/fs/filesystem/Property;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/office/fc/fs/filesystem/Property;->isDirectory()Z

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    if-eqz v2, :cond_2

    .line 42
    .line 43
    invoke-direct {p0, v0, p2}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->createPropertyTree(Lcom/intsig/office/fc/fs/filesystem/Property;Ljava/util/List;)V

    .line 44
    .line 45
    .line 46
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/office/fc/fs/filesystem/Property;->getPreviousPropertyIndex()I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    if-ltz v2, :cond_3

    .line 51
    .line 52
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    check-cast v2, Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 57
    .line 58
    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    :cond_3
    invoke-virtual {v0}, Lcom/intsig/office/fc/fs/filesystem/Property;->getNextPropertyIndex()I

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    if-ltz v0, :cond_1

    .line 66
    .line 67
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    check-cast v0, Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 72
    .line 73
    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_4
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private getPropertyRawData(Lcom/intsig/office/fc/fs/filesystem/Property;Lcom/intsig/office/fc/fs/storage/BlockList;Lcom/intsig/office/fc/fs/storage/BlockList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/filesystem/Property;->getName()Ljava/lang/String;

    move-result-object v0

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/filesystem/Property;->getStartBlock()I

    move-result v1

    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/filesystem/Property;->shouldUseSmallBlocks()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4
    iget-object p3, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->headerBlock:Lcom/intsig/office/fc/fs/storage/HeaderBlock;

    invoke-virtual {p3}, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->getPropertyStart()I

    move-result p3

    invoke-virtual {p2, v1, p3}, Lcom/intsig/office/fc/fs/storage/BlockList;->fetchBlocks(II)[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    move-result-object p2

    goto :goto_0

    .line 5
    :cond_0
    iget-object p2, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->headerBlock:Lcom/intsig/office/fc/fs/storage/HeaderBlock;

    invoke-virtual {p2}, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->getPropertyStart()I

    move-result p2

    invoke-virtual {p3, v1, p2}, Lcom/intsig/office/fc/fs/storage/BlockList;->fetchBlocks(II)[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    move-result-object p2

    :goto_0
    if-eqz p2, :cond_5

    .line 6
    array-length p3, p2

    if-nez p3, :cond_1

    goto :goto_3

    :cond_1
    const-string p3, "Pictures"

    .line 7
    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_4

    const-string p3, "WorkBook"

    .line 8
    invoke-virtual {v0, p3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_4

    const-string p3, "PowerPoint Document"

    .line 9
    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_4

    const-string p3, "Ole"

    .line 10
    invoke-virtual {v0, p3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_4

    const-string p3, "ObjInfo"

    .line 11
    invoke-virtual {v0, p3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_4

    const-string p3, "ComObj"

    .line 12
    invoke-virtual {v0, p3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_4

    const-string p3, "EPRINT"

    .line 13
    invoke-virtual {v0, p3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_2

    goto :goto_2

    :cond_2
    const/4 p3, 0x0

    .line 14
    aget-object v0, p2, p3

    invoke-virtual {v0}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    move-result-object v0

    array-length v0, v0

    .line 15
    array-length v1, p2

    mul-int v1, v1, v0

    new-array v1, v1, [B

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 16
    :goto_1
    array-length v4, p2

    if-ge v2, v4, :cond_3

    .line 17
    aget-object v4, p2, v2

    invoke-virtual {v4}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    move-result-object v4

    invoke-static {v4, p3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v3, v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 18
    :cond_3
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/fs/filesystem/Property;->setDocumentRawData([B)V

    return-void

    .line 19
    :cond_4
    :goto_2
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/fs/filesystem/Property;->setBlocks([Lcom/intsig/office/fc/fs/storage/RawDataBlock;)V

    :cond_5
    :goto_3
    return-void
.end method

.method private readPrepertiesRawData(Lcom/intsig/office/fc/fs/storage/BlockList;Lcom/intsig/office/fc/fs/storage/BlockList;Lcom/intsig/office/fc/fs/filesystem/Property;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object p3, p3, Lcom/intsig/office/fc/fs/filesystem/Property;->properties:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {p3}, Ljava/util/Map;->values()Ljava/util/Collection;

    .line 4
    .line 5
    .line 6
    move-result-object p3

    .line 7
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object p3

    .line 11
    :cond_0
    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_2

    .line 16
    .line 17
    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/office/fc/fs/filesystem/Property;->isDocument()Z

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-eqz v1, :cond_1

    .line 28
    .line 29
    invoke-direct {p0, v0, p1, p2}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->getPropertyRawData(Lcom/intsig/office/fc/fs/filesystem/Property;Lcom/intsig/office/fc/fs/storage/BlockList;Lcom/intsig/office/fc/fs/storage/BlockList;)V

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/office/fc/fs/filesystem/Property;->isDirectory()Z

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    if-eqz v1, :cond_0

    .line 38
    .line 39
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->readPrepertiesRawData(Lcom/intsig/office/fc/fs/storage/BlockList;Lcom/intsig/office/fc/fs/storage/BlockList;Lcom/intsig/office/fc/fs/filesystem/Property;)V

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_2
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private readProperties([Lcom/intsig/office/fc/fs/storage/RawDataBlock;Lcom/intsig/office/fc/fs/storage/BlockList;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/intsig/office/fc/fs/storage/RawDataBlock;",
            "Lcom/intsig/office/fc/fs/storage/BlockList;",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/fs/filesystem/Property;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 p2, 0x0

    .line 2
    const/4 v0, 0x0

    .line 3
    :goto_0
    array-length v1, p1

    .line 4
    if-ge v0, v1, :cond_4

    .line 5
    .line 6
    aget-object v1, p1, v0

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    array-length v2, v1

    .line 13
    div-int/lit16 v2, v2, 0x80

    .line 14
    .line 15
    const/4 v3, 0x0

    .line 16
    const/4 v4, 0x0

    .line 17
    :goto_1
    if-ge v3, v2, :cond_3

    .line 18
    .line 19
    add-int/lit8 v5, v4, 0x42

    .line 20
    .line 21
    aget-byte v5, v1, v5

    .line 22
    .line 23
    const/4 v6, 0x1

    .line 24
    if-eq v5, v6, :cond_2

    .line 25
    .line 26
    const/4 v6, 0x2

    .line 27
    if-eq v5, v6, :cond_1

    .line 28
    .line 29
    const/4 v6, 0x5

    .line 30
    if-eq v5, v6, :cond_0

    .line 31
    .line 32
    goto :goto_2

    .line 33
    :cond_0
    new-instance v5, Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 34
    .line 35
    invoke-interface {p3}, Ljava/util/List;->size()I

    .line 36
    .line 37
    .line 38
    move-result v6

    .line 39
    invoke-direct {v5, v6, v1, v4}, Lcom/intsig/office/fc/fs/filesystem/Property;-><init>(I[BI)V

    .line 40
    .line 41
    .line 42
    iput-object v5, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->root:Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 43
    .line 44
    invoke-interface {p3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    .line 46
    .line 47
    goto :goto_2

    .line 48
    :cond_1
    new-instance v5, Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 49
    .line 50
    invoke-interface {p3}, Ljava/util/List;->size()I

    .line 51
    .line 52
    .line 53
    move-result v6

    .line 54
    invoke-direct {v5, v6, v1, v4}, Lcom/intsig/office/fc/fs/filesystem/Property;-><init>(I[BI)V

    .line 55
    .line 56
    .line 57
    invoke-interface {p3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    .line 59
    .line 60
    goto :goto_2

    .line 61
    :cond_2
    new-instance v5, Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 62
    .line 63
    invoke-interface {p3}, Ljava/util/List;->size()I

    .line 64
    .line 65
    .line 66
    move-result v6

    .line 67
    invoke-direct {v5, v6, v1, v4}, Lcom/intsig/office/fc/fs/filesystem/Property;-><init>(I[BI)V

    .line 68
    .line 69
    .line 70
    invoke-interface {p3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    .line 72
    .line 73
    :goto_2
    add-int/lit16 v4, v4, 0x80

    .line 74
    .line 75
    add-int/lit8 v3, v3, 0x1

    .line 76
    .line 77
    goto :goto_1

    .line 78
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_4
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private readSmallRawDataBlock(Lcom/intsig/office/fc/fs/storage/BlockList;)Lcom/intsig/office/fc/fs/storage/BlockList;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->root:Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/fs/filesystem/Property;->getStartBlock()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, -0x1

    .line 8
    invoke-virtual {p1, v0, v1}, Lcom/intsig/office/fc/fs/storage/BlockList;->fetchBlocks(II)[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iget-object v2, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->headerBlock:Lcom/intsig/office/fc/fs/storage/HeaderBlock;

    .line 13
    .line 14
    invoke-virtual {v2}, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->getBigBlockSize()Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    invoke-virtual {v2}, Lcom/intsig/office/fc/fs/filesystem/BlockSize;->getBigBlockSize()I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    const/16 v3, 0x40

    .line 23
    .line 24
    div-int/2addr v2, v3

    .line 25
    new-instance v4, Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 28
    .line 29
    .line 30
    const/4 v5, 0x0

    .line 31
    const/4 v6, 0x0

    .line 32
    :goto_0
    array-length v7, v0

    .line 33
    if-ge v6, v7, :cond_1

    .line 34
    .line 35
    aget-object v7, v0, v6

    .line 36
    .line 37
    invoke-virtual {v7}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;->getData()[B

    .line 38
    .line 39
    .line 40
    move-result-object v7

    .line 41
    const/4 v8, 0x0

    .line 42
    :goto_1
    if-ge v8, v2, :cond_0

    .line 43
    .line 44
    new-array v9, v3, [B

    .line 45
    .line 46
    mul-int/lit8 v10, v8, 0x40

    .line 47
    .line 48
    invoke-static {v7, v10, v9, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49
    .line 50
    .line 51
    new-instance v10, Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 52
    .line 53
    invoke-direct {v10, v9}, Lcom/intsig/office/fc/fs/storage/RawDataBlock;-><init>([B)V

    .line 54
    .line 55
    .line 56
    invoke-interface {v4, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    add-int/lit8 v8, v8, 0x1

    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_0
    add-int/lit8 v6, v6, 0x1

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_1
    new-instance v0, Lcom/intsig/office/fc/fs/storage/BlockList;

    .line 66
    .line 67
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 68
    .line 69
    .line 70
    move-result v2

    .line 71
    new-array v2, v2, [Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 72
    .line 73
    invoke-interface {v4, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    check-cast v2, [Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 78
    .line 79
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/fs/storage/BlockList;-><init>([Lcom/intsig/office/fc/fs/storage/RawDataBlock;)V

    .line 80
    .line 81
    .line 82
    new-instance v2, Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;

    .line 83
    .line 84
    iget-object v3, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->bigBlockSize:Lcom/intsig/office/fc/fs/filesystem/BlockSize;

    .line 85
    .line 86
    iget-object v4, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->headerBlock:Lcom/intsig/office/fc/fs/storage/HeaderBlock;

    .line 87
    .line 88
    invoke-virtual {v4}, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->getSBATStart()I

    .line 89
    .line 90
    .line 91
    move-result v4

    .line 92
    invoke-virtual {p1, v4, v1}, Lcom/intsig/office/fc/fs/storage/BlockList;->fetchBlocks(II)[Lcom/intsig/office/fc/fs/storage/RawDataBlock;

    .line 93
    .line 94
    .line 95
    move-result-object p1

    .line 96
    invoke-direct {v2, v3, p1, v0}, Lcom/intsig/office/fc/fs/storage/BlockAllocationTableReader;-><init>(Lcom/intsig/office/fc/fs/filesystem/BlockSize;[Lcom/intsig/office/fc/fs/storage/RawDataBlock;Lcom/intsig/office/fc/fs/storage/BlockList;)V

    .line 97
    .line 98
    .line 99
    return-object v0
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->headerBlock:Lcom/intsig/office/fc/fs/storage/HeaderBlock;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/fs/storage/HeaderBlock;->dispose()V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->headerBlock:Lcom/intsig/office/fc/fs/storage/HeaderBlock;

    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->root:Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 12
    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/fc/fs/filesystem/Property;->dispose()V

    .line 16
    .line 17
    .line 18
    :cond_1
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public getProperty(Ljava/lang/String;)Lcom/intsig/office/fc/fs/filesystem/Property;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->root:Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/fs/filesystem/Property;->getChlidProperty(Ljava/lang/String;)Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getPropertyRawData(Ljava/lang/String;)[B
    .locals 0

    .line 20
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->getProperty(Ljava/lang/String;)Lcom/intsig/office/fc/fs/filesystem/Property;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 21
    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/filesystem/Property;->getDocumentRawData()[B

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

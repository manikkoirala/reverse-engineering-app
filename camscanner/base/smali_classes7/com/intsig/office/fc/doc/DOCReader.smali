.class public Lcom/intsig/office/fc/doc/DOCReader;
.super Lcom/intsig/office/system/AbstractReader;
.source "DOCReader.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DOCReader"


# instance fields
.field private bms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/common/bookmark/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field private docRealOffset:J

.field private filePath:Ljava/lang/String;

.field private hyperlinkAddress:Ljava/lang/String;

.field private hyperlinkPattern:Ljava/util/regex/Pattern;

.field private isBreakChar:Z

.field private offset:J

.field private poiDoc:Lcom/intsig/office/fc/hwpf/HWPFDocument;

.field private textboxIndex:J

.field private uri:Landroid/net/Uri;

.field private wpdoc:Lcom/intsig/office/wp/model/WPDocument;


# direct methods
.method public constructor <init>(Lcom/intsig/office/system/IControl;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/system/AbstractReader;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, "[ \\t\\r\\n]*HYPERLINK \"(.*)\"[ \\t\\r\\n]*"

    .line 5
    .line 6
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->hyperlinkPattern:Ljava/util/regex/Pattern;

    .line 11
    .line 12
    new-instance v0, Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->bms:Ljava/util/List;

    .line 18
    .line 19
    iput-object p1, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 20
    .line 21
    iput-object p2, p0, Lcom/intsig/office/fc/doc/DOCReader;->filePath:Ljava/lang/String;

    .line 22
    .line 23
    iput-object p3, p0, Lcom/intsig/office/fc/doc/DOCReader;->uri:Landroid/net/Uri;

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private adjustBookmarkOffset(JJ)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->bms:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/office/common/bookmark/Bookmark;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/common/bookmark/Bookmark;->getStart()J

    .line 20
    .line 21
    .line 22
    move-result-wide v2

    .line 23
    cmp-long v4, v2, p1

    .line 24
    .line 25
    if-ltz v4, :cond_0

    .line 26
    .line 27
    invoke-virtual {v1}, Lcom/intsig/office/common/bookmark/Bookmark;->getStart()J

    .line 28
    .line 29
    .line 30
    move-result-wide v2

    .line 31
    cmp-long v4, v2, p3

    .line 32
    .line 33
    if-gtz v4, :cond_0

    .line 34
    .line 35
    iget-wide v2, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 36
    .line 37
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/common/bookmark/Bookmark;->setStart(J)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private converFill(Lcom/intsig/office/fc/hwpf/usermodel/HWPFAutoShape;Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;I)Lcom/intsig/office/common/bg/BackgroundAndFill;
    .locals 18

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p2

    .line 4
    .line 5
    move/from16 v2, p3

    .line 6
    .line 7
    const/16 v3, 0x14

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    if-eq v2, v3, :cond_14

    .line 11
    .line 12
    const/16 v3, 0x20

    .line 13
    .line 14
    if-eq v2, v3, :cond_14

    .line 15
    .line 16
    const/16 v3, 0x21

    .line 17
    .line 18
    if-eq v2, v3, :cond_14

    .line 19
    .line 20
    const/16 v3, 0x22

    .line 21
    .line 22
    if-eq v2, v3, :cond_14

    .line 23
    .line 24
    const/16 v3, 0x26

    .line 25
    .line 26
    if-ne v2, v3, :cond_0

    .line 27
    .line 28
    goto/16 :goto_8

    .line 29
    .line 30
    :cond_0
    if-eqz p1, :cond_14

    .line 31
    .line 32
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getFillType()I

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    const/4 v3, 0x0

    .line 37
    if-eqz v2, :cond_13

    .line 38
    .line 39
    const/16 v5, 0x9

    .line 40
    .line 41
    if-ne v2, v5, :cond_1

    .line 42
    .line 43
    goto/16 :goto_7

    .line 44
    .line 45
    :cond_1
    const/4 v5, 0x1

    .line 46
    const/4 v6, 0x6

    .line 47
    const/4 v7, 0x5

    .line 48
    const/4 v8, 0x4

    .line 49
    const/4 v9, 0x7

    .line 50
    const/4 v10, 0x2

    .line 51
    if-eq v2, v9, :cond_6

    .line 52
    .line 53
    if-eq v2, v8, :cond_6

    .line 54
    .line 55
    if-eq v2, v7, :cond_6

    .line 56
    .line 57
    if-ne v2, v6, :cond_2

    .line 58
    .line 59
    goto/16 :goto_0

    .line 60
    .line 61
    :cond_2
    if-ne v2, v10, :cond_3

    .line 62
    .line 63
    iget-object v2, v0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 64
    .line 65
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getBackgroundPictureIdx()I

    .line 66
    .line 67
    .line 68
    move-result v5

    .line 69
    invoke-interface {v1, v2, v5}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getPictureData(Lcom/intsig/office/system/IControl;I)[B

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    if-eqz v2, :cond_14

    .line 74
    .line 75
    invoke-static {v2}, Lcom/intsig/office/fc/hwpf/usermodel/PictureType;->findMatchingType([B)Lcom/intsig/office/fc/hwpf/usermodel/PictureType;

    .line 76
    .line 77
    .line 78
    move-result-object v5

    .line 79
    invoke-direct {v0, v5}, Lcom/intsig/office/fc/doc/DOCReader;->isSupportPicture(Lcom/intsig/office/fc/hwpf/usermodel/PictureType;)Z

    .line 80
    .line 81
    .line 82
    move-result v5

    .line 83
    if-eqz v5, :cond_14

    .line 84
    .line 85
    new-instance v4, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 86
    .line 87
    invoke-direct {v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 88
    .line 89
    .line 90
    invoke-virtual {v4, v10}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 91
    .line 92
    .line 93
    iget-object v5, v0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 94
    .line 95
    invoke-interface {v5}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 96
    .line 97
    .line 98
    move-result-object v5

    .line 99
    invoke-virtual {v5}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 100
    .line 101
    .line 102
    move-result-object v5

    .line 103
    iget-object v6, v0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 104
    .line 105
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getTempFilePath(Lcom/intsig/office/system/IControl;)Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v6

    .line 109
    invoke-virtual {v5, v6}, Lcom/intsig/office/common/picture/PictureManage;->getPictureIndex(Ljava/lang/String;)I

    .line 110
    .line 111
    .line 112
    move-result v5

    .line 113
    if-gez v5, :cond_14

    .line 114
    .line 115
    new-instance v5, Lcom/intsig/office/common/picture/Picture;

    .line 116
    .line 117
    invoke-direct {v5}, Lcom/intsig/office/common/picture/Picture;-><init>()V

    .line 118
    .line 119
    .line 120
    iget-object v6, v0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 121
    .line 122
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getTempFilePath(Lcom/intsig/office/system/IControl;)Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v1

    .line 126
    invoke-virtual {v5, v1}, Lcom/intsig/office/common/picture/Picture;->setTempFilePath(Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    invoke-static {v2}, Lcom/intsig/office/fc/hwpf/usermodel/PictureType;->findMatchingType([B)Lcom/intsig/office/fc/hwpf/usermodel/PictureType;

    .line 130
    .line 131
    .line 132
    move-result-object v1

    .line 133
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/PictureType;->getExtension()Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v1

    .line 137
    invoke-virtual {v5, v1}, Lcom/intsig/office/common/picture/Picture;->setPictureType(Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    iget-object v1, v0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 141
    .line 142
    invoke-interface {v1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    invoke-virtual {v1}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 147
    .line 148
    .line 149
    move-result-object v1

    .line 150
    invoke-virtual {v1, v5}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/common/picture/Picture;)I

    .line 151
    .line 152
    .line 153
    move-result v1

    .line 154
    new-instance v2, Lcom/intsig/office/common/bg/TileShader;

    .line 155
    .line 156
    iget-object v5, v0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 157
    .line 158
    invoke-interface {v5}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 159
    .line 160
    .line 161
    move-result-object v5

    .line 162
    invoke-virtual {v5}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 163
    .line 164
    .line 165
    move-result-object v5

    .line 166
    invoke-virtual {v5, v1}, Lcom/intsig/office/common/picture/PictureManage;->getPicture(I)Lcom/intsig/office/common/picture/Picture;

    .line 167
    .line 168
    .line 169
    move-result-object v1

    .line 170
    const/high16 v5, 0x3f800000    # 1.0f

    .line 171
    .line 172
    invoke-direct {v2, v1, v3, v5, v5}, Lcom/intsig/office/common/bg/TileShader;-><init>(Lcom/intsig/office/common/picture/Picture;IFF)V

    .line 173
    .line 174
    .line 175
    invoke-virtual {v4, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setShader(Lcom/intsig/office/common/bg/AShader;)V

    .line 176
    .line 177
    .line 178
    goto/16 :goto_8

    .line 179
    .line 180
    :cond_3
    const/4 v6, 0x3

    .line 181
    if-ne v2, v6, :cond_5

    .line 182
    .line 183
    iget-object v2, v0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 184
    .line 185
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getBackgroundPictureIdx()I

    .line 186
    .line 187
    .line 188
    move-result v3

    .line 189
    invoke-interface {v1, v2, v3}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getPictureData(Lcom/intsig/office/system/IControl;I)[B

    .line 190
    .line 191
    .line 192
    move-result-object v2

    .line 193
    if-eqz v2, :cond_14

    .line 194
    .line 195
    invoke-static {v2}, Lcom/intsig/office/fc/hwpf/usermodel/PictureType;->findMatchingType([B)Lcom/intsig/office/fc/hwpf/usermodel/PictureType;

    .line 196
    .line 197
    .line 198
    move-result-object v3

    .line 199
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/doc/DOCReader;->isSupportPicture(Lcom/intsig/office/fc/hwpf/usermodel/PictureType;)Z

    .line 200
    .line 201
    .line 202
    move-result v3

    .line 203
    if-eqz v3, :cond_14

    .line 204
    .line 205
    new-instance v3, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 206
    .line 207
    invoke-direct {v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 208
    .line 209
    .line 210
    invoke-virtual {v3, v6}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 211
    .line 212
    .line 213
    iget-object v4, v0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 214
    .line 215
    invoke-interface {v4}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 216
    .line 217
    .line 218
    move-result-object v4

    .line 219
    invoke-virtual {v4}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 220
    .line 221
    .line 222
    move-result-object v4

    .line 223
    iget-object v5, v0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 224
    .line 225
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getTempFilePath(Lcom/intsig/office/system/IControl;)Ljava/lang/String;

    .line 226
    .line 227
    .line 228
    move-result-object v5

    .line 229
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/picture/PictureManage;->getPictureIndex(Ljava/lang/String;)I

    .line 230
    .line 231
    .line 232
    move-result v4

    .line 233
    if-gez v4, :cond_4

    .line 234
    .line 235
    new-instance v4, Lcom/intsig/office/common/picture/Picture;

    .line 236
    .line 237
    invoke-direct {v4}, Lcom/intsig/office/common/picture/Picture;-><init>()V

    .line 238
    .line 239
    .line 240
    iget-object v5, v0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 241
    .line 242
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getTempFilePath(Lcom/intsig/office/system/IControl;)Ljava/lang/String;

    .line 243
    .line 244
    .line 245
    move-result-object v1

    .line 246
    invoke-virtual {v4, v1}, Lcom/intsig/office/common/picture/Picture;->setTempFilePath(Ljava/lang/String;)V

    .line 247
    .line 248
    .line 249
    invoke-static {v2}, Lcom/intsig/office/fc/hwpf/usermodel/PictureType;->findMatchingType([B)Lcom/intsig/office/fc/hwpf/usermodel/PictureType;

    .line 250
    .line 251
    .line 252
    move-result-object v1

    .line 253
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/PictureType;->getExtension()Ljava/lang/String;

    .line 254
    .line 255
    .line 256
    move-result-object v1

    .line 257
    invoke-virtual {v4, v1}, Lcom/intsig/office/common/picture/Picture;->setPictureType(Ljava/lang/String;)V

    .line 258
    .line 259
    .line 260
    iget-object v1, v0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 261
    .line 262
    invoke-interface {v1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 263
    .line 264
    .line 265
    move-result-object v1

    .line 266
    invoke-virtual {v1}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 267
    .line 268
    .line 269
    move-result-object v1

    .line 270
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/common/picture/Picture;)I

    .line 271
    .line 272
    .line 273
    move-result v4

    .line 274
    :cond_4
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setPictureIndex(I)V

    .line 275
    .line 276
    .line 277
    move-object v4, v3

    .line 278
    goto/16 :goto_8

    .line 279
    .line 280
    :cond_5
    if-ne v2, v5, :cond_14

    .line 281
    .line 282
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getFillbackColor()Lcom/intsig/office/java/awt/Color;

    .line 283
    .line 284
    .line 285
    move-result-object v1

    .line 286
    if-eqz v1, :cond_14

    .line 287
    .line 288
    new-instance v4, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 289
    .line 290
    invoke-direct {v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 291
    .line 292
    .line 293
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 294
    .line 295
    .line 296
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getFillbackColor()Lcom/intsig/office/java/awt/Color;

    .line 297
    .line 298
    .line 299
    move-result-object v1

    .line 300
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 301
    .line 302
    .line 303
    move-result v1

    .line 304
    invoke-virtual {v4, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 305
    .line 306
    .line 307
    goto/16 :goto_8

    .line 308
    .line 309
    :cond_6
    :goto_0
    new-instance v1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 310
    .line 311
    invoke-direct {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 312
    .line 313
    .line 314
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getFillAngle()I

    .line 315
    .line 316
    .line 317
    move-result v11

    .line 318
    const/16 v12, -0x87

    .line 319
    .line 320
    if-eq v11, v12, :cond_9

    .line 321
    .line 322
    const/16 v12, -0x5a

    .line 323
    .line 324
    if-eq v11, v12, :cond_8

    .line 325
    .line 326
    const/16 v12, -0x2d

    .line 327
    .line 328
    if-eq v11, v12, :cond_7

    .line 329
    .line 330
    if-eqz v11, :cond_8

    .line 331
    .line 332
    goto :goto_1

    .line 333
    :cond_7
    const/16 v11, 0x87

    .line 334
    .line 335
    goto :goto_1

    .line 336
    :cond_8
    add-int/lit8 v11, v11, 0x5a

    .line 337
    .line 338
    goto :goto_1

    .line 339
    :cond_9
    const/16 v11, 0x2d

    .line 340
    .line 341
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getFillFocus()I

    .line 342
    .line 343
    .line 344
    move-result v12

    .line 345
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getForegroundColor()Lcom/intsig/office/java/awt/Color;

    .line 346
    .line 347
    .line 348
    move-result-object v13

    .line 349
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getFillbackColor()Lcom/intsig/office/java/awt/Color;

    .line 350
    .line 351
    .line 352
    move-result-object v14

    .line 353
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->isShaderPreset()Z

    .line 354
    .line 355
    .line 356
    move-result v15

    .line 357
    if-eqz v15, :cond_a

    .line 358
    .line 359
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getShaderColors()[I

    .line 360
    .line 361
    .line 362
    move-result-object v15

    .line 363
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getShaderPositions()[F

    .line 364
    .line 365
    .line 366
    move-result-object v16

    .line 367
    goto :goto_2

    .line 368
    :cond_a
    move-object v15, v4

    .line 369
    move-object/from16 v16, v15

    .line 370
    .line 371
    :goto_2
    if-nez v15, :cond_d

    .line 372
    .line 373
    new-array v15, v10, [I

    .line 374
    .line 375
    const/16 v17, -0x1

    .line 376
    .line 377
    if-nez v13, :cond_b

    .line 378
    .line 379
    const/4 v13, -0x1

    .line 380
    goto :goto_3

    .line 381
    :cond_b
    invoke-virtual {v13}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 382
    .line 383
    .line 384
    move-result v13

    .line 385
    :goto_3
    aput v13, v15, v3

    .line 386
    .line 387
    if-nez v14, :cond_c

    .line 388
    .line 389
    goto :goto_4

    .line 390
    :cond_c
    invoke-virtual {v14}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 391
    .line 392
    .line 393
    move-result v17

    .line 394
    :goto_4
    aput v17, v15, v5

    .line 395
    .line 396
    :cond_d
    if-nez v16, :cond_e

    .line 397
    .line 398
    new-array v3, v10, [F

    .line 399
    .line 400
    fill-array-data v3, :array_0

    .line 401
    .line 402
    .line 403
    goto :goto_5

    .line 404
    :cond_e
    move-object/from16 v3, v16

    .line 405
    .line 406
    :goto_5
    if-ne v2, v9, :cond_f

    .line 407
    .line 408
    new-instance v4, Lcom/intsig/office/common/bg/LinearGradientShader;

    .line 409
    .line 410
    int-to-float v5, v11

    .line 411
    invoke-direct {v4, v5, v15, v3}, Lcom/intsig/office/common/bg/LinearGradientShader;-><init>(F[I[F)V

    .line 412
    .line 413
    .line 414
    goto :goto_6

    .line 415
    :cond_f
    if-eq v2, v8, :cond_10

    .line 416
    .line 417
    if-eq v2, v7, :cond_10

    .line 418
    .line 419
    if-ne v2, v6, :cond_11

    .line 420
    .line 421
    :cond_10
    new-instance v4, Lcom/intsig/office/common/bg/RadialGradientShader;

    .line 422
    .line 423
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getRadialGradientPositionType()I

    .line 424
    .line 425
    .line 426
    move-result v5

    .line 427
    invoke-direct {v4, v5, v15, v3}, Lcom/intsig/office/common/bg/RadialGradientShader;-><init>(I[I[F)V

    .line 428
    .line 429
    .line 430
    :cond_11
    :goto_6
    if-eqz v4, :cond_12

    .line 431
    .line 432
    invoke-virtual {v4, v12}, Lcom/intsig/office/common/bg/Gradient;->setFocus(I)V

    .line 433
    .line 434
    .line 435
    :cond_12
    int-to-byte v2, v2

    .line 436
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 437
    .line 438
    .line 439
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setShader(Lcom/intsig/office/common/bg/AShader;)V

    .line 440
    .line 441
    .line 442
    move-object v4, v1

    .line 443
    goto :goto_8

    .line 444
    :cond_13
    :goto_7
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getForegroundColor()Lcom/intsig/office/java/awt/Color;

    .line 445
    .line 446
    .line 447
    move-result-object v1

    .line 448
    if-eqz v1, :cond_14

    .line 449
    .line 450
    new-instance v4, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 451
    .line 452
    invoke-direct {v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 453
    .line 454
    .line 455
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 456
    .line 457
    .line 458
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getForegroundColor()Lcom/intsig/office/java/awt/Color;

    .line 459
    .line 460
    .line 461
    move-result-object v1

    .line 462
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 463
    .line 464
    .line 465
    move-result v1

    .line 466
    invoke-virtual {v4, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 467
    .line 468
    .line 469
    :cond_14
    :goto_8
    return-object v4

    .line 470
    nop

    .line 471
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private convertShape(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;Lcom/intsig/office/java/awt/Rectangle;FF)Z
    .locals 22

    .line 1
    move-object/from16 v8, p0

    .line 2
    .line 3
    move-object/from16 v9, p2

    .line 4
    .line 5
    move-object/from16 v10, p3

    .line 6
    .line 7
    move-object/from16 v0, p4

    .line 8
    .line 9
    move-object/from16 v1, p5

    .line 10
    .line 11
    move/from16 v11, p6

    .line 12
    .line 13
    move/from16 v12, p7

    .line 14
    .line 15
    const/4 v13, 0x0

    .line 16
    if-nez v1, :cond_0

    .line 17
    .line 18
    return v13

    .line 19
    :cond_0
    instance-of v2, v0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFAutoShape;

    .line 20
    .line 21
    const/4 v7, 0x1

    .line 22
    if-eqz v2, :cond_1b

    .line 23
    .line 24
    move-object v2, v0

    .line 25
    check-cast v2, Lcom/intsig/office/fc/hwpf/usermodel/HWPFAutoShape;

    .line 26
    .line 27
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getShapeType()I

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    invoke-direct {v8, v2, v9, v3}, Lcom/intsig/office/fc/doc/DOCReader;->converFill(Lcom/intsig/office/fc/hwpf/usermodel/HWPFAutoShape;Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;I)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 32
    .line 33
    .line 34
    move-result-object v4

    .line 35
    const/16 v5, 0x14

    .line 36
    .line 37
    if-ne v3, v5, :cond_1

    .line 38
    .line 39
    const/4 v6, 0x1

    .line 40
    goto :goto_0

    .line 41
    :cond_1
    const/4 v6, 0x0

    .line 42
    :goto_0
    invoke-virtual {v0, v6}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getLine(Z)Lcom/intsig/office/common/borders/Line;

    .line 43
    .line 44
    .line 45
    move-result-object v6

    .line 46
    const/16 v11, 0x4b

    .line 47
    .line 48
    if-nez v6, :cond_2

    .line 49
    .line 50
    if-nez v4, :cond_2

    .line 51
    .line 52
    const/16 v12, 0xca

    .line 53
    .line 54
    if-eq v3, v12, :cond_2

    .line 55
    .line 56
    if-ne v3, v11, :cond_20

    .line 57
    .line 58
    :cond_2
    invoke-direct {v8, v10, v1}, Lcom/intsig/office/fc/doc/DOCReader;->processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    if-ne v3, v11, :cond_3

    .line 63
    .line 64
    new-instance v12, Lcom/intsig/office/common/shape/WPPictureShape;

    .line 65
    .line 66
    invoke-direct {v12}, Lcom/intsig/office/common/shape/WPPictureShape;-><init>()V

    .line 67
    .line 68
    .line 69
    goto :goto_1

    .line 70
    :cond_3
    new-instance v12, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 71
    .line 72
    invoke-direct {v12}, Lcom/intsig/office/common/shape/WPAutoShape;-><init>()V

    .line 73
    .line 74
    .line 75
    :goto_1
    invoke-virtual {v12, v3}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 76
    .line 77
    .line 78
    invoke-virtual {v12, v13}, Lcom/intsig/office/common/shape/AutoShape;->setAuotShape07(Z)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getRotation()I

    .line 82
    .line 83
    .line 84
    move-result v16

    .line 85
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(I)I

    .line 86
    .line 87
    .line 88
    move-result v14

    .line 89
    int-to-float v14, v14

    .line 90
    invoke-static {v1, v14}, Lcom/intsig/office/ss/util/ModelUtil;->processRect(Lcom/intsig/office/java/awt/Rectangle;F)Lcom/intsig/office/java/awt/Rectangle;

    .line 91
    .line 92
    .line 93
    move-result-object v14

    .line 94
    invoke-virtual {v12, v14}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v12, v4}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 98
    .line 99
    .line 100
    if-eqz v6, :cond_4

    .line 101
    .line 102
    invoke-virtual {v12, v6}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 103
    .line 104
    .line 105
    :cond_4
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getAdjustmentValue()[Ljava/lang/Float;

    .line 106
    .line 107
    .line 108
    move-result-object v14

    .line 109
    invoke-virtual {v12, v14}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 110
    .line 111
    .line 112
    invoke-direct {v8, v2, v12}, Lcom/intsig/office/fc/doc/DOCReader;->processRotation(Lcom/intsig/office/fc/hwpf/usermodel/HWPFAutoShape;Lcom/intsig/office/common/shape/IShape;)V

    .line 113
    .line 114
    .line 115
    invoke-direct {v8, v2, v12}, Lcom/intsig/office/fc/doc/DOCReader;->processAutoshapePosition(Lcom/intsig/office/fc/hwpf/usermodel/HWPFAutoShape;Lcom/intsig/office/common/shape/WPAutoShape;)V

    .line 116
    .line 117
    .line 118
    if-ne v3, v11, :cond_6

    .line 119
    .line 120
    invoke-direct {v8, v9, v2}, Lcom/intsig/office/fc/doc/DOCReader;->getPictureframeData(Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;)[B

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    if-eqz v0, :cond_17

    .line 125
    .line 126
    invoke-static {v0}, Lcom/intsig/office/fc/hwpf/usermodel/PictureType;->findMatchingType([B)Lcom/intsig/office/fc/hwpf/usermodel/PictureType;

    .line 127
    .line 128
    .line 129
    move-result-object v2

    .line 130
    invoke-direct {v8, v2}, Lcom/intsig/office/fc/doc/DOCReader;->isSupportPicture(Lcom/intsig/office/fc/hwpf/usermodel/PictureType;)Z

    .line 131
    .line 132
    .line 133
    move-result v2

    .line 134
    if-eqz v2, :cond_17

    .line 135
    .line 136
    new-instance v2, Lcom/intsig/office/common/shape/PictureShape;

    .line 137
    .line 138
    invoke-direct {v2}, Lcom/intsig/office/common/shape/PictureShape;-><init>()V

    .line 139
    .line 140
    .line 141
    iget-object v3, v8, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 142
    .line 143
    invoke-interface {v3}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 144
    .line 145
    .line 146
    move-result-object v3

    .line 147
    invoke-virtual {v3}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 148
    .line 149
    .line 150
    move-result-object v3

    .line 151
    iget-object v4, v8, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 152
    .line 153
    invoke-interface {v9, v4}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getTempFilePath(Lcom/intsig/office/system/IControl;)Ljava/lang/String;

    .line 154
    .line 155
    .line 156
    move-result-object v4

    .line 157
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/picture/PictureManage;->getPictureIndex(Ljava/lang/String;)I

    .line 158
    .line 159
    .line 160
    move-result v3

    .line 161
    if-gez v3, :cond_5

    .line 162
    .line 163
    new-instance v3, Lcom/intsig/office/common/picture/Picture;

    .line 164
    .line 165
    invoke-direct {v3}, Lcom/intsig/office/common/picture/Picture;-><init>()V

    .line 166
    .line 167
    .line 168
    iget-object v4, v8, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 169
    .line 170
    invoke-interface {v9, v4}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getTempFilePath(Lcom/intsig/office/system/IControl;)Ljava/lang/String;

    .line 171
    .line 172
    .line 173
    move-result-object v4

    .line 174
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/picture/Picture;->setTempFilePath(Ljava/lang/String;)V

    .line 175
    .line 176
    .line 177
    invoke-static {v0}, Lcom/intsig/office/fc/hwpf/usermodel/PictureType;->findMatchingType([B)Lcom/intsig/office/fc/hwpf/usermodel/PictureType;

    .line 178
    .line 179
    .line 180
    move-result-object v0

    .line 181
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/PictureType;->getExtension()Ljava/lang/String;

    .line 182
    .line 183
    .line 184
    move-result-object v0

    .line 185
    invoke-virtual {v3, v0}, Lcom/intsig/office/common/picture/Picture;->setPictureType(Ljava/lang/String;)V

    .line 186
    .line 187
    .line 188
    iget-object v0, v8, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 189
    .line 190
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 191
    .line 192
    .line 193
    move-result-object v0

    .line 194
    invoke-virtual {v0}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 195
    .line 196
    .line 197
    move-result-object v0

    .line 198
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/common/picture/Picture;)I

    .line 199
    .line 200
    .line 201
    move-result v3

    .line 202
    :cond_5
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/shape/PictureShape;->setPictureIndex(I)V

    .line 203
    .line 204
    .line 205
    invoke-virtual {v2, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 206
    .line 207
    .line 208
    const/16 v0, 0x3e8

    .line 209
    .line 210
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/shape/PictureShape;->setZoomX(S)V

    .line 211
    .line 212
    .line 213
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/shape/PictureShape;->setZoomY(S)V

    .line 214
    .line 215
    .line 216
    invoke-interface/range {p2 .. p2}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getPictureEffectInfor()Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 217
    .line 218
    .line 219
    move-result-object v0

    .line 220
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/shape/PictureShape;->setPictureEffectInfor(Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;)V

    .line 221
    .line 222
    .line 223
    move-object v0, v12

    .line 224
    check-cast v0, Lcom/intsig/office/common/shape/WPPictureShape;

    .line 225
    .line 226
    invoke-virtual {v0, v2}, Lcom/intsig/office/common/shape/WPPictureShape;->setPictureShape(Lcom/intsig/office/common/shape/PictureShape;)V

    .line 227
    .line 228
    .line 229
    goto/16 :goto_9

    .line 230
    .line 231
    :cond_6
    const/16 v11, 0x21

    .line 232
    .line 233
    if-eq v3, v5, :cond_14

    .line 234
    .line 235
    const/16 v5, 0x20

    .line 236
    .line 237
    if-eq v3, v5, :cond_14

    .line 238
    .line 239
    if-eq v3, v11, :cond_14

    .line 240
    .line 241
    const/16 v5, 0x22

    .line 242
    .line 243
    if-eq v3, v5, :cond_14

    .line 244
    .line 245
    const/16 v5, 0x26

    .line 246
    .line 247
    if-ne v3, v5, :cond_7

    .line 248
    .line 249
    goto/16 :goto_8

    .line 250
    .line 251
    :cond_7
    if-eqz v3, :cond_9

    .line 252
    .line 253
    const/16 v5, 0x64

    .line 254
    .line 255
    if-ne v3, v5, :cond_8

    .line 256
    .line 257
    goto :goto_2

    .line 258
    :cond_8
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 259
    .line 260
    .line 261
    move-result-object v0

    .line 262
    iget-object v1, v8, Lcom/intsig/office/fc/doc/DOCReader;->poiDoc:Lcom/intsig/office/fc/hwpf/HWPFDocument;

    .line 263
    .line 264
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/HWPFDocument;->getMainTextboxRange()Lcom/intsig/office/fc/hwpf/usermodel/Range;

    .line 265
    .line 266
    .line 267
    move-result-object v1

    .line 268
    invoke-virtual {v1, v13}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getSection(I)Lcom/intsig/office/fc/hwpf/usermodel/Section;

    .line 269
    .line 270
    .line 271
    move-result-object v1

    .line 272
    invoke-direct {v8, v0, v12, v1}, Lcom/intsig/office/fc/doc/DOCReader;->processTextbox(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/fc/hwpf/usermodel/Section;)V

    .line 273
    .line 274
    .line 275
    goto/16 :goto_9

    .line 276
    .line 277
    :cond_9
    :goto_2
    const/16 v3, 0xe9

    .line 278
    .line 279
    invoke-virtual {v12, v3}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 280
    .line 281
    .line 282
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getStartArrowType()I

    .line 283
    .line 284
    .line 285
    move-result v3

    .line 286
    const/4 v5, 0x5

    .line 287
    if-lez v3, :cond_d

    .line 288
    .line 289
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getStartArrowPath(Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 290
    .line 291
    .line 292
    move-result-object v14

    .line 293
    if-eqz v14, :cond_d

    .line 294
    .line 295
    invoke-virtual {v14}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 296
    .line 297
    .line 298
    move-result-object v16

    .line 299
    if-eqz v16, :cond_d

    .line 300
    .line 301
    invoke-virtual {v14}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 302
    .line 303
    .line 304
    move-result-object v16

    .line 305
    new-instance v11, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 306
    .line 307
    invoke-direct {v11}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 308
    .line 309
    .line 310
    invoke-virtual {v14}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 311
    .line 312
    .line 313
    move-result-object v14

    .line 314
    invoke-virtual {v11, v14}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 315
    .line 316
    .line 317
    invoke-virtual {v11, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 318
    .line 319
    .line 320
    if-eq v3, v5, :cond_c

    .line 321
    .line 322
    if-eqz v6, :cond_a

    .line 323
    .line 324
    invoke-virtual {v6}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 325
    .line 326
    .line 327
    move-result-object v14

    .line 328
    if-nez v14, :cond_b

    .line 329
    .line 330
    :cond_a
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getLineColor()Lcom/intsig/office/java/awt/Color;

    .line 331
    .line 332
    .line 333
    move-result-object v14

    .line 334
    if-eqz v14, :cond_b

    .line 335
    .line 336
    new-instance v14, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 337
    .line 338
    invoke-direct {v14}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 339
    .line 340
    .line 341
    invoke-virtual {v14, v13}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 342
    .line 343
    .line 344
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getLineColor()Lcom/intsig/office/java/awt/Color;

    .line 345
    .line 346
    .line 347
    move-result-object v17

    .line 348
    invoke-virtual/range {v17 .. v17}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 349
    .line 350
    .line 351
    move-result v15

    .line 352
    invoke-virtual {v14, v15}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 353
    .line 354
    .line 355
    invoke-virtual {v11, v14}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 356
    .line 357
    .line 358
    goto :goto_3

    .line 359
    :cond_b
    invoke-virtual {v6}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 360
    .line 361
    .line 362
    move-result-object v14

    .line 363
    invoke-virtual {v11, v14}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 364
    .line 365
    .line 366
    goto :goto_3

    .line 367
    :cond_c
    invoke-virtual {v11, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 368
    .line 369
    .line 370
    :goto_3
    invoke-virtual {v12, v11}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 371
    .line 372
    .line 373
    move-object/from16 v18, v16

    .line 374
    .line 375
    goto :goto_4

    .line 376
    :cond_d
    const/16 v18, 0x0

    .line 377
    .line 378
    :goto_4
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getEndArrowType()I

    .line 379
    .line 380
    .line 381
    move-result v11

    .line 382
    if-lez v11, :cond_11

    .line 383
    .line 384
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getEndArrowPath(Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 385
    .line 386
    .line 387
    move-result-object v14

    .line 388
    if-eqz v14, :cond_11

    .line 389
    .line 390
    invoke-virtual {v14}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 391
    .line 392
    .line 393
    move-result-object v15

    .line 394
    if-eqz v15, :cond_11

    .line 395
    .line 396
    invoke-virtual {v14}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 397
    .line 398
    .line 399
    move-result-object v15

    .line 400
    new-instance v13, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 401
    .line 402
    invoke-direct {v13}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 403
    .line 404
    .line 405
    invoke-virtual {v14}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 406
    .line 407
    .line 408
    move-result-object v14

    .line 409
    invoke-virtual {v13, v14}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 410
    .line 411
    .line 412
    invoke-virtual {v13, v7}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 413
    .line 414
    .line 415
    if-eq v11, v5, :cond_10

    .line 416
    .line 417
    if-eqz v6, :cond_e

    .line 418
    .line 419
    invoke-virtual {v6}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 420
    .line 421
    .line 422
    move-result-object v5

    .line 423
    if-nez v5, :cond_f

    .line 424
    .line 425
    :cond_e
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getLineColor()Lcom/intsig/office/java/awt/Color;

    .line 426
    .line 427
    .line 428
    move-result-object v5

    .line 429
    if-eqz v5, :cond_f

    .line 430
    .line 431
    new-instance v5, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 432
    .line 433
    invoke-direct {v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 434
    .line 435
    .line 436
    const/4 v14, 0x0

    .line 437
    invoke-virtual {v5, v14}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 438
    .line 439
    .line 440
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getLineColor()Lcom/intsig/office/java/awt/Color;

    .line 441
    .line 442
    .line 443
    move-result-object v0

    .line 444
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 445
    .line 446
    .line 447
    move-result v0

    .line 448
    invoke-virtual {v5, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 449
    .line 450
    .line 451
    invoke-virtual {v13, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 452
    .line 453
    .line 454
    goto :goto_5

    .line 455
    :cond_f
    invoke-virtual {v6}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 456
    .line 457
    .line 458
    move-result-object v0

    .line 459
    invoke-virtual {v13, v0}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 460
    .line 461
    .line 462
    goto :goto_5

    .line 463
    :cond_10
    invoke-virtual {v13, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 464
    .line 465
    .line 466
    :goto_5
    invoke-virtual {v12, v13}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 467
    .line 468
    .line 469
    move-object/from16 v20, v15

    .line 470
    .line 471
    goto :goto_6

    .line 472
    :cond_11
    const/16 v20, 0x0

    .line 473
    .line 474
    :goto_6
    int-to-byte v0, v3

    .line 475
    int-to-byte v3, v11

    .line 476
    move-object/from16 v16, v2

    .line 477
    .line 478
    move-object/from16 v17, v1

    .line 479
    .line 480
    move/from16 v19, v0

    .line 481
    .line 482
    move/from16 v21, v3

    .line 483
    .line 484
    invoke-virtual/range {v16 .. v21}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getFreeformPath(Lcom/intsig/office/java/awt/Rectangle;Landroid/graphics/PointF;BLandroid/graphics/PointF;B)[Landroid/graphics/Path;

    .line 485
    .line 486
    .line 487
    move-result-object v0

    .line 488
    const/4 v1, 0x0

    .line 489
    :goto_7
    array-length v2, v0

    .line 490
    if-ge v1, v2, :cond_17

    .line 491
    .line 492
    new-instance v2, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 493
    .line 494
    invoke-direct {v2}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 495
    .line 496
    .line 497
    aget-object v3, v0, v1

    .line 498
    .line 499
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 500
    .line 501
    .line 502
    if-eqz v6, :cond_12

    .line 503
    .line 504
    invoke-virtual {v2, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 505
    .line 506
    .line 507
    :cond_12
    if-eqz v4, :cond_13

    .line 508
    .line 509
    invoke-virtual {v2, v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 510
    .line 511
    .line 512
    :cond_13
    invoke-virtual {v12, v2}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 513
    .line 514
    .line 515
    add-int/lit8 v1, v1, 0x1

    .line 516
    .line 517
    goto :goto_7

    .line 518
    :cond_14
    :goto_8
    invoke-virtual {v12}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 519
    .line 520
    .line 521
    move-result v0

    .line 522
    if-ne v0, v11, :cond_15

    .line 523
    .line 524
    if-nez v14, :cond_15

    .line 525
    .line 526
    new-array v0, v7, [Ljava/lang/Float;

    .line 527
    .line 528
    const/high16 v1, 0x3f800000    # 1.0f

    .line 529
    .line 530
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 531
    .line 532
    .line 533
    move-result-object v1

    .line 534
    const/4 v3, 0x0

    .line 535
    aput-object v1, v0, v3

    .line 536
    .line 537
    invoke-virtual {v12, v0}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 538
    .line 539
    .line 540
    :cond_15
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getStartArrowType()I

    .line 541
    .line 542
    .line 543
    move-result v0

    .line 544
    if-lez v0, :cond_16

    .line 545
    .line 546
    int-to-byte v0, v0

    .line 547
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getStartArrowWidth()I

    .line 548
    .line 549
    .line 550
    move-result v1

    .line 551
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getStartArrowLength()I

    .line 552
    .line 553
    .line 554
    move-result v3

    .line 555
    invoke-virtual {v12, v0, v1, v3}, Lcom/intsig/office/common/shape/LineShape;->createStartArrow(BII)V

    .line 556
    .line 557
    .line 558
    :cond_16
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getEndArrowType()I

    .line 559
    .line 560
    .line 561
    move-result v0

    .line 562
    if-lez v0, :cond_17

    .line 563
    .line 564
    int-to-byte v0, v0

    .line 565
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getEndArrowWidth()I

    .line 566
    .line 567
    .line 568
    move-result v1

    .line 569
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getEndArrowLength()I

    .line 570
    .line 571
    .line 572
    move-result v2

    .line 573
    invoke-virtual {v12, v0, v1, v2}, Lcom/intsig/office/common/shape/LineShape;->createEndArrow(BII)V

    .line 574
    .line 575
    .line 576
    :cond_17
    :goto_9
    if-nez v10, :cond_1a

    .line 577
    .line 578
    invoke-interface/range {p2 .. p2}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getWrap()I

    .line 579
    .line 580
    .line 581
    move-result v0

    .line 582
    const/4 v1, 0x3

    .line 583
    if-ne v0, v1, :cond_19

    .line 584
    .line 585
    invoke-interface/range {p2 .. p2}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->isAnchorLock()Z

    .line 586
    .line 587
    .line 588
    move-result v0

    .line 589
    if-nez v0, :cond_19

    .line 590
    .line 591
    invoke-interface/range {p2 .. p2}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->isBelowText()Z

    .line 592
    .line 593
    .line 594
    move-result v0

    .line 595
    if-eqz v0, :cond_18

    .line 596
    .line 597
    const/4 v0, 0x6

    .line 598
    invoke-virtual {v12, v0}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 599
    .line 600
    .line 601
    goto :goto_a

    .line 602
    :cond_18
    invoke-virtual {v12, v1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 603
    .line 604
    .line 605
    invoke-virtual {v12}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 606
    .line 607
    .line 608
    goto :goto_a

    .line 609
    :cond_19
    const/4 v0, 0x2

    .line 610
    invoke-virtual {v12, v0}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 611
    .line 612
    .line 613
    :goto_a
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 614
    .line 615
    .line 616
    move-result-object v0

    .line 617
    invoke-interface/range {p1 .. p1}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 618
    .line 619
    .line 620
    move-result-object v1

    .line 621
    iget-object v2, v8, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 622
    .line 623
    invoke-interface {v2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 624
    .line 625
    .line 626
    move-result-object v2

    .line 627
    invoke-virtual {v2}, Lcom/intsig/office/system/SysKit;->getWPShapeManage()Lcom/intsig/office/wp/control/WPShapeManage;

    .line 628
    .line 629
    .line 630
    move-result-object v2

    .line 631
    invoke-virtual {v2, v12}, Lcom/intsig/office/wp/control/WPShapeManage;->addShape(Lcom/intsig/office/common/shape/AbstractShape;)I

    .line 632
    .line 633
    .line 634
    move-result v2

    .line 635
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setShapeID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 636
    .line 637
    .line 638
    return v7

    .line 639
    :cond_1a
    invoke-virtual {v10, v12}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 640
    .line 641
    .line 642
    const/4 v0, 0x0

    .line 643
    return v0

    .line 644
    :cond_1b
    instance-of v2, v0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;

    .line 645
    .line 646
    if-eqz v2, :cond_20

    .line 647
    .line 648
    check-cast v0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;

    .line 649
    .line 650
    new-instance v13, Lcom/intsig/office/common/shape/WPGroupShape;

    .line 651
    .line 652
    invoke-direct {v13}, Lcom/intsig/office/common/shape/WPGroupShape;-><init>()V

    .line 653
    .line 654
    .line 655
    if-nez v10, :cond_1c

    .line 656
    .line 657
    new-instance v2, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 658
    .line 659
    invoke-direct {v2}, Lcom/intsig/office/common/shape/WPAutoShape;-><init>()V

    .line 660
    .line 661
    .line 662
    invoke-virtual {v2, v13}, Lcom/intsig/office/common/shape/WPAutoShape;->addGroupShape(Lcom/intsig/office/common/shape/WPGroupShape;)V

    .line 663
    .line 664
    .line 665
    move-object v14, v2

    .line 666
    goto :goto_b

    .line 667
    :cond_1c
    move-object v14, v13

    .line 668
    :goto_b
    invoke-virtual {v0, v1, v11, v12}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;->getShapeAnchorFit(Lcom/intsig/office/java/awt/Rectangle;FF)[F

    .line 669
    .line 670
    .line 671
    move-result-object v15

    .line 672
    invoke-direct {v8, v10, v1}, Lcom/intsig/office/fc/doc/DOCReader;->processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;

    .line 673
    .line 674
    .line 675
    move-result-object v6

    .line 676
    const/4 v1, 0x0

    .line 677
    aget v2, v15, v1

    .line 678
    .line 679
    mul-float v2, v2, v11

    .line 680
    .line 681
    aget v1, v15, v7

    .line 682
    .line 683
    mul-float v1, v1, v12

    .line 684
    .line 685
    invoke-virtual {v0, v2, v1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;->getCoordinates(FF)Lcom/intsig/office/java/awt/Rectangle;

    .line 686
    .line 687
    .line 688
    move-result-object v1

    .line 689
    iget v2, v6, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 690
    .line 691
    iget v3, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 692
    .line 693
    sub-int/2addr v2, v3

    .line 694
    iget v3, v6, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 695
    .line 696
    iget v1, v1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 697
    .line 698
    sub-int/2addr v3, v1

    .line 699
    invoke-virtual {v13, v2, v3}, Lcom/intsig/office/common/shape/GroupShape;->setOffPostion(II)V

    .line 700
    .line 701
    .line 702
    invoke-virtual {v13, v6}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 703
    .line 704
    .line 705
    invoke-virtual {v13, v10}, Lcom/intsig/office/common/shape/AbstractShape;->setParent(Lcom/intsig/office/common/shape/IShape;)V

    .line 706
    .line 707
    .line 708
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;->getGroupRotation()I

    .line 709
    .line 710
    .line 711
    move-result v1

    .line 712
    int-to-float v1, v1

    .line 713
    invoke-virtual {v13, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setRotation(F)V

    .line 714
    .line 715
    .line 716
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;->getFlipHorizontal()Z

    .line 717
    .line 718
    .line 719
    move-result v1

    .line 720
    invoke-virtual {v13, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setFlipHorizontal(Z)V

    .line 721
    .line 722
    .line 723
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;->getFlipVertical()Z

    .line 724
    .line 725
    .line 726
    move-result v1

    .line 727
    invoke-virtual {v13, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setFlipVertical(Z)V

    .line 728
    .line 729
    .line 730
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;->getShapes()[Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;

    .line 731
    .line 732
    .line 733
    move-result-object v5

    .line 734
    if-eqz v5, :cond_1d

    .line 735
    .line 736
    const/4 v4, 0x0

    .line 737
    :goto_c
    array-length v0, v5

    .line 738
    if-ge v4, v0, :cond_1d

    .line 739
    .line 740
    aget-object v3, v5, v4

    .line 741
    .line 742
    const/4 v0, 0x0

    .line 743
    aget v1, v15, v0

    .line 744
    .line 745
    mul-float v1, v1, v11

    .line 746
    .line 747
    aget v2, v15, v7

    .line 748
    .line 749
    mul-float v2, v2, v12

    .line 750
    .line 751
    invoke-virtual {v3, v6, v1, v2}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getAnchor(Lcom/intsig/office/java/awt/Rectangle;FF)Lcom/intsig/office/java/awt/Rectangle;

    .line 752
    .line 753
    .line 754
    move-result-object v16

    .line 755
    aget v1, v15, v0

    .line 756
    .line 757
    mul-float v17, v1, v11

    .line 758
    .line 759
    aget v0, v15, v7

    .line 760
    .line 761
    mul-float v18, v0, v12

    .line 762
    .line 763
    move-object/from16 v0, p0

    .line 764
    .line 765
    move-object/from16 v1, p1

    .line 766
    .line 767
    move-object/from16 v2, p2

    .line 768
    .line 769
    move-object/from16 v19, v3

    .line 770
    .line 771
    move-object v3, v13

    .line 772
    move/from16 v20, v4

    .line 773
    .line 774
    move-object/from16 v4, v19

    .line 775
    .line 776
    move-object/from16 v19, v5

    .line 777
    .line 778
    move-object/from16 v5, v16

    .line 779
    .line 780
    move-object/from16 v16, v6

    .line 781
    .line 782
    move/from16 v6, v17

    .line 783
    .line 784
    const/16 v17, 0x1

    .line 785
    .line 786
    move/from16 v7, v18

    .line 787
    .line 788
    invoke-direct/range {v0 .. v7}, Lcom/intsig/office/fc/doc/DOCReader;->convertShape(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;Lcom/intsig/office/java/awt/Rectangle;FF)Z

    .line 789
    .line 790
    .line 791
    add-int/lit8 v4, v20, 0x1

    .line 792
    .line 793
    move-object/from16 v6, v16

    .line 794
    .line 795
    move-object/from16 v5, v19

    .line 796
    .line 797
    const/4 v7, 0x1

    .line 798
    goto :goto_c

    .line 799
    :cond_1d
    const/16 v17, 0x1

    .line 800
    .line 801
    if-nez v10, :cond_1f

    .line 802
    .line 803
    invoke-interface/range {p2 .. p2}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getWrap()I

    .line 804
    .line 805
    .line 806
    move-result v0

    .line 807
    const/4 v1, 0x3

    .line 808
    if-ne v0, v1, :cond_1e

    .line 809
    .line 810
    invoke-interface/range {p2 .. p2}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->isAnchorLock()Z

    .line 811
    .line 812
    .line 813
    move-result v0

    .line 814
    if-nez v0, :cond_1e

    .line 815
    .line 816
    move-object v0, v14

    .line 817
    check-cast v0, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 818
    .line 819
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 820
    .line 821
    .line 822
    goto :goto_d

    .line 823
    :cond_1e
    move-object v0, v14

    .line 824
    check-cast v0, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 825
    .line 826
    const/4 v1, 0x2

    .line 827
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 828
    .line 829
    .line 830
    :goto_d
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 831
    .line 832
    .line 833
    move-result-object v0

    .line 834
    invoke-interface/range {p1 .. p1}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 835
    .line 836
    .line 837
    move-result-object v1

    .line 838
    iget-object v2, v8, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 839
    .line 840
    invoke-interface {v2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 841
    .line 842
    .line 843
    move-result-object v2

    .line 844
    invoke-virtual {v2}, Lcom/intsig/office/system/SysKit;->getWPShapeManage()Lcom/intsig/office/wp/control/WPShapeManage;

    .line 845
    .line 846
    .line 847
    move-result-object v2

    .line 848
    invoke-virtual {v2, v14}, Lcom/intsig/office/wp/control/WPShapeManage;->addShape(Lcom/intsig/office/common/shape/AbstractShape;)I

    .line 849
    .line 850
    .line 851
    move-result v2

    .line 852
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setShapeID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 853
    .line 854
    .line 855
    goto :goto_e

    .line 856
    :cond_1f
    invoke-virtual {v14, v10}, Lcom/intsig/office/common/shape/AbstractShape;->setParent(Lcom/intsig/office/common/shape/IShape;)V

    .line 857
    .line 858
    .line 859
    invoke-virtual {v10, v14}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 860
    .line 861
    .line 862
    :goto_e
    return v17

    .line 863
    :cond_20
    const/4 v0, 0x0

    .line 864
    return v0
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
.end method

.method private converterColorForIndex(S)I
    .locals 5

    .line 1
    const/16 v0, -0x100

    .line 2
    .line 3
    const/high16 v1, -0x10000

    .line 4
    .line 5
    const v2, -0xff01

    .line 6
    .line 7
    .line 8
    const v3, -0xff0100

    .line 9
    .line 10
    .line 11
    const v4, -0xffff01

    .line 12
    .line 13
    .line 14
    packed-switch p1, :pswitch_data_0

    .line 15
    .line 16
    .line 17
    :pswitch_0
    const/4 p1, -0x1

    .line 18
    return p1

    .line 19
    :pswitch_1
    const p1, -0x333334

    .line 20
    .line 21
    .line 22
    return p1

    .line 23
    :pswitch_2
    const p1, -0x777778

    .line 24
    .line 25
    .line 26
    return p1

    .line 27
    :pswitch_3
    return v0

    .line 28
    :pswitch_4
    return v1

    .line 29
    :pswitch_5
    return v2

    .line 30
    :pswitch_6
    return v3

    .line 31
    :pswitch_7
    const p1, -0xbbbbbc

    .line 32
    .line 33
    .line 34
    return p1

    .line 35
    :pswitch_8
    return v4

    .line 36
    :pswitch_9
    return v0

    .line 37
    :pswitch_a
    return v1

    .line 38
    :pswitch_b
    return v2

    .line 39
    :pswitch_c
    return v3

    .line 40
    :pswitch_d
    const p1, -0xff0001

    .line 41
    .line 42
    .line 43
    return p1

    .line 44
    :pswitch_e
    return v4

    .line 45
    :pswitch_f
    const/high16 p1, -0x1000000

    .line 46
    .line 47
    return p1

    .line 48
    nop

    .line 49
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private converterLineSpace(Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;->getMultiLinespace()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-ne v0, v1, :cond_2

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;->getDyaLine()S

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    int-to-float p1, p1

    .line 13
    const/high16 v0, 0x43700000    # 240.0f

    .line 14
    .line 15
    div-float/2addr p1, v0

    .line 16
    const/high16 v0, 0x3f800000    # 1.0f

    .line 17
    .line 18
    cmpl-float v2, p1, v0

    .line 19
    .line 20
    if-nez v2, :cond_0

    .line 21
    .line 22
    const/4 v1, 0x0

    .line 23
    const/high16 p1, 0x3f800000    # 1.0f

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    float-to-double v2, p1

    .line 27
    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    .line 28
    .line 29
    cmpl-double v0, v2, v4

    .line 30
    .line 31
    if-nez v0, :cond_1

    .line 32
    .line 33
    const/high16 p1, 0x3fc00000    # 1.5f

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    const/4 v1, 0x2

    .line 37
    const/high16 v0, 0x40000000    # 2.0f

    .line 38
    .line 39
    cmpl-float v2, p1, v0

    .line 40
    .line 41
    if-nez v2, :cond_4

    .line 42
    .line 43
    const/high16 p1, 0x40000000    # 2.0f

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;->getDyaLine()S

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    int-to-float p1, p1

    .line 51
    const/4 v0, 0x0

    .line 52
    cmpl-float v0, p1, v0

    .line 53
    .line 54
    if-ltz v0, :cond_3

    .line 55
    .line 56
    const/4 v1, 0x3

    .line 57
    goto :goto_0

    .line 58
    :cond_3
    neg-float p1, p1

    .line 59
    const/4 v1, 0x4

    .line 60
    :cond_4
    :goto_0
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;F)V

    .line 65
    .line 66
    .line 67
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    invoke-virtual {p1, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private converterNumberChar([C)[C
    .locals 4

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    return-object p1

    .line 5
    :cond_0
    const/4 v0, 0x0

    .line 6
    :goto_0
    array-length v1, p1

    .line 7
    if-ge v0, v1, :cond_8

    .line 8
    .line 9
    aget-char v1, p1, v0

    .line 10
    .line 11
    const v2, 0xf06c

    .line 12
    .line 13
    .line 14
    const/16 v3, 0x25cf

    .line 15
    .line 16
    if-ne v1, v2, :cond_1

    .line 17
    .line 18
    aput-char v3, p1, v0

    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_1
    const v2, 0xf06e

    .line 22
    .line 23
    .line 24
    if-ne v1, v2, :cond_2

    .line 25
    .line 26
    const/16 v1, 0x25a0

    .line 27
    .line 28
    aput-char v1, p1, v0

    .line 29
    .line 30
    goto :goto_1

    .line 31
    :cond_2
    const v2, 0xf075

    .line 32
    .line 33
    .line 34
    if-ne v1, v2, :cond_3

    .line 35
    .line 36
    const/16 v1, 0x25c6

    .line 37
    .line 38
    aput-char v1, p1, v0

    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_3
    const v2, 0xf0fc

    .line 42
    .line 43
    .line 44
    if-ne v1, v2, :cond_4

    .line 45
    .line 46
    const/16 v1, 0x221a

    .line 47
    .line 48
    aput-char v1, p1, v0

    .line 49
    .line 50
    goto :goto_1

    .line 51
    :cond_4
    const v2, 0xf0d8

    .line 52
    .line 53
    .line 54
    if-ne v1, v2, :cond_5

    .line 55
    .line 56
    const/16 v1, 0x2605

    .line 57
    .line 58
    aput-char v1, p1, v0

    .line 59
    .line 60
    goto :goto_1

    .line 61
    :cond_5
    const v2, 0xf0b2

    .line 62
    .line 63
    .line 64
    if-ne v1, v2, :cond_6

    .line 65
    .line 66
    const/16 v1, 0x2606

    .line 67
    .line 68
    aput-char v1, p1, v0

    .line 69
    .line 70
    goto :goto_1

    .line 71
    :cond_6
    const v2, 0xf060

    .line 72
    .line 73
    .line 74
    if-lt v1, v2, :cond_7

    .line 75
    .line 76
    aput-char v3, p1, v0

    .line 77
    .line 78
    :cond_7
    :goto_1
    add-int/lit8 v0, v0, 0x1

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_8
    return-object p1
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private converterParaHorAlign(I)B
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eq p1, v0, :cond_1

    .line 3
    .line 4
    const/4 v1, 0x2

    .line 5
    if-eq p1, v1, :cond_0

    .line 6
    .line 7
    const/4 v2, 0x5

    .line 8
    if-eq p1, v2, :cond_1

    .line 9
    .line 10
    const/16 v0, 0x8

    .line 11
    .line 12
    if-eq p1, v0, :cond_0

    .line 13
    .line 14
    const/4 p1, 0x0

    .line 15
    return p1

    .line 16
    :cond_0
    return v1

    .line 17
    :cond_1
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private converterSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 6
    .line 7
    .line 8
    if-gez p2, :cond_0

    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v1, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    add-int/2addr v1, p2

    .line 23
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 24
    .line 25
    .line 26
    :cond_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private getPictureframeData(Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;)[B
    .locals 2

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    const/16 v0, -0xff5

    .line 6
    .line 7
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 8
    .line 9
    .line 10
    move-result-object p2

    .line 11
    check-cast p2, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    if-nez p2, :cond_0

    .line 15
    .line 16
    return-object v0

    .line 17
    :cond_0
    const/16 v1, 0x104

    .line 18
    .line 19
    invoke-virtual {p2, v1}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->lookup(I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 20
    .line 21
    .line 22
    move-result-object p2

    .line 23
    check-cast p2, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 24
    .line 25
    if-nez p2, :cond_1

    .line 26
    .line 27
    return-object v0

    .line 28
    :cond_1
    invoke-virtual {p2}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 29
    .line 30
    .line 31
    move-result p2

    .line 32
    iget-object v0, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 33
    .line 34
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getPictureData(Lcom/intsig/office/system/IControl;I)[B

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    return-object p1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private getTextboxId(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)S
    .locals 2

    .line 1
    const/16 v0, -0xff3

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 8
    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/ddf/EscherTextboxRecord;->getData()[B

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    array-length v0, p1

    .line 18
    const/4 v1, 0x4

    .line 19
    if-ne v0, v1, :cond_0

    .line 20
    .line 21
    const/4 v0, 0x2

    .line 22
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    return p1

    .line 27
    :cond_0
    const/4 p1, -0x1

    .line 28
    return p1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private isPageNumber(Lcom/intsig/office/fc/hwpf/usermodel/Field;Ljava/lang/String;)Z
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eqz p1, :cond_1

    .line 3
    .line 4
    invoke-interface {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Field;->getType()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    const/16 v2, 0x21

    .line 9
    .line 10
    if-eq v1, v2, :cond_0

    .line 11
    .line 12
    invoke-interface {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Field;->getType()I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    const/16 v1, 0x1a

    .line 17
    .line 18
    if-ne p1, v1, :cond_1

    .line 19
    .line 20
    :cond_0
    return v0

    .line 21
    :cond_1
    if-eqz p2, :cond_3

    .line 22
    .line 23
    const-string p1, "NUMPAGES"

    .line 24
    .line 25
    invoke-virtual {p2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    if-nez p1, :cond_2

    .line 30
    .line 31
    const-string p1, "PAGE"

    .line 32
    .line 33
    invoke-virtual {p2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    if-eqz p1, :cond_3

    .line 38
    .line 39
    :cond_2
    return v0

    .line 40
    :cond_3
    const/4 p1, 0x0

    .line 41
    return p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private isSupportPicture(Lcom/intsig/office/fc/hwpf/usermodel/PictureType;)Z
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/PictureType;->getExtension()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const-string v0, "gif"

    .line 6
    .line 7
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    const-string v0, "jpeg"

    .line 14
    .line 15
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-nez v0, :cond_1

    .line 20
    .line 21
    const-string v0, "jpg"

    .line 22
    .line 23
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-nez v0, :cond_1

    .line 28
    .line 29
    const-string v0, "bmp"

    .line 30
    .line 31
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-nez v0, :cond_1

    .line 36
    .line 37
    const-string v0, "png"

    .line 38
    .line 39
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-nez v0, :cond_1

    .line 44
    .line 45
    const-string v0, "wmf"

    .line 46
    .line 47
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    if-nez v0, :cond_1

    .line 52
    .line 53
    const-string v0, "emf"

    .line 54
    .line 55
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    if-eqz p1, :cond_0

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_0
    const/4 p1, 0x0

    .line 63
    goto :goto_1

    .line 64
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 65
    :goto_1
    return p1
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private processAutoshapePosition(Lcom/intsig/office/fc/hwpf/usermodel/HWPFAutoShape;Lcom/intsig/office/common/shape/WPAutoShape;)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getPosition_H()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x7

    .line 6
    const/4 v2, 0x6

    .line 7
    const/4 v3, 0x0

    .line 8
    const/4 v4, 0x5

    .line 9
    const/4 v5, 0x4

    .line 10
    const/4 v6, 0x3

    .line 11
    const/4 v7, 0x1

    .line 12
    const/4 v8, 0x2

    .line 13
    if-eqz v0, :cond_5

    .line 14
    .line 15
    if-eq v0, v7, :cond_4

    .line 16
    .line 17
    if-eq v0, v8, :cond_3

    .line 18
    .line 19
    if-eq v0, v6, :cond_2

    .line 20
    .line 21
    if-eq v0, v5, :cond_1

    .line 22
    .line 23
    if-eq v0, v4, :cond_0

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    invoke-virtual {p2, v1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalAlignment(B)V

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    invoke-virtual {p2, v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalAlignment(B)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_2
    invoke-virtual {p2, v6}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalAlignment(B)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_3
    invoke-virtual {p2, v8}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalAlignment(B)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_4
    invoke-virtual {p2, v7}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalAlignment(B)V

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_5
    invoke-virtual {p2, v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorPositionType(B)V

    .line 47
    .line 48
    .line 49
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getPositionRelTo_H()I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    if-eqz v0, :cond_9

    .line 54
    .line 55
    if-eq v0, v7, :cond_8

    .line 56
    .line 57
    if-eq v0, v8, :cond_7

    .line 58
    .line 59
    if-eq v0, v6, :cond_6

    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_6
    invoke-virtual {p2, v6}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    .line 63
    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_7
    invoke-virtual {p2, v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    .line 67
    .line 68
    .line 69
    goto :goto_1

    .line 70
    :cond_8
    invoke-virtual {p2, v8}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    .line 71
    .line 72
    .line 73
    goto :goto_1

    .line 74
    :cond_9
    invoke-virtual {p2, v7}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    .line 75
    .line 76
    .line 77
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getPosition_V()I

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    if-eqz v0, :cond_f

    .line 82
    .line 83
    if-eq v0, v7, :cond_e

    .line 84
    .line 85
    if-eq v0, v8, :cond_d

    .line 86
    .line 87
    if-eq v0, v6, :cond_c

    .line 88
    .line 89
    if-eq v0, v5, :cond_b

    .line 90
    .line 91
    if-eq v0, v4, :cond_a

    .line 92
    .line 93
    goto :goto_2

    .line 94
    :cond_a
    invoke-virtual {p2, v1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalAlignment(B)V

    .line 95
    .line 96
    .line 97
    goto :goto_2

    .line 98
    :cond_b
    invoke-virtual {p2, v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalAlignment(B)V

    .line 99
    .line 100
    .line 101
    goto :goto_2

    .line 102
    :cond_c
    invoke-virtual {p2, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalAlignment(B)V

    .line 103
    .line 104
    .line 105
    goto :goto_2

    .line 106
    :cond_d
    invoke-virtual {p2, v8}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalAlignment(B)V

    .line 107
    .line 108
    .line 109
    goto :goto_2

    .line 110
    :cond_e
    invoke-virtual {p2, v5}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalAlignment(B)V

    .line 111
    .line 112
    .line 113
    goto :goto_2

    .line 114
    :cond_f
    invoke-virtual {p2, v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerPositionType(B)V

    .line 115
    .line 116
    .line 117
    :goto_2
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getPositionRelTo_V()I

    .line 118
    .line 119
    .line 120
    move-result p1

    .line 121
    if-eqz p1, :cond_13

    .line 122
    .line 123
    if-eq p1, v7, :cond_12

    .line 124
    .line 125
    if-eq p1, v8, :cond_11

    .line 126
    .line 127
    if-eq p1, v6, :cond_10

    .line 128
    .line 129
    goto :goto_3

    .line 130
    :cond_10
    const/16 p1, 0xb

    .line 131
    .line 132
    invoke-virtual {p2, p1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    .line 133
    .line 134
    .line 135
    goto :goto_3

    .line 136
    :cond_11
    const/16 p1, 0xa

    .line 137
    .line 138
    invoke-virtual {p2, p1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    .line 139
    .line 140
    .line 141
    goto :goto_3

    .line 142
    :cond_12
    invoke-virtual {p2, v8}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    .line 143
    .line 144
    .line 145
    goto :goto_3

    .line 146
    :cond_13
    invoke-virtual {p2, v7}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    .line 147
    .line 148
    .line 149
    :goto_3
    return-void
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processBookmark()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->poiDoc:Lcom/intsig/office/fc/hwpf/HWPFDocument;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/HWPFDocument;->getBookmarks()Lcom/intsig/office/fc/hwpf/usermodel/Bookmarks;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    :goto_0
    invoke-interface {v0}, Lcom/intsig/office/fc/hwpf/usermodel/Bookmarks;->getBookmarksCount()I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    if-ge v1, v2, :cond_0

    .line 15
    .line 16
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/hwpf/usermodel/Bookmarks;->getBookmark(I)Lcom/intsig/office/fc/hwpf/usermodel/POIBookmark;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    new-instance v9, Lcom/intsig/office/common/bookmark/Bookmark;

    .line 21
    .line 22
    invoke-interface {v2}, Lcom/intsig/office/fc/hwpf/usermodel/POIBookmark;->getName()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v4

    .line 26
    invoke-interface {v2}, Lcom/intsig/office/fc/hwpf/usermodel/POIBookmark;->getStart()I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    int-to-long v5, v3

    .line 31
    invoke-interface {v2}, Lcom/intsig/office/fc/hwpf/usermodel/POIBookmark;->getEnd()I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    int-to-long v7, v2

    .line 36
    move-object v3, v9

    .line 37
    invoke-direct/range {v3 .. v8}, Lcom/intsig/office/common/bookmark/Bookmark;-><init>(Ljava/lang/String;JJ)V

    .line 38
    .line 39
    .line 40
    iget-object v2, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 41
    .line 42
    invoke-interface {v2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    invoke-virtual {v2}, Lcom/intsig/office/system/SysKit;->getBookmarkManage()Lcom/intsig/office/common/bookmark/BookmarkManage;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    invoke-virtual {v2, v9}, Lcom/intsig/office/common/bookmark/BookmarkManage;->addBookmark(Lcom/intsig/office/common/bookmark/Bookmark;)V

    .line 51
    .line 52
    .line 53
    iget-object v2, p0, Lcom/intsig/office/fc/doc/DOCReader;->bms:Ljava/util/List;

    .line 54
    .line 55
    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    .line 57
    .line 58
    add-int/lit8 v1, v1, 0x1

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_0
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private processBulletNumber()V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->poiDoc:Lcom/intsig/office/fc/hwpf/HWPFDocument;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->getListTables()Lcom/intsig/office/fc/hwpf/model/ListTables;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/ListTables;->getOverrideCount()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    const/4 v2, 0x0

    .line 15
    const/4 v3, 0x0

    .line 16
    :goto_0
    if-ge v3, v1, :cond_3

    .line 17
    .line 18
    new-instance v4, Lcom/intsig/office/common/bulletnumber/ListData;

    .line 19
    .line 20
    invoke-direct {v4}, Lcom/intsig/office/common/bulletnumber/ListData;-><init>()V

    .line 21
    .line 22
    .line 23
    add-int/lit8 v3, v3, 0x1

    .line 24
    .line 25
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hwpf/model/ListTables;->getOverride(I)Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;

    .line 26
    .line 27
    .line 28
    move-result-object v5

    .line 29
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/model/types/LFOAbstractType;->getLsid()I

    .line 30
    .line 31
    .line 32
    move-result v5

    .line 33
    invoke-virtual {v0, v5}, Lcom/intsig/office/fc/hwpf/model/ListTables;->getListData(I)Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 34
    .line 35
    .line 36
    move-result-object v5

    .line 37
    if-nez v5, :cond_1

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/model/POIListData;->getLsid()I

    .line 41
    .line 42
    .line 43
    move-result v6

    .line 44
    invoke-virtual {v4, v6}, Lcom/intsig/office/common/bulletnumber/ListData;->setListID(I)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/model/POIListData;->getLevels()[Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 48
    .line 49
    .line 50
    move-result-object v5

    .line 51
    array-length v6, v5

    .line 52
    new-array v7, v6, [Lcom/intsig/office/common/bulletnumber/ListLevel;

    .line 53
    .line 54
    const/4 v8, 0x0

    .line 55
    :goto_1
    if-ge v8, v6, :cond_2

    .line 56
    .line 57
    new-instance v9, Lcom/intsig/office/common/bulletnumber/ListLevel;

    .line 58
    .line 59
    invoke-direct {v9}, Lcom/intsig/office/common/bulletnumber/ListLevel;-><init>()V

    .line 60
    .line 61
    .line 62
    aput-object v9, v7, v8

    .line 63
    .line 64
    aget-object v10, v5, v8

    .line 65
    .line 66
    invoke-direct {p0, v10, v9}, Lcom/intsig/office/fc/doc/DOCReader;->processListLevel(Lcom/intsig/office/fc/hwpf/model/POIListLevel;Lcom/intsig/office/common/bulletnumber/ListLevel;)V

    .line 67
    .line 68
    .line 69
    add-int/lit8 v8, v8, 0x1

    .line 70
    .line 71
    goto :goto_1

    .line 72
    :cond_2
    invoke-virtual {v4, v7}, Lcom/intsig/office/common/bulletnumber/ListData;->setLevels([Lcom/intsig/office/common/bulletnumber/ListLevel;)V

    .line 73
    .line 74
    .line 75
    int-to-byte v5, v6

    .line 76
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/bulletnumber/ListData;->setSimpleList(B)V

    .line 77
    .line 78
    .line 79
    iget-object v5, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 80
    .line 81
    invoke-interface {v5}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 82
    .line 83
    .line 84
    move-result-object v5

    .line 85
    invoke-virtual {v5}, Lcom/intsig/office/system/SysKit;->getListManage()Lcom/intsig/office/common/bulletnumber/ListManage;

    .line 86
    .line 87
    .line 88
    move-result-object v5

    .line 89
    invoke-virtual {v4}, Lcom/intsig/office/common/bulletnumber/ListData;->getListID()I

    .line 90
    .line 91
    .line 92
    move-result v6

    .line 93
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 94
    .line 95
    .line 96
    move-result-object v6

    .line 97
    invoke-virtual {v5, v6, v4}, Lcom/intsig/office/common/bulletnumber/ListManage;->putListData(Ljava/lang/Integer;Lcom/intsig/office/common/bulletnumber/ListData;)I

    .line 98
    .line 99
    .line 100
    goto :goto_0

    .line 101
    :cond_3
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private processCellAttribute(Lcom/intsig/office/fc/hwpf/usermodel/TableCell;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableCell;->isFirstMerged()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableHorFirstMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 13
    .line 14
    .line 15
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableCell;->isMerged()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v0, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableHorMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 26
    .line 27
    .line 28
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableCell;->isFirstVerticallyMerged()Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-eqz v0, :cond_2

    .line 33
    .line 34
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-virtual {v0, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableVerFirstMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 39
    .line 40
    .line 41
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableCell;->isVerticallyMerged()Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-eqz v0, :cond_3

    .line 46
    .line 47
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-virtual {v0, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableVerMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 52
    .line 53
    .line 54
    :cond_3
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableCell;->getVertAlign()B

    .line 59
    .line 60
    .line 61
    move-result v1

    .line 62
    invoke-virtual {v0, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableCellVerAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 63
    .line 64
    .line 65
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableCell;->getWidth()I

    .line 70
    .line 71
    .line 72
    move-result p1

    .line 73
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableCellWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 74
    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processDoc()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getActivity()Landroid/app/Activity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCReader;->uri:Landroid/net/Uri;

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/office/ss/util/StreamUtils;->getInputStream(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/InputStream;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    new-instance v1, Lcom/intsig/office/fc/hwpf/HWPFDocument;

    .line 14
    .line 15
    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hwpf/HWPFDocument;-><init>(Ljava/io/InputStream;)V

    .line 16
    .line 17
    .line 18
    iput-object v1, p0, Lcom/intsig/office/fc/doc/DOCReader;->poiDoc:Lcom/intsig/office/fc/hwpf/HWPFDocument;

    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/office/fc/doc/DOCReader;->processBulletNumber()V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/office/fc/doc/DOCReader;->processBookmark()V

    .line 24
    .line 25
    .line 26
    const-wide/16 v0, 0x0

    .line 27
    .line 28
    iput-wide v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 29
    .line 30
    iput-wide v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->docRealOffset:J

    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->poiDoc:Lcom/intsig/office/fc/hwpf/HWPFDocument;

    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/HWPFDocument;->getRange()Lcom/intsig/office/fc/hwpf/usermodel/Range;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->numSections()I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    const/4 v2, 0x0

    .line 43
    const/4 v3, 0x0

    .line 44
    :goto_0
    if-ge v3, v1, :cond_1

    .line 45
    .line 46
    iget-boolean v4, p0, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 47
    .line 48
    if-nez v4, :cond_1

    .line 49
    .line 50
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getSection(I)Lcom/intsig/office/fc/hwpf/usermodel/Section;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    invoke-direct {p0, v4}, Lcom/intsig/office/fc/doc/DOCReader;->processSection(Lcom/intsig/office/fc/hwpf/usermodel/Section;)V

    .line 55
    .line 56
    .line 57
    iget-boolean v4, p0, Lcom/intsig/office/fc/doc/DOCReader;->isBreakChar:Z

    .line 58
    .line 59
    if-eqz v4, :cond_0

    .line 60
    .line 61
    iget-object v4, p0, Lcom/intsig/office/fc/doc/DOCReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 62
    .line 63
    iget-wide v5, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 64
    .line 65
    const-wide/16 v7, 0x1

    .line 66
    .line 67
    sub-long/2addr v5, v7

    .line 68
    invoke-virtual {v4, v5, v6}, Lcom/intsig/office/simpletext/model/STDocument;->getLeaf(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 69
    .line 70
    .line 71
    move-result-object v4

    .line 72
    if-eqz v4, :cond_0

    .line 73
    .line 74
    instance-of v5, v4, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 75
    .line 76
    if-eqz v5, :cond_0

    .line 77
    .line 78
    iget-object v5, p0, Lcom/intsig/office/fc/doc/DOCReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 79
    .line 80
    invoke-interface {v4, v5}, Lcom/intsig/office/simpletext/model/IElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v5

    .line 84
    if-eqz v5, :cond_0

    .line 85
    .line 86
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 87
    .line 88
    .line 89
    move-result v6

    .line 90
    const/4 v7, 0x1

    .line 91
    if-ne v6, v7, :cond_0

    .line 92
    .line 93
    invoke-virtual {v5, v2}, Ljava/lang/String;->charAt(I)C

    .line 94
    .line 95
    .line 96
    move-result v5

    .line 97
    const/16 v6, 0xc

    .line 98
    .line 99
    if-ne v5, v6, :cond_0

    .line 100
    .line 101
    check-cast v4, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 102
    .line 103
    const/16 v5, 0xa

    .line 104
    .line 105
    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v5

    .line 109
    invoke-virtual {v4, v5}, Lcom/intsig/office/simpletext/model/LeafElement;->setText(Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 113
    .line 114
    goto :goto_0

    .line 115
    :cond_1
    invoke-direct {p0}, Lcom/intsig/office/fc/doc/DOCReader;->processHeaderFooter()V

    .line 116
    .line 117
    .line 118
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget v0, p2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/GroupShape;->getOffX()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    add-int/2addr v0, v1

    .line 10
    iput v0, p2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 11
    .line 12
    iget v0, p2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/GroupShape;->getOffY()I

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    add-int/2addr v0, p1

    .line 19
    iput v0, p2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 20
    .line 21
    :cond_0
    return-object p2
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private processHeaderFooter()V
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/HeaderStories;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCReader;->poiDoc:Lcom/intsig/office/fc/hwpf/HWPFDocument;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hwpf/usermodel/HeaderStories;-><init>(Lcom/intsig/office/fc/hwpf/HWPFDocument;)V

    .line 6
    .line 7
    .line 8
    const-wide/high16 v1, 0x1000000000000000L

    .line 9
    .line 10
    iput-wide v1, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 11
    .line 12
    iput-wide v1, p0, Lcom/intsig/office/fc/doc/DOCReader;->docRealOffset:J

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/HeaderStories;->getOddHeaderSubrange()Lcom/intsig/office/fc/hwpf/usermodel/Range;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    const/4 v2, 0x1

    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    const/4 v3, 0x5

    .line 22
    invoke-direct {p0, v1, v3, v2}, Lcom/intsig/office/fc/doc/DOCReader;->processHeaderFooterPara(Lcom/intsig/office/fc/hwpf/usermodel/Range;SB)V

    .line 23
    .line 24
    .line 25
    :cond_0
    const-wide/high16 v3, 0x2000000000000000L

    .line 26
    .line 27
    iput-wide v3, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 28
    .line 29
    iput-wide v3, p0, Lcom/intsig/office/fc/doc/DOCReader;->docRealOffset:J

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/HeaderStories;->getOddFooterSubrange()Lcom/intsig/office/fc/hwpf/usermodel/Range;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    const/4 v1, 0x6

    .line 38
    invoke-direct {p0, v0, v1, v2}, Lcom/intsig/office/fc/doc/DOCReader;->processHeaderFooterPara(Lcom/intsig/office/fc/hwpf/usermodel/Range;SB)V

    .line 39
    .line 40
    .line 41
    :cond_1
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private processHeaderFooterPara(Lcom/intsig/office/fc/hwpf/usermodel/Range;SB)V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/wp/model/HFElement;

    .line 2
    .line 3
    invoke-direct {v0, p2, p3}, Lcom/intsig/office/wp/model/HFElement;-><init>(SB)V

    .line 4
    .line 5
    .line 6
    iget-wide p2, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 7
    .line 8
    invoke-interface {v0, p2, p3}, Lcom/intsig/office/simpletext/model/IElement;->setStartOffset(J)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->numParagraphs()I

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    const/4 p3, 0x0

    .line 16
    :goto_0
    if-ge p3, p2, :cond_1

    .line 17
    .line 18
    iget-boolean v1, p0, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 19
    .line 20
    if-nez v1, :cond_1

    .line 21
    .line 22
    invoke-virtual {p1, p3}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getParagraph(I)Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->isInTable()Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-eqz v2, :cond_0

    .line 31
    .line 32
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getTable(Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;)Lcom/intsig/office/fc/hwpf/usermodel/Table;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/doc/DOCReader;->processTable(Lcom/intsig/office/fc/hwpf/usermodel/Table;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->numParagraphs()I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    add-int/lit8 v1, v1, -0x1

    .line 44
    .line 45
    add-int/2addr p3, v1

    .line 46
    goto :goto_1

    .line 47
    :cond_0
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/doc/DOCReader;->processParagraph(Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;)V

    .line 48
    .line 49
    .line 50
    :goto_1
    add-int/lit8 p3, p3, 0x1

    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_1
    iget-wide p1, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 54
    .line 55
    invoke-interface {v0, p1, p2}, Lcom/intsig/office/simpletext/model/IElement;->setEndOffset(J)V

    .line 56
    .line 57
    .line 58
    iget-object p1, p0, Lcom/intsig/office/fc/doc/DOCReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 59
    .line 60
    iget-wide p2, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 61
    .line 62
    invoke-virtual {p1, v0, p2, p3}, Lcom/intsig/office/wp/model/WPDocument;->appendElement(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 63
    .line 64
    .line 65
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private processListLevel(Lcom/intsig/office/fc/hwpf/model/POIListLevel;Lcom/intsig/office/common/bulletnumber/ListLevel;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->getStartAt()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p2, v0}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setStartAt(I)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->getAlignment()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    int-to-byte v0, v0

    .line 13
    invoke-virtual {p2, v0}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setAlign(B)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->getTypeOfCharFollowingTheNumber()B

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    invoke-virtual {p2, v0}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setFollowChar(B)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->getNumberFormat()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    invoke-virtual {p2, v0}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setNumberFormat(I)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->getNumberChar()[C

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/doc/DOCReader;->converterNumberChar([C)[C

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-virtual {p2, v0}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setNumberText([C)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->getSpecialIndnet()I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    invoke-virtual {p2, v0}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setSpecialIndent(I)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->getTextIndent()I

    .line 49
    .line 50
    .line 51
    move-result p1

    .line 52
    invoke-virtual {p2, p1}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setTextIndent(I)V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processParagraph(Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;)V
    .locals 24

    .line 1
    move-object/from16 v7, p0

    .line 2
    .line 3
    new-instance v8, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 4
    .line 5
    invoke-direct {v8}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 9
    .line 10
    .line 11
    move-result-object v9

    .line 12
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getSpacingBefore()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    invoke-virtual {v0, v9, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 21
    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getSpacingAfter()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    invoke-virtual {v0, v9, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 32
    .line 33
    .line 34
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getIndentFromLeft()I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    invoke-virtual {v0, v9, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 43
    .line 44
    .line 45
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getIndentFromRight()I

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    invoke-virtual {v0, v9, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 54
    .line 55
    .line 56
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getJustification()I

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    invoke-direct {v7, v1}, Lcom/intsig/office/fc/doc/DOCReader;->converterParaHorAlign(I)B

    .line 65
    .line 66
    .line 67
    move-result v1

    .line 68
    invoke-virtual {v0, v9, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 69
    .line 70
    .line 71
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getFontAlignment()I

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    invoke-virtual {v0, v9, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 80
    .line 81
    .line 82
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getFirstLineIndent()I

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    invoke-direct {v7, v9, v0}, Lcom/intsig/office/fc/doc/DOCReader;->converterSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 87
    .line 88
    .line 89
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getLineSpacing()Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    invoke-direct {v7, v0, v9}, Lcom/intsig/office/fc/doc/DOCReader;->converterLineSpace(Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 94
    .line 95
    .line 96
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getIlfo()I

    .line 97
    .line 98
    .line 99
    move-result v0

    .line 100
    if-lez v0, :cond_1

    .line 101
    .line 102
    iget-object v0, v7, Lcom/intsig/office/fc/doc/DOCReader;->poiDoc:Lcom/intsig/office/fc/hwpf/HWPFDocument;

    .line 103
    .line 104
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->getListTables()Lcom/intsig/office/fc/hwpf/model/ListTables;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    if-eqz v0, :cond_1

    .line 109
    .line 110
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getIlfo()I

    .line 111
    .line 112
    .line 113
    move-result v1

    .line 114
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/ListTables;->getOverride(I)Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    if-eqz v0, :cond_0

    .line 119
    .line 120
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 121
    .line 122
    .line 123
    move-result-object v1

    .line 124
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/LFOAbstractType;->getLsid()I

    .line 125
    .line 126
    .line 127
    move-result v0

    .line 128
    invoke-virtual {v1, v9, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaListID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 129
    .line 130
    .line 131
    :cond_0
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 132
    .line 133
    .line 134
    move-result-object v0

    .line 135
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getIlvl()I

    .line 136
    .line 137
    .line 138
    move-result v1

    .line 139
    invoke-virtual {v0, v9, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaListLevel(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 140
    .line 141
    .line 142
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->isInTable()Z

    .line 143
    .line 144
    .line 145
    move-result v0

    .line 146
    if-eqz v0, :cond_2

    .line 147
    .line 148
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 149
    .line 150
    .line 151
    move-result-object v0

    .line 152
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getTableLevel()I

    .line 153
    .line 154
    .line 155
    move-result v1

    .line 156
    invoke-virtual {v0, v9, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLevel(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 157
    .line 158
    .line 159
    :cond_2
    iget-wide v0, v7, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 160
    .line 161
    invoke-virtual {v8, v0, v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 162
    .line 163
    .line 164
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->numCharacterRuns()I

    .line 165
    .line 166
    .line 167
    move-result v10

    .line 168
    iget-wide v11, v7, Lcom/intsig/office/fc/doc/DOCReader;->docRealOffset:J

    .line 169
    .line 170
    const-string v13, ""

    .line 171
    .line 172
    move-object v4, v13

    .line 173
    move-object v5, v4

    .line 174
    const/4 v1, 0x0

    .line 175
    const/4 v3, 0x0

    .line 176
    const/4 v6, 0x0

    .line 177
    const/16 v16, 0x0

    .line 178
    .line 179
    const/16 v17, 0x0

    .line 180
    .line 181
    :goto_0
    if-ge v3, v10, :cond_18

    .line 182
    .line 183
    iget-boolean v0, v7, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 184
    .line 185
    if-nez v0, :cond_18

    .line 186
    .line 187
    move-object/from16 v2, p1

    .line 188
    .line 189
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getCharacterRun(I)Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;

    .line 190
    .line 191
    .line 192
    move-result-object v18

    .line 193
    invoke-virtual/range {v18 .. v18}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    .line 194
    .line 195
    .line 196
    move-result-object v0

    .line 197
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 198
    .line 199
    .line 200
    move-result v19

    .line 201
    if-eqz v19, :cond_15

    .line 202
    .line 203
    invoke-virtual/range {v18 .. v18}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->isMarkedDeleted()Z

    .line 204
    .line 205
    .line 206
    move-result v19

    .line 207
    if-eqz v19, :cond_3

    .line 208
    .line 209
    goto/16 :goto_b

    .line 210
    .line 211
    :cond_3
    iget-wide v14, v7, Lcom/intsig/office/fc/doc/DOCReader;->docRealOffset:J

    .line 212
    .line 213
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 214
    .line 215
    .line 216
    move-result v2

    .line 217
    move/from16 v20, v3

    .line 218
    .line 219
    int-to-long v2, v2

    .line 220
    add-long/2addr v14, v2

    .line 221
    iput-wide v14, v7, Lcom/intsig/office/fc/doc/DOCReader;->docRealOffset:J

    .line 222
    .line 223
    const/4 v2, 0x0

    .line 224
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    .line 225
    .line 226
    .line 227
    move-result v3

    .line 228
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 229
    .line 230
    .line 231
    move-result v2

    .line 232
    const/4 v14, 0x1

    .line 233
    sub-int/2addr v2, v14

    .line 234
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    .line 235
    .line 236
    .line 237
    move-result v2

    .line 238
    const/16 v15, 0x9

    .line 239
    .line 240
    if-ne v3, v15, :cond_4

    .line 241
    .line 242
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 243
    .line 244
    .line 245
    move-result v15

    .line 246
    if-eq v15, v14, :cond_16

    .line 247
    .line 248
    :cond_4
    const/4 v15, 0x5

    .line 249
    if-ne v3, v15, :cond_5

    .line 250
    .line 251
    goto/16 :goto_c

    .line 252
    .line 253
    :cond_5
    const/16 v15, 0x13

    .line 254
    .line 255
    const/16 v14, 0x15

    .line 256
    .line 257
    if-eq v3, v15, :cond_11

    .line 258
    .line 259
    if-ne v2, v15, :cond_6

    .line 260
    .line 261
    goto/16 :goto_8

    .line 262
    .line 263
    :cond_6
    const/16 v15, 0x14

    .line 264
    .line 265
    if-eq v3, v15, :cond_10

    .line 266
    .line 267
    if-ne v2, v15, :cond_7

    .line 268
    .line 269
    goto/16 :goto_7

    .line 270
    .line 271
    :cond_7
    if-eq v3, v14, :cond_b

    .line 272
    .line 273
    if-ne v2, v14, :cond_8

    .line 274
    .line 275
    goto :goto_2

    .line 276
    :cond_8
    if-eqz v16, :cond_9

    .line 277
    .line 278
    new-instance v0, Ljava/lang/StringBuilder;

    .line 279
    .line 280
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 281
    .line 282
    .line 283
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    .line 285
    .line 286
    invoke-virtual/range {v18 .. v18}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    .line 287
    .line 288
    .line 289
    move-result-object v1

    .line 290
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    .line 292
    .line 293
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 294
    .line 295
    .line 296
    move-result-object v5

    .line 297
    :goto_1
    const/4 v0, 0x0

    .line 298
    goto/16 :goto_6

    .line 299
    .line 300
    :cond_9
    if-eqz v17, :cond_a

    .line 301
    .line 302
    invoke-direct {v7, v6, v5}, Lcom/intsig/office/fc/doc/DOCReader;->isPageNumber(Lcom/intsig/office/fc/hwpf/usermodel/Field;Ljava/lang/String;)Z

    .line 303
    .line 304
    .line 305
    move-result v0

    .line 306
    if-eqz v0, :cond_a

    .line 307
    .line 308
    new-instance v0, Ljava/lang/StringBuilder;

    .line 309
    .line 310
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 311
    .line 312
    .line 313
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    .line 315
    .line 316
    invoke-virtual/range {v18 .. v18}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    .line 317
    .line 318
    .line 319
    move-result-object v1

    .line 320
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    .line 322
    .line 323
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 324
    .line 325
    .line 326
    move-result-object v4

    .line 327
    goto :goto_1

    .line 328
    :cond_a
    const/4 v14, 0x0

    .line 329
    const/4 v15, 0x0

    .line 330
    move-object/from16 v0, p0

    .line 331
    .line 332
    move-object/from16 v1, v18

    .line 333
    .line 334
    move-object/from16 v2, p1

    .line 335
    .line 336
    move-object v3, v6

    .line 337
    move-object/from16 v21, v4

    .line 338
    .line 339
    move-object v4, v8

    .line 340
    move-object/from16 v22, v5

    .line 341
    .line 342
    move-object v5, v14

    .line 343
    move-object v14, v6

    .line 344
    move-object v6, v15

    .line 345
    invoke-direct/range {v0 .. v6}, Lcom/intsig/office/fc/doc/DOCReader;->processRun(Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;Lcom/intsig/office/fc/hwpf/usermodel/Range;Lcom/intsig/office/fc/hwpf/usermodel/Field;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    .line 347
    .line 348
    move-object v4, v14

    .line 349
    move-object/from16 v6, v21

    .line 350
    .line 351
    move-object/from16 v1, v22

    .line 352
    .line 353
    const/4 v0, 0x0

    .line 354
    const/16 v19, 0x0

    .line 355
    .line 356
    goto/16 :goto_d

    .line 357
    .line 358
    :cond_b
    :goto_2
    move-object/from16 v21, v4

    .line 359
    .line 360
    move-object/from16 v22, v5

    .line 361
    .line 362
    move-object v4, v6

    .line 363
    move-object/from16 v6, v21

    .line 364
    .line 365
    if-eqz v1, :cond_e

    .line 366
    .line 367
    if-eqz v6, :cond_e

    .line 368
    .line 369
    if-eqz v4, :cond_e

    .line 370
    .line 371
    invoke-interface {v4}, Lcom/intsig/office/fc/hwpf/usermodel/Field;->getType()I

    .line 372
    .line 373
    .line 374
    move-result v3

    .line 375
    const/16 v5, 0x3a

    .line 376
    .line 377
    if-ne v3, v5, :cond_e

    .line 378
    .line 379
    const-string v3, "EQ"

    .line 380
    .line 381
    invoke-virtual {v6, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 382
    .line 383
    .line 384
    move-result v3

    .line 385
    if-ltz v3, :cond_c

    .line 386
    .line 387
    const-string v3, "jc"

    .line 388
    .line 389
    invoke-virtual {v6, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 390
    .line 391
    .line 392
    move-result v3

    .line 393
    if-ltz v3, :cond_c

    .line 394
    .line 395
    move-object/from16 v0, p0

    .line 396
    .line 397
    move-object/from16 v2, p1

    .line 398
    .line 399
    move-object v3, v4

    .line 400
    move-object v4, v8

    .line 401
    move-object/from16 v5, v22

    .line 402
    .line 403
    invoke-direct/range {v0 .. v6}, Lcom/intsig/office/fc/doc/DOCReader;->processRun(Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;Lcom/intsig/office/fc/hwpf/usermodel/Range;Lcom/intsig/office/fc/hwpf/usermodel/Field;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    .line 405
    .line 406
    const/4 v0, 0x0

    .line 407
    const/4 v14, 0x0

    .line 408
    goto :goto_5

    .line 409
    :cond_c
    if-ne v2, v14, :cond_d

    .line 410
    .line 411
    new-instance v1, Ljava/lang/StringBuilder;

    .line 412
    .line 413
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 414
    .line 415
    .line 416
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417
    .line 418
    .line 419
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 420
    .line 421
    .line 422
    move-result v2

    .line 423
    const/4 v5, 0x1

    .line 424
    sub-int/2addr v2, v5

    .line 425
    const/4 v14, 0x0

    .line 426
    invoke-virtual {v0, v14, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 427
    .line 428
    .line 429
    move-result-object v0

    .line 430
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 431
    .line 432
    .line 433
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 434
    .line 435
    .line 436
    move-result-object v0

    .line 437
    move-object v6, v0

    .line 438
    goto :goto_3

    .line 439
    :cond_d
    const/4 v14, 0x0

    .line 440
    :goto_3
    move-object/from16 v0, p0

    .line 441
    .line 442
    move-object/from16 v1, v18

    .line 443
    .line 444
    move-object/from16 v2, p1

    .line 445
    .line 446
    move-object v3, v4

    .line 447
    move-object v4, v8

    .line 448
    move-object/from16 v5, v22

    .line 449
    .line 450
    invoke-direct/range {v0 .. v6}, Lcom/intsig/office/fc/doc/DOCReader;->processRun(Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;Lcom/intsig/office/fc/hwpf/usermodel/Range;Lcom/intsig/office/fc/hwpf/usermodel/Field;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    .line 452
    .line 453
    goto :goto_4

    .line 454
    :cond_e
    const/4 v14, 0x0

    .line 455
    move-object/from16 v15, v22

    .line 456
    .line 457
    invoke-direct {v7, v4, v15}, Lcom/intsig/office/fc/doc/DOCReader;->isPageNumber(Lcom/intsig/office/fc/hwpf/usermodel/Field;Ljava/lang/String;)Z

    .line 458
    .line 459
    .line 460
    move-result v0

    .line 461
    if-eqz v0, :cond_f

    .line 462
    .line 463
    move-object/from16 v0, p0

    .line 464
    .line 465
    move-object/from16 v1, v18

    .line 466
    .line 467
    move-object/from16 v2, p1

    .line 468
    .line 469
    move-object v3, v4

    .line 470
    move-object v4, v8

    .line 471
    move-object v5, v15

    .line 472
    invoke-direct/range {v0 .. v6}, Lcom/intsig/office/fc/doc/DOCReader;->processRun(Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;Lcom/intsig/office/fc/hwpf/usermodel/Range;Lcom/intsig/office/fc/hwpf/usermodel/Field;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    .line 474
    .line 475
    :cond_f
    :goto_4
    const/4 v0, 0x0

    .line 476
    :goto_5
    iput-object v0, v7, Lcom/intsig/office/fc/doc/DOCReader;->hyperlinkAddress:Ljava/lang/String;

    .line 477
    .line 478
    move-object v6, v0

    .line 479
    move-object v4, v13

    .line 480
    move-object v5, v4

    .line 481
    const/16 v16, 0x0

    .line 482
    .line 483
    const/16 v17, 0x0

    .line 484
    .line 485
    :goto_6
    const/16 v19, 0x0

    .line 486
    .line 487
    goto/16 :goto_f

    .line 488
    .line 489
    :cond_10
    :goto_7
    move-object v15, v5

    .line 490
    const/4 v0, 0x0

    .line 491
    const/4 v5, 0x1

    .line 492
    const/4 v14, 0x0

    .line 493
    move-object/from16 v23, v6

    .line 494
    .line 495
    move-object v6, v4

    .line 496
    move-object/from16 v4, v23

    .line 497
    .line 498
    move-object v5, v15

    .line 499
    const/16 v16, 0x0

    .line 500
    .line 501
    const/16 v17, 0x1

    .line 502
    .line 503
    const/16 v19, 0x0

    .line 504
    .line 505
    goto :goto_e

    .line 506
    :cond_11
    :goto_8
    move-object v1, v5

    .line 507
    const/4 v0, 0x0

    .line 508
    const/4 v5, 0x1

    .line 509
    const/16 v19, 0x0

    .line 510
    .line 511
    move-object/from16 v23, v6

    .line 512
    .line 513
    move-object v6, v4

    .line 514
    move-object/from16 v4, v23

    .line 515
    .line 516
    if-ne v3, v14, :cond_12

    .line 517
    .line 518
    if-eq v2, v15, :cond_17

    .line 519
    .line 520
    :cond_12
    iget-wide v2, v7, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 521
    .line 522
    const-wide/high16 v14, -0x1000000000000000L    # -3.105036184601418E231

    .line 523
    .line 524
    and-long/2addr v2, v14

    .line 525
    const-wide/high16 v14, 0x1000000000000000L

    .line 526
    .line 527
    cmp-long v4, v2, v14

    .line 528
    .line 529
    if-eqz v4, :cond_14

    .line 530
    .line 531
    const-wide/high16 v14, 0x2000000000000000L

    .line 532
    .line 533
    cmp-long v4, v2, v14

    .line 534
    .line 535
    if-nez v4, :cond_13

    .line 536
    .line 537
    goto :goto_9

    .line 538
    :cond_13
    sget-object v2, Lcom/intsig/office/fc/hwpf/model/FieldsDocumentPart;->MAIN:Lcom/intsig/office/fc/hwpf/model/FieldsDocumentPart;

    .line 539
    .line 540
    goto :goto_a

    .line 541
    :cond_14
    :goto_9
    sget-object v2, Lcom/intsig/office/fc/hwpf/model/FieldsDocumentPart;->HEADER:Lcom/intsig/office/fc/hwpf/model/FieldsDocumentPart;

    .line 542
    .line 543
    :goto_a
    iget-object v3, v7, Lcom/intsig/office/fc/doc/DOCReader;->poiDoc:Lcom/intsig/office/fc/hwpf/HWPFDocument;

    .line 544
    .line 545
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/HWPFDocument;->getFields()Lcom/intsig/office/fc/hwpf/usermodel/Fields;

    .line 546
    .line 547
    .line 548
    move-result-object v3

    .line 549
    invoke-virtual/range {v18 .. v18}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getStartOffset()I

    .line 550
    .line 551
    .line 552
    move-result v4

    .line 553
    invoke-interface {v3, v2, v4}, Lcom/intsig/office/fc/hwpf/usermodel/Fields;->getFieldByStartOffset(Lcom/intsig/office/fc/hwpf/model/FieldsDocumentPart;I)Lcom/intsig/office/fc/hwpf/usermodel/Field;

    .line 554
    .line 555
    .line 556
    move-result-object v2

    .line 557
    move-object v5, v1

    .line 558
    move-object v4, v6

    .line 559
    const/16 v16, 0x1

    .line 560
    .line 561
    move-object v6, v2

    .line 562
    goto :goto_f

    .line 563
    :cond_15
    :goto_b
    move/from16 v20, v3

    .line 564
    .line 565
    :cond_16
    :goto_c
    move-object v1, v5

    .line 566
    const/4 v0, 0x0

    .line 567
    const/16 v19, 0x0

    .line 568
    .line 569
    move-object/from16 v23, v6

    .line 570
    .line 571
    move-object v6, v4

    .line 572
    move-object/from16 v4, v23

    .line 573
    .line 574
    :cond_17
    :goto_d
    move-object v5, v1

    .line 575
    :goto_e
    move-object/from16 v23, v6

    .line 576
    .line 577
    move-object v6, v4

    .line 578
    move-object/from16 v4, v23

    .line 579
    .line 580
    :goto_f
    add-int/lit8 v3, v20, 0x1

    .line 581
    .line 582
    move-object/from16 v1, v18

    .line 583
    .line 584
    goto/16 :goto_0

    .line 585
    .line 586
    :cond_18
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getTabClearPosition()S

    .line 587
    .line 588
    .line 589
    move-result v0

    .line 590
    if-lez v0, :cond_19

    .line 591
    .line 592
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 593
    .line 594
    .line 595
    move-result-object v0

    .line 596
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getTabClearPosition()S

    .line 597
    .line 598
    .line 599
    move-result v1

    .line 600
    invoke-virtual {v0, v9, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaTabsClearPostion(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 601
    .line 602
    .line 603
    :cond_19
    iget-wide v0, v7, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 604
    .line 605
    invoke-virtual {v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 606
    .line 607
    .line 608
    move-result-wide v2

    .line 609
    cmp-long v4, v0, v2

    .line 610
    .line 611
    if-nez v4, :cond_1a

    .line 612
    .line 613
    invoke-virtual {v8}, Lcom/intsig/office/simpletext/model/ParagraphElement;->dispose()V

    .line 614
    .line 615
    .line 616
    return-void

    .line 617
    :cond_1a
    iget-wide v0, v7, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 618
    .line 619
    invoke-virtual {v8, v0, v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 620
    .line 621
    .line 622
    iget-object v0, v7, Lcom/intsig/office/fc/doc/DOCReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 623
    .line 624
    iget-wide v1, v7, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 625
    .line 626
    invoke-virtual {v0, v8, v1, v2}, Lcom/intsig/office/wp/model/WPDocument;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 627
    .line 628
    .line 629
    iget-wide v0, v7, Lcom/intsig/office/fc/doc/DOCReader;->docRealOffset:J

    .line 630
    .line 631
    invoke-direct {v7, v11, v12, v0, v1}, Lcom/intsig/office/fc/doc/DOCReader;->adjustBookmarkOffset(JJ)V

    .line 632
    .line 633
    .line 634
    return-void
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method private processPicturePosition(Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;Lcom/intsig/office/common/shape/WPAutoShape;)V
    .locals 9

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getHorizontalPositioning()B

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x7

    .line 6
    const/4 v2, 0x6

    .line 7
    const/4 v3, 0x0

    .line 8
    const/4 v4, 0x5

    .line 9
    const/4 v5, 0x4

    .line 10
    const/4 v6, 0x3

    .line 11
    const/4 v7, 0x1

    .line 12
    const/4 v8, 0x2

    .line 13
    if-eqz v0, :cond_5

    .line 14
    .line 15
    if-eq v0, v7, :cond_4

    .line 16
    .line 17
    if-eq v0, v8, :cond_3

    .line 18
    .line 19
    if-eq v0, v6, :cond_2

    .line 20
    .line 21
    if-eq v0, v5, :cond_1

    .line 22
    .line 23
    if-eq v0, v4, :cond_0

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    invoke-virtual {p2, v1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalAlignment(B)V

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    invoke-virtual {p2, v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalAlignment(B)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_2
    invoke-virtual {p2, v6}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalAlignment(B)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_3
    invoke-virtual {p2, v8}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalAlignment(B)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_4
    invoke-virtual {p2, v7}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalAlignment(B)V

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_5
    invoke-virtual {p2, v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorPositionType(B)V

    .line 47
    .line 48
    .line 49
    :goto_0
    invoke-interface {p1}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getHorizontalRelative()B

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    if-eqz v0, :cond_9

    .line 54
    .line 55
    if-eq v0, v7, :cond_8

    .line 56
    .line 57
    if-eq v0, v8, :cond_7

    .line 58
    .line 59
    if-eq v0, v6, :cond_6

    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_6
    invoke-virtual {p2, v6}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    .line 63
    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_7
    invoke-virtual {p2, v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    .line 67
    .line 68
    .line 69
    goto :goto_1

    .line 70
    :cond_8
    invoke-virtual {p2, v8}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    .line 71
    .line 72
    .line 73
    goto :goto_1

    .line 74
    :cond_9
    invoke-virtual {p2, v7}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    .line 75
    .line 76
    .line 77
    :goto_1
    invoke-interface {p1}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getVerticalPositioning()B

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    if-eqz v0, :cond_f

    .line 82
    .line 83
    if-eq v0, v7, :cond_e

    .line 84
    .line 85
    if-eq v0, v8, :cond_d

    .line 86
    .line 87
    if-eq v0, v6, :cond_c

    .line 88
    .line 89
    if-eq v0, v5, :cond_b

    .line 90
    .line 91
    if-eq v0, v4, :cond_a

    .line 92
    .line 93
    goto :goto_2

    .line 94
    :cond_a
    invoke-virtual {p2, v1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalAlignment(B)V

    .line 95
    .line 96
    .line 97
    goto :goto_2

    .line 98
    :cond_b
    invoke-virtual {p2, v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalAlignment(B)V

    .line 99
    .line 100
    .line 101
    goto :goto_2

    .line 102
    :cond_c
    invoke-virtual {p2, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalAlignment(B)V

    .line 103
    .line 104
    .line 105
    goto :goto_2

    .line 106
    :cond_d
    invoke-virtual {p2, v8}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalAlignment(B)V

    .line 107
    .line 108
    .line 109
    goto :goto_2

    .line 110
    :cond_e
    invoke-virtual {p2, v5}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalAlignment(B)V

    .line 111
    .line 112
    .line 113
    goto :goto_2

    .line 114
    :cond_f
    invoke-virtual {p2, v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerPositionType(B)V

    .line 115
    .line 116
    .line 117
    :goto_2
    invoke-interface {p1}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getVerticalRelativeElement()B

    .line 118
    .line 119
    .line 120
    move-result p1

    .line 121
    if-eqz p1, :cond_13

    .line 122
    .line 123
    if-eq p1, v7, :cond_12

    .line 124
    .line 125
    if-eq p1, v8, :cond_11

    .line 126
    .line 127
    if-eq p1, v6, :cond_10

    .line 128
    .line 129
    goto :goto_3

    .line 130
    :cond_10
    const/16 p1, 0xb

    .line 131
    .line 132
    invoke-virtual {p2, p1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    .line 133
    .line 134
    .line 135
    goto :goto_3

    .line 136
    :cond_11
    const/16 p1, 0xa

    .line 137
    .line 138
    invoke-virtual {p2, p1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    .line 139
    .line 140
    .line 141
    goto :goto_3

    .line 142
    :cond_12
    invoke-virtual {p2, v8}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    .line 143
    .line 144
    .line 145
    goto :goto_3

    .line 146
    :cond_13
    invoke-virtual {p2, v7}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    .line 147
    .line 148
    .line 149
    :goto_3
    return-void
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processRotation(Lcom/intsig/office/fc/hwpf/usermodel/HWPFAutoShape;Lcom/intsig/office/common/shape/IShape;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getRotation()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getFlipHorizontal()Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    const/4 v2, 0x1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-interface {p2, v2}, Lcom/intsig/office/common/shape/IShape;->setFlipHorizontal(Z)V

    .line 14
    .line 15
    .line 16
    neg-float v0, v0

    .line 17
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getFlipVertical()Z

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    if-eqz p1, :cond_1

    .line 22
    .line 23
    invoke-interface {p2, v2}, Lcom/intsig/office/common/shape/IShape;->setFlipVertical(Z)V

    .line 24
    .line 25
    .line 26
    neg-float v0, v0

    .line 27
    :cond_1
    instance-of p1, p2, Lcom/intsig/office/common/shape/LineShape;

    .line 28
    .line 29
    if-eqz p1, :cond_3

    .line 30
    .line 31
    const/high16 p1, 0x42340000    # 45.0f

    .line 32
    .line 33
    cmpl-float p1, v0, p1

    .line 34
    .line 35
    if-eqz p1, :cond_2

    .line 36
    .line 37
    const/high16 p1, 0x43070000    # 135.0f

    .line 38
    .line 39
    cmpl-float p1, v0, p1

    .line 40
    .line 41
    if-eqz p1, :cond_2

    .line 42
    .line 43
    const/high16 p1, 0x43610000    # 225.0f

    .line 44
    .line 45
    cmpl-float p1, v0, p1

    .line 46
    .line 47
    if-nez p1, :cond_3

    .line 48
    .line 49
    :cond_2
    invoke-interface {p2}, Lcom/intsig/office/common/shape/IShape;->getFlipHorizontal()Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    if-nez p1, :cond_3

    .line 54
    .line 55
    invoke-interface {p2}, Lcom/intsig/office/common/shape/IShape;->getFlipVertical()Z

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    if-nez p1, :cond_3

    .line 60
    .line 61
    const/high16 p1, 0x42b40000    # 90.0f

    .line 62
    .line 63
    sub-float/2addr v0, p1

    .line 64
    :cond_3
    invoke-interface {p2, v0}, Lcom/intsig/office/common/shape/IShape;->setRotation(F)V

    .line 65
    .line 66
    .line 67
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processRowAttribute(Lcom/intsig/office/fc/hwpf/usermodel/TableRow;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableRow;->getRowHeight()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableRow;->getRowHeight()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {v0, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableRowHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 16
    .line 17
    .line 18
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableRow;->isTableHeader()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    const/4 v1, 0x1

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-virtual {v0, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableHeaderRow(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 30
    .line 31
    .line 32
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableRow;->cantSplit()Z

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    if-eqz p1, :cond_2

    .line 37
    .line 38
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-virtual {p1, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableRowSplit(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 43
    .line 44
    .line 45
    :cond_2
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private processRun(Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;Lcom/intsig/office/fc/hwpf/usermodel/Range;Lcom/intsig/office/fc/hwpf/usermodel/Field;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz p6, :cond_0

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    move-object p6, v0

    .line 9
    :goto_0
    const/4 v0, 0x1

    .line 10
    if-eqz p6, :cond_8

    .line 11
    .line 12
    invoke-virtual {p6}, Ljava/lang/String;->length()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-lez v1, :cond_8

    .line 17
    .line 18
    const/4 v1, 0x0

    .line 19
    invoke-virtual {p6, v1}, Ljava/lang/String;->charAt(I)C

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    const/16 v3, 0xc

    .line 24
    .line 25
    if-ne v2, v3, :cond_1

    .line 26
    .line 27
    const/4 v3, 0x1

    .line 28
    goto :goto_1

    .line 29
    :cond_1
    const/4 v3, 0x0

    .line 30
    :goto_1
    iput-boolean v3, p0, Lcom/intsig/office/fc/doc/DOCReader;->isBreakChar:Z

    .line 31
    .line 32
    const/16 v3, 0x8

    .line 33
    .line 34
    if-eq v2, v3, :cond_2

    .line 35
    .line 36
    if-ne v2, v0, :cond_8

    .line 37
    .line 38
    :cond_2
    const/4 p2, 0x0

    .line 39
    :goto_2
    invoke-virtual {p6}, Ljava/lang/String;->length()I

    .line 40
    .line 41
    .line 42
    move-result p3

    .line 43
    if-ge p2, p3, :cond_7

    .line 44
    .line 45
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->isVanished()Z

    .line 46
    .line 47
    .line 48
    move-result p3

    .line 49
    if-nez p3, :cond_7

    .line 50
    .line 51
    invoke-virtual {p6, p2}, Ljava/lang/String;->charAt(I)C

    .line 52
    .line 53
    .line 54
    move-result p3

    .line 55
    if-eq p3, v3, :cond_3

    .line 56
    .line 57
    if-ne p3, v0, :cond_6

    .line 58
    .line 59
    :cond_3
    new-instance p5, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 60
    .line 61
    invoke-static {p3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v2

    .line 65
    invoke-direct {p5, v2}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    if-ne p3, v3, :cond_4

    .line 69
    .line 70
    const/4 p3, 0x1

    .line 71
    goto :goto_3

    .line 72
    :cond_4
    const/4 p3, 0x0

    .line 73
    :goto_3
    invoke-direct {p0, p1, p5, p3, p2}, Lcom/intsig/office/fc/doc/DOCReader;->processShape(Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;Lcom/intsig/office/simpletext/model/IElement;ZI)Z

    .line 74
    .line 75
    .line 76
    move-result p3

    .line 77
    if-nez p3, :cond_5

    .line 78
    .line 79
    return-void

    .line 80
    :cond_5
    iget-wide v4, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 81
    .line 82
    invoke-virtual {p5, v4, v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 83
    .line 84
    .line 85
    iget-wide v4, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 86
    .line 87
    const-wide/16 v6, 0x1

    .line 88
    .line 89
    add-long/2addr v4, v6

    .line 90
    iput-wide v4, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 91
    .line 92
    invoke-virtual {p5, v4, v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 93
    .line 94
    .line 95
    invoke-virtual {p4, p5}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 96
    .line 97
    .line 98
    :cond_6
    add-int/lit8 p2, p2, 0x1

    .line 99
    .line 100
    goto :goto_2

    .line 101
    :cond_7
    return-void

    .line 102
    :cond_8
    new-instance v1, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 103
    .line 104
    invoke-direct {v1, p6}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 108
    .line 109
    .line 110
    move-result-object v2

    .line 111
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 112
    .line 113
    .line 114
    move-result-object v3

    .line 115
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->getFontSize()I

    .line 116
    .line 117
    .line 118
    move-result v4

    .line 119
    int-to-float v4, v4

    .line 120
    const/high16 v5, 0x40000000    # 2.0f

    .line 121
    .line 122
    div-float/2addr v4, v5

    .line 123
    float-to-double v4, v4

    .line 124
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    .line 125
    .line 126
    add-double/2addr v4, v6

    .line 127
    double-to-int v4, v4

    .line 128
    invoke-virtual {v3, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontSize(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 129
    .line 130
    .line 131
    invoke-static {}, Lcom/intsig/office/simpletext/font/FontTypefaceManage;->instance()Lcom/intsig/office/simpletext/font/FontTypefaceManage;

    .line 132
    .line 133
    .line 134
    move-result-object v3

    .line 135
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->getFontName()Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v4

    .line 139
    invoke-virtual {v3, v4}, Lcom/intsig/office/simpletext/font/FontTypefaceManage;->addFontName(Ljava/lang/String;)I

    .line 140
    .line 141
    .line 142
    move-result v3

    .line 143
    if-ltz v3, :cond_9

    .line 144
    .line 145
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 146
    .line 147
    .line 148
    move-result-object v4

    .line 149
    invoke-virtual {v4, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontName(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 150
    .line 151
    .line 152
    :cond_9
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 153
    .line 154
    .line 155
    move-result-object v3

    .line 156
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->getIco24()I

    .line 157
    .line 158
    .line 159
    move-result v4

    .line 160
    invoke-static {v4}, Lcom/intsig/office/fc/FCKit;->BGRtoRGB(I)I

    .line 161
    .line 162
    .line 163
    move-result v4

    .line 164
    invoke-virtual {v3, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 165
    .line 166
    .line 167
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 168
    .line 169
    .line 170
    move-result-object v3

    .line 171
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->isBold()Z

    .line 172
    .line 173
    .line 174
    move-result v4

    .line 175
    invoke-virtual {v3, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontBold(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 176
    .line 177
    .line 178
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 179
    .line 180
    .line 181
    move-result-object v3

    .line 182
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->isItalic()Z

    .line 183
    .line 184
    .line 185
    move-result v4

    .line 186
    invoke-virtual {v3, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontItalic(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 187
    .line 188
    .line 189
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 190
    .line 191
    .line 192
    move-result-object v3

    .line 193
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->isStrikeThrough()Z

    .line 194
    .line 195
    .line 196
    move-result v4

    .line 197
    invoke-virtual {v3, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontStrike(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 198
    .line 199
    .line 200
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 201
    .line 202
    .line 203
    move-result-object v3

    .line 204
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->isDoubleStrikeThrough()Z

    .line 205
    .line 206
    .line 207
    move-result v4

    .line 208
    invoke-virtual {v3, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontDoubleStrike(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 209
    .line 210
    .line 211
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 212
    .line 213
    .line 214
    move-result-object v3

    .line 215
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->getUnderlineCode()I

    .line 216
    .line 217
    .line 218
    move-result v4

    .line 219
    invoke-virtual {v3, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontUnderline(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 220
    .line 221
    .line 222
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 223
    .line 224
    .line 225
    move-result-object v3

    .line 226
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->getUnderlineColor()I

    .line 227
    .line 228
    .line 229
    move-result v4

    .line 230
    invoke-static {v4}, Lcom/intsig/office/fc/FCKit;->BGRtoRGB(I)I

    .line 231
    .line 232
    .line 233
    move-result v4

    .line 234
    invoke-virtual {v3, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontUnderlineColr(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 235
    .line 236
    .line 237
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 238
    .line 239
    .line 240
    move-result-object v3

    .line 241
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->getSubSuperScriptIndex()S

    .line 242
    .line 243
    .line 244
    move-result v4

    .line 245
    invoke-virtual {v3, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontScript(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 246
    .line 247
    .line 248
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 249
    .line 250
    .line 251
    move-result-object v3

    .line 252
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->getHighlightedColor()B

    .line 253
    .line 254
    .line 255
    move-result p1

    .line 256
    int-to-short p1, p1

    .line 257
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/doc/DOCReader;->converterColorForIndex(S)I

    .line 258
    .line 259
    .line 260
    move-result p1

    .line 261
    invoke-virtual {v3, v2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontHighLight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 262
    .line 263
    .line 264
    const p1, -0xffff01

    .line 265
    .line 266
    .line 267
    if-eqz p3, :cond_b

    .line 268
    .line 269
    invoke-interface {p3}, Lcom/intsig/office/fc/hwpf/usermodel/Field;->getType()I

    .line 270
    .line 271
    .line 272
    move-result v3

    .line 273
    const/16 v4, 0x58

    .line 274
    .line 275
    if-ne v3, v4, :cond_b

    .line 276
    .line 277
    iget-object p5, p0, Lcom/intsig/office/fc/doc/DOCReader;->hyperlinkAddress:Ljava/lang/String;

    .line 278
    .line 279
    if-nez p5, :cond_a

    .line 280
    .line 281
    invoke-interface {p3, p2}, Lcom/intsig/office/fc/hwpf/usermodel/Field;->firstSubrange(Lcom/intsig/office/fc/hwpf/usermodel/Range;)Lcom/intsig/office/fc/hwpf/usermodel/Range;

    .line 282
    .line 283
    .line 284
    move-result-object p2

    .line 285
    if-eqz p2, :cond_a

    .line 286
    .line 287
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->text()Ljava/lang/String;

    .line 288
    .line 289
    .line 290
    move-result-object p2

    .line 291
    iget-object p3, p0, Lcom/intsig/office/fc/doc/DOCReader;->hyperlinkPattern:Ljava/util/regex/Pattern;

    .line 292
    .line 293
    invoke-virtual {p3, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    .line 294
    .line 295
    .line 296
    move-result-object p2

    .line 297
    invoke-virtual {p2}, Ljava/util/regex/Matcher;->find()Z

    .line 298
    .line 299
    .line 300
    move-result p3

    .line 301
    if-eqz p3, :cond_a

    .line 302
    .line 303
    invoke-virtual {p2, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    .line 304
    .line 305
    .line 306
    move-result-object p2

    .line 307
    iput-object p2, p0, Lcom/intsig/office/fc/doc/DOCReader;->hyperlinkAddress:Ljava/lang/String;

    .line 308
    .line 309
    :cond_a
    iget-object p2, p0, Lcom/intsig/office/fc/doc/DOCReader;->hyperlinkAddress:Ljava/lang/String;

    .line 310
    .line 311
    if-eqz p2, :cond_10

    .line 312
    .line 313
    iget-object p2, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 314
    .line 315
    invoke-interface {p2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 316
    .line 317
    .line 318
    move-result-object p2

    .line 319
    invoke-virtual {p2}, Lcom/intsig/office/system/SysKit;->getHyperlinkManage()Lcom/intsig/office/common/hyperlink/HyperlinkManage;

    .line 320
    .line 321
    .line 322
    move-result-object p2

    .line 323
    iget-object p3, p0, Lcom/intsig/office/fc/doc/DOCReader;->hyperlinkAddress:Ljava/lang/String;

    .line 324
    .line 325
    invoke-virtual {p2, p3, v0}, Lcom/intsig/office/common/hyperlink/HyperlinkManage;->addHyperlink(Ljava/lang/String;I)I

    .line 326
    .line 327
    .line 328
    move-result p2

    .line 329
    if-ltz p2, :cond_10

    .line 330
    .line 331
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 332
    .line 333
    .line 334
    move-result-object p3

    .line 335
    invoke-virtual {p3, v2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 336
    .line 337
    .line 338
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 339
    .line 340
    .line 341
    move-result-object p3

    .line 342
    invoke-virtual {p3, v2, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontUnderline(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 343
    .line 344
    .line 345
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 346
    .line 347
    .line 348
    move-result-object p3

    .line 349
    invoke-virtual {p3, v2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontUnderlineColr(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 350
    .line 351
    .line 352
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 353
    .line 354
    .line 355
    move-result-object p1

    .line 356
    invoke-virtual {p1, v2, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->setHyperlinkID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 357
    .line 358
    .line 359
    goto/16 :goto_5

    .line 360
    .line 361
    :cond_b
    if-eqz p5, :cond_10

    .line 362
    .line 363
    const-string p2, "HYPERLINK"

    .line 364
    .line 365
    invoke-virtual {p5, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 366
    .line 367
    .line 368
    move-result p2

    .line 369
    if-lez p2, :cond_c

    .line 370
    .line 371
    const-string p2, "_Toc"

    .line 372
    .line 373
    invoke-virtual {p5, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 374
    .line 375
    .line 376
    move-result p2

    .line 377
    if-lez p2, :cond_10

    .line 378
    .line 379
    const/16 p3, 0x22

    .line 380
    .line 381
    invoke-virtual {p5, p3}, Ljava/lang/String;->lastIndexOf(I)I

    .line 382
    .line 383
    .line 384
    move-result p3

    .line 385
    if-lez p3, :cond_10

    .line 386
    .line 387
    if-le p3, p2, :cond_10

    .line 388
    .line 389
    invoke-virtual {p5, p2, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 390
    .line 391
    .line 392
    move-result-object p2

    .line 393
    iget-object p3, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 394
    .line 395
    invoke-interface {p3}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 396
    .line 397
    .line 398
    move-result-object p3

    .line 399
    invoke-virtual {p3}, Lcom/intsig/office/system/SysKit;->getHyperlinkManage()Lcom/intsig/office/common/hyperlink/HyperlinkManage;

    .line 400
    .line 401
    .line 402
    move-result-object p3

    .line 403
    const/4 p5, 0x5

    .line 404
    invoke-virtual {p3, p2, p5}, Lcom/intsig/office/common/hyperlink/HyperlinkManage;->addHyperlink(Ljava/lang/String;I)I

    .line 405
    .line 406
    .line 407
    move-result p2

    .line 408
    if-ltz p2, :cond_10

    .line 409
    .line 410
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 411
    .line 412
    .line 413
    move-result-object p3

    .line 414
    invoke-virtual {p3, v2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 415
    .line 416
    .line 417
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 418
    .line 419
    .line 420
    move-result-object p3

    .line 421
    invoke-virtual {p3, v2, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontUnderline(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 422
    .line 423
    .line 424
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 425
    .line 426
    .line 427
    move-result-object p3

    .line 428
    invoke-virtual {p3, v2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontUnderlineColr(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 429
    .line 430
    .line 431
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 432
    .line 433
    .line 434
    move-result-object p1

    .line 435
    invoke-virtual {p1, v2, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->setHyperlinkID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 436
    .line 437
    .line 438
    goto :goto_5

    .line 439
    :cond_c
    iget-wide p1, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 440
    .line 441
    const-wide/high16 v2, -0x1000000000000000L    # -3.105036184601418E231

    .line 442
    .line 443
    and-long/2addr p1, v2

    .line 444
    const-wide/high16 v2, 0x1000000000000000L

    .line 445
    .line 446
    cmp-long p3, p1, v2

    .line 447
    .line 448
    if-eqz p3, :cond_d

    .line 449
    .line 450
    const-wide/high16 v2, 0x2000000000000000L

    .line 451
    .line 452
    cmp-long p3, p1, v2

    .line 453
    .line 454
    if-nez p3, :cond_10

    .line 455
    .line 456
    :cond_d
    const-string p1, "NUMPAGES"

    .line 457
    .line 458
    invoke-virtual {p5, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 459
    .line 460
    .line 461
    move-result p1

    .line 462
    if-eqz p1, :cond_e

    .line 463
    .line 464
    const/4 v0, 0x2

    .line 465
    goto :goto_4

    .line 466
    :cond_e
    const-string p1, "PAGE"

    .line 467
    .line 468
    invoke-virtual {p5, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 469
    .line 470
    .line 471
    move-result p1

    .line 472
    if-eqz p1, :cond_f

    .line 473
    .line 474
    goto :goto_4

    .line 475
    :cond_f
    const/4 v0, -0x1

    .line 476
    :goto_4
    if-lez v0, :cond_10

    .line 477
    .line 478
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 479
    .line 480
    .line 481
    move-result-object p1

    .line 482
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 483
    .line 484
    .line 485
    move-result-object p2

    .line 486
    invoke-virtual {p1, p2, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontPageNumberType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 487
    .line 488
    .line 489
    :cond_10
    :goto_5
    iget-wide p1, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 490
    .line 491
    invoke-virtual {v1, p1, p2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 492
    .line 493
    .line 494
    iget-wide p1, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 495
    .line 496
    invoke-virtual {p6}, Ljava/lang/String;->length()I

    .line 497
    .line 498
    .line 499
    move-result p3

    .line 500
    int-to-long p5, p3

    .line 501
    add-long/2addr p1, p5

    .line 502
    iput-wide p1, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 503
    .line 504
    invoke-virtual {v1, p1, p2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 505
    .line 506
    .line 507
    invoke-virtual {p4, v1}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 508
    .line 509
    .line 510
    return-void
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
.end method

.method private processSection(Lcom/intsig/office/fc/hwpf/usermodel/Section;)V
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getPageWidth()I

    .line 15
    .line 16
    .line 17
    move-result v3

    .line 18
    invoke-virtual {v2, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 19
    .line 20
    .line 21
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getPageHeight()I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    invoke-virtual {v2, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 30
    .line 31
    .line 32
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getMarginLeft()I

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    invoke-virtual {v2, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 41
    .line 42
    .line 43
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getMarginRight()I

    .line 48
    .line 49
    .line 50
    move-result v3

    .line 51
    invoke-virtual {v2, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 52
    .line 53
    .line 54
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getMarginTop()I

    .line 59
    .line 60
    .line 61
    move-result v3

    .line 62
    invoke-virtual {v2, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 63
    .line 64
    .line 65
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getMarginBottom()I

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    invoke-virtual {v2, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 74
    .line 75
    .line 76
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getMarginHeader()I

    .line 81
    .line 82
    .line 83
    move-result v3

    .line 84
    invoke-virtual {v2, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeaderMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 85
    .line 86
    .line 87
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 88
    .line 89
    .line 90
    move-result-object v2

    .line 91
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getMarginFooter()I

    .line 92
    .line 93
    .line 94
    move-result v3

    .line 95
    invoke-virtual {v2, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageFooterMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getGridType()I

    .line 99
    .line 100
    .line 101
    move-result v2

    .line 102
    if-eqz v2, :cond_0

    .line 103
    .line 104
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 105
    .line 106
    .line 107
    move-result-object v2

    .line 108
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getLinePitch()I

    .line 109
    .line 110
    .line 111
    move-result v3

    .line 112
    invoke-virtual {v2, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageLinePitch(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 113
    .line 114
    .line 115
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/doc/DOCReader;->processSectionBorder(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/hwpf/usermodel/Section;)V

    .line 116
    .line 117
    .line 118
    iget-wide v1, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 119
    .line 120
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 121
    .line 122
    .line 123
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->numParagraphs()I

    .line 124
    .line 125
    .line 126
    move-result v1

    .line 127
    const/4 v2, 0x0

    .line 128
    :goto_0
    if-ge v2, v1, :cond_2

    .line 129
    .line 130
    iget-boolean v3, p0, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 131
    .line 132
    if-nez v3, :cond_2

    .line 133
    .line 134
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getParagraph(I)Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;

    .line 135
    .line 136
    .line 137
    move-result-object v3

    .line 138
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->isInTable()Z

    .line 139
    .line 140
    .line 141
    move-result v4

    .line 142
    if-eqz v4, :cond_1

    .line 143
    .line 144
    invoke-virtual {p1, v3}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getTable(Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;)Lcom/intsig/office/fc/hwpf/usermodel/Table;

    .line 145
    .line 146
    .line 147
    move-result-object v3

    .line 148
    invoke-direct {p0, v3}, Lcom/intsig/office/fc/doc/DOCReader;->processTable(Lcom/intsig/office/fc/hwpf/usermodel/Table;)V

    .line 149
    .line 150
    .line 151
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->numParagraphs()I

    .line 152
    .line 153
    .line 154
    move-result v3

    .line 155
    add-int/lit8 v3, v3, -0x1

    .line 156
    .line 157
    add-int/2addr v2, v3

    .line 158
    goto :goto_1

    .line 159
    :cond_1
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getParagraph(I)Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;

    .line 160
    .line 161
    .line 162
    move-result-object v3

    .line 163
    invoke-direct {p0, v3}, Lcom/intsig/office/fc/doc/DOCReader;->processParagraph(Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;)V

    .line 164
    .line 165
    .line 166
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 167
    .line 168
    goto :goto_0

    .line 169
    :cond_2
    iget-wide v1, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 170
    .line 171
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 172
    .line 173
    .line 174
    iget-object p1, p0, Lcom/intsig/office/fc/doc/DOCReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 175
    .line 176
    invoke-virtual {p1, v0}, Lcom/intsig/office/wp/model/WPDocument;->appendSection(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 177
    .line 178
    .line 179
    return-void
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private processSectionBorder(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/hwpf/usermodel/Section;)V
    .locals 8

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getTopBorder()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getBottomBorder()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getLeftBorder()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getRightBorder()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 14
    .line 15
    .line 16
    move-result-object v3

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    if-nez v1, :cond_0

    .line 20
    .line 21
    if-nez v2, :cond_0

    .line 22
    .line 23
    if-eqz v3, :cond_9

    .line 24
    .line 25
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getPageBorderInfo()I

    .line 26
    .line 27
    .line 28
    move-result p2

    .line 29
    int-to-byte p2, p2

    .line 30
    shr-int/lit8 p2, p2, 0x5

    .line 31
    .line 32
    and-int/lit8 p2, p2, 0x7

    .line 33
    .line 34
    int-to-byte p2, p2

    .line 35
    new-instance v4, Lcom/intsig/office/common/borders/Borders;

    .line 36
    .line 37
    invoke-direct {v4}, Lcom/intsig/office/common/borders/Borders;-><init>()V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v4, p2}, Lcom/intsig/office/common/borders/Borders;->setOnType(B)V

    .line 41
    .line 42
    .line 43
    const p2, 0x3faaaaab

    .line 44
    .line 45
    .line 46
    const/high16 v5, -0x1000000

    .line 47
    .line 48
    if-eqz v0, :cond_2

    .line 49
    .line 50
    new-instance v6, Lcom/intsig/office/common/borders/Border;

    .line 51
    .line 52
    invoke-direct {v6}, Lcom/intsig/office/common/borders/Border;-><init>()V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->getColor()S

    .line 56
    .line 57
    .line 58
    move-result v7

    .line 59
    if-nez v7, :cond_1

    .line 60
    .line 61
    const/high16 v7, -0x1000000

    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->getColor()S

    .line 65
    .line 66
    .line 67
    move-result v7

    .line 68
    invoke-direct {p0, v7}, Lcom/intsig/office/fc/doc/DOCReader;->converterColorForIndex(S)I

    .line 69
    .line 70
    .line 71
    move-result v7

    .line 72
    :goto_0
    invoke-virtual {v6, v7}, Lcom/intsig/office/common/borders/Border;->setColor(I)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->getSpace()I

    .line 76
    .line 77
    .line 78
    move-result v0

    .line 79
    int-to-float v0, v0

    .line 80
    mul-float v0, v0, p2

    .line 81
    .line 82
    float-to-int v0, v0

    .line 83
    int-to-short v0, v0

    .line 84
    invoke-virtual {v6, v0}, Lcom/intsig/office/common/borders/Border;->setSpace(S)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {v4, v6}, Lcom/intsig/office/common/borders/Borders;->setTopBorder(Lcom/intsig/office/common/borders/Border;)V

    .line 88
    .line 89
    .line 90
    :cond_2
    if-eqz v1, :cond_4

    .line 91
    .line 92
    new-instance v0, Lcom/intsig/office/common/borders/Border;

    .line 93
    .line 94
    invoke-direct {v0}, Lcom/intsig/office/common/borders/Border;-><init>()V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->getColor()S

    .line 98
    .line 99
    .line 100
    move-result v6

    .line 101
    if-nez v6, :cond_3

    .line 102
    .line 103
    const/high16 v6, -0x1000000

    .line 104
    .line 105
    goto :goto_1

    .line 106
    :cond_3
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->getColor()S

    .line 107
    .line 108
    .line 109
    move-result v6

    .line 110
    invoke-direct {p0, v6}, Lcom/intsig/office/fc/doc/DOCReader;->converterColorForIndex(S)I

    .line 111
    .line 112
    .line 113
    move-result v6

    .line 114
    :goto_1
    invoke-virtual {v0, v6}, Lcom/intsig/office/common/borders/Border;->setColor(I)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->getSpace()I

    .line 118
    .line 119
    .line 120
    move-result v1

    .line 121
    int-to-float v1, v1

    .line 122
    mul-float v1, v1, p2

    .line 123
    .line 124
    float-to-int v1, v1

    .line 125
    int-to-short v1, v1

    .line 126
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/borders/Border;->setSpace(S)V

    .line 127
    .line 128
    .line 129
    invoke-virtual {v4, v0}, Lcom/intsig/office/common/borders/Borders;->setBottomBorder(Lcom/intsig/office/common/borders/Border;)V

    .line 130
    .line 131
    .line 132
    :cond_4
    if-eqz v2, :cond_6

    .line 133
    .line 134
    new-instance v0, Lcom/intsig/office/common/borders/Border;

    .line 135
    .line 136
    invoke-direct {v0}, Lcom/intsig/office/common/borders/Border;-><init>()V

    .line 137
    .line 138
    .line 139
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->getColor()S

    .line 140
    .line 141
    .line 142
    move-result v1

    .line 143
    if-nez v1, :cond_5

    .line 144
    .line 145
    const/high16 v1, -0x1000000

    .line 146
    .line 147
    goto :goto_2

    .line 148
    :cond_5
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->getColor()S

    .line 149
    .line 150
    .line 151
    move-result v1

    .line 152
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/doc/DOCReader;->converterColorForIndex(S)I

    .line 153
    .line 154
    .line 155
    move-result v1

    .line 156
    :goto_2
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/borders/Border;->setColor(I)V

    .line 157
    .line 158
    .line 159
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->getSpace()I

    .line 160
    .line 161
    .line 162
    move-result v1

    .line 163
    int-to-float v1, v1

    .line 164
    mul-float v1, v1, p2

    .line 165
    .line 166
    float-to-int v1, v1

    .line 167
    int-to-short v1, v1

    .line 168
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/borders/Border;->setSpace(S)V

    .line 169
    .line 170
    .line 171
    invoke-virtual {v4, v0}, Lcom/intsig/office/common/borders/Borders;->setLeftBorder(Lcom/intsig/office/common/borders/Border;)V

    .line 172
    .line 173
    .line 174
    :cond_6
    if-eqz v3, :cond_8

    .line 175
    .line 176
    new-instance v0, Lcom/intsig/office/common/borders/Border;

    .line 177
    .line 178
    invoke-direct {v0}, Lcom/intsig/office/common/borders/Border;-><init>()V

    .line 179
    .line 180
    .line 181
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->getColor()S

    .line 182
    .line 183
    .line 184
    move-result v1

    .line 185
    if-nez v1, :cond_7

    .line 186
    .line 187
    goto :goto_3

    .line 188
    :cond_7
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->getColor()S

    .line 189
    .line 190
    .line 191
    move-result v1

    .line 192
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/doc/DOCReader;->converterColorForIndex(S)I

    .line 193
    .line 194
    .line 195
    move-result v5

    .line 196
    :goto_3
    invoke-virtual {v0, v5}, Lcom/intsig/office/common/borders/Border;->setColor(I)V

    .line 197
    .line 198
    .line 199
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->getSpace()I

    .line 200
    .line 201
    .line 202
    move-result v1

    .line 203
    int-to-float v1, v1

    .line 204
    mul-float v1, v1, p2

    .line 205
    .line 206
    float-to-int p2, v1

    .line 207
    int-to-short p2, p2

    .line 208
    invoke-virtual {v0, p2}, Lcom/intsig/office/common/borders/Border;->setSpace(S)V

    .line 209
    .line 210
    .line 211
    invoke-virtual {v4, v0}, Lcom/intsig/office/common/borders/Borders;->setRightBorder(Lcom/intsig/office/common/borders/Border;)V

    .line 212
    .line 213
    .line 214
    :cond_8
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 215
    .line 216
    .line 217
    move-result-object p2

    .line 218
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 219
    .line 220
    .line 221
    move-result-object p1

    .line 222
    iget-object v0, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 223
    .line 224
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 225
    .line 226
    .line 227
    move-result-object v0

    .line 228
    invoke-virtual {v0}, Lcom/intsig/office/system/SysKit;->getBordersManage()Lcom/intsig/office/common/borders/BordersManage;

    .line 229
    .line 230
    .line 231
    move-result-object v0

    .line 232
    invoke-virtual {v0, v4}, Lcom/intsig/office/common/borders/BordersManage;->addBorders(Lcom/intsig/office/common/borders/Borders;)I

    .line 233
    .line 234
    .line 235
    move-result v0

    .line 236
    invoke-virtual {p2, p1, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageBorder(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 237
    .line 238
    .line 239
    :cond_9
    return-void
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processShape(Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;Lcom/intsig/office/simpletext/model/IElement;ZI)Z
    .locals 9

    .line 1
    const/4 v1, 0x1

    .line 2
    const/4 v2, 0x2

    .line 3
    const/4 v3, 0x0

    .line 4
    const v4, 0x3d888889

    .line 5
    .line 6
    .line 7
    if-eqz p3, :cond_5

    .line 8
    .line 9
    iget-object v5, p0, Lcom/intsig/office/fc/doc/DOCReader;->poiDoc:Lcom/intsig/office/fc/hwpf/HWPFDocument;

    .line 10
    .line 11
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/HWPFDocument;->getOfficeDrawingsMain()Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawings;

    .line 12
    .line 13
    .line 14
    move-result-object v5

    .line 15
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getStartOffset()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    add-int/2addr v0, p4

    .line 20
    invoke-interface {v5, v0}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawings;->getOfficeDrawingAt(I)Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;

    .line 21
    .line 22
    .line 23
    move-result-object v5

    .line 24
    if-nez v5, :cond_0

    .line 25
    .line 26
    return v3

    .line 27
    :cond_0
    new-instance v6, Lcom/intsig/office/java/awt/Rectangle;

    .line 28
    .line 29
    invoke-direct {v6}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 30
    .line 31
    .line 32
    invoke-interface {v5}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getRectangleLeft()I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    int-to-float v0, v0

    .line 37
    mul-float v0, v0, v4

    .line 38
    .line 39
    float-to-int v0, v0

    .line 40
    iput v0, v6, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 41
    .line 42
    invoke-interface {v5}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getRectangleTop()I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    int-to-float v0, v0

    .line 47
    mul-float v0, v0, v4

    .line 48
    .line 49
    float-to-int v0, v0

    .line 50
    iput v0, v6, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 51
    .line 52
    invoke-interface {v5}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getRectangleRight()I

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    invoke-interface {v5}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getRectangleLeft()I

    .line 57
    .line 58
    .line 59
    move-result v7

    .line 60
    sub-int/2addr v0, v7

    .line 61
    int-to-float v0, v0

    .line 62
    mul-float v0, v0, v4

    .line 63
    .line 64
    float-to-int v0, v0

    .line 65
    iput v0, v6, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 66
    .line 67
    invoke-interface {v5}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getRectangleBottom()I

    .line 68
    .line 69
    .line 70
    move-result v0

    .line 71
    invoke-interface {v5}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getRectangleTop()I

    .line 72
    .line 73
    .line 74
    move-result v7

    .line 75
    sub-int/2addr v0, v7

    .line 76
    int-to-float v0, v0

    .line 77
    mul-float v0, v0, v4

    .line 78
    .line 79
    float-to-int v0, v0

    .line 80
    iput v0, v6, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 83
    .line 84
    invoke-interface {v5, v0}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getPictureData(Lcom/intsig/office/system/IControl;)[B

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    if-eqz v0, :cond_4

    .line 89
    .line 90
    invoke-static {v0}, Lcom/intsig/office/fc/hwpf/usermodel/PictureType;->findMatchingType([B)Lcom/intsig/office/fc/hwpf/usermodel/PictureType;

    .line 91
    .line 92
    .line 93
    move-result-object v4

    .line 94
    invoke-direct {p0, v4}, Lcom/intsig/office/fc/doc/DOCReader;->isSupportPicture(Lcom/intsig/office/fc/hwpf/usermodel/PictureType;)Z

    .line 95
    .line 96
    .line 97
    move-result v4

    .line 98
    if-eqz v4, :cond_8

    .line 99
    .line 100
    new-instance v3, Lcom/intsig/office/common/shape/PictureShape;

    .line 101
    .line 102
    invoke-direct {v3}, Lcom/intsig/office/common/shape/PictureShape;-><init>()V

    .line 103
    .line 104
    .line 105
    iget-object v4, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 106
    .line 107
    invoke-interface {v4}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 108
    .line 109
    .line 110
    move-result-object v4

    .line 111
    invoke-virtual {v4}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 112
    .line 113
    .line 114
    move-result-object v4

    .line 115
    iget-object v7, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 116
    .line 117
    invoke-interface {v5, v7}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getTempFilePath(Lcom/intsig/office/system/IControl;)Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object v7

    .line 121
    invoke-virtual {v4, v7}, Lcom/intsig/office/common/picture/PictureManage;->getPictureIndex(Ljava/lang/String;)I

    .line 122
    .line 123
    .line 124
    move-result v4

    .line 125
    if-gez v4, :cond_1

    .line 126
    .line 127
    new-instance v4, Lcom/intsig/office/common/picture/Picture;

    .line 128
    .line 129
    invoke-direct {v4}, Lcom/intsig/office/common/picture/Picture;-><init>()V

    .line 130
    .line 131
    .line 132
    iget-object v7, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 133
    .line 134
    invoke-interface {v5, v7}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getTempFilePath(Lcom/intsig/office/system/IControl;)Ljava/lang/String;

    .line 135
    .line 136
    .line 137
    move-result-object v7

    .line 138
    invoke-virtual {v4, v7}, Lcom/intsig/office/common/picture/Picture;->setTempFilePath(Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    invoke-static {v0}, Lcom/intsig/office/fc/hwpf/usermodel/PictureType;->findMatchingType([B)Lcom/intsig/office/fc/hwpf/usermodel/PictureType;

    .line 142
    .line 143
    .line 144
    move-result-object v0

    .line 145
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/PictureType;->getExtension()Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v0

    .line 149
    invoke-virtual {v4, v0}, Lcom/intsig/office/common/picture/Picture;->setPictureType(Ljava/lang/String;)V

    .line 150
    .line 151
    .line 152
    iget-object v0, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 153
    .line 154
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 155
    .line 156
    .line 157
    move-result-object v0

    .line 158
    invoke-virtual {v0}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 159
    .line 160
    .line 161
    move-result-object v0

    .line 162
    invoke-virtual {v0, v4}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/common/picture/Picture;)I

    .line 163
    .line 164
    .line 165
    move-result v4

    .line 166
    :cond_1
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/shape/PictureShape;->setPictureIndex(I)V

    .line 167
    .line 168
    .line 169
    invoke-virtual {v3, v6}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 170
    .line 171
    .line 172
    const/16 v0, 0x3e8

    .line 173
    .line 174
    invoke-virtual {v3, v0}, Lcom/intsig/office/common/shape/PictureShape;->setZoomX(S)V

    .line 175
    .line 176
    .line 177
    invoke-virtual {v3, v0}, Lcom/intsig/office/common/shape/PictureShape;->setZoomY(S)V

    .line 178
    .line 179
    .line 180
    invoke-interface {v5}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getPictureEffectInfor()Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 181
    .line 182
    .line 183
    move-result-object v0

    .line 184
    invoke-virtual {v3, v0}, Lcom/intsig/office/common/shape/PictureShape;->setPictureEffectInfor(Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;)V

    .line 185
    .line 186
    .line 187
    new-instance v0, Lcom/intsig/office/common/shape/WPPictureShape;

    .line 188
    .line 189
    invoke-direct {v0}, Lcom/intsig/office/common/shape/WPPictureShape;-><init>()V

    .line 190
    .line 191
    .line 192
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/shape/WPPictureShape;->setPictureShape(Lcom/intsig/office/common/shape/PictureShape;)V

    .line 193
    .line 194
    .line 195
    invoke-interface {v5}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getWrap()I

    .line 196
    .line 197
    .line 198
    move-result v3

    .line 199
    const/4 v4, 0x3

    .line 200
    if-ne v3, v4, :cond_3

    .line 201
    .line 202
    invoke-interface {v5}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->isAnchorLock()Z

    .line 203
    .line 204
    .line 205
    move-result v3

    .line 206
    if-nez v3, :cond_3

    .line 207
    .line 208
    invoke-interface {v5}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->isBelowText()Z

    .line 209
    .line 210
    .line 211
    move-result v2

    .line 212
    if-eqz v2, :cond_2

    .line 213
    .line 214
    const/4 v2, 0x6

    .line 215
    invoke-virtual {v0, v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 216
    .line 217
    .line 218
    goto :goto_0

    .line 219
    :cond_2
    invoke-virtual {v0, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 220
    .line 221
    .line 222
    :goto_0
    invoke-direct {p0, v5, v0}, Lcom/intsig/office/fc/doc/DOCReader;->processPicturePosition(Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;Lcom/intsig/office/common/shape/WPAutoShape;)V

    .line 223
    .line 224
    .line 225
    goto :goto_1

    .line 226
    :cond_3
    invoke-virtual {v0, v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 227
    .line 228
    .line 229
    :goto_1
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 230
    .line 231
    .line 232
    move-result-object v2

    .line 233
    invoke-interface {p2}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 234
    .line 235
    .line 236
    move-result-object v3

    .line 237
    iget-object v4, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 238
    .line 239
    invoke-interface {v4}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 240
    .line 241
    .line 242
    move-result-object v4

    .line 243
    invoke-virtual {v4}, Lcom/intsig/office/system/SysKit;->getWPShapeManage()Lcom/intsig/office/wp/control/WPShapeManage;

    .line 244
    .line 245
    .line 246
    move-result-object v4

    .line 247
    invoke-virtual {v4, v0}, Lcom/intsig/office/wp/control/WPShapeManage;->addShape(Lcom/intsig/office/common/shape/AbstractShape;)I

    .line 248
    .line 249
    .line 250
    move-result v0

    .line 251
    invoke-virtual {v2, v3, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setShapeID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 252
    .line 253
    .line 254
    return v1

    .line 255
    :cond_4
    invoke-interface {v5}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;->getAutoShape()Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;

    .line 256
    .line 257
    .line 258
    move-result-object v4

    .line 259
    if-eqz v4, :cond_8

    .line 260
    .line 261
    const/4 v3, 0x0

    .line 262
    const/high16 v7, 0x3f800000    # 1.0f

    .line 263
    .line 264
    const/high16 v8, 0x3f800000    # 1.0f

    .line 265
    .line 266
    move-object v0, p0

    .line 267
    move-object v1, p2

    .line 268
    move-object v2, v5

    .line 269
    move-object v5, v6

    .line 270
    move v6, v7

    .line 271
    move v7, v8

    .line 272
    invoke-direct/range {v0 .. v7}, Lcom/intsig/office/fc/doc/DOCReader;->convertShape(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;Lcom/intsig/office/java/awt/Rectangle;FF)Z

    .line 273
    .line 274
    .line 275
    move-result v0

    .line 276
    return v0

    .line 277
    :cond_5
    iget-object v5, p0, Lcom/intsig/office/fc/doc/DOCReader;->poiDoc:Lcom/intsig/office/fc/hwpf/HWPFDocument;

    .line 278
    .line 279
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/HWPFDocument;->getPicturesTable()Lcom/intsig/office/fc/hwpf/model/PicturesTable;

    .line 280
    .line 281
    .line 282
    move-result-object v5

    .line 283
    iget-object v6, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 284
    .line 285
    invoke-interface {v6}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 286
    .line 287
    .line 288
    move-result-object v6

    .line 289
    invoke-virtual {v6}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 290
    .line 291
    .line 292
    move-result-object v6

    .line 293
    invoke-virtual {v6}, Lcom/intsig/office/common/picture/PictureManage;->getPicTempPath()Ljava/lang/String;

    .line 294
    .line 295
    .line 296
    move-result-object v6

    .line 297
    invoke-virtual {v5, v6, p1, v3}, Lcom/intsig/office/fc/hwpf/model/PicturesTable;->extractPicture(Ljava/lang/String;Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;Z)Lcom/intsig/office/fc/hwpf/usermodel/Picture;

    .line 298
    .line 299
    .line 300
    move-result-object v6

    .line 301
    const/high16 v7, 0x447a0000    # 1000.0f

    .line 302
    .line 303
    if-eqz v6, :cond_7

    .line 304
    .line 305
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/usermodel/Picture;->suggestPictureType()Lcom/intsig/office/fc/hwpf/usermodel/PictureType;

    .line 306
    .line 307
    .line 308
    move-result-object v8

    .line 309
    invoke-direct {p0, v8}, Lcom/intsig/office/fc/doc/DOCReader;->isSupportPicture(Lcom/intsig/office/fc/hwpf/usermodel/PictureType;)Z

    .line 310
    .line 311
    .line 312
    move-result v8

    .line 313
    if-eqz v8, :cond_7

    .line 314
    .line 315
    new-instance v0, Lcom/intsig/office/common/shape/PictureShape;

    .line 316
    .line 317
    invoke-direct {v0}, Lcom/intsig/office/common/shape/PictureShape;-><init>()V

    .line 318
    .line 319
    .line 320
    iget-object v3, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 321
    .line 322
    invoke-interface {v3}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 323
    .line 324
    .line 325
    move-result-object v3

    .line 326
    invoke-virtual {v3}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 327
    .line 328
    .line 329
    move-result-object v3

    .line 330
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/usermodel/Picture;->getTempFilePath()Ljava/lang/String;

    .line 331
    .line 332
    .line 333
    move-result-object v5

    .line 334
    invoke-virtual {v3, v5}, Lcom/intsig/office/common/picture/PictureManage;->getPictureIndex(Ljava/lang/String;)I

    .line 335
    .line 336
    .line 337
    move-result v3

    .line 338
    if-gez v3, :cond_6

    .line 339
    .line 340
    new-instance v3, Lcom/intsig/office/common/picture/Picture;

    .line 341
    .line 342
    invoke-direct {v3}, Lcom/intsig/office/common/picture/Picture;-><init>()V

    .line 343
    .line 344
    .line 345
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/usermodel/Picture;->getTempFilePath()Ljava/lang/String;

    .line 346
    .line 347
    .line 348
    move-result-object v5

    .line 349
    invoke-virtual {v3, v5}, Lcom/intsig/office/common/picture/Picture;->setTempFilePath(Ljava/lang/String;)V

    .line 350
    .line 351
    .line 352
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/usermodel/Picture;->suggestPictureType()Lcom/intsig/office/fc/hwpf/usermodel/PictureType;

    .line 353
    .line 354
    .line 355
    move-result-object v5

    .line 356
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/usermodel/PictureType;->getExtension()Ljava/lang/String;

    .line 357
    .line 358
    .line 359
    move-result-object v5

    .line 360
    invoke-virtual {v3, v5}, Lcom/intsig/office/common/picture/Picture;->setPictureType(Ljava/lang/String;)V

    .line 361
    .line 362
    .line 363
    iget-object v5, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 364
    .line 365
    invoke-interface {v5}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 366
    .line 367
    .line 368
    move-result-object v5

    .line 369
    invoke-virtual {v5}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 370
    .line 371
    .line 372
    move-result-object v5

    .line 373
    invoke-virtual {v5, v3}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/common/picture/Picture;)I

    .line 374
    .line 375
    .line 376
    move-result v3

    .line 377
    :cond_6
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/shape/PictureShape;->setPictureIndex(I)V

    .line 378
    .line 379
    .line 380
    new-instance v3, Lcom/intsig/office/java/awt/Rectangle;

    .line 381
    .line 382
    invoke-direct {v3}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 383
    .line 384
    .line 385
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/usermodel/Picture;->getDxaGoal()I

    .line 386
    .line 387
    .line 388
    move-result v5

    .line 389
    int-to-float v5, v5

    .line 390
    mul-float v5, v5, v4

    .line 391
    .line 392
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/usermodel/Picture;->getHorizontalScalingFactor()I

    .line 393
    .line 394
    .line 395
    move-result v8

    .line 396
    int-to-float v8, v8

    .line 397
    mul-float v5, v5, v8

    .line 398
    .line 399
    div-float/2addr v5, v7

    .line 400
    float-to-int v5, v5

    .line 401
    iput v5, v3, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 402
    .line 403
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/usermodel/Picture;->getDyaGoal()I

    .line 404
    .line 405
    .line 406
    move-result v5

    .line 407
    int-to-float v5, v5

    .line 408
    mul-float v5, v5, v4

    .line 409
    .line 410
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/usermodel/Picture;->getVerticalScalingFactor()I

    .line 411
    .line 412
    .line 413
    move-result v4

    .line 414
    int-to-float v4, v4

    .line 415
    mul-float v5, v5, v4

    .line 416
    .line 417
    div-float/2addr v5, v7

    .line 418
    float-to-int v4, v5

    .line 419
    iput v4, v3, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 420
    .line 421
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 422
    .line 423
    .line 424
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->getZoomX()S

    .line 425
    .line 426
    .line 427
    move-result v3

    .line 428
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/shape/PictureShape;->setZoomX(S)V

    .line 429
    .line 430
    .line 431
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->getZoomY()S

    .line 432
    .line 433
    .line 434
    move-result v3

    .line 435
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/shape/PictureShape;->setZoomY(S)V

    .line 436
    .line 437
    .line 438
    invoke-static {v6}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;->getPictureEffectInfor(Lcom/intsig/office/fc/hwpf/usermodel/Picture;)Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 439
    .line 440
    .line 441
    move-result-object v3

    .line 442
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/shape/PictureShape;->setPictureEffectInfor(Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;)V

    .line 443
    .line 444
    .line 445
    new-instance v3, Lcom/intsig/office/common/shape/WPPictureShape;

    .line 446
    .line 447
    invoke-direct {v3}, Lcom/intsig/office/common/shape/WPPictureShape;-><init>()V

    .line 448
    .line 449
    .line 450
    invoke-virtual {v3, v0}, Lcom/intsig/office/common/shape/WPPictureShape;->setPictureShape(Lcom/intsig/office/common/shape/PictureShape;)V

    .line 451
    .line 452
    .line 453
    invoke-virtual {v3, v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 454
    .line 455
    .line 456
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 457
    .line 458
    .line 459
    move-result-object v0

    .line 460
    invoke-interface {p2}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 461
    .line 462
    .line 463
    move-result-object v2

    .line 464
    iget-object v4, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 465
    .line 466
    invoke-interface {v4}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 467
    .line 468
    .line 469
    move-result-object v4

    .line 470
    invoke-virtual {v4}, Lcom/intsig/office/system/SysKit;->getWPShapeManage()Lcom/intsig/office/wp/control/WPShapeManage;

    .line 471
    .line 472
    .line 473
    move-result-object v4

    .line 474
    invoke-virtual {v4, v3}, Lcom/intsig/office/wp/control/WPShapeManage;->addShape(Lcom/intsig/office/common/shape/AbstractShape;)I

    .line 475
    .line 476
    .line 477
    move-result v3

    .line 478
    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setShapeID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 479
    .line 480
    .line 481
    return v1

    .line 482
    :cond_7
    invoke-virtual {v5, p1}, Lcom/intsig/office/fc/hwpf/model/PicturesTable;->extracInlineWordArt(Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;)Lcom/intsig/office/fc/hwpf/usermodel/InlineWordArt;

    .line 483
    .line 484
    .line 485
    move-result-object v0

    .line 486
    if-eqz v0, :cond_8

    .line 487
    .line 488
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/InlineWordArt;->getInlineWordArt()Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;

    .line 489
    .line 490
    .line 491
    move-result-object v5

    .line 492
    if-eqz v5, :cond_8

    .line 493
    .line 494
    new-instance v3, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 495
    .line 496
    invoke-direct {v3}, Lcom/intsig/office/common/shape/WPAutoShape;-><init>()V

    .line 497
    .line 498
    .line 499
    new-instance v5, Lcom/intsig/office/java/awt/Rectangle;

    .line 500
    .line 501
    invoke-direct {v5}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 502
    .line 503
    .line 504
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/InlineWordArt;->getDxaGoal()I

    .line 505
    .line 506
    .line 507
    move-result v6

    .line 508
    int-to-float v6, v6

    .line 509
    mul-float v6, v6, v4

    .line 510
    .line 511
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/InlineWordArt;->getHorizontalScalingFactor()I

    .line 512
    .line 513
    .line 514
    move-result v8

    .line 515
    int-to-float v8, v8

    .line 516
    mul-float v6, v6, v8

    .line 517
    .line 518
    div-float/2addr v6, v7

    .line 519
    float-to-int v6, v6

    .line 520
    iput v6, v5, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 521
    .line 522
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/InlineWordArt;->getDyaGoal()I

    .line 523
    .line 524
    .line 525
    move-result v6

    .line 526
    int-to-float v6, v6

    .line 527
    mul-float v6, v6, v4

    .line 528
    .line 529
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/InlineWordArt;->getVerticalScalingFactor()I

    .line 530
    .line 531
    .line 532
    move-result v4

    .line 533
    int-to-float v4, v4

    .line 534
    mul-float v6, v6, v4

    .line 535
    .line 536
    div-float/2addr v6, v7

    .line 537
    float-to-int v4, v6

    .line 538
    iput v4, v5, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 539
    .line 540
    invoke-virtual {v3, v5}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 541
    .line 542
    .line 543
    invoke-virtual {v3, v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 544
    .line 545
    .line 546
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/InlineWordArt;->getInlineWordArt()Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;

    .line 547
    .line 548
    .line 549
    move-result-object v0

    .line 550
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 551
    .line 552
    .line 553
    move-result-object v0

    .line 554
    invoke-direct {p0, v0, v3}, Lcom/intsig/office/fc/doc/DOCReader;->processWordArtTextbox(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/common/shape/WPAutoShape;)V

    .line 555
    .line 556
    .line 557
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 558
    .line 559
    .line 560
    move-result-object v0

    .line 561
    invoke-interface {p2}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 562
    .line 563
    .line 564
    move-result-object v2

    .line 565
    iget-object v4, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 566
    .line 567
    invoke-interface {v4}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 568
    .line 569
    .line 570
    move-result-object v4

    .line 571
    invoke-virtual {v4}, Lcom/intsig/office/system/SysKit;->getWPShapeManage()Lcom/intsig/office/wp/control/WPShapeManage;

    .line 572
    .line 573
    .line 574
    move-result-object v4

    .line 575
    invoke-virtual {v4, v3}, Lcom/intsig/office/wp/control/WPShapeManage;->addShape(Lcom/intsig/office/common/shape/AbstractShape;)I

    .line 576
    .line 577
    .line 578
    move-result v3

    .line 579
    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setShapeID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 580
    .line 581
    .line 582
    return v1

    .line 583
    :cond_8
    return v3
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
.end method

.method private processSimpleTextBox(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/fc/hwpf/usermodel/Section;)V
    .locals 10

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/doc/DOCReader;->getTextboxId(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    add-int/lit8 v0, v0, -0x1

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCReader;->poiDoc:Lcom/intsig/office/fc/hwpf/HWPFDocument;

    .line 8
    .line 9
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hwpf/HWPFDocument;->getTextboxStart(I)I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    iget-object v2, p0, Lcom/intsig/office/fc/doc/DOCReader;->poiDoc:Lcom/intsig/office/fc/hwpf/HWPFDocument;

    .line 14
    .line 15
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/hwpf/HWPFDocument;->getTextboxEnd(I)I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    iget-wide v2, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 20
    .line 21
    iget-wide v4, p0, Lcom/intsig/office/fc/doc/DOCReader;->textboxIndex:J

    .line 22
    .line 23
    const/16 v6, 0x20

    .line 24
    .line 25
    shl-long v6, v4, v6

    .line 26
    .line 27
    const-wide/high16 v8, 0x5000000000000000L    # 2.315841784746324E77

    .line 28
    .line 29
    add-long/2addr v6, v8

    .line 30
    iput-wide v6, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 31
    .line 32
    long-to-int v5, v4

    .line 33
    invoke-virtual {p2, v5}, Lcom/intsig/office/common/shape/WPAbstractShape;->setElementIndex(I)V

    .line 34
    .line 35
    .line 36
    new-instance v4, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 37
    .line 38
    invoke-direct {v4}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    .line 39
    .line 40
    .line 41
    iget-wide v5, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 42
    .line 43
    invoke-virtual {v4, v5, v6}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 44
    .line 45
    .line 46
    iget-object v5, p0, Lcom/intsig/office/fc/doc/DOCReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 47
    .line 48
    iget-wide v6, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 49
    .line 50
    invoke-virtual {v5, v4, v6, v7}, Lcom/intsig/office/wp/model/WPDocument;->appendElement(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 54
    .line 55
    .line 56
    move-result-object v5

    .line 57
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 58
    .line 59
    .line 60
    move-result-object v6

    .line 61
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 62
    .line 63
    .line 64
    move-result-object v7

    .line 65
    iget v7, v7, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 66
    .line 67
    int-to-float v7, v7

    .line 68
    const/high16 v8, 0x41700000    # 15.0f

    .line 69
    .line 70
    mul-float v7, v7, v8

    .line 71
    .line 72
    float-to-int v7, v7

    .line 73
    invoke-virtual {v6, v5, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 74
    .line 75
    .line 76
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 77
    .line 78
    .line 79
    move-result-object v6

    .line 80
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 81
    .line 82
    .line 83
    move-result-object v7

    .line 84
    iget v7, v7, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 85
    .line 86
    int-to-float v7, v7

    .line 87
    mul-float v7, v7, v8

    .line 88
    .line 89
    float-to-int v7, v7

    .line 90
    invoke-virtual {v6, v5, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 91
    .line 92
    .line 93
    invoke-virtual {p3}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getGridType()I

    .line 94
    .line 95
    .line 96
    move-result v6

    .line 97
    if-eqz v6, :cond_0

    .line 98
    .line 99
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 100
    .line 101
    .line 102
    move-result-object v6

    .line 103
    invoke-virtual {p3}, Lcom/intsig/office/fc/hwpf/usermodel/Section;->getLinePitch()I

    .line 104
    .line 105
    .line 106
    move-result v7

    .line 107
    invoke-virtual {v6, v5, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageLinePitch(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 108
    .line 109
    .line 110
    :cond_0
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 111
    .line 112
    .line 113
    move-result-object v6

    .line 114
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getTextboxMarginTop(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F

    .line 115
    .line 116
    .line 117
    move-result v7

    .line 118
    mul-float v7, v7, v8

    .line 119
    .line 120
    float-to-int v7, v7

    .line 121
    invoke-virtual {v6, v5, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 122
    .line 123
    .line 124
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 125
    .line 126
    .line 127
    move-result-object v6

    .line 128
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getTextboxMarginBottom(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F

    .line 129
    .line 130
    .line 131
    move-result v7

    .line 132
    mul-float v7, v7, v8

    .line 133
    .line 134
    float-to-int v7, v7

    .line 135
    invoke-virtual {v6, v5, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 136
    .line 137
    .line 138
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 139
    .line 140
    .line 141
    move-result-object v6

    .line 142
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getTextboxMarginLeft(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F

    .line 143
    .line 144
    .line 145
    move-result v7

    .line 146
    mul-float v7, v7, v8

    .line 147
    .line 148
    float-to-int v7, v7

    .line 149
    invoke-virtual {v6, v5, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 150
    .line 151
    .line 152
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 153
    .line 154
    .line 155
    move-result-object v6

    .line 156
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getTextboxMarginRight(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F

    .line 157
    .line 158
    .line 159
    move-result v7

    .line 160
    mul-float v7, v7, v8

    .line 161
    .line 162
    float-to-int v7, v7

    .line 163
    invoke-virtual {v6, v5, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 164
    .line 165
    .line 166
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 167
    .line 168
    .line 169
    move-result-object v6

    .line 170
    const/4 v7, 0x0

    .line 171
    invoke-virtual {v6, v5, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 172
    .line 173
    .line 174
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->isTextboxWrapLine(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z

    .line 175
    .line 176
    .line 177
    move-result p1

    .line 178
    invoke-virtual {p2, p1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setTextWrapLine(Z)V

    .line 179
    .line 180
    .line 181
    iget-wide v5, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 182
    .line 183
    invoke-virtual {v4, v5, v6}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 184
    .line 185
    .line 186
    invoke-virtual {p3}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->numParagraphs()I

    .line 187
    .line 188
    .line 189
    move-result p1

    .line 190
    const/4 v5, 0x0

    .line 191
    :goto_0
    if-ge v7, p1, :cond_3

    .line 192
    .line 193
    iget-boolean v6, p0, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 194
    .line 195
    if-nez v6, :cond_3

    .line 196
    .line 197
    invoke-virtual {p3, v7}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getParagraph(I)Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;

    .line 198
    .line 199
    .line 200
    move-result-object v6

    .line 201
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->text()Ljava/lang/String;

    .line 202
    .line 203
    .line 204
    move-result-object v8

    .line 205
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    .line 206
    .line 207
    .line 208
    move-result v8

    .line 209
    add-int/2addr v5, v8

    .line 210
    if-le v5, v1, :cond_2

    .line 211
    .line 212
    if-gt v5, v0, :cond_2

    .line 213
    .line 214
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->isInTable()Z

    .line 215
    .line 216
    .line 217
    move-result v8

    .line 218
    if-eqz v8, :cond_1

    .line 219
    .line 220
    invoke-virtual {p3, v6}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getTable(Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;)Lcom/intsig/office/fc/hwpf/usermodel/Table;

    .line 221
    .line 222
    .line 223
    move-result-object v6

    .line 224
    invoke-direct {p0, v6}, Lcom/intsig/office/fc/doc/DOCReader;->processTable(Lcom/intsig/office/fc/hwpf/usermodel/Table;)V

    .line 225
    .line 226
    .line 227
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->numParagraphs()I

    .line 228
    .line 229
    .line 230
    move-result v6

    .line 231
    add-int/lit8 v6, v6, -0x1

    .line 232
    .line 233
    add-int/2addr v7, v6

    .line 234
    goto :goto_1

    .line 235
    :cond_1
    invoke-virtual {p3, v7}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getParagraph(I)Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;

    .line 236
    .line 237
    .line 238
    move-result-object v6

    .line 239
    invoke-direct {p0, v6}, Lcom/intsig/office/fc/doc/DOCReader;->processParagraph(Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;)V

    .line 240
    .line 241
    .line 242
    :cond_2
    :goto_1
    add-int/lit8 v7, v7, 0x1

    .line 243
    .line 244
    goto :goto_0

    .line 245
    :cond_3
    iget-wide v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->textboxIndex:J

    .line 246
    .line 247
    long-to-int p1, v0

    .line 248
    invoke-virtual {p2, p1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setElementIndex(I)V

    .line 249
    .line 250
    .line 251
    iget-wide p1, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 252
    .line 253
    invoke-virtual {v4, p1, p2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 254
    .line 255
    .line 256
    iget-wide p1, p0, Lcom/intsig/office/fc/doc/DOCReader;->textboxIndex:J

    .line 257
    .line 258
    const-wide/16 v0, 0x1

    .line 259
    .line 260
    add-long/2addr p1, v0

    .line 261
    iput-wide p1, p0, Lcom/intsig/office/fc/doc/DOCReader;->textboxIndex:J

    .line 262
    .line 263
    iput-wide v2, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 264
    .line 265
    return-void
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private processTable(Lcom/intsig/office/fc/hwpf/usermodel/Table;)V
    .locals 19

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/office/wp/model/TableElement;

    .line 4
    .line 5
    invoke-direct {v1}, Lcom/intsig/office/wp/model/TableElement;-><init>()V

    .line 6
    .line 7
    .line 8
    iget-wide v2, v0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 9
    .line 10
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 11
    .line 12
    .line 13
    new-instance v2, Ljava/util/Vector;

    .line 14
    .line 15
    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 16
    .line 17
    .line 18
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hwpf/usermodel/Table;->numRows()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    const/4 v5, 0x0

    .line 23
    :goto_0
    if-ge v5, v3, :cond_6

    .line 24
    .line 25
    move-object/from16 v6, p1

    .line 26
    .line 27
    invoke-virtual {v6, v5}, Lcom/intsig/office/fc/hwpf/usermodel/Table;->getRow(I)Lcom/intsig/office/fc/hwpf/usermodel/TableRow;

    .line 28
    .line 29
    .line 30
    move-result-object v7

    .line 31
    if-nez v5, :cond_0

    .line 32
    .line 33
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 34
    .line 35
    .line 36
    move-result-object v8

    .line 37
    invoke-direct {v0, v7, v8}, Lcom/intsig/office/fc/doc/DOCReader;->processTableAttribute(Lcom/intsig/office/fc/hwpf/usermodel/TableRow;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 38
    .line 39
    .line 40
    :cond_0
    new-instance v8, Lcom/intsig/office/wp/model/RowElement;

    .line 41
    .line 42
    invoke-direct {v8}, Lcom/intsig/office/wp/model/RowElement;-><init>()V

    .line 43
    .line 44
    .line 45
    iget-wide v9, v0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 46
    .line 47
    invoke-virtual {v8, v9, v10}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 51
    .line 52
    .line 53
    move-result-object v9

    .line 54
    invoke-direct {v0, v7, v9}, Lcom/intsig/office/fc/doc/DOCReader;->processRowAttribute(Lcom/intsig/office/fc/hwpf/usermodel/TableRow;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v7}, Lcom/intsig/office/fc/hwpf/usermodel/TableRow;->numCells()I

    .line 58
    .line 59
    .line 60
    move-result v9

    .line 61
    const/4 v10, 0x0

    .line 62
    const/4 v11, 0x0

    .line 63
    :goto_1
    if-ge v10, v9, :cond_4

    .line 64
    .line 65
    invoke-virtual {v7, v10}, Lcom/intsig/office/fc/hwpf/usermodel/TableRow;->getCell(I)Lcom/intsig/office/fc/hwpf/usermodel/TableCell;

    .line 66
    .line 67
    .line 68
    move-result-object v12

    .line 69
    invoke-virtual {v12}, Lcom/intsig/office/fc/hwpf/usermodel/TableCell;->isBackward()Z

    .line 70
    .line 71
    .line 72
    new-instance v13, Lcom/intsig/office/wp/model/CellElement;

    .line 73
    .line 74
    invoke-direct {v13}, Lcom/intsig/office/wp/model/CellElement;-><init>()V

    .line 75
    .line 76
    .line 77
    iget-wide v14, v0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 78
    .line 79
    invoke-virtual {v13, v14, v15}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 80
    .line 81
    .line 82
    invoke-virtual {v13}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 83
    .line 84
    .line 85
    move-result-object v14

    .line 86
    invoke-direct {v0, v12, v14}, Lcom/intsig/office/fc/doc/DOCReader;->processCellAttribute(Lcom/intsig/office/fc/hwpf/usermodel/TableCell;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {v12}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->numParagraphs()I

    .line 90
    .line 91
    .line 92
    move-result v14

    .line 93
    const/4 v15, 0x0

    .line 94
    :goto_2
    if-ge v15, v14, :cond_1

    .line 95
    .line 96
    invoke-virtual {v12, v15}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getParagraph(I)Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;

    .line 97
    .line 98
    .line 99
    move-result-object v4

    .line 100
    invoke-direct {v0, v4}, Lcom/intsig/office/fc/doc/DOCReader;->processParagraph(Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;)V

    .line 101
    .line 102
    .line 103
    add-int/lit8 v15, v15, 0x1

    .line 104
    .line 105
    goto :goto_2

    .line 106
    :cond_1
    iget-wide v14, v0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 107
    .line 108
    invoke-virtual {v13, v14, v15}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 109
    .line 110
    .line 111
    iget-wide v14, v0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 112
    .line 113
    invoke-virtual {v13}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 114
    .line 115
    .line 116
    move-result-wide v16

    .line 117
    cmp-long v4, v14, v16

    .line 118
    .line 119
    if-lez v4, :cond_2

    .line 120
    .line 121
    invoke-virtual {v8, v13}, Lcom/intsig/office/wp/model/RowElement;->appendCell(Lcom/intsig/office/wp/model/CellElement;)V

    .line 122
    .line 123
    .line 124
    :cond_2
    invoke-virtual {v12}, Lcom/intsig/office/fc/hwpf/usermodel/TableCell;->getWidth()I

    .line 125
    .line 126
    .line 127
    move-result v4

    .line 128
    add-int/2addr v11, v4

    .line 129
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 130
    .line 131
    .line 132
    move-result-object v4

    .line 133
    invoke-virtual {v2, v4}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    .line 134
    .line 135
    .line 136
    move-result v4

    .line 137
    if-nez v4, :cond_3

    .line 138
    .line 139
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 140
    .line 141
    .line 142
    move-result-object v4

    .line 143
    invoke-virtual {v2, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 144
    .line 145
    .line 146
    :cond_3
    add-int/lit8 v10, v10, 0x1

    .line 147
    .line 148
    goto :goto_1

    .line 149
    :cond_4
    iget-wide v9, v0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 150
    .line 151
    invoke-virtual {v8, v9, v10}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 152
    .line 153
    .line 154
    iget-wide v9, v0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 155
    .line 156
    invoke-virtual {v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 157
    .line 158
    .line 159
    move-result-wide v11

    .line 160
    cmp-long v4, v9, v11

    .line 161
    .line 162
    if-lez v4, :cond_5

    .line 163
    .line 164
    invoke-virtual {v1, v8}, Lcom/intsig/office/wp/model/TableElement;->appendRow(Lcom/intsig/office/wp/model/RowElement;)V

    .line 165
    .line 166
    .line 167
    :cond_5
    add-int/lit8 v5, v5, 0x1

    .line 168
    .line 169
    goto/16 :goto_0

    .line 170
    .line 171
    :cond_6
    iget-wide v3, v0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 172
    .line 173
    invoke-virtual {v1, v3, v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 174
    .line 175
    .line 176
    iget-wide v3, v0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 177
    .line 178
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 179
    .line 180
    .line 181
    move-result-wide v5

    .line 182
    cmp-long v7, v3, v5

    .line 183
    .line 184
    if-lez v7, :cond_b

    .line 185
    .line 186
    iget-object v3, v0, Lcom/intsig/office/fc/doc/DOCReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 187
    .line 188
    iget-wide v4, v0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 189
    .line 190
    invoke-virtual {v3, v1, v4, v5}, Lcom/intsig/office/wp/model/WPDocument;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 191
    .line 192
    .line 193
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    .line 194
    .line 195
    .line 196
    move-result v3

    .line 197
    new-array v4, v3, [I

    .line 198
    .line 199
    const/4 v5, 0x0

    .line 200
    :goto_3
    if-ge v5, v3, :cond_7

    .line 201
    .line 202
    invoke-virtual {v2, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 203
    .line 204
    .line 205
    move-result-object v6

    .line 206
    check-cast v6, Ljava/lang/Integer;

    .line 207
    .line 208
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    .line 209
    .line 210
    .line 211
    move-result v6

    .line 212
    aput v6, v4, v5

    .line 213
    .line 214
    add-int/lit8 v5, v5, 0x1

    .line 215
    .line 216
    goto :goto_3

    .line 217
    :cond_7
    invoke-static {v4}, Lcom/intsig/office/java/util/Arrays;->sort([I)V

    .line 218
    .line 219
    .line 220
    const/4 v2, 0x0

    .line 221
    invoke-virtual {v1, v2}, Lcom/intsig/office/wp/model/TableElement;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 222
    .line 223
    .line 224
    move-result-object v5

    .line 225
    check-cast v5, Lcom/intsig/office/wp/model/RowElement;

    .line 226
    .line 227
    const/4 v6, 0x1

    .line 228
    :goto_4
    if-eqz v5, :cond_b

    .line 229
    .line 230
    invoke-virtual {v5, v2}, Lcom/intsig/office/wp/model/RowElement;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 231
    .line 232
    .line 233
    move-result-object v7

    .line 234
    const/4 v8, 0x0

    .line 235
    const/4 v9, 0x0

    .line 236
    const/4 v10, 0x0

    .line 237
    :goto_5
    if-eqz v7, :cond_a

    .line 238
    .line 239
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 240
    .line 241
    .line 242
    move-result-object v11

    .line 243
    invoke-interface {v7}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 244
    .line 245
    .line 246
    move-result-object v7

    .line 247
    invoke-virtual {v11, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->getTableCellWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 248
    .line 249
    .line 250
    move-result v7

    .line 251
    add-int/2addr v8, v7

    .line 252
    :goto_6
    if-ge v10, v3, :cond_9

    .line 253
    .line 254
    aget v7, v4, v10

    .line 255
    .line 256
    if-le v8, v7, :cond_8

    .line 257
    .line 258
    new-instance v7, Lcom/intsig/office/wp/model/CellElement;

    .line 259
    .line 260
    invoke-direct {v7}, Lcom/intsig/office/wp/model/CellElement;-><init>()V

    .line 261
    .line 262
    .line 263
    add-int/lit8 v9, v9, 0x1

    .line 264
    .line 265
    invoke-virtual {v5, v7, v9}, Lcom/intsig/office/wp/model/RowElement;->insertElementForIndex(Lcom/intsig/office/simpletext/model/IElement;I)V

    .line 266
    .line 267
    .line 268
    add-int/lit8 v10, v10, 0x1

    .line 269
    .line 270
    goto :goto_6

    .line 271
    :cond_8
    add-int/lit8 v10, v10, 0x1

    .line 272
    .line 273
    :cond_9
    add-int/lit8 v9, v9, 0x1

    .line 274
    .line 275
    invoke-virtual {v5, v9}, Lcom/intsig/office/wp/model/RowElement;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 276
    .line 277
    .line 278
    move-result-object v7

    .line 279
    goto :goto_5

    .line 280
    :cond_a
    add-int/lit8 v5, v6, 0x1

    .line 281
    .line 282
    invoke-virtual {v1, v6}, Lcom/intsig/office/wp/model/TableElement;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 283
    .line 284
    .line 285
    move-result-object v6

    .line 286
    check-cast v6, Lcom/intsig/office/wp/model/RowElement;

    .line 287
    .line 288
    move-object/from16 v18, v6

    .line 289
    .line 290
    move v6, v5

    .line 291
    move-object/from16 v5, v18

    .line 292
    .line 293
    goto :goto_4

    .line 294
    :cond_b
    return-void
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method private processTableAttribute(Lcom/intsig/office/fc/hwpf/usermodel/TableRow;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableRow;->getRowJustification()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableRow;->getRowJustification()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {v0, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 16
    .line 17
    .line 18
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableRow;->getTableIndent()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableRow;->getTableIndent()I

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 33
    .line 34
    .line 35
    :cond_1
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private processTextbox(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/fc/hwpf/usermodel/Section;)V
    .locals 1

    .line 1
    if-nez p3, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/doc/DOCReader;->getTextboxId(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)S

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    add-int/lit8 v0, v0, -0x1

    .line 9
    .line 10
    if-ltz v0, :cond_1

    .line 11
    .line 12
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/fc/doc/DOCReader;->processSimpleTextBox(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/fc/hwpf/usermodel/Section;)V

    .line 13
    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/doc/DOCReader;->processWordArtTextbox(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/common/shape/WPAutoShape;)V

    .line 17
    .line 18
    .line 19
    :goto_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private processWordArtTextbox(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/common/shape/WPAutoShape;)V
    .locals 13

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getUnicodeGeoText(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-lez v1, :cond_2

    .line 12
    .line 13
    iget-wide v1, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 14
    .line 15
    iget-wide v3, p0, Lcom/intsig/office/fc/doc/DOCReader;->textboxIndex:J

    .line 16
    .line 17
    const/16 v5, 0x20

    .line 18
    .line 19
    shl-long v5, v3, v5

    .line 20
    .line 21
    const-wide/high16 v7, 0x5000000000000000L    # 2.315841784746324E77

    .line 22
    .line 23
    add-long/2addr v5, v7

    .line 24
    iput-wide v5, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 25
    .line 26
    long-to-int v4, v3

    .line 27
    invoke-virtual {p2, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setElementIndex(I)V

    .line 28
    .line 29
    .line 30
    new-instance v3, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 31
    .line 32
    invoke-direct {v3}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    .line 33
    .line 34
    .line 35
    iget-wide v4, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 36
    .line 37
    invoke-virtual {v3, v4, v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 38
    .line 39
    .line 40
    iget-object v4, p0, Lcom/intsig/office/fc/doc/DOCReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 41
    .line 42
    iget-wide v5, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 43
    .line 44
    invoke-virtual {v4, v3, v5, v6}, Lcom/intsig/office/wp/model/WPDocument;->appendElement(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 48
    .line 49
    .line 50
    move-result-object v4

    .line 51
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 52
    .line 53
    .line 54
    move-result-object v5

    .line 55
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 56
    .line 57
    .line 58
    move-result-object v6

    .line 59
    iget v6, v6, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 60
    .line 61
    int-to-float v6, v6

    .line 62
    const/high16 v7, 0x41700000    # 15.0f

    .line 63
    .line 64
    mul-float v6, v6, v7

    .line 65
    .line 66
    float-to-int v6, v6

    .line 67
    invoke-virtual {v5, v4, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 68
    .line 69
    .line 70
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 71
    .line 72
    .line 73
    move-result-object v5

    .line 74
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 75
    .line 76
    .line 77
    move-result-object v6

    .line 78
    iget v6, v6, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 79
    .line 80
    int-to-float v6, v6

    .line 81
    mul-float v6, v6, v7

    .line 82
    .line 83
    float-to-int v6, v6

    .line 84
    invoke-virtual {v5, v4, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 85
    .line 86
    .line 87
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 88
    .line 89
    .line 90
    move-result-object v5

    .line 91
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getTextboxMarginTop(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F

    .line 92
    .line 93
    .line 94
    move-result v6

    .line 95
    mul-float v6, v6, v7

    .line 96
    .line 97
    float-to-int v6, v6

    .line 98
    invoke-virtual {v5, v4, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 99
    .line 100
    .line 101
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 102
    .line 103
    .line 104
    move-result-object v5

    .line 105
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getTextboxMarginBottom(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F

    .line 106
    .line 107
    .line 108
    move-result v6

    .line 109
    mul-float v6, v6, v7

    .line 110
    .line 111
    float-to-int v6, v6

    .line 112
    invoke-virtual {v5, v4, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 113
    .line 114
    .line 115
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 116
    .line 117
    .line 118
    move-result-object v5

    .line 119
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getTextboxMarginLeft(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F

    .line 120
    .line 121
    .line 122
    move-result v6

    .line 123
    mul-float v6, v6, v7

    .line 124
    .line 125
    float-to-int v6, v6

    .line 126
    invoke-virtual {v5, v4, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 127
    .line 128
    .line 129
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 130
    .line 131
    .line 132
    move-result-object v5

    .line 133
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getTextboxMarginRight(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F

    .line 134
    .line 135
    .line 136
    move-result v6

    .line 137
    mul-float v6, v6, v7

    .line 138
    .line 139
    float-to-int v6, v6

    .line 140
    invoke-virtual {v5, v4, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 141
    .line 142
    .line 143
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 144
    .line 145
    .line 146
    move-result-object v5

    .line 147
    const/4 v6, 0x0

    .line 148
    invoke-virtual {v5, v4, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 149
    .line 150
    .line 151
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->isTextboxWrapLine(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z

    .line 152
    .line 153
    .line 154
    move-result v4

    .line 155
    invoke-virtual {p2, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setTextWrapLine(Z)V

    .line 156
    .line 157
    .line 158
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 159
    .line 160
    .line 161
    move-result-object v4

    .line 162
    iget v4, v4, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 163
    .line 164
    int-to-float v4, v4

    .line 165
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getTextboxMarginLeft(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F

    .line 166
    .line 167
    .line 168
    move-result v5

    .line 169
    sub-float/2addr v4, v5

    .line 170
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getTextboxMarginRight(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F

    .line 171
    .line 172
    .line 173
    move-result v5

    .line 174
    sub-float/2addr v4, v5

    .line 175
    float-to-int v4, v4

    .line 176
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 177
    .line 178
    .line 179
    move-result-object v5

    .line 180
    iget v5, v5, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 181
    .line 182
    int-to-float v5, v5

    .line 183
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getTextboxMarginTop(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F

    .line 184
    .line 185
    .line 186
    move-result v6

    .line 187
    sub-float/2addr v5, v6

    .line 188
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getTextboxMarginBottom(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)F

    .line 189
    .line 190
    .line 191
    move-result v6

    .line 192
    sub-float/2addr v5, v6

    .line 193
    float-to-int v5, v5

    .line 194
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    .line 195
    .line 196
    .line 197
    move-result-object v6

    .line 198
    invoke-virtual {v6}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    .line 199
    .line 200
    .line 201
    move-result-object v6

    .line 202
    const/16 v7, 0xc

    .line 203
    .line 204
    int-to-float v8, v7

    .line 205
    invoke-virtual {v6, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 206
    .line 207
    .line 208
    invoke-virtual {v6}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 209
    .line 210
    .line 211
    move-result-object v8

    .line 212
    :goto_0
    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 213
    .line 214
    .line 215
    move-result v9

    .line 216
    float-to-int v9, v9

    .line 217
    if-ge v9, v4, :cond_0

    .line 218
    .line 219
    iget v9, v8, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 220
    .line 221
    iget v8, v8, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 222
    .line 223
    sub-float/2addr v9, v8

    .line 224
    float-to-double v8, v9

    .line 225
    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    .line 226
    .line 227
    .line 228
    move-result-wide v8

    .line 229
    double-to-int v8, v8

    .line 230
    if-ge v8, v5, :cond_0

    .line 231
    .line 232
    add-int/lit8 v7, v7, 0x1

    .line 233
    .line 234
    int-to-float v8, v7

    .line 235
    invoke-virtual {v6, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 236
    .line 237
    .line 238
    invoke-virtual {v6}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 239
    .line 240
    .line 241
    move-result-object v8

    .line 242
    goto :goto_0

    .line 243
    :cond_0
    iget-wide v4, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 244
    .line 245
    invoke-virtual {v3, v4, v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 246
    .line 247
    .line 248
    new-instance v4, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 249
    .line 250
    invoke-direct {v4}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 251
    .line 252
    .line 253
    iget-wide v5, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 254
    .line 255
    invoke-virtual {v4, v5, v6}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 256
    .line 257
    .line 258
    iget-wide v5, p0, Lcom/intsig/office/fc/doc/DOCReader;->docRealOffset:J

    .line 259
    .line 260
    new-instance v8, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 261
    .line 262
    invoke-direct {v8, v0}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 263
    .line 264
    .line 265
    invoke-virtual {v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 266
    .line 267
    .line 268
    move-result-object v9

    .line 269
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 270
    .line 271
    .line 272
    move-result-object v10

    .line 273
    add-int/lit8 v7, v7, -0x1

    .line 274
    .line 275
    int-to-float v7, v7

    .line 276
    const/high16 v11, 0x3f400000    # 0.75f

    .line 277
    .line 278
    mul-float v7, v7, v11

    .line 279
    .line 280
    float-to-int v7, v7

    .line 281
    invoke-virtual {v10, v9, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontSize(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 282
    .line 283
    .line 284
    const/4 v7, 0x0

    .line 285
    const/4 v10, 0x2

    .line 286
    invoke-static {p1, v7, v10}, Lcom/intsig/office/fc/ShapeKit;->getForegroundColor(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/lang/Object;I)Lcom/intsig/office/java/awt/Color;

    .line 287
    .line 288
    .line 289
    move-result-object p1

    .line 290
    if-eqz p1, :cond_1

    .line 291
    .line 292
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 293
    .line 294
    .line 295
    move-result-object v7

    .line 296
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 297
    .line 298
    .line 299
    move-result p1

    .line 300
    invoke-virtual {v7, v9, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 301
    .line 302
    .line 303
    :cond_1
    iget-wide v9, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 304
    .line 305
    invoke-virtual {v8, v9, v10}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 306
    .line 307
    .line 308
    iget-wide v9, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 309
    .line 310
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 311
    .line 312
    .line 313
    move-result p1

    .line 314
    int-to-long v11, p1

    .line 315
    add-long/2addr v9, v11

    .line 316
    iput-wide v9, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 317
    .line 318
    invoke-virtual {v8, v9, v10}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 319
    .line 320
    .line 321
    invoke-virtual {v4, v8}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 322
    .line 323
    .line 324
    iget-wide v7, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 325
    .line 326
    invoke-virtual {v4, v7, v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 327
    .line 328
    .line 329
    iget-object p1, p0, Lcom/intsig/office/fc/doc/DOCReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 330
    .line 331
    iget-wide v7, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 332
    .line 333
    invoke-virtual {p1, v4, v7, v8}, Lcom/intsig/office/wp/model/WPDocument;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 334
    .line 335
    .line 336
    iget-wide v7, p0, Lcom/intsig/office/fc/doc/DOCReader;->docRealOffset:J

    .line 337
    .line 338
    invoke-direct {p0, v5, v6, v7, v8}, Lcom/intsig/office/fc/doc/DOCReader;->adjustBookmarkOffset(JJ)V

    .line 339
    .line 340
    .line 341
    iget-wide v4, p0, Lcom/intsig/office/fc/doc/DOCReader;->textboxIndex:J

    .line 342
    .line 343
    long-to-int p1, v4

    .line 344
    invoke-virtual {p2, p1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setElementIndex(I)V

    .line 345
    .line 346
    .line 347
    iget-wide p1, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 348
    .line 349
    invoke-virtual {v3, p1, p2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 350
    .line 351
    .line 352
    iget-wide p1, p0, Lcom/intsig/office/fc/doc/DOCReader;->textboxIndex:J

    .line 353
    .line 354
    const-wide/16 v3, 0x1

    .line 355
    .line 356
    add-long/2addr p1, v3

    .line 357
    iput-wide p1, p0, Lcom/intsig/office/fc/doc/DOCReader;->textboxIndex:J

    .line 358
    .line 359
    iput-wide v1, p0, Lcom/intsig/office/fc/doc/DOCReader;->offset:J

    .line 360
    .line 361
    :cond_2
    return-void
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/system/AbstractReader;->isReaderFinish()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->filePath:Ljava/lang/String;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->poiDoc:Lcom/intsig/office/fc/hwpf/HWPFDocument;

    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->hyperlinkAddress:Ljava/lang/String;

    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCReader;->bms:Ljava/util/List;

    .line 19
    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 23
    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->bms:Ljava/util/List;

    .line 26
    .line 27
    :cond_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getModel()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    new-instance v0, Lcom/intsig/office/wp/model/WPDocument;

    .line 7
    .line 8
    invoke-direct {v0}, Lcom/intsig/office/wp/model/WPDocument;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/office/fc/doc/DOCReader;->processDoc()V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
.end method

.method public searchContent(Ljava/io/File;Ljava/lang/String;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/HWPFDocument;

    .line 2
    .line 3
    new-instance v1, Ljava/io/FileInputStream;

    .line 4
    .line 5
    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hwpf/HWPFDocument;-><init>(Ljava/io/InputStream;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/HWPFDocument;->getRange()Lcom/intsig/office/fc/hwpf/usermodel/Range;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    const/4 v2, 0x0

    .line 22
    const/4 v3, 0x0

    .line 23
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->numSections()I

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    if-ge v2, v4, :cond_3

    .line 28
    .line 29
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getSection(I)Lcom/intsig/office/fc/hwpf/usermodel/Section;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    const/4 v5, 0x0

    .line 34
    :goto_1
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->numParagraphs()I

    .line 35
    .line 36
    .line 37
    move-result v6

    .line 38
    if-ge v5, v6, :cond_2

    .line 39
    .line 40
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getParagraph(I)Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;

    .line 41
    .line 42
    .line 43
    move-result-object v6

    .line 44
    const/4 v7, 0x0

    .line 45
    :goto_2
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->numCharacterRuns()I

    .line 46
    .line 47
    .line 48
    move-result v8

    .line 49
    if-ge v7, v8, :cond_0

    .line 50
    .line 51
    invoke-virtual {v6, v7}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getCharacterRun(I)Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;

    .line 52
    .line 53
    .line 54
    move-result-object v8

    .line 55
    invoke-virtual {v8}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v8

    .line 59
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    add-int/lit8 v7, v7, 0x1

    .line 63
    .line 64
    goto :goto_2

    .line 65
    :cond_0
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    .line 66
    .line 67
    .line 68
    move-result v6

    .line 69
    if-ltz v6, :cond_1

    .line 70
    .line 71
    const/4 v3, 0x1

    .line 72
    goto :goto_3

    .line 73
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 74
    .line 75
    .line 76
    move-result v6

    .line 77
    invoke-virtual {v0, v1, v6}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    add-int/lit8 v5, v5, 0x1

    .line 81
    .line 82
    goto :goto_1

    .line 83
    :cond_2
    :goto_3
    add-int/lit8 v2, v2, 0x1

    .line 84
    .line 85
    goto :goto_0

    .line 86
    :cond_3
    return v3
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

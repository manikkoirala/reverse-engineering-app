.class public Lcom/intsig/office/fc/doc/DOCXReader;
.super Lcom/intsig/office/system/AbstractReader;
.source "DOCXReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/doc/DOCXReader$DOCXSaxHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DOCXReader"


# instance fields
.field bulletNumbersID:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private filePath:Ljava/lang/String;

.field private hfPart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

.field private isProcessHF:Z

.field private isProcessSectionAttribute:Z

.field private isProcessWatermark:Z

.field private offset:J

.field private packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

.field private relativeType:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/common/shape/IShape;",
            ">;"
        }
    .end annotation
.end field

.field private relativeValue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/intsig/office/common/shape/IShape;",
            "[I>;"
        }
    .end annotation
.end field

.field private secElem:Lcom/intsig/office/simpletext/model/SectionElement;

.field private styleID:I

.field private styleStrID:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private tableGridCol:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private tableStyle:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private textboxIndex:J

.field private themeColor:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private uri:Landroid/net/Uri;

.field private wpdoc:Lcom/intsig/office/wp/model/WPDocument;

.field private zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;


# direct methods
.method public constructor <init>(Lcom/intsig/office/system/IControl;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/system/AbstractReader;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/HashMap;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleStrID:Ljava/util/Map;

    .line 10
    .line 11
    new-instance v0, Ljava/util/HashMap;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->tableStyle:Ljava/util/Map;

    .line 17
    .line 18
    new-instance v0, Ljava/util/HashMap;

    .line 19
    .line 20
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->tableGridCol:Ljava/util/Map;

    .line 24
    .line 25
    new-instance v0, Ljava/util/Hashtable;

    .line 26
    .line 27
    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->bulletNumbersID:Ljava/util/Hashtable;

    .line 31
    .line 32
    new-instance v0, Ljava/util/ArrayList;

    .line 33
    .line 34
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .line 36
    .line 37
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeType:Ljava/util/List;

    .line 38
    .line 39
    new-instance v0, Ljava/util/HashMap;

    .line 40
    .line 41
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 42
    .line 43
    .line 44
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeValue:Ljava/util/Map;

    .line 45
    .line 46
    iput-object p1, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 47
    .line 48
    iput-object p2, p0, Lcom/intsig/office/fc/doc/DOCXReader;->filePath:Ljava/lang/String;

    .line 49
    .line 50
    iput-object p3, p0, Lcom/intsig/office/fc/doc/DOCXReader;->uri:Landroid/net/Uri;

    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method static bridge synthetic O8(Lcom/intsig/office/fc/doc/DOCXReader;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/doc/DOCXReader;->processParagraphs(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic Oo08(Lcom/intsig/office/fc/doc/DOCXReader;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->processPicture(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method static synthetic access$000(Lcom/intsig/office/fc/doc/DOCXReader;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private addPicture(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/shape/PictureShape;
    .locals 3

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    if-eqz p2, :cond_1

    .line 4
    .line 5
    const-string v0, "blipFill"

    .line 6
    .line 7
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    if-eqz p1, :cond_1

    .line 12
    .line 13
    invoke-static {p1}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;->getPictureEffectInfor(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const-string v1, "blip"

    .line 18
    .line 19
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    if-eqz p1, :cond_1

    .line 24
    .line 25
    const-string v1, "embed"

    .line 26
    .line 27
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    if-eqz p1, :cond_1

    .line 32
    .line 33
    iget-boolean v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessHF:Z

    .line 34
    .line 35
    if-eqz v1, :cond_0

    .line 36
    .line 37
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->hfPart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 38
    .line 39
    if-eqz v1, :cond_0

    .line 40
    .line 41
    iget-object v2, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 42
    .line 43
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationship(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    invoke-virtual {v2, p1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    goto :goto_0

    .line 56
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 57
    .line 58
    iget-object v2, p0, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 59
    .line 60
    invoke-virtual {v2, p1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationship(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    :goto_0
    if-eqz p1, :cond_1

    .line 73
    .line 74
    new-instance v1, Lcom/intsig/office/common/shape/PictureShape;

    .line 75
    .line 76
    invoke-direct {v1}, Lcom/intsig/office/common/shape/PictureShape;-><init>()V

    .line 77
    .line 78
    .line 79
    :try_start_0
    iget-object v2, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 80
    .line 81
    invoke-interface {v2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 82
    .line 83
    .line 84
    move-result-object v2

    .line 85
    invoke-virtual {v2}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 86
    .line 87
    .line 88
    move-result-object v2

    .line 89
    invoke-virtual {v2, p1}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)I

    .line 90
    .line 91
    .line 92
    move-result p1

    .line 93
    invoke-virtual {v1, p1}, Lcom/intsig/office/common/shape/PictureShape;->setPictureIndex(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    .line 95
    .line 96
    goto :goto_1

    .line 97
    :catch_0
    move-exception p1

    .line 98
    iget-object v2, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 99
    .line 100
    invoke-interface {v2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 101
    .line 102
    .line 103
    move-result-object v2

    .line 104
    invoke-virtual {v2}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    .line 105
    .line 106
    .line 107
    move-result-object v2

    .line 108
    invoke-virtual {v2, p1}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;)V

    .line 109
    .line 110
    .line 111
    :goto_1
    const/16 p1, 0x3e8

    .line 112
    .line 113
    invoke-virtual {v1, p1}, Lcom/intsig/office/common/shape/PictureShape;->setZoomX(S)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {v1, p1}, Lcom/intsig/office/common/shape/PictureShape;->setZoomY(S)V

    .line 117
    .line 118
    .line 119
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/shape/PictureShape;->setPictureEffectInfor(Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {v1, p2}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 123
    .line 124
    .line 125
    goto :goto_2

    .line 126
    :cond_1
    const/4 v1, 0x0

    .line 127
    :goto_2
    return-object v1
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private addShape(Lcom/intsig/office/common/shape/AbstractShape;Lcom/intsig/office/simpletext/model/ParagraphElement;)V
    .locals 5

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-direct {v0, v1}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-wide v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 16
    .line 17
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 18
    .line 19
    .line 20
    iget-wide v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 21
    .line 22
    const-wide/16 v3, 0x1

    .line 23
    .line 24
    add-long/2addr v1, v3

    .line 25
    iput-wide v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 26
    .line 27
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p2, v0}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 31
    .line 32
    .line 33
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 34
    .line 35
    .line 36
    move-result-object p2

    .line 37
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 42
    .line 43
    invoke-interface {v1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-virtual {v1}, Lcom/intsig/office/system/SysKit;->getWPShapeManage()Lcom/intsig/office/wp/control/WPShapeManage;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    invoke-virtual {v1, p1}, Lcom/intsig/office/wp/control/WPShapeManage;->addShape(Lcom/intsig/office/common/shape/AbstractShape;)I

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    invoke-virtual {p2, v0, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setShapeID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 56
    .line 57
    .line 58
    :cond_0
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private convertedNumberFormat(Ljava/lang/String;)I
    .locals 2

    .line 1
    const-string v0, "decimal"

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    return v1

    .line 11
    :cond_0
    const-string v0, "upperRoman"

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    const/4 p1, 0x1

    .line 20
    return p1

    .line 21
    :cond_1
    const-string v0, "lowerRoman"

    .line 22
    .line 23
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_2

    .line 28
    .line 29
    const/4 p1, 0x2

    .line 30
    return p1

    .line 31
    :cond_2
    const-string v0, "upperLetter"

    .line 32
    .line 33
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_3

    .line 38
    .line 39
    const/4 p1, 0x3

    .line 40
    return p1

    .line 41
    :cond_3
    const-string v0, "lowerLetter"

    .line 42
    .line 43
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-eqz v0, :cond_4

    .line 48
    .line 49
    const/4 p1, 0x4

    .line 50
    return p1

    .line 51
    :cond_4
    const-string v0, "chineseCountingThousand"

    .line 52
    .line 53
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    if-eqz v0, :cond_5

    .line 58
    .line 59
    const/16 p1, 0x27

    .line 60
    .line 61
    return p1

    .line 62
    :cond_5
    const-string v0, "chineseLegalSimplified"

    .line 63
    .line 64
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    if-eqz v0, :cond_6

    .line 69
    .line 70
    const/16 p1, 0x26

    .line 71
    .line 72
    return p1

    .line 73
    :cond_6
    const-string v0, "ideographTraditional"

    .line 74
    .line 75
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 76
    .line 77
    .line 78
    move-result v0

    .line 79
    if-eqz v0, :cond_7

    .line 80
    .line 81
    const/16 p1, 0x1e

    .line 82
    .line 83
    return p1

    .line 84
    :cond_7
    const-string v0, "ideographZodiac"

    .line 85
    .line 86
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    if-eqz v0, :cond_8

    .line 91
    .line 92
    const/16 p1, 0x1f

    .line 93
    .line 94
    return p1

    .line 95
    :cond_8
    const-string v0, "ordinal"

    .line 96
    .line 97
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 98
    .line 99
    .line 100
    move-result v0

    .line 101
    if-eqz v0, :cond_9

    .line 102
    .line 103
    const/4 p1, 0x5

    .line 104
    return p1

    .line 105
    :cond_9
    const-string v0, "cardinalText"

    .line 106
    .line 107
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 108
    .line 109
    .line 110
    move-result v0

    .line 111
    if-eqz v0, :cond_a

    .line 112
    .line 113
    const/4 p1, 0x6

    .line 114
    return p1

    .line 115
    :cond_a
    const-string v0, "ordinalText"

    .line 116
    .line 117
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 118
    .line 119
    .line 120
    move-result v0

    .line 121
    if-eqz v0, :cond_b

    .line 122
    .line 123
    const/4 p1, 0x7

    .line 124
    return p1

    .line 125
    :cond_b
    const-string v0, "decimalZero"

    .line 126
    .line 127
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 128
    .line 129
    .line 130
    move-result p1

    .line 131
    if-eqz p1, :cond_c

    .line 132
    .line 133
    const/16 p1, 0x16

    .line 134
    .line 135
    return p1

    .line 136
    :cond_c
    return v1
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private getAlign(Ljava/lang/String;)B
    .locals 1

    .line 1
    const-string v0, "left"

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    return p1

    .line 11
    :cond_0
    const-string v0, "right"

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    const/4 p1, 0x3

    .line 20
    return p1

    .line 21
    :cond_1
    const-string v0, "top"

    .line 22
    .line 23
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_2

    .line 28
    .line 29
    const/4 p1, 0x4

    .line 30
    return p1

    .line 31
    :cond_2
    const-string v0, "bottom"

    .line 32
    .line 33
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_3

    .line 38
    .line 39
    const/4 p1, 0x5

    .line 40
    return p1

    .line 41
    :cond_3
    const-string v0, "center"

    .line 42
    .line 43
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-eqz v0, :cond_4

    .line 48
    .line 49
    const/4 p1, 0x2

    .line 50
    return p1

    .line 51
    :cond_4
    const-string v0, "inside"

    .line 52
    .line 53
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    if-eqz v0, :cond_5

    .line 58
    .line 59
    const/4 p1, 0x6

    .line 60
    return p1

    .line 61
    :cond_5
    const-string v0, "outside"

    .line 62
    .line 63
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 64
    .line 65
    .line 66
    move-result p1

    .line 67
    if-eqz p1, :cond_6

    .line 68
    .line 69
    const/4 p1, 0x7

    .line 70
    return p1

    .line 71
    :cond_6
    const/4 p1, 0x0

    .line 72
    return p1
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private getArrowExtendPath(Landroid/graphics/Path;Lcom/intsig/office/common/bg/BackgroundAndFill;Lcom/intsig/office/common/borders/Line;ZB)Lcom/intsig/office/common/autoshape/ExtendPath;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 11
    .line 12
    .line 13
    if-nez p2, :cond_0

    .line 14
    .line 15
    if-eqz p4, :cond_4

    .line 16
    .line 17
    :cond_0
    if-eqz p4, :cond_3

    .line 18
    .line 19
    const/4 p1, 0x5

    .line 20
    if-eq p5, p1, :cond_2

    .line 21
    .line 22
    if-eqz p3, :cond_1

    .line 23
    .line 24
    invoke-virtual {p3}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    if-eqz p2, :cond_4

    .line 33
    .line 34
    invoke-virtual {v0, p2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_2
    invoke-virtual {v0, p3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_3
    if-eqz p2, :cond_4

    .line 43
    .line 44
    invoke-virtual {v0, p2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 45
    .line 46
    .line 47
    :cond_4
    :goto_0
    return-object v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method private getArrowLength(Ljava/lang/String;)I
    .locals 1

    .line 1
    const-string v0, "short"

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    return p1

    .line 11
    :cond_0
    const-string v0, "long"

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    if-eqz p1, :cond_1

    .line 18
    .line 19
    const/4 p1, 0x2

    .line 20
    return p1

    .line 21
    :cond_1
    const/4 p1, 0x1

    .line 22
    return p1
    .line 23
    .line 24
.end method

.method private getArrowType(Ljava/lang/String;)B
    .locals 1

    .line 1
    const-string v0, "block"

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    return p1

    .line 11
    :cond_0
    const-string v0, "classic"

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    const/4 p1, 0x2

    .line 20
    return p1

    .line 21
    :cond_1
    const-string v0, "oval"

    .line 22
    .line 23
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_2

    .line 28
    .line 29
    const/4 p1, 0x4

    .line 30
    return p1

    .line 31
    :cond_2
    const-string v0, "diamond"

    .line 32
    .line 33
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_3

    .line 38
    .line 39
    const/4 p1, 0x3

    .line 40
    return p1

    .line 41
    :cond_3
    const-string v0, "open"

    .line 42
    .line 43
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    if-eqz p1, :cond_4

    .line 48
    .line 49
    const/4 p1, 0x5

    .line 50
    return p1

    .line 51
    :cond_4
    const/4 p1, 0x0

    .line 52
    return p1
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private getArrowWidth(Ljava/lang/String;)I
    .locals 1

    .line 1
    const-string v0, "narrow"

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    return p1

    .line 11
    :cond_0
    const-string v0, "wide"

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    if-eqz p1, :cond_1

    .line 18
    .line 19
    const/4 p1, 0x2

    .line 20
    return p1

    .line 21
    :cond_1
    const/4 p1, 0x1

    .line 22
    return p1
    .line 23
    .line 24
.end method

.method private getAutoShapeType(Lcom/intsig/office/fc/dom4j/Element;)I
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "rect"

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const-string v1, "roundrect"

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_1

    .line 22
    .line 23
    const/4 v0, 0x2

    .line 24
    goto :goto_0

    .line 25
    :cond_1
    const-string v1, "oval"

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    if-eqz v1, :cond_2

    .line 32
    .line 33
    const/4 v0, 0x3

    .line 34
    goto :goto_0

    .line 35
    :cond_2
    const-string v1, "curve"

    .line 36
    .line 37
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    if-eqz v1, :cond_3

    .line 42
    .line 43
    const/16 v0, 0xf8

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_3
    const-string v1, "polyline"

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    if-eqz v1, :cond_4

    .line 53
    .line 54
    const/16 v0, 0xf9

    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_4
    const-string v1, "line"

    .line 58
    .line 59
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    if-eqz v0, :cond_5

    .line 64
    .line 65
    const/16 v0, 0xf7

    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_5
    const/4 v0, 0x0

    .line 69
    :goto_0
    const-string v1, "type"

    .line 70
    .line 71
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    if-eqz v2, :cond_6

    .line 76
    .line 77
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object p1

    .line 81
    if-eqz p1, :cond_7

    .line 82
    .line 83
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 84
    .line 85
    .line 86
    move-result v1

    .line 87
    const/16 v2, 0x9

    .line 88
    .line 89
    if-le v1, v2, :cond_7

    .line 90
    .line 91
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 96
    .line 97
    .line 98
    move-result p1

    .line 99
    move v0, p1

    .line 100
    goto :goto_1

    .line 101
    :cond_6
    const-string v1, "path"

    .line 102
    .line 103
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 104
    .line 105
    .line 106
    move-result-object p1

    .line 107
    if-eqz p1, :cond_7

    .line 108
    .line 109
    const/16 v0, 0xe9

    .line 110
    .line 111
    :cond_7
    :goto_1
    return v0
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private getColor(Ljava/lang/String;Z)I
    .locals 5

    .line 1
    const/4 v0, -0x1

    .line 2
    const/high16 v1, -0x1000000

    .line 3
    .line 4
    if-nez p1, :cond_1

    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    return v0

    .line 9
    :cond_0
    return v1

    .line 10
    :cond_1
    const-string v2, " "

    .line 11
    .line 12
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    const/4 v3, 0x0

    .line 17
    if-lez v2, :cond_2

    .line 18
    .line 19
    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    :cond_2
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    const/16 v3, 0x23

    .line 28
    .line 29
    if-ne v2, v3, :cond_5

    .line 30
    .line 31
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    const/4 v4, 0x6

    .line 36
    if-le v2, v4, :cond_3

    .line 37
    .line 38
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    return p1

    .line 43
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    const/4 v4, 0x4

    .line 48
    if-ne v2, v4, :cond_5

    .line 49
    .line 50
    new-instance p2, Ljava/lang/StringBuilder;

    .line 51
    .line 52
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    const/4 v0, 0x1

    .line 59
    :goto_0
    if-ge v0, v4, :cond_4

    .line 60
    .line 61
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    add-int/lit8 v0, v0, 0x1

    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_4
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 83
    .line 84
    .line 85
    move-result p1

    .line 86
    return p1

    .line 87
    :cond_5
    const-string v2, "black"

    .line 88
    .line 89
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 90
    .line 91
    .line 92
    move-result v2

    .line 93
    if-nez v2, :cond_16

    .line 94
    .line 95
    const-string v2, "darken"

    .line 96
    .line 97
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 98
    .line 99
    .line 100
    move-result v2

    .line 101
    if-eqz v2, :cond_6

    .line 102
    .line 103
    goto/16 :goto_1

    .line 104
    .line 105
    :cond_6
    const-string v2, "green"

    .line 106
    .line 107
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 108
    .line 109
    .line 110
    move-result v2

    .line 111
    if-eqz v2, :cond_7

    .line 112
    .line 113
    const p1, -0xff8000

    .line 114
    .line 115
    .line 116
    return p1

    .line 117
    :cond_7
    const-string v2, "silver"

    .line 118
    .line 119
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 120
    .line 121
    .line 122
    move-result v2

    .line 123
    if-eqz v2, :cond_8

    .line 124
    .line 125
    const p1, -0x3f3f40

    .line 126
    .line 127
    .line 128
    return p1

    .line 129
    :cond_8
    const-string v2, "lime"

    .line 130
    .line 131
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 132
    .line 133
    .line 134
    move-result v2

    .line 135
    if-eqz v2, :cond_9

    .line 136
    .line 137
    const p1, -0xff0100

    .line 138
    .line 139
    .line 140
    return p1

    .line 141
    :cond_9
    const-string v2, "gray"

    .line 142
    .line 143
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 144
    .line 145
    .line 146
    move-result v2

    .line 147
    if-eqz v2, :cond_a

    .line 148
    .line 149
    const p1, -0x7f7f80

    .line 150
    .line 151
    .line 152
    return p1

    .line 153
    :cond_a
    const-string v2, "olive"

    .line 154
    .line 155
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 156
    .line 157
    .line 158
    move-result v2

    .line 159
    if-eqz v2, :cond_b

    .line 160
    .line 161
    const p1, -0x7f8000

    .line 162
    .line 163
    .line 164
    return p1

    .line 165
    :cond_b
    const-string v2, "white"

    .line 166
    .line 167
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 168
    .line 169
    .line 170
    move-result v2

    .line 171
    if-eqz v2, :cond_c

    .line 172
    .line 173
    return v0

    .line 174
    :cond_c
    const-string v2, "yellow"

    .line 175
    .line 176
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 177
    .line 178
    .line 179
    move-result v2

    .line 180
    if-eqz v2, :cond_d

    .line 181
    .line 182
    const/16 p1, -0x100

    .line 183
    .line 184
    return p1

    .line 185
    :cond_d
    const-string v2, "maroon"

    .line 186
    .line 187
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 188
    .line 189
    .line 190
    move-result v2

    .line 191
    if-eqz v2, :cond_e

    .line 192
    .line 193
    const/high16 p1, -0x800000    # Float.NEGATIVE_INFINITY

    .line 194
    .line 195
    return p1

    .line 196
    :cond_e
    const-string v2, "navy"

    .line 197
    .line 198
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 199
    .line 200
    .line 201
    move-result v2

    .line 202
    if-eqz v2, :cond_f

    .line 203
    .line 204
    const p1, -0xffff80

    .line 205
    .line 206
    .line 207
    return p1

    .line 208
    :cond_f
    const-string v2, "red"

    .line 209
    .line 210
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 211
    .line 212
    .line 213
    move-result v2

    .line 214
    if-eqz v2, :cond_10

    .line 215
    .line 216
    const/high16 p1, -0x10000

    .line 217
    .line 218
    return p1

    .line 219
    :cond_10
    const-string v2, "blue"

    .line 220
    .line 221
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 222
    .line 223
    .line 224
    move-result v2

    .line 225
    if-eqz v2, :cond_11

    .line 226
    .line 227
    const p1, -0xffff01

    .line 228
    .line 229
    .line 230
    return p1

    .line 231
    :cond_11
    const-string v2, "purple"

    .line 232
    .line 233
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 234
    .line 235
    .line 236
    move-result v2

    .line 237
    if-eqz v2, :cond_12

    .line 238
    .line 239
    const p1, -0x7fff80

    .line 240
    .line 241
    .line 242
    return p1

    .line 243
    :cond_12
    const-string v2, "teal"

    .line 244
    .line 245
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 246
    .line 247
    .line 248
    move-result v2

    .line 249
    if-eqz v2, :cond_13

    .line 250
    .line 251
    const p1, -0xff7f80

    .line 252
    .line 253
    .line 254
    return p1

    .line 255
    :cond_13
    const-string v2, "fuchsia"

    .line 256
    .line 257
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 258
    .line 259
    .line 260
    move-result v2

    .line 261
    if-eqz v2, :cond_14

    .line 262
    .line 263
    const p1, -0xff01

    .line 264
    .line 265
    .line 266
    return p1

    .line 267
    :cond_14
    const-string v2, "aqua"

    .line 268
    .line 269
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 270
    .line 271
    .line 272
    move-result p1

    .line 273
    if-eqz p1, :cond_15

    .line 274
    .line 275
    const p1, -0xff0001

    .line 276
    .line 277
    .line 278
    return p1

    .line 279
    :cond_15
    if-eqz p2, :cond_16

    .line 280
    .line 281
    return v0

    .line 282
    :cond_16
    :goto_1
    return v1
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private getDrawingWrapType(Lcom/intsig/office/fc/dom4j/Element;)S
    .locals 1

    .line 1
    if-eqz p1, :cond_6

    .line 2
    .line 3
    const-string v0, "wrapNone"

    .line 4
    .line 5
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    const-string v0, "behindDoc"

    .line 12
    .line 13
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    const-string v0, "1"

    .line 18
    .line 19
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    if-eqz p1, :cond_0

    .line 24
    .line 25
    const/4 p1, 0x6

    .line 26
    return p1

    .line 27
    :cond_0
    const/4 p1, 0x3

    .line 28
    return p1

    .line 29
    :cond_1
    const-string v0, "wrapSquare"

    .line 30
    .line 31
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    if-eqz v0, :cond_2

    .line 36
    .line 37
    const/4 p1, 0x1

    .line 38
    return p1

    .line 39
    :cond_2
    const-string v0, "wrapTight"

    .line 40
    .line 41
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    if-eqz v0, :cond_3

    .line 46
    .line 47
    const/4 p1, 0x0

    .line 48
    return p1

    .line 49
    :cond_3
    const-string v0, "wrapThrough"

    .line 50
    .line 51
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    if-eqz v0, :cond_4

    .line 56
    .line 57
    const/4 p1, 0x4

    .line 58
    return p1

    .line 59
    :cond_4
    const-string v0, "wrapTopAndBottom"

    .line 60
    .line 61
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    if-eqz p1, :cond_5

    .line 66
    .line 67
    const/4 p1, 0x5

    .line 68
    return p1

    .line 69
    :cond_5
    const/4 p1, 0x2

    .line 70
    return p1

    .line 71
    :cond_6
    const/4 p1, -0x1

    .line 72
    return p1
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private getFillType(Ljava/lang/String;)B
    .locals 1

    .line 1
    const-string v0, "gradient"

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x7

    .line 10
    return p1

    .line 11
    :cond_0
    const-string v0, "gradientRadial"

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    const/4 p1, 0x4

    .line 20
    return p1

    .line 21
    :cond_1
    const-string v0, "pattern"

    .line 22
    .line 23
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_2

    .line 28
    .line 29
    const/4 p1, 0x1

    .line 30
    return p1

    .line 31
    :cond_2
    const-string v0, "tile"

    .line 32
    .line 33
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_3

    .line 38
    .line 39
    const/4 p1, 0x2

    .line 40
    return p1

    .line 41
    :cond_3
    const-string v0, "frame"

    .line 42
    .line 43
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    if-eqz p1, :cond_4

    .line 48
    .line 49
    const/4 p1, 0x3

    .line 50
    return p1

    .line 51
    :cond_4
    const/4 p1, 0x0

    .line 52
    return p1
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private getRadialGradientPositionType(Lcom/intsig/office/fc/dom4j/Element;)I
    .locals 6

    .line 1
    const-string v0, "focusposition"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const/4 v0, 0x0

    .line 8
    if-eqz p1, :cond_4

    .line 9
    .line 10
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-lez v1, :cond_4

    .line 15
    .line 16
    const-string v1, ","

    .line 17
    .line 18
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    if-eqz p1, :cond_4

    .line 23
    .line 24
    array-length v1, p1

    .line 25
    const/4 v2, 0x2

    .line 26
    const-string v3, "1"

    .line 27
    .line 28
    const/4 v4, 0x1

    .line 29
    if-ne v1, v2, :cond_3

    .line 30
    .line 31
    aget-object v1, p1, v0

    .line 32
    .line 33
    const-string v5, ".5"

    .line 34
    .line 35
    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    if-eqz v1, :cond_0

    .line 40
    .line 41
    aget-object v1, p1, v4

    .line 42
    .line 43
    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    if-eqz v1, :cond_0

    .line 48
    .line 49
    const/4 v0, 0x4

    .line 50
    goto :goto_1

    .line 51
    :cond_0
    aget-object v1, p1, v0

    .line 52
    .line 53
    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 54
    .line 55
    .line 56
    move-result v1

    .line 57
    if-eqz v1, :cond_1

    .line 58
    .line 59
    aget-object v1, p1, v4

    .line 60
    .line 61
    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    if-eqz v1, :cond_1

    .line 66
    .line 67
    const/4 v0, 0x3

    .line 68
    goto :goto_1

    .line 69
    :cond_1
    aget-object v1, p1, v0

    .line 70
    .line 71
    const-string v5, ""

    .line 72
    .line 73
    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 74
    .line 75
    .line 76
    move-result v1

    .line 77
    if-eqz v1, :cond_2

    .line 78
    .line 79
    aget-object v1, p1, v4

    .line 80
    .line 81
    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 82
    .line 83
    .line 84
    move-result v1

    .line 85
    if-eqz v1, :cond_2

    .line 86
    .line 87
    const/4 v0, 0x2

    .line 88
    goto :goto_1

    .line 89
    :cond_2
    aget-object v1, p1, v0

    .line 90
    .line 91
    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 92
    .line 93
    .line 94
    move-result v1

    .line 95
    if-eqz v1, :cond_4

    .line 96
    .line 97
    aget-object p1, p1, v4

    .line 98
    .line 99
    invoke-virtual {v5, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 100
    .line 101
    .line 102
    move-result p1

    .line 103
    if-eqz p1, :cond_4

    .line 104
    .line 105
    goto :goto_0

    .line 106
    :cond_3
    array-length v1, p1

    .line 107
    if-ne v1, v4, :cond_4

    .line 108
    .line 109
    aget-object p1, p1, v0

    .line 110
    .line 111
    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 112
    .line 113
    .line 114
    move-result p1

    .line 115
    if-eqz p1, :cond_4

    .line 116
    .line 117
    :goto_0
    const/4 v0, 0x1

    .line 118
    :cond_4
    :goto_1
    return v0
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private getRelative(Ljava/lang/String;)B
    .locals 2

    .line 1
    const-string v0, "margin"

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    return p1

    .line 11
    :cond_0
    const-string v0, "page"

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    const/4 p1, 0x2

    .line 20
    return p1

    .line 21
    :cond_1
    const-string v0, "column"

    .line 22
    .line 23
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    const/4 v1, 0x0

    .line 28
    if-eqz v0, :cond_2

    .line 29
    .line 30
    return v1

    .line 31
    :cond_2
    const-string v0, "character"

    .line 32
    .line 33
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_3

    .line 38
    .line 39
    const/4 p1, 0x3

    .line 40
    return p1

    .line 41
    :cond_3
    const-string v0, "leftMargin"

    .line 42
    .line 43
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-eqz v0, :cond_4

    .line 48
    .line 49
    const/4 p1, 0x4

    .line 50
    return p1

    .line 51
    :cond_4
    const-string v0, "rightMargin"

    .line 52
    .line 53
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    if-eqz v0, :cond_5

    .line 58
    .line 59
    const/4 p1, 0x5

    .line 60
    return p1

    .line 61
    :cond_5
    const-string v0, "insideMargin"

    .line 62
    .line 63
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    if-eqz v0, :cond_6

    .line 68
    .line 69
    const/16 p1, 0x8

    .line 70
    .line 71
    return p1

    .line 72
    :cond_6
    const-string v0, "outsideMargin"

    .line 73
    .line 74
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 75
    .line 76
    .line 77
    move-result v0

    .line 78
    if-eqz v0, :cond_7

    .line 79
    .line 80
    const/16 p1, 0x9

    .line 81
    .line 82
    return p1

    .line 83
    :cond_7
    const-string v0, "paragraph"

    .line 84
    .line 85
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    if-eqz v0, :cond_8

    .line 90
    .line 91
    const/16 p1, 0xa

    .line 92
    .line 93
    return p1

    .line 94
    :cond_8
    const-string v0, "line"

    .line 95
    .line 96
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 97
    .line 98
    .line 99
    move-result v0

    .line 100
    if-eqz v0, :cond_9

    .line 101
    .line 102
    const/16 p1, 0xb

    .line 103
    .line 104
    return p1

    .line 105
    :cond_9
    const-string v0, "topMargin"

    .line 106
    .line 107
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 108
    .line 109
    .line 110
    move-result v0

    .line 111
    if-eqz v0, :cond_a

    .line 112
    .line 113
    const/4 p1, 0x6

    .line 114
    return p1

    .line 115
    :cond_a
    const-string v0, "bottomMargin"

    .line 116
    .line 117
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 118
    .line 119
    .line 120
    move-result p1

    .line 121
    if-eqz p1, :cond_b

    .line 122
    .line 123
    const/4 p1, 0x7

    .line 124
    return p1

    .line 125
    :cond_b
    return v1
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private getShapeWrapType(Lcom/intsig/office/fc/dom4j/Element;)S
    .locals 3

    .line 1
    const-string v0, "wrap"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-eqz v0, :cond_4

    .line 9
    .line 10
    const-string v2, "type"

    .line 11
    .line 12
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    const-string v2, "none"

    .line 17
    .line 18
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_0

    .line 23
    .line 24
    const/4 p1, 0x2

    .line 25
    return p1

    .line 26
    :cond_0
    const-string v2, "square"

    .line 27
    .line 28
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-eqz v2, :cond_1

    .line 33
    .line 34
    return v1

    .line 35
    :cond_1
    const-string v2, "tight"

    .line 36
    .line 37
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    if-eqz v2, :cond_2

    .line 42
    .line 43
    const/4 p1, 0x0

    .line 44
    return p1

    .line 45
    :cond_2
    const-string v2, "topAndBottom"

    .line 46
    .line 47
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 48
    .line 49
    .line 50
    move-result v2

    .line 51
    if-eqz v2, :cond_3

    .line 52
    .line 53
    const/4 p1, 0x5

    .line 54
    return p1

    .line 55
    :cond_3
    const-string v2, "through"

    .line 56
    .line 57
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    if-eqz v0, :cond_4

    .line 62
    .line 63
    const/4 p1, 0x4

    .line 64
    return p1

    .line 65
    :cond_4
    const-string v0, "style"

    .line 66
    .line 67
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    if-eqz p1, :cond_7

    .line 72
    .line 73
    const-string v0, ";"

    .line 74
    .line 75
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    array-length v0, p1

    .line 80
    sub-int/2addr v0, v1

    .line 81
    :goto_0
    if-ltz v0, :cond_7

    .line 82
    .line 83
    aget-object v1, p1, v0

    .line 84
    .line 85
    const-string v2, "z-index:"

    .line 86
    .line 87
    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 88
    .line 89
    .line 90
    move-result v1

    .line 91
    if-eqz v1, :cond_6

    .line 92
    .line 93
    aget-object p1, p1, v0

    .line 94
    .line 95
    const-string v0, ""

    .line 96
    .line 97
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 102
    .line 103
    .line 104
    move-result p1

    .line 105
    if-lez p1, :cond_5

    .line 106
    .line 107
    const/4 p1, 0x3

    .line 108
    return p1

    .line 109
    :cond_5
    const/4 p1, 0x6

    .line 110
    return p1

    .line 111
    :cond_6
    add-int/lit8 v0, v0, -0x1

    .line 112
    .line 113
    goto :goto_0

    .line 114
    :cond_7
    const/4 p1, -0x1

    .line 115
    return p1
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private getValue(Ljava/lang/String;)F
    .locals 3

    .line 1
    const-string v0, "pt"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-lez v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const-string v0, "in"

    .line 20
    .line 21
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    const/high16 v2, 0x42900000    # 72.0f

    .line 26
    .line 27
    if-lez v0, :cond_1

    .line 28
    .line 29
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    mul-float p1, p1, v2

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    mul-float p1, p1, v2

    .line 45
    .line 46
    const v0, 0x495f3e00    # 914400.0f

    .line 47
    .line 48
    .line 49
    div-float/2addr p1, v0

    .line 50
    :goto_0
    const v0, 0x3faaaaab

    .line 51
    .line 52
    .line 53
    mul-float p1, p1, v0

    .line 54
    .line 55
    return p1
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private getValueInPt(Ljava/lang/String;F)F
    .locals 4

    .line 1
    const-string v0, "pt"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/4 v2, 0x0

    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 11
    .line 12
    .line 13
    move-result p2

    .line 14
    invoke-virtual {p1, v2, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    return p1

    .line 23
    :cond_0
    const-string v0, "in"

    .line 24
    .line 25
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    const/high16 v3, 0x42900000    # 72.0f

    .line 30
    .line 31
    if-eqz v1, :cond_1

    .line 32
    .line 33
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 34
    .line 35
    .line 36
    move-result p2

    .line 37
    invoke-virtual {p1, v2, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    mul-float p1, p1, v3

    .line 46
    .line 47
    return p1

    .line 48
    :cond_1
    const-string v0, "mm"

    .line 49
    .line 50
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    if-eqz v1, :cond_2

    .line 55
    .line 56
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 57
    .line 58
    .line 59
    move-result p2

    .line 60
    invoke-virtual {p1, v2, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 65
    .line 66
    .line 67
    move-result p1

    .line 68
    const p2, 0x403570a4    # 2.835f

    .line 69
    .line 70
    .line 71
    mul-float p1, p1, p2

    .line 72
    .line 73
    return p1

    .line 74
    :cond_2
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    mul-float p1, p1, p2

    .line 79
    .line 80
    mul-float p1, p1, v3

    .line 81
    .line 82
    const p2, 0x495f3e00    # 914400.0f

    .line 83
    .line 84
    .line 85
    div-float/2addr p1, p2

    .line 86
    return p1
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private hasTextbox2007(Lcom/intsig/office/fc/dom4j/Element;)Z
    .locals 4

    .line 1
    const-string v0, "textbox"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x1

    .line 8
    const/4 v2, 0x0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const-string p1, "txbxContent"

    .line 12
    .line 13
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    if-eqz p1, :cond_2

    .line 18
    .line 19
    return v1

    .line 20
    :cond_0
    const-string v0, "textpath"

    .line 21
    .line 22
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    if-eqz v3, :cond_2

    .line 27
    .line 28
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    const-string v0, "string"

    .line 33
    .line 34
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    if-eqz p1, :cond_1

    .line 39
    .line 40
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    if-lez p1, :cond_1

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_1
    const/4 v1, 0x0

    .line 48
    :goto_0
    return v1

    .line 49
    :cond_2
    return v2
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private hasTextbox2010(Lcom/intsig/office/fc/dom4j/Element;)Z
    .locals 1

    .line 1
    const-string v0, "txbx"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    const-string v0, "txbxContent"

    .line 10
    .line 11
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    const/4 p1, 0x1

    .line 18
    return p1

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    return p1
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private openFile()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getActivity()Landroid/app/Activity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->uri:Landroid/net/Uri;

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/office/ss/util/StreamUtils;->getInputStream(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/InputStream;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->filePath:Ljava/lang/String;

    .line 14
    .line 15
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    new-instance v1, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 22
    .line 23
    invoke-direct {v1, v0}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;-><init>(Ljava/io/InputStream;)V

    .line 24
    .line 25
    .line 26
    iput-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->filePath:Ljava/lang/String;

    .line 32
    .line 33
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;-><init>(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 37
    .line 38
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getParts()Ljava/util/ArrayList;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    const-string v1, "Format error"

    .line 49
    .line 50
    if-eqz v0, :cond_5

    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 53
    .line 54
    const-string v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

    .line 55
    .line 56
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    const/4 v2, 0x0

    .line 61
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationship(I)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    invoke-virtual {v2}, Ljava/net/URI;->toString()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    const-string v3, "/word/document"

    .line 74
    .line 75
    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 76
    .line 77
    .line 78
    move-result v2

    .line 79
    if-eqz v2, :cond_4

    .line 80
    .line 81
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 82
    .line 83
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 88
    .line 89
    invoke-direct {p0}, Lcom/intsig/office/fc/doc/DOCXReader;->processThemeColor()V

    .line 90
    .line 91
    .line 92
    invoke-direct {p0}, Lcom/intsig/office/fc/doc/DOCXReader;->processBulletNumber()V

    .line 93
    .line 94
    .line 95
    invoke-direct {p0}, Lcom/intsig/office/fc/doc/DOCXReader;->processStyle()V

    .line 96
    .line 97
    .line 98
    new-instance v0, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 99
    .line 100
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    .line 101
    .line 102
    .line 103
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->secElem:Lcom/intsig/office/simpletext/model/SectionElement;

    .line 104
    .line 105
    const-wide/16 v0, 0x0

    .line 106
    .line 107
    iput-wide v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 108
    .line 109
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 110
    .line 111
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 112
    .line 113
    .line 114
    new-instance v1, Lcom/intsig/office/fc/doc/DOCXReader$DOCXSaxHandler;

    .line 115
    .line 116
    invoke-direct {v1, p0}, Lcom/intsig/office/fc/doc/DOCXReader$DOCXSaxHandler;-><init>(Lcom/intsig/office/fc/doc/DOCXReader;)V

    .line 117
    .line 118
    .line 119
    const-string v2, "/document/body/tbl"

    .line 120
    .line 121
    invoke-virtual {v0, v2, v1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->addHandler(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 122
    .line 123
    .line 124
    const-string v2, "/document/body/p"

    .line 125
    .line 126
    invoke-virtual {v0, v2, v1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->addHandler(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 127
    .line 128
    .line 129
    const-string v2, "/document/body/sdt"

    .line 130
    .line 131
    invoke-virtual {v0, v2, v1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->addHandler(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 132
    .line 133
    .line 134
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 135
    .line 136
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 137
    .line 138
    .line 139
    move-result-object v1

    .line 140
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 145
    .line 146
    .line 147
    move-result-object v1

    .line 148
    const-string v2, "background"

    .line 149
    .line 150
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 151
    .line 152
    .line 153
    move-result-object v1

    .line 154
    if-eqz v1, :cond_3

    .line 155
    .line 156
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 157
    .line 158
    .line 159
    move-result-object v3

    .line 160
    if-eqz v3, :cond_1

    .line 161
    .line 162
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 163
    .line 164
    .line 165
    move-result-object v1

    .line 166
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->processBackgroundAndFill(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 167
    .line 168
    .line 169
    move-result-object v1

    .line 170
    goto :goto_1

    .line 171
    :cond_1
    const-string v2, "color"

    .line 172
    .line 173
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 174
    .line 175
    .line 176
    move-result-object v1

    .line 177
    if-eqz v1, :cond_2

    .line 178
    .line 179
    new-instance v2, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 180
    .line 181
    invoke-direct {v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 182
    .line 183
    .line 184
    new-instance v3, Ljava/lang/StringBuilder;

    .line 185
    .line 186
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 187
    .line 188
    .line 189
    const-string v4, "#"

    .line 190
    .line 191
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    .line 193
    .line 194
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    .line 196
    .line 197
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 198
    .line 199
    .line 200
    move-result-object v1

    .line 201
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 202
    .line 203
    .line 204
    move-result v1

    .line 205
    invoke-virtual {v2, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 206
    .line 207
    .line 208
    move-object v1, v2

    .line 209
    goto :goto_1

    .line 210
    :cond_2
    const/4 v1, 0x0

    .line 211
    :goto_1
    iget-object v2, p0, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 212
    .line 213
    invoke-virtual {v2, v1}, Lcom/intsig/office/wp/model/WPDocument;->setPageBackground(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 214
    .line 215
    .line 216
    :cond_3
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 217
    .line 218
    .line 219
    move-result-object v0

    .line 220
    const-string v1, "body"

    .line 221
    .line 222
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 223
    .line 224
    .line 225
    move-result-object v0

    .line 226
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->processSection(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 227
    .line 228
    .line 229
    invoke-direct {p0}, Lcom/intsig/office/fc/doc/DOCXReader;->processRelativeShapeSize()V

    .line 230
    .line 231
    .line 232
    return-void

    .line 233
    :cond_4
    new-instance v0, Ljava/lang/Exception;

    .line 234
    .line 235
    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 236
    .line 237
    .line 238
    throw v0

    .line 239
    :cond_5
    new-instance v0, Ljava/lang/Exception;

    .line 240
    .line 241
    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 242
    .line 243
    .line 244
    throw v0
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method static bridge synthetic o〇0(Lcom/intsig/office/fc/doc/DOCXReader;Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/doc/DOCXReader;->processTable(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private processAlternateContent(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;)V
    .locals 13

    .line 1
    if-eqz p1, :cond_6

    .line 2
    .line 3
    const-string v0, "Choice"

    .line 4
    .line 5
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    if-eqz p1, :cond_6

    .line 10
    .line 11
    const-string v0, "drawing"

    .line 12
    .line 13
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    if-eqz p1, :cond_6

    .line 18
    .line 19
    const-string v0, "anchor"

    .line 20
    .line 21
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const/4 v1, -0x1

    .line 26
    if-nez v0, :cond_0

    .line 27
    .line 28
    const-string v0, "inline"

    .line 29
    .line 30
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const/4 p1, 0x2

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    const/4 p1, -0x1

    .line 37
    :goto_0
    if-eqz v0, :cond_6

    .line 38
    .line 39
    const-string v2, "docPr"

    .line 40
    .line 41
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    if-eqz v2, :cond_2

    .line 46
    .line 47
    const-string v3, "name"

    .line 48
    .line 49
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    if-eqz v2, :cond_2

    .line 54
    .line 55
    const-string v3, "Genko"

    .line 56
    .line 57
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    if-nez v3, :cond_1

    .line 62
    .line 63
    const-string v3, "Header"

    .line 64
    .line 65
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 66
    .line 67
    .line 68
    move-result v3

    .line 69
    if-nez v3, :cond_1

    .line 70
    .line 71
    const-string v3, "Footer"

    .line 72
    .line 73
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 74
    .line 75
    .line 76
    move-result v2

    .line 77
    if-eqz v2, :cond_2

    .line 78
    .line 79
    :cond_1
    return-void

    .line 80
    :cond_2
    const-string v2, "graphic"

    .line 81
    .line 82
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 83
    .line 84
    .line 85
    move-result-object v2

    .line 86
    if-eqz v2, :cond_6

    .line 87
    .line 88
    const-string v3, "graphicData"

    .line 89
    .line 90
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 91
    .line 92
    .line 93
    move-result-object v2

    .line 94
    if-eqz v2, :cond_6

    .line 95
    .line 96
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Element;->elementIterator()Ljava/util/Iterator;

    .line 97
    .line 98
    .line 99
    move-result-object v2

    .line 100
    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 101
    .line 102
    .line 103
    move-result v3

    .line 104
    if-eqz v3, :cond_6

    .line 105
    .line 106
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 107
    .line 108
    .line 109
    move-result-object v3

    .line 110
    move-object v6, v3

    .line 111
    check-cast v6, Lcom/intsig/office/fc/dom4j/Element;

    .line 112
    .line 113
    const/4 v7, 0x0

    .line 114
    const/high16 v8, 0x3f800000    # 1.0f

    .line 115
    .line 116
    const/high16 v9, 0x3f800000    # 1.0f

    .line 117
    .line 118
    const/4 v10, 0x0

    .line 119
    const/4 v11, 0x0

    .line 120
    const/4 v12, 0x1

    .line 121
    move-object v4, p0

    .line 122
    move-object v5, p2

    .line 123
    invoke-direct/range {v4 .. v12}, Lcom/intsig/office/fc/doc/DOCXReader;->processAutoShape2010(Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/WPGroupShape;FFIIZ)Lcom/intsig/office/common/shape/AbstractShape;

    .line 124
    .line 125
    .line 126
    move-result-object v3

    .line 127
    if-eqz v3, :cond_3

    .line 128
    .line 129
    instance-of v4, v3, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 130
    .line 131
    if-eqz v4, :cond_5

    .line 132
    .line 133
    move-object v4, v3

    .line 134
    check-cast v4, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 135
    .line 136
    invoke-virtual {v4}, Lcom/intsig/office/common/shape/WPAutoShape;->getGroupShape()Lcom/intsig/office/common/shape/WPGroupShape;

    .line 137
    .line 138
    .line 139
    move-result-object v5

    .line 140
    if-eqz v5, :cond_5

    .line 141
    .line 142
    invoke-virtual {v4}, Lcom/intsig/office/common/shape/WPAutoShape;->getGroupShape()Lcom/intsig/office/common/shape/WPGroupShape;

    .line 143
    .line 144
    .line 145
    move-result-object v4

    .line 146
    if-ne p1, v1, :cond_4

    .line 147
    .line 148
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->getDrawingWrapType(Lcom/intsig/office/fc/dom4j/Element;)S

    .line 149
    .line 150
    .line 151
    move-result p1

    .line 152
    :cond_4
    invoke-virtual {v4, p1}, Lcom/intsig/office/common/shape/WPGroupShape;->setWrapType(S)V

    .line 153
    .line 154
    .line 155
    invoke-direct {p0, v4, p1}, Lcom/intsig/office/fc/doc/DOCXReader;->setShapeWrapType(Lcom/intsig/office/common/shape/WPGroupShape;S)V

    .line 156
    .line 157
    .line 158
    :cond_5
    move-object v4, v3

    .line 159
    check-cast v4, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 160
    .line 161
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/AbstractShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 162
    .line 163
    .line 164
    move-result-object v3

    .line 165
    invoke-direct {p0, v4, v0, v3}, Lcom/intsig/office/fc/doc/DOCXReader;->processWrapAndPosition_Drawing(Lcom/intsig/office/common/shape/WPAbstractShape;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)V

    .line 166
    .line 167
    .line 168
    goto :goto_1

    .line 169
    :cond_6
    return-void
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processArrow(Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 3

    .line 1
    const-string v0, "stroke"

    .line 2
    .line 3
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object p2

    .line 7
    if-eqz p2, :cond_1

    .line 8
    .line 9
    const-string v0, "startarrow"

    .line 10
    .line 11
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->getArrowType(Ljava/lang/String;)B

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-lez v0, :cond_0

    .line 20
    .line 21
    const-string v1, "startarrowwidth"

    .line 22
    .line 23
    invoke-interface {p2, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->getArrowWidth(Ljava/lang/String;)I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    const-string v2, "startarrowlength"

    .line 32
    .line 33
    invoke-interface {p2, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/doc/DOCXReader;->getArrowLength(Ljava/lang/String;)I

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    invoke-virtual {p1, v0, v1, v2}, Lcom/intsig/office/common/shape/LineShape;->createStartArrow(BII)V

    .line 42
    .line 43
    .line 44
    :cond_0
    const-string v0, "endarrow"

    .line 45
    .line 46
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->getArrowType(Ljava/lang/String;)B

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    if-lez v0, :cond_1

    .line 55
    .line 56
    const-string v1, "endarrowwidth"

    .line 57
    .line 58
    invoke-interface {p2, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->getArrowWidth(Ljava/lang/String;)I

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    const-string v2, "endarrowlength"

    .line 67
    .line 68
    invoke-interface {p2, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object p2

    .line 72
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->getArrowLength(Ljava/lang/String;)I

    .line 73
    .line 74
    .line 75
    move-result p2

    .line 76
    invoke-virtual {p1, v0, v1, p2}, Lcom/intsig/office/common/shape/LineShape;->createEndArrow(BII)V

    .line 77
    .line 78
    .line 79
    :cond_1
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processAutoShape(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/common/shape/WPGroupShape;FFZ)Lcom/intsig/office/common/shape/WPAutoShape;
    .locals 28

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    move-object/from16 v7, p1

    .line 4
    .line 5
    move-object/from16 v8, p3

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    if-eqz v7, :cond_2d

    .line 9
    .line 10
    invoke-direct/range {p0 .. p1}, Lcom/intsig/office/fc/doc/DOCXReader;->getAutoShapeType(Lcom/intsig/office/fc/dom4j/Element;)I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    const-string v2, "adj"

    .line 15
    .line 16
    invoke-interface {v7, v2}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 17
    .line 18
    .line 19
    move-result-object v3

    .line 20
    if-eqz v3, :cond_0

    .line 21
    .line 22
    invoke-interface {v7, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    if-eqz v2, :cond_0

    .line 27
    .line 28
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    if-lez v3, :cond_0

    .line 33
    .line 34
    const-string v3, ","

    .line 35
    .line 36
    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    goto :goto_0

    .line 41
    :cond_0
    move-object v2, v0

    .line 42
    :goto_0
    const/4 v9, 0x0

    .line 43
    if-eqz v2, :cond_2

    .line 44
    .line 45
    array-length v3, v2

    .line 46
    if-lez v3, :cond_2

    .line 47
    .line 48
    array-length v3, v2

    .line 49
    new-array v3, v3, [Ljava/lang/Float;

    .line 50
    .line 51
    const/4 v4, 0x0

    .line 52
    :goto_1
    array-length v5, v2

    .line 53
    if-ge v4, v5, :cond_3

    .line 54
    .line 55
    aget-object v5, v2, v4

    .line 56
    .line 57
    if-eqz v5, :cond_1

    .line 58
    .line 59
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 60
    .line 61
    .line 62
    move-result v10

    .line 63
    if-lez v10, :cond_1

    .line 64
    .line 65
    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 66
    .line 67
    .line 68
    move-result v5

    .line 69
    const v10, 0x46a8c000    # 21600.0f

    .line 70
    .line 71
    .line 72
    div-float/2addr v5, v10

    .line 73
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 74
    .line 75
    .line 76
    move-result-object v5

    .line 77
    aput-object v5, v3, v4

    .line 78
    .line 79
    goto :goto_2

    .line 80
    :cond_1
    aput-object v0, v3, v4

    .line 81
    .line 82
    :goto_2
    add-int/lit8 v4, v4, 0x1

    .line 83
    .line 84
    goto :goto_1

    .line 85
    :cond_2
    move-object v3, v0

    .line 86
    :cond_3
    invoke-direct/range {p0 .. p1}, Lcom/intsig/office/fc/doc/DOCXReader;->processBackgroundAndFill(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 87
    .line 88
    .line 89
    move-result-object v10

    .line 90
    invoke-direct/range {p0 .. p1}, Lcom/intsig/office/fc/doc/DOCXReader;->processLine(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/borders/Line;

    .line 91
    .line 92
    .line 93
    move-result-object v11

    .line 94
    const/16 v2, 0x20

    .line 95
    .line 96
    const/16 v4, 0x21

    .line 97
    .line 98
    const/4 v12, 0x1

    .line 99
    if-eq v1, v2, :cond_27

    .line 100
    .line 101
    if-eq v1, v4, :cond_27

    .line 102
    .line 103
    const/16 v2, 0x22

    .line 104
    .line 105
    if-eq v1, v2, :cond_27

    .line 106
    .line 107
    const/16 v2, 0x26

    .line 108
    .line 109
    if-ne v1, v2, :cond_4

    .line 110
    .line 111
    goto/16 :goto_11

    .line 112
    .line 113
    :cond_4
    const/16 v2, 0xe9

    .line 114
    .line 115
    if-ne v1, v2, :cond_b

    .line 116
    .line 117
    new-instance v13, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 118
    .line 119
    invoke-direct {v13}, Lcom/intsig/office/common/shape/WPAutoShape;-><init>()V

    .line 120
    .line 121
    .line 122
    invoke-virtual {v13, v2}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 123
    .line 124
    .line 125
    invoke-direct {v6, v13, v7}, Lcom/intsig/office/fc/doc/DOCXReader;->processArrow(Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 126
    .line 127
    .line 128
    const-string v0, "path"

    .line 129
    .line 130
    invoke-interface {v7, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 131
    .line 132
    .line 133
    move-result-object v14

    .line 134
    move-object/from16 v0, p0

    .line 135
    .line 136
    move-object/from16 v1, p1

    .line 137
    .line 138
    move-object v2, v13

    .line 139
    move-object/from16 v3, p3

    .line 140
    .line 141
    move/from16 v4, p4

    .line 142
    .line 143
    move/from16 v5, p5

    .line 144
    .line 145
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/doc/DOCXReader;->processPolygonZoom(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/AbstractShape;Lcom/intsig/office/common/shape/GroupShape;FF)F

    .line 146
    .line 147
    .line 148
    move-result v0

    .line 149
    if-eqz v11, :cond_5

    .line 150
    .line 151
    invoke-virtual {v11}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 152
    .line 153
    .line 154
    move-result v1

    .line 155
    goto :goto_3

    .line 156
    :cond_5
    const/4 v1, 0x1

    .line 157
    :goto_3
    int-to-float v1, v1

    .line 158
    mul-float v1, v1, v0

    .line 159
    .line 160
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 161
    .line 162
    .line 163
    move-result v0

    .line 164
    invoke-static {}, Lcom/intsig/office/fc/doc/VMLPathParser;->instance()Lcom/intsig/office/fc/doc/VMLPathParser;

    .line 165
    .line 166
    .line 167
    move-result-object v1

    .line 168
    invoke-virtual {v1, v13, v14, v0}, Lcom/intsig/office/fc/doc/VMLPathParser;->createPath(Lcom/intsig/office/common/shape/WPAutoShape;Ljava/lang/String;I)Lcom/intsig/office/fc/doc/PathWithArrow;

    .line 169
    .line 170
    .line 171
    move-result-object v14

    .line 172
    if-eqz v14, :cond_a

    .line 173
    .line 174
    invoke-virtual {v14}, Lcom/intsig/office/fc/doc/PathWithArrow;->getPolygonPath()[Landroid/graphics/Path;

    .line 175
    .line 176
    .line 177
    move-result-object v0

    .line 178
    if-eqz v0, :cond_8

    .line 179
    .line 180
    const/4 v1, 0x0

    .line 181
    :goto_4
    array-length v2, v0

    .line 182
    if-ge v1, v2, :cond_8

    .line 183
    .line 184
    new-instance v2, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 185
    .line 186
    invoke-direct {v2}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 187
    .line 188
    .line 189
    aget-object v3, v0, v1

    .line 190
    .line 191
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 192
    .line 193
    .line 194
    if-eqz v11, :cond_6

    .line 195
    .line 196
    invoke-virtual {v2, v11}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 197
    .line 198
    .line 199
    :cond_6
    if-eqz v10, :cond_7

    .line 200
    .line 201
    invoke-virtual {v2, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 202
    .line 203
    .line 204
    :cond_7
    invoke-virtual {v13, v2}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 205
    .line 206
    .line 207
    add-int/lit8 v1, v1, 0x1

    .line 208
    .line 209
    goto :goto_4

    .line 210
    :cond_8
    invoke-virtual {v14}, Lcom/intsig/office/fc/doc/PathWithArrow;->getStartArrow()Landroid/graphics/Path;

    .line 211
    .line 212
    .line 213
    move-result-object v0

    .line 214
    if-eqz v0, :cond_9

    .line 215
    .line 216
    invoke-virtual {v14}, Lcom/intsig/office/fc/doc/PathWithArrow;->getStartArrow()Landroid/graphics/Path;

    .line 217
    .line 218
    .line 219
    move-result-object v1

    .line 220
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 221
    .line 222
    .line 223
    move-result-object v0

    .line 224
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 225
    .line 226
    .line 227
    move-result v5

    .line 228
    const/4 v4, 0x1

    .line 229
    move-object/from16 v0, p0

    .line 230
    .line 231
    move-object v2, v10

    .line 232
    move-object v3, v11

    .line 233
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/doc/DOCXReader;->getArrowExtendPath(Landroid/graphics/Path;Lcom/intsig/office/common/bg/BackgroundAndFill;Lcom/intsig/office/common/borders/Line;ZB)Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 234
    .line 235
    .line 236
    move-result-object v0

    .line 237
    invoke-virtual {v13, v0}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 238
    .line 239
    .line 240
    :cond_9
    invoke-virtual {v14}, Lcom/intsig/office/fc/doc/PathWithArrow;->getEndArrow()Landroid/graphics/Path;

    .line 241
    .line 242
    .line 243
    move-result-object v0

    .line 244
    if-eqz v0, :cond_a

    .line 245
    .line 246
    invoke-virtual {v14}, Lcom/intsig/office/fc/doc/PathWithArrow;->getEndArrow()Landroid/graphics/Path;

    .line 247
    .line 248
    .line 249
    move-result-object v1

    .line 250
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 251
    .line 252
    .line 253
    move-result-object v0

    .line 254
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 255
    .line 256
    .line 257
    move-result v5

    .line 258
    const/4 v4, 0x1

    .line 259
    move-object/from16 v0, p0

    .line 260
    .line 261
    move-object v2, v10

    .line 262
    move-object v3, v11

    .line 263
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/doc/DOCXReader;->getArrowExtendPath(Landroid/graphics/Path;Lcom/intsig/office/common/bg/BackgroundAndFill;Lcom/intsig/office/common/borders/Line;ZB)Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 264
    .line 265
    .line 266
    move-result-object v0

    .line 267
    invoke-virtual {v13, v0}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 268
    .line 269
    .line 270
    :cond_a
    :goto_5
    move-object/from16 v27, v7

    .line 271
    .line 272
    move-object v7, v6

    .line 273
    move-object/from16 v6, v27

    .line 274
    .line 275
    goto/16 :goto_12

    .line 276
    .line 277
    :cond_b
    const/16 v2, 0xf7

    .line 278
    .line 279
    const/16 v4, 0xf9

    .line 280
    .line 281
    const/16 v5, 0xf8

    .line 282
    .line 283
    if-eq v1, v2, :cond_11

    .line 284
    .line 285
    if-eq v1, v5, :cond_11

    .line 286
    .line 287
    if-ne v1, v4, :cond_c

    .line 288
    .line 289
    goto :goto_7

    .line 290
    :cond_c
    const-string v0, "id"

    .line 291
    .line 292
    invoke-interface {v7, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 293
    .line 294
    .line 295
    move-result-object v0

    .line 296
    if-eqz v0, :cond_d

    .line 297
    .line 298
    const-string v2, "WaterMarkObject"

    .line 299
    .line 300
    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 301
    .line 302
    .line 303
    move-result v0

    .line 304
    if-lez v0, :cond_d

    .line 305
    .line 306
    iput-boolean v12, v6, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessWatermark:Z

    .line 307
    .line 308
    :cond_d
    iget-boolean v0, v6, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessWatermark:Z

    .line 309
    .line 310
    if-eqz v0, :cond_e

    .line 311
    .line 312
    new-instance v0, Lcom/intsig/office/common/shape/WatermarkShape;

    .line 313
    .line 314
    invoke-direct {v0}, Lcom/intsig/office/common/shape/WatermarkShape;-><init>()V

    .line 315
    .line 316
    .line 317
    goto :goto_6

    .line 318
    :cond_e
    new-instance v0, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 319
    .line 320
    invoke-direct {v0}, Lcom/intsig/office/common/shape/WPAutoShape;-><init>()V

    .line 321
    .line 322
    .line 323
    :goto_6
    move-object v13, v0

    .line 324
    invoke-virtual {v13, v1}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 325
    .line 326
    .line 327
    invoke-direct {v6, v13, v7}, Lcom/intsig/office/fc/doc/DOCXReader;->processArrow(Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 328
    .line 329
    .line 330
    if-eqz v10, :cond_f

    .line 331
    .line 332
    invoke-virtual {v13, v10}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 333
    .line 334
    .line 335
    :cond_f
    if-eqz v11, :cond_10

    .line 336
    .line 337
    invoke-virtual {v13, v11}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 338
    .line 339
    .line 340
    :cond_10
    invoke-virtual {v13, v3}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 341
    .line 342
    .line 343
    goto :goto_5

    .line 344
    :cond_11
    :goto_7
    new-instance v13, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 345
    .line 346
    invoke-direct {v13}, Lcom/intsig/office/common/shape/WPAutoShape;-><init>()V

    .line 347
    .line 348
    .line 349
    invoke-virtual {v13, v1}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 350
    .line 351
    .line 352
    invoke-direct {v6, v13, v7}, Lcom/intsig/office/fc/doc/DOCXReader;->processArrow(Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 353
    .line 354
    .line 355
    new-instance v2, Landroid/graphics/Path;

    .line 356
    .line 357
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 358
    .line 359
    .line 360
    if-eqz v11, :cond_12

    .line 361
    .line 362
    invoke-virtual {v11}, Lcom/intsig/office/common/borders/Border;->getLineWidth()I

    .line 363
    .line 364
    .line 365
    move-result v3

    .line 366
    goto :goto_8

    .line 367
    :cond_12
    const/4 v3, 0x1

    .line 368
    :goto_8
    const/16 v14, 0x14

    .line 369
    .line 370
    const-string v15, "to"

    .line 371
    .line 372
    const-string v0, "from"

    .line 373
    .line 374
    if-ne v1, v14, :cond_18

    .line 375
    .line 376
    if-eqz v11, :cond_13

    .line 377
    .line 378
    invoke-virtual {v11}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 379
    .line 380
    .line 381
    move-result-object v10

    .line 382
    :cond_13
    invoke-interface {v7, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 383
    .line 384
    .line 385
    move-result-object v0

    .line 386
    invoke-direct {v6, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->processPoints(Ljava/lang/String;)[Landroid/graphics/PointF;

    .line 387
    .line 388
    .line 389
    move-result-object v0

    .line 390
    aget-object v0, v0, v9

    .line 391
    .line 392
    invoke-interface {v7, v15}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 393
    .line 394
    .line 395
    move-result-object v1

    .line 396
    invoke-direct {v6, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->processPoints(Ljava/lang/String;)[Landroid/graphics/PointF;

    .line 397
    .line 398
    .line 399
    move-result-object v1

    .line 400
    aget-object v1, v1, v9

    .line 401
    .line 402
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getStartArrowhead()Z

    .line 403
    .line 404
    .line 405
    move-result v4

    .line 406
    if-eqz v4, :cond_14

    .line 407
    .line 408
    iget v14, v1, Landroid/graphics/PointF;->x:F

    .line 409
    .line 410
    iget v15, v1, Landroid/graphics/PointF;->y:F

    .line 411
    .line 412
    iget v4, v0, Landroid/graphics/PointF;->x:F

    .line 413
    .line 414
    iget v5, v0, Landroid/graphics/PointF;->y:F

    .line 415
    .line 416
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 417
    .line 418
    .line 419
    move-result-object v18

    .line 420
    move/from16 v16, v4

    .line 421
    .line 422
    move/from16 v17, v5

    .line 423
    .line 424
    move/from16 v19, v3

    .line 425
    .line 426
    invoke-static/range {v14 .. v19}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 427
    .line 428
    .line 429
    move-result-object v4

    .line 430
    invoke-virtual {v4}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 431
    .line 432
    .line 433
    move-result-object v5

    .line 434
    invoke-virtual {v4}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 435
    .line 436
    .line 437
    move-result-object v4

    .line 438
    goto :goto_9

    .line 439
    :cond_14
    const/4 v4, 0x0

    .line 440
    const/4 v5, 0x0

    .line 441
    :goto_9
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowhead()Z

    .line 442
    .line 443
    .line 444
    move-result v14

    .line 445
    if-eqz v14, :cond_15

    .line 446
    .line 447
    iget v14, v0, Landroid/graphics/PointF;->x:F

    .line 448
    .line 449
    iget v15, v0, Landroid/graphics/PointF;->y:F

    .line 450
    .line 451
    iget v12, v1, Landroid/graphics/PointF;->x:F

    .line 452
    .line 453
    iget v9, v1, Landroid/graphics/PointF;->y:F

    .line 454
    .line 455
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 456
    .line 457
    .line 458
    move-result-object v18

    .line 459
    move/from16 v16, v12

    .line 460
    .line 461
    move/from16 v17, v9

    .line 462
    .line 463
    move/from16 v19, v3

    .line 464
    .line 465
    invoke-static/range {v14 .. v19}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 466
    .line 467
    .line 468
    move-result-object v3

    .line 469
    invoke-virtual {v3}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 470
    .line 471
    .line 472
    move-result-object v9

    .line 473
    invoke-virtual {v3}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 474
    .line 475
    .line 476
    move-result-object v3

    .line 477
    goto :goto_a

    .line 478
    :cond_15
    const/4 v3, 0x0

    .line 479
    const/4 v9, 0x0

    .line 480
    :goto_a
    if-eqz v4, :cond_16

    .line 481
    .line 482
    iget v12, v0, Landroid/graphics/PointF;->x:F

    .line 483
    .line 484
    iget v0, v0, Landroid/graphics/PointF;->y:F

    .line 485
    .line 486
    iget v14, v4, Landroid/graphics/PointF;->x:F

    .line 487
    .line 488
    iget v4, v4, Landroid/graphics/PointF;->y:F

    .line 489
    .line 490
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 491
    .line 492
    .line 493
    move-result-object v15

    .line 494
    invoke-virtual {v15}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 495
    .line 496
    .line 497
    move-result v15

    .line 498
    invoke-static {v12, v0, v14, v4, v15}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    .line 499
    .line 500
    .line 501
    move-result-object v0

    .line 502
    :cond_16
    if-eqz v3, :cond_17

    .line 503
    .line 504
    iget v4, v1, Landroid/graphics/PointF;->x:F

    .line 505
    .line 506
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 507
    .line 508
    iget v12, v3, Landroid/graphics/PointF;->x:F

    .line 509
    .line 510
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 511
    .line 512
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 513
    .line 514
    .line 515
    move-result-object v14

    .line 516
    invoke-virtual {v14}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 517
    .line 518
    .line 519
    move-result v14

    .line 520
    invoke-static {v4, v1, v12, v3, v14}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    .line 521
    .line 522
    .line 523
    move-result-object v1

    .line 524
    :cond_17
    iget v3, v0, Landroid/graphics/PointF;->x:F

    .line 525
    .line 526
    iget v0, v0, Landroid/graphics/PointF;->y:F

    .line 527
    .line 528
    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 529
    .line 530
    .line 531
    iget v0, v1, Landroid/graphics/PointF;->x:F

    .line 532
    .line 533
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 534
    .line 535
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 536
    .line 537
    .line 538
    move-object v1, v5

    .line 539
    move-object/from16 v24, v9

    .line 540
    .line 541
    move-object/from16 v26, v11

    .line 542
    .line 543
    move-object/from16 v27, v7

    .line 544
    .line 545
    move-object v7, v6

    .line 546
    move-object/from16 v6, v27

    .line 547
    .line 548
    goto/16 :goto_10

    .line 549
    .line 550
    :cond_18
    if-ne v1, v5, :cond_1d

    .line 551
    .line 552
    invoke-interface {v7, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 553
    .line 554
    .line 555
    move-result-object v0

    .line 556
    invoke-direct {v6, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->processPoints(Ljava/lang/String;)[Landroid/graphics/PointF;

    .line 557
    .line 558
    .line 559
    move-result-object v0

    .line 560
    const/4 v1, 0x0

    .line 561
    aget-object v0, v0, v1

    .line 562
    .line 563
    const-string v4, "control1"

    .line 564
    .line 565
    invoke-interface {v7, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 566
    .line 567
    .line 568
    move-result-object v4

    .line 569
    invoke-direct {v6, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->processPoints(Ljava/lang/String;)[Landroid/graphics/PointF;

    .line 570
    .line 571
    .line 572
    move-result-object v4

    .line 573
    aget-object v4, v4, v1

    .line 574
    .line 575
    const-string v5, "control2"

    .line 576
    .line 577
    invoke-interface {v7, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 578
    .line 579
    .line 580
    move-result-object v5

    .line 581
    invoke-direct {v6, v5}, Lcom/intsig/office/fc/doc/DOCXReader;->processPoints(Ljava/lang/String;)[Landroid/graphics/PointF;

    .line 582
    .line 583
    .line 584
    move-result-object v5

    .line 585
    aget-object v5, v5, v1

    .line 586
    .line 587
    invoke-interface {v7, v15}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 588
    .line 589
    .line 590
    move-result-object v9

    .line 591
    invoke-direct {v6, v9}, Lcom/intsig/office/fc/doc/DOCXReader;->processPoints(Ljava/lang/String;)[Landroid/graphics/PointF;

    .line 592
    .line 593
    .line 594
    move-result-object v9

    .line 595
    aget-object v9, v9, v1

    .line 596
    .line 597
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getStartArrowhead()Z

    .line 598
    .line 599
    .line 600
    move-result v1

    .line 601
    if-eqz v1, :cond_19

    .line 602
    .line 603
    iget v14, v9, Landroid/graphics/PointF;->x:F

    .line 604
    .line 605
    iget v15, v9, Landroid/graphics/PointF;->y:F

    .line 606
    .line 607
    iget v1, v5, Landroid/graphics/PointF;->x:F

    .line 608
    .line 609
    iget v12, v5, Landroid/graphics/PointF;->y:F

    .line 610
    .line 611
    move-object/from16 v25, v10

    .line 612
    .line 613
    iget v10, v4, Landroid/graphics/PointF;->x:F

    .line 614
    .line 615
    iget v8, v4, Landroid/graphics/PointF;->y:F

    .line 616
    .line 617
    move-object/from16 v26, v11

    .line 618
    .line 619
    iget v11, v0, Landroid/graphics/PointF;->x:F

    .line 620
    .line 621
    iget v6, v0, Landroid/graphics/PointF;->y:F

    .line 622
    .line 623
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 624
    .line 625
    .line 626
    move-result-object v22

    .line 627
    move/from16 v16, v1

    .line 628
    .line 629
    move/from16 v17, v12

    .line 630
    .line 631
    move/from16 v18, v10

    .line 632
    .line 633
    move/from16 v19, v8

    .line 634
    .line 635
    move/from16 v20, v11

    .line 636
    .line 637
    move/from16 v21, v6

    .line 638
    .line 639
    move/from16 v23, v3

    .line 640
    .line 641
    invoke-static/range {v14 .. v23}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getCubicBezArrowPath(FFFFFFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 642
    .line 643
    .line 644
    move-result-object v1

    .line 645
    invoke-virtual {v1}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 646
    .line 647
    .line 648
    move-result-object v6

    .line 649
    invoke-virtual {v1}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 650
    .line 651
    .line 652
    move-result-object v1

    .line 653
    goto :goto_b

    .line 654
    :cond_19
    move-object/from16 v25, v10

    .line 655
    .line 656
    move-object/from16 v26, v11

    .line 657
    .line 658
    const/4 v1, 0x0

    .line 659
    const/4 v6, 0x0

    .line 660
    :goto_b
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowhead()Z

    .line 661
    .line 662
    .line 663
    move-result v8

    .line 664
    if-eqz v8, :cond_1a

    .line 665
    .line 666
    iget v14, v0, Landroid/graphics/PointF;->x:F

    .line 667
    .line 668
    iget v15, v0, Landroid/graphics/PointF;->y:F

    .line 669
    .line 670
    iget v8, v4, Landroid/graphics/PointF;->x:F

    .line 671
    .line 672
    iget v10, v4, Landroid/graphics/PointF;->y:F

    .line 673
    .line 674
    iget v11, v5, Landroid/graphics/PointF;->x:F

    .line 675
    .line 676
    iget v12, v5, Landroid/graphics/PointF;->y:F

    .line 677
    .line 678
    move-object/from16 p6, v6

    .line 679
    .line 680
    iget v6, v9, Landroid/graphics/PointF;->x:F

    .line 681
    .line 682
    iget v7, v9, Landroid/graphics/PointF;->y:F

    .line 683
    .line 684
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 685
    .line 686
    .line 687
    move-result-object v22

    .line 688
    move/from16 v16, v8

    .line 689
    .line 690
    move/from16 v17, v10

    .line 691
    .line 692
    move/from16 v18, v11

    .line 693
    .line 694
    move/from16 v19, v12

    .line 695
    .line 696
    move/from16 v20, v6

    .line 697
    .line 698
    move/from16 v21, v7

    .line 699
    .line 700
    move/from16 v23, v3

    .line 701
    .line 702
    invoke-static/range {v14 .. v23}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getCubicBezArrowPath(FFFFFFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 703
    .line 704
    .line 705
    move-result-object v3

    .line 706
    invoke-virtual {v3}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 707
    .line 708
    .line 709
    move-result-object v6

    .line 710
    invoke-virtual {v3}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 711
    .line 712
    .line 713
    move-result-object v3

    .line 714
    goto :goto_c

    .line 715
    :cond_1a
    move-object/from16 p6, v6

    .line 716
    .line 717
    const/4 v3, 0x0

    .line 718
    const/4 v6, 0x0

    .line 719
    :goto_c
    if-eqz v1, :cond_1b

    .line 720
    .line 721
    iget v7, v0, Landroid/graphics/PointF;->x:F

    .line 722
    .line 723
    iget v0, v0, Landroid/graphics/PointF;->y:F

    .line 724
    .line 725
    iget v8, v1, Landroid/graphics/PointF;->x:F

    .line 726
    .line 727
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 728
    .line 729
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 730
    .line 731
    .line 732
    move-result-object v10

    .line 733
    invoke-virtual {v10}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 734
    .line 735
    .line 736
    move-result v10

    .line 737
    invoke-static {v7, v0, v8, v1, v10}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    .line 738
    .line 739
    .line 740
    move-result-object v0

    .line 741
    :cond_1b
    if-eqz v3, :cond_1c

    .line 742
    .line 743
    iget v1, v9, Landroid/graphics/PointF;->x:F

    .line 744
    .line 745
    iget v7, v9, Landroid/graphics/PointF;->y:F

    .line 746
    .line 747
    iget v8, v3, Landroid/graphics/PointF;->x:F

    .line 748
    .line 749
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 750
    .line 751
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 752
    .line 753
    .line 754
    move-result-object v9

    .line 755
    invoke-virtual {v9}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 756
    .line 757
    .line 758
    move-result v9

    .line 759
    invoke-static {v1, v7, v8, v3, v9}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    .line 760
    .line 761
    .line 762
    move-result-object v9

    .line 763
    :cond_1c
    iget v1, v0, Landroid/graphics/PointF;->x:F

    .line 764
    .line 765
    iget v0, v0, Landroid/graphics/PointF;->y:F

    .line 766
    .line 767
    invoke-virtual {v2, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 768
    .line 769
    .line 770
    iget v15, v4, Landroid/graphics/PointF;->x:F

    .line 771
    .line 772
    iget v0, v4, Landroid/graphics/PointF;->y:F

    .line 773
    .line 774
    iget v1, v5, Landroid/graphics/PointF;->x:F

    .line 775
    .line 776
    iget v3, v5, Landroid/graphics/PointF;->y:F

    .line 777
    .line 778
    iget v4, v9, Landroid/graphics/PointF;->x:F

    .line 779
    .line 780
    iget v5, v9, Landroid/graphics/PointF;->y:F

    .line 781
    .line 782
    move-object v14, v2

    .line 783
    move/from16 v16, v0

    .line 784
    .line 785
    move/from16 v17, v1

    .line 786
    .line 787
    move/from16 v18, v3

    .line 788
    .line 789
    move/from16 v19, v4

    .line 790
    .line 791
    move/from16 v20, v5

    .line 792
    .line 793
    invoke-virtual/range {v14 .. v20}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 794
    .line 795
    .line 796
    move-object/from16 v7, p0

    .line 797
    .line 798
    move-object/from16 v1, p6

    .line 799
    .line 800
    move-object/from16 v24, v6

    .line 801
    .line 802
    move-object/from16 v10, v25

    .line 803
    .line 804
    move-object/from16 v6, p1

    .line 805
    .line 806
    goto/16 :goto_10

    .line 807
    .line 808
    :cond_1d
    move-object/from16 v25, v10

    .line 809
    .line 810
    move-object/from16 v26, v11

    .line 811
    .line 812
    if-ne v1, v4, :cond_23

    .line 813
    .line 814
    const-string v0, "points"

    .line 815
    .line 816
    move-object/from16 v6, p1

    .line 817
    .line 818
    invoke-interface {v6, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 819
    .line 820
    .line 821
    move-result-object v0

    .line 822
    move-object/from16 v7, p0

    .line 823
    .line 824
    invoke-direct {v7, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->processPoints(Ljava/lang/String;)[Landroid/graphics/PointF;

    .line 825
    .line 826
    .line 827
    move-result-object v0

    .line 828
    array-length v1, v0

    .line 829
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getStartArrowhead()Z

    .line 830
    .line 831
    .line 832
    move-result v4

    .line 833
    if-eqz v4, :cond_1e

    .line 834
    .line 835
    const/4 v4, 0x1

    .line 836
    aget-object v5, v0, v4

    .line 837
    .line 838
    iget v14, v5, Landroid/graphics/PointF;->x:F

    .line 839
    .line 840
    iget v15, v5, Landroid/graphics/PointF;->y:F

    .line 841
    .line 842
    const/4 v4, 0x0

    .line 843
    aget-object v5, v0, v4

    .line 844
    .line 845
    iget v4, v5, Landroid/graphics/PointF;->x:F

    .line 846
    .line 847
    iget v5, v5, Landroid/graphics/PointF;->y:F

    .line 848
    .line 849
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 850
    .line 851
    .line 852
    move-result-object v18

    .line 853
    move/from16 v16, v4

    .line 854
    .line 855
    move/from16 v17, v5

    .line 856
    .line 857
    move/from16 v19, v3

    .line 858
    .line 859
    invoke-static/range {v14 .. v19}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 860
    .line 861
    .line 862
    move-result-object v4

    .line 863
    invoke-virtual {v4}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 864
    .line 865
    .line 866
    move-result-object v5

    .line 867
    invoke-virtual {v4}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 868
    .line 869
    .line 870
    move-result-object v4

    .line 871
    goto :goto_d

    .line 872
    :cond_1e
    const/4 v4, 0x0

    .line 873
    const/4 v5, 0x0

    .line 874
    :goto_d
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowhead()Z

    .line 875
    .line 876
    .line 877
    move-result v8

    .line 878
    if-eqz v8, :cond_1f

    .line 879
    .line 880
    add-int/lit8 v8, v1, -0x2

    .line 881
    .line 882
    aget-object v8, v0, v8

    .line 883
    .line 884
    iget v14, v8, Landroid/graphics/PointF;->x:F

    .line 885
    .line 886
    iget v15, v8, Landroid/graphics/PointF;->y:F

    .line 887
    .line 888
    add-int/lit8 v8, v1, -0x1

    .line 889
    .line 890
    aget-object v8, v0, v8

    .line 891
    .line 892
    iget v9, v8, Landroid/graphics/PointF;->x:F

    .line 893
    .line 894
    iget v8, v8, Landroid/graphics/PointF;->y:F

    .line 895
    .line 896
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 897
    .line 898
    .line 899
    move-result-object v18

    .line 900
    move/from16 v16, v9

    .line 901
    .line 902
    move/from16 v17, v8

    .line 903
    .line 904
    move/from16 v19, v3

    .line 905
    .line 906
    invoke-static/range {v14 .. v19}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 907
    .line 908
    .line 909
    move-result-object v3

    .line 910
    invoke-virtual {v3}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 911
    .line 912
    .line 913
    move-result-object v8

    .line 914
    invoke-virtual {v3}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 915
    .line 916
    .line 917
    move-result-object v3

    .line 918
    move-object/from16 v24, v8

    .line 919
    .line 920
    goto :goto_e

    .line 921
    :cond_1f
    const/4 v3, 0x0

    .line 922
    const/16 v24, 0x0

    .line 923
    .line 924
    :goto_e
    if-eqz v4, :cond_20

    .line 925
    .line 926
    const/4 v8, 0x0

    .line 927
    aget-object v9, v0, v8

    .line 928
    .line 929
    iget v10, v9, Landroid/graphics/PointF;->x:F

    .line 930
    .line 931
    iget v9, v9, Landroid/graphics/PointF;->y:F

    .line 932
    .line 933
    iget v11, v4, Landroid/graphics/PointF;->x:F

    .line 934
    .line 935
    iget v4, v4, Landroid/graphics/PointF;->y:F

    .line 936
    .line 937
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 938
    .line 939
    .line 940
    move-result-object v12

    .line 941
    invoke-virtual {v12}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 942
    .line 943
    .line 944
    move-result v12

    .line 945
    invoke-static {v10, v9, v11, v4, v12}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    .line 946
    .line 947
    .line 948
    move-result-object v4

    .line 949
    aput-object v4, v0, v8

    .line 950
    .line 951
    :cond_20
    if-eqz v3, :cond_21

    .line 952
    .line 953
    const/4 v4, 0x1

    .line 954
    sub-int/2addr v1, v4

    .line 955
    aget-object v4, v0, v1

    .line 956
    .line 957
    iget v8, v4, Landroid/graphics/PointF;->x:F

    .line 958
    .line 959
    iget v4, v4, Landroid/graphics/PointF;->y:F

    .line 960
    .line 961
    iget v9, v3, Landroid/graphics/PointF;->x:F

    .line 962
    .line 963
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 964
    .line 965
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 966
    .line 967
    .line 968
    move-result-object v10

    .line 969
    invoke-virtual {v10}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 970
    .line 971
    .line 972
    move-result v10

    .line 973
    invoke-static {v8, v4, v9, v3, v10}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    .line 974
    .line 975
    .line 976
    move-result-object v3

    .line 977
    aput-object v3, v0, v1

    .line 978
    .line 979
    :cond_21
    const/4 v1, 0x0

    .line 980
    aget-object v3, v0, v1

    .line 981
    .line 982
    iget v1, v3, Landroid/graphics/PointF;->x:F

    .line 983
    .line 984
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 985
    .line 986
    invoke-virtual {v2, v1, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 987
    .line 988
    .line 989
    const/4 v1, 0x1

    .line 990
    :goto_f
    array-length v3, v0

    .line 991
    if-ge v1, v3, :cond_22

    .line 992
    .line 993
    aget-object v3, v0, v1

    .line 994
    .line 995
    iget v4, v3, Landroid/graphics/PointF;->x:F

    .line 996
    .line 997
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 998
    .line 999
    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1000
    .line 1001
    .line 1002
    add-int/lit8 v1, v1, 0x1

    .line 1003
    .line 1004
    goto :goto_f

    .line 1005
    :cond_22
    move-object v1, v5

    .line 1006
    move-object/from16 v10, v25

    .line 1007
    .line 1008
    goto :goto_10

    .line 1009
    :cond_23
    move-object/from16 v7, p0

    .line 1010
    .line 1011
    move-object/from16 v6, p1

    .line 1012
    .line 1013
    move-object/from16 v10, v25

    .line 1014
    .line 1015
    const/4 v1, 0x0

    .line 1016
    const/16 v24, 0x0

    .line 1017
    .line 1018
    :goto_10
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 1019
    .line 1020
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 1021
    .line 1022
    .line 1023
    invoke-virtual {v0, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 1024
    .line 1025
    .line 1026
    move-object/from16 v8, v26

    .line 1027
    .line 1028
    if-eqz v26, :cond_24

    .line 1029
    .line 1030
    invoke-virtual {v0, v8}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 1031
    .line 1032
    .line 1033
    :cond_24
    if-eqz v10, :cond_25

    .line 1034
    .line 1035
    invoke-virtual {v0, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 1036
    .line 1037
    .line 1038
    :cond_25
    invoke-virtual {v13, v0}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 1039
    .line 1040
    .line 1041
    if-eqz v1, :cond_26

    .line 1042
    .line 1043
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 1044
    .line 1045
    .line 1046
    move-result-object v0

    .line 1047
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 1048
    .line 1049
    .line 1050
    move-result v5

    .line 1051
    const/4 v4, 0x1

    .line 1052
    move-object/from16 v0, p0

    .line 1053
    .line 1054
    move-object v2, v10

    .line 1055
    move-object v3, v8

    .line 1056
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/doc/DOCXReader;->getArrowExtendPath(Landroid/graphics/Path;Lcom/intsig/office/common/bg/BackgroundAndFill;Lcom/intsig/office/common/borders/Line;ZB)Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 1057
    .line 1058
    .line 1059
    move-result-object v0

    .line 1060
    invoke-virtual {v13, v0}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 1061
    .line 1062
    .line 1063
    :cond_26
    if-eqz v24, :cond_29

    .line 1064
    .line 1065
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    .line 1066
    .line 1067
    .line 1068
    move-result-object v0

    .line 1069
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 1070
    .line 1071
    .line 1072
    move-result v5

    .line 1073
    const/4 v4, 0x1

    .line 1074
    move-object/from16 v0, p0

    .line 1075
    .line 1076
    move-object/from16 v1, v24

    .line 1077
    .line 1078
    move-object v2, v10

    .line 1079
    move-object v3, v8

    .line 1080
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/doc/DOCXReader;->getArrowExtendPath(Landroid/graphics/Path;Lcom/intsig/office/common/bg/BackgroundAndFill;Lcom/intsig/office/common/borders/Line;ZB)Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 1081
    .line 1082
    .line 1083
    move-result-object v0

    .line 1084
    invoke-virtual {v13, v0}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 1085
    .line 1086
    .line 1087
    goto :goto_12

    .line 1088
    :cond_27
    :goto_11
    move-object v8, v11

    .line 1089
    move-object/from16 v27, v7

    .line 1090
    .line 1091
    move-object v7, v6

    .line 1092
    move-object/from16 v6, v27

    .line 1093
    .line 1094
    new-instance v13, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 1095
    .line 1096
    invoke-direct {v13}, Lcom/intsig/office/common/shape/WPAutoShape;-><init>()V

    .line 1097
    .line 1098
    .line 1099
    invoke-virtual {v13, v1}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 1100
    .line 1101
    .line 1102
    invoke-virtual {v13, v8}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 1103
    .line 1104
    .line 1105
    invoke-direct {v7, v13, v6}, Lcom/intsig/office/fc/doc/DOCXReader;->processArrow(Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 1106
    .line 1107
    .line 1108
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 1109
    .line 1110
    .line 1111
    move-result v0

    .line 1112
    if-ne v0, v4, :cond_28

    .line 1113
    .line 1114
    if-nez v3, :cond_28

    .line 1115
    .line 1116
    const/4 v0, 0x1

    .line 1117
    new-array v1, v0, [Ljava/lang/Float;

    .line 1118
    .line 1119
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1120
    .line 1121
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 1122
    .line 1123
    .line 1124
    move-result-object v0

    .line 1125
    const/4 v2, 0x0

    .line 1126
    aput-object v0, v1, v2

    .line 1127
    .line 1128
    invoke-virtual {v13, v1}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 1129
    .line 1130
    .line 1131
    goto :goto_12

    .line 1132
    :cond_28
    invoke-virtual {v13, v3}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 1133
    .line 1134
    .line 1135
    :cond_29
    :goto_12
    const/4 v0, 0x1

    .line 1136
    invoke-virtual {v13, v0}, Lcom/intsig/office/common/shape/AutoShape;->setAuotShape07(Z)V

    .line 1137
    .line 1138
    .line 1139
    move-object/from16 v8, p3

    .line 1140
    .line 1141
    if-nez v8, :cond_2a

    .line 1142
    .line 1143
    invoke-direct/range {p0 .. p1}, Lcom/intsig/office/fc/doc/DOCXReader;->getShapeWrapType(Lcom/intsig/office/fc/dom4j/Element;)S

    .line 1144
    .line 1145
    .line 1146
    move-result v0

    .line 1147
    invoke-virtual {v13, v0}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 1148
    .line 1149
    .line 1150
    goto :goto_13

    .line 1151
    :cond_2a
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/common/shape/WPGroupShape;->getWrapType()S

    .line 1152
    .line 1153
    .line 1154
    move-result v0

    .line 1155
    invoke-virtual {v13, v0}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 1156
    .line 1157
    .line 1158
    :goto_13
    move-object/from16 v0, p0

    .line 1159
    .line 1160
    move-object/from16 v1, p1

    .line 1161
    .line 1162
    move-object v2, v13

    .line 1163
    move-object/from16 v3, p3

    .line 1164
    .line 1165
    move/from16 v4, p4

    .line 1166
    .line 1167
    move/from16 v5, p5

    .line 1168
    .line 1169
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/doc/DOCXReader;->processAutoShapeStyle(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/AbstractShape;Lcom/intsig/office/common/shape/GroupShape;FF)Lcom/intsig/office/java/awt/Rectangle;

    .line 1170
    .line 1171
    .line 1172
    invoke-virtual {v7, v13}, Lcom/intsig/office/fc/doc/DOCXReader;->processRotation(Lcom/intsig/office/common/shape/AutoShape;)V

    .line 1173
    .line 1174
    .line 1175
    iget-boolean v0, v7, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessWatermark:Z

    .line 1176
    .line 1177
    if-eqz v0, :cond_2b

    .line 1178
    .line 1179
    move-object v0, v13

    .line 1180
    check-cast v0, Lcom/intsig/office/common/shape/WatermarkShape;

    .line 1181
    .line 1182
    invoke-direct {v7, v0, v6}, Lcom/intsig/office/fc/doc/DOCXReader;->processWatermark(Lcom/intsig/office/common/shape/WatermarkShape;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 1183
    .line 1184
    .line 1185
    const/4 v0, 0x0

    .line 1186
    iput-boolean v0, v7, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessWatermark:Z

    .line 1187
    .line 1188
    :cond_2b
    if-nez v8, :cond_2c

    .line 1189
    .line 1190
    move-object/from16 v0, p2

    .line 1191
    .line 1192
    invoke-direct {v7, v13, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->addShape(Lcom/intsig/office/common/shape/AbstractShape;Lcom/intsig/office/simpletext/model/ParagraphElement;)V

    .line 1193
    .line 1194
    .line 1195
    goto :goto_14

    .line 1196
    :cond_2c
    invoke-virtual {v8, v13}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 1197
    .line 1198
    .line 1199
    :goto_14
    return-object v13

    .line 1200
    :cond_2d
    move-object v7, v6

    .line 1201
    return-object v0
.end method

.method private processAutoShape2010(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/WPGroupShape;FFIIZ)Lcom/intsig/office/common/shape/AbstractShape;
    .locals 20

    move-object/from16 v11, p0

    move-object/from16 v0, p1

    move-object/from16 v9, p3

    move-object/from16 v12, p4

    move/from16 v13, p5

    move/from16 v14, p6

    if-eqz v9, :cond_13

    .line 2
    invoke-interface/range {p3 .. p3}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "wsp"

    .line 3
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const-string v4, "grpSp"

    const-string v5, "grpSpPr"

    const-string v6, "wgp"

    const-string v7, "pic"

    const-string v8, "sp"

    if-nez v3, :cond_3

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    .line 4
    :cond_0
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_2

    .line 5
    :cond_2
    :goto_0
    invoke-interface {v9, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    goto :goto_2

    :cond_3
    :goto_1
    const-string v3, "spPr"

    .line 6
    invoke-interface {v9, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    :goto_2
    const-string v15, "xfrm"

    if-eqz v3, :cond_6

    .line 7
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v10

    invoke-interface {v3, v15}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    invoke-virtual {v10, v3, v13, v14}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getShapeAnchor(Lcom/intsig/office/fc/dom4j/Element;FF)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v3

    if-eqz v3, :cond_5

    if-eqz p9, :cond_4

    .line 8
    iget v10, v3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    add-int v10, v10, p7

    iput v10, v3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 9
    iget v10, v3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    add-int v10, v10, p8

    iput v10, v3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 10
    :cond_4
    invoke-direct {v11, v12, v3}, Lcom/intsig/office/fc/doc/DOCXReader;->processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v3

    :cond_5
    move-object v10, v3

    goto :goto_3

    :cond_6
    const/4 v10, 0x0

    :goto_3
    if-eqz v10, :cond_b

    .line 11
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 12
    :cond_7
    invoke-interface {v9, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 13
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v2

    invoke-interface {v1, v15}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getAnchorFitZoom(Lcom/intsig/office/fc/dom4j/Element;)[F

    move-result-object v16

    .line 14
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v2

    invoke-interface {v1, v15}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    const/4 v15, 0x0

    aget v4, v16, v15

    mul-float v4, v4, v13

    const/16 v17, 0x1

    aget v5, v16, v17

    mul-float v5, v5, v14

    invoke-virtual {v2, v3, v4, v5}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getChildShapeAnchor(Lcom/intsig/office/fc/dom4j/Element;FF)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v2

    if-eqz p9, :cond_8

    .line 15
    iget v3, v2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    add-int v3, v3, p7

    iput v3, v2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 16
    iget v3, v2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    add-int v3, v3, p8

    iput v3, v2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 17
    :cond_8
    new-instance v8, Lcom/intsig/office/common/shape/WPGroupShape;

    invoke-direct {v8}, Lcom/intsig/office/common/shape/WPGroupShape;-><init>()V

    .line 18
    iget v3, v10, Lcom/intsig/office/java/awt/Rectangle;->x:I

    iget v4, v2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    sub-int/2addr v3, v4

    iget v4, v10, Lcom/intsig/office/java/awt/Rectangle;->y:I

    iget v2, v2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    sub-int/2addr v4, v2

    invoke-virtual {v8, v3, v4}, Lcom/intsig/office/common/shape/GroupShape;->setOffPostion(II)V

    .line 19
    invoke-virtual {v8, v10}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 20
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v2

    invoke-virtual {v2, v1, v8}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->processRotation(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/IShape;)V

    .line 21
    invoke-interface/range {p3 .. p3}, Lcom/intsig/office/fc/dom4j/Element;->elementIterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_4
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 22
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    aget v1, v16, v15

    mul-float v6, v1, v13

    aget v1, v16, v17

    mul-float v7, v1, v14

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v19, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object v5, v8

    move-object v15, v8

    move v8, v9

    move v9, v10

    move/from16 v10, v19

    invoke-direct/range {v1 .. v10}, Lcom/intsig/office/fc/doc/DOCXReader;->processAutoShape2010(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/WPGroupShape;FFIIZ)Lcom/intsig/office/common/shape/AbstractShape;

    move-object v8, v15

    const/4 v15, 0x0

    goto :goto_4

    :cond_9
    move-object v15, v8

    if-nez v12, :cond_a

    .line 23
    new-instance v10, Lcom/intsig/office/common/shape/WPAutoShape;

    invoke-direct {v10}, Lcom/intsig/office/common/shape/WPAutoShape;-><init>()V

    .line 24
    invoke-virtual {v10, v15}, Lcom/intsig/office/common/shape/WPAutoShape;->addGroupShape(Lcom/intsig/office/common/shape/WPGroupShape;)V

    goto/16 :goto_7

    :cond_a
    move-object v10, v15

    goto/16 :goto_7

    :cond_b
    if-eqz v10, :cond_f

    .line 25
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    goto :goto_5

    .line 26
    :cond_c
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 27
    invoke-direct {v11, v9, v10}, Lcom/intsig/office/fc/doc/DOCXReader;->addPicture(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/shape/PictureShape;

    move-result-object v0

    move-object v10, v0

    goto :goto_7

    .line 28
    :cond_d
    :goto_5
    :try_start_0
    iget-boolean v1, v11, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessHF:Z

    if-eqz v1, :cond_e

    iget-object v3, v11, Lcom/intsig/office/fc/doc/DOCXReader;->hfPart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    if-eqz v3, :cond_e

    .line 29
    iget-object v1, v11, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    iget-object v2, v11, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    iget-object v6, v11, Lcom/intsig/office/fc/doc/DOCXReader;->themeColor:Ljava/util/Map;

    const/4 v7, 0x0

    .line 30
    invoke-direct {v11, v9}, Lcom/intsig/office/fc/doc/DOCXReader;->hasTextbox2010(Lcom/intsig/office/fc/dom4j/Element;)Z

    move-result v8

    move-object/from16 v4, p3

    move-object v5, v10

    .line 31
    invoke-static/range {v1 .. v8}, Lcom/intsig/office/common/autoshape/AutoShapeDataKit;->getAutoShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;Ljava/util/Map;IZ)Lcom/intsig/office/common/shape/AbstractShape;

    move-result-object v10
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v10, :cond_10

    .line 32
    :try_start_1
    move-object v1, v10

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    invoke-direct {v11, v0, v1, v9}, Lcom/intsig/office/fc/doc/DOCXReader;->processTextbox2010(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/fc/dom4j/Element;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_7

    .line 33
    :cond_e
    :try_start_2
    iget-object v1, v11, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    iget-object v2, v11, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    iget-object v6, v11, Lcom/intsig/office/fc/doc/DOCXReader;->themeColor:Ljava/util/Map;

    const/4 v7, 0x0

    .line 34
    invoke-direct {v11, v9}, Lcom/intsig/office/fc/doc/DOCXReader;->hasTextbox2010(Lcom/intsig/office/fc/dom4j/Element;)Z

    move-result v8

    move-object/from16 v3, p1

    move-object/from16 v4, p3

    move-object v5, v10

    .line 35
    invoke-static/range {v1 .. v8}, Lcom/intsig/office/common/autoshape/AutoShapeDataKit;->getAutoShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;Ljava/util/Map;IZ)Lcom/intsig/office/common/shape/AbstractShape;

    move-result-object v10
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz v10, :cond_10

    .line 36
    :try_start_3
    move-object v1, v10

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    invoke-direct {v11, v0, v1, v9}, Lcom/intsig/office/fc/doc/DOCXReader;->processTextbox2010(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/fc/dom4j/Element;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_7

    :catch_0
    move-exception v0

    goto :goto_6

    :catch_1
    move-exception v0

    const/4 v10, 0x0

    .line 37
    :goto_6
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_7

    :cond_f
    const/4 v10, 0x0

    :cond_10
    :goto_7
    if-eqz v10, :cond_14

    if-nez v12, :cond_11

    move-object/from16 v1, p2

    .line 38
    invoke-direct {v11, v10, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->addShape(Lcom/intsig/office/common/shape/AbstractShape;Lcom/intsig/office/simpletext/model/ParagraphElement;)V

    goto :goto_8

    .line 39
    :cond_11
    invoke-virtual {v10, v12}, Lcom/intsig/office/common/shape/AbstractShape;->setParent(Lcom/intsig/office/common/shape/IShape;)V

    .line 40
    instance-of v0, v10, Lcom/intsig/office/common/shape/WPAutoShape;

    if-eqz v0, :cond_12

    .line 41
    move-object v0, v10

    check-cast v0, Lcom/intsig/office/common/shape/WPAutoShape;

    invoke-virtual/range {p4 .. p4}, Lcom/intsig/office/common/shape/WPGroupShape;->getWrapType()S

    move-result v1

    invoke-virtual {v0, v1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 42
    :cond_12
    invoke-virtual {v12, v10}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    goto :goto_8

    :cond_13
    const/4 v10, 0x0

    :cond_14
    :goto_8
    return-object v10
.end method

.method private processAutoShape2010(Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/WPGroupShape;FFIIZ)Lcom/intsig/office/common/shape/AbstractShape;
    .locals 11

    move-object v10, p0

    .line 1
    iget-object v1, v10, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/intsig/office/fc/doc/DOCXReader;->processAutoShape2010(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/WPGroupShape;FFIIZ)Lcom/intsig/office/common/shape/AbstractShape;

    move-result-object v0

    return-object v0
.end method

.method private processAutoShapeForPict(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/common/shape/WPGroupShape;FF)V
    .locals 18

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v6, p3

    .line 1
    invoke-interface/range {p1 .. p1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "group"

    .line 2
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "Footer"

    const-string v3, "Header"

    const-string v4, "Genko"

    const-string v5, "id"

    if-eqz v1, :cond_11

    .line 3
    invoke-interface {v8, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 4
    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 5
    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    .line 7
    :cond_1
    new-instance v9, Lcom/intsig/office/common/shape/WPGroupShape;

    invoke-direct {v9}, Lcom/intsig/office/common/shape/WPGroupShape;-><init>()V

    if-nez v6, :cond_2

    .line 8
    new-instance v0, Lcom/intsig/office/common/shape/WPAutoShape;

    invoke-direct {v0}, Lcom/intsig/office/common/shape/WPAutoShape;-><init>()V

    .line 9
    invoke-virtual {v0, v9}, Lcom/intsig/office/common/shape/WPAutoShape;->addGroupShape(Lcom/intsig/office/common/shape/WPGroupShape;)V

    move-object v10, v0

    goto :goto_0

    :cond_2
    move-object v10, v9

    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v2, v10

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    .line 10
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/doc/DOCXReader;->processAutoShapeStyle(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/AbstractShape;Lcom/intsig/office/common/shape/GroupShape;FF)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v0

    if-eqz v0, :cond_10

    const/4 v1, 0x2

    new-array v11, v1, [F

    .line 11
    fill-array-data v11, :array_0

    .line 12
    new-instance v2, Lcom/intsig/office/java/awt/Rectangle;

    invoke-direct {v2}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    const-string v3, "coordorigin"

    .line 13
    invoke-interface {v8, v3}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v4

    const-string v5, ","

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x0

    if-eqz v4, :cond_5

    .line 14
    invoke-interface {v8, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 15
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_5

    .line 16
    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 17
    array-length v4, v3

    if-ne v4, v1, :cond_4

    .line 18
    aget-object v4, v3, v14

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_3

    .line 19
    aget-object v4, v3, v14

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    .line 20
    :goto_1
    aget-object v3, v3, v13

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    goto :goto_2

    .line 21
    :cond_4
    array-length v4, v3

    if-ne v4, v13, :cond_5

    .line 22
    aget-object v3, v3, v14

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    const/4 v3, 0x0

    goto :goto_2

    :cond_5
    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_2
    const-string v15, "coordsize"

    .line 23
    invoke-interface {v8, v15}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v16

    if-eqz v16, :cond_8

    .line 24
    invoke-interface {v8, v15}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_8

    .line 25
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v16

    if-lez v16, :cond_8

    .line 26
    invoke-virtual {v15, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_8

    .line 27
    array-length v15, v5

    if-ne v15, v1, :cond_7

    .line 28
    aget-object v1, v5, v14

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_6

    .line 29
    aget-object v1, v5, v14

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    goto :goto_3

    :cond_6
    const/4 v1, 0x0

    .line 30
    :goto_3
    aget-object v5, v5, v13

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    goto :goto_5

    .line 31
    :cond_7
    array-length v1, v5

    if-ne v1, v13, :cond_8

    .line 32
    aget-object v1, v5, v14

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    goto :goto_4

    :cond_8
    const/4 v1, 0x0

    :goto_4
    const/4 v5, 0x0

    :goto_5
    const/high16 v15, 0x42c00000    # 96.0f

    cmpl-float v16, v1, v12

    if-eqz v16, :cond_9

    cmpl-float v12, v5, v12

    if-eqz v12, :cond_9

    .line 33
    iget v12, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    const v16, 0xdf3e0

    mul-int v12, v12, v16

    int-to-float v12, v12

    div-float/2addr v12, v15

    div-float v12, v12, p4

    div-float/2addr v12, v1

    aput v12, v11, v14

    .line 34
    iget v12, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    mul-int v12, v12, v16

    int-to-float v12, v12

    div-float/2addr v12, v15

    div-float v12, v12, p5

    div-float/2addr v12, v5

    aput v12, v11, v13

    .line 35
    :cond_9
    invoke-direct {v7, v6, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v0

    aget v12, v11, v14

    mul-float v4, v4, v12

    mul-float v4, v4, p4

    mul-float v4, v4, v15

    const v16, 0x495f3e00    # 914400.0f

    div-float v4, v4, v16

    float-to-int v4, v4

    .line 36
    iput v4, v2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    aget v17, v11, v13

    mul-float v3, v3, v17

    mul-float v3, v3, p5

    mul-float v3, v3, v15

    div-float v3, v3, v16

    float-to-int v3, v3

    .line 37
    iput v3, v2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    mul-float v1, v1, v12

    mul-float v1, v1, p4

    mul-float v1, v1, v15

    div-float v1, v1, v16

    float-to-int v1, v1

    .line 38
    iput v1, v2, Lcom/intsig/office/java/awt/Rectangle;->width:I

    mul-float v5, v5, v17

    mul-float v5, v5, p5

    mul-float v5, v5, v15

    div-float v5, v5, v16

    float-to-int v1, v5

    .line 39
    iput v1, v2, Lcom/intsig/office/java/awt/Rectangle;->height:I

    if-nez v6, :cond_a

    .line 40
    iget v1, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    add-int/2addr v4, v1

    iput v4, v2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 41
    iget v1, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 42
    :cond_a
    iget v1, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    iget v3, v2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    sub-int/2addr v1, v3

    iget v3, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    iget v2, v2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    sub-int/2addr v3, v2

    invoke-virtual {v9, v1, v3}, Lcom/intsig/office/common/shape/GroupShape;->setOffPostion(II)V

    .line 43
    invoke-virtual {v9, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 44
    invoke-virtual {v9, v6}, Lcom/intsig/office/common/shape/AbstractShape;->setParent(Lcom/intsig/office/common/shape/IShape;)V

    .line 45
    invoke-virtual {v10}, Lcom/intsig/office/common/shape/AbstractShape;->getRotation()F

    move-result v0

    invoke-virtual {v9, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setRotation(F)V

    .line 46
    invoke-virtual {v10}, Lcom/intsig/office/common/shape/AbstractShape;->getFlipHorizontal()Z

    move-result v0

    invoke-virtual {v9, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setFlipHorizontal(Z)V

    .line 47
    invoke-virtual {v10}, Lcom/intsig/office/common/shape/AbstractShape;->getFlipVertical()Z

    move-result v0

    invoke-virtual {v9, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setFlipVertical(Z)V

    if-nez v6, :cond_b

    .line 48
    invoke-direct/range {p0 .. p1}, Lcom/intsig/office/fc/doc/DOCXReader;->getShapeWrapType(Lcom/intsig/office/fc/dom4j/Element;)S

    move-result v0

    .line 49
    invoke-virtual {v9, v0}, Lcom/intsig/office/common/shape/WPGroupShape;->setWrapType(S)V

    .line 50
    move-object v1, v10

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    invoke-virtual {v1, v0}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    goto :goto_6

    .line 51
    :cond_b
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/common/shape/WPGroupShape;->getWrapType()S

    move-result v0

    invoke-virtual {v9, v0}, Lcom/intsig/office/common/shape/WPGroupShape;->setWrapType(S)V

    .line 52
    :goto_6
    invoke-interface/range {p1 .. p1}, Lcom/intsig/office/fc/dom4j/Element;->elementIterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 53
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    aget v0, v11, v14

    mul-float v4, v0, p4

    aget v0, v11, v13

    mul-float v5, v0, p5

    move-object/from16 v0, p0

    move-object/from16 v2, p2

    move-object v3, v9

    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/doc/DOCXReader;->processAutoShapeForPict(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/common/shape/WPGroupShape;FF)V

    goto :goto_7

    .line 54
    :cond_c
    invoke-virtual {v9}, Lcom/intsig/office/common/shape/GroupShape;->getShapes()[Lcom/intsig/office/common/shape/IShape;

    move-result-object v0

    .line 55
    array-length v1, v0

    :goto_8
    if-ge v14, v1, :cond_e

    aget-object v2, v0, v14

    .line 56
    instance-of v3, v2, Lcom/intsig/office/common/shape/WPAbstractShape;

    if-eqz v3, :cond_d

    instance-of v3, v10, Lcom/intsig/office/common/shape/WPAbstractShape;

    if-eqz v3, :cond_d

    .line 57
    check-cast v2, Lcom/intsig/office/common/shape/WPAbstractShape;

    move-object v3, v10

    check-cast v3, Lcom/intsig/office/common/shape/WPAbstractShape;

    invoke-virtual {v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->getWrap()I

    move-result v4

    int-to-short v4, v4

    invoke-virtual {v2, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 58
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->getHorPositionType()B

    move-result v4

    invoke-virtual {v2, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorPositionType(B)V

    .line 59
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->getHorizontalRelativeTo()B

    move-result v4

    invoke-virtual {v2, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    .line 60
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->getHorRelativeValue()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorRelativeValue(I)V

    .line 61
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->getHorizontalAlignment()B

    move-result v4

    invoke-virtual {v2, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalAlignment(B)V

    .line 62
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->getVerPositionType()B

    move-result v4

    invoke-virtual {v2, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerPositionType(B)V

    .line 63
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->getVerticalRelativeTo()B

    move-result v4

    invoke-virtual {v2, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    .line 64
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->getVerRelativeValue()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerRelativeValue(I)V

    .line 65
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->getVerticalAlignment()B

    move-result v3

    invoke-virtual {v2, v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalAlignment(B)V

    :cond_d
    add-int/lit8 v14, v14, 0x1

    goto :goto_8

    :cond_e
    if-nez v6, :cond_f

    move-object/from16 v9, p2

    .line 66
    invoke-direct {v7, v10, v9}, Lcom/intsig/office/fc/doc/DOCXReader;->addShape(Lcom/intsig/office/common/shape/AbstractShape;Lcom/intsig/office/simpletext/model/ParagraphElement;)V

    goto/16 :goto_9

    .line 67
    :cond_f
    invoke-virtual {v6, v10}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    goto/16 :goto_9

    .line 68
    :cond_10
    invoke-virtual {v10}, Lcom/intsig/office/common/shape/AbstractShape;->dispose()V

    goto/16 :goto_9

    :cond_11
    move-object/from16 v9, p2

    .line 69
    invoke-interface {v8, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_13

    .line 70
    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_12

    .line 71
    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_12

    .line 72
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    :cond_12
    return-void

    :cond_13
    const-string v1, "shape"

    .line 73
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    const-string v0, "imagedata"

    .line 74
    invoke-interface {v8, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 75
    invoke-direct/range {p0 .. p2}, Lcom/intsig/office/fc/doc/DOCXReader;->processPicture(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;)V

    goto/16 :goto_9

    .line 76
    :cond_14
    invoke-direct/range {p0 .. p1}, Lcom/intsig/office/fc/doc/DOCXReader;->hasTextbox2007(Lcom/intsig/office/fc/dom4j/Element;)Z

    move-result v10

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move v6, v10

    invoke-direct/range {v0 .. v6}, Lcom/intsig/office/fc/doc/DOCXReader;->processAutoShape(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/common/shape/WPGroupShape;FFZ)Lcom/intsig/office/common/shape/WPAutoShape;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 77
    iget-object v1, v7, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    invoke-direct {v7, v1, v0, v8}, Lcom/intsig/office/fc/doc/DOCXReader;->processTextbox2007(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/fc/dom4j/Element;)Z

    goto :goto_9

    :cond_15
    const-string v1, "line"

    .line 78
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_16

    const-string v1, "polyline"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_16

    const-string v1, "curve"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_16

    const-string v1, "rect"

    .line 79
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_16

    const-string v1, "roundrect"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_16

    const-string v1, "oval"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 80
    :cond_16
    invoke-direct/range {p0 .. p1}, Lcom/intsig/office/fc/doc/DOCXReader;->hasTextbox2007(Lcom/intsig/office/fc/dom4j/Element;)Z

    move-result v10

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move v6, v10

    invoke-direct/range {v0 .. v6}, Lcom/intsig/office/fc/doc/DOCXReader;->processAutoShape(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/common/shape/WPGroupShape;FFZ)Lcom/intsig/office/common/shape/WPAutoShape;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 81
    iget-object v1, v7, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    invoke-direct {v7, v1, v0, v8}, Lcom/intsig/office/fc/doc/DOCXReader;->processTextbox2007(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/fc/dom4j/Element;)Z

    :cond_17
    :goto_9
    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private processAutoShapeStyle(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/AbstractShape;Lcom/intsig/office/common/shape/GroupShape;FF)Lcom/intsig/office/java/awt/Rectangle;
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    if-eqz v1, :cond_2b

    if-eqz v2, :cond_2b

    const-string v6, "style"

    .line 1
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v7

    if-eqz v7, :cond_2b

    .line 2
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v7

    if-eqz v7, :cond_2b

    .line 3
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2b

    const-string v7, ";"

    .line 4
    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 5
    :goto_0
    array-length v13, v6

    const/4 v15, 0x1

    if-ge v12, v13, :cond_28

    .line 6
    aget-object v13, v6, v12

    const-string v14, ":"

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_26

    .line 7
    aget-object v14, v13, v8

    if-eqz v14, :cond_26

    aget-object v17, v13, v15

    if-eqz v17, :cond_26

    const-string v15, "position"

    .line 8
    invoke-virtual {v15, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    goto/16 :goto_4

    :cond_0
    const-string v14, "left"

    .line 9
    aget-object v15, v13, v8

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    const/4 v14, 0x1

    .line 10
    aget-object v13, v13, v14

    invoke-direct {v0, v13, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->getValueInPt(Ljava/lang/String;F)F

    move-result v13

    :goto_1
    add-float/2addr v7, v13

    :goto_2
    move-object/from16 v18, v6

    goto/16 :goto_6

    :cond_1
    const/4 v14, 0x1

    const-string v15, "top"

    .line 11
    aget-object v14, v13, v8

    invoke-virtual {v15, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2

    const/4 v14, 0x1

    .line 12
    aget-object v13, v13, v14

    invoke-direct {v0, v13, v5}, Lcom/intsig/office/fc/doc/DOCXReader;->getValueInPt(Ljava/lang/String;F)F

    move-result v13

    :goto_3
    add-float/2addr v9, v13

    goto :goto_2

    :cond_2
    const-string v14, "text-align"

    .line 13
    aget-object v15, v13, v8

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_3

    goto/16 :goto_4

    :cond_3
    const-string v14, "margin-left"

    .line 14
    aget-object v15, v13, v8

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    const/high16 v15, 0x3f800000    # 1.0f

    if-eqz v14, :cond_4

    const/4 v14, 0x1

    .line 15
    aget-object v13, v13, v14

    invoke-direct {v0, v13, v15}, Lcom/intsig/office/fc/doc/DOCXReader;->getValueInPt(Ljava/lang/String;F)F

    move-result v13

    goto :goto_1

    :cond_4
    const/4 v14, 0x1

    const-string v15, "margin-top"

    .line 16
    aget-object v14, v13, v8

    invoke-virtual {v15, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_5

    const/4 v14, 0x1

    .line 17
    aget-object v13, v13, v14

    const/high16 v14, 0x3f800000    # 1.0f

    invoke-direct {v0, v13, v14}, Lcom/intsig/office/fc/doc/DOCXReader;->getValueInPt(Ljava/lang/String;F)F

    move-result v13

    goto :goto_3

    :cond_5
    const/4 v14, 0x1

    const-string v15, "width"

    .line 18
    aget-object v14, v13, v8

    invoke-virtual {v15, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_6

    const/4 v14, 0x1

    .line 19
    aget-object v10, v13, v14

    invoke-direct {v0, v10, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->getValueInPt(Ljava/lang/String;F)F

    move-result v10

    goto :goto_2

    :cond_6
    const/4 v14, 0x1

    const-string v15, "height"

    .line 20
    aget-object v14, v13, v8

    invoke-virtual {v15, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_7

    const/4 v14, 0x1

    .line 21
    aget-object v11, v13, v14

    invoke-direct {v0, v11, v5}, Lcom/intsig/office/fc/doc/DOCXReader;->getValueInPt(Ljava/lang/String;F)F

    move-result v11

    goto :goto_2

    :cond_7
    const-string v14, "mso-width-percent"

    .line 22
    aget-object v15, v13, v8

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    const/4 v15, 0x4

    if-eqz v14, :cond_9

    .line 23
    iget-object v14, v0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeType:Ljava/util/List;

    invoke-interface {v14, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_8

    new-array v14, v15, [I

    const/4 v15, 0x1

    .line 24
    aget-object v13, v13, v15

    invoke-static {v13}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v13

    float-to-int v13, v13

    aput v13, v14, v8

    .line 25
    iget-object v13, v0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeType:Ljava/util/List;

    invoke-interface {v13, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    iget-object v13, v0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeValue:Ljava/util/Map;

    invoke-interface {v13, v2, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    .line 27
    :cond_8
    iget-object v14, v0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeValue:Ljava/util/Map;

    invoke-interface {v14, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, [I

    const/4 v15, 0x1

    aget-object v13, v13, v15

    invoke-static {v13}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v13

    float-to-int v13, v13

    aput v13, v14, v8

    goto/16 :goto_4

    :cond_9
    const-string v14, "mso-height-percent"

    .line 28
    aget-object v15, v13, v8

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    const/4 v15, 0x2

    if-eqz v14, :cond_b

    .line 29
    iget-object v14, v0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeType:Ljava/util/List;

    invoke-interface {v14, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_a

    const/4 v14, 0x4

    new-array v14, v14, [I

    const/16 v16, 0x1

    .line 30
    aget-object v13, v13, v16

    invoke-static {v13}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v13

    float-to-int v13, v13

    aput v13, v14, v15

    .line 31
    iget-object v13, v0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeType:Ljava/util/List;

    invoke-interface {v13, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    iget-object v13, v0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeValue:Ljava/util/Map;

    invoke-interface {v13, v2, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    .line 33
    :cond_a
    iget-object v14, v0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeValue:Ljava/util/Map;

    invoke-interface {v14, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, [I

    const/4 v8, 0x1

    aget-object v8, v13, v8

    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    float-to-int v8, v8

    aput v8, v14, v15

    goto/16 :goto_4

    :cond_b
    const/4 v8, 0x1

    const-string v14, "flip"

    const/16 v17, 0x0

    .line 34
    aget-object v15, v13, v17

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_d

    const-string v14, "x"

    .line 35
    aget-object v15, v13, v8

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_c

    .line 36
    invoke-virtual {v2, v8}, Lcom/intsig/office/common/shape/AbstractShape;->setFlipHorizontal(Z)V

    goto/16 :goto_4

    :cond_c
    const-string v14, "y"

    .line 37
    aget-object v13, v13, v8

    invoke-virtual {v14, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_26

    .line 38
    invoke-virtual {v2, v8}, Lcom/intsig/office/common/shape/AbstractShape;->setFlipVertical(Z)V

    goto/16 :goto_4

    :cond_d
    const-string v14, "rotation"

    const/4 v15, 0x0

    .line 39
    aget-object v8, v13, v15

    invoke-virtual {v14, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_f

    const/4 v8, 0x1

    .line 40
    aget-object v14, v13, v8

    const-string v15, "fd"

    invoke-virtual {v14, v15}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v14

    if-lez v14, :cond_e

    .line 41
    aget-object v14, v13, v8

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v15

    const/16 v16, 0x2

    add-int/lit8 v15, v15, -0x2

    const/4 v4, 0x0

    invoke-virtual {v14, v4, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v13, v8

    .line 42
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const v8, 0xea60

    div-int/2addr v4, v8

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Lcom/intsig/office/common/shape/AbstractShape;->setRotation(F)V

    goto/16 :goto_4

    .line 43
    :cond_e
    aget-object v4, v13, v8

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Lcom/intsig/office/common/shape/AbstractShape;->setRotation(F)V

    goto/16 :goto_4

    :cond_f
    const-string v4, "mso-width-relative"

    const/4 v8, 0x0

    .line 44
    aget-object v14, v13, v8

    invoke-virtual {v4, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    goto/16 :goto_4

    :cond_10
    const-string v4, "mso-height-relative"

    .line 45
    aget-object v14, v13, v8

    invoke-virtual {v4, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_11

    goto/16 :goto_4

    :cond_11
    if-nez v3, :cond_12

    .line 46
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/common/shape/AbstractShape;->getType()S

    move-result v4

    const/4 v14, 0x7

    if-eq v4, v14, :cond_12

    const-string v4, "mso-position-horizontal"

    aget-object v14, v13, v8

    .line 47
    invoke-virtual {v4, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 48
    move-object v4, v2

    check-cast v4, Lcom/intsig/office/common/shape/WPAutoShape;

    const/4 v8, 0x1

    aget-object v8, v13, v8

    invoke-direct {v0, v8}, Lcom/intsig/office/fc/doc/DOCXReader;->getAlign(Ljava/lang/String;)B

    move-result v8

    invoke-virtual {v4, v8}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalAlignment(B)V

    goto/16 :goto_4

    :cond_12
    if-nez v3, :cond_13

    .line 49
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/common/shape/AbstractShape;->getType()S

    move-result v4

    const/4 v8, 0x7

    if-eq v4, v8, :cond_13

    const-string v4, "mso-left-percent"

    const/4 v8, 0x0

    aget-object v14, v13, v8

    .line 50
    invoke-virtual {v4, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 51
    move-object v4, v2

    check-cast v4, Lcom/intsig/office/common/shape/WPAutoShape;

    const/4 v8, 0x1

    aget-object v13, v13, v8

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v4, v13}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorRelativeValue(I)V

    .line 52
    invoke-virtual {v4, v8}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorPositionType(B)V

    goto/16 :goto_4

    :cond_13
    const-string v14, "outer-margin-area"

    const-string v15, "inner-margin-area"

    const-string v4, "text"

    const-string v8, "page"

    const-string v5, "margin"

    move-object/from16 v18, v6

    if-nez v3, :cond_1b

    .line 53
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/common/shape/AbstractShape;->getType()S

    move-result v6

    const/4 v1, 0x7

    if-eq v6, v1, :cond_1b

    const-string v1, "mso-position-horizontal-relative"

    move/from16 v19, v11

    const/4 v6, 0x0

    aget-object v11, v13, v6

    .line 54
    invoke-virtual {v1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    const/4 v1, 0x1

    .line 55
    aget-object v6, v13, v1

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 56
    move-object v4, v2

    check-cast v4, Lcom/intsig/office/common/shape/WPAutoShape;

    invoke-virtual {v4, v1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    goto/16 :goto_5

    .line 57
    :cond_14
    aget-object v5, v13, v1

    invoke-virtual {v8, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 58
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    goto/16 :goto_5

    :cond_15
    const-string v5, "left-margin-area"

    .line 59
    aget-object v6, v13, v1

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 60
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    goto/16 :goto_5

    :cond_16
    const-string v5, "right-margin-area"

    .line 61
    aget-object v6, v13, v1

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 62
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/4 v4, 0x5

    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    goto/16 :goto_5

    .line 63
    :cond_17
    aget-object v5, v13, v1

    invoke-virtual {v15, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 64
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    goto/16 :goto_5

    .line 65
    :cond_18
    aget-object v5, v13, v1

    invoke-virtual {v14, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_19

    .line 66
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/16 v4, 0x9

    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    goto/16 :goto_5

    .line 67
    :cond_19
    aget-object v5, v13, v1

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 68
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    goto/16 :goto_5

    :cond_1a
    const-string v4, "char"

    .line 69
    aget-object v1, v13, v1

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 70
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/4 v4, 0x3

    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    goto/16 :goto_5

    :cond_1b
    move/from16 v19, v11

    :cond_1c
    if-nez v3, :cond_1d

    .line 71
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/common/shape/AbstractShape;->getType()S

    move-result v1

    const/4 v6, 0x7

    if-eq v1, v6, :cond_1d

    const-string v1, "mso-position-vertical"

    const/4 v6, 0x0

    aget-object v11, v13, v6

    .line 72
    invoke-virtual {v1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 73
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/4 v4, 0x1

    aget-object v4, v13, v4

    invoke-direct {v0, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->getAlign(Ljava/lang/String;)B

    move-result v4

    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalAlignment(B)V

    goto/16 :goto_5

    :cond_1d
    if-nez v3, :cond_1e

    .line 74
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/common/shape/AbstractShape;->getType()S

    move-result v1

    const/4 v6, 0x7

    if-eq v1, v6, :cond_1e

    const-string v1, "mso-top-percent"

    const/4 v6, 0x0

    aget-object v11, v13, v6

    .line 75
    invoke-virtual {v1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 76
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/4 v4, 0x1

    aget-object v5, v13, v4

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerRelativeValue(I)V

    .line 77
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerPositionType(B)V

    goto/16 :goto_5

    :cond_1e
    if-nez v3, :cond_27

    .line 78
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/common/shape/AbstractShape;->getType()S

    move-result v1

    const/4 v6, 0x7

    if-eq v1, v6, :cond_27

    const-string v1, "mso-position-vertical-relative"

    const/4 v6, 0x0

    aget-object v11, v13, v6

    .line 79
    invoke-virtual {v1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_27

    const-string v1, "line"

    const/4 v6, 0x1

    .line 80
    aget-object v11, v13, v6

    invoke-virtual {v1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 81
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/16 v4, 0xb

    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    goto/16 :goto_5

    .line 82
    :cond_1f
    aget-object v1, v13, v6

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 83
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/16 v4, 0xa

    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    goto/16 :goto_5

    .line 84
    :cond_20
    aget-object v1, v13, v6

    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 85
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    invoke-virtual {v1, v6}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    goto :goto_5

    .line 86
    :cond_21
    aget-object v1, v13, v6

    invoke-virtual {v8, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 87
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    goto :goto_5

    :cond_22
    const-string v1, "top-margin-area"

    .line 88
    aget-object v4, v13, v6

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 89
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/4 v4, 0x6

    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    goto :goto_5

    :cond_23
    const-string v1, "bottom-margin-area"

    .line 90
    aget-object v4, v13, v6

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 91
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/4 v4, 0x7

    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    goto :goto_5

    .line 92
    :cond_24
    aget-object v1, v13, v6

    invoke-virtual {v15, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 93
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    goto :goto_5

    .line 94
    :cond_25
    aget-object v1, v13, v6

    invoke-virtual {v14, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 95
    move-object v1, v2

    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    const/16 v4, 0x9

    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    goto :goto_5

    :cond_26
    :goto_4
    move-object/from16 v18, v6

    move/from16 v19, v11

    :cond_27
    :goto_5
    move/from16 v11, v19

    :goto_6
    add-int/lit8 v12, v12, 0x1

    move-object/from16 v1, p1

    move/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, v18

    const/4 v8, 0x0

    goto/16 :goto_0

    :cond_28
    move/from16 v19, v11

    .line 96
    new-instance v1, Lcom/intsig/office/java/awt/Rectangle;

    invoke-direct {v1}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    const v4, 0x3faaaaab

    mul-float v7, v7, v4

    float-to-int v5, v7

    .line 97
    iput v5, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    mul-float v9, v9, v4

    float-to-int v5, v9

    .line 98
    iput v5, v1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    mul-float v10, v10, v4

    float-to-int v5, v10

    .line 99
    iput v5, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    mul-float v11, v19, v4

    float-to-int v4, v11

    .line 100
    iput v4, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 101
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/common/shape/AbstractShape;->getType()S

    move-result v4

    const/4 v5, 0x7

    if-eq v4, v5, :cond_29

    move-object v4, v2

    check-cast v4, Lcom/intsig/office/common/shape/WPAutoShape;

    invoke-virtual {v4}, Lcom/intsig/office/common/shape/WPAutoShape;->getGroupShape()Lcom/intsig/office/common/shape/WPGroupShape;

    move-result-object v4

    if-nez v4, :cond_29

    .line 102
    invoke-direct {v0, v3, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;

    .line 103
    :cond_29
    instance-of v3, v2, Lcom/intsig/office/common/shape/WPAutoShape;

    if-eqz v3, :cond_2a

    move-object v3, v2

    check-cast v3, Lcom/intsig/office/common/shape/WPAutoShape;

    invoke-virtual {v3}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    move-result v4

    const/16 v5, 0xe9

    if-ne v4, v5, :cond_2a

    const-string v4, "coordsize"

    move-object/from16 v5, p1

    .line 104
    invoke-interface {v5, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2a

    .line 105
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2a

    const-string v5, ","

    .line 106
    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 107
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    const/4 v6, 0x0

    .line 108
    aget-object v6, v4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    const/4 v7, 0x1

    .line 109
    aget-object v4, v4, v7

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    int-to-float v4, v4

    .line 110
    iget v7, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    int-to-float v7, v7

    div-float/2addr v7, v6

    iget v6, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    int-to-float v6, v6

    div-float/2addr v6, v4

    invoke-virtual {v5, v7, v6}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 111
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->getPaths()Ljava/util/List;

    move-result-object v3

    .line 112
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 113
    invoke-virtual {v4}, Lcom/intsig/office/common/autoshape/ExtendPath;->getPath()Landroid/graphics/Path;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    goto :goto_7

    .line 114
    :cond_2a
    invoke-virtual {v2, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    goto :goto_8

    :cond_2b
    const/4 v1, 0x0

    :goto_8
    return-object v1
.end method

.method private processBackgroundAndFill(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/bg/BackgroundAndFill;
    .locals 13

    .line 1
    const-string v0, "filled"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const/4 v2, 0x0

    .line 8
    const/4 v3, 0x1

    .line 9
    if-eqz v1, :cond_1

    .line 10
    .line 11
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-lez v1, :cond_1

    .line 22
    .line 23
    const-string v1, "f"

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    if-nez v1, :cond_0

    .line 30
    .line 31
    const-string v1, "false"

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_1

    .line 38
    .line 39
    :cond_0
    const/4 v0, 0x0

    .line 40
    goto :goto_0

    .line 41
    :cond_1
    const/4 v0, 0x1

    .line 42
    :goto_0
    const/4 v1, 0x0

    .line 43
    if-eqz v0, :cond_e

    .line 44
    .line 45
    const-string v0, "fill"

    .line 46
    .line 47
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    const-string v4, "fillcolor"

    .line 52
    .line 53
    const-string v5, "type"

    .line 54
    .line 55
    const/high16 v6, 0x3f800000    # 1.0f

    .line 56
    .line 57
    const/4 v7, -0x1

    .line 58
    if-eqz v0, :cond_7

    .line 59
    .line 60
    const-string v8, "id"

    .line 61
    .line 62
    invoke-interface {v0, v8}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 63
    .line 64
    .line 65
    move-result-object v9

    .line 66
    if-eqz v9, :cond_7

    .line 67
    .line 68
    invoke-interface {v0, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v8

    .line 72
    if-eqz v8, :cond_7

    .line 73
    .line 74
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    .line 75
    .line 76
    .line 77
    move-result v9

    .line 78
    if-lez v9, :cond_7

    .line 79
    .line 80
    iget-boolean v9, p0, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessHF:Z

    .line 81
    .line 82
    if-eqz v9, :cond_2

    .line 83
    .line 84
    iget-object v9, p0, Lcom/intsig/office/fc/doc/DOCXReader;->hfPart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 85
    .line 86
    if-eqz v9, :cond_2

    .line 87
    .line 88
    iget-object v10, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 89
    .line 90
    invoke-virtual {v9, v8}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationship(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 91
    .line 92
    .line 93
    move-result-object v8

    .line 94
    invoke-virtual {v8}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 95
    .line 96
    .line 97
    move-result-object v8

    .line 98
    invoke-virtual {v10, v8}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 99
    .line 100
    .line 101
    move-result-object v8

    .line 102
    goto :goto_1

    .line 103
    :cond_2
    iget-object v9, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 104
    .line 105
    iget-object v10, p0, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 106
    .line 107
    invoke-virtual {v10, v8}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationship(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 108
    .line 109
    .line 110
    move-result-object v8

    .line 111
    invoke-virtual {v8}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 112
    .line 113
    .line 114
    move-result-object v8

    .line 115
    invoke-virtual {v9, v8}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 116
    .line 117
    .line 118
    move-result-object v8

    .line 119
    :goto_1
    if-eqz v8, :cond_7

    .line 120
    .line 121
    new-instance v1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 122
    .line 123
    invoke-direct {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 124
    .line 125
    .line 126
    invoke-interface {v0, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 127
    .line 128
    .line 129
    move-result-object v9

    .line 130
    invoke-direct {p0, v9}, Lcom/intsig/office/fc/doc/DOCXReader;->getFillType(Ljava/lang/String;)B

    .line 131
    .line 132
    .line 133
    move-result v9

    .line 134
    const/4 v10, 0x2

    .line 135
    if-ne v9, v10, :cond_3

    .line 136
    .line 137
    :try_start_0
    invoke-virtual {v1, v10}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 138
    .line 139
    .line 140
    iget-object v9, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 141
    .line 142
    invoke-interface {v9}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 143
    .line 144
    .line 145
    move-result-object v9

    .line 146
    invoke-virtual {v9}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 147
    .line 148
    .line 149
    move-result-object v9

    .line 150
    invoke-virtual {v9, v8}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)I

    .line 151
    .line 152
    .line 153
    move-result v8

    .line 154
    new-instance v9, Lcom/intsig/office/common/bg/TileShader;

    .line 155
    .line 156
    iget-object v10, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 157
    .line 158
    invoke-interface {v10}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 159
    .line 160
    .line 161
    move-result-object v10

    .line 162
    invoke-virtual {v10}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 163
    .line 164
    .line 165
    move-result-object v10

    .line 166
    invoke-virtual {v10, v8}, Lcom/intsig/office/common/picture/PictureManage;->getPicture(I)Lcom/intsig/office/common/picture/Picture;

    .line 167
    .line 168
    .line 169
    move-result-object v8

    .line 170
    invoke-direct {v9, v8, v2, v6, v6}, Lcom/intsig/office/common/bg/TileShader;-><init>(Lcom/intsig/office/common/picture/Picture;IFF)V

    .line 171
    .line 172
    .line 173
    invoke-virtual {v1, v9}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setShader(Lcom/intsig/office/common/bg/AShader;)V

    .line 174
    .line 175
    .line 176
    goto/16 :goto_5

    .line 177
    .line 178
    :catch_0
    move-exception v8

    .line 179
    goto :goto_4

    .line 180
    :cond_3
    if-ne v9, v3, :cond_6

    .line 181
    .line 182
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 183
    .line 184
    .line 185
    move-result-object v9

    .line 186
    if-eqz v9, :cond_4

    .line 187
    .line 188
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    .line 189
    .line 190
    .line 191
    move-result v10

    .line 192
    if-lez v10, :cond_4

    .line 193
    .line 194
    invoke-direct {p0, v9, v2}, Lcom/intsig/office/fc/doc/DOCXReader;->getColor(Ljava/lang/String;Z)I

    .line 195
    .line 196
    .line 197
    move-result v9

    .line 198
    goto :goto_2

    .line 199
    :cond_4
    const/4 v9, -0x1

    .line 200
    :goto_2
    const-string v10, "color2"

    .line 201
    .line 202
    invoke-interface {v0, v10}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 203
    .line 204
    .line 205
    move-result-object v10

    .line 206
    if-eqz v10, :cond_5

    .line 207
    .line 208
    invoke-direct {p0, v10, v3}, Lcom/intsig/office/fc/doc/DOCXReader;->getColor(Ljava/lang/String;Z)I

    .line 209
    .line 210
    .line 211
    move-result v10

    .line 212
    goto :goto_3

    .line 213
    :cond_5
    const/4 v10, -0x1

    .line 214
    :goto_3
    invoke-virtual {v1, v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 215
    .line 216
    .line 217
    iget-object v11, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 218
    .line 219
    invoke-interface {v11}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 220
    .line 221
    .line 222
    move-result-object v11

    .line 223
    invoke-virtual {v11}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 224
    .line 225
    .line 226
    move-result-object v11

    .line 227
    invoke-virtual {v11, v8}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)I

    .line 228
    .line 229
    .line 230
    move-result v8

    .line 231
    new-instance v11, Lcom/intsig/office/common/bg/PatternShader;

    .line 232
    .line 233
    iget-object v12, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 234
    .line 235
    invoke-interface {v12}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 236
    .line 237
    .line 238
    move-result-object v12

    .line 239
    invoke-virtual {v12}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 240
    .line 241
    .line 242
    move-result-object v12

    .line 243
    invoke-virtual {v12, v8}, Lcom/intsig/office/common/picture/PictureManage;->getPicture(I)Lcom/intsig/office/common/picture/Picture;

    .line 244
    .line 245
    .line 246
    move-result-object v8

    .line 247
    invoke-direct {v11, v8, v10, v9}, Lcom/intsig/office/common/bg/PatternShader;-><init>(Lcom/intsig/office/common/picture/Picture;II)V

    .line 248
    .line 249
    .line 250
    invoke-virtual {v1, v11}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setShader(Lcom/intsig/office/common/bg/AShader;)V

    .line 251
    .line 252
    .line 253
    goto :goto_5

    .line 254
    :cond_6
    const/4 v9, 0x3

    .line 255
    invoke-virtual {v1, v9}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 256
    .line 257
    .line 258
    iget-object v9, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 259
    .line 260
    invoke-interface {v9}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 261
    .line 262
    .line 263
    move-result-object v9

    .line 264
    invoke-virtual {v9}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 265
    .line 266
    .line 267
    move-result-object v9

    .line 268
    invoke-virtual {v9, v8}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)I

    .line 269
    .line 270
    .line 271
    move-result v8

    .line 272
    invoke-virtual {v1, v8}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setPictureIndex(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    .line 274
    .line 275
    goto :goto_5

    .line 276
    :goto_4
    iget-object v9, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 277
    .line 278
    invoke-interface {v9}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 279
    .line 280
    .line 281
    move-result-object v9

    .line 282
    invoke-virtual {v9}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    .line 283
    .line 284
    .line 285
    move-result-object v9

    .line 286
    invoke-virtual {v9, v8}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;)V

    .line 287
    .line 288
    .line 289
    :cond_7
    :goto_5
    if-nez v1, :cond_e

    .line 290
    .line 291
    new-instance v1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 292
    .line 293
    invoke-direct {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 294
    .line 295
    .line 296
    if-eqz v0, :cond_8

    .line 297
    .line 298
    invoke-interface {v0, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 299
    .line 300
    .line 301
    move-result-object v5

    .line 302
    invoke-direct {p0, v5}, Lcom/intsig/office/fc/doc/DOCXReader;->getFillType(Ljava/lang/String;)B

    .line 303
    .line 304
    .line 305
    move-result v5

    .line 306
    goto :goto_6

    .line 307
    :cond_8
    const/4 v5, 0x0

    .line 308
    :goto_6
    if-eqz v0, :cond_a

    .line 309
    .line 310
    if-nez v5, :cond_9

    .line 311
    .line 312
    goto :goto_7

    .line 313
    :cond_9
    invoke-direct {p0, p1, v0, v5}, Lcom/intsig/office/fc/doc/DOCXReader;->readGradient(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/Element;B)Lcom/intsig/office/common/bg/Gradient;

    .line 314
    .line 315
    .line 316
    move-result-object p1

    .line 317
    invoke-virtual {v1, v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 318
    .line 319
    .line 320
    invoke-virtual {v1, p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setShader(Lcom/intsig/office/common/bg/AShader;)V

    .line 321
    .line 322
    .line 323
    goto :goto_8

    .line 324
    :cond_a
    :goto_7
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 325
    .line 326
    .line 327
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 328
    .line 329
    .line 330
    move-result-object p1

    .line 331
    if-eqz p1, :cond_b

    .line 332
    .line 333
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 334
    .line 335
    .line 336
    move-result v2

    .line 337
    if-lez v2, :cond_b

    .line 338
    .line 339
    invoke-direct {p0, p1, v3}, Lcom/intsig/office/fc/doc/DOCXReader;->getColor(Ljava/lang/String;Z)I

    .line 340
    .line 341
    .line 342
    move-result v7

    .line 343
    :cond_b
    if-eqz v0, :cond_d

    .line 344
    .line 345
    const-string p1, "opacity"

    .line 346
    .line 347
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 348
    .line 349
    .line 350
    move-result-object p1

    .line 351
    if-eqz p1, :cond_d

    .line 352
    .line 353
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 354
    .line 355
    .line 356
    move-result p1

    .line 357
    cmpl-float v0, p1, v6

    .line 358
    .line 359
    if-lez v0, :cond_c

    .line 360
    .line 361
    const/high16 v0, 0x47800000    # 65536.0f

    .line 362
    .line 363
    div-float/2addr p1, v0

    .line 364
    :cond_c
    const/high16 v0, 0x437f0000    # 255.0f

    .line 365
    .line 366
    mul-float p1, p1, v0

    .line 367
    .line 368
    float-to-int p1, p1

    .line 369
    int-to-byte p1, p1

    .line 370
    shl-int/lit8 p1, p1, 0x18

    .line 371
    .line 372
    const v0, 0xffffff

    .line 373
    .line 374
    .line 375
    and-int/2addr v0, v7

    .line 376
    or-int v7, p1, v0

    .line 377
    .line 378
    :cond_d
    invoke-virtual {v1, v7}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 379
    .line 380
    .line 381
    :cond_e
    :goto_8
    return-object v1
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method private processBorder(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/borders/Border;)V
    .locals 3

    .line 1
    const-string v0, "color"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    const-string v1, "auto"

    .line 10
    .line 11
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v2, "#"

    .line 24
    .line 25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    invoke-virtual {p2, v0}, Lcom/intsig/office/common/borders/Border;->setColor(I)V

    .line 40
    .line 41
    .line 42
    goto :goto_1

    .line 43
    :cond_1
    :goto_0
    const/high16 v0, -0x1000000

    .line 44
    .line 45
    invoke-virtual {p2, v0}, Lcom/intsig/office/common/borders/Border;->setColor(I)V

    .line 46
    .line 47
    .line 48
    :goto_1
    const-string v0, "space"

    .line 49
    .line 50
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    if-nez p1, :cond_2

    .line 55
    .line 56
    const/16 p1, 0x20

    .line 57
    .line 58
    invoke-virtual {p2, p1}, Lcom/intsig/office/common/borders/Border;->setSpace(S)V

    .line 59
    .line 60
    .line 61
    goto :goto_2

    .line 62
    :cond_2
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    int-to-float p1, p1

    .line 67
    const v0, 0x3faaaaab

    .line 68
    .line 69
    .line 70
    mul-float p1, p1, v0

    .line 71
    .line 72
    float-to-int p1, p1

    .line 73
    int-to-short p1, p1

    .line 74
    invoke-virtual {p2, p1}, Lcom/intsig/office/common/borders/Border;->setSpace(S)V

    .line 75
    .line 76
    .line 77
    :goto_2
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processBulletNumber()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 2
    .line 3
    const-string v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationship(I)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_6

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    if-eqz v0, :cond_6

    .line 27
    .line 28
    new-instance v2, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 29
    .line 30
    invoke-direct {v2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    const-string v3, "num"

    .line 46
    .line 47
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 48
    .line 49
    .line 50
    move-result-object v3

    .line 51
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 52
    .line 53
    .line 54
    move-result-object v3

    .line 55
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 56
    .line 57
    .line 58
    move-result v4

    .line 59
    const-string v5, "val"

    .line 60
    .line 61
    const-string v6, "abstractNumId"

    .line 62
    .line 63
    if-eqz v4, :cond_1

    .line 64
    .line 65
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 66
    .line 67
    .line 68
    move-result-object v4

    .line 69
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 70
    .line 71
    invoke-interface {v4, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 72
    .line 73
    .line 74
    move-result-object v6

    .line 75
    if-eqz v6, :cond_0

    .line 76
    .line 77
    invoke-interface {v6, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v5

    .line 81
    const-string v6, "numId"

    .line 82
    .line 83
    invoke-interface {v4, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v4

    .line 87
    iget-object v6, p0, Lcom/intsig/office/fc/doc/DOCXReader;->bulletNumbersID:Ljava/util/Hashtable;

    .line 88
    .line 89
    invoke-virtual {v6, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    .line 91
    .line 92
    goto :goto_0

    .line 93
    :cond_1
    const-string v3, "abstractNum"

    .line 94
    .line 95
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 96
    .line 97
    .line 98
    move-result-object v2

    .line 99
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 100
    .line 101
    .line 102
    move-result-object v2

    .line 103
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 104
    .line 105
    .line 106
    move-result v3

    .line 107
    if-eqz v3, :cond_5

    .line 108
    .line 109
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 110
    .line 111
    .line 112
    move-result-object v3

    .line 113
    check-cast v3, Lcom/intsig/office/fc/dom4j/Element;

    .line 114
    .line 115
    new-instance v4, Lcom/intsig/office/common/bulletnumber/ListData;

    .line 116
    .line 117
    invoke-direct {v4}, Lcom/intsig/office/common/bulletnumber/ListData;-><init>()V

    .line 118
    .line 119
    .line 120
    invoke-interface {v3, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v7

    .line 124
    if-eqz v7, :cond_2

    .line 125
    .line 126
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 127
    .line 128
    .line 129
    move-result v7

    .line 130
    invoke-virtual {v4, v7}, Lcom/intsig/office/common/bulletnumber/ListData;->setListID(I)V

    .line 131
    .line 132
    .line 133
    :cond_2
    const-string v7, "lvl"

    .line 134
    .line 135
    invoke-interface {v3, v7}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 136
    .line 137
    .line 138
    move-result-object v7

    .line 139
    invoke-interface {v7}, Ljava/util/List;->size()I

    .line 140
    .line 141
    .line 142
    move-result v8

    .line 143
    new-array v9, v8, [Lcom/intsig/office/common/bulletnumber/ListLevel;

    .line 144
    .line 145
    int-to-byte v10, v8

    .line 146
    invoke-virtual {v4, v10}, Lcom/intsig/office/common/bulletnumber/ListData;->setSimpleList(B)V

    .line 147
    .line 148
    .line 149
    const/4 v11, 0x0

    .line 150
    :goto_2
    if-ge v11, v8, :cond_3

    .line 151
    .line 152
    new-instance v12, Lcom/intsig/office/common/bulletnumber/ListLevel;

    .line 153
    .line 154
    invoke-direct {v12}, Lcom/intsig/office/common/bulletnumber/ListLevel;-><init>()V

    .line 155
    .line 156
    .line 157
    aput-object v12, v9, v11

    .line 158
    .line 159
    invoke-interface {v7, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 160
    .line 161
    .line 162
    move-result-object v13

    .line 163
    check-cast v13, Lcom/intsig/office/fc/dom4j/Element;

    .line 164
    .line 165
    invoke-direct {p0, v12, v13}, Lcom/intsig/office/fc/doc/DOCXReader;->processListLevel(Lcom/intsig/office/common/bulletnumber/ListLevel;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 166
    .line 167
    .line 168
    add-int/lit8 v11, v11, 0x1

    .line 169
    .line 170
    goto :goto_2

    .line 171
    :cond_3
    invoke-virtual {v4, v9}, Lcom/intsig/office/common/bulletnumber/ListData;->setLevels([Lcom/intsig/office/common/bulletnumber/ListLevel;)V

    .line 172
    .line 173
    .line 174
    invoke-virtual {v4, v10}, Lcom/intsig/office/common/bulletnumber/ListData;->setSimpleList(B)V

    .line 175
    .line 176
    .line 177
    if-nez v8, :cond_4

    .line 178
    .line 179
    const-string v7, "numStyleLink"

    .line 180
    .line 181
    invoke-interface {v3, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 182
    .line 183
    .line 184
    move-result-object v3

    .line 185
    if-eqz v3, :cond_4

    .line 186
    .line 187
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 188
    .line 189
    .line 190
    move-result-object v3

    .line 191
    if-eqz v3, :cond_4

    .line 192
    .line 193
    iget-object v7, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleStrID:Ljava/util/Map;

    .line 194
    .line 195
    invoke-interface {v7, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    .line 197
    .line 198
    move-result-object v3

    .line 199
    check-cast v3, Ljava/lang/Integer;

    .line 200
    .line 201
    if-eqz v3, :cond_4

    .line 202
    .line 203
    invoke-virtual {v3}, Ljava/lang/Integer;->shortValue()S

    .line 204
    .line 205
    .line 206
    move-result v7

    .line 207
    invoke-virtual {v4, v7}, Lcom/intsig/office/common/bulletnumber/ListData;->setLinkStyleID(S)V

    .line 208
    .line 209
    .line 210
    invoke-static {}, Lcom/intsig/office/simpletext/model/StyleManage;->instance()Lcom/intsig/office/simpletext/model/StyleManage;

    .line 211
    .line 212
    .line 213
    move-result-object v7

    .line 214
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 215
    .line 216
    .line 217
    move-result v3

    .line 218
    invoke-virtual {v7, v3}, Lcom/intsig/office/simpletext/model/StyleManage;->getStyle(I)Lcom/intsig/office/simpletext/model/Style;

    .line 219
    .line 220
    .line 221
    move-result-object v3

    .line 222
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 223
    .line 224
    .line 225
    move-result-object v7

    .line 226
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 227
    .line 228
    .line 229
    move-result-object v8

    .line 230
    invoke-virtual {v7, v8}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaListID(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 231
    .line 232
    .line 233
    move-result v7

    .line 234
    if-ltz v7, :cond_4

    .line 235
    .line 236
    iget-object v8, p0, Lcom/intsig/office/fc/doc/DOCXReader;->bulletNumbersID:Ljava/util/Hashtable;

    .line 237
    .line 238
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 239
    .line 240
    .line 241
    move-result-object v7

    .line 242
    invoke-virtual {v8, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    .line 244
    .line 245
    move-result-object v7

    .line 246
    check-cast v7, Ljava/lang/String;

    .line 247
    .line 248
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 249
    .line 250
    .line 251
    move-result v7

    .line 252
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 253
    .line 254
    .line 255
    move-result-object v8

    .line 256
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 257
    .line 258
    .line 259
    move-result-object v3

    .line 260
    invoke-virtual {v8, v3, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaListID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 261
    .line 262
    .line 263
    :cond_4
    iget-object v3, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 264
    .line 265
    invoke-interface {v3}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 266
    .line 267
    .line 268
    move-result-object v3

    .line 269
    invoke-virtual {v3}, Lcom/intsig/office/system/SysKit;->getListManage()Lcom/intsig/office/common/bulletnumber/ListManage;

    .line 270
    .line 271
    .line 272
    move-result-object v3

    .line 273
    invoke-virtual {v4}, Lcom/intsig/office/common/bulletnumber/ListData;->getListID()I

    .line 274
    .line 275
    .line 276
    move-result v7

    .line 277
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 278
    .line 279
    .line 280
    move-result-object v7

    .line 281
    invoke-virtual {v3, v7, v4}, Lcom/intsig/office/common/bulletnumber/ListManage;->putListData(Ljava/lang/Integer;Lcom/intsig/office/common/bulletnumber/ListData;)I

    .line 282
    .line 283
    .line 284
    goto/16 :goto_1

    .line 285
    .line 286
    :cond_5
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 287
    .line 288
    .line 289
    :cond_6
    return-void
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private processCell(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/wp/model/RowElement;IZLjava/lang/String;)I
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/wp/model/CellElement;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/wp/model/CellElement;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-wide v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 7
    .line 8
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 9
    .line 10
    .line 11
    const-string v1, "tcPr"

    .line 12
    .line 13
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-direct {p0, v1, v2, p3}, Lcom/intsig/office/fc/doc/DOCXReader;->processCellAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;I)I

    .line 24
    .line 25
    .line 26
    move-result p3

    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const/4 p3, 0x0

    .line 29
    :goto_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    const/4 v1, 0x1

    .line 34
    invoke-direct {p0, p1, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->processParagraphs_Table(Ljava/util/List;I)V

    .line 35
    .line 36
    .line 37
    iget-wide v2, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 38
    .line 39
    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {p2, v0}, Lcom/intsig/office/wp/model/RowElement;->appendCell(Lcom/intsig/office/wp/model/CellElement;)V

    .line 43
    .line 44
    .line 45
    if-eqz p4, :cond_1

    .line 46
    .line 47
    iget-object p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->tableStyle:Ljava/util/Map;

    .line 48
    .line 49
    invoke-interface {p1, p5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    if-eqz p1, :cond_1

    .line 54
    .line 55
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 60
    .line 61
    .line 62
    move-result-object p4

    .line 63
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->tableStyle:Ljava/util/Map;

    .line 64
    .line 65
    invoke-interface {v0, p5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    .line 67
    .line 68
    move-result-object p5

    .line 69
    check-cast p5, Ljava/lang/Integer;

    .line 70
    .line 71
    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    .line 72
    .line 73
    .line 74
    move-result p5

    .line 75
    invoke-virtual {p1, p4, p5}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableCellBackground(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 76
    .line 77
    .line 78
    :cond_1
    if-lez p3, :cond_2

    .line 79
    .line 80
    :goto_1
    if-ge v1, p3, :cond_2

    .line 81
    .line 82
    new-instance p1, Lcom/intsig/office/wp/model/CellElement;

    .line 83
    .line 84
    invoke-direct {p1}, Lcom/intsig/office/wp/model/CellElement;-><init>()V

    .line 85
    .line 86
    .line 87
    invoke-virtual {p2, p1}, Lcom/intsig/office/wp/model/RowElement;->appendCell(Lcom/intsig/office/wp/model/CellElement;)V

    .line 88
    .line 89
    .line 90
    add-int/lit8 v1, v1, 0x1

    .line 91
    .line 92
    goto :goto_1

    .line 93
    :cond_2
    return p3
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method private processCellAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;I)I
    .locals 8

    .line 1
    const-string v0, "gridSpan"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "val"

    .line 8
    .line 9
    const/4 v2, 0x1

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v0, 0x1

    .line 22
    :goto_0
    const-string v3, "tcW"

    .line 23
    .line 24
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    const/4 v4, 0x0

    .line 29
    if-eqz v3, :cond_4

    .line 30
    .line 31
    const-string v5, "w"

    .line 32
    .line 33
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v5

    .line 37
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 38
    .line 39
    .line 40
    move-result v5

    .line 41
    const-string v6, "type"

    .line 42
    .line 43
    invoke-interface {v3, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v3

    .line 47
    const-string v6, "pct"

    .line 48
    .line 49
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 50
    .line 51
    .line 52
    move-result v6

    .line 53
    if-nez v6, :cond_1

    .line 54
    .line 55
    const-string v6, "auto"

    .line 56
    .line 57
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    if-eqz v3, :cond_3

    .line 62
    .line 63
    :cond_1
    move v3, p3

    .line 64
    :goto_1
    add-int v6, p3, v0

    .line 65
    .line 66
    if-ge v3, v6, :cond_2

    .line 67
    .line 68
    iget-object v6, p0, Lcom/intsig/office/fc/doc/DOCXReader;->tableGridCol:Ljava/util/Map;

    .line 69
    .line 70
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 71
    .line 72
    .line 73
    move-result-object v7

    .line 74
    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    .line 76
    .line 77
    move-result-object v6

    .line 78
    check-cast v6, Ljava/lang/Integer;

    .line 79
    .line 80
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    .line 81
    .line 82
    .line 83
    move-result v6

    .line 84
    add-int/2addr v4, v6

    .line 85
    add-int/lit8 v3, v3, 0x1

    .line 86
    .line 87
    goto :goto_1

    .line 88
    :cond_2
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    .line 89
    .line 90
    .line 91
    move-result v5

    .line 92
    :cond_3
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 93
    .line 94
    .line 95
    move-result-object p3

    .line 96
    invoke-virtual {p3, p2, v5}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableCellWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 97
    .line 98
    .line 99
    goto :goto_3

    .line 100
    :cond_4
    move v3, p3

    .line 101
    :goto_2
    add-int v5, p3, v0

    .line 102
    .line 103
    if-ge v3, v5, :cond_5

    .line 104
    .line 105
    iget-object v5, p0, Lcom/intsig/office/fc/doc/DOCXReader;->tableGridCol:Ljava/util/Map;

    .line 106
    .line 107
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 108
    .line 109
    .line 110
    move-result-object v6

    .line 111
    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    .line 113
    .line 114
    move-result-object v5

    .line 115
    check-cast v5, Ljava/lang/Integer;

    .line 116
    .line 117
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    .line 118
    .line 119
    .line 120
    move-result v5

    .line 121
    add-int/2addr v4, v5

    .line 122
    add-int/lit8 v3, v3, 0x1

    .line 123
    .line 124
    goto :goto_2

    .line 125
    :cond_5
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 126
    .line 127
    .line 128
    move-result-object p3

    .line 129
    invoke-virtual {p3, p2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableCellWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 130
    .line 131
    .line 132
    :goto_3
    const-string p3, "vMerge"

    .line 133
    .line 134
    invoke-interface {p1, p3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 135
    .line 136
    .line 137
    move-result-object p3

    .line 138
    if-eqz p3, :cond_6

    .line 139
    .line 140
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 141
    .line 142
    .line 143
    move-result-object v3

    .line 144
    invoke-virtual {v3, p2, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableVerMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 145
    .line 146
    .line 147
    invoke-interface {p3, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 148
    .line 149
    .line 150
    move-result-object p3

    .line 151
    if-eqz p3, :cond_6

    .line 152
    .line 153
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 154
    .line 155
    .line 156
    move-result-object p3

    .line 157
    invoke-virtual {p3, p2, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableVerFirstMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 158
    .line 159
    .line 160
    :cond_6
    const-string p3, "vAlign"

    .line 161
    .line 162
    invoke-interface {p1, p3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 163
    .line 164
    .line 165
    move-result-object p1

    .line 166
    if-eqz p1, :cond_8

    .line 167
    .line 168
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object p1

    .line 172
    const-string p3, "center"

    .line 173
    .line 174
    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 175
    .line 176
    .line 177
    move-result p3

    .line 178
    if-eqz p3, :cond_7

    .line 179
    .line 180
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 181
    .line 182
    .line 183
    move-result-object p1

    .line 184
    invoke-virtual {p1, p2, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableCellVerAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 185
    .line 186
    .line 187
    goto :goto_4

    .line 188
    :cond_7
    const-string p3, "bottom"

    .line 189
    .line 190
    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 191
    .line 192
    .line 193
    move-result p1

    .line 194
    if-eqz p1, :cond_8

    .line 195
    .line 196
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 197
    .line 198
    .line 199
    move-result-object p1

    .line 200
    const/4 p3, 0x2

    .line 201
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableCellVerAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 202
    .line 203
    .line 204
    :cond_8
    :goto_4
    return v0
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private processEmbeddedTable(Lcom/intsig/office/fc/dom4j/Element;I)V
    .locals 2

    .line 1
    const-string v0, "tr"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 22
    .line 23
    const-string v1, "tc"

    .line 24
    .line 25
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    if-eqz v1, :cond_0

    .line 38
    .line 39
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 44
    .line 45
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-direct {p0, v1, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->processParagraphs_Table(Ljava/util/List;I)V

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_1
    return-void
.end method

.method private processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget v0, p2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/GroupShape;->getOffX()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    add-int/2addr v0, v1

    .line 10
    iput v0, p2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 11
    .line 12
    iget v0, p2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/GroupShape;->getOffY()I

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    add-int/2addr v0, p1

    .line 19
    iput v0, p2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 20
    .line 21
    :cond_0
    return-object p2
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private processHeaderAndFooter(Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    iput-object p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->hfPart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 14
    .line 15
    if-eqz p1, :cond_2

    .line 16
    .line 17
    const/4 p1, 0x1

    .line 18
    iput-boolean p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessHF:Z

    .line 19
    .line 20
    if-eqz p2, :cond_0

    .line 21
    .line 22
    const-wide/high16 v0, 0x1000000000000000L

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const-wide/high16 v0, 0x2000000000000000L

    .line 26
    .line 27
    :goto_0
    iput-wide v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 28
    .line 29
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 30
    .line 31
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 32
    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->hfPart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 35
    .line 36
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    new-instance v2, Lcom/intsig/office/wp/model/HFElement;

    .line 53
    .line 54
    if-eqz p2, :cond_1

    .line 55
    .line 56
    const/4 p2, 0x5

    .line 57
    goto :goto_1

    .line 58
    :cond_1
    const/4 p2, 0x6

    .line 59
    :goto_1
    invoke-direct {v2, p2, p1}, Lcom/intsig/office/wp/model/HFElement;-><init>(SB)V

    .line 60
    .line 61
    .line 62
    iget-wide p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 63
    .line 64
    invoke-virtual {v2, p1, p2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 65
    .line 66
    .line 67
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->processParagraphs(Ljava/util/List;)V

    .line 68
    .line 69
    .line 70
    iget-wide p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 71
    .line 72
    invoke-virtual {v2, p1, p2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 73
    .line 74
    .line 75
    iget-object p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 76
    .line 77
    iget-wide v3, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 78
    .line 79
    invoke-virtual {p1, v2, v3, v4}, Lcom/intsig/office/wp/model/WPDocument;->appendElement(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 80
    .line 81
    .line 82
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 83
    .line 84
    .line 85
    const/4 p1, 0x0

    .line 86
    iput-boolean p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessHF:Z

    .line 87
    .line 88
    :cond_2
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processHyperlinkRun(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;)Lcom/intsig/office/simpletext/model/LeafElement;
    .locals 13

    .line 1
    const/4 v0, 0x1

    .line 2
    const/4 v1, 0x0

    .line 3
    :try_start_0
    const-string v2, "id"

    .line 4
    .line 5
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    if-eqz v2, :cond_0

    .line 10
    .line 11
    iget-object v3, p0, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 12
    .line 13
    const-string v4, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink"

    .line 14
    .line 15
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationshipByID(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 20
    .line 21
    .line 22
    move-result-object v2
    :try_end_0
    .catch Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    goto :goto_0

    .line 24
    :catch_0
    move-exception v2

    .line 25
    iget-object v3, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 26
    .line 27
    invoke-interface {v3}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    invoke-virtual {v3}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    invoke-virtual {v3, v2, v0}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;Z)V

    .line 36
    .line 37
    .line 38
    :cond_0
    move-object v2, v1

    .line 39
    :goto_0
    const/4 v3, -0x1

    .line 40
    if-eqz v2, :cond_1

    .line 41
    .line 42
    iget-object v4, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 43
    .line 44
    invoke-interface {v4}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 45
    .line 46
    .line 47
    move-result-object v4

    .line 48
    invoke-virtual {v4}, Lcom/intsig/office/system/SysKit;->getHyperlinkManage()Lcom/intsig/office/common/hyperlink/HyperlinkManage;

    .line 49
    .line 50
    .line 51
    move-result-object v4

    .line 52
    invoke-virtual {v2}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    invoke-virtual {v2}, Ljava/net/URI;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    invoke-virtual {v4, v2, v0}, Lcom/intsig/office/common/hyperlink/HyperlinkManage;->addHyperlink(Ljava/lang/String;I)I

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    goto :goto_1

    .line 65
    :cond_1
    const/4 v2, -0x1

    .line 66
    :goto_1
    if-ne v2, v3, :cond_2

    .line 67
    .line 68
    const-string v4, "anchor"

    .line 69
    .line 70
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v4

    .line 74
    if-eqz v4, :cond_2

    .line 75
    .line 76
    iget-object v2, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 77
    .line 78
    invoke-interface {v2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 79
    .line 80
    .line 81
    move-result-object v2

    .line 82
    invoke-virtual {v2}, Lcom/intsig/office/system/SysKit;->getHyperlinkManage()Lcom/intsig/office/common/hyperlink/HyperlinkManage;

    .line 83
    .line 84
    .line 85
    move-result-object v2

    .line 86
    const/4 v5, 0x5

    .line 87
    invoke-virtual {v2, v4, v5}, Lcom/intsig/office/common/hyperlink/HyperlinkManage;->addHyperlink(Ljava/lang/String;I)I

    .line 88
    .line 89
    .line 90
    move-result v2

    .line 91
    :cond_2
    const-string v4, "r"

    .line 92
    .line 93
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 94
    .line 95
    .line 96
    move-result-object p1

    .line 97
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    :goto_2
    move-object v5, v1

    .line 102
    :cond_3
    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 103
    .line 104
    .line 105
    move-result v6

    .line 106
    if-eqz v6, :cond_8

    .line 107
    .line 108
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 109
    .line 110
    .line 111
    move-result-object v6

    .line 112
    check-cast v6, Lcom/intsig/office/fc/dom4j/Element;

    .line 113
    .line 114
    const-string v7, "instrText"

    .line 115
    .line 116
    invoke-interface {v6, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 117
    .line 118
    .line 119
    move-result-object v7

    .line 120
    if-eqz v7, :cond_4

    .line 121
    .line 122
    invoke-interface {v7}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v7

    .line 126
    if-eqz v7, :cond_4

    .line 127
    .line 128
    const-string v8, "PAGEREF"

    .line 129
    .line 130
    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 131
    .line 132
    .line 133
    move-result v7

    .line 134
    if-eqz v7, :cond_4

    .line 135
    .line 136
    const/4 v2, -0x1

    .line 137
    :cond_4
    const-string v7, "ruby"

    .line 138
    .line 139
    invoke-interface {v6, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 140
    .line 141
    .line 142
    move-result-object v7

    .line 143
    if-eqz v7, :cond_5

    .line 144
    .line 145
    const-string v8, "rubyBase"

    .line 146
    .line 147
    invoke-interface {v7, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 148
    .line 149
    .line 150
    move-result-object v7

    .line 151
    if-eqz v7, :cond_5

    .line 152
    .line 153
    invoke-interface {v7, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 154
    .line 155
    .line 156
    move-result-object v6

    .line 157
    if-nez v6, :cond_5

    .line 158
    .line 159
    goto :goto_3

    .line 160
    :cond_5
    const-string v7, "t"

    .line 161
    .line 162
    invoke-interface {v6, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 163
    .line 164
    .line 165
    move-result-object v7

    .line 166
    if-nez v7, :cond_6

    .line 167
    .line 168
    const-string v7, "drawing"

    .line 169
    .line 170
    invoke-interface {v6, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 171
    .line 172
    .line 173
    move-result-object v6

    .line 174
    if-eqz v6, :cond_3

    .line 175
    .line 176
    invoke-direct {p0, v6, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->processPictureAndDiagram(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;)V

    .line 177
    .line 178
    .line 179
    goto :goto_2

    .line 180
    :cond_6
    invoke-interface {v7}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 181
    .line 182
    .line 183
    move-result-object v7

    .line 184
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    .line 185
    .line 186
    .line 187
    move-result v8

    .line 188
    if-lez v8, :cond_3

    .line 189
    .line 190
    new-instance v5, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 191
    .line 192
    invoke-direct {v5, v7}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 193
    .line 194
    .line 195
    invoke-virtual {v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 196
    .line 197
    .line 198
    move-result-object v7

    .line 199
    const-string v9, "rPr"

    .line 200
    .line 201
    invoke-interface {v6, v9}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 202
    .line 203
    .line 204
    move-result-object v6

    .line 205
    if-eqz v6, :cond_7

    .line 206
    .line 207
    invoke-direct {p0, v6, v7}, Lcom/intsig/office/fc/doc/DOCXReader;->processRunAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 208
    .line 209
    .line 210
    :cond_7
    iget-wide v9, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 211
    .line 212
    invoke-virtual {v5, v9, v10}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 213
    .line 214
    .line 215
    iget-wide v9, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 216
    .line 217
    int-to-long v11, v8

    .line 218
    add-long/2addr v9, v11

    .line 219
    iput-wide v9, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 220
    .line 221
    invoke-virtual {v5, v9, v10}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 222
    .line 223
    .line 224
    invoke-virtual {p2, v5}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 225
    .line 226
    .line 227
    if-ltz v2, :cond_3

    .line 228
    .line 229
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 230
    .line 231
    .line 232
    move-result-object v6

    .line 233
    const v8, -0xffff01

    .line 234
    .line 235
    .line 236
    invoke-virtual {v6, v7, v8}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 237
    .line 238
    .line 239
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 240
    .line 241
    .line 242
    move-result-object v6

    .line 243
    invoke-virtual {v6, v7, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontUnderline(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 244
    .line 245
    .line 246
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 247
    .line 248
    .line 249
    move-result-object v6

    .line 250
    invoke-virtual {v6, v7, v8}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontUnderlineColr(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 251
    .line 252
    .line 253
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 254
    .line 255
    .line 256
    move-result-object v6

    .line 257
    invoke-virtual {v6, v7, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setHyperlinkID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 258
    .line 259
    .line 260
    goto/16 :goto_3

    .line 261
    .line 262
    :cond_8
    return-object v5
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private processLine(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/borders/Line;
    .locals 9

    .line 1
    const-string v0, "stroked"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-string v2, "f"

    .line 8
    .line 9
    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 10
    .line 11
    .line 12
    move-result v3

    .line 13
    const/4 v4, 0x0

    .line 14
    if-nez v3, :cond_b

    .line 15
    .line 16
    const-string v3, "false"

    .line 17
    .line 18
    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-eqz v1, :cond_0

    .line 23
    .line 24
    goto/16 :goto_4

    .line 25
    .line 26
    :cond_0
    const-string v1, "type"

    .line 27
    .line 28
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getParent()Lcom/intsig/office/fc/dom4j/Element;

    .line 33
    .line 34
    .line 35
    move-result-object v5

    .line 36
    const-string v6, "shapetype"

    .line 37
    .line 38
    invoke-interface {v5, v6}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 39
    .line 40
    .line 41
    move-result-object v5

    .line 42
    if-eqz v1, :cond_2

    .line 43
    .line 44
    if-eqz v5, :cond_2

    .line 45
    .line 46
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 47
    .line 48
    .line 49
    move-result-object v5

    .line 50
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    .line 51
    .line 52
    .line 53
    move-result v6

    .line 54
    if-eqz v6, :cond_2

    .line 55
    .line 56
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 57
    .line 58
    .line 59
    move-result-object v6

    .line 60
    check-cast v6, Lcom/intsig/office/fc/dom4j/Element;

    .line 61
    .line 62
    new-instance v7, Ljava/lang/StringBuilder;

    .line 63
    .line 64
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .line 66
    .line 67
    const-string v8, "#"

    .line 68
    .line 69
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    const-string v8, "id"

    .line 73
    .line 74
    invoke-interface {v6, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v8

    .line 78
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v7

    .line 85
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 86
    .line 87
    .line 88
    move-result v7

    .line 89
    if-eqz v7, :cond_1

    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_2
    move-object v6, v4

    .line 93
    :goto_0
    if-eqz v6, :cond_4

    .line 94
    .line 95
    invoke-interface {v6, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 100
    .line 101
    .line 102
    move-result v1

    .line 103
    if-nez v1, :cond_3

    .line 104
    .line 105
    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 106
    .line 107
    .line 108
    move-result v0

    .line 109
    if-eqz v0, :cond_4

    .line 110
    .line 111
    :cond_3
    return-object v4

    .line 112
    :cond_4
    const-string v0, "strokecolor"

    .line 113
    .line 114
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    const/4 v1, 0x0

    .line 119
    if-eqz v0, :cond_5

    .line 120
    .line 121
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 122
    .line 123
    .line 124
    move-result v2

    .line 125
    if-lez v2, :cond_5

    .line 126
    .line 127
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->getColor(Ljava/lang/String;Z)I

    .line 128
    .line 129
    .line 130
    move-result v0

    .line 131
    goto :goto_1

    .line 132
    :cond_5
    const/high16 v0, -0x1000000

    .line 133
    .line 134
    :goto_1
    new-instance v2, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 135
    .line 136
    invoke-direct {v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 137
    .line 138
    .line 139
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 140
    .line 141
    .line 142
    const-string v0, "strokeweight"

    .line 143
    .line 144
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object v3

    .line 148
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v0

    .line 152
    const/4 v4, 0x1

    .line 153
    if-eqz v0, :cond_9

    .line 154
    .line 155
    const-string v0, "pt"

    .line 156
    .line 157
    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 158
    .line 159
    .line 160
    move-result v5

    .line 161
    const-string v6, ""

    .line 162
    .line 163
    if-ltz v5, :cond_6

    .line 164
    .line 165
    invoke-virtual {v3, v0, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 166
    .line 167
    .line 168
    move-result-object v3

    .line 169
    goto :goto_2

    .line 170
    :cond_6
    const-string v0, "mm"

    .line 171
    .line 172
    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 173
    .line 174
    .line 175
    move-result v5

    .line 176
    if-ltz v5, :cond_7

    .line 177
    .line 178
    invoke-virtual {v3, v0, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 179
    .line 180
    .line 181
    move-result-object v3

    .line 182
    goto :goto_2

    .line 183
    :cond_7
    const-string v0, "cm"

    .line 184
    .line 185
    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 186
    .line 187
    .line 188
    move-result v5

    .line 189
    if-ltz v5, :cond_8

    .line 190
    .line 191
    invoke-virtual {v3, v0, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 192
    .line 193
    .line 194
    move-result-object v3

    .line 195
    :cond_8
    :goto_2
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 196
    .line 197
    .line 198
    move-result v0

    .line 199
    const v3, 0x3faaaaab

    .line 200
    .line 201
    .line 202
    mul-float v0, v0, v3

    .line 203
    .line 204
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 205
    .line 206
    .line 207
    move-result v0

    .line 208
    goto :goto_3

    .line 209
    :cond_9
    const/4 v0, 0x1

    .line 210
    :goto_3
    const-string v3, "stroke"

    .line 211
    .line 212
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 213
    .line 214
    .line 215
    move-result-object v5

    .line 216
    if-eqz v5, :cond_a

    .line 217
    .line 218
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 219
    .line 220
    .line 221
    move-result-object p1

    .line 222
    const-string v3, "dashstyle"

    .line 223
    .line 224
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 225
    .line 226
    .line 227
    move-result-object p1

    .line 228
    if-eqz p1, :cond_a

    .line 229
    .line 230
    const/4 v1, 0x1

    .line 231
    :cond_a
    new-instance p1, Lcom/intsig/office/common/borders/Line;

    .line 232
    .line 233
    invoke-direct {p1}, Lcom/intsig/office/common/borders/Line;-><init>()V

    .line 234
    .line 235
    .line 236
    invoke-virtual {p1, v2}, Lcom/intsig/office/common/borders/Line;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 237
    .line 238
    .line 239
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/borders/Border;->setLineWidth(I)V

    .line 240
    .line 241
    .line 242
    invoke-virtual {p1, v1}, Lcom/intsig/office/common/borders/Line;->setDash(Z)V

    .line 243
    .line 244
    .line 245
    return-object p1

    .line 246
    :cond_b
    :goto_4
    return-object v4
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private processListLevel(Lcom/intsig/office/common/bulletnumber/ListLevel;Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 9

    .line 1
    const-string v0, "start"

    .line 2
    .line 3
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "val"

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setStartAt(I)V

    .line 20
    .line 21
    .line 22
    :cond_0
    const-string v0, "lvlJc"

    .line 23
    .line 24
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const-string v2, "left"

    .line 29
    .line 30
    const/4 v3, 0x2

    .line 31
    const/4 v4, 0x0

    .line 32
    const/4 v5, 0x1

    .line 33
    if-eqz v0, :cond_3

    .line 34
    .line 35
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 40
    .line 41
    .line 42
    move-result v6

    .line 43
    if-eqz v6, :cond_1

    .line 44
    .line 45
    invoke-virtual {p1, v4}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setAlign(B)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_1
    const-string v6, "center"

    .line 50
    .line 51
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    move-result v6

    .line 55
    if-eqz v6, :cond_2

    .line 56
    .line 57
    invoke-virtual {p1, v5}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setAlign(B)V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_2
    const-string v6, "right"

    .line 62
    .line 63
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    if-eqz v0, :cond_3

    .line 68
    .line 69
    invoke-virtual {p1, v3}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setAlign(B)V

    .line 70
    .line 71
    .line 72
    :cond_3
    :goto_0
    const-string v0, "suff"

    .line 73
    .line 74
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    if-eqz v0, :cond_5

    .line 79
    .line 80
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    const-string v6, "space"

    .line 85
    .line 86
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 87
    .line 88
    .line 89
    move-result v6

    .line 90
    if-eqz v6, :cond_4

    .line 91
    .line 92
    invoke-virtual {p1, v5}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setFollowChar(B)V

    .line 93
    .line 94
    .line 95
    goto :goto_1

    .line 96
    :cond_4
    const-string v6, "nothing"

    .line 97
    .line 98
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 99
    .line 100
    .line 101
    move-result v0

    .line 102
    if-eqz v0, :cond_5

    .line 103
    .line 104
    invoke-virtual {p1, v3}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setFollowChar(B)V

    .line 105
    .line 106
    .line 107
    :cond_5
    :goto_1
    const-string v0, "numFmt"

    .line 108
    .line 109
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 110
    .line 111
    .line 112
    move-result-object v0

    .line 113
    if-eqz v0, :cond_6

    .line 114
    .line 115
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->convertedNumberFormat(Ljava/lang/String;)I

    .line 120
    .line 121
    .line 122
    move-result v0

    .line 123
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setNumberFormat(I)V

    .line 124
    .line 125
    .line 126
    :cond_6
    const-string v0, "lvlText"

    .line 127
    .line 128
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    if-eqz v0, :cond_10

    .line 133
    .line 134
    new-instance v3, Ljava/lang/StringBuilder;

    .line 135
    .line 136
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    .line 138
    .line 139
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 140
    .line 141
    .line 142
    move-result-object v0

    .line 143
    const/4 v1, 0x0

    .line 144
    :goto_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 145
    .line 146
    .line 147
    move-result v6

    .line 148
    if-ge v1, v6, :cond_f

    .line 149
    .line 150
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    .line 151
    .line 152
    .line 153
    move-result v6

    .line 154
    const/16 v7, 0x25

    .line 155
    .line 156
    if-ne v6, v7, :cond_7

    .line 157
    .line 158
    add-int/lit8 v6, v1, 0x1

    .line 159
    .line 160
    add-int/lit8 v1, v1, 0x2

    .line 161
    .line 162
    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object v1

    .line 166
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 167
    .line 168
    .line 169
    move-result v1

    .line 170
    sub-int/2addr v1, v5

    .line 171
    int-to-char v1, v1

    .line 172
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 173
    .line 174
    .line 175
    move v1, v6

    .line 176
    goto :goto_5

    .line 177
    :cond_7
    const v7, 0xf06c

    .line 178
    .line 179
    .line 180
    const/16 v8, 0x25cf

    .line 181
    .line 182
    if-ne v6, v7, :cond_8

    .line 183
    .line 184
    :goto_3
    const/16 v6, 0x25cf

    .line 185
    .line 186
    goto :goto_4

    .line 187
    :cond_8
    const v7, 0xf06e

    .line 188
    .line 189
    .line 190
    if-ne v6, v7, :cond_9

    .line 191
    .line 192
    const/16 v6, 0x25a0

    .line 193
    .line 194
    goto :goto_4

    .line 195
    :cond_9
    const v7, 0xf075

    .line 196
    .line 197
    .line 198
    if-ne v6, v7, :cond_a

    .line 199
    .line 200
    const/16 v6, 0x25c6

    .line 201
    .line 202
    goto :goto_4

    .line 203
    :cond_a
    const v7, 0xf0fc

    .line 204
    .line 205
    .line 206
    if-ne v6, v7, :cond_b

    .line 207
    .line 208
    const/16 v6, 0x221a

    .line 209
    .line 210
    goto :goto_4

    .line 211
    :cond_b
    const v7, 0xf0d8

    .line 212
    .line 213
    .line 214
    if-ne v6, v7, :cond_c

    .line 215
    .line 216
    const/16 v6, 0x2605

    .line 217
    .line 218
    goto :goto_4

    .line 219
    :cond_c
    const v7, 0xf0b2

    .line 220
    .line 221
    .line 222
    if-ne v6, v7, :cond_d

    .line 223
    .line 224
    const/16 v6, 0x2606

    .line 225
    .line 226
    goto :goto_4

    .line 227
    :cond_d
    const v7, 0xf000

    .line 228
    .line 229
    .line 230
    if-lt v6, v7, :cond_e

    .line 231
    .line 232
    goto :goto_3

    .line 233
    :cond_e
    :goto_4
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 234
    .line 235
    .line 236
    :goto_5
    add-int/2addr v1, v5

    .line 237
    goto :goto_2

    .line 238
    :cond_f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    .line 239
    .line 240
    .line 241
    move-result v0

    .line 242
    new-array v0, v0, [C

    .line 243
    .line 244
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    .line 245
    .line 246
    .line 247
    move-result v1

    .line 248
    invoke-virtual {v3, v4, v1, v0, v4}, Ljava/lang/StringBuilder;->getChars(II[CI)V

    .line 249
    .line 250
    .line 251
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setNumberText([C)V

    .line 252
    .line 253
    .line 254
    :cond_10
    const-string v0, "pPr"

    .line 255
    .line 256
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 257
    .line 258
    .line 259
    move-result-object p2

    .line 260
    if-eqz p2, :cond_12

    .line 261
    .line 262
    const-string v0, "ind"

    .line 263
    .line 264
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 265
    .line 266
    .line 267
    move-result-object p2

    .line 268
    if-eqz p2, :cond_12

    .line 269
    .line 270
    const-string v0, "hanging"

    .line 271
    .line 272
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 273
    .line 274
    .line 275
    move-result-object v0

    .line 276
    if-eqz v0, :cond_11

    .line 277
    .line 278
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 279
    .line 280
    .line 281
    move-result v0

    .line 282
    neg-int v0, v0

    .line 283
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setSpecialIndent(I)V

    .line 284
    .line 285
    .line 286
    :cond_11
    invoke-interface {p2, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 287
    .line 288
    .line 289
    move-result-object p2

    .line 290
    if-eqz p2, :cond_12

    .line 291
    .line 292
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 293
    .line 294
    .line 295
    move-result p2

    .line 296
    invoke-virtual {p1, p2}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setTextIndent(I)V

    .line 297
    .line 298
    .line 299
    :cond_12
    return-void
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private processParaAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 17

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    const-string v4, "docDefaults"

    const-string v5, "line"

    if-lez v3, :cond_0

    .line 1
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLevel(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    :cond_0
    if-nez v0, :cond_1

    return-void

    :cond_1
    :try_start_0
    const-string v3, "pStyle"

    .line 2
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v6, 0x0

    const-string v7, "val"

    if-eqz v3, :cond_2

    .line 3
    :try_start_1
    invoke-interface {v3, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 4
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_3

    .line 5
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v8

    iget-object v9, v1, Lcom/intsig/office/fc/doc/DOCXReader;->styleStrID:Ljava/util/Map;

    invoke-interface {v9, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v8, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaStyleID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_0

    .line 6
    :cond_2
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v3

    invoke-virtual {v3, v2, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaStyleID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    :cond_3
    :goto_0
    const-string v3, "spacing"

    .line 7
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    const/high16 v8, 0x42c80000    # 100.0f

    if-eqz v3, :cond_11

    .line 8
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v10, "afterLines"

    const-string v11, "after"

    const-string v12, "beforeLines"

    const-string v13, "before"

    const/high16 v14, 0x43700000    # 240.0f

    const/4 v15, 0x0

    if-eqz v9, :cond_8

    .line 9
    :try_start_2
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v9

    cmpl-float v16, v9, v15

    if-nez v16, :cond_4

    const/high16 v9, 0x43700000    # 240.0f

    .line 10
    :cond_4
    invoke-interface {v3, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_5

    .line 11
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v16

    if-lez v16, :cond_5

    .line 12
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v6

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    int-to-float v15, v15

    div-float/2addr v15, v8

    mul-float v15, v15, v9

    float-to-int v15, v15

    invoke-virtual {v6, v2, v15}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    :cond_5
    if-nez v12, :cond_6

    .line 13
    invoke-interface {v3, v13}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 14
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_6

    .line 15
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v12

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v12, v2, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 16
    :cond_6
    invoke-interface {v3, v10}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 17
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_7

    .line 18
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v10

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    int-to-float v12, v12

    div-float/2addr v12, v8

    mul-float v12, v12, v9

    float-to-int v9, v12

    invoke-virtual {v10, v2, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    :cond_7
    if-nez v6, :cond_c

    .line 19
    invoke-interface {v3, v11}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_c

    .line 20
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_c

    .line 21
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v9

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v9, v2, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_2

    .line 22
    :cond_8
    invoke-interface {v3, v13}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 23
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_9

    .line 24
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v9

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v9, v2, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_1

    .line 25
    :cond_9
    invoke-interface {v3, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_a

    .line 26
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_a

    .line 27
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v9

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v8

    mul-float v6, v6, v14

    float-to-int v6, v6

    invoke-virtual {v9, v2, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 28
    :cond_a
    :goto_1
    invoke-interface {v3, v11}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 29
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_b

    .line 30
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v9

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v9, v2, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_2

    .line 31
    :cond_b
    invoke-interface {v3, v10}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_c

    .line 32
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_c

    .line 33
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v9

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v8

    mul-float v6, v6, v14

    float-to-int v6, v6

    invoke-virtual {v9, v2, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    :cond_c
    :goto_2
    const-string v6, "lineRule"

    .line 34
    invoke-interface {v3, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 35
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_d

    .line 36
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    goto :goto_3

    :cond_d
    const/4 v3, 0x0

    :goto_3
    const-string v5, "auto"

    .line 37
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    const/4 v9, 0x5

    if-eqz v5, :cond_e

    .line 38
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v5

    invoke-virtual {v5, v2, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    const/4 v5, 0x0

    cmpl-float v5, v3, v5

    if-eqz v5, :cond_11

    .line 39
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v5

    div-float/2addr v3, v14

    invoke-virtual {v5, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;F)V

    goto :goto_4

    :cond_e
    const-string v5, "atLeast"

    .line 40
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 41
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v5, v2, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    const/4 v5, 0x0

    cmpl-float v5, v3, v5

    if-eqz v5, :cond_11

    .line 42
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;F)V

    goto :goto_4

    :cond_f
    const-string v5, "exact"

    .line 43
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 44
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v2, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    const/4 v5, 0x0

    cmpl-float v5, v3, v5

    if-eqz v5, :cond_11

    .line 45
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v5

    neg-float v3, v3

    invoke-virtual {v5, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;F)V

    goto :goto_4

    .line 46
    :cond_10
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v5

    invoke-virtual {v5, v2, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    const/4 v5, 0x0

    cmpl-float v5, v3, v5

    if-eqz v5, :cond_11

    .line 47
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v5

    neg-float v3, v3

    div-float/2addr v3, v14

    invoke-virtual {v5, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;F)V

    :cond_11
    :goto_4
    const-string v3, "ind"

    .line 48
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const-string v5, "right"

    const-string v6, "left"

    if-eqz v3, :cond_1c

    .line 49
    :try_start_3
    iget-object v9, v1, Lcom/intsig/office/fc/doc/DOCXReader;->styleStrID:Ljava/util/Map;

    invoke-interface {v9, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 50
    invoke-static {}, Lcom/intsig/office/simpletext/model/StyleManage;->instance()Lcom/intsig/office/simpletext/model/StyleManage;

    move-result-object v9

    iget-object v10, v1, Lcom/intsig/office/fc/doc/DOCXReader;->styleStrID:Ljava/util/Map;

    invoke-interface {v10, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v9, v4}, Lcom/intsig/office/simpletext/model/StyleManage;->getStyle(I)Lcom/intsig/office/simpletext/model/Style;

    move-result-object v4

    goto :goto_5

    :cond_12
    const/4 v4, 0x0

    :goto_5
    if-eqz v4, :cond_13

    .line 51
    invoke-virtual {v4}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v4

    if-eqz v4, :cond_13

    .line 52
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v9

    invoke-virtual {v9, v4, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontSize(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    move-result v4

    goto :goto_6

    :cond_13
    const/16 v4, 0xc

    :goto_6
    const-string v9, "leftChars"

    .line 53
    invoke-interface {v3, v9}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const/high16 v10, 0x41a00000    # 20.0f

    const-string v11, "0"

    if-eqz v9, :cond_14

    .line 54
    :try_start_4
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_14

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_14

    .line 55
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v9, v8

    .line 56
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v12

    int-to-float v13, v4

    mul-float v13, v13, v9

    mul-float v13, v13, v10

    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    move-result v9

    invoke-virtual {v12, v2, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_7

    .line 57
    :cond_14
    invoke-interface {v3, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_15

    .line 58
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_15

    .line 59
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 60
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v12

    invoke-virtual {v12, v2, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_8

    :cond_15
    :goto_7
    const/4 v9, 0x0

    :goto_8
    const-string v12, "rightChars"

    .line 61
    invoke-interface {v3, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_16

    .line 62
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_16

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_16

    .line 63
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    int-to-float v12, v12

    div-float/2addr v12, v8

    .line 64
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v13

    int-to-float v14, v4

    mul-float v14, v14, v12

    mul-float v14, v14, v10

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v12

    invoke-virtual {v13, v2, v12}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_9

    .line 65
    :cond_16
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_17

    .line 66
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_17

    .line 67
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v13

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v13, v2, v12}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    :cond_17
    :goto_9
    const-string v12, "firstLineChars"

    .line 68
    invoke-interface {v3, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_18

    .line 69
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_18

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_18

    .line 70
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    int-to-float v12, v12

    div-float/2addr v12, v8

    .line 71
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v13

    int-to-float v14, v4

    mul-float v14, v14, v12

    mul-float v14, v14, v10

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v12

    invoke-virtual {v13, v2, v12}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_a

    :cond_18
    const-string v12, "firstLine"

    .line 72
    invoke-interface {v3, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_19

    .line 73
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_19

    .line 74
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v13

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v13, v2, v12}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    :cond_19
    :goto_a
    const-string v12, "hangingChars"

    .line 75
    invoke-interface {v3, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_1b

    .line 76
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_1b

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1b

    .line 77
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v8

    int-to-float v4, v4

    mul-float v4, v4, v3

    mul-float v4, v4, v10

    .line 78
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v3

    neg-int v3, v3

    .line 79
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    if-nez v9, :cond_1a

    .line 80
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    move-result v9

    :cond_1a
    if-gez v3, :cond_1c

    .line 81
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v4

    add-int/2addr v9, v3

    invoke-virtual {v4, v2, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_b

    :cond_1b
    const-string v4, "hanging"

    .line 82
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1c

    .line 83
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1c

    .line 84
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    neg-int v3, v3

    .line 85
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    if-gez v3, :cond_1c

    .line 86
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v4

    add-int/2addr v9, v3

    invoke-virtual {v4, v2, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    :cond_1c
    :goto_b
    const-string v3, "jc"

    .line 87
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    if-eqz v3, :cond_20

    .line 88
    invoke-interface {v3, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 89
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1f

    const-string v4, "both"

    .line 90
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1f

    const-string v4, "distribute"

    .line 91
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1d

    goto :goto_c

    :cond_1d
    const-string v4, "center"

    .line 92
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 93
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_d

    .line 94
    :cond_1e
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 95
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_d

    .line 96
    :cond_1f
    :goto_c
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_e

    :cond_20
    :goto_d
    const/4 v4, 0x0

    :goto_e
    const-string v3, "numPr"

    .line 97
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    if-eqz v3, :cond_22

    const-string v4, "ilvl"

    .line 98
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v4

    if-eqz v4, :cond_21

    .line 99
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v5

    invoke-interface {v4, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v5, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaListLevel(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    :cond_21
    const-string v4, "numId"

    .line 100
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    if-eqz v3, :cond_25

    .line 101
    invoke-interface {v3, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_25

    .line 102
    iget-object v4, v1, Lcom/intsig/office/fc/doc/DOCXReader;->bulletNumbersID:Ljava/util/Hashtable;

    invoke-virtual {v4, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz v3, :cond_25

    .line 103
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v4, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaListID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_10

    .line 104
    :cond_22
    invoke-static {}, Lcom/intsig/office/simpletext/model/StyleManage;->instance()Lcom/intsig/office/simpletext/model/StyleManage;

    move-result-object v3

    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaStyleID(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/intsig/office/simpletext/model/StyleManage;->getStyle(I)Lcom/intsig/office/simpletext/model/Style;

    move-result-object v3

    if-eqz v3, :cond_25

    .line 105
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v5

    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaListLevel(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    move-result v5

    .line 106
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v6

    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaListID(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    move-result v3

    const/4 v6, -0x1

    if-le v3, v6, :cond_24

    if-gez v5, :cond_23

    goto :goto_f

    :cond_23
    move v4, v5

    .line 107
    :goto_f
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaListID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    move v5, v4

    :cond_24
    if-le v5, v6, :cond_25

    .line 108
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v3

    invoke-virtual {v3, v2, v5}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaListLevel(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    :cond_25
    :goto_10
    const-string v3, "tabs"

    .line 109
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    if-eqz v3, :cond_27

    const-string v4, "tab"

    .line 110
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_27

    .line 111
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_26
    :goto_11
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_27

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    const-string v5, "clear"

    .line 112
    invoke-interface {v4, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_26

    const-string v5, "pos"

    .line 113
    invoke-interface {v4, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_26

    .line 114
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_26

    .line 115
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v5, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaTabsClearPostion(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_11

    .line 116
    :cond_27
    iget-boolean v2, v1, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessSectionAttribute:Z

    if-nez v2, :cond_28

    const-string v2, "sectPr"

    .line 117
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->processSectionAttribute(Lcom/intsig/office/fc/dom4j/Element;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_12

    :catch_0
    move-exception v0

    .line 118
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processParaAttribute error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "DOCXReader"

    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    :cond_28
    :goto_12
    return-void
.end method

.method private processParagraph(Lcom/intsig/office/fc/dom4j/Element;I)V
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-wide v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 7
    .line 8
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 9
    .line 10
    .line 11
    const-string v3, "pPr"

    .line 12
    .line 13
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 14
    .line 15
    .line 16
    move-result-object v3

    .line 17
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 18
    .line 19
    .line 20
    move-result-object v4

    .line 21
    invoke-direct {p0, v3, v4, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->processParaAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 22
    .line 23
    .line 24
    const/4 p2, 0x1

    .line 25
    invoke-direct {p0, p1, v0, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->processRun(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;Z)Z

    .line 26
    .line 27
    .line 28
    iget-wide p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 29
    .line 30
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 31
    .line 32
    .line 33
    iget-wide p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 34
    .line 35
    cmp-long v3, p1, v1

    .line 36
    .line 37
    if-lez v3, :cond_0

    .line 38
    .line 39
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 40
    .line 41
    invoke-virtual {v1, v0, p1, p2}, Lcom/intsig/office/wp/model/WPDocument;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 42
    .line 43
    .line 44
    :cond_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private processParagraphs(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/dom4j/Element;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_3

    .line 10
    .line 11
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 16
    .line 17
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    const-string v2, "sdt"

    .line 22
    .line 23
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-eqz v1, :cond_1

    .line 28
    .line 29
    const-string v1, "sdtContent"

    .line 30
    .line 31
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->processParagraphs(Ljava/util/List;)V

    .line 42
    .line 43
    .line 44
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    const-string v2, "p"

    .line 49
    .line 50
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    if-eqz v1, :cond_2

    .line 55
    .line 56
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->processParagraph(Lcom/intsig/office/fc/dom4j/Element;I)V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_2
    const-string v1, "tbl"

    .line 62
    .line 63
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    if-eqz v1, :cond_0

    .line 72
    .line 73
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->processTable(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 74
    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_3
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private processParagraphs_Table(Ljava/util/List;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/dom4j/Element;",
            ">;I)V"
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_3

    .line 10
    .line 11
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 16
    .line 17
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    const-string v2, "sdt"

    .line 22
    .line 23
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-eqz v1, :cond_1

    .line 28
    .line 29
    const-string v1, "sdtContent"

    .line 30
    .line 31
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-direct {p0, v1, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->processParagraphs_Table(Ljava/util/List;I)V

    .line 42
    .line 43
    .line 44
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    const-string v2, "p"

    .line 49
    .line 50
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    if-eqz v1, :cond_2

    .line 55
    .line 56
    invoke-direct {p0, v0, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->processParagraph(Lcom/intsig/office/fc/dom4j/Element;I)V

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_2
    const-string v1, "tbl"

    .line 61
    .line 62
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v2

    .line 66
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 67
    .line 68
    .line 69
    move-result v1

    .line 70
    if-eqz v1, :cond_0

    .line 71
    .line 72
    invoke-direct {p0, v0, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->processEmbeddedTable(Lcom/intsig/office/fc/dom4j/Element;I)V

    .line 73
    .line 74
    .line 75
    goto :goto_0

    .line 76
    :cond_3
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processPicture(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;)V
    .locals 8

    .line 1
    if-eqz p1, :cond_7

    .line 2
    .line 3
    const-string v0, "imagedata"

    .line 4
    .line 5
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const-string v0, "rect"

    .line 12
    .line 13
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    const-string p1, "fill"

    .line 20
    .line 21
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    move-object v7, v0

    .line 26
    move-object v0, p1

    .line 27
    move-object p1, v7

    .line 28
    :cond_0
    if-eqz v0, :cond_7

    .line 29
    .line 30
    const-string v1, "id"

    .line 31
    .line 32
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    if-eqz v2, :cond_7

    .line 37
    .line 38
    iget-boolean v3, p0, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessHF:Z

    .line 39
    .line 40
    if-eqz v3, :cond_1

    .line 41
    .line 42
    iget-object v3, p0, Lcom/intsig/office/fc/doc/DOCXReader;->hfPart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 43
    .line 44
    if-eqz v3, :cond_1

    .line 45
    .line 46
    iget-object v4, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 47
    .line 48
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationship(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    invoke-virtual {v2}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    invoke-virtual {v4, v2}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    goto :goto_0

    .line 61
    :cond_1
    iget-object v3, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 62
    .line 63
    iget-object v4, p0, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 64
    .line 65
    invoke-virtual {v4, v2}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationship(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    invoke-virtual {v2}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    :goto_0
    const-string v3, "style"

    .line 78
    .line 79
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v3

    .line 83
    if-eqz v2, :cond_7

    .line 84
    .line 85
    if-eqz v3, :cond_7

    .line 86
    .line 87
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    const/4 v3, 0x1

    .line 92
    if-eqz v1, :cond_2

    .line 93
    .line 94
    const-string v4, "PictureWatermark"

    .line 95
    .line 96
    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 97
    .line 98
    .line 99
    move-result v1

    .line 100
    if-lez v1, :cond_2

    .line 101
    .line 102
    iput-boolean v3, p0, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessWatermark:Z

    .line 103
    .line 104
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 105
    .line 106
    invoke-interface {v1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 107
    .line 108
    .line 109
    move-result-object v1

    .line 110
    invoke-virtual {v1}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 111
    .line 112
    .line 113
    move-result-object v1

    .line 114
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)I

    .line 115
    .line 116
    .line 117
    move-result v1

    .line 118
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/doc/DOCXReader;->getShapeWrapType(Lcom/intsig/office/fc/dom4j/Element;)S

    .line 119
    .line 120
    .line 121
    move-result v2

    .line 122
    iget-boolean v4, p0, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessWatermark:Z

    .line 123
    .line 124
    if-eqz v4, :cond_5

    .line 125
    .line 126
    new-instance v4, Lcom/intsig/office/common/shape/WatermarkShape;

    .line 127
    .line 128
    invoke-direct {v4}, Lcom/intsig/office/common/shape/WatermarkShape;-><init>()V

    .line 129
    .line 130
    .line 131
    const-string v5, "blacklevel"

    .line 132
    .line 133
    invoke-interface {v0, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v5

    .line 137
    const v6, 0x47c35000    # 100000.0f

    .line 138
    .line 139
    .line 140
    if-eqz v5, :cond_3

    .line 141
    .line 142
    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 143
    .line 144
    .line 145
    move-result v5

    .line 146
    div-float/2addr v5, v6

    .line 147
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/shape/WatermarkShape;->setBlacklevel(F)V

    .line 148
    .line 149
    .line 150
    :cond_3
    const-string v5, "gain"

    .line 151
    .line 152
    invoke-interface {v0, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 153
    .line 154
    .line 155
    move-result-object v0

    .line 156
    if-eqz v0, :cond_4

    .line 157
    .line 158
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 159
    .line 160
    .line 161
    move-result v0

    .line 162
    div-float/2addr v0, v6

    .line 163
    invoke-virtual {v4, v0}, Lcom/intsig/office/common/shape/WatermarkShape;->setGain(F)V

    .line 164
    .line 165
    .line 166
    :cond_4
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/shape/WatermarkShape;->setWatermarkType(B)V

    .line 167
    .line 168
    .line 169
    invoke-virtual {v4, v1}, Lcom/intsig/office/common/shape/WatermarkShape;->setPictureIndex(I)V

    .line 170
    .line 171
    .line 172
    invoke-virtual {v4, v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 173
    .line 174
    .line 175
    goto :goto_1

    .line 176
    :cond_5
    invoke-static {v0}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;->getPictureEffectInfor_ImageData(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 177
    .line 178
    .line 179
    move-result-object v0

    .line 180
    new-instance v3, Lcom/intsig/office/common/shape/PictureShape;

    .line 181
    .line 182
    invoke-direct {v3}, Lcom/intsig/office/common/shape/PictureShape;-><init>()V

    .line 183
    .line 184
    .line 185
    invoke-virtual {v3, v1}, Lcom/intsig/office/common/shape/PictureShape;->setPictureIndex(I)V

    .line 186
    .line 187
    .line 188
    const/16 v1, 0x3e8

    .line 189
    .line 190
    invoke-virtual {v3, v1}, Lcom/intsig/office/common/shape/PictureShape;->setZoomX(S)V

    .line 191
    .line 192
    .line 193
    invoke-virtual {v3, v1}, Lcom/intsig/office/common/shape/PictureShape;->setZoomY(S)V

    .line 194
    .line 195
    .line 196
    invoke-virtual {v3, v0}, Lcom/intsig/office/common/shape/PictureShape;->setPictureEffectInfor(Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;)V

    .line 197
    .line 198
    .line 199
    new-instance v4, Lcom/intsig/office/common/shape/WPPictureShape;

    .line 200
    .line 201
    invoke-direct {v4}, Lcom/intsig/office/common/shape/WPPictureShape;-><init>()V

    .line 202
    .line 203
    .line 204
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/shape/WPPictureShape;->setPictureShape(Lcom/intsig/office/common/shape/PictureShape;)V

    .line 205
    .line 206
    .line 207
    invoke-virtual {v4, v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 208
    .line 209
    .line 210
    :goto_1
    move-object v0, v4

    .line 211
    const/4 v4, 0x0

    .line 212
    const/high16 v5, 0x447a0000    # 1000.0f

    .line 213
    .line 214
    const/high16 v6, 0x447a0000    # 1000.0f

    .line 215
    .line 216
    move-object v1, p0

    .line 217
    move-object v2, p1

    .line 218
    move-object v3, v0

    .line 219
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/fc/doc/DOCXReader;->processAutoShapeStyle(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/AbstractShape;Lcom/intsig/office/common/shape/GroupShape;FF)Lcom/intsig/office/java/awt/Rectangle;

    .line 220
    .line 221
    .line 222
    move-result-object v1

    .line 223
    iget-boolean v2, p0, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessWatermark:Z

    .line 224
    .line 225
    if-nez v2, :cond_6

    .line 226
    .line 227
    move-object v2, v0

    .line 228
    check-cast v2, Lcom/intsig/office/common/shape/WPPictureShape;

    .line 229
    .line 230
    invoke-virtual {v2}, Lcom/intsig/office/common/shape/WPPictureShape;->getPictureShape()Lcom/intsig/office/common/shape/PictureShape;

    .line 231
    .line 232
    .line 233
    move-result-object v2

    .line 234
    invoke-virtual {v2, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 235
    .line 236
    .line 237
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/doc/DOCXReader;->processBackgroundAndFill(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 238
    .line 239
    .line 240
    move-result-object v1

    .line 241
    invoke-virtual {v2, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 242
    .line 243
    .line 244
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/doc/DOCXReader;->processLine(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/borders/Line;

    .line 245
    .line 246
    .line 247
    move-result-object p1

    .line 248
    invoke-virtual {v2, p1}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 249
    .line 250
    .line 251
    :cond_6
    invoke-direct {p0, v0, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->addShape(Lcom/intsig/office/common/shape/AbstractShape;Lcom/intsig/office/simpletext/model/ParagraphElement;)V

    .line 252
    .line 253
    .line 254
    const/4 p1, 0x0

    .line 255
    iput-boolean p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessWatermark:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    .line 257
    goto :goto_2

    .line 258
    :catch_0
    move-exception p1

    .line 259
    iget-object p2, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 260
    .line 261
    invoke-interface {p2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 262
    .line 263
    .line 264
    move-result-object p2

    .line 265
    invoke-virtual {p2}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    .line 266
    .line 267
    .line 268
    move-result-object p2

    .line 269
    invoke-virtual {p2, p1}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;)V

    .line 270
    .line 271
    .line 272
    :cond_7
    :goto_2
    return-void
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private processPictureAndDiagram(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;)V
    .locals 19

    .line 1
    move-object/from16 v8, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    move-object/from16 v4, p2

    .line 6
    .line 7
    const-string v1, "inline"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    const-string v1, "anchor"

    .line 16
    .line 17
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const/4 v1, 0x0

    .line 22
    move-object v5, v0

    .line 23
    const/4 v7, 0x0

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v0, 0x1

    .line 26
    move-object v5, v1

    .line 27
    const/4 v7, 0x1

    .line 28
    :goto_0
    if-eqz v5, :cond_d

    .line 29
    .line 30
    const-string v0, "graphic"

    .line 31
    .line 32
    invoke-interface {v5, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    if-eqz v0, :cond_d

    .line 37
    .line 38
    const-string v1, "graphicData"

    .line 39
    .line 40
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    if-eqz v0, :cond_d

    .line 45
    .line 46
    const-string v1, "pic"

    .line 47
    .line 48
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    const/4 v2, 0x2

    .line 53
    if-eqz v1, :cond_4

    .line 54
    .line 55
    const-string v0, "spPr"

    .line 56
    .line 57
    invoke-interface {v1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 58
    .line 59
    .line 60
    move-result-object v12

    .line 61
    if-eqz v12, :cond_d

    .line 62
    .line 63
    const-string v0, "blipFill"

    .line 64
    .line 65
    invoke-interface {v12, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 66
    .line 67
    .line 68
    move-result-object v3

    .line 69
    if-eqz v3, :cond_2

    .line 70
    .line 71
    invoke-interface {v12, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    const-string v3, "blip"

    .line 76
    .line 77
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    if-eqz v0, :cond_2

    .line 82
    .line 83
    const-string v3, "embed"

    .line 84
    .line 85
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    if-eqz v0, :cond_2

    .line 90
    .line 91
    iget-boolean v0, v8, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessHF:Z

    .line 92
    .line 93
    if-eqz v0, :cond_1

    .line 94
    .line 95
    iget-object v0, v8, Lcom/intsig/office/fc/doc/DOCXReader;->hfPart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 96
    .line 97
    if-eqz v0, :cond_1

    .line 98
    .line 99
    goto :goto_1

    .line 100
    :cond_1
    iget-object v0, v8, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 101
    .line 102
    goto :goto_1

    .line 103
    :cond_2
    const/4 v0, 0x0

    .line 104
    :goto_1
    move-object v11, v0

    .line 105
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    const-string v3, "xfrm"

    .line 110
    .line 111
    invoke-interface {v12, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 112
    .line 113
    .line 114
    move-result-object v3

    .line 115
    const/high16 v6, 0x3f800000    # 1.0f

    .line 116
    .line 117
    invoke-virtual {v0, v3, v6, v6}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getShapeAnchor(Lcom/intsig/office/fc/dom4j/Element;FF)Lcom/intsig/office/java/awt/Rectangle;

    .line 118
    .line 119
    .line 120
    move-result-object v0

    .line 121
    invoke-direct {v8, v1, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->addPicture(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/shape/PictureShape;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    if-eqz v0, :cond_d

    .line 126
    .line 127
    iget-object v9, v8, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 128
    .line 129
    iget-object v10, v8, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 130
    .line 131
    iget-object v13, v8, Lcom/intsig/office/fc/doc/DOCXReader;->themeColor:Ljava/util/Map;

    .line 132
    .line 133
    move-object v14, v0

    .line 134
    invoke-static/range {v9 .. v14}, Lcom/intsig/office/common/autoshape/AutoShapeDataKit;->processPictureShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;Lcom/intsig/office/common/shape/PictureShape;)V

    .line 135
    .line 136
    .line 137
    new-instance v1, Lcom/intsig/office/common/shape/WPPictureShape;

    .line 138
    .line 139
    invoke-direct {v1}, Lcom/intsig/office/common/shape/WPPictureShape;-><init>()V

    .line 140
    .line 141
    .line 142
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/shape/WPPictureShape;->setPictureShape(Lcom/intsig/office/common/shape/PictureShape;)V

    .line 143
    .line 144
    .line 145
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/AbstractShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 146
    .line 147
    .line 148
    move-result-object v3

    .line 149
    invoke-virtual {v1, v3}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 150
    .line 151
    .line 152
    if-nez v7, :cond_3

    .line 153
    .line 154
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/AbstractShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 155
    .line 156
    .line 157
    move-result-object v0

    .line 158
    invoke-direct {v8, v1, v5, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->processWrapAndPosition_Drawing(Lcom/intsig/office/common/shape/WPAbstractShape;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)V

    .line 159
    .line 160
    .line 161
    goto :goto_2

    .line 162
    :cond_3
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 163
    .line 164
    .line 165
    :goto_2
    invoke-direct {v8, v1, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->addShape(Lcom/intsig/office/common/shape/AbstractShape;Lcom/intsig/office/simpletext/model/ParagraphElement;)V

    .line 166
    .line 167
    .line 168
    goto/16 :goto_4

    .line 169
    .line 170
    :cond_4
    const-string v1, "chart"

    .line 171
    .line 172
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 173
    .line 174
    .line 175
    move-result-object v3

    .line 176
    const-string v6, "extent"

    .line 177
    .line 178
    const-string v9, "cy"

    .line 179
    .line 180
    const-string v10, "cx"

    .line 181
    .line 182
    const v11, 0x495f3e00    # 914400.0f

    .line 183
    .line 184
    .line 185
    const/high16 v12, 0x42c00000    # 96.0f

    .line 186
    .line 187
    if-eqz v3, :cond_a

    .line 188
    .line 189
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 190
    .line 191
    .line 192
    move-result-object v0

    .line 193
    if-eqz v0, :cond_d

    .line 194
    .line 195
    const-string v1, "id"

    .line 196
    .line 197
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 198
    .line 199
    .line 200
    move-result-object v3

    .line 201
    if-eqz v3, :cond_d

    .line 202
    .line 203
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 204
    .line 205
    .line 206
    move-result-object v0

    .line 207
    iget-object v1, v8, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 208
    .line 209
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationship(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 210
    .line 211
    .line 212
    move-result-object v0

    .line 213
    if-eqz v0, :cond_d

    .line 214
    .line 215
    iget-object v1, v8, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 216
    .line 217
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 218
    .line 219
    .line 220
    move-result-object v0

    .line 221
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 222
    .line 223
    .line 224
    move-result-object v16

    .line 225
    :try_start_0
    invoke-static {}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->instance()Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;

    .line 226
    .line 227
    .line 228
    move-result-object v13

    .line 229
    iget-object v14, v8, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 230
    .line 231
    iget-object v15, v8, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 232
    .line 233
    iget-object v0, v8, Lcom/intsig/office/fc/doc/DOCXReader;->themeColor:Ljava/util/Map;

    .line 234
    .line 235
    const/16 v18, 0x0

    .line 236
    .line 237
    move-object/from16 v17, v0

    .line 238
    .line 239
    invoke-virtual/range {v13 .. v18}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->read(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Ljava/util/Map;B)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 240
    .line 241
    .line 242
    move-result-object v0

    .line 243
    if-eqz v0, :cond_d

    .line 244
    .line 245
    new-instance v1, Lcom/intsig/office/java/awt/Rectangle;

    .line 246
    .line 247
    invoke-direct {v1}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 248
    .line 249
    .line 250
    const-string v3, "simplePos"

    .line 251
    .line 252
    invoke-interface {v5, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 253
    .line 254
    .line 255
    move-result-object v3

    .line 256
    if-eqz v3, :cond_6

    .line 257
    .line 258
    const-string v13, "x"

    .line 259
    .line 260
    invoke-interface {v3, v13}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 261
    .line 262
    .line 263
    move-result-object v13

    .line 264
    if-eqz v13, :cond_5

    .line 265
    .line 266
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 267
    .line 268
    .line 269
    move-result v13

    .line 270
    int-to-float v13, v13

    .line 271
    mul-float v13, v13, v12

    .line 272
    .line 273
    div-float/2addr v13, v11

    .line 274
    float-to-int v13, v13

    .line 275
    iput v13, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 276
    .line 277
    :cond_5
    const-string v13, "y"

    .line 278
    .line 279
    invoke-interface {v3, v13}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 280
    .line 281
    .line 282
    move-result-object v3

    .line 283
    if-eqz v3, :cond_6

    .line 284
    .line 285
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 286
    .line 287
    .line 288
    move-result v3

    .line 289
    int-to-float v3, v3

    .line 290
    mul-float v3, v3, v12

    .line 291
    .line 292
    div-float/2addr v3, v11

    .line 293
    float-to-int v3, v3

    .line 294
    iput v3, v1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 295
    .line 296
    :cond_6
    invoke-interface {v5, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 297
    .line 298
    .line 299
    move-result-object v3

    .line 300
    if-eqz v3, :cond_8

    .line 301
    .line 302
    invoke-interface {v3, v10}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 303
    .line 304
    .line 305
    move-result-object v6

    .line 306
    if-eqz v6, :cond_7

    .line 307
    .line 308
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 309
    .line 310
    .line 311
    move-result v6

    .line 312
    int-to-float v6, v6

    .line 313
    mul-float v6, v6, v12

    .line 314
    .line 315
    div-float/2addr v6, v11

    .line 316
    float-to-int v6, v6

    .line 317
    iput v6, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 318
    .line 319
    :cond_7
    invoke-interface {v3, v9}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 320
    .line 321
    .line 322
    move-result-object v3

    .line 323
    if-eqz v3, :cond_8

    .line 324
    .line 325
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 326
    .line 327
    .line 328
    move-result v3

    .line 329
    int-to-float v3, v3

    .line 330
    mul-float v3, v3, v12

    .line 331
    .line 332
    div-float/2addr v3, v11

    .line 333
    float-to-int v3, v3

    .line 334
    iput v3, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 335
    .line 336
    :cond_8
    new-instance v3, Lcom/intsig/office/common/shape/WPChartShape;

    .line 337
    .line 338
    invoke-direct {v3}, Lcom/intsig/office/common/shape/WPChartShape;-><init>()V

    .line 339
    .line 340
    .line 341
    invoke-virtual {v3, v0}, Lcom/intsig/office/common/shape/WPChartShape;->setAChart(Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;)V

    .line 342
    .line 343
    .line 344
    invoke-virtual {v3, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 345
    .line 346
    .line 347
    if-nez v7, :cond_9

    .line 348
    .line 349
    invoke-direct {v8, v3, v5, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->processWrapAndPosition_Drawing(Lcom/intsig/office/common/shape/WPAbstractShape;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)V

    .line 350
    .line 351
    .line 352
    goto :goto_3

    .line 353
    :cond_9
    invoke-virtual {v3, v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 354
    .line 355
    .line 356
    :goto_3
    invoke-direct {v8, v3, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->addShape(Lcom/intsig/office/common/shape/AbstractShape;Lcom/intsig/office/simpletext/model/ParagraphElement;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 357
    .line 358
    .line 359
    goto/16 :goto_4

    .line 360
    .line 361
    :catch_0
    return-void

    .line 362
    :cond_a
    const-string v1, "relIds"

    .line 363
    .line 364
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 365
    .line 366
    .line 367
    move-result-object v2

    .line 368
    if-eqz v2, :cond_d

    .line 369
    .line 370
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 371
    .line 372
    .line 373
    move-result-object v0

    .line 374
    if-eqz v0, :cond_d

    .line 375
    .line 376
    const-string v1, "dm"

    .line 377
    .line 378
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 379
    .line 380
    .line 381
    move-result-object v0

    .line 382
    if-eqz v0, :cond_d

    .line 383
    .line 384
    :try_start_1
    iget-object v1, v8, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 385
    .line 386
    const-string v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/diagramData"

    .line 387
    .line 388
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 389
    .line 390
    .line 391
    move-result-object v1

    .line 392
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationshipByID(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 393
    .line 394
    .line 395
    move-result-object v0

    .line 396
    if-eqz v0, :cond_d

    .line 397
    .line 398
    new-instance v13, Lcom/intsig/office/java/awt/Rectangle;

    .line 399
    .line 400
    invoke-direct {v13}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 401
    .line 402
    .line 403
    invoke-interface {v5, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 404
    .line 405
    .line 406
    move-result-object v1

    .line 407
    if-eqz v1, :cond_c

    .line 408
    .line 409
    invoke-interface {v1, v10}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 410
    .line 411
    .line 412
    move-result-object v2

    .line 413
    if-eqz v2, :cond_b

    .line 414
    .line 415
    invoke-interface {v1, v10}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 416
    .line 417
    .line 418
    move-result-object v2

    .line 419
    if-eqz v2, :cond_b

    .line 420
    .line 421
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 422
    .line 423
    .line 424
    move-result v3

    .line 425
    if-lez v3, :cond_b

    .line 426
    .line 427
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 428
    .line 429
    .line 430
    move-result v2

    .line 431
    int-to-float v2, v2

    .line 432
    mul-float v2, v2, v12

    .line 433
    .line 434
    div-float/2addr v2, v11

    .line 435
    float-to-int v2, v2

    .line 436
    iput v2, v13, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 437
    .line 438
    :cond_b
    invoke-interface {v1, v9}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 439
    .line 440
    .line 441
    move-result-object v2

    .line 442
    if-eqz v2, :cond_c

    .line 443
    .line 444
    invoke-interface {v1, v9}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 445
    .line 446
    .line 447
    move-result-object v1

    .line 448
    if-eqz v1, :cond_c

    .line 449
    .line 450
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    .line 451
    .line 452
    .line 453
    move-result v2

    .line 454
    if-lez v2, :cond_c

    .line 455
    .line 456
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 457
    .line 458
    .line 459
    move-result v1

    .line 460
    int-to-float v1, v1

    .line 461
    mul-float v1, v1, v12

    .line 462
    .line 463
    div-float/2addr v1, v11

    .line 464
    float-to-int v1, v1

    .line 465
    iput v1, v13, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 466
    .line 467
    :cond_c
    iget-object v1, v8, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 468
    .line 469
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 470
    .line 471
    .line 472
    move-result-object v0

    .line 473
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 474
    .line 475
    .line 476
    move-result-object v3

    .line 477
    iget-object v1, v8, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 478
    .line 479
    iget-object v2, v8, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 480
    .line 481
    move-object/from16 v0, p0

    .line 482
    .line 483
    move-object/from16 v4, p2

    .line 484
    .line 485
    move-object v6, v13

    .line 486
    invoke-direct/range {v0 .. v7}, Lcom/intsig/office/fc/doc/DOCXReader;->processSmart(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 487
    .line 488
    .line 489
    :catch_1
    :cond_d
    :goto_4
    return-void
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private processPoints(Ljava/lang/String;)[Landroid/graphics/PointF;
    .locals 6

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    const-string v1, ","

    .line 9
    .line 10
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    array-length v1, p1

    .line 15
    const/4 v2, 0x0

    .line 16
    :goto_0
    add-int/lit8 v3, v1, -0x1

    .line 17
    .line 18
    if-ge v2, v3, :cond_0

    .line 19
    .line 20
    new-instance v3, Landroid/graphics/PointF;

    .line 21
    .line 22
    aget-object v4, p1, v2

    .line 23
    .line 24
    invoke-direct {p0, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->getValue(Ljava/lang/String;)F

    .line 25
    .line 26
    .line 27
    move-result v4

    .line 28
    add-int/lit8 v5, v2, 0x1

    .line 29
    .line 30
    aget-object v5, p1, v5

    .line 31
    .line 32
    invoke-direct {p0, v5}, Lcom/intsig/office/fc/doc/DOCXReader;->getValue(Ljava/lang/String;)F

    .line 33
    .line 34
    .line 35
    move-result v5

    .line 36
    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 37
    .line 38
    .line 39
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    .line 41
    .line 42
    add-int/lit8 v2, v2, 0x2

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    new-array p1, p1, [Landroid/graphics/PointF;

    .line 50
    .line 51
    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    check-cast p1, [Landroid/graphics/PointF;

    .line 56
    .line 57
    return-object p1
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private processPolygonZoom(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/AbstractShape;Lcom/intsig/office/common/shape/GroupShape;FF)F
    .locals 16

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    move-object/from16 v1, p2

    .line 4
    .line 5
    if-eqz v0, :cond_14

    .line 6
    .line 7
    if-eqz v1, :cond_14

    .line 8
    .line 9
    const-string v2, "style"

    .line 10
    .line 11
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 12
    .line 13
    .line 14
    move-result-object v3

    .line 15
    if-eqz v3, :cond_14

    .line 16
    .line 17
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    if-eqz v3, :cond_14

    .line 22
    .line 23
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    if-eqz v2, :cond_14

    .line 28
    .line 29
    const-string v3, ";"

    .line 30
    .line 31
    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    const/4 v3, 0x0

    .line 36
    const/4 v4, 0x0

    .line 37
    const/4 v5, 0x0

    .line 38
    const/4 v6, 0x0

    .line 39
    const/4 v7, 0x0

    .line 40
    const/4 v8, 0x0

    .line 41
    :goto_0
    array-length v9, v2

    .line 42
    const/4 v10, 0x1

    .line 43
    if-ge v8, v9, :cond_12

    .line 44
    .line 45
    aget-object v9, v2, v8

    .line 46
    .line 47
    const-string v11, ":"

    .line 48
    .line 49
    invoke-virtual {v9, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v9

    .line 53
    if-eqz v9, :cond_11

    .line 54
    .line 55
    aget-object v11, v9, v4

    .line 56
    .line 57
    if-eqz v11, :cond_11

    .line 58
    .line 59
    aget-object v12, v9, v10

    .line 60
    .line 61
    if-eqz v12, :cond_11

    .line 62
    .line 63
    const-string v12, "left"

    .line 64
    .line 65
    invoke-virtual {v12, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 66
    .line 67
    .line 68
    move-result v11

    .line 69
    const v12, 0x495f3e00    # 914400.0f

    .line 70
    .line 71
    .line 72
    const-string v13, "in"

    .line 73
    .line 74
    const-string v14, "pt"

    .line 75
    .line 76
    const/high16 v15, 0x42900000    # 72.0f

    .line 77
    .line 78
    if-eqz v11, :cond_2

    .line 79
    .line 80
    aget-object v11, v9, v10

    .line 81
    .line 82
    invoke-virtual {v11, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 83
    .line 84
    .line 85
    move-result v11

    .line 86
    if-lez v11, :cond_0

    .line 87
    .line 88
    aget-object v9, v9, v10

    .line 89
    .line 90
    invoke-virtual {v9, v4, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v9

    .line 94
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 95
    .line 96
    .line 97
    move-result v9

    .line 98
    :goto_1
    add-float/2addr v3, v9

    .line 99
    goto/16 :goto_6

    .line 100
    .line 101
    :cond_0
    aget-object v11, v9, v10

    .line 102
    .line 103
    invoke-virtual {v11, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 104
    .line 105
    .line 106
    move-result v11

    .line 107
    if-lez v11, :cond_1

    .line 108
    .line 109
    aget-object v9, v9, v10

    .line 110
    .line 111
    invoke-virtual {v9, v4, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object v9

    .line 115
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 116
    .line 117
    .line 118
    move-result v9

    .line 119
    :goto_2
    mul-float v9, v9, v15

    .line 120
    .line 121
    goto :goto_1

    .line 122
    :cond_1
    aget-object v9, v9, v10

    .line 123
    .line 124
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 125
    .line 126
    .line 127
    move-result v9

    .line 128
    mul-float v9, v9, p4

    .line 129
    .line 130
    mul-float v9, v9, v15

    .line 131
    .line 132
    div-float/2addr v9, v12

    .line 133
    goto :goto_1

    .line 134
    :cond_2
    const-string v11, "top"

    .line 135
    .line 136
    aget-object v12, v9, v4

    .line 137
    .line 138
    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 139
    .line 140
    .line 141
    move-result v11

    .line 142
    if-eqz v11, :cond_5

    .line 143
    .line 144
    aget-object v11, v9, v10

    .line 145
    .line 146
    invoke-virtual {v11, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 147
    .line 148
    .line 149
    move-result v11

    .line 150
    if-lez v11, :cond_3

    .line 151
    .line 152
    aget-object v9, v9, v10

    .line 153
    .line 154
    invoke-virtual {v9, v4, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v9

    .line 158
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 159
    .line 160
    .line 161
    move-result v9

    .line 162
    :goto_3
    add-float/2addr v5, v9

    .line 163
    goto/16 :goto_6

    .line 164
    .line 165
    :cond_3
    aget-object v11, v9, v10

    .line 166
    .line 167
    invoke-virtual {v11, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 168
    .line 169
    .line 170
    move-result v11

    .line 171
    if-lez v11, :cond_4

    .line 172
    .line 173
    aget-object v9, v9, v10

    .line 174
    .line 175
    invoke-virtual {v9, v4, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 176
    .line 177
    .line 178
    move-result-object v9

    .line 179
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 180
    .line 181
    .line 182
    move-result v9

    .line 183
    :goto_4
    mul-float v9, v9, v15

    .line 184
    .line 185
    goto :goto_3

    .line 186
    :cond_4
    aget-object v9, v9, v10

    .line 187
    .line 188
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 189
    .line 190
    .line 191
    move-result v9

    .line 192
    mul-float v9, v9, p5

    .line 193
    .line 194
    :goto_5
    mul-float v9, v9, v15

    .line 195
    .line 196
    const v10, 0x495f3e00    # 914400.0f

    .line 197
    .line 198
    .line 199
    div-float/2addr v9, v10

    .line 200
    goto :goto_3

    .line 201
    :cond_5
    const-string v11, "margin-left"

    .line 202
    .line 203
    aget-object v12, v9, v4

    .line 204
    .line 205
    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 206
    .line 207
    .line 208
    move-result v11

    .line 209
    if-eqz v11, :cond_8

    .line 210
    .line 211
    aget-object v11, v9, v10

    .line 212
    .line 213
    invoke-virtual {v11, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 214
    .line 215
    .line 216
    move-result v11

    .line 217
    if-lez v11, :cond_6

    .line 218
    .line 219
    aget-object v9, v9, v10

    .line 220
    .line 221
    invoke-virtual {v9, v4, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 222
    .line 223
    .line 224
    move-result-object v9

    .line 225
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 226
    .line 227
    .line 228
    move-result v9

    .line 229
    goto/16 :goto_1

    .line 230
    .line 231
    :cond_6
    aget-object v11, v9, v10

    .line 232
    .line 233
    invoke-virtual {v11, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 234
    .line 235
    .line 236
    move-result v11

    .line 237
    if-lez v11, :cond_7

    .line 238
    .line 239
    aget-object v9, v9, v10

    .line 240
    .line 241
    invoke-virtual {v9, v4, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 242
    .line 243
    .line 244
    move-result-object v9

    .line 245
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 246
    .line 247
    .line 248
    move-result v9

    .line 249
    goto/16 :goto_2

    .line 250
    .line 251
    :cond_7
    aget-object v9, v9, v10

    .line 252
    .line 253
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 254
    .line 255
    .line 256
    move-result v9

    .line 257
    mul-float v9, v9, v15

    .line 258
    .line 259
    const v10, 0x495f3e00    # 914400.0f

    .line 260
    .line 261
    .line 262
    div-float/2addr v9, v10

    .line 263
    goto/16 :goto_1

    .line 264
    .line 265
    :cond_8
    const-string v11, "margin-top"

    .line 266
    .line 267
    aget-object v12, v9, v4

    .line 268
    .line 269
    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 270
    .line 271
    .line 272
    move-result v11

    .line 273
    if-eqz v11, :cond_b

    .line 274
    .line 275
    aget-object v11, v9, v10

    .line 276
    .line 277
    invoke-virtual {v11, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 278
    .line 279
    .line 280
    move-result v11

    .line 281
    if-lez v11, :cond_9

    .line 282
    .line 283
    aget-object v9, v9, v10

    .line 284
    .line 285
    invoke-virtual {v9, v4, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 286
    .line 287
    .line 288
    move-result-object v9

    .line 289
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 290
    .line 291
    .line 292
    move-result v9

    .line 293
    goto/16 :goto_3

    .line 294
    .line 295
    :cond_9
    aget-object v11, v9, v10

    .line 296
    .line 297
    invoke-virtual {v11, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 298
    .line 299
    .line 300
    move-result v11

    .line 301
    if-lez v11, :cond_a

    .line 302
    .line 303
    aget-object v9, v9, v10

    .line 304
    .line 305
    invoke-virtual {v9, v4, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 306
    .line 307
    .line 308
    move-result-object v9

    .line 309
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 310
    .line 311
    .line 312
    move-result v9

    .line 313
    goto/16 :goto_4

    .line 314
    .line 315
    :cond_a
    aget-object v9, v9, v10

    .line 316
    .line 317
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 318
    .line 319
    .line 320
    move-result v9

    .line 321
    goto :goto_5

    .line 322
    :cond_b
    const-string v11, "width"

    .line 323
    .line 324
    aget-object v12, v9, v4

    .line 325
    .line 326
    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 327
    .line 328
    .line 329
    move-result v11

    .line 330
    if-eqz v11, :cond_e

    .line 331
    .line 332
    aget-object v6, v9, v10

    .line 333
    .line 334
    invoke-virtual {v6, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 335
    .line 336
    .line 337
    move-result v6

    .line 338
    if-lez v6, :cond_c

    .line 339
    .line 340
    aget-object v9, v9, v10

    .line 341
    .line 342
    invoke-virtual {v9, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 343
    .line 344
    .line 345
    move-result-object v6

    .line 346
    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 347
    .line 348
    .line 349
    move-result v6

    .line 350
    goto :goto_6

    .line 351
    :cond_c
    aget-object v6, v9, v10

    .line 352
    .line 353
    invoke-virtual {v6, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 354
    .line 355
    .line 356
    move-result v6

    .line 357
    if-lez v6, :cond_d

    .line 358
    .line 359
    aget-object v9, v9, v10

    .line 360
    .line 361
    invoke-virtual {v9, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 362
    .line 363
    .line 364
    move-result-object v6

    .line 365
    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 366
    .line 367
    .line 368
    move-result v6

    .line 369
    mul-float v6, v6, v15

    .line 370
    .line 371
    goto :goto_6

    .line 372
    :cond_d
    aget-object v6, v9, v10

    .line 373
    .line 374
    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 375
    .line 376
    .line 377
    move-result v6

    .line 378
    mul-float v6, v6, p4

    .line 379
    .line 380
    mul-float v6, v6, v15

    .line 381
    .line 382
    const v9, 0x495f3e00    # 914400.0f

    .line 383
    .line 384
    .line 385
    div-float/2addr v6, v9

    .line 386
    goto :goto_6

    .line 387
    :cond_e
    const-string v11, "height"

    .line 388
    .line 389
    aget-object v12, v9, v4

    .line 390
    .line 391
    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 392
    .line 393
    .line 394
    move-result v11

    .line 395
    if-eqz v11, :cond_11

    .line 396
    .line 397
    aget-object v7, v9, v10

    .line 398
    .line 399
    invoke-virtual {v7, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 400
    .line 401
    .line 402
    move-result v7

    .line 403
    if-lez v7, :cond_f

    .line 404
    .line 405
    aget-object v9, v9, v10

    .line 406
    .line 407
    invoke-virtual {v9, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 408
    .line 409
    .line 410
    move-result-object v7

    .line 411
    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 412
    .line 413
    .line 414
    move-result v7

    .line 415
    goto :goto_6

    .line 416
    :cond_f
    aget-object v7, v9, v10

    .line 417
    .line 418
    invoke-virtual {v7, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 419
    .line 420
    .line 421
    move-result v7

    .line 422
    if-lez v7, :cond_10

    .line 423
    .line 424
    aget-object v9, v9, v10

    .line 425
    .line 426
    invoke-virtual {v9, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 427
    .line 428
    .line 429
    move-result-object v7

    .line 430
    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 431
    .line 432
    .line 433
    move-result v7

    .line 434
    mul-float v7, v7, v15

    .line 435
    .line 436
    goto :goto_6

    .line 437
    :cond_10
    aget-object v7, v9, v10

    .line 438
    .line 439
    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 440
    .line 441
    .line 442
    move-result v7

    .line 443
    mul-float v7, v7, p5

    .line 444
    .line 445
    mul-float v7, v7, v15

    .line 446
    .line 447
    const v9, 0x495f3e00    # 914400.0f

    .line 448
    .line 449
    .line 450
    div-float/2addr v7, v9

    .line 451
    :cond_11
    :goto_6
    add-int/lit8 v8, v8, 0x1

    .line 452
    .line 453
    goto/16 :goto_0

    .line 454
    .line 455
    :cond_12
    new-instance v2, Lcom/intsig/office/java/awt/Rectangle;

    .line 456
    .line 457
    invoke-direct {v2}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 458
    .line 459
    .line 460
    const v8, 0x3faaaaab

    .line 461
    .line 462
    .line 463
    mul-float v3, v3, v8

    .line 464
    .line 465
    float-to-int v3, v3

    .line 466
    iput v3, v2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 467
    .line 468
    mul-float v5, v5, v8

    .line 469
    .line 470
    float-to-int v3, v5

    .line 471
    iput v3, v2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 472
    .line 473
    mul-float v6, v6, v8

    .line 474
    .line 475
    float-to-int v3, v6

    .line 476
    iput v3, v2, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 477
    .line 478
    mul-float v7, v7, v8

    .line 479
    .line 480
    float-to-int v3, v7

    .line 481
    iput v3, v2, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 482
    .line 483
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/common/shape/AbstractShape;->getType()S

    .line 484
    .line 485
    .line 486
    move-result v3

    .line 487
    const/4 v5, 0x7

    .line 488
    if-eq v3, v5, :cond_13

    .line 489
    .line 490
    move-object v3, v1

    .line 491
    check-cast v3, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 492
    .line 493
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/WPAutoShape;->getGroupShape()Lcom/intsig/office/common/shape/WPGroupShape;

    .line 494
    .line 495
    .line 496
    move-result-object v3

    .line 497
    if-nez v3, :cond_13

    .line 498
    .line 499
    move-object/from16 v3, p0

    .line 500
    .line 501
    move-object/from16 v5, p3

    .line 502
    .line 503
    invoke-direct {v3, v5, v2}, Lcom/intsig/office/fc/doc/DOCXReader;->processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;

    .line 504
    .line 505
    .line 506
    goto :goto_7

    .line 507
    :cond_13
    move-object/from16 v3, p0

    .line 508
    .line 509
    :goto_7
    instance-of v5, v1, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 510
    .line 511
    if-eqz v5, :cond_15

    .line 512
    .line 513
    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 514
    .line 515
    invoke-virtual {v1}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 516
    .line 517
    .line 518
    move-result v1

    .line 519
    const/16 v5, 0xe9

    .line 520
    .line 521
    if-ne v1, v5, :cond_15

    .line 522
    .line 523
    const-string v1, "coordsize"

    .line 524
    .line 525
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 526
    .line 527
    .line 528
    move-result-object v0

    .line 529
    if-eqz v0, :cond_15

    .line 530
    .line 531
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 532
    .line 533
    .line 534
    move-result v1

    .line 535
    if-lez v1, :cond_15

    .line 536
    .line 537
    const-string v1, ","

    .line 538
    .line 539
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 540
    .line 541
    .line 542
    move-result-object v0

    .line 543
    aget-object v1, v0, v4

    .line 544
    .line 545
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 546
    .line 547
    .line 548
    move-result v1

    .line 549
    int-to-float v1, v1

    .line 550
    aget-object v0, v0, v10

    .line 551
    .line 552
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 553
    .line 554
    .line 555
    move-result v0

    .line 556
    int-to-float v0, v0

    .line 557
    iget v4, v2, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 558
    .line 559
    int-to-float v4, v4

    .line 560
    div-float/2addr v1, v4

    .line 561
    iget v2, v2, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 562
    .line 563
    int-to-float v2, v2

    .line 564
    div-float/2addr v0, v2

    .line 565
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    .line 566
    .line 567
    .line 568
    move-result v0

    .line 569
    return v0

    .line 570
    :cond_14
    move-object/from16 v3, p0

    .line 571
    .line 572
    :cond_15
    const/high16 v0, 0x3f800000    # 1.0f

    .line 573
    .line 574
    return v0
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method private processRelativeShapeSize()V
    .locals 9

    .line 1
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->secElem:Lcom/intsig/office/simpletext/model/SectionElement;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    iget-object v2, p0, Lcom/intsig/office/fc/doc/DOCXReader;->secElem:Lcom/intsig/office/simpletext/model/SectionElement;

    .line 20
    .line 21
    invoke-virtual {v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-virtual {v1, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    iget-object v2, p0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeType:Ljava/util/List;

    .line 30
    .line 31
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    if-eqz v3, :cond_2

    .line 40
    .line 41
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    check-cast v3, Lcom/intsig/office/common/shape/IShape;

    .line 46
    .line 47
    iget-object v4, p0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeValue:Ljava/util/Map;

    .line 48
    .line 49
    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    move-result-object v4

    .line 53
    check-cast v4, [I

    .line 54
    .line 55
    invoke-interface {v3}, Lcom/intsig/office/common/shape/IShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 56
    .line 57
    .line 58
    move-result-object v3

    .line 59
    const/4 v5, 0x0

    .line 60
    aget v5, v4, v5

    .line 61
    .line 62
    const/high16 v6, 0x447a0000    # 1000.0f

    .line 63
    .line 64
    const v7, 0x3d888889

    .line 65
    .line 66
    .line 67
    if-lez v5, :cond_1

    .line 68
    .line 69
    int-to-float v8, v0

    .line 70
    mul-float v8, v8, v7

    .line 71
    .line 72
    int-to-float v5, v5

    .line 73
    mul-float v8, v8, v5

    .line 74
    .line 75
    div-float/2addr v8, v6

    .line 76
    float-to-int v5, v8

    .line 77
    iput v5, v3, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 78
    .line 79
    :cond_1
    const/4 v5, 0x2

    .line 80
    aget v4, v4, v5

    .line 81
    .line 82
    if-lez v4, :cond_0

    .line 83
    .line 84
    int-to-float v5, v1

    .line 85
    mul-float v5, v5, v7

    .line 86
    .line 87
    int-to-float v4, v4

    .line 88
    mul-float v5, v5, v4

    .line 89
    .line 90
    div-float/2addr v5, v6

    .line 91
    float-to-int v4, v5

    .line 92
    iput v4, v3, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_2
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private processRow(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/wp/model/TableElement;ZLjava/lang/String;)V
    .locals 8

    .line 1
    new-instance v6, Lcom/intsig/office/wp/model/RowElement;

    .line 2
    .line 3
    invoke-direct {v6}, Lcom/intsig/office/wp/model/RowElement;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-wide v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 7
    .line 8
    invoke-virtual {v6, v0, v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 9
    .line 10
    .line 11
    const-string v0, "trPr"

    .line 12
    .line 13
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-virtual {v6}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->processRowAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 24
    .line 25
    .line 26
    :cond_0
    const-string v0, "tc"

    .line 27
    .line 28
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    const/4 v0, 0x0

    .line 37
    const/4 v7, 0x0

    .line 38
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    if-eqz v0, :cond_1

    .line 43
    .line 44
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    move-object v1, v0

    .line 49
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 50
    .line 51
    move-object v0, p0

    .line 52
    move-object v2, v6

    .line 53
    move v3, v7

    .line 54
    move v4, p3

    .line 55
    move-object v5, p4

    .line 56
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/doc/DOCXReader;->processCell(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/wp/model/RowElement;IZLjava/lang/String;)I

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    add-int/2addr v7, v0

    .line 61
    goto :goto_0

    .line 62
    :cond_1
    iget-wide p3, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 63
    .line 64
    invoke-virtual {v6, p3, p4}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p2, v6}, Lcom/intsig/office/wp/model/TableElement;->appendRow(Lcom/intsig/office/wp/model/RowElement;)V

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private processRowAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 2

    .line 1
    const-string v0, "trHeight"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v1, "val"

    .line 14
    .line 15
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setTableRowHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 24
    .line 25
    .line 26
    :cond_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private processRun(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;BZ)Z
    .locals 23

    move-object/from16 v6, p0

    move-object/from16 v7, p2

    move/from16 v8, p3

    .line 2
    invoke-interface/range {p1 .. p1}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    move-result-object v0

    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    const/4 v11, 0x0

    const-string v12, ""

    move-object v5, v12

    move-object v15, v5

    const/4 v0, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    :goto_0
    const/16 v16, 0x0

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const-string v2, "rPr"

    if-eqz v1, :cond_33

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "smartTag"

    .line 5
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 6
    invoke-direct {v6, v1, v7, v11}, Lcom/intsig/office/fc/doc/DOCXReader;->processRun(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;Z)Z

    move-result v1

    :goto_2
    move v13, v1

    :goto_3
    move-object/from16 v21, v9

    :goto_4
    const/4 v3, 0x0

    goto/16 :goto_13

    :cond_0
    const-string v4, "hyperlink"

    .line 7
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 8
    invoke-direct {v6, v1, v7}, Lcom/intsig/office/fc/doc/DOCXReader;->processHyperlinkRun(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;)Lcom/intsig/office/simpletext/model/LeafElement;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v13, 0x1

    goto :goto_1

    :cond_1
    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    const-string v4, "bookmarkStart"

    .line 9
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v2, "name"

    .line 10
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_3

    .line 11
    iget-object v1, v6, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    invoke-interface {v1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/office/system/SysKit;->getBookmarkManage()Lcom/intsig/office/common/bookmark/BookmarkManage;

    move-result-object v1

    new-instance v2, Lcom/intsig/office/common/bookmark/Bookmark;

    iget-wide v3, v6, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    move-object/from16 v17, v2

    move-wide/from16 v19, v3

    move-wide/from16 v21, v3

    invoke-direct/range {v17 .. v22}, Lcom/intsig/office/common/bookmark/Bookmark;-><init>(Ljava/lang/String;JJ)V

    invoke-virtual {v1, v2}, Lcom/intsig/office/common/bookmark/BookmarkManage;->addBookmark(Lcom/intsig/office/common/bookmark/Bookmark;)V

    :cond_3
    move-object v11, v5

    move-object/from16 v21, v9

    goto/16 :goto_d

    :cond_4
    const-string v4, "fldSimple"

    .line 12
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    const-string v10, "PAGE"

    const-string v11, "NUMPAGES"

    const/16 v19, 0x2

    const/16 v20, -0x1

    if-eqz v4, :cond_7

    const-string v0, "instr"

    .line 13
    invoke-interface {v1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 14
    invoke-virtual {v0, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v0, 0x0

    const/4 v10, 0x2

    goto :goto_5

    .line 15
    :cond_5
    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    const/4 v10, 0x1

    goto :goto_5

    :cond_6
    const/4 v0, 0x0

    const/4 v10, -0x1

    .line 16
    :goto_5
    invoke-direct {v6, v1, v7, v10, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->processRun(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;BZ)Z

    move-result v1

    move v13, v1

    move-object/from16 v21, v9

    const/4 v0, 0x0

    goto :goto_4

    :cond_7
    const-string v4, "sdt"

    .line 17
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    const-string v2, "sdtContent"

    .line 18
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v2

    if-eqz v2, :cond_a

    const-string v0, "sdtPr"

    .line 19
    invoke-interface {v1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_9

    const-string v1, "docPartObj"

    .line 20
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_9

    const-string v1, "docPartGallery"

    .line 21
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_9

    const-string v1, "val"

    .line 22
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 23
    iget-boolean v1, v6, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessHF:Z

    if-eqz v1, :cond_8

    const-string v1, "Margins"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v1, 0x0

    return v1

    :cond_8
    const-string v1, "Watermarks"

    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    .line 25
    iput-boolean v0, v6, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessWatermark:Z

    :cond_9
    const/4 v4, 0x0

    .line 26
    invoke-direct {v6, v2, v7, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->processRun(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;Z)Z

    move-result v13

    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_b
    const-string v4, "ins"

    .line 27
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/4 v4, 0x0

    .line 28
    invoke-direct {v6, v1, v7, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->processRun(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;Z)Z

    move-result v1

    goto/16 :goto_2

    :cond_c
    const-string v4, "r"

    .line 29
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_31

    const-string v3, "fldChar"

    .line 30
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    move-object/from16 v21, v9

    if-eqz v3, :cond_1a

    const-string v9, "fldCharType"

    .line 31
    invoke-interface {v3, v9}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v9, "begin"

    .line 32
    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    move-object/from16 v9, v21

    const/4 v11, 0x0

    const/16 v16, 0x1

    goto/16 :goto_1

    :cond_d
    const-string v9, "separate"

    .line 33
    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    move-object v11, v5

    goto/16 :goto_d

    :cond_e
    const-string v9, "end"

    .line 34
    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a

    if-eqz v15, :cond_19

    const/16 v3, 0x25cb

    .line 35
    invoke-virtual {v15, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    const-string v4, ","

    if-lez v3, :cond_f

    .line 36
    invoke-virtual {v15, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/16 v17, 0x1

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v15, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x0

    :goto_6
    const/4 v4, -0x1

    goto :goto_7

    :cond_f
    const/16 v17, 0x1

    const/16 v3, 0x25a1

    .line 37
    invoke-virtual {v15, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-lez v3, :cond_10

    .line 38
    invoke-virtual {v15, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v15, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x1

    goto :goto_6

    :cond_10
    const/16 v3, 0x25b3

    .line 39
    invoke-virtual {v15, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-lez v3, :cond_11

    .line 40
    invoke-virtual {v15, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v15, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x2

    goto :goto_6

    :cond_11
    const/16 v3, 0x25c7

    .line 41
    invoke-virtual {v15, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-lez v3, :cond_12

    .line 42
    invoke-virtual {v15, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v15, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x3

    goto :goto_6

    .line 43
    :cond_12
    invoke-virtual {v15, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_13

    const/4 v3, -0x1

    const/4 v4, 0x2

    goto :goto_7

    .line 44
    :cond_13
    invoke-virtual {v15, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_14

    const/4 v3, -0x1

    const/4 v4, 0x1

    goto :goto_7

    :cond_14
    const/4 v3, -0x1

    goto :goto_6

    .line 45
    :goto_7
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_18

    .line 46
    new-instance v0, Lcom/intsig/office/simpletext/model/LeafElement;

    invoke-direct {v0, v5}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 47
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    if-eqz v1, :cond_15

    .line 48
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v2

    invoke-direct {v6, v1, v2}, Lcom/intsig/office/fc/doc/DOCXReader;->processRunAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 49
    :cond_15
    iget-wide v1, v6, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 50
    iget-wide v1, v6, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    int-to-long v9, v5

    add-long/2addr v1, v9

    iput-wide v1, v6, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 51
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    if-ltz v3, :cond_16

    .line 52
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v1

    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setEncloseChanacterType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_8

    .line 53
    :cond_16
    iget-boolean v1, v6, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessHF:Z

    if-eqz v1, :cond_17

    iget-object v1, v6, Lcom/intsig/office/fc/doc/DOCXReader;->hfPart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    if-eqz v1, :cond_17

    if-lez v4, :cond_17

    .line 54
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v1

    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontPageNumberType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 55
    :cond_17
    :goto_8
    invoke-virtual {v7, v0}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    const/4 v13, 0x1

    :cond_18
    move-object v5, v12

    move-object v15, v5

    :cond_19
    move-object/from16 v9, v21

    const/4 v11, 0x0

    goto/16 :goto_0

    :cond_1a
    const-string v3, "t"

    if-eqz v16, :cond_1c

    const-string v2, "instrText"

    .line 56
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v2

    if-eqz v2, :cond_1b

    .line 57
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_13

    .line 58
    :cond_1b
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    if-eqz v1, :cond_32

    .line 59
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_13

    :cond_1c
    const-string v9, "object"

    .line 60
    invoke-interface {v1, v9}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v9

    if-eqz v9, :cond_1e

    .line 61
    invoke-interface {v9}, Lcom/intsig/office/fc/dom4j/Element;->elementIterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_9
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 62
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v10, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    move-object/from16 v2, p2

    move-object v11, v5

    move v5, v10

    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/doc/DOCXReader;->processAutoShapeForPict(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/common/shape/WPGroupShape;FF)V

    move-object v5, v11

    goto :goto_9

    :cond_1d
    move-object v11, v5

    goto :goto_b

    :cond_1e
    move-object v11, v5

    const-string v5, "drawing"

    .line 63
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v5

    if-eqz v5, :cond_20

    .line 64
    invoke-direct {v6, v5, v7}, Lcom/intsig/office/fc/doc/DOCXReader;->processPictureAndDiagram(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;)V

    :cond_1f
    :goto_a
    move-object v5, v11

    :goto_b
    move-object/from16 v9, v21

    const/4 v0, 0x0

    goto/16 :goto_14

    :cond_20
    const-string v5, "pict"

    .line 65
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v5

    if-eqz v5, :cond_21

    .line 66
    invoke-interface {v5}, Lcom/intsig/office/fc/dom4j/Element;->elementIterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_c
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 67
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    move-object/from16 v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/doc/DOCXReader;->processAutoShapeForPict(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/common/shape/WPGroupShape;FF)V

    goto :goto_c

    :cond_21
    const-string v5, "AlternateContent"

    .line 68
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v5

    if-eqz v5, :cond_22

    .line 69
    invoke-direct {v6, v5, v7}, Lcom/intsig/office/fc/doc/DOCXReader;->processAlternateContent(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;)V

    goto :goto_a

    :cond_22
    const-string v5, "ruby"

    .line 70
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v5

    if-eqz v5, :cond_23

    const-string v9, "rubyBase"

    .line 71
    invoke-interface {v5, v9}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v5

    if-eqz v5, :cond_23

    .line 72
    invoke-interface {v5, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    if-nez v1, :cond_23

    :goto_d
    move-object v5, v11

    goto/16 :goto_13

    :cond_23
    const-string v4, "br"

    .line 73
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v4

    .line 74
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    const/16 v5, 0xb

    if-eqz v3, :cond_24

    .line 75
    invoke-interface {v3}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    move-result-object v3

    if-eqz v4, :cond_27

    .line 76
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_e

    :cond_24
    if-eqz v4, :cond_26

    const-string v3, "type"

    .line 77
    invoke-interface {v4, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "page"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_25

    const/16 v3, 0xc

    .line 78
    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    const/4 v14, 0x1

    goto :goto_e

    .line 79
    :cond_25
    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    goto :goto_e

    :cond_26
    move-object v3, v12

    .line 80
    :cond_27
    :goto_e
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_30

    .line 81
    new-instance v0, Lcom/intsig/office/simpletext/model/LeafElement;

    invoke-direct {v0, v3}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 82
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    if-eqz v1, :cond_28

    .line 83
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v2

    invoke-direct {v6, v1, v2}, Lcom/intsig/office/fc/doc/DOCXReader;->processRunAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 84
    :cond_28
    iget-boolean v1, v6, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessHF:Z

    if-eqz v1, :cond_29

    iget-object v1, v6, Lcom/intsig/office/fc/doc/DOCXReader;->hfPart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    if-eqz v1, :cond_29

    if-lez v8, :cond_29

    .line 85
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v1

    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v2

    invoke-virtual {v1, v2, v8}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontPageNumberType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 86
    :cond_29
    iget-wide v1, v6, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 87
    iget-wide v1, v6, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    int-to-long v3, v4

    add-long/2addr v1, v3

    iput-wide v1, v6, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 88
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 89
    invoke-virtual {v7, v0}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    if-eqz v15, :cond_2f

    const-string v1, "mailto"

    .line 90
    invoke-virtual {v15, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const-string v3, "\""

    if-ltz v2, :cond_2a

    .line 91
    invoke-virtual {v15, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v15, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 92
    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_2b

    const/4 v3, 0x0

    .line 93
    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_f

    :cond_2a
    const-string v1, "HYPERLINK"

    .line 94
    invoke-virtual {v15, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_2c

    .line 95
    invoke-virtual {v15, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    add-int/2addr v1, v2

    invoke-virtual {v15, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/"

    .line 96
    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_2b

    const/4 v3, 0x0

    .line 97
    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_10

    :cond_2b
    :goto_f
    const/4 v3, 0x0

    goto :goto_10

    :cond_2c
    const/4 v3, 0x0

    const/4 v1, 0x0

    :goto_10
    if-eqz v1, :cond_2d

    .line 98
    iget-object v2, v6, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    invoke-interface {v2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    move-result-object v2

    invoke-virtual {v2}, Lcom/intsig/office/system/SysKit;->getHyperlinkManage()Lcom/intsig/office/common/hyperlink/HyperlinkManage;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v4}, Lcom/intsig/office/common/hyperlink/HyperlinkManage;->addHyperlink(Ljava/lang/String;I)I

    move-result v1

    if-ltz v1, :cond_2e

    .line 99
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v2

    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v5

    const v9, -0xffff01

    invoke-virtual {v2, v5, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 100
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v2

    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v5

    invoke-virtual {v2, v5, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontUnderline(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 101
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v2

    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v5

    invoke-virtual {v2, v5, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontUnderlineColr(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 102
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v2

    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v5

    invoke-virtual {v2, v5, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setHyperlinkID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_11

    :cond_2d
    const/4 v4, 0x1

    :cond_2e
    :goto_11
    move-object v5, v12

    move-object v15, v5

    goto :goto_12

    :cond_2f
    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v5, v11

    :goto_12
    const/4 v13, 0x1

    goto :goto_13

    :cond_30
    const/4 v3, 0x0

    goto/16 :goto_d

    :cond_31
    move-object v11, v5

    goto/16 :goto_3

    :cond_32
    :goto_13
    move-object/from16 v9, v21

    :goto_14
    const/4 v11, 0x0

    goto/16 :goto_1

    :cond_33
    const-wide/16 v3, 0x1

    const-string v1, "\n"

    if-nez v13, :cond_36

    .line 103
    new-instance v0, Lcom/intsig/office/simpletext/model/LeafElement;

    invoke-direct {v0, v1}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    const-string v1, "pPr"

    move-object/from16 v5, p1

    .line 104
    invoke-interface {v5, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    if-eqz v1, :cond_34

    .line 105
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    :cond_34
    if-eqz v1, :cond_35

    .line 106
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v2

    invoke-direct {v6, v1, v2}, Lcom/intsig/office/fc/doc/DOCXReader;->processRunAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 107
    :cond_35
    iget-wide v1, v6, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 108
    iget-wide v1, v6, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    add-long/2addr v1, v3

    iput-wide v1, v6, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 109
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 110
    invoke-virtual {v7, v0}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    return v13

    :cond_36
    if-eqz p4, :cond_37

    if-eqz v0, :cond_37

    if-nez v14, :cond_37

    .line 111
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v6, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    invoke-virtual {v0, v5}, Lcom/intsig/office/simpletext/model/LeafElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/model/LeafElement;->setText(Ljava/lang/String;)V

    .line 112
    iget-wide v0, v6, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    add-long/2addr v0, v3

    iput-wide v0, v6, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    :cond_37
    return v13
.end method

.method private processRun(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;Z)Z
    .locals 1

    const/4 v0, -0x1

    .line 1
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/intsig/office/fc/doc/DOCXReader;->processRun(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/ParagraphElement;BZ)Z

    move-result p1

    return p1
.end method

.method private processRunAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 6

    .line 1
    const-string v0, "szCs"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "sz"

    .line 8
    .line 9
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const-string v2, "val"

    .line 14
    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    if-eqz v1, :cond_3

    .line 18
    .line 19
    :cond_0
    const/high16 v3, 0x40000000    # 2.0f

    .line 20
    .line 21
    const/high16 v4, 0x41400000    # 12.0f

    .line 22
    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    div-float/2addr v0, v3

    .line 34
    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    .line 35
    .line 36
    .line 37
    move-result v4

    .line 38
    :cond_1
    if-eqz v1, :cond_2

    .line 39
    .line 40
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    div-float/2addr v0, v3

    .line 49
    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    .line 50
    .line 51
    .line 52
    move-result v4

    .line 53
    :cond_2
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    float-to-int v1, v4

    .line 58
    invoke-virtual {v0, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontSize(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 59
    .line 60
    .line 61
    :cond_3
    const-string v0, "rFonts"

    .line 62
    .line 63
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    if-eqz v0, :cond_5

    .line 68
    .line 69
    const-string v1, "hAnsi"

    .line 70
    .line 71
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    if-nez v1, :cond_4

    .line 76
    .line 77
    const-string v1, "eastAsia"

    .line 78
    .line 79
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    :cond_4
    if-eqz v1, :cond_5

    .line 84
    .line 85
    invoke-static {}, Lcom/intsig/office/simpletext/font/FontTypefaceManage;->instance()Lcom/intsig/office/simpletext/font/FontTypefaceManage;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/font/FontTypefaceManage;->addFontName(Ljava/lang/String;)I

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    if-ltz v0, :cond_5

    .line 94
    .line 95
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 96
    .line 97
    .line 98
    move-result-object v1

    .line 99
    invoke-virtual {v1, p2, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontName(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 100
    .line 101
    .line 102
    :cond_5
    const-string v0, "color"

    .line 103
    .line 104
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 105
    .line 106
    .line 107
    move-result-object v1

    .line 108
    const-string v3, "#"

    .line 109
    .line 110
    if-eqz v1, :cond_8

    .line 111
    .line 112
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v1

    .line 116
    const-string v4, "auto"

    .line 117
    .line 118
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 119
    .line 120
    .line 121
    move-result v4

    .line 122
    if-nez v4, :cond_7

    .line 123
    .line 124
    const-string v4, "FFFFFF"

    .line 125
    .line 126
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 127
    .line 128
    .line 129
    move-result v4

    .line 130
    if-eqz v4, :cond_6

    .line 131
    .line 132
    goto :goto_0

    .line 133
    :cond_6
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 134
    .line 135
    .line 136
    move-result-object v4

    .line 137
    new-instance v5, Ljava/lang/StringBuilder;

    .line 138
    .line 139
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 140
    .line 141
    .line 142
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    .line 147
    .line 148
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v1

    .line 152
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 153
    .line 154
    .line 155
    move-result v1

    .line 156
    invoke-virtual {v4, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 157
    .line 158
    .line 159
    goto :goto_1

    .line 160
    :cond_7
    :goto_0
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 161
    .line 162
    .line 163
    move-result-object v1

    .line 164
    const/high16 v4, -0x1000000

    .line 165
    .line 166
    invoke-virtual {v1, p2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 167
    .line 168
    .line 169
    :cond_8
    :goto_1
    const-string v1, "b"

    .line 170
    .line 171
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 172
    .line 173
    .line 174
    move-result-object v1

    .line 175
    const/4 v4, 0x1

    .line 176
    if-eqz v1, :cond_9

    .line 177
    .line 178
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 179
    .line 180
    .line 181
    move-result-object v1

    .line 182
    invoke-virtual {v1, p2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontBold(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 183
    .line 184
    .line 185
    :cond_9
    const-string v1, "i"

    .line 186
    .line 187
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 188
    .line 189
    .line 190
    move-result-object v1

    .line 191
    if-eqz v1, :cond_a

    .line 192
    .line 193
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 194
    .line 195
    .line 196
    move-result-object v1

    .line 197
    invoke-virtual {v1, p2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontItalic(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 198
    .line 199
    .line 200
    :cond_a
    const-string v1, "u"

    .line 201
    .line 202
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 203
    .line 204
    .line 205
    move-result-object v1

    .line 206
    if-eqz v1, :cond_b

    .line 207
    .line 208
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 209
    .line 210
    .line 211
    move-result-object v5

    .line 212
    if-eqz v5, :cond_b

    .line 213
    .line 214
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 215
    .line 216
    .line 217
    move-result-object v5

    .line 218
    invoke-virtual {v5, p2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontUnderline(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 219
    .line 220
    .line 221
    invoke-interface {v1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 222
    .line 223
    .line 224
    move-result-object v0

    .line 225
    if-eqz v0, :cond_b

    .line 226
    .line 227
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 228
    .line 229
    .line 230
    move-result v1

    .line 231
    if-lez v1, :cond_b

    .line 232
    .line 233
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 234
    .line 235
    .line 236
    move-result-object v1

    .line 237
    new-instance v5, Ljava/lang/StringBuilder;

    .line 238
    .line 239
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 240
    .line 241
    .line 242
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    .line 244
    .line 245
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    .line 247
    .line 248
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 249
    .line 250
    .line 251
    move-result-object v0

    .line 252
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 253
    .line 254
    .line 255
    move-result v0

    .line 256
    invoke-virtual {v1, p2, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontUnderlineColr(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 257
    .line 258
    .line 259
    :cond_b
    const-string v0, "strike"

    .line 260
    .line 261
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 262
    .line 263
    .line 264
    move-result-object v0

    .line 265
    const-string v1, "0"

    .line 266
    .line 267
    if-eqz v0, :cond_c

    .line 268
    .line 269
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 270
    .line 271
    .line 272
    move-result-object v3

    .line 273
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 274
    .line 275
    .line 276
    move-result-object v0

    .line 277
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 278
    .line 279
    .line 280
    move-result v0

    .line 281
    xor-int/2addr v0, v4

    .line 282
    invoke-virtual {v3, p2, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontStrike(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 283
    .line 284
    .line 285
    :cond_c
    const-string v0, "dstrike"

    .line 286
    .line 287
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 288
    .line 289
    .line 290
    move-result-object v0

    .line 291
    if-eqz v0, :cond_d

    .line 292
    .line 293
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 294
    .line 295
    .line 296
    move-result-object v3

    .line 297
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 298
    .line 299
    .line 300
    move-result-object v0

    .line 301
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 302
    .line 303
    .line 304
    move-result v0

    .line 305
    xor-int/2addr v0, v4

    .line 306
    invoke-virtual {v3, p2, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontDoubleStrike(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 307
    .line 308
    .line 309
    :cond_d
    const-string v0, "vertAlign"

    .line 310
    .line 311
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 312
    .line 313
    .line 314
    move-result-object v0

    .line 315
    if-eqz v0, :cond_f

    .line 316
    .line 317
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 318
    .line 319
    .line 320
    move-result-object v0

    .line 321
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 322
    .line 323
    .line 324
    move-result-object v1

    .line 325
    const-string v3, "superscript"

    .line 326
    .line 327
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 328
    .line 329
    .line 330
    move-result v0

    .line 331
    if-eqz v0, :cond_e

    .line 332
    .line 333
    goto :goto_2

    .line 334
    :cond_e
    const/4 v4, 0x2

    .line 335
    :goto_2
    invoke-virtual {v1, p2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontScript(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 336
    .line 337
    .line 338
    :cond_f
    const-string v0, "rStyle"

    .line 339
    .line 340
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 341
    .line 342
    .line 343
    move-result-object v0

    .line 344
    if-eqz v0, :cond_10

    .line 345
    .line 346
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 347
    .line 348
    .line 349
    move-result-object v0

    .line 350
    if-eqz v0, :cond_10

    .line 351
    .line 352
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 353
    .line 354
    .line 355
    move-result v1

    .line 356
    if-lez v1, :cond_10

    .line 357
    .line 358
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 359
    .line 360
    .line 361
    move-result-object v1

    .line 362
    iget-object v3, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleStrID:Ljava/util/Map;

    .line 363
    .line 364
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    .line 366
    .line 367
    move-result-object v0

    .line 368
    check-cast v0, Ljava/lang/Integer;

    .line 369
    .line 370
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 371
    .line 372
    .line 373
    move-result v0

    .line 374
    invoke-virtual {v1, p2, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaStyleID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 375
    .line 376
    .line 377
    :cond_10
    const-string v0, "highlight"

    .line 378
    .line 379
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 380
    .line 381
    .line 382
    move-result-object p1

    .line 383
    if-eqz p1, :cond_11

    .line 384
    .line 385
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 386
    .line 387
    .line 388
    move-result-object v0

    .line 389
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 390
    .line 391
    .line 392
    move-result-object p1

    .line 393
    invoke-static {p1}, Lcom/intsig/office/fc/FCKit;->convertColor(Ljava/lang/String;)I

    .line 394
    .line 395
    .line 396
    move-result p1

    .line 397
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontHighLight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 398
    .line 399
    .line 400
    :cond_11
    return-void
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method private processSection(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->secElem:Lcom/intsig/office/simpletext/model/SectionElement;

    .line 2
    .line 3
    const-wide/16 v1, 0x0

    .line 4
    .line 5
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->secElem:Lcom/intsig/office/simpletext/model/SectionElement;

    .line 9
    .line 10
    iget-wide v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 11
    .line 12
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->secElem:Lcom/intsig/office/simpletext/model/SectionElement;

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/office/wp/model/WPDocument;->appendSection(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 20
    .line 21
    .line 22
    const-string v0, "sectPr"

    .line 23
    .line 24
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/doc/DOCXReader;->processSectionAttribute(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private processSectionAttribute(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 11

    .line 1
    if-eqz p1, :cond_14

    .line 2
    .line 3
    iget-boolean v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessSectionAttribute:Z

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    goto/16 :goto_5

    .line 8
    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->secElem:Lcom/intsig/office/simpletext/model/SectionElement;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "pgSz"

    .line 16
    .line 17
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    if-eqz v1, :cond_1

    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    const-string v3, "w"

    .line 28
    .line 29
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v3

    .line 33
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 34
    .line 35
    .line 36
    move-result v3

    .line 37
    invoke-virtual {v2, v0, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 38
    .line 39
    .line 40
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 41
    .line 42
    .line 43
    move-result-object v2

    .line 44
    const-string v3, "h"

    .line 45
    .line 46
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    invoke-virtual {v2, v0, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 55
    .line 56
    .line 57
    :cond_1
    const-string v1, "pgMar"

    .line 58
    .line 59
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    const-string v2, "bottom"

    .line 64
    .line 65
    const-string v3, "top"

    .line 66
    .line 67
    const-string v4, "right"

    .line 68
    .line 69
    const-string v5, "left"

    .line 70
    .line 71
    if-eqz v1, :cond_3

    .line 72
    .line 73
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 74
    .line 75
    .line 76
    move-result-object v6

    .line 77
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v7

    .line 81
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 82
    .line 83
    .line 84
    move-result v7

    .line 85
    invoke-virtual {v6, v0, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 86
    .line 87
    .line 88
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 89
    .line 90
    .line 91
    move-result-object v6

    .line 92
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v7

    .line 96
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 97
    .line 98
    .line 99
    move-result v7

    .line 100
    invoke-virtual {v6, v0, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 101
    .line 102
    .line 103
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 104
    .line 105
    .line 106
    move-result-object v6

    .line 107
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v7

    .line 111
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 112
    .line 113
    .line 114
    move-result v7

    .line 115
    invoke-virtual {v6, v0, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 116
    .line 117
    .line 118
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 119
    .line 120
    .line 121
    move-result-object v6

    .line 122
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v7

    .line 126
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 127
    .line 128
    .line 129
    move-result v7

    .line 130
    invoke-virtual {v6, v0, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 131
    .line 132
    .line 133
    const-string v6, "header"

    .line 134
    .line 135
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v7

    .line 139
    if-eqz v7, :cond_2

    .line 140
    .line 141
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 142
    .line 143
    .line 144
    move-result-object v7

    .line 145
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v6

    .line 149
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 150
    .line 151
    .line 152
    move-result v6

    .line 153
    invoke-virtual {v7, v0, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeaderMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 154
    .line 155
    .line 156
    :cond_2
    const-string v6, "footer"

    .line 157
    .line 158
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 159
    .line 160
    .line 161
    move-result-object v7

    .line 162
    if-eqz v7, :cond_3

    .line 163
    .line 164
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 165
    .line 166
    .line 167
    move-result-object v7

    .line 168
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object v1

    .line 172
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 173
    .line 174
    .line 175
    move-result v1

    .line 176
    invoke-virtual {v7, v0, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageFooterMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 177
    .line 178
    .line 179
    :cond_3
    const-string v1, "pgBorders"

    .line 180
    .line 181
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 182
    .line 183
    .line 184
    move-result-object v1

    .line 185
    const/4 v6, 0x1

    .line 186
    if-eqz v1, :cond_9

    .line 187
    .line 188
    new-instance v7, Lcom/intsig/office/common/borders/Borders;

    .line 189
    .line 190
    invoke-direct {v7}, Lcom/intsig/office/common/borders/Borders;-><init>()V

    .line 191
    .line 192
    .line 193
    const-string v8, "offsetFrom"

    .line 194
    .line 195
    invoke-interface {v1, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 196
    .line 197
    .line 198
    move-result-object v8

    .line 199
    const-string v9, "page"

    .line 200
    .line 201
    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 202
    .line 203
    .line 204
    move-result v8

    .line 205
    if-eqz v8, :cond_4

    .line 206
    .line 207
    invoke-virtual {v7, v6}, Lcom/intsig/office/common/borders/Borders;->setOnType(B)V

    .line 208
    .line 209
    .line 210
    :cond_4
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 211
    .line 212
    .line 213
    move-result-object v3

    .line 214
    if-eqz v3, :cond_5

    .line 215
    .line 216
    new-instance v8, Lcom/intsig/office/common/borders/Border;

    .line 217
    .line 218
    invoke-direct {v8}, Lcom/intsig/office/common/borders/Border;-><init>()V

    .line 219
    .line 220
    .line 221
    invoke-direct {p0, v3, v8}, Lcom/intsig/office/fc/doc/DOCXReader;->processBorder(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/borders/Border;)V

    .line 222
    .line 223
    .line 224
    invoke-virtual {v7, v8}, Lcom/intsig/office/common/borders/Borders;->setTopBorder(Lcom/intsig/office/common/borders/Border;)V

    .line 225
    .line 226
    .line 227
    :cond_5
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 228
    .line 229
    .line 230
    move-result-object v3

    .line 231
    if-eqz v3, :cond_6

    .line 232
    .line 233
    new-instance v5, Lcom/intsig/office/common/borders/Border;

    .line 234
    .line 235
    invoke-direct {v5}, Lcom/intsig/office/common/borders/Border;-><init>()V

    .line 236
    .line 237
    .line 238
    invoke-direct {p0, v3, v5}, Lcom/intsig/office/fc/doc/DOCXReader;->processBorder(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/borders/Border;)V

    .line 239
    .line 240
    .line 241
    invoke-virtual {v7, v5}, Lcom/intsig/office/common/borders/Borders;->setLeftBorder(Lcom/intsig/office/common/borders/Border;)V

    .line 242
    .line 243
    .line 244
    :cond_6
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 245
    .line 246
    .line 247
    move-result-object v3

    .line 248
    if-eqz v3, :cond_7

    .line 249
    .line 250
    new-instance v4, Lcom/intsig/office/common/borders/Border;

    .line 251
    .line 252
    invoke-direct {v4}, Lcom/intsig/office/common/borders/Border;-><init>()V

    .line 253
    .line 254
    .line 255
    invoke-direct {p0, v3, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->processBorder(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/borders/Border;)V

    .line 256
    .line 257
    .line 258
    invoke-virtual {v7, v4}, Lcom/intsig/office/common/borders/Borders;->setRightBorder(Lcom/intsig/office/common/borders/Border;)V

    .line 259
    .line 260
    .line 261
    :cond_7
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 262
    .line 263
    .line 264
    move-result-object v1

    .line 265
    if-eqz v1, :cond_8

    .line 266
    .line 267
    new-instance v2, Lcom/intsig/office/common/borders/Border;

    .line 268
    .line 269
    invoke-direct {v2}, Lcom/intsig/office/common/borders/Border;-><init>()V

    .line 270
    .line 271
    .line 272
    invoke-direct {p0, v1, v2}, Lcom/intsig/office/fc/doc/DOCXReader;->processBorder(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/borders/Border;)V

    .line 273
    .line 274
    .line 275
    invoke-virtual {v7, v2}, Lcom/intsig/office/common/borders/Borders;->setBottomBorder(Lcom/intsig/office/common/borders/Border;)V

    .line 276
    .line 277
    .line 278
    :cond_8
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 279
    .line 280
    .line 281
    move-result-object v1

    .line 282
    iget-object v2, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 283
    .line 284
    invoke-interface {v2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 285
    .line 286
    .line 287
    move-result-object v2

    .line 288
    invoke-virtual {v2}, Lcom/intsig/office/system/SysKit;->getBordersManage()Lcom/intsig/office/common/borders/BordersManage;

    .line 289
    .line 290
    .line 291
    move-result-object v2

    .line 292
    invoke-virtual {v2, v7}, Lcom/intsig/office/common/borders/BordersManage;->addBorders(Lcom/intsig/office/common/borders/Borders;)I

    .line 293
    .line 294
    .line 295
    move-result v2

    .line 296
    invoke-virtual {v1, v0, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageBorder(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 297
    .line 298
    .line 299
    :cond_9
    const-string v1, "docGrid"

    .line 300
    .line 301
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 302
    .line 303
    .line 304
    move-result-object v1

    .line 305
    const-string v2, "type"

    .line 306
    .line 307
    const/4 v3, 0x0

    .line 308
    if-eqz v1, :cond_b

    .line 309
    .line 310
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 311
    .line 312
    .line 313
    move-result-object v4

    .line 314
    const-string v5, "lines"

    .line 315
    .line 316
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 317
    .line 318
    .line 319
    move-result v5

    .line 320
    if-nez v5, :cond_a

    .line 321
    .line 322
    const-string v5, "linesAndChars"

    .line 323
    .line 324
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 325
    .line 326
    .line 327
    move-result v5

    .line 328
    if-nez v5, :cond_a

    .line 329
    .line 330
    const-string v5, "snapToChars"

    .line 331
    .line 332
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 333
    .line 334
    .line 335
    move-result v4

    .line 336
    if-eqz v4, :cond_b

    .line 337
    .line 338
    :cond_a
    const-string v4, "linePitch"

    .line 339
    .line 340
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 341
    .line 342
    .line 343
    move-result-object v1

    .line 344
    if-eqz v1, :cond_b

    .line 345
    .line 346
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    .line 347
    .line 348
    .line 349
    move-result v4

    .line 350
    if-lez v4, :cond_b

    .line 351
    .line 352
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 353
    .line 354
    .line 355
    move-result-object v4

    .line 356
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 357
    .line 358
    .line 359
    move-result v1

    .line 360
    invoke-virtual {v4, v0, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageLinePitch(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 361
    .line 362
    .line 363
    const/4 v0, 0x0

    .line 364
    :goto_0
    int-to-long v4, v0

    .line 365
    iget-wide v7, p0, Lcom/intsig/office/fc/doc/DOCXReader;->textboxIndex:J

    .line 366
    .line 367
    cmp-long v1, v4, v7

    .line 368
    .line 369
    if-gez v1, :cond_b

    .line 370
    .line 371
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 372
    .line 373
    invoke-virtual {v1, v0}, Lcom/intsig/office/wp/model/WPDocument;->getTextboxSectionElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 374
    .line 375
    .line 376
    move-result-object v1

    .line 377
    iget-object v4, p0, Lcom/intsig/office/fc/doc/DOCXReader;->secElem:Lcom/intsig/office/simpletext/model/SectionElement;

    .line 378
    .line 379
    invoke-virtual {v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 380
    .line 381
    .line 382
    move-result-object v4

    .line 383
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 384
    .line 385
    .line 386
    move-result-object v5

    .line 387
    invoke-interface {v1}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 388
    .line 389
    .line 390
    move-result-object v1

    .line 391
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 392
    .line 393
    .line 394
    move-result-object v7

    .line 395
    invoke-virtual {v7, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageLinePitch(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 396
    .line 397
    .line 398
    move-result v4

    .line 399
    invoke-virtual {v5, v1, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageLinePitch(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 400
    .line 401
    .line 402
    add-int/lit8 v0, v0, 0x1

    .line 403
    .line 404
    goto :goto_0

    .line 405
    :cond_b
    iget-wide v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 406
    .line 407
    const-string v4, "headerReference"

    .line 408
    .line 409
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 410
    .line 411
    .line 412
    move-result-object v4

    .line 413
    const-string v5, "default"

    .line 414
    .line 415
    const-string v7, ""

    .line 416
    .line 417
    const-string v8, "id"

    .line 418
    .line 419
    if-eqz v4, :cond_f

    .line 420
    .line 421
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 422
    .line 423
    .line 424
    move-result v9

    .line 425
    if-lez v9, :cond_f

    .line 426
    .line 427
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 428
    .line 429
    .line 430
    move-result v9

    .line 431
    if-ne v9, v6, :cond_c

    .line 432
    .line 433
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 434
    .line 435
    .line 436
    move-result-object v4

    .line 437
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 438
    .line 439
    invoke-interface {v4, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 440
    .line 441
    .line 442
    move-result-object v4

    .line 443
    goto :goto_1

    .line 444
    :cond_c
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 445
    .line 446
    .line 447
    move-result-object v4

    .line 448
    :cond_d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 449
    .line 450
    .line 451
    move-result v9

    .line 452
    if-eqz v9, :cond_e

    .line 453
    .line 454
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 455
    .line 456
    .line 457
    move-result-object v9

    .line 458
    check-cast v9, Lcom/intsig/office/fc/dom4j/Element;

    .line 459
    .line 460
    invoke-interface {v9, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 461
    .line 462
    .line 463
    move-result-object v10

    .line 464
    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 465
    .line 466
    .line 467
    move-result v10

    .line 468
    if-eqz v10, :cond_d

    .line 469
    .line 470
    invoke-interface {v9, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 471
    .line 472
    .line 473
    move-result-object v4

    .line 474
    goto :goto_1

    .line 475
    :cond_e
    move-object v4, v7

    .line 476
    :goto_1
    if-eqz v4, :cond_f

    .line 477
    .line 478
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 479
    .line 480
    .line 481
    move-result v9

    .line 482
    if-lez v9, :cond_f

    .line 483
    .line 484
    :try_start_0
    iget-object v9, p0, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 485
    .line 486
    const-string v10, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/header"

    .line 487
    .line 488
    invoke-virtual {v9, v10}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 489
    .line 490
    .line 491
    move-result-object v9

    .line 492
    invoke-virtual {v9, v4}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationshipByID(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 493
    .line 494
    .line 495
    move-result-object v4

    .line 496
    if-eqz v4, :cond_f

    .line 497
    .line 498
    invoke-direct {p0, v4, v6}, Lcom/intsig/office/fc/doc/DOCXReader;->processHeaderAndFooter(Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 499
    .line 500
    .line 501
    goto :goto_2

    .line 502
    :catch_0
    move-exception v4

    .line 503
    iget-object v9, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 504
    .line 505
    invoke-interface {v9}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 506
    .line 507
    .line 508
    move-result-object v9

    .line 509
    invoke-virtual {v9}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    .line 510
    .line 511
    .line 512
    move-result-object v9

    .line 513
    invoke-virtual {v9, v4, v6}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;Z)V

    .line 514
    .line 515
    .line 516
    :cond_f
    :goto_2
    const-string v4, "footerReference"

    .line 517
    .line 518
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 519
    .line 520
    .line 521
    move-result-object p1

    .line 522
    if-eqz p1, :cond_13

    .line 523
    .line 524
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 525
    .line 526
    .line 527
    move-result v4

    .line 528
    if-lez v4, :cond_13

    .line 529
    .line 530
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 531
    .line 532
    .line 533
    move-result v4

    .line 534
    if-ne v4, v6, :cond_10

    .line 535
    .line 536
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 537
    .line 538
    .line 539
    move-result-object p1

    .line 540
    check-cast p1, Lcom/intsig/office/fc/dom4j/Element;

    .line 541
    .line 542
    invoke-interface {p1, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 543
    .line 544
    .line 545
    move-result-object v7

    .line 546
    goto :goto_3

    .line 547
    :cond_10
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 548
    .line 549
    .line 550
    move-result-object p1

    .line 551
    :cond_11
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 552
    .line 553
    .line 554
    move-result v4

    .line 555
    if-eqz v4, :cond_12

    .line 556
    .line 557
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 558
    .line 559
    .line 560
    move-result-object v4

    .line 561
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 562
    .line 563
    invoke-interface {v4, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 564
    .line 565
    .line 566
    move-result-object v9

    .line 567
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 568
    .line 569
    .line 570
    move-result v9

    .line 571
    if-eqz v9, :cond_11

    .line 572
    .line 573
    invoke-interface {v4, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 574
    .line 575
    .line 576
    move-result-object v7

    .line 577
    :cond_12
    :goto_3
    if-eqz v7, :cond_13

    .line 578
    .line 579
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    .line 580
    .line 581
    .line 582
    move-result p1

    .line 583
    if-lez p1, :cond_13

    .line 584
    .line 585
    :try_start_1
    iget-object p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 586
    .line 587
    const-string v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/footer"

    .line 588
    .line 589
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 590
    .line 591
    .line 592
    move-result-object p1

    .line 593
    invoke-virtual {p1, v7}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationshipByID(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 594
    .line 595
    .line 596
    move-result-object p1

    .line 597
    if-eqz p1, :cond_13

    .line 598
    .line 599
    invoke-direct {p0, p1, v3}, Lcom/intsig/office/fc/doc/DOCXReader;->processHeaderAndFooter(Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 600
    .line 601
    .line 602
    goto :goto_4

    .line 603
    :catch_1
    move-exception p1

    .line 604
    iget-object v2, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 605
    .line 606
    invoke-interface {v2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 607
    .line 608
    .line 609
    move-result-object v2

    .line 610
    invoke-virtual {v2}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    .line 611
    .line 612
    .line 613
    move-result-object v2

    .line 614
    invoke-virtual {v2, p1, v6}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;Z)V

    .line 615
    .line 616
    .line 617
    :cond_13
    :goto_4
    iput-wide v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 618
    .line 619
    iput-boolean v6, p0, Lcom/intsig/office/fc/doc/DOCXReader;->isProcessSectionAttribute:Z

    .line 620
    .line 621
    :cond_14
    :goto_5
    return-void
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method private processSmart(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;Z)V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    move-object v10, p0

    .line 2
    move-object/from16 v0, p1

    .line 3
    .line 4
    move-object/from16 v1, p2

    .line 5
    .line 6
    move-object/from16 v2, p3

    .line 7
    .line 8
    move-object/from16 v3, p5

    .line 9
    .line 10
    move-object/from16 v4, p6

    .line 11
    .line 12
    if-eqz v2, :cond_5

    .line 13
    .line 14
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 15
    .line 16
    .line 17
    move-result-object v5

    .line 18
    if-eqz v5, :cond_5

    .line 19
    .line 20
    new-instance v6, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 21
    .line 22
    invoke-direct {v6}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v6, v5}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 26
    .line 27
    .line 28
    move-result-object v7

    .line 29
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 30
    .line 31
    .line 32
    invoke-interface {v7}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 33
    .line 34
    .line 35
    move-result-object v5

    .line 36
    const-string v7, "bg"

    .line 37
    .line 38
    invoke-interface {v5, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 39
    .line 40
    .line 41
    move-result-object v7

    .line 42
    iget-object v8, v10, Lcom/intsig/office/fc/doc/DOCXReader;->themeColor:Ljava/util/Map;

    .line 43
    .line 44
    invoke-static {v0, v1, v2, v7, v8}, Lcom/intsig/office/common/autoshape/AutoShapeDataKit;->processBackground(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 45
    .line 46
    .line 47
    move-result-object v7

    .line 48
    const-string v8, "whole"

    .line 49
    .line 50
    invoke-interface {v5, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 51
    .line 52
    .line 53
    move-result-object v8

    .line 54
    const-string v9, "ln"

    .line 55
    .line 56
    invoke-interface {v8, v9}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 57
    .line 58
    .line 59
    move-result-object v8

    .line 60
    iget-object v9, v10, Lcom/intsig/office/fc/doc/DOCXReader;->themeColor:Ljava/util/Map;

    .line 61
    .line 62
    invoke-static {v0, v1, v2, v8, v9}, Lcom/intsig/office/fc/LineKit;->createLine(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;)Lcom/intsig/office/common/borders/Line;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    const-string v2, "extLst"

    .line 67
    .line 68
    invoke-interface {v5, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 69
    .line 70
    .line 71
    move-result-object v2

    .line 72
    if-eqz v2, :cond_0

    .line 73
    .line 74
    const-string v5, "ext"

    .line 75
    .line 76
    invoke-interface {v2, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    if-eqz v2, :cond_0

    .line 81
    .line 82
    const-string v5, "dataModelExt"

    .line 83
    .line 84
    invoke-interface {v2, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 85
    .line 86
    .line 87
    move-result-object v2

    .line 88
    if-eqz v2, :cond_0

    .line 89
    .line 90
    const-string v5, "relId"

    .line 91
    .line 92
    invoke-interface {v2, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v2

    .line 96
    if-eqz v2, :cond_0

    .line 97
    .line 98
    iget-object v5, v10, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 99
    .line 100
    invoke-virtual {v5, v2}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationship(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 101
    .line 102
    .line 103
    move-result-object v2

    .line 104
    invoke-virtual {v2}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 105
    .line 106
    .line 107
    move-result-object v2

    .line 108
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    goto :goto_0

    .line 113
    :cond_0
    const/4 v1, 0x0

    .line 114
    :goto_0
    move-object v11, v1

    .line 115
    if-nez v11, :cond_1

    .line 116
    .line 117
    return-void

    .line 118
    :cond_1
    invoke-virtual {v11}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 119
    .line 120
    .line 121
    move-result-object v1

    .line 122
    invoke-virtual {v6, v1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 123
    .line 124
    .line 125
    move-result-object v2

    .line 126
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 127
    .line 128
    .line 129
    if-eqz v2, :cond_5

    .line 130
    .line 131
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 132
    .line 133
    .line 134
    move-result-object v1

    .line 135
    if-eqz v1, :cond_5

    .line 136
    .line 137
    const-string v2, "spTree"

    .line 138
    .line 139
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 140
    .line 141
    .line 142
    move-result-object v1

    .line 143
    if-eqz v1, :cond_5

    .line 144
    .line 145
    new-instance v12, Lcom/intsig/office/common/shape/WPGroupShape;

    .line 146
    .line 147
    invoke-direct {v12}, Lcom/intsig/office/common/shape/WPGroupShape;-><init>()V

    .line 148
    .line 149
    .line 150
    new-instance v13, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 151
    .line 152
    invoke-direct {v13}, Lcom/intsig/office/common/shape/WPAutoShape;-><init>()V

    .line 153
    .line 154
    .line 155
    invoke-virtual {v13, v12}, Lcom/intsig/office/common/shape/WPAutoShape;->addGroupShape(Lcom/intsig/office/common/shape/WPGroupShape;)V

    .line 156
    .line 157
    .line 158
    const/4 v2, 0x2

    .line 159
    if-nez p7, :cond_2

    .line 160
    .line 161
    invoke-direct {p0, v13, v3, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->processWrapAndPosition_Drawing(Lcom/intsig/office/common/shape/WPAbstractShape;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)V

    .line 162
    .line 163
    .line 164
    invoke-direct {p0, v3}, Lcom/intsig/office/fc/doc/DOCXReader;->getDrawingWrapType(Lcom/intsig/office/fc/dom4j/Element;)S

    .line 165
    .line 166
    .line 167
    move-result v3

    .line 168
    goto :goto_1

    .line 169
    :cond_2
    const/4 v3, 0x2

    .line 170
    :goto_1
    invoke-virtual {v12, v4}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 171
    .line 172
    .line 173
    invoke-virtual {v13, v7}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 174
    .line 175
    .line 176
    invoke-virtual {v13, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 177
    .line 178
    .line 179
    const/4 v0, 0x1

    .line 180
    invoke-virtual {v13, v0}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 181
    .line 182
    .line 183
    if-eq v3, v2, :cond_3

    .line 184
    .line 185
    invoke-virtual {v12, v3}, Lcom/intsig/office/common/shape/WPGroupShape;->setWrapType(S)V

    .line 186
    .line 187
    .line 188
    invoke-virtual {v13, v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 189
    .line 190
    .line 191
    goto :goto_2

    .line 192
    :cond_3
    invoke-virtual {v12, v2}, Lcom/intsig/office/common/shape/WPGroupShape;->setWrapType(S)V

    .line 193
    .line 194
    .line 195
    invoke-virtual {v13, v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 196
    .line 197
    .line 198
    :goto_2
    invoke-virtual {v13, v4}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 199
    .line 200
    .line 201
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->elementIterator()Ljava/util/Iterator;

    .line 202
    .line 203
    .line 204
    move-result-object v14

    .line 205
    :goto_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    .line 206
    .line 207
    .line 208
    move-result v0

    .line 209
    if-eqz v0, :cond_4

    .line 210
    .line 211
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 212
    .line 213
    .line 214
    move-result-object v0

    .line 215
    move-object v3, v0

    .line 216
    check-cast v3, Lcom/intsig/office/fc/dom4j/Element;

    .line 217
    .line 218
    const/high16 v5, 0x3f800000    # 1.0f

    .line 219
    .line 220
    const/high16 v6, 0x3f800000    # 1.0f

    .line 221
    .line 222
    const/4 v7, 0x0

    .line 223
    const/4 v8, 0x0

    .line 224
    const/4 v9, 0x0

    .line 225
    move-object v0, p0

    .line 226
    move-object v1, v11

    .line 227
    move-object/from16 v2, p4

    .line 228
    .line 229
    move-object v4, v12

    .line 230
    invoke-direct/range {v0 .. v9}, Lcom/intsig/office/fc/doc/DOCXReader;->processAutoShape2010(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/WPGroupShape;FFIIZ)Lcom/intsig/office/common/shape/AbstractShape;

    .line 231
    .line 232
    .line 233
    goto :goto_3

    .line 234
    :cond_4
    move-object/from16 v0, p4

    .line 235
    .line 236
    invoke-direct {p0, v13, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->addShape(Lcom/intsig/office/common/shape/AbstractShape;Lcom/intsig/office/simpletext/model/ParagraphElement;)V

    .line 237
    .line 238
    .line 239
    :cond_5
    return-void
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
.end method

.method private processStyle()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 2
    .line 3
    const-string v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationship(I)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_e

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    if-eqz v0, :cond_e

    .line 27
    .line 28
    new-instance v2, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 29
    .line 30
    invoke-direct {v2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    const-string v3, "style"

    .line 46
    .line 47
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 48
    .line 49
    .line 50
    move-result-object v3

    .line 51
    const-string v4, "docDefaults"

    .line 52
    .line 53
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    const-string v5, "rPr"

    .line 58
    .line 59
    const-string v6, "pPr"

    .line 60
    .line 61
    if-eqz v2, :cond_2

    .line 62
    .line 63
    new-instance v7, Lcom/intsig/office/simpletext/model/Style;

    .line 64
    .line 65
    invoke-direct {v7}, Lcom/intsig/office/simpletext/model/Style;-><init>()V

    .line 66
    .line 67
    .line 68
    iget-object v8, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleStrID:Ljava/util/Map;

    .line 69
    .line 70
    iget v9, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleID:I

    .line 71
    .line 72
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 73
    .line 74
    .line 75
    move-result-object v9

    .line 76
    invoke-interface {v8, v4, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    .line 78
    .line 79
    iget v8, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleID:I

    .line 80
    .line 81
    invoke-virtual {v7, v8}, Lcom/intsig/office/simpletext/model/Style;->setId(I)V

    .line 82
    .line 83
    .line 84
    iget v8, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleID:I

    .line 85
    .line 86
    add-int/lit8 v8, v8, 0x1

    .line 87
    .line 88
    iput v8, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleID:I

    .line 89
    .line 90
    invoke-virtual {v7, v1}, Lcom/intsig/office/simpletext/model/Style;->setType(B)V

    .line 91
    .line 92
    .line 93
    invoke-virtual {v7, v4}, Lcom/intsig/office/simpletext/model/Style;->setName(Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    const-string v4, "pPrDefault"

    .line 97
    .line 98
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 99
    .line 100
    .line 101
    move-result-object v4

    .line 102
    if-eqz v4, :cond_0

    .line 103
    .line 104
    invoke-interface {v4, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 105
    .line 106
    .line 107
    move-result-object v4

    .line 108
    if-eqz v4, :cond_0

    .line 109
    .line 110
    invoke-virtual {v7}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 111
    .line 112
    .line 113
    move-result-object v8

    .line 114
    invoke-direct {p0, v4, v8, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->processParaAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 115
    .line 116
    .line 117
    :cond_0
    const-string v4, "rPrDefault"

    .line 118
    .line 119
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 120
    .line 121
    .line 122
    move-result-object v2

    .line 123
    if-eqz v2, :cond_1

    .line 124
    .line 125
    invoke-interface {v2, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 126
    .line 127
    .line 128
    move-result-object v2

    .line 129
    if-eqz v2, :cond_1

    .line 130
    .line 131
    invoke-virtual {v7}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 132
    .line 133
    .line 134
    move-result-object v4

    .line 135
    invoke-direct {p0, v2, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->processRunAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 136
    .line 137
    .line 138
    :cond_1
    invoke-static {}, Lcom/intsig/office/simpletext/model/StyleManage;->instance()Lcom/intsig/office/simpletext/model/StyleManage;

    .line 139
    .line 140
    .line 141
    move-result-object v2

    .line 142
    invoke-virtual {v2, v7}, Lcom/intsig/office/simpletext/model/StyleManage;->addStyle(Lcom/intsig/office/simpletext/model/Style;)V

    .line 143
    .line 144
    .line 145
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 146
    .line 147
    .line 148
    move-result-object v2

    .line 149
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 150
    .line 151
    .line 152
    move-result v3

    .line 153
    if-eqz v3, :cond_d

    .line 154
    .line 155
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 156
    .line 157
    .line 158
    move-result-object v3

    .line 159
    check-cast v3, Lcom/intsig/office/fc/dom4j/Element;

    .line 160
    .line 161
    iget-boolean v4, p0, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 162
    .line 163
    if-eqz v4, :cond_3

    .line 164
    .line 165
    goto/16 :goto_3

    .line 166
    .line 167
    :cond_3
    const-string v4, "type"

    .line 168
    .line 169
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 170
    .line 171
    .line 172
    move-result-object v7

    .line 173
    const-string v8, "table"

    .line 174
    .line 175
    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 176
    .line 177
    .line 178
    move-result v7

    .line 179
    const-string v8, "styleId"

    .line 180
    .line 181
    if-eqz v7, :cond_4

    .line 182
    .line 183
    const-string v7, "tblStylePr"

    .line 184
    .line 185
    invoke-interface {v3, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 186
    .line 187
    .line 188
    move-result-object v7

    .line 189
    if-eqz v7, :cond_4

    .line 190
    .line 191
    const-string v9, "firstRow"

    .line 192
    .line 193
    invoke-interface {v7, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 194
    .line 195
    .line 196
    move-result-object v10

    .line 197
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 198
    .line 199
    .line 200
    move-result v9

    .line 201
    if-eqz v9, :cond_4

    .line 202
    .line 203
    const-string v9, "tcPr"

    .line 204
    .line 205
    invoke-interface {v7, v9}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 206
    .line 207
    .line 208
    move-result-object v7

    .line 209
    if-eqz v7, :cond_4

    .line 210
    .line 211
    const-string v9, "shd"

    .line 212
    .line 213
    invoke-interface {v7, v9}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 214
    .line 215
    .line 216
    move-result-object v7

    .line 217
    if-eqz v7, :cond_4

    .line 218
    .line 219
    const-string v9, "fill"

    .line 220
    .line 221
    invoke-interface {v7, v9}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 222
    .line 223
    .line 224
    move-result-object v7

    .line 225
    if-eqz v7, :cond_4

    .line 226
    .line 227
    iget-object v9, p0, Lcom/intsig/office/fc/doc/DOCXReader;->tableStyle:Ljava/util/Map;

    .line 228
    .line 229
    invoke-interface {v3, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 230
    .line 231
    .line 232
    move-result-object v10

    .line 233
    new-instance v11, Ljava/lang/StringBuilder;

    .line 234
    .line 235
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 236
    .line 237
    .line 238
    const-string v12, "#"

    .line 239
    .line 240
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    .line 242
    .line 243
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    .line 245
    .line 246
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 247
    .line 248
    .line 249
    move-result-object v7

    .line 250
    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 251
    .line 252
    .line 253
    move-result v7

    .line 254
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 255
    .line 256
    .line 257
    move-result-object v7

    .line 258
    invoke-interface {v9, v10, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    .line 260
    .line 261
    :cond_4
    new-instance v7, Lcom/intsig/office/simpletext/model/Style;

    .line 262
    .line 263
    invoke-direct {v7}, Lcom/intsig/office/simpletext/model/Style;-><init>()V

    .line 264
    .line 265
    .line 266
    invoke-interface {v3, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 267
    .line 268
    .line 269
    move-result-object v8

    .line 270
    if-eqz v8, :cond_6

    .line 271
    .line 272
    iget-object v9, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleStrID:Ljava/util/Map;

    .line 273
    .line 274
    invoke-interface {v9, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    .line 276
    .line 277
    move-result-object v9

    .line 278
    check-cast v9, Ljava/lang/Integer;

    .line 279
    .line 280
    if-nez v9, :cond_5

    .line 281
    .line 282
    iget-object v9, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleStrID:Ljava/util/Map;

    .line 283
    .line 284
    iget v10, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleID:I

    .line 285
    .line 286
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 287
    .line 288
    .line 289
    move-result-object v10

    .line 290
    invoke-interface {v9, v8, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    .line 292
    .line 293
    iget v8, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleID:I

    .line 294
    .line 295
    invoke-virtual {v7, v8}, Lcom/intsig/office/simpletext/model/Style;->setId(I)V

    .line 296
    .line 297
    .line 298
    iget v8, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleID:I

    .line 299
    .line 300
    add-int/lit8 v8, v8, 0x1

    .line 301
    .line 302
    iput v8, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleID:I

    .line 303
    .line 304
    goto :goto_1

    .line 305
    :cond_5
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    .line 306
    .line 307
    .line 308
    move-result v8

    .line 309
    invoke-virtual {v7, v8}, Lcom/intsig/office/simpletext/model/Style;->setId(I)V

    .line 310
    .line 311
    .line 312
    :cond_6
    :goto_1
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 313
    .line 314
    .line 315
    move-result-object v4

    .line 316
    const-string v8, "paragraph"

    .line 317
    .line 318
    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 319
    .line 320
    .line 321
    move-result v4

    .line 322
    xor-int/lit8 v4, v4, 0x1

    .line 323
    .line 324
    int-to-byte v4, v4

    .line 325
    invoke-virtual {v7, v4}, Lcom/intsig/office/simpletext/model/Style;->setType(B)V

    .line 326
    .line 327
    .line 328
    const-string v4, "name"

    .line 329
    .line 330
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 331
    .line 332
    .line 333
    move-result-object v4

    .line 334
    const-string v8, "val"

    .line 335
    .line 336
    if-eqz v4, :cond_7

    .line 337
    .line 338
    invoke-interface {v4, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 339
    .line 340
    .line 341
    move-result-object v4

    .line 342
    invoke-virtual {v7, v4}, Lcom/intsig/office/simpletext/model/Style;->setName(Ljava/lang/String;)V

    .line 343
    .line 344
    .line 345
    :cond_7
    const-string v4, "basedOn"

    .line 346
    .line 347
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 348
    .line 349
    .line 350
    move-result-object v4

    .line 351
    if-eqz v4, :cond_9

    .line 352
    .line 353
    invoke-interface {v4, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 354
    .line 355
    .line 356
    move-result-object v4

    .line 357
    if-eqz v4, :cond_a

    .line 358
    .line 359
    iget-object v8, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleStrID:Ljava/util/Map;

    .line 360
    .line 361
    invoke-interface {v8, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    .line 363
    .line 364
    move-result-object v8

    .line 365
    check-cast v8, Ljava/lang/Integer;

    .line 366
    .line 367
    if-nez v8, :cond_8

    .line 368
    .line 369
    iget-object v8, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleStrID:Ljava/util/Map;

    .line 370
    .line 371
    iget v9, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleID:I

    .line 372
    .line 373
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 374
    .line 375
    .line 376
    move-result-object v9

    .line 377
    invoke-interface {v8, v4, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    .line 379
    .line 380
    iget v4, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleID:I

    .line 381
    .line 382
    invoke-virtual {v7, v4}, Lcom/intsig/office/simpletext/model/Style;->setBaseID(I)V

    .line 383
    .line 384
    .line 385
    iget v4, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleID:I

    .line 386
    .line 387
    add-int/lit8 v4, v4, 0x1

    .line 388
    .line 389
    iput v4, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleID:I

    .line 390
    .line 391
    goto :goto_2

    .line 392
    :cond_8
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    .line 393
    .line 394
    .line 395
    move-result v4

    .line 396
    invoke-virtual {v7, v4}, Lcom/intsig/office/simpletext/model/Style;->setBaseID(I)V

    .line 397
    .line 398
    .line 399
    goto :goto_2

    .line 400
    :cond_9
    const-string v4, "default"

    .line 401
    .line 402
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 403
    .line 404
    .line 405
    move-result-object v4

    .line 406
    const-string v8, "1"

    .line 407
    .line 408
    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 409
    .line 410
    .line 411
    move-result v4

    .line 412
    if-eqz v4, :cond_a

    .line 413
    .line 414
    invoke-virtual {v7, v1}, Lcom/intsig/office/simpletext/model/Style;->setBaseID(I)V

    .line 415
    .line 416
    .line 417
    :cond_a
    :goto_2
    invoke-interface {v3, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 418
    .line 419
    .line 420
    move-result-object v4

    .line 421
    if-eqz v4, :cond_b

    .line 422
    .line 423
    invoke-virtual {v7}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 424
    .line 425
    .line 426
    move-result-object v8

    .line 427
    invoke-direct {p0, v4, v8, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->processParaAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 428
    .line 429
    .line 430
    :cond_b
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 431
    .line 432
    .line 433
    move-result-object v3

    .line 434
    if-eqz v3, :cond_c

    .line 435
    .line 436
    invoke-virtual {v7}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 437
    .line 438
    .line 439
    move-result-object v4

    .line 440
    invoke-direct {p0, v3, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->processRunAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 441
    .line 442
    .line 443
    :cond_c
    invoke-static {}, Lcom/intsig/office/simpletext/model/StyleManage;->instance()Lcom/intsig/office/simpletext/model/StyleManage;

    .line 444
    .line 445
    .line 446
    move-result-object v3

    .line 447
    invoke-virtual {v3, v7}, Lcom/intsig/office/simpletext/model/StyleManage;->addStyle(Lcom/intsig/office/simpletext/model/Style;)V

    .line 448
    .line 449
    .line 450
    goto/16 :goto_0

    .line 451
    .line 452
    :cond_d
    :goto_3
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 453
    .line 454
    .line 455
    :cond_e
    return-void
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
    .line 1825
    .line 1826
    .line 1827
    .line 1828
    .line 1829
    .line 1830
    .line 1831
    .line 1832
    .line 1833
    .line 1834
    .line 1835
    .line 1836
    .line 1837
    .line 1838
    .line 1839
    .line 1840
    .line 1841
    .line 1842
    .line 1843
    .line 1844
    .line 1845
    .line 1846
    .line 1847
    .line 1848
    .line 1849
    .line 1850
    .line 1851
    .line 1852
    .line 1853
    .line 1854
    .line 1855
    .line 1856
    .line 1857
    .line 1858
    .line 1859
    .line 1860
    .line 1861
    .line 1862
    .line 1863
    .line 1864
    .line 1865
    .line 1866
    .line 1867
    .line 1868
    .line 1869
    .line 1870
    .line 1871
    .line 1872
    .line 1873
    .line 1874
    .line 1875
    .line 1876
    .line 1877
    .line 1878
    .line 1879
    .line 1880
    .line 1881
    .line 1882
    .line 1883
    .line 1884
    .line 1885
    .line 1886
    .line 1887
    .line 1888
    .line 1889
    .line 1890
    .line 1891
    .line 1892
    .line 1893
    .line 1894
    .line 1895
    .line 1896
    .line 1897
    .line 1898
    .line 1899
    .line 1900
    .line 1901
    .line 1902
    .line 1903
    .line 1904
    .line 1905
    .line 1906
    .line 1907
    .line 1908
    .line 1909
    .line 1910
    .line 1911
    .line 1912
    .line 1913
    .line 1914
    .line 1915
    .line 1916
    .line 1917
    .line 1918
    .line 1919
    .line 1920
    .line 1921
    .line 1922
    .line 1923
    .line 1924
    .line 1925
    .line 1926
    .line 1927
    .line 1928
    .line 1929
    .line 1930
    .line 1931
    .line 1932
    .line 1933
    .line 1934
    .line 1935
    .line 1936
    .line 1937
    .line 1938
    .line 1939
    .line 1940
    .line 1941
    .line 1942
    .line 1943
    .line 1944
    .line 1945
    .line 1946
    .line 1947
    .line 1948
    .line 1949
    .line 1950
    .line 1951
    .line 1952
    .line 1953
    .line 1954
    .line 1955
    .line 1956
    .line 1957
    .line 1958
    .line 1959
    .line 1960
    .line 1961
    .line 1962
    .line 1963
    .line 1964
    .line 1965
    .line 1966
    .line 1967
    .line 1968
    .line 1969
    .line 1970
    .line 1971
    .line 1972
    .line 1973
    .line 1974
    .line 1975
    .line 1976
    .line 1977
    .line 1978
    .line 1979
    .line 1980
    .line 1981
    .line 1982
    .line 1983
    .line 1984
    .line 1985
    .line 1986
    .line 1987
    .line 1988
    .line 1989
    .line 1990
    .line 1991
    .line 1992
    .line 1993
    .line 1994
    .line 1995
    .line 1996
    .line 1997
    .line 1998
    .line 1999
    .line 2000
    .line 2001
    .line 2002
    .line 2003
    .line 2004
    .line 2005
    .line 2006
    .line 2007
    .line 2008
    .line 2009
    .line 2010
    .line 2011
    .line 2012
    .line 2013
    .line 2014
    .line 2015
    .line 2016
    .line 2017
    .line 2018
    .line 2019
    .line 2020
    .line 2021
    .line 2022
    .line 2023
    .line 2024
    .line 2025
    .line 2026
    .line 2027
    .line 2028
    .line 2029
    .line 2030
    .line 2031
    .line 2032
    .line 2033
    .line 2034
    .line 2035
    .line 2036
    .line 2037
    .line 2038
    .line 2039
    .line 2040
    .line 2041
    .line 2042
    .line 2043
    .line 2044
    .line 2045
    .line 2046
    .line 2047
    .line 2048
    .line 2049
    .line 2050
    .line 2051
    .line 2052
    .line 2053
    .line 2054
    .line 2055
    .line 2056
    .line 2057
    .line 2058
    .line 2059
    .line 2060
    .line 2061
    .line 2062
    .line 2063
    .line 2064
    .line 2065
    .line 2066
    .line 2067
    .line 2068
    .line 2069
    .line 2070
    .line 2071
    .line 2072
    .line 2073
    .line 2074
    .line 2075
    .line 2076
    .line 2077
    .line 2078
    .line 2079
    .line 2080
    .line 2081
    .line 2082
    .line 2083
    .line 2084
    .line 2085
    .line 2086
.end method

.method private processTable(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 9

    .line 1
    new-instance v0, Lcom/intsig/office/wp/model/TableElement;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/wp/model/TableElement;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-wide v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 7
    .line 8
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 9
    .line 10
    .line 11
    const-string v1, "tblPr"

    .line 12
    .line 13
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const-string v2, ""

    .line 18
    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    invoke-direct {p0, v1, v3}, Lcom/intsig/office/fc/doc/DOCXReader;->processTableAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 26
    .line 27
    .line 28
    const-string v3, "tblStyle"

    .line 29
    .line 30
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    if-eqz v1, :cond_1

    .line 35
    .line 36
    const-string v3, "val"

    .line 37
    .line 38
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    if-nez v1, :cond_0

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_0
    move-object v2, v1

    .line 46
    :cond_1
    :goto_0
    const-string v1, "tblGrid"

    .line 47
    .line 48
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    const/4 v3, 0x0

    .line 53
    if-eqz v1, :cond_2

    .line 54
    .line 55
    const-string v4, "gridCol"

    .line 56
    .line 57
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    if-eqz v1, :cond_2

    .line 62
    .line 63
    const/4 v4, 0x0

    .line 64
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 65
    .line 66
    .line 67
    move-result v5

    .line 68
    if-ge v4, v5, :cond_2

    .line 69
    .line 70
    iget-object v5, p0, Lcom/intsig/office/fc/doc/DOCXReader;->tableGridCol:Ljava/util/Map;

    .line 71
    .line 72
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 73
    .line 74
    .line 75
    move-result-object v6

    .line 76
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 77
    .line 78
    .line 79
    move-result-object v7

    .line 80
    check-cast v7, Lcom/intsig/office/fc/dom4j/Element;

    .line 81
    .line 82
    const-string v8, "w"

    .line 83
    .line 84
    invoke-interface {v7, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v7

    .line 88
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 89
    .line 90
    .line 91
    move-result v7

    .line 92
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 93
    .line 94
    .line 95
    move-result-object v7

    .line 96
    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    .line 98
    .line 99
    add-int/lit8 v4, v4, 0x1

    .line 100
    .line 101
    goto :goto_1

    .line 102
    :cond_2
    const-string v1, "tr"

    .line 103
    .line 104
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 109
    .line 110
    .line 111
    move-result-object p1

    .line 112
    const/4 v1, 0x1

    .line 113
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 114
    .line 115
    .line 116
    move-result v4

    .line 117
    if-eqz v4, :cond_3

    .line 118
    .line 119
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 120
    .line 121
    .line 122
    move-result-object v4

    .line 123
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 124
    .line 125
    invoke-direct {p0, v4, v0, v1, v2}, Lcom/intsig/office/fc/doc/DOCXReader;->processRow(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/wp/model/TableElement;ZLjava/lang/String;)V

    .line 126
    .line 127
    .line 128
    const/4 v1, 0x0

    .line 129
    goto :goto_2

    .line 130
    :cond_3
    iget-wide v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 131
    .line 132
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 133
    .line 134
    .line 135
    iget-object p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 136
    .line 137
    iget-wide v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 138
    .line 139
    invoke-virtual {p1, v0, v1, v2}, Lcom/intsig/office/wp/model/WPDocument;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 140
    .line 141
    .line 142
    return-void
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private processTableAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 2

    .line 1
    const-string v0, "jc"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    const-string v1, "val"

    .line 10
    .line 11
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "center"

    .line 16
    .line 17
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_0

    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const/4 v1, 0x1

    .line 28
    invoke-virtual {v0, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const-string v1, "right"

    .line 33
    .line 34
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    if-eqz v0, :cond_1

    .line 39
    .line 40
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    const/4 v1, 0x2

    .line 45
    invoke-virtual {v0, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 46
    .line 47
    .line 48
    :cond_1
    :goto_0
    const-string v0, "tblInd"

    .line 49
    .line 50
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    if-eqz p1, :cond_2

    .line 55
    .line 56
    const-string v0, "w"

    .line 57
    .line 58
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    if-eqz p1, :cond_2

    .line 63
    .line 64
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 69
    .line 70
    .line 71
    move-result p1

    .line 72
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 73
    .line 74
    .line 75
    :cond_2
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processTextbox2007(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/fc/dom4j/Element;)Z
    .locals 27

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p2

    .line 4
    .line 5
    move-object/from16 v2, p3

    .line 6
    .line 7
    const-string v3, "textbox"

    .line 8
    .line 9
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    const-string v4, "bottom"

    .line 14
    .line 15
    const-string v5, "none"

    .line 16
    .line 17
    const-string v6, "middle"

    .line 18
    .line 19
    const-string v7, "mso-wrap-style"

    .line 20
    .line 21
    const-string v8, "v-text-anchor"

    .line 22
    .line 23
    const-string v9, "text-align"

    .line 24
    .line 25
    const-string v10, ":"

    .line 26
    .line 27
    const-string v11, ";"

    .line 28
    .line 29
    const-string v14, "style"

    .line 30
    .line 31
    const/16 v15, 0x20

    .line 32
    .line 33
    const-wide/high16 v16, 0x5000000000000000L    # 2.315841784746324E77

    .line 34
    .line 35
    const/high16 v18, 0x41700000    # 15.0f

    .line 36
    .line 37
    if-eqz v3, :cond_c

    .line 38
    .line 39
    const-string v13, "txbxContent"

    .line 40
    .line 41
    invoke-interface {v3, v13}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 42
    .line 43
    .line 44
    move-result-object v13

    .line 45
    if-eqz v13, :cond_19

    .line 46
    .line 47
    move-object/from16 v19, v13

    .line 48
    .line 49
    iget-wide v12, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 50
    .line 51
    move-wide/from16 v20, v12

    .line 52
    .line 53
    iget-wide v12, v0, Lcom/intsig/office/fc/doc/DOCXReader;->textboxIndex:J

    .line 54
    .line 55
    shl-long v22, v12, v15

    .line 56
    .line 57
    move-object/from16 v24, v4

    .line 58
    .line 59
    move-object/from16 v25, v5

    .line 60
    .line 61
    add-long v4, v22, v16

    .line 62
    .line 63
    iput-wide v4, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 64
    .line 65
    long-to-int v4, v12

    .line 66
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setElementIndex(I)V

    .line 67
    .line 68
    .line 69
    new-instance v4, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 70
    .line 71
    invoke-direct {v4}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    .line 72
    .line 73
    .line 74
    iget-wide v12, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 75
    .line 76
    invoke-virtual {v4, v12, v13}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 77
    .line 78
    .line 79
    iget-object v5, v0, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 80
    .line 81
    iget-wide v12, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 82
    .line 83
    invoke-virtual {v5, v4, v12, v13}, Lcom/intsig/office/wp/model/WPDocument;->appendElement(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 84
    .line 85
    .line 86
    invoke-interface/range {v19 .. v19}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 87
    .line 88
    .line 89
    move-result-object v5

    .line 90
    invoke-direct {v0, v5}, Lcom/intsig/office/fc/doc/DOCXReader;->processParagraphs(Ljava/util/List;)V

    .line 91
    .line 92
    .line 93
    invoke-virtual {v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 94
    .line 95
    .line 96
    move-result-object v5

    .line 97
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 98
    .line 99
    .line 100
    move-result-object v12

    .line 101
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 102
    .line 103
    .line 104
    move-result-object v13

    .line 105
    iget v13, v13, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 106
    .line 107
    int-to-float v13, v13

    .line 108
    mul-float v13, v13, v18

    .line 109
    .line 110
    float-to-int v13, v13

    .line 111
    invoke-virtual {v12, v5, v13}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 112
    .line 113
    .line 114
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 115
    .line 116
    .line 117
    move-result-object v12

    .line 118
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 119
    .line 120
    .line 121
    move-result-object v13

    .line 122
    iget v13, v13, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 123
    .line 124
    int-to-float v13, v13

    .line 125
    mul-float v13, v13, v18

    .line 126
    .line 127
    float-to-int v13, v13

    .line 128
    invoke-virtual {v12, v5, v13}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 129
    .line 130
    .line 131
    const-string v12, "inset"

    .line 132
    .line 133
    invoke-interface {v3, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v3

    .line 137
    if-eqz v3, :cond_4

    .line 138
    .line 139
    const-string v12, ","

    .line 140
    .line 141
    invoke-virtual {v3, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object v3

    .line 145
    array-length v12, v3

    .line 146
    const/high16 v13, 0x41a00000    # 20.0f

    .line 147
    .line 148
    const/high16 v15, 0x3f800000    # 1.0f

    .line 149
    .line 150
    if-lez v12, :cond_0

    .line 151
    .line 152
    const/4 v12, 0x0

    .line 153
    aget-object v16, v3, v12

    .line 154
    .line 155
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    .line 156
    .line 157
    .line 158
    move-result v16

    .line 159
    if-lez v16, :cond_0

    .line 160
    .line 161
    move-object/from16 v16, v4

    .line 162
    .line 163
    aget-object v4, v3, v12

    .line 164
    .line 165
    invoke-direct {v0, v4, v15}, Lcom/intsig/office/fc/doc/DOCXReader;->getValueInPt(Ljava/lang/String;F)F

    .line 166
    .line 167
    .line 168
    move-result v4

    .line 169
    mul-float v4, v4, v13

    .line 170
    .line 171
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 172
    .line 173
    .line 174
    move-result v4

    .line 175
    goto :goto_0

    .line 176
    :cond_0
    move-object/from16 v16, v4

    .line 177
    .line 178
    const/16 v4, 0x90

    .line 179
    .line 180
    :goto_0
    array-length v12, v3

    .line 181
    const/4 v13, 0x1

    .line 182
    if-le v12, v13, :cond_1

    .line 183
    .line 184
    aget-object v12, v3, v13

    .line 185
    .line 186
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    .line 187
    .line 188
    .line 189
    move-result v12

    .line 190
    if-lez v12, :cond_1

    .line 191
    .line 192
    aget-object v12, v3, v13

    .line 193
    .line 194
    invoke-direct {v0, v12, v15}, Lcom/intsig/office/fc/doc/DOCXReader;->getValueInPt(Ljava/lang/String;F)F

    .line 195
    .line 196
    .line 197
    move-result v12

    .line 198
    const/high16 v13, 0x41a00000    # 20.0f

    .line 199
    .line 200
    mul-float v12, v12, v13

    .line 201
    .line 202
    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    .line 203
    .line 204
    .line 205
    move-result v12

    .line 206
    goto :goto_1

    .line 207
    :cond_1
    const/16 v12, 0x48

    .line 208
    .line 209
    :goto_1
    array-length v13, v3

    .line 210
    const/4 v15, 0x2

    .line 211
    if-le v13, v15, :cond_2

    .line 212
    .line 213
    aget-object v13, v3, v15

    .line 214
    .line 215
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    .line 216
    .line 217
    .line 218
    move-result v13

    .line 219
    if-lez v13, :cond_2

    .line 220
    .line 221
    aget-object v13, v3, v15

    .line 222
    .line 223
    const/high16 v15, 0x3f800000    # 1.0f

    .line 224
    .line 225
    invoke-direct {v0, v13, v15}, Lcom/intsig/office/fc/doc/DOCXReader;->getValueInPt(Ljava/lang/String;F)F

    .line 226
    .line 227
    .line 228
    move-result v13

    .line 229
    const/high16 v15, 0x41a00000    # 20.0f

    .line 230
    .line 231
    mul-float v13, v13, v15

    .line 232
    .line 233
    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    .line 234
    .line 235
    .line 236
    move-result v13

    .line 237
    goto :goto_2

    .line 238
    :cond_2
    const/16 v13, 0x90

    .line 239
    .line 240
    :goto_2
    array-length v15, v3

    .line 241
    move/from16 v19, v4

    .line 242
    .line 243
    const/4 v4, 0x3

    .line 244
    if-le v15, v4, :cond_3

    .line 245
    .line 246
    aget-object v15, v3, v4

    .line 247
    .line 248
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    .line 249
    .line 250
    .line 251
    move-result v15

    .line 252
    if-lez v15, :cond_3

    .line 253
    .line 254
    aget-object v3, v3, v4

    .line 255
    .line 256
    const/high16 v4, 0x3f800000    # 1.0f

    .line 257
    .line 258
    invoke-direct {v0, v3, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->getValueInPt(Ljava/lang/String;F)F

    .line 259
    .line 260
    .line 261
    move-result v3

    .line 262
    const/high16 v4, 0x41a00000    # 20.0f

    .line 263
    .line 264
    mul-float v3, v3, v4

    .line 265
    .line 266
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 267
    .line 268
    .line 269
    move-result v3

    .line 270
    move v4, v13

    .line 271
    move/from16 v13, v19

    .line 272
    .line 273
    goto :goto_3

    .line 274
    :cond_3
    move v4, v13

    .line 275
    move/from16 v13, v19

    .line 276
    .line 277
    const/16 v3, 0x48

    .line 278
    .line 279
    goto :goto_3

    .line 280
    :cond_4
    move-object/from16 v16, v4

    .line 281
    .line 282
    const/16 v3, 0x48

    .line 283
    .line 284
    const/16 v4, 0x90

    .line 285
    .line 286
    const/16 v12, 0x48

    .line 287
    .line 288
    const/16 v13, 0x90

    .line 289
    .line 290
    :goto_3
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 291
    .line 292
    .line 293
    move-result-object v15

    .line 294
    invoke-virtual {v15, v5, v12}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 295
    .line 296
    .line 297
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 298
    .line 299
    .line 300
    move-result-object v12

    .line 301
    invoke-virtual {v12, v5, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 302
    .line 303
    .line 304
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 305
    .line 306
    .line 307
    move-result-object v3

    .line 308
    invoke-virtual {v3, v5, v13}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 309
    .line 310
    .line 311
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 312
    .line 313
    .line 314
    move-result-object v3

    .line 315
    invoke-virtual {v3, v5, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 316
    .line 317
    .line 318
    invoke-interface {v2, v14}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 319
    .line 320
    .line 321
    move-result-object v2

    .line 322
    if-eqz v2, :cond_b

    .line 323
    .line 324
    invoke-virtual {v2, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 325
    .line 326
    .line 327
    move-result-object v2

    .line 328
    const/4 v3, 0x0

    .line 329
    :goto_4
    array-length v4, v2

    .line 330
    if-ge v3, v4, :cond_b

    .line 331
    .line 332
    aget-object v4, v2, v3

    .line 333
    .line 334
    invoke-virtual {v4, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 335
    .line 336
    .line 337
    move-result-object v4

    .line 338
    if-eqz v4, :cond_9

    .line 339
    .line 340
    const/4 v11, 0x0

    .line 341
    aget-object v12, v4, v11

    .line 342
    .line 343
    if-eqz v12, :cond_9

    .line 344
    .line 345
    const/4 v13, 0x1

    .line 346
    aget-object v14, v4, v13

    .line 347
    .line 348
    if-eqz v14, :cond_9

    .line 349
    .line 350
    invoke-virtual {v9, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 351
    .line 352
    .line 353
    move-result v12

    .line 354
    if-eqz v12, :cond_5

    .line 355
    .line 356
    goto :goto_5

    .line 357
    :cond_5
    aget-object v12, v4, v11

    .line 358
    .line 359
    invoke-virtual {v8, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 360
    .line 361
    .line 362
    move-result v11

    .line 363
    if-eqz v11, :cond_8

    .line 364
    .line 365
    aget-object v11, v4, v13

    .line 366
    .line 367
    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 368
    .line 369
    .line 370
    move-result v11

    .line 371
    if-eqz v11, :cond_6

    .line 372
    .line 373
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 374
    .line 375
    .line 376
    move-result-object v4

    .line 377
    invoke-virtual {v4, v5, v13}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 378
    .line 379
    .line 380
    goto :goto_5

    .line 381
    :cond_6
    aget-object v11, v4, v13

    .line 382
    .line 383
    move-object/from16 v12, v24

    .line 384
    .line 385
    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 386
    .line 387
    .line 388
    move-result v11

    .line 389
    if-eqz v11, :cond_7

    .line 390
    .line 391
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 392
    .line 393
    .line 394
    move-result-object v4

    .line 395
    const/4 v11, 0x2

    .line 396
    invoke-virtual {v4, v5, v11}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 397
    .line 398
    .line 399
    goto :goto_6

    .line 400
    :cond_7
    const-string v11, "top"

    .line 401
    .line 402
    aget-object v4, v4, v13

    .line 403
    .line 404
    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 405
    .line 406
    .line 407
    move-result v4

    .line 408
    if-eqz v4, :cond_a

    .line 409
    .line 410
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 411
    .line 412
    .line 413
    move-result-object v4

    .line 414
    const/4 v11, 0x0

    .line 415
    invoke-virtual {v4, v5, v11}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 416
    .line 417
    .line 418
    goto :goto_6

    .line 419
    :cond_8
    move-object/from16 v12, v24

    .line 420
    .line 421
    const/4 v11, 0x0

    .line 422
    aget-object v14, v4, v11

    .line 423
    .line 424
    invoke-virtual {v7, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 425
    .line 426
    .line 427
    move-result v11

    .line 428
    if-eqz v11, :cond_a

    .line 429
    .line 430
    aget-object v4, v4, v13

    .line 431
    .line 432
    move-object/from16 v11, v25

    .line 433
    .line 434
    invoke-virtual {v11, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 435
    .line 436
    .line 437
    move-result v4

    .line 438
    xor-int/2addr v4, v13

    .line 439
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setTextWrapLine(Z)V

    .line 440
    .line 441
    .line 442
    goto :goto_7

    .line 443
    :cond_9
    :goto_5
    move-object/from16 v12, v24

    .line 444
    .line 445
    :cond_a
    :goto_6
    move-object/from16 v11, v25

    .line 446
    .line 447
    :goto_7
    add-int/lit8 v3, v3, 0x1

    .line 448
    .line 449
    move-object/from16 v25, v11

    .line 450
    .line 451
    move-object/from16 v24, v12

    .line 452
    .line 453
    goto :goto_4

    .line 454
    :cond_b
    iget-wide v2, v0, Lcom/intsig/office/fc/doc/DOCXReader;->textboxIndex:J

    .line 455
    .line 456
    long-to-int v3, v2

    .line 457
    invoke-virtual {v1, v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->setElementIndex(I)V

    .line 458
    .line 459
    .line 460
    iget-wide v1, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 461
    .line 462
    move-object/from16 v3, v16

    .line 463
    .line 464
    invoke-virtual {v3, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 465
    .line 466
    .line 467
    iget-wide v1, v0, Lcom/intsig/office/fc/doc/DOCXReader;->textboxIndex:J

    .line 468
    .line 469
    const-wide/16 v3, 0x1

    .line 470
    .line 471
    add-long/2addr v1, v3

    .line 472
    iput-wide v1, v0, Lcom/intsig/office/fc/doc/DOCXReader;->textboxIndex:J

    .line 473
    .line 474
    move-wide/from16 v1, v20

    .line 475
    .line 476
    iput-wide v1, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 477
    .line 478
    :goto_8
    const/4 v1, 0x1

    .line 479
    return v1

    .line 480
    :cond_c
    move-object v12, v4

    .line 481
    move-object v3, v5

    .line 482
    const-string v4, "textpath"

    .line 483
    .line 484
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 485
    .line 486
    .line 487
    move-result-object v5

    .line 488
    if-eqz v5, :cond_19

    .line 489
    .line 490
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 491
    .line 492
    .line 493
    move-result-object v4

    .line 494
    const-string v5, "string"

    .line 495
    .line 496
    invoke-interface {v4, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 497
    .line 498
    .line 499
    move-result-object v4

    .line 500
    const/4 v5, 0x0

    .line 501
    invoke-virtual {v1, v5}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 502
    .line 503
    .line 504
    move-object/from16 v24, v12

    .line 505
    .line 506
    iget-wide v12, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 507
    .line 508
    move-wide/from16 v19, v12

    .line 509
    .line 510
    iget-wide v12, v0, Lcom/intsig/office/fc/doc/DOCXReader;->textboxIndex:J

    .line 511
    .line 512
    shl-long v21, v12, v15

    .line 513
    .line 514
    move-object v15, v6

    .line 515
    add-long v5, v21, v16

    .line 516
    .line 517
    iput-wide v5, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 518
    .line 519
    long-to-int v5, v12

    .line 520
    invoke-virtual {v1, v5}, Lcom/intsig/office/common/shape/WPAbstractShape;->setElementIndex(I)V

    .line 521
    .line 522
    .line 523
    new-instance v5, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 524
    .line 525
    invoke-direct {v5}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    .line 526
    .line 527
    .line 528
    iget-wide v12, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 529
    .line 530
    invoke-virtual {v5, v12, v13}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 531
    .line 532
    .line 533
    iget-object v6, v0, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 534
    .line 535
    iget-wide v12, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 536
    .line 537
    invoke-virtual {v6, v5, v12, v13}, Lcom/intsig/office/wp/model/WPDocument;->appendElement(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 538
    .line 539
    .line 540
    new-instance v6, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 541
    .line 542
    invoke-direct {v6}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 543
    .line 544
    .line 545
    iget-wide v12, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 546
    .line 547
    invoke-virtual {v6, v12, v13}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 548
    .line 549
    .line 550
    move-object/from16 v16, v15

    .line 551
    .line 552
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 553
    .line 554
    .line 555
    move-result v15

    .line 556
    if-lez v15, :cond_f

    .line 557
    .line 558
    new-instance v1, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 559
    .line 560
    invoke-direct {v1, v4}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 561
    .line 562
    .line 563
    move-object/from16 v25, v3

    .line 564
    .line 565
    const-string v3, "fillcolor"

    .line 566
    .line 567
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 568
    .line 569
    .line 570
    move-result-object v3

    .line 571
    if-eqz v3, :cond_d

    .line 572
    .line 573
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    .line 574
    .line 575
    .line 576
    move-result v17

    .line 577
    if-lez v17, :cond_d

    .line 578
    .line 579
    move-object/from16 v17, v7

    .line 580
    .line 581
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 582
    .line 583
    .line 584
    move-result-object v7

    .line 585
    move-object/from16 v21, v8

    .line 586
    .line 587
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 588
    .line 589
    .line 590
    move-result-object v8

    .line 591
    move-object/from16 v22, v9

    .line 592
    .line 593
    const/4 v9, 0x1

    .line 594
    invoke-direct {v0, v3, v9}, Lcom/intsig/office/fc/doc/DOCXReader;->getColor(Ljava/lang/String;Z)I

    .line 595
    .line 596
    .line 597
    move-result v3

    .line 598
    invoke-virtual {v7, v8, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 599
    .line 600
    .line 601
    goto :goto_9

    .line 602
    :cond_d
    move-object/from16 v17, v7

    .line 603
    .line 604
    move-object/from16 v21, v8

    .line 605
    .line 606
    move-object/from16 v22, v9

    .line 607
    .line 608
    :goto_9
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 609
    .line 610
    .line 611
    move-result-object v3

    .line 612
    invoke-virtual {v3}, Lcom/intsig/office/java/awt/Rectangle;->getWidth()D

    .line 613
    .line 614
    .line 615
    move-result-wide v7

    .line 616
    double-to-float v3, v7

    .line 617
    const v7, 0x4199999a    # 19.2f

    .line 618
    .line 619
    .line 620
    sub-float/2addr v3, v7

    .line 621
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 622
    .line 623
    .line 624
    move-result-object v7

    .line 625
    invoke-virtual {v7}, Lcom/intsig/office/java/awt/Rectangle;->getHeight()D

    .line 626
    .line 627
    .line 628
    move-result-wide v7

    .line 629
    double-to-float v7, v7

    .line 630
    const v8, 0x4119999a    # 9.6f

    .line 631
    .line 632
    .line 633
    sub-float/2addr v7, v8

    .line 634
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    .line 635
    .line 636
    .line 637
    move-result-object v8

    .line 638
    invoke-virtual {v8}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    .line 639
    .line 640
    .line 641
    move-result-object v8

    .line 642
    const/16 v9, 0xc

    .line 643
    .line 644
    move-object/from16 v23, v10

    .line 645
    .line 646
    int-to-float v10, v9

    .line 647
    invoke-virtual {v8, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 648
    .line 649
    .line 650
    invoke-virtual {v8}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 651
    .line 652
    .line 653
    move-result-object v10

    .line 654
    move-object/from16 v26, v11

    .line 655
    .line 656
    :goto_a
    invoke-virtual {v8, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 657
    .line 658
    .line 659
    move-result v11

    .line 660
    float-to-int v11, v11

    .line 661
    int-to-float v11, v11

    .line 662
    cmpg-float v11, v11, v3

    .line 663
    .line 664
    if-gez v11, :cond_e

    .line 665
    .line 666
    iget v11, v10, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 667
    .line 668
    iget v10, v10, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 669
    .line 670
    sub-float/2addr v11, v10

    .line 671
    float-to-double v10, v11

    .line 672
    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    .line 673
    .line 674
    .line 675
    move-result-wide v10

    .line 676
    double-to-int v10, v10

    .line 677
    int-to-float v10, v10

    .line 678
    cmpg-float v10, v10, v7

    .line 679
    .line 680
    if-gez v10, :cond_e

    .line 681
    .line 682
    add-int/lit8 v9, v9, 0x1

    .line 683
    .line 684
    int-to-float v10, v9

    .line 685
    invoke-virtual {v8, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 686
    .line 687
    .line 688
    invoke-virtual {v8}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 689
    .line 690
    .line 691
    move-result-object v10

    .line 692
    goto :goto_a

    .line 693
    :cond_e
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 694
    .line 695
    .line 696
    move-result-object v3

    .line 697
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 698
    .line 699
    .line 700
    move-result-object v4

    .line 701
    const/4 v7, 0x1

    .line 702
    sub-int/2addr v9, v7

    .line 703
    int-to-float v7, v9

    .line 704
    const/high16 v8, 0x3f400000    # 0.75f

    .line 705
    .line 706
    mul-float v7, v7, v8

    .line 707
    .line 708
    float-to-int v7, v7

    .line 709
    invoke-virtual {v3, v4, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontSize(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 710
    .line 711
    .line 712
    iget-wide v3, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 713
    .line 714
    invoke-virtual {v1, v3, v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 715
    .line 716
    .line 717
    iget-wide v3, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 718
    .line 719
    int-to-long v7, v15

    .line 720
    add-long/2addr v3, v7

    .line 721
    iput-wide v3, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 722
    .line 723
    invoke-virtual {v1, v3, v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 724
    .line 725
    .line 726
    invoke-virtual {v6, v1}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 727
    .line 728
    .line 729
    goto :goto_b

    .line 730
    :cond_f
    move-object/from16 v25, v3

    .line 731
    .line 732
    move-object/from16 v17, v7

    .line 733
    .line 734
    move-object/from16 v21, v8

    .line 735
    .line 736
    move-object/from16 v22, v9

    .line 737
    .line 738
    move-object/from16 v23, v10

    .line 739
    .line 740
    move-object/from16 v26, v11

    .line 741
    .line 742
    :goto_b
    iget-wide v3, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 743
    .line 744
    invoke-virtual {v6, v3, v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 745
    .line 746
    .line 747
    iget-wide v3, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 748
    .line 749
    cmp-long v1, v3, v12

    .line 750
    .line 751
    if-lez v1, :cond_10

    .line 752
    .line 753
    iget-object v1, v0, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 754
    .line 755
    invoke-virtual {v1, v6, v3, v4}, Lcom/intsig/office/wp/model/WPDocument;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 756
    .line 757
    .line 758
    :cond_10
    invoke-virtual {v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 759
    .line 760
    .line 761
    move-result-object v1

    .line 762
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 763
    .line 764
    .line 765
    move-result-object v3

    .line 766
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 767
    .line 768
    .line 769
    move-result-object v4

    .line 770
    iget v4, v4, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 771
    .line 772
    int-to-float v4, v4

    .line 773
    mul-float v4, v4, v18

    .line 774
    .line 775
    float-to-int v4, v4

    .line 776
    invoke-virtual {v3, v1, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 777
    .line 778
    .line 779
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 780
    .line 781
    .line 782
    move-result-object v3

    .line 783
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 784
    .line 785
    .line 786
    move-result-object v4

    .line 787
    iget v4, v4, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 788
    .line 789
    int-to-float v4, v4

    .line 790
    mul-float v4, v4, v18

    .line 791
    .line 792
    float-to-int v4, v4

    .line 793
    invoke-virtual {v3, v1, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 794
    .line 795
    .line 796
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 797
    .line 798
    .line 799
    move-result-object v3

    .line 800
    const/16 v4, 0x48

    .line 801
    .line 802
    invoke-virtual {v3, v1, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 803
    .line 804
    .line 805
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 806
    .line 807
    .line 808
    move-result-object v3

    .line 809
    invoke-virtual {v3, v1, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 810
    .line 811
    .line 812
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 813
    .line 814
    .line 815
    move-result-object v3

    .line 816
    const/16 v4, 0x90

    .line 817
    .line 818
    invoke-virtual {v3, v1, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 819
    .line 820
    .line 821
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 822
    .line 823
    .line 824
    move-result-object v3

    .line 825
    invoke-virtual {v3, v1, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 826
    .line 827
    .line 828
    invoke-interface {v2, v14}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 829
    .line 830
    .line 831
    move-result-object v2

    .line 832
    if-eqz v2, :cond_18

    .line 833
    .line 834
    move-object/from16 v3, v26

    .line 835
    .line 836
    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 837
    .line 838
    .line 839
    move-result-object v2

    .line 840
    const/4 v12, 0x0

    .line 841
    :goto_c
    array-length v3, v2

    .line 842
    if-ge v12, v3, :cond_18

    .line 843
    .line 844
    aget-object v3, v2, v12

    .line 845
    .line 846
    move-object/from16 v4, v23

    .line 847
    .line 848
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 849
    .line 850
    .line 851
    move-result-object v3

    .line 852
    if-eqz v3, :cond_17

    .line 853
    .line 854
    const/4 v6, 0x0

    .line 855
    aget-object v7, v3, v6

    .line 856
    .line 857
    if-eqz v7, :cond_17

    .line 858
    .line 859
    const/4 v8, 0x1

    .line 860
    aget-object v9, v3, v8

    .line 861
    .line 862
    if-eqz v9, :cond_17

    .line 863
    .line 864
    move-object/from16 v9, v22

    .line 865
    .line 866
    invoke-virtual {v9, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 867
    .line 868
    .line 869
    move-result v7

    .line 870
    if-eqz v7, :cond_11

    .line 871
    .line 872
    move-object/from16 v8, p2

    .line 873
    .line 874
    move-object/from16 v10, v16

    .line 875
    .line 876
    move-object/from16 v13, v17

    .line 877
    .line 878
    move-object/from16 v6, v21

    .line 879
    .line 880
    goto/16 :goto_f

    .line 881
    .line 882
    :cond_11
    aget-object v7, v3, v6

    .line 883
    .line 884
    move-object/from16 v6, v21

    .line 885
    .line 886
    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 887
    .line 888
    .line 889
    move-result v7

    .line 890
    if-eqz v7, :cond_15

    .line 891
    .line 892
    aget-object v7, v3, v8

    .line 893
    .line 894
    move-object/from16 v10, v16

    .line 895
    .line 896
    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 897
    .line 898
    .line 899
    move-result v7

    .line 900
    if-eqz v7, :cond_12

    .line 901
    .line 902
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 903
    .line 904
    .line 905
    move-result-object v3

    .line 906
    invoke-virtual {v3, v1, v8}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 907
    .line 908
    .line 909
    move-object/from16 v8, p2

    .line 910
    .line 911
    move-object/from16 v13, v17

    .line 912
    .line 913
    goto :goto_f

    .line 914
    :cond_12
    aget-object v7, v3, v8

    .line 915
    .line 916
    move-object/from16 v11, v24

    .line 917
    .line 918
    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 919
    .line 920
    .line 921
    move-result v7

    .line 922
    if-eqz v7, :cond_13

    .line 923
    .line 924
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 925
    .line 926
    .line 927
    move-result-object v3

    .line 928
    const/4 v7, 0x2

    .line 929
    invoke-virtual {v3, v1, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 930
    .line 931
    .line 932
    goto :goto_d

    .line 933
    :cond_13
    const/4 v7, 0x2

    .line 934
    const-string v13, "top"

    .line 935
    .line 936
    aget-object v3, v3, v8

    .line 937
    .line 938
    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 939
    .line 940
    .line 941
    move-result v3

    .line 942
    if-eqz v3, :cond_14

    .line 943
    .line 944
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 945
    .line 946
    .line 947
    move-result-object v3

    .line 948
    const/4 v13, 0x0

    .line 949
    invoke-virtual {v3, v1, v13}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 950
    .line 951
    .line 952
    :cond_14
    :goto_d
    move-object/from16 v8, p2

    .line 953
    .line 954
    move-object/from16 v13, v17

    .line 955
    .line 956
    goto :goto_e

    .line 957
    :cond_15
    move-object/from16 v10, v16

    .line 958
    .line 959
    move-object/from16 v11, v24

    .line 960
    .line 961
    const/4 v7, 0x2

    .line 962
    const/4 v13, 0x0

    .line 963
    aget-object v14, v3, v13

    .line 964
    .line 965
    move-object/from16 v13, v17

    .line 966
    .line 967
    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 968
    .line 969
    .line 970
    move-result v14

    .line 971
    if-eqz v14, :cond_16

    .line 972
    .line 973
    aget-object v3, v3, v8

    .line 974
    .line 975
    move-object/from16 v14, v25

    .line 976
    .line 977
    invoke-virtual {v14, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 978
    .line 979
    .line 980
    move-result v3

    .line 981
    xor-int/2addr v3, v8

    .line 982
    move-object/from16 v8, p2

    .line 983
    .line 984
    invoke-virtual {v8, v3}, Lcom/intsig/office/common/shape/WPAbstractShape;->setTextWrapLine(Z)V

    .line 985
    .line 986
    .line 987
    goto :goto_10

    .line 988
    :cond_16
    move-object/from16 v8, p2

    .line 989
    .line 990
    :goto_e
    move-object/from16 v14, v25

    .line 991
    .line 992
    goto :goto_10

    .line 993
    :cond_17
    move-object/from16 v8, p2

    .line 994
    .line 995
    move-object/from16 v10, v16

    .line 996
    .line 997
    move-object/from16 v13, v17

    .line 998
    .line 999
    move-object/from16 v6, v21

    .line 1000
    .line 1001
    move-object/from16 v9, v22

    .line 1002
    .line 1003
    :goto_f
    move-object/from16 v11, v24

    .line 1004
    .line 1005
    move-object/from16 v14, v25

    .line 1006
    .line 1007
    const/4 v7, 0x2

    .line 1008
    :goto_10
    add-int/lit8 v12, v12, 0x1

    .line 1009
    .line 1010
    move-object/from16 v23, v4

    .line 1011
    .line 1012
    move-object/from16 v21, v6

    .line 1013
    .line 1014
    move-object/from16 v22, v9

    .line 1015
    .line 1016
    move-object/from16 v16, v10

    .line 1017
    .line 1018
    move-object/from16 v24, v11

    .line 1019
    .line 1020
    move-object/from16 v17, v13

    .line 1021
    .line 1022
    move-object/from16 v25, v14

    .line 1023
    .line 1024
    goto/16 :goto_c

    .line 1025
    .line 1026
    :cond_18
    move-object/from16 v8, p2

    .line 1027
    .line 1028
    iget-wide v1, v0, Lcom/intsig/office/fc/doc/DOCXReader;->textboxIndex:J

    .line 1029
    .line 1030
    long-to-int v2, v1

    .line 1031
    invoke-virtual {v8, v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setElementIndex(I)V

    .line 1032
    .line 1033
    .line 1034
    iget-wide v1, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 1035
    .line 1036
    invoke-virtual {v5, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 1037
    .line 1038
    .line 1039
    iget-wide v1, v0, Lcom/intsig/office/fc/doc/DOCXReader;->textboxIndex:J

    .line 1040
    .line 1041
    const-wide/16 v3, 0x1

    .line 1042
    .line 1043
    add-long/2addr v1, v3

    .line 1044
    iput-wide v1, v0, Lcom/intsig/office/fc/doc/DOCXReader;->textboxIndex:J

    .line 1045
    .line 1046
    move-wide/from16 v1, v19

    .line 1047
    .line 1048
    iput-wide v1, v0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 1049
    .line 1050
    goto/16 :goto_8

    .line 1051
    .line 1052
    :cond_19
    const/4 v1, 0x0

    .line 1053
    return v1
.end method

.method private processTextbox2010(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/fc/dom4j/Element;)Z
    .locals 9

    .line 1
    const-string p1, "txbx"

    .line 2
    .line 3
    invoke-interface {p3, p1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const/4 v0, 0x0

    .line 8
    if-eqz p1, :cond_6

    .line 9
    .line 10
    const-string v1, "txbxContent"

    .line 11
    .line 12
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    if-eqz p1, :cond_6

    .line 17
    .line 18
    iget-wide v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 19
    .line 20
    iget-wide v3, p0, Lcom/intsig/office/fc/doc/DOCXReader;->textboxIndex:J

    .line 21
    .line 22
    const/16 v5, 0x20

    .line 23
    .line 24
    shl-long v5, v3, v5

    .line 25
    .line 26
    const-wide/high16 v7, 0x5000000000000000L    # 2.315841784746324E77

    .line 27
    .line 28
    add-long/2addr v5, v7

    .line 29
    iput-wide v5, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 30
    .line 31
    long-to-int v4, v3

    .line 32
    invoke-virtual {p2, v4}, Lcom/intsig/office/common/shape/WPAbstractShape;->setElementIndex(I)V

    .line 33
    .line 34
    .line 35
    new-instance v3, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 36
    .line 37
    invoke-direct {v3}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    .line 38
    .line 39
    .line 40
    iget-wide v4, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 41
    .line 42
    invoke-virtual {v3, v4, v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 43
    .line 44
    .line 45
    iget-object v4, p0, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 46
    .line 47
    iget-wide v5, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 48
    .line 49
    invoke-virtual {v4, v3, v5, v6}, Lcom/intsig/office/wp/model/WPDocument;->appendElement(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 50
    .line 51
    .line 52
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/doc/DOCXReader;->processParagraphs(Ljava/util/List;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 64
    .line 65
    .line 66
    move-result-object v4

    .line 67
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 68
    .line 69
    .line 70
    move-result-object v5

    .line 71
    iget v5, v5, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 72
    .line 73
    int-to-float v5, v5

    .line 74
    const/high16 v6, 0x41700000    # 15.0f

    .line 75
    .line 76
    mul-float v5, v5, v6

    .line 77
    .line 78
    float-to-int v5, v5

    .line 79
    invoke-virtual {v4, p1, v5}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 80
    .line 81
    .line 82
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 83
    .line 84
    .line 85
    move-result-object v4

    .line 86
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 87
    .line 88
    .line 89
    move-result-object v5

    .line 90
    iget v5, v5, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 91
    .line 92
    int-to-float v5, v5

    .line 93
    mul-float v5, v5, v6

    .line 94
    .line 95
    float-to-int v5, v5

    .line 96
    invoke-virtual {v4, p1, v5}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 97
    .line 98
    .line 99
    const-string v4, "bodyPr"

    .line 100
    .line 101
    invoke-interface {p3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 102
    .line 103
    .line 104
    move-result-object p3

    .line 105
    const/4 v4, 0x1

    .line 106
    if-eqz p3, :cond_5

    .line 107
    .line 108
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 109
    .line 110
    .line 111
    move-result-object v5

    .line 112
    const-string v6, "tIns"

    .line 113
    .line 114
    invoke-interface {p3, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v6

    .line 118
    invoke-direct {p0, v6, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->processValue(Ljava/lang/String;Z)I

    .line 119
    .line 120
    .line 121
    move-result v6

    .line 122
    invoke-virtual {v5, p1, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 123
    .line 124
    .line 125
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 126
    .line 127
    .line 128
    move-result-object v5

    .line 129
    const-string v6, "bIns"

    .line 130
    .line 131
    invoke-interface {p3, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 132
    .line 133
    .line 134
    move-result-object v6

    .line 135
    invoke-direct {p0, v6, v0}, Lcom/intsig/office/fc/doc/DOCXReader;->processValue(Ljava/lang/String;Z)I

    .line 136
    .line 137
    .line 138
    move-result v6

    .line 139
    invoke-virtual {v5, p1, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 140
    .line 141
    .line 142
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 143
    .line 144
    .line 145
    move-result-object v5

    .line 146
    const-string v6, "lIns"

    .line 147
    .line 148
    invoke-interface {p3, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v6

    .line 152
    invoke-direct {p0, v6, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->processValue(Ljava/lang/String;Z)I

    .line 153
    .line 154
    .line 155
    move-result v6

    .line 156
    invoke-virtual {v5, p1, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 157
    .line 158
    .line 159
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 160
    .line 161
    .line 162
    move-result-object v5

    .line 163
    const-string v6, "rIns"

    .line 164
    .line 165
    invoke-interface {p3, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 166
    .line 167
    .line 168
    move-result-object v6

    .line 169
    invoke-direct {p0, v6, v4}, Lcom/intsig/office/fc/doc/DOCXReader;->processValue(Ljava/lang/String;Z)I

    .line 170
    .line 171
    .line 172
    move-result v6

    .line 173
    invoke-virtual {v5, p1, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 174
    .line 175
    .line 176
    const-string v5, "anchor"

    .line 177
    .line 178
    invoke-interface {p3, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 179
    .line 180
    .line 181
    move-result-object v5

    .line 182
    const-string v6, "ctr"

    .line 183
    .line 184
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 185
    .line 186
    .line 187
    move-result v6

    .line 188
    if-eqz v6, :cond_0

    .line 189
    .line 190
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 191
    .line 192
    .line 193
    move-result-object v5

    .line 194
    invoke-virtual {v5, p1, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 195
    .line 196
    .line 197
    goto :goto_0

    .line 198
    :cond_0
    const-string v6, "b"

    .line 199
    .line 200
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 201
    .line 202
    .line 203
    move-result v6

    .line 204
    if-eqz v6, :cond_1

    .line 205
    .line 206
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 207
    .line 208
    .line 209
    move-result-object v5

    .line 210
    const/4 v6, 0x2

    .line 211
    invoke-virtual {v5, p1, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 212
    .line 213
    .line 214
    goto :goto_0

    .line 215
    :cond_1
    const-string v6, "t"

    .line 216
    .line 217
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 218
    .line 219
    .line 220
    move-result v5

    .line 221
    if-eqz v5, :cond_2

    .line 222
    .line 223
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 224
    .line 225
    .line 226
    move-result-object v5

    .line 227
    invoke-virtual {v5, p1, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 228
    .line 229
    .line 230
    :cond_2
    :goto_0
    const-string p1, "wrap"

    .line 231
    .line 232
    invoke-interface {p3, p1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 233
    .line 234
    .line 235
    move-result-object p1

    .line 236
    if-eqz p1, :cond_3

    .line 237
    .line 238
    const-string p3, "square"

    .line 239
    .line 240
    invoke-virtual {p3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 241
    .line 242
    .line 243
    move-result p1

    .line 244
    if-eqz p1, :cond_4

    .line 245
    .line 246
    :cond_3
    const/4 v0, 0x1

    .line 247
    :cond_4
    invoke-virtual {p2, v0}, Lcom/intsig/office/common/shape/WPAbstractShape;->setTextWrapLine(Z)V

    .line 248
    .line 249
    .line 250
    iget-wide v5, p0, Lcom/intsig/office/fc/doc/DOCXReader;->textboxIndex:J

    .line 251
    .line 252
    long-to-int p1, v5

    .line 253
    invoke-virtual {p2, p1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setElementIndex(I)V

    .line 254
    .line 255
    .line 256
    :cond_5
    iget-wide p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 257
    .line 258
    invoke-virtual {v3, p1, p2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 259
    .line 260
    .line 261
    iget-wide p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->textboxIndex:J

    .line 262
    .line 263
    const-wide/16 v5, 0x1

    .line 264
    .line 265
    add-long/2addr p1, v5

    .line 266
    iput-wide p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->textboxIndex:J

    .line 267
    .line 268
    iput-wide v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 269
    .line 270
    return v4

    .line 271
    :cond_6
    return v0
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private processThemeColor()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const-string v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme"

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/4 v1, 0x0

    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationship(I)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    if-eqz v0, :cond_0

    .line 29
    .line 30
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ThemeReader;->instance()Lcom/intsig/office/fc/ppt/reader/ThemeReader;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/ppt/reader/ThemeReader;->getThemeColorMap(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)Ljava/util/Map;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->themeColor:Ljava/util/Map;

    .line 39
    .line 40
    if-eqz v0, :cond_0

    .line 41
    .line 42
    const-string v1, "lt1"

    .line 43
    .line 44
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    check-cast v1, Ljava/lang/Integer;

    .line 49
    .line 50
    const-string v2, "bg1"

    .line 51
    .line 52
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->themeColor:Ljava/util/Map;

    .line 56
    .line 57
    const-string v1, "dk1"

    .line 58
    .line 59
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    check-cast v1, Ljava/lang/Integer;

    .line 64
    .line 65
    const-string v2, "tx1"

    .line 66
    .line 67
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    .line 69
    .line 70
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->themeColor:Ljava/util/Map;

    .line 71
    .line 72
    const-string v1, "lt2"

    .line 73
    .line 74
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    check-cast v1, Ljava/lang/Integer;

    .line 79
    .line 80
    const-string v2, "bg2"

    .line 81
    .line 82
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->themeColor:Ljava/util/Map;

    .line 86
    .line 87
    const-string v1, "dk2"

    .line 88
    .line 89
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    .line 91
    .line 92
    move-result-object v1

    .line 93
    check-cast v1, Ljava/lang/Integer;

    .line 94
    .line 95
    const-string v2, "tx2"

    .line 96
    .line 97
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    .line 99
    .line 100
    :cond_0
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private processValue(Ljava/lang/String;Z)I
    .locals 3

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    const/16 p2, 0x90

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const/16 p2, 0x48

    .line 7
    .line 8
    :goto_0
    if-eqz p1, :cond_2

    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 11
    .line 12
    .line 13
    move-result-object p2

    .line 14
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->isDecimal(Ljava/lang/String;)Z

    .line 15
    .line 16
    .line 17
    move-result p2

    .line 18
    const/high16 v0, 0x41700000    # 15.0f

    .line 19
    .line 20
    const v1, 0x495f3e00    # 914400.0f

    .line 21
    .line 22
    .line 23
    const/high16 v2, 0x42c00000    # 96.0f

    .line 24
    .line 25
    if-eqz p2, :cond_1

    .line 26
    .line 27
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    goto :goto_1

    .line 32
    :cond_1
    const/16 p2, 0x10

    .line 33
    .line 34
    invoke-static {p1, p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    :goto_1
    int-to-float p1, p1

    .line 39
    mul-float p1, p1, v2

    .line 40
    .line 41
    div-float/2addr p1, v1

    .line 42
    mul-float p1, p1, v0

    .line 43
    .line 44
    float-to-int p2, p1

    .line 45
    :cond_2
    return p2
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private processWatermark(Lcom/intsig/office/common/shape/WatermarkShape;Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 7

    .line 1
    const-string v0, "textpath"

    .line 2
    .line 3
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_4

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    invoke-virtual {p1, v1}, Lcom/intsig/office/common/shape/WatermarkShape;->setWatermarkType(B)V

    .line 11
    .line 12
    .line 13
    const-string v2, "fillcolor"

    .line 14
    .line 15
    invoke-interface {p2, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    if-eqz v2, :cond_0

    .line 20
    .line 21
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    if-lez v3, :cond_0

    .line 26
    .line 27
    invoke-direct {p0, v2, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->getColor(Ljava/lang/String;Z)I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    invoke-virtual {p1, v2}, Lcom/intsig/office/common/shape/WatermarkShape;->setFontColor(I)V

    .line 32
    .line 33
    .line 34
    :cond_0
    const-string v2, "fill"

    .line 35
    .line 36
    invoke-interface {p2, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 37
    .line 38
    .line 39
    move-result-object p2

    .line 40
    if-eqz p2, :cond_1

    .line 41
    .line 42
    const-string v2, "opacity"

    .line 43
    .line 44
    invoke-interface {p2, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p2

    .line 48
    invoke-static {p2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 49
    .line 50
    .line 51
    move-result p2

    .line 52
    invoke-virtual {p1, p2}, Lcom/intsig/office/common/shape/WatermarkShape;->setOpacity(F)V

    .line 53
    .line 54
    .line 55
    :cond_1
    const-string p2, "string"

    .line 56
    .line 57
    invoke-interface {v0, p2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object p2

    .line 61
    invoke-virtual {p1, p2}, Lcom/intsig/office/common/shape/WatermarkShape;->setWatermartString(Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    const-string p2, "style"

    .line 65
    .line 66
    invoke-interface {v0, p2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object p2

    .line 70
    const-string v0, ";"

    .line 71
    .line 72
    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object p2

    .line 76
    array-length v0, p2

    .line 77
    const/4 v2, 0x0

    .line 78
    :goto_0
    if-ge v2, v0, :cond_4

    .line 79
    .line 80
    aget-object v3, p2, v2

    .line 81
    .line 82
    const-string v4, ":"

    .line 83
    .line 84
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v3

    .line 88
    aget-object v4, v3, v1

    .line 89
    .line 90
    const-string v5, "font-size"

    .line 91
    .line 92
    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 93
    .line 94
    .line 95
    move-result v4

    .line 96
    if-eqz v4, :cond_3

    .line 97
    .line 98
    const/4 v4, 0x1

    .line 99
    aget-object v3, v3, v4

    .line 100
    .line 101
    const-string v5, "pt"

    .line 102
    .line 103
    const-string v6, ""

    .line 104
    .line 105
    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v3

    .line 109
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 110
    .line 111
    .line 112
    move-result v3

    .line 113
    if-ne v3, v4, :cond_2

    .line 114
    .line 115
    invoke-virtual {p1, v4}, Lcom/intsig/office/common/shape/WatermarkShape;->setAutoFontSize(Z)V

    .line 116
    .line 117
    .line 118
    goto :goto_1

    .line 119
    :cond_2
    invoke-virtual {p1, v3}, Lcom/intsig/office/common/shape/WatermarkShape;->setFontSize(I)V

    .line 120
    .line 121
    .line 122
    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 123
    .line 124
    goto :goto_0

    .line 125
    :cond_4
    return-void
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processWrapAndPosition_Drawing(Lcom/intsig/office/common/shape/WPAbstractShape;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)V
    .locals 12

    .line 1
    const-string v0, "behindDoc"

    .line 2
    .line 3
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "1"

    .line 8
    .line 9
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x6

    .line 16
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 17
    .line 18
    .line 19
    :cond_0
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->getDrawingWrapType(Lcom/intsig/office/fc/dom4j/Element;)S

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 24
    .line 25
    .line 26
    const-string v0, "AlternateContent"

    .line 27
    .line 28
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    const/4 v1, 0x0

    .line 37
    move-object v2, v1

    .line 38
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    const-string v4, "positionV"

    .line 43
    .line 44
    const-string v5, "positionH"

    .line 45
    .line 46
    if-eqz v3, :cond_3

    .line 47
    .line 48
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    move-result-object v3

    .line 52
    check-cast v3, Lcom/intsig/office/fc/dom4j/Element;

    .line 53
    .line 54
    const-string v6, "Choice"

    .line 55
    .line 56
    invoke-interface {v3, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 57
    .line 58
    .line 59
    move-result-object v3

    .line 60
    if-eqz v3, :cond_1

    .line 61
    .line 62
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 63
    .line 64
    .line 65
    move-result-object v6

    .line 66
    if-eqz v6, :cond_2

    .line 67
    .line 68
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    :cond_2
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 73
    .line 74
    .line 75
    move-result-object v5

    .line 76
    if-eqz v5, :cond_1

    .line 77
    .line 78
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 79
    .line 80
    .line 81
    move-result-object v2

    .line 82
    goto :goto_0

    .line 83
    :cond_3
    if-nez v1, :cond_4

    .line 84
    .line 85
    invoke-interface {p2, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 86
    .line 87
    .line 88
    move-result-object v1

    .line 89
    :cond_4
    const/4 v0, 0x1

    .line 90
    const/4 v3, 0x0

    .line 91
    const v5, 0x495f3e00    # 914400.0f

    .line 92
    .line 93
    .line 94
    const/high16 v6, 0x42c00000    # 96.0f

    .line 95
    .line 96
    const-string v7, "relativeFrom"

    .line 97
    .line 98
    const-string v8, "posOffset"

    .line 99
    .line 100
    const-string v9, "align"

    .line 101
    .line 102
    if-eqz v1, :cond_7

    .line 103
    .line 104
    invoke-interface {v1, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object v10

    .line 108
    invoke-direct {p0, v10}, Lcom/intsig/office/fc/doc/DOCXReader;->getRelative(Ljava/lang/String;)B

    .line 109
    .line 110
    .line 111
    move-result v10

    .line 112
    invoke-virtual {p1, v10}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalRelativeTo(B)V

    .line 113
    .line 114
    .line 115
    invoke-interface {v1, v9}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 116
    .line 117
    .line 118
    move-result-object v10

    .line 119
    if-eqz v10, :cond_5

    .line 120
    .line 121
    invoke-interface {v1, v9}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 122
    .line 123
    .line 124
    move-result-object v1

    .line 125
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v1

    .line 129
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/doc/DOCXReader;->getAlign(Ljava/lang/String;)B

    .line 130
    .line 131
    .line 132
    move-result v1

    .line 133
    invoke-virtual {p1, v1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorizontalAlignment(B)V

    .line 134
    .line 135
    .line 136
    goto :goto_1

    .line 137
    :cond_5
    invoke-interface {v1, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 138
    .line 139
    .line 140
    move-result-object v10

    .line 141
    if-eqz v10, :cond_6

    .line 142
    .line 143
    invoke-interface {v1, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 144
    .line 145
    .line 146
    move-result-object v1

    .line 147
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 148
    .line 149
    .line 150
    move-result-object v1

    .line 151
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 152
    .line 153
    .line 154
    move-result v1

    .line 155
    int-to-float v1, v1

    .line 156
    mul-float v1, v1, v6

    .line 157
    .line 158
    div-float/2addr v1, v5

    .line 159
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 160
    .line 161
    .line 162
    move-result v1

    .line 163
    invoke-virtual {p3, v1, v3}, Lcom/intsig/office/java/awt/Rectangle;->translate(II)V

    .line 164
    .line 165
    .line 166
    goto :goto_1

    .line 167
    :cond_6
    const-string v10, "pctPosHOffset"

    .line 168
    .line 169
    invoke-interface {v1, v10}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 170
    .line 171
    .line 172
    move-result-object v11

    .line 173
    if-eqz v11, :cond_7

    .line 174
    .line 175
    invoke-interface {v1, v10}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 176
    .line 177
    .line 178
    move-result-object v1

    .line 179
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 180
    .line 181
    .line 182
    move-result-object v1

    .line 183
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 184
    .line 185
    .line 186
    move-result v1

    .line 187
    div-int/lit8 v1, v1, 0x64

    .line 188
    .line 189
    invoke-virtual {p1, v1}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorRelativeValue(I)V

    .line 190
    .line 191
    .line 192
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/shape/WPAbstractShape;->setHorPositionType(B)V

    .line 193
    .line 194
    .line 195
    :cond_7
    :goto_1
    if-nez v2, :cond_8

    .line 196
    .line 197
    invoke-interface {p2, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 198
    .line 199
    .line 200
    move-result-object v2

    .line 201
    :cond_8
    if-eqz v2, :cond_b

    .line 202
    .line 203
    invoke-interface {v2, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 204
    .line 205
    .line 206
    move-result-object p2

    .line 207
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->getRelative(Ljava/lang/String;)B

    .line 208
    .line 209
    .line 210
    move-result p2

    .line 211
    invoke-virtual {p1, p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalRelativeTo(B)V

    .line 212
    .line 213
    .line 214
    invoke-interface {v2, v9}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 215
    .line 216
    .line 217
    move-result-object p2

    .line 218
    if-eqz p2, :cond_9

    .line 219
    .line 220
    invoke-interface {v2, v9}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 221
    .line 222
    .line 223
    move-result-object p2

    .line 224
    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 225
    .line 226
    .line 227
    move-result-object p2

    .line 228
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->getAlign(Ljava/lang/String;)B

    .line 229
    .line 230
    .line 231
    move-result p2

    .line 232
    invoke-virtual {p1, p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerticalAlignment(B)V

    .line 233
    .line 234
    .line 235
    goto :goto_2

    .line 236
    :cond_9
    invoke-interface {v2, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 237
    .line 238
    .line 239
    move-result-object p2

    .line 240
    if-eqz p2, :cond_a

    .line 241
    .line 242
    invoke-interface {v2, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 243
    .line 244
    .line 245
    move-result-object p1

    .line 246
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 247
    .line 248
    .line 249
    move-result-object p1

    .line 250
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 251
    .line 252
    .line 253
    move-result p1

    .line 254
    int-to-float p1, p1

    .line 255
    mul-float p1, p1, v6

    .line 256
    .line 257
    div-float/2addr p1, v5

    .line 258
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    .line 259
    .line 260
    .line 261
    move-result p1

    .line 262
    invoke-virtual {p3, v3, p1}, Lcom/intsig/office/java/awt/Rectangle;->translate(II)V

    .line 263
    .line 264
    .line 265
    goto :goto_2

    .line 266
    :cond_a
    const-string p2, "pctPosVOffset"

    .line 267
    .line 268
    invoke-interface {v2, p2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 269
    .line 270
    .line 271
    move-result-object p3

    .line 272
    if-eqz p3, :cond_b

    .line 273
    .line 274
    invoke-interface {v2, p2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 275
    .line 276
    .line 277
    move-result-object p2

    .line 278
    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 279
    .line 280
    .line 281
    move-result-object p2

    .line 282
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 283
    .line 284
    .line 285
    move-result p2

    .line 286
    div-int/lit8 p2, p2, 0x64

    .line 287
    .line 288
    invoke-virtual {p1, p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerRelativeValue(I)V

    .line 289
    .line 290
    .line 291
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/shape/WPAbstractShape;->setVerPositionType(B)V

    .line 292
    .line 293
    .line 294
    :cond_b
    :goto_2
    return-void
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private readGradient(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/Element;B)Lcom/intsig/office/common/bg/Gradient;
    .locals 11

    .line 1
    const-string v0, "focus"

    .line 2
    .line 3
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, ""

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    const-string v3, "%"

    .line 13
    .line 14
    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v0, 0x0

    .line 24
    :goto_0
    const-string v3, "angle"

    .line 25
    .line 26
    invoke-interface {p2, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    if-eqz v3, :cond_1

    .line 31
    .line 32
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    goto :goto_1

    .line 37
    :cond_1
    const/4 v3, 0x0

    .line 38
    :goto_1
    const/16 v4, -0x87

    .line 39
    .line 40
    if-eq v3, v4, :cond_4

    .line 41
    .line 42
    const/16 v4, -0x5a

    .line 43
    .line 44
    if-eq v3, v4, :cond_3

    .line 45
    .line 46
    const/16 v4, -0x2d

    .line 47
    .line 48
    if-eq v3, v4, :cond_2

    .line 49
    .line 50
    if-eqz v3, :cond_3

    .line 51
    .line 52
    goto :goto_2

    .line 53
    :cond_2
    const/16 v3, 0x87

    .line 54
    .line 55
    goto :goto_2

    .line 56
    :cond_3
    add-int/lit8 v3, v3, 0x5a

    .line 57
    .line 58
    goto :goto_2

    .line 59
    :cond_4
    const/16 v3, 0x2d

    .line 60
    .line 61
    :goto_2
    const-string v4, "colors"

    .line 62
    .line 63
    invoke-interface {p2, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v4

    .line 67
    const/4 v5, 0x1

    .line 68
    if-eqz v4, :cond_6

    .line 69
    .line 70
    const-string p1, ";"

    .line 71
    .line 72
    invoke-virtual {v4, p1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    array-length v1, p1

    .line 77
    new-array v4, v1, [I

    .line 78
    .line 79
    new-array v6, v1, [F

    .line 80
    .line 81
    const/4 v7, 0x0

    .line 82
    :goto_3
    if-ge v7, v1, :cond_9

    .line 83
    .line 84
    aget-object v8, p1, v7

    .line 85
    .line 86
    const-string v9, " "

    .line 87
    .line 88
    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 89
    .line 90
    .line 91
    move-result v8

    .line 92
    aget-object v9, p1, v7

    .line 93
    .line 94
    invoke-virtual {v9, v2, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v9

    .line 98
    const-string v10, "f"

    .line 99
    .line 100
    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 101
    .line 102
    .line 103
    move-result v10

    .line 104
    if-eqz v10, :cond_5

    .line 105
    .line 106
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 107
    .line 108
    .line 109
    move-result v9

    .line 110
    const v10, 0x47c35000    # 100000.0f

    .line 111
    .line 112
    .line 113
    div-float/2addr v9, v10

    .line 114
    aput v9, v6, v7

    .line 115
    .line 116
    goto :goto_4

    .line 117
    :cond_5
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 118
    .line 119
    .line 120
    move-result v9

    .line 121
    aput v9, v6, v7

    .line 122
    .line 123
    :goto_4
    aget-object v9, p1, v7

    .line 124
    .line 125
    add-int/lit8 v8, v8, 0x1

    .line 126
    .line 127
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    .line 128
    .line 129
    .line 130
    move-result v10

    .line 131
    invoke-virtual {v9, v8, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 132
    .line 133
    .line 134
    move-result-object v8

    .line 135
    invoke-direct {p0, v8, v5}, Lcom/intsig/office/fc/doc/DOCXReader;->getColor(Ljava/lang/String;Z)I

    .line 136
    .line 137
    .line 138
    move-result v8

    .line 139
    aput v8, v4, v7

    .line 140
    .line 141
    add-int/lit8 v7, v7, 0x1

    .line 142
    .line 143
    goto :goto_3

    .line 144
    :cond_6
    const-string v4, "fillcolor"

    .line 145
    .line 146
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 147
    .line 148
    .line 149
    move-result-object p1

    .line 150
    invoke-direct {p0, p1, v5}, Lcom/intsig/office/fc/doc/DOCXReader;->getColor(Ljava/lang/String;Z)I

    .line 151
    .line 152
    .line 153
    move-result p1

    .line 154
    const-string v4, "color2"

    .line 155
    .line 156
    invoke-interface {p2, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object v4

    .line 160
    if-eqz v4, :cond_8

    .line 161
    .line 162
    const-string v6, "fill "

    .line 163
    .line 164
    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 165
    .line 166
    .line 167
    move-result-object v1

    .line 168
    const-string v4, "("

    .line 169
    .line 170
    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 171
    .line 172
    .line 173
    move-result v4

    .line 174
    const-string v6, ")"

    .line 175
    .line 176
    invoke-virtual {v1, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 177
    .line 178
    .line 179
    move-result v6

    .line 180
    if-ltz v4, :cond_7

    .line 181
    .line 182
    if-ltz v6, :cond_7

    .line 183
    .line 184
    add-int/2addr v4, v5

    .line 185
    invoke-virtual {v1, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    :cond_7
    invoke-direct {p0, v1, v5}, Lcom/intsig/office/fc/doc/DOCXReader;->getColor(Ljava/lang/String;Z)I

    .line 189
    .line 190
    .line 191
    move-result v1

    .line 192
    goto :goto_5

    .line 193
    :cond_8
    const/4 v1, 0x0

    .line 194
    :goto_5
    const/4 v4, 0x2

    .line 195
    new-array v6, v4, [I

    .line 196
    .line 197
    aput p1, v6, v2

    .line 198
    .line 199
    aput v1, v6, v5

    .line 200
    .line 201
    new-array p1, v4, [F

    .line 202
    .line 203
    fill-array-data p1, :array_0

    .line 204
    .line 205
    .line 206
    move-object v4, v6

    .line 207
    move-object v6, p1

    .line 208
    :cond_9
    const/4 p1, 0x7

    .line 209
    if-ne p3, p1, :cond_a

    .line 210
    .line 211
    new-instance p1, Lcom/intsig/office/common/bg/LinearGradientShader;

    .line 212
    .line 213
    int-to-float p2, v3

    .line 214
    invoke-direct {p1, p2, v4, v6}, Lcom/intsig/office/common/bg/LinearGradientShader;-><init>(F[I[F)V

    .line 215
    .line 216
    .line 217
    goto :goto_6

    .line 218
    :cond_a
    const/4 p1, 0x4

    .line 219
    if-ne p3, p1, :cond_b

    .line 220
    .line 221
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->getRadialGradientPositionType(Lcom/intsig/office/fc/dom4j/Element;)I

    .line 222
    .line 223
    .line 224
    move-result p1

    .line 225
    new-instance p2, Lcom/intsig/office/common/bg/RadialGradientShader;

    .line 226
    .line 227
    invoke-direct {p2, p1, v4, v6}, Lcom/intsig/office/common/bg/RadialGradientShader;-><init>(I[I[F)V

    .line 228
    .line 229
    .line 230
    move-object p1, p2

    .line 231
    goto :goto_6

    .line 232
    :cond_b
    const/4 p1, 0x0

    .line 233
    :goto_6
    if-eqz p1, :cond_c

    .line 234
    .line 235
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/bg/Gradient;->setFocus(I)V

    .line 236
    .line 237
    .line 238
    :cond_c
    return-object p1

    .line 239
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private setShapeWrapType(Lcom/intsig/office/common/shape/WPGroupShape;S)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/GroupShape;->getShapes()[Lcom/intsig/office/common/shape/IShape;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    array-length v0, p1

    .line 6
    const/4 v1, 0x0

    .line 7
    :goto_0
    if-ge v1, v0, :cond_2

    .line 8
    .line 9
    aget-object v2, p1, v1

    .line 10
    .line 11
    instance-of v3, v2, Lcom/intsig/office/common/shape/WPAbstractShape;

    .line 12
    .line 13
    if-eqz v3, :cond_0

    .line 14
    .line 15
    check-cast v2, Lcom/intsig/office/common/shape/WPAbstractShape;

    .line 16
    .line 17
    invoke-virtual {v2, p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->setWrap(S)V

    .line 18
    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_0
    instance-of v3, v2, Lcom/intsig/office/common/shape/WPGroupShape;

    .line 22
    .line 23
    if-eqz v3, :cond_1

    .line 24
    .line 25
    check-cast v2, Lcom/intsig/office/common/shape/WPGroupShape;

    .line 26
    .line 27
    invoke-direct {p0, v2, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->setShapeWrapType(Lcom/intsig/office/common/shape/WPGroupShape;S)V

    .line 28
    .line 29
    .line 30
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_2
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/fc/doc/DOCXReader;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->offset:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/office/fc/doc/DOCXReader;)Lcom/intsig/office/wp/model/WPDocument;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/office/fc/doc/DOCXReader;Lcom/intsig/office/fc/dom4j/Element;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/doc/DOCXReader;->processParagraph(Lcom/intsig/office/fc/dom4j/Element;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/system/AbstractReader;->isReaderFinish()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_4

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->filePath:Ljava/lang/String;

    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleStrID:Ljava/util/Map;

    .line 17
    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->styleStrID:Ljava/util/Map;

    .line 24
    .line 25
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->tableGridCol:Ljava/util/Map;

    .line 26
    .line 27
    if-eqz v1, :cond_1

    .line 28
    .line 29
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 30
    .line 31
    .line 32
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->tableGridCol:Ljava/util/Map;

    .line 33
    .line 34
    :cond_1
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->tableGridCol:Ljava/util/Map;

    .line 35
    .line 36
    iput-object v0, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 37
    .line 38
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeType:Ljava/util/List;

    .line 39
    .line 40
    if-eqz v1, :cond_2

    .line 41
    .line 42
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 43
    .line 44
    .line 45
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeType:Ljava/util/List;

    .line 46
    .line 47
    :cond_2
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeValue:Ljava/util/Map;

    .line 48
    .line 49
    if-eqz v1, :cond_3

    .line 50
    .line 51
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 52
    .line 53
    .line 54
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->relativeValue:Ljava/util/Map;

    .line 55
    .line 56
    :cond_3
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->bulletNumbersID:Ljava/util/Hashtable;

    .line 57
    .line 58
    if-eqz v1, :cond_4

    .line 59
    .line 60
    invoke-virtual {v1}, Ljava/util/Hashtable;->clear()V

    .line 61
    .line 62
    .line 63
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->bulletNumbersID:Ljava/util/Hashtable;

    .line 64
    .line 65
    :cond_4
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getModel()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    new-instance v0, Lcom/intsig/office/wp/model/WPDocument;

    .line 7
    .line 8
    invoke-direct {v0}, Lcom/intsig/office/wp/model/WPDocument;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/office/fc/doc/DOCXReader;->openFile()V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->wpdoc:Lcom/intsig/office/wp/model/WPDocument;

    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
.end method

.method public processRotation(Lcom/intsig/office/common/shape/AutoShape;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AbstractShape;->getRotation()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AbstractShape;->getFlipHorizontal()Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    neg-float v0, v0

    .line 12
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AbstractShape;->getFlipVertical()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-eqz v1, :cond_1

    .line 17
    .line 18
    neg-float v0, v0

    .line 19
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    const/16 v2, 0x20

    .line 24
    .line 25
    if-eq v1, v2, :cond_2

    .line 26
    .line 27
    const/16 v2, 0x22

    .line 28
    .line 29
    if-eq v1, v2, :cond_2

    .line 30
    .line 31
    const/16 v2, 0x26

    .line 32
    .line 33
    if-ne v1, v2, :cond_4

    .line 34
    .line 35
    :cond_2
    const/high16 v1, 0x42340000    # 45.0f

    .line 36
    .line 37
    cmpl-float v1, v0, v1

    .line 38
    .line 39
    if-eqz v1, :cond_3

    .line 40
    .line 41
    const/high16 v1, 0x43070000    # 135.0f

    .line 42
    .line 43
    cmpl-float v1, v0, v1

    .line 44
    .line 45
    if-eqz v1, :cond_3

    .line 46
    .line 47
    const/high16 v1, 0x43610000    # 225.0f

    .line 48
    .line 49
    cmpl-float v1, v0, v1

    .line 50
    .line 51
    if-nez v1, :cond_4

    .line 52
    .line 53
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AbstractShape;->getFlipHorizontal()Z

    .line 54
    .line 55
    .line 56
    move-result v1

    .line 57
    if-nez v1, :cond_4

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AbstractShape;->getFlipVertical()Z

    .line 60
    .line 61
    .line 62
    move-result v1

    .line 63
    if-nez v1, :cond_4

    .line 64
    .line 65
    const/high16 v1, 0x42b40000    # 90.0f

    .line 66
    .line 67
    sub-float/2addr v0, v1

    .line 68
    :cond_4
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setRotation(F)V

    .line 69
    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public searchContent(Ljava/io/File;Ljava/lang/String;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    new-instance p1, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/doc/DOCXReader;->filePath:Ljava/lang/String;

    .line 4
    .line 5
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iput-object p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 9
    .line 10
    const-string v0, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

    .line 11
    .line 12
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    const/4 v0, 0x0

    .line 17
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationship(I)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 22
    .line 23
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    iput-object p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 28
    .line 29
    new-instance p1, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 30
    .line 31
    invoke-direct {p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 32
    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 35
    .line 36
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    const-string v2, "body"

    .line 49
    .line 50
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    new-instance v2, Ljava/lang/StringBuilder;

    .line 55
    .line 56
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    .line 58
    .line 59
    if-eqz p1, :cond_3

    .line 60
    .line 61
    const-string v3, "p"

    .line 62
    .line 63
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 72
    .line 73
    .line 74
    move-result v3

    .line 75
    if-eqz v3, :cond_3

    .line 76
    .line 77
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 78
    .line 79
    .line 80
    move-result-object v3

    .line 81
    check-cast v3, Lcom/intsig/office/fc/dom4j/Element;

    .line 82
    .line 83
    const-string v4, "r"

    .line 84
    .line 85
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 86
    .line 87
    .line 88
    move-result-object v3

    .line 89
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 90
    .line 91
    .line 92
    move-result-object v3

    .line 93
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 94
    .line 95
    .line 96
    move-result v4

    .line 97
    if-eqz v4, :cond_1

    .line 98
    .line 99
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 100
    .line 101
    .line 102
    move-result-object v4

    .line 103
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 104
    .line 105
    const-string v5, "t"

    .line 106
    .line 107
    invoke-interface {v4, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 108
    .line 109
    .line 110
    move-result-object v4

    .line 111
    if-nez v4, :cond_0

    .line 112
    .line 113
    goto :goto_1

    .line 114
    :cond_0
    invoke-interface {v4}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v4

    .line 118
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    goto :goto_1

    .line 122
    :cond_1
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    .line 123
    .line 124
    .line 125
    move-result v3

    .line 126
    if-ltz v3, :cond_2

    .line 127
    .line 128
    const/4 v0, 0x1

    .line 129
    goto :goto_2

    .line 130
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    .line 131
    .line 132
    .line 133
    move-result v3

    .line 134
    invoke-virtual {v2, v0, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    goto :goto_0

    .line 138
    :cond_3
    :goto_2
    const/4 p1, 0x0

    .line 139
    iput-object p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 140
    .line 141
    iput-object p1, p0, Lcom/intsig/office/fc/doc/DOCXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 142
    .line 143
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 144
    .line 145
    .line 146
    return v0
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

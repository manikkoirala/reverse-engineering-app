.class public Lcom/intsig/office/fc/doc/VMLPathParser;
.super Ljava/lang/Object;
.source "VMLPathParser.java"


# static fields
.field public static final Command_AngleEllipse:B = 0xbt

.field public static final Command_AngleEllipseTo:B = 0xat

.field public static final Command_Arc:B = 0xdt

.field public static final Command_ArcTo:B = 0xct

.field public static final Command_ClockwiseArc:B = 0xft

.field public static final Command_ClockwiseArcTo:B = 0xet

.field public static final Command_Close:B = 0x3t

.field public static final Command_CurveTo:B = 0x2t

.field private static final Command_EllipticalQaudrantX:B = 0x10t

.field private static final Command_EllipticalQaudrantY:B = 0x11t

.field public static final Command_End:B = 0x4t

.field public static final Command_Invalid:B = -0x1t

.field public static final Command_LineTo:B = 0x1t

.field public static final Command_MoveTo:B = 0x0t

.field public static final Command_NoFill:B = 0x8t

.field public static final Command_NoStroke:B = 0x9t

.field private static final Command_QuadraticBezier:B = 0x12t

.field public static final Command_RCurveTo:B = 0x7t

.field public static final Command_RLineTo:B = 0x6t

.field public static final Command_RMoveTo:B = 0x5t

.field private static NodeType_End:B

.field private static NodeType_Invalidate:B

.field private static NodeType_Middle:B

.field private static NodeType_Start:B

.field private static instance:Lcom/intsig/office/fc/doc/VMLPathParser;


# instance fields
.field private builder:Ljava/lang/StringBuilder;

.field private ctrNode1:Landroid/graphics/PointF;

.field private ctrNode2:Landroid/graphics/PointF;

.field private currentNodeType:B

.field endArrowPath:Landroid/graphics/Path;

.field private index:I

.field private nextNode:Landroid/graphics/PointF;

.field private paraList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private preNode:Landroid/graphics/PointF;

.field private preNodeType:B

.field startArrowPath:Landroid/graphics/Path;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/doc/VMLPathParser;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/doc/VMLPathParser;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/doc/VMLPathParser;->instance:Lcom/intsig/office/fc/doc/VMLPathParser;

    .line 7
    .line 8
    const/4 v0, -0x1

    .line 9
    sput-byte v0, Lcom/intsig/office/fc/doc/VMLPathParser;->NodeType_Invalidate:B

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    sput-byte v0, Lcom/intsig/office/fc/doc/VMLPathParser;->NodeType_Start:B

    .line 13
    .line 14
    const/4 v0, 0x1

    .line 15
    sput-byte v0, Lcom/intsig/office/fc/doc/VMLPathParser;->NodeType_Middle:B

    .line 16
    .line 17
    const/4 v0, 0x2

    .line 18
    sput-byte v0, Lcom/intsig/office/fc/doc/VMLPathParser;->NodeType_End:B

    .line 19
    .line 20
    return-void
    .line 21
.end method

.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-byte v0, Lcom/intsig/office/fc/doc/VMLPathParser;->NodeType_Invalidate:B

    .line 5
    .line 6
    iput-byte v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->currentNodeType:B

    .line 7
    .line 8
    iput-byte v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->preNodeType:B

    .line 9
    .line 10
    new-instance v0, Landroid/graphics/PointF;

    .line 11
    .line 12
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->preNode:Landroid/graphics/PointF;

    .line 16
    .line 17
    new-instance v0, Landroid/graphics/PointF;

    .line 18
    .line 19
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 20
    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->ctrNode1:Landroid/graphics/PointF;

    .line 23
    .line 24
    new-instance v0, Landroid/graphics/PointF;

    .line 25
    .line 26
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 27
    .line 28
    .line 29
    iput-object v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->ctrNode2:Landroid/graphics/PointF;

    .line 30
    .line 31
    new-instance v0, Landroid/graphics/PointF;

    .line 32
    .line 33
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 34
    .line 35
    .line 36
    iput-object v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 37
    .line 38
    const/4 v0, 0x0

    .line 39
    iput-object v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->startArrowPath:Landroid/graphics/Path;

    .line 40
    .line 41
    iput-object v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->endArrowPath:Landroid/graphics/Path;

    .line 42
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    iput-object v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->builder:Ljava/lang/StringBuilder;

    .line 49
    .line 50
    new-instance v0, Ljava/util/ArrayList;

    .line 51
    .line 52
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 53
    .line 54
    .line 55
    iput-object v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->paraList:Ljava/util/List;

    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private hasNextPoint(Ljava/lang/String;)Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-ge v0, v1, :cond_0

    .line 8
    .line 9
    iget v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 10
    .line 11
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    invoke-static {p1}, Ljava/lang/Character;->isLetter(C)Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-nez p1, :cond_0

    .line 20
    .line 21
    const/4 p1, 0x1

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 p1, 0x0

    .line 24
    :goto_0
    return p1
.end method

.method public static instance()Lcom/intsig/office/fc/doc/VMLPathParser;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/doc/VMLPathParser;->instance:Lcom/intsig/office/fc/doc/VMLPathParser;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private nextCommand(Ljava/lang/String;)B
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->builder:Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    :goto_0
    iget v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 12
    .line 13
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-ge v0, v1, :cond_0

    .line 18
    .line 19
    iget v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 20
    .line 21
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_0

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->builder:Ljava/lang/StringBuilder;

    .line 32
    .line 33
    iget v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 34
    .line 35
    add-int/lit8 v3, v1, 0x1

    .line 36
    .line 37
    iput v3, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 38
    .line 39
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->builder:Ljava/lang/StringBuilder;

    .line 48
    .line 49
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    const-string v0, "h"

    .line 54
    .line 55
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    const/4 v1, 0x2

    .line 60
    if-eqz v0, :cond_1

    .line 61
    .line 62
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    :cond_1
    const-string v0, "m"

    .line 67
    .line 68
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    if-eqz v0, :cond_2

    .line 73
    .line 74
    return v2

    .line 75
    :cond_2
    const-string v0, "l"

    .line 76
    .line 77
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    const/4 v2, 0x1

    .line 82
    if-eqz v0, :cond_3

    .line 83
    .line 84
    return v2

    .line 85
    :cond_3
    const-string v0, "c"

    .line 86
    .line 87
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 88
    .line 89
    .line 90
    move-result v0

    .line 91
    if-eqz v0, :cond_4

    .line 92
    .line 93
    return v1

    .line 94
    :cond_4
    const-string v0, "x"

    .line 95
    .line 96
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 97
    .line 98
    .line 99
    move-result v1

    .line 100
    const/4 v3, 0x3

    .line 101
    if-eqz v1, :cond_5

    .line 102
    .line 103
    return v3

    .line 104
    :cond_5
    const-string v1, "e"

    .line 105
    .line 106
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 107
    .line 108
    .line 109
    move-result v1

    .line 110
    if-eqz v1, :cond_6

    .line 111
    .line 112
    const/4 p1, 0x4

    .line 113
    return p1

    .line 114
    :cond_6
    const-string v1, "t"

    .line 115
    .line 116
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 117
    .line 118
    .line 119
    move-result v1

    .line 120
    if-eqz v1, :cond_7

    .line 121
    .line 122
    const/4 p1, 0x5

    .line 123
    return p1

    .line 124
    :cond_7
    const-string v1, "r"

    .line 125
    .line 126
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 127
    .line 128
    .line 129
    move-result v1

    .line 130
    if-eqz v1, :cond_8

    .line 131
    .line 132
    const/4 p1, 0x6

    .line 133
    return p1

    .line 134
    :cond_8
    const-string v1, "v"

    .line 135
    .line 136
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 137
    .line 138
    .line 139
    move-result v1

    .line 140
    if-eqz v1, :cond_9

    .line 141
    .line 142
    const/4 p1, 0x7

    .line 143
    return p1

    .line 144
    :cond_9
    const-string v1, "nf"

    .line 145
    .line 146
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 147
    .line 148
    .line 149
    move-result v1

    .line 150
    if-eqz v1, :cond_a

    .line 151
    .line 152
    const/16 p1, 0x8

    .line 153
    .line 154
    return p1

    .line 155
    :cond_a
    const-string v1, "ns"

    .line 156
    .line 157
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 158
    .line 159
    .line 160
    move-result v1

    .line 161
    if-eqz v1, :cond_b

    .line 162
    .line 163
    const/16 p1, 0x9

    .line 164
    .line 165
    return p1

    .line 166
    :cond_b
    const-string v1, "ae"

    .line 167
    .line 168
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 169
    .line 170
    .line 171
    move-result v1

    .line 172
    if-eqz v1, :cond_c

    .line 173
    .line 174
    const/16 p1, 0xa

    .line 175
    .line 176
    return p1

    .line 177
    :cond_c
    const-string v1, "al"

    .line 178
    .line 179
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 180
    .line 181
    .line 182
    move-result v1

    .line 183
    if-eqz v1, :cond_d

    .line 184
    .line 185
    const/16 p1, 0xb

    .line 186
    .line 187
    return p1

    .line 188
    :cond_d
    const-string v1, "at"

    .line 189
    .line 190
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 191
    .line 192
    .line 193
    move-result v1

    .line 194
    if-eqz v1, :cond_e

    .line 195
    .line 196
    const/16 p1, 0xc

    .line 197
    .line 198
    return p1

    .line 199
    :cond_e
    const-string v1, "ar"

    .line 200
    .line 201
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 202
    .line 203
    .line 204
    move-result v1

    .line 205
    if-eqz v1, :cond_f

    .line 206
    .line 207
    const/16 p1, 0xd

    .line 208
    .line 209
    return p1

    .line 210
    :cond_f
    const-string v1, "wa"

    .line 211
    .line 212
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 213
    .line 214
    .line 215
    move-result v1

    .line 216
    if-eqz v1, :cond_10

    .line 217
    .line 218
    const/16 p1, 0xe

    .line 219
    .line 220
    return p1

    .line 221
    :cond_10
    const-string v1, "wr"

    .line 222
    .line 223
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 224
    .line 225
    .line 226
    move-result v1

    .line 227
    if-eqz v1, :cond_11

    .line 228
    .line 229
    const/16 p1, 0xf

    .line 230
    .line 231
    return p1

    .line 232
    :cond_11
    const-string v1, "qx"

    .line 233
    .line 234
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 235
    .line 236
    .line 237
    move-result v1

    .line 238
    if-eqz v1, :cond_12

    .line 239
    .line 240
    const/16 p1, 0x10

    .line 241
    .line 242
    return p1

    .line 243
    :cond_12
    const-string v1, "qy"

    .line 244
    .line 245
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 246
    .line 247
    .line 248
    move-result v1

    .line 249
    if-eqz v1, :cond_13

    .line 250
    .line 251
    const/16 p1, 0x11

    .line 252
    .line 253
    return p1

    .line 254
    :cond_13
    const-string v1, "qb"

    .line 255
    .line 256
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 257
    .line 258
    .line 259
    move-result v1

    .line 260
    if-eqz v1, :cond_14

    .line 261
    .line 262
    const/16 p1, 0x12

    .line 263
    .line 264
    return p1

    .line 265
    :cond_14
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 266
    .line 267
    .line 268
    move-result v0

    .line 269
    if-nez v0, :cond_16

    .line 270
    .line 271
    const-string v0, "X"

    .line 272
    .line 273
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 274
    .line 275
    .line 276
    move-result v0

    .line 277
    if-eqz v0, :cond_15

    .line 278
    .line 279
    goto :goto_1

    .line 280
    :cond_15
    const/4 p1, -0x1

    .line 281
    return p1

    .line 282
    :cond_16
    :goto_1
    iget v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 283
    .line 284
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 285
    .line 286
    .line 287
    move-result p1

    .line 288
    sub-int/2addr p1, v2

    .line 289
    sub-int/2addr v0, p1

    .line 290
    iput v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 291
    .line 292
    return v3
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method private nextParameters(Ljava/lang/String;)[Ljava/lang/Integer;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->paraList:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    :goto_0
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/doc/VMLPathParser;->hasNextPoint(Ljava/lang/String;)Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/doc/VMLPathParser;->nextPoint(Ljava/lang/String;)[I

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->paraList:Ljava/util/List;

    .line 17
    .line 18
    const/4 v2, 0x0

    .line 19
    aget v2, v0, v2

    .line 20
    .line 21
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->paraList:Ljava/util/List;

    .line 29
    .line 30
    const/4 v2, 0x1

    .line 31
    aget v0, v0, v2

    .line 32
    .line 33
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->paraList:Ljava/util/List;

    .line 42
    .line 43
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    new-array v0, v0, [Ljava/lang/Integer;

    .line 48
    .line 49
    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    check-cast p1, [Ljava/lang/Integer;

    .line 54
    .line 55
    return-object p1
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private nextPoint(Ljava/lang/String;)[I
    .locals 7

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [I

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->builder:Ljava/lang/StringBuilder;

    .line 5
    .line 6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    const/4 v3, 0x0

    .line 11
    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    :goto_0
    iget v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 15
    .line 16
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    const/16 v4, 0x2d

    .line 21
    .line 22
    if-ge v1, v2, :cond_1

    .line 23
    .line 24
    iget v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 25
    .line 26
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    if-nez v1, :cond_0

    .line 35
    .line 36
    iget v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 37
    .line 38
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    if-ne v1, v4, :cond_1

    .line 43
    .line 44
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->builder:Ljava/lang/StringBuilder;

    .line 45
    .line 46
    iget v2, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 47
    .line 48
    add-int/lit8 v4, v2, 0x1

    .line 49
    .line 50
    iput v4, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 51
    .line 52
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->builder:Ljava/lang/StringBuilder;

    .line 61
    .line 62
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    if-lez v1, :cond_2

    .line 67
    .line 68
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->builder:Ljava/lang/StringBuilder;

    .line 69
    .line 70
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 75
    .line 76
    .line 77
    move-result v1

    .line 78
    aput v1, v0, v3

    .line 79
    .line 80
    :cond_2
    iget v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 81
    .line 82
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 83
    .line 84
    .line 85
    move-result v2

    .line 86
    if-ge v1, v2, :cond_6

    .line 87
    .line 88
    iget v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 89
    .line 90
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    .line 91
    .line 92
    .line 93
    move-result v1

    .line 94
    const/16 v2, 0x2c

    .line 95
    .line 96
    if-ne v1, v2, :cond_6

    .line 97
    .line 98
    iget v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 99
    .line 100
    const/4 v5, 0x1

    .line 101
    add-int/2addr v1, v5

    .line 102
    iput v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 103
    .line 104
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->builder:Ljava/lang/StringBuilder;

    .line 105
    .line 106
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    .line 107
    .line 108
    .line 109
    move-result v6

    .line 110
    invoke-virtual {v1, v3, v6}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    :goto_1
    iget v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 114
    .line 115
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 116
    .line 117
    .line 118
    move-result v3

    .line 119
    if-ge v1, v3, :cond_4

    .line 120
    .line 121
    iget v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 122
    .line 123
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    .line 124
    .line 125
    .line 126
    move-result v1

    .line 127
    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    .line 128
    .line 129
    .line 130
    move-result v1

    .line 131
    if-nez v1, :cond_3

    .line 132
    .line 133
    iget v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 134
    .line 135
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    .line 136
    .line 137
    .line 138
    move-result v1

    .line 139
    if-ne v1, v4, :cond_4

    .line 140
    .line 141
    :cond_3
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->builder:Ljava/lang/StringBuilder;

    .line 142
    .line 143
    iget v3, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 144
    .line 145
    add-int/lit8 v6, v3, 0x1

    .line 146
    .line 147
    iput v6, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 148
    .line 149
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    .line 150
    .line 151
    .line 152
    move-result v3

    .line 153
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 154
    .line 155
    .line 156
    goto :goto_1

    .line 157
    :cond_4
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->builder:Ljava/lang/StringBuilder;

    .line 158
    .line 159
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    .line 160
    .line 161
    .line 162
    move-result v1

    .line 163
    if-lez v1, :cond_5

    .line 164
    .line 165
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->builder:Ljava/lang/StringBuilder;

    .line 166
    .line 167
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 168
    .line 169
    .line 170
    move-result-object v1

    .line 171
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 172
    .line 173
    .line 174
    move-result v1

    .line 175
    aput v1, v0, v5

    .line 176
    .line 177
    :cond_5
    iget v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 178
    .line 179
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 180
    .line 181
    .line 182
    move-result v3

    .line 183
    if-ge v1, v3, :cond_6

    .line 184
    .line 185
    iget v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 186
    .line 187
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    .line 188
    .line 189
    .line 190
    move-result p1

    .line 191
    if-ne p1, v2, :cond_6

    .line 192
    .line 193
    iget p1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 194
    .line 195
    add-int/2addr p1, v5

    .line 196
    iput p1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 197
    .line 198
    :cond_6
    return-object v0
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private processCommand_CurveTo(Landroid/graphics/Path;[Ljava/lang/Integer;)V
    .locals 13

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    array-length v1, p2

    .line 3
    add-int/lit8 v1, v1, -0x5

    .line 4
    .line 5
    if-ge v0, v1, :cond_0

    .line 6
    .line 7
    aget-object v1, p2, v0

    .line 8
    .line 9
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    int-to-float v3, v1

    .line 14
    add-int/lit8 v1, v0, 0x1

    .line 15
    .line 16
    aget-object v2, p2, v1

    .line 17
    .line 18
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    int-to-float v4, v2

    .line 23
    add-int/lit8 v9, v0, 0x2

    .line 24
    .line 25
    aget-object v2, p2, v9

    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    int-to-float v5, v2

    .line 32
    add-int/lit8 v10, v0, 0x3

    .line 33
    .line 34
    aget-object v2, p2, v10

    .line 35
    .line 36
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    int-to-float v6, v2

    .line 41
    add-int/lit8 v11, v0, 0x4

    .line 42
    .line 43
    aget-object v2, p2, v11

    .line 44
    .line 45
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    int-to-float v7, v2

    .line 50
    add-int/lit8 v12, v0, 0x5

    .line 51
    .line 52
    aget-object v2, p2, v12

    .line 53
    .line 54
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    int-to-float v8, v2

    .line 59
    move-object v2, p1

    .line 60
    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 61
    .line 62
    .line 63
    iget-object v2, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->preNode:Landroid/graphics/PointF;

    .line 64
    .line 65
    iget-object v3, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 66
    .line 67
    invoke-virtual {v2, v3}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 68
    .line 69
    .line 70
    iget-object v2, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->ctrNode1:Landroid/graphics/PointF;

    .line 71
    .line 72
    aget-object v3, p2, v0

    .line 73
    .line 74
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 75
    .line 76
    .line 77
    move-result v3

    .line 78
    int-to-float v3, v3

    .line 79
    aget-object v1, p2, v1

    .line 80
    .line 81
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 82
    .line 83
    .line 84
    move-result v1

    .line 85
    int-to-float v1, v1

    .line 86
    invoke-virtual {v2, v3, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 87
    .line 88
    .line 89
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->ctrNode2:Landroid/graphics/PointF;

    .line 90
    .line 91
    aget-object v2, p2, v9

    .line 92
    .line 93
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 94
    .line 95
    .line 96
    move-result v2

    .line 97
    int-to-float v2, v2

    .line 98
    aget-object v3, p2, v10

    .line 99
    .line 100
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 101
    .line 102
    .line 103
    move-result v3

    .line 104
    int-to-float v3, v3

    .line 105
    invoke-virtual {v1, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 106
    .line 107
    .line 108
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 109
    .line 110
    aget-object v2, p2, v11

    .line 111
    .line 112
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 113
    .line 114
    .line 115
    move-result v2

    .line 116
    int-to-float v2, v2

    .line 117
    aget-object v3, p2, v12

    .line 118
    .line 119
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 120
    .line 121
    .line 122
    move-result v3

    .line 123
    int-to-float v3, v3

    .line 124
    invoke-virtual {v1, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 125
    .line 126
    .line 127
    add-int/lit8 v0, v0, 0x6

    .line 128
    .line 129
    goto :goto_0

    .line 130
    :cond_0
    return-void
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processCommand_LineTo(Landroid/graphics/Path;[Ljava/lang/Integer;)V
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    array-length v1, p2

    .line 3
    add-int/lit8 v1, v1, -0x1

    .line 4
    .line 5
    if-ge v0, v1, :cond_0

    .line 6
    .line 7
    aget-object v1, p2, v0

    .line 8
    .line 9
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    int-to-float v1, v1

    .line 14
    add-int/lit8 v2, v0, 0x1

    .line 15
    .line 16
    aget-object v3, p2, v2

    .line 17
    .line 18
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    int-to-float v3, v3

    .line 23
    invoke-virtual {p1, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 24
    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->preNode:Landroid/graphics/PointF;

    .line 27
    .line 28
    iget-object v3, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 29
    .line 30
    invoke-virtual {v1, v3}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 31
    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 34
    .line 35
    aget-object v3, p2, v0

    .line 36
    .line 37
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    int-to-float v3, v3

    .line 42
    aget-object v2, p2, v2

    .line 43
    .line 44
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    int-to-float v2, v2

    .line 49
    invoke-virtual {v1, v3, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 50
    .line 51
    .line 52
    add-int/lit8 v0, v0, 0x2

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processCommand_MoveTo(Landroid/graphics/Path;[Ljava/lang/Integer;)V
    .locals 4

    .line 1
    array-length v0, p2

    .line 2
    const/4 v1, 0x2

    .line 3
    const/4 v2, 0x0

    .line 4
    const/4 v3, 0x1

    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    aget-object v0, p2, v2

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    aget-object p2, p2, v3

    .line 15
    .line 16
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 17
    .line 18
    .line 19
    move-result p2

    .line 20
    int-to-float p2, p2

    .line 21
    goto :goto_0

    .line 22
    :cond_0
    array-length v0, p2

    .line 23
    const/4 v1, 0x0

    .line 24
    if-ne v0, v3, :cond_1

    .line 25
    .line 26
    aget-object p2, p2, v2

    .line 27
    .line 28
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 29
    .line 30
    .line 31
    move-result p2

    .line 32
    int-to-float v0, p2

    .line 33
    const/4 p2, 0x0

    .line 34
    goto :goto_0

    .line 35
    :cond_1
    const/4 p2, 0x0

    .line 36
    const/4 v0, 0x0

    .line 37
    :goto_0
    invoke-virtual {p1, v0, p2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 38
    .line 39
    .line 40
    iget-object p1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 41
    .line 42
    invoke-virtual {p1, v0, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private processCommand_rCurveTo(Landroid/graphics/Path;[Ljava/lang/Integer;)V
    .locals 13

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    array-length v1, p2

    .line 3
    add-int/lit8 v1, v1, -0x5

    .line 4
    .line 5
    if-ge v0, v1, :cond_0

    .line 6
    .line 7
    aget-object v1, p2, v0

    .line 8
    .line 9
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    int-to-float v3, v1

    .line 14
    add-int/lit8 v1, v0, 0x1

    .line 15
    .line 16
    aget-object v2, p2, v1

    .line 17
    .line 18
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    int-to-float v4, v2

    .line 23
    add-int/lit8 v9, v0, 0x2

    .line 24
    .line 25
    aget-object v2, p2, v9

    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    int-to-float v5, v2

    .line 32
    add-int/lit8 v10, v0, 0x3

    .line 33
    .line 34
    aget-object v2, p2, v10

    .line 35
    .line 36
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    int-to-float v6, v2

    .line 41
    add-int/lit8 v11, v0, 0x4

    .line 42
    .line 43
    aget-object v2, p2, v11

    .line 44
    .line 45
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    int-to-float v7, v2

    .line 50
    add-int/lit8 v12, v0, 0x5

    .line 51
    .line 52
    aget-object v2, p2, v12

    .line 53
    .line 54
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    int-to-float v8, v2

    .line 59
    move-object v2, p1

    .line 60
    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 61
    .line 62
    .line 63
    iget-object v2, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->preNode:Landroid/graphics/PointF;

    .line 64
    .line 65
    iget-object v3, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 66
    .line 67
    invoke-virtual {v2, v3}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 68
    .line 69
    .line 70
    iget-object v2, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->ctrNode1:Landroid/graphics/PointF;

    .line 71
    .line 72
    aget-object v3, p2, v0

    .line 73
    .line 74
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 75
    .line 76
    .line 77
    move-result v3

    .line 78
    int-to-float v3, v3

    .line 79
    aget-object v1, p2, v1

    .line 80
    .line 81
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 82
    .line 83
    .line 84
    move-result v1

    .line 85
    int-to-float v1, v1

    .line 86
    invoke-virtual {v2, v3, v1}, Landroid/graphics/PointF;->offset(FF)V

    .line 87
    .line 88
    .line 89
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->ctrNode2:Landroid/graphics/PointF;

    .line 90
    .line 91
    aget-object v2, p2, v9

    .line 92
    .line 93
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 94
    .line 95
    .line 96
    move-result v2

    .line 97
    int-to-float v2, v2

    .line 98
    aget-object v3, p2, v10

    .line 99
    .line 100
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 101
    .line 102
    .line 103
    move-result v3

    .line 104
    int-to-float v3, v3

    .line 105
    invoke-virtual {v1, v2, v3}, Landroid/graphics/PointF;->offset(FF)V

    .line 106
    .line 107
    .line 108
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 109
    .line 110
    aget-object v2, p2, v11

    .line 111
    .line 112
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 113
    .line 114
    .line 115
    move-result v2

    .line 116
    int-to-float v2, v2

    .line 117
    aget-object v3, p2, v12

    .line 118
    .line 119
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 120
    .line 121
    .line 122
    move-result v3

    .line 123
    int-to-float v3, v3

    .line 124
    invoke-virtual {v1, v2, v3}, Landroid/graphics/PointF;->offset(FF)V

    .line 125
    .line 126
    .line 127
    add-int/lit8 v0, v0, 0x6

    .line 128
    .line 129
    goto :goto_0

    .line 130
    :cond_0
    return-void
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processCommand_rLineTo(Landroid/graphics/Path;[Ljava/lang/Integer;)V
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    array-length v1, p2

    .line 3
    add-int/lit8 v1, v1, -0x1

    .line 4
    .line 5
    if-ge v0, v1, :cond_0

    .line 6
    .line 7
    aget-object v1, p2, v0

    .line 8
    .line 9
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    int-to-float v1, v1

    .line 14
    add-int/lit8 v2, v0, 0x1

    .line 15
    .line 16
    aget-object v3, p2, v2

    .line 17
    .line 18
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    int-to-float v3, v3

    .line 23
    invoke-virtual {p1, v1, v3}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 24
    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->preNode:Landroid/graphics/PointF;

    .line 27
    .line 28
    iget-object v3, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 29
    .line 30
    invoke-virtual {v1, v3}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 31
    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 34
    .line 35
    aget-object v3, p2, v0

    .line 36
    .line 37
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    int-to-float v3, v3

    .line 42
    aget-object v2, p2, v2

    .line 43
    .line 44
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    int-to-float v2, v2

    .line 49
    invoke-virtual {v1, v3, v2}, Landroid/graphics/PointF;->offset(FF)V

    .line 50
    .line 51
    .line 52
    add-int/lit8 v0, v0, 0x2

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processCommand_rMoveTo(Landroid/graphics/Path;[Ljava/lang/Integer;)V
    .locals 4

    .line 1
    array-length v0, p2

    .line 2
    const/4 v1, 0x2

    .line 3
    const/4 v2, 0x1

    .line 4
    const/4 v3, 0x0

    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    aget-object v0, p2, v3

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    aget-object v1, p2, v2

    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    int-to-float v1, v1

    .line 21
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->rMoveTo(FF)V

    .line 22
    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->preNode:Landroid/graphics/PointF;

    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 27
    .line 28
    invoke-virtual {p1, v0}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 32
    .line 33
    aget-object v0, p2, v3

    .line 34
    .line 35
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    int-to-float v0, v0

    .line 40
    aget-object p2, p2, v2

    .line 41
    .line 42
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 43
    .line 44
    .line 45
    move-result p2

    .line 46
    int-to-float p2, p2

    .line 47
    invoke-virtual {p1, v0, p2}, Landroid/graphics/PointF;->offset(FF)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_0
    array-length v0, p2

    .line 52
    const/4 v1, 0x0

    .line 53
    if-ne v0, v2, :cond_1

    .line 54
    .line 55
    aget-object v0, p2, v3

    .line 56
    .line 57
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    int-to-float v0, v0

    .line 62
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->rMoveTo(FF)V

    .line 63
    .line 64
    .line 65
    iget-object p1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->preNode:Landroid/graphics/PointF;

    .line 66
    .line 67
    iget-object v0, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 68
    .line 69
    invoke-virtual {p1, v0}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 70
    .line 71
    .line 72
    iget-object p1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 73
    .line 74
    aget-object p2, p2, v3

    .line 75
    .line 76
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 77
    .line 78
    .line 79
    move-result p2

    .line 80
    int-to-float p2, p2

    .line 81
    invoke-virtual {p1, p2, v1}, Landroid/graphics/PointF;->offset(FF)V

    .line 82
    .line 83
    .line 84
    goto :goto_0

    .line 85
    :cond_1
    invoke-virtual {p1, v1, v1}, Landroid/graphics/Path;->rMoveTo(FF)V

    .line 86
    .line 87
    .line 88
    iget-object p1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->preNode:Landroid/graphics/PointF;

    .line 89
    .line 90
    iget-object p2, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 91
    .line 92
    invoke-virtual {p1, p2}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 93
    .line 94
    .line 95
    iget-object p1, p0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    .line 96
    .line 97
    invoke-virtual {p1, v1, v1}, Landroid/graphics/PointF;->offset(FF)V

    .line 98
    .line 99
    .line 100
    :goto_0
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private processPath(Lcom/intsig/office/common/shape/WPAutoShape;ILandroid/graphics/Path;B[Ljava/lang/Integer;)V
    .locals 24

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, p4

    move-object/from16 v3, p5

    .line 1
    iget-byte v4, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->preNodeType:B

    sget-byte v5, Lcom/intsig/office/fc/doc/VMLPathParser;->NodeType_Start:B

    const/4 v6, 0x7

    const/4 v7, 0x5

    const/4 v8, 0x3

    const/4 v9, 0x6

    const/4 v10, 0x2

    const/4 v11, 0x1

    if-ne v4, v5, :cond_4

    if-eqz p1, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getStartArrowhead()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x0

    if-eq v2, v11, :cond_3

    const/4 v5, 0x4

    if-eq v2, v10, :cond_2

    if-eq v2, v9, :cond_1

    if-eq v2, v6, :cond_0

    goto/16 :goto_0

    .line 2
    :cond_0
    aget-object v5, v3, v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v5, v5

    iget-object v13, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->x:F

    add-float v14, v5, v13

    aget-object v5, v3, v7

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v5, v5

    iget-object v13, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->y:F

    add-float v15, v5, v13

    aget-object v5, v3, v10

    .line 3
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v5, v5

    iget-object v13, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->x:F

    add-float v16, v5, v13

    aget-object v5, v3, v8

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v5, v5

    iget-object v13, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->y:F

    add-float v17, v5, v13

    aget-object v4, v3, v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    add-float v18, v4, v5

    aget-object v4, v3, v11

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v13, v5, Landroid/graphics/PointF;->y:F

    add-float v19, v4, v13

    iget v4, v5, Landroid/graphics/PointF;->x:F

    .line 4
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    move-result-object v22

    move/from16 v20, v4

    move/from16 v21, v13

    move/from16 v23, p2

    .line 5
    invoke-static/range {v14 .. v23}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getCubicBezArrowPath(FFFFFFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    move-result-object v4

    goto/16 :goto_1

    .line 6
    :cond_1
    aget-object v4, v3, v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    add-float v13, v4, v5

    aget-object v4, v3, v11

    .line 7
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v15, v5, Landroid/graphics/PointF;->y:F

    add-float v14, v4, v15

    iget v4, v5, Landroid/graphics/PointF;->x:F

    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    move-result-object v17

    move v5, v15

    move v15, v4

    move/from16 v16, v5

    move/from16 v18, p2

    .line 8
    invoke-static/range {v13 .. v18}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    move-result-object v4

    goto :goto_1

    .line 9
    :cond_2
    aget-object v5, v3, v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v13, v5

    aget-object v5, v3, v7

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v14, v5

    aget-object v5, v3, v10

    .line 10
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v15, v5

    aget-object v5, v3, v8

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v5, v5

    aget-object v4, v3, v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    aget-object v16, v3, v11

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v12

    int-to-float v12, v12

    iget-object v7, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v7, Landroid/graphics/PointF;->x:F

    iget v7, v7, Landroid/graphics/PointF;->y:F

    .line 11
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    move-result-object v21

    move/from16 v16, v5

    move/from16 v17, v4

    move/from16 v18, v12

    move/from16 v19, v8

    move/from16 v20, v7

    move/from16 v22, p2

    .line 12
    invoke-static/range {v13 .. v22}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getCubicBezArrowPath(FFFFFFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    move-result-object v4

    goto :goto_1

    .line 13
    :cond_3
    aget-object v4, v3, v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v12, v4

    aget-object v4, v3, v11

    .line 14
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v13, v4

    iget-object v4, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v14, v4, Landroid/graphics/PointF;->x:F

    iget v15, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getStartArrow()Lcom/intsig/office/common/shape/Arrow;

    move-result-object v16

    move/from16 v17, p2

    .line 15
    invoke-static/range {v12 .. v17}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    move-result-object v4

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v4, 0x0

    .line 16
    :goto_1
    iget-byte v5, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->currentNodeType:B

    sget-byte v7, Lcom/intsig/office/fc/doc/VMLPathParser;->NodeType_End:B

    if-ne v5, v7, :cond_d

    if-eqz p1, :cond_d

    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowhead()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 17
    array-length v5, v3

    if-eq v2, v11, :cond_b

    if-eq v2, v10, :cond_9

    if-eq v2, v9, :cond_7

    if-eq v2, v6, :cond_5

    goto/16 :goto_2

    :cond_5
    if-le v5, v9, :cond_6

    add-int/lit8 v7, v5, -0x8

    .line 18
    aget-object v7, v3, v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    add-float v12, v7, v8

    add-int/lit8 v7, v5, -0x7

    aget-object v7, v3, v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    add-float v13, v7, v8

    add-int/lit8 v7, v5, -0x6

    aget-object v7, v3, v7

    .line 19
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    add-float v14, v7, v8

    add-int/lit8 v7, v5, -0x5

    aget-object v7, v3, v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    add-float v15, v7, v8

    add-int/lit8 v7, v5, -0x4

    aget-object v7, v3, v7

    .line 20
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    add-float v16, v7, v8

    add-int/lit8 v7, v5, -0x3

    aget-object v7, v3, v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    add-float v17, v7, v8

    add-int/lit8 v7, v5, -0x2

    aget-object v7, v3, v7

    .line 21
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    add-float v18, v7, v8

    sub-int/2addr v5, v11

    aget-object v5, v3, v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v5, v5

    iget-object v7, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    add-float v19, v5, v7

    .line 22
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    move-result-object v20

    move/from16 v21, p2

    .line 23
    invoke-static/range {v12 .. v21}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getCubicBezArrowPath(FFFFFFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    move-result-object v12

    goto/16 :goto_3

    .line 24
    :cond_6
    iget-object v7, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v12, v7, Landroid/graphics/PointF;->x:F

    iget v13, v7, Landroid/graphics/PointF;->y:F

    add-int/lit8 v7, v5, -0x6

    aget-object v7, v3, v7

    .line 25
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    add-float v14, v7, v8

    add-int/lit8 v7, v5, -0x5

    aget-object v7, v3, v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    add-float v15, v7, v8

    add-int/lit8 v7, v5, -0x4

    aget-object v7, v3, v7

    .line 26
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    add-float v16, v7, v8

    add-int/lit8 v7, v5, -0x3

    aget-object v7, v3, v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    add-float v17, v7, v8

    add-int/lit8 v7, v5, -0x2

    aget-object v7, v3, v7

    .line 27
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    add-float v18, v7, v8

    sub-int/2addr v5, v11

    aget-object v5, v3, v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v5, v5

    iget-object v7, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    add-float v19, v5, v7

    .line 28
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    move-result-object v20

    move/from16 v21, p2

    .line 29
    invoke-static/range {v12 .. v21}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getCubicBezArrowPath(FFFFFFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    move-result-object v12

    goto/16 :goto_3

    :cond_7
    if-le v5, v10, :cond_8

    add-int/lit8 v7, v5, -0x4

    .line 30
    aget-object v7, v3, v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    add-float v12, v7, v8

    add-int/lit8 v7, v5, -0x3

    aget-object v7, v3, v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    add-float v13, v7, v8

    add-int/lit8 v7, v5, -0x2

    aget-object v7, v3, v7

    .line 31
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    add-float v14, v7, v8

    sub-int/2addr v5, v11

    aget-object v5, v3, v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v5, v5

    iget-object v7, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    add-float v15, v5, v7

    .line 32
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    move-result-object v16

    move/from16 v17, p2

    .line 33
    invoke-static/range {v12 .. v17}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    move-result-object v12

    goto/16 :goto_3

    .line 34
    :cond_8
    iget-object v7, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v12, v7, Landroid/graphics/PointF;->x:F

    iget v13, v7, Landroid/graphics/PointF;->y:F

    add-int/lit8 v7, v5, -0x2

    aget-object v7, v3, v7

    .line 35
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    add-float v14, v7, v8

    sub-int/2addr v5, v11

    aget-object v5, v3, v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v5, v5

    iget-object v7, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    add-float v15, v5, v7

    .line 36
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    move-result-object v16

    move/from16 v17, p2

    .line 37
    invoke-static/range {v12 .. v17}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    move-result-object v12

    goto/16 :goto_3

    :cond_9
    if-le v5, v9, :cond_a

    add-int/lit8 v7, v5, -0x8

    .line 38
    aget-object v7, v3, v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v12, v7

    add-int/lit8 v7, v5, -0x7

    aget-object v7, v3, v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v13, v7

    add-int/lit8 v7, v5, -0x6

    aget-object v7, v3, v7

    .line 39
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v14, v7

    add-int/lit8 v7, v5, -0x5

    aget-object v7, v3, v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v15, v7

    add-int/lit8 v7, v5, -0x4

    aget-object v7, v3, v7

    .line 40
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    add-int/lit8 v8, v5, -0x3

    aget-object v8, v3, v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    int-to-float v8, v8

    add-int/lit8 v16, v5, -0x2

    aget-object v16, v3, v16

    .line 41
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v6, v6

    sub-int/2addr v5, v11

    aget-object v5, v3, v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v5, v5

    .line 42
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    move-result-object v20

    move/from16 v16, v7

    move/from16 v17, v8

    move/from16 v18, v6

    move/from16 v19, v5

    move/from16 v21, p2

    .line 43
    invoke-static/range {v12 .. v21}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getCubicBezArrowPath(FFFFFFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    move-result-object v12

    goto/16 :goto_3

    .line 44
    :cond_a
    iget-object v6, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v12, v6, Landroid/graphics/PointF;->x:F

    iget v13, v6, Landroid/graphics/PointF;->y:F

    add-int/lit8 v6, v5, -0x6

    aget-object v6, v3, v6

    .line 45
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v14, v6

    add-int/lit8 v6, v5, -0x5

    aget-object v6, v3, v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v15, v6

    add-int/lit8 v6, v5, -0x4

    aget-object v6, v3, v6

    .line 46
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v6, v6

    add-int/lit8 v7, v5, -0x3

    aget-object v7, v3, v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    add-int/lit8 v8, v5, -0x2

    aget-object v8, v3, v8

    .line 47
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    int-to-float v8, v8

    sub-int/2addr v5, v11

    aget-object v5, v3, v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v5, v5

    .line 48
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    move-result-object v20

    move/from16 v16, v6

    move/from16 v17, v7

    move/from16 v18, v8

    move/from16 v19, v5

    move/from16 v21, p2

    .line 49
    invoke-static/range {v12 .. v21}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getCubicBezArrowPath(FFFFFFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    move-result-object v12

    goto :goto_3

    :cond_b
    if-le v5, v10, :cond_c

    add-int/lit8 v6, v5, -0x4

    .line 50
    aget-object v6, v3, v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v12, v6

    add-int/lit8 v6, v5, -0x3

    aget-object v6, v3, v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v13, v6

    add-int/lit8 v6, v5, -0x2

    aget-object v6, v3, v6

    .line 51
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v14, v6

    sub-int/2addr v5, v11

    aget-object v5, v3, v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v15, v5

    .line 52
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    move-result-object v16

    move/from16 v17, p2

    .line 53
    invoke-static/range {v12 .. v17}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    move-result-object v12

    goto :goto_3

    .line 54
    :cond_c
    iget-object v6, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v12, v6, Landroid/graphics/PointF;->x:F

    iget v13, v6, Landroid/graphics/PointF;->y:F

    add-int/lit8 v6, v5, -0x2

    aget-object v6, v3, v6

    .line 55
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v14, v6

    sub-int/2addr v5, v11

    aget-object v5, v3, v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v15, v5

    .line 56
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getEndArrow()Lcom/intsig/office/common/shape/Arrow;

    move-result-object v16

    move/from16 v17, p2

    .line 57
    invoke-static/range {v12 .. v17}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getDirectLineArrowPath(FFFFLcom/intsig/office/common/shape/Arrow;I)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    move-result-object v12

    goto :goto_3

    :cond_d
    :goto_2
    const/4 v12, 0x0

    :goto_3
    if-eqz v4, :cond_e

    .line 58
    invoke-virtual {v4}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    move-result-object v5

    iput-object v5, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->startArrowPath:Landroid/graphics/Path;

    .line 59
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Path;->reset()V

    .line 60
    iget-object v5, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->nextNode:Landroid/graphics/PointF;

    iget v6, v5, Landroid/graphics/PointF;->x:F

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    move-result-object v7

    iget v7, v7, Landroid/graphics/PointF;->x:F

    invoke-virtual {v4}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getStartArrowType()B

    move-result v8

    invoke-static {v6, v5, v7, v4, v8}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    move-result-object v4

    .line 61
    iget v5, v4, Landroid/graphics/PointF;->x:F

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v5, v4}, Landroid/graphics/Path;->moveTo(FF)V

    :cond_e
    if-eqz v12, :cond_f

    .line 62
    invoke-virtual {v12}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    move-result-object v4

    iput-object v4, v0, Lcom/intsig/office/fc/doc/VMLPathParser;->endArrowPath:Landroid/graphics/Path;

    .line 63
    array-length v4, v3

    add-int/lit8 v5, v4, -0x2

    .line 64
    aget-object v6, v3, v5

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v6, v6

    sub-int/2addr v4, v11

    aget-object v7, v3, v4

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v12}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    move-result-object v8

    iget v8, v8, Landroid/graphics/PointF;->x:F

    invoke-virtual {v12}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    move-result-object v12

    iget v12, v12, Landroid/graphics/PointF;->y:F

    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/LineShape;->getEndArrowType()B

    move-result v13

    invoke-static {v6, v7, v8, v12, v13}, Lcom/intsig/office/common/autoshape/pathbuilder/LineArrowPathBuilder;->getReferencedPosition(FFFFB)Landroid/graphics/PointF;

    move-result-object v6

    .line 65
    iget v7, v6, Landroid/graphics/PointF;->x:F

    float-to-int v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v5

    .line 66
    iget v5, v6, Landroid/graphics/PointF;->y:F

    float-to-int v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    :cond_f
    if-eqz v2, :cond_16

    if-eq v2, v11, :cond_15

    if-eq v2, v10, :cond_14

    const/4 v4, 0x3

    if-eq v2, v4, :cond_13

    const/4 v4, 0x5

    if-eq v2, v4, :cond_12

    if-eq v2, v9, :cond_11

    const/4 v4, 0x7

    if-eq v2, v4, :cond_10

    goto :goto_4

    .line 67
    :cond_10
    invoke-direct {v0, v1, v3}, Lcom/intsig/office/fc/doc/VMLPathParser;->processCommand_rCurveTo(Landroid/graphics/Path;[Ljava/lang/Integer;)V

    goto :goto_4

    .line 68
    :cond_11
    invoke-direct {v0, v1, v3}, Lcom/intsig/office/fc/doc/VMLPathParser;->processCommand_rLineTo(Landroid/graphics/Path;[Ljava/lang/Integer;)V

    goto :goto_4

    .line 69
    :cond_12
    invoke-direct {v0, v1, v3}, Lcom/intsig/office/fc/doc/VMLPathParser;->processCommand_rMoveTo(Landroid/graphics/Path;[Ljava/lang/Integer;)V

    goto :goto_4

    .line 70
    :cond_13
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Path;->close()V

    goto :goto_4

    .line 71
    :cond_14
    invoke-direct {v0, v1, v3}, Lcom/intsig/office/fc/doc/VMLPathParser;->processCommand_CurveTo(Landroid/graphics/Path;[Ljava/lang/Integer;)V

    goto :goto_4

    .line 72
    :cond_15
    invoke-direct {v0, v1, v3}, Lcom/intsig/office/fc/doc/VMLPathParser;->processCommand_LineTo(Landroid/graphics/Path;[Ljava/lang/Integer;)V

    goto :goto_4

    .line 73
    :cond_16
    invoke-direct {v0, v1, v3}, Lcom/intsig/office/fc/doc/VMLPathParser;->processCommand_MoveTo(Landroid/graphics/Path;[Ljava/lang/Integer;)V

    :goto_4
    return-void
.end method


# virtual methods
.method public createPath(Lcom/intsig/office/common/shape/WPAutoShape;Ljava/lang/String;I)Lcom/intsig/office/fc/doc/PathWithArrow;
    .locals 15

    .line 1
    move-object v6, p0

    .line 2
    move-object/from16 v7, p2

    .line 3
    .line 4
    const/4 v8, 0x0

    .line 5
    const/4 v9, 0x0

    .line 6
    :try_start_0
    iput v8, v6, Lcom/intsig/office/fc/doc/VMLPathParser;->index:I

    .line 7
    .line 8
    iput-object v9, v6, Lcom/intsig/office/fc/doc/VMLPathParser;->startArrowPath:Landroid/graphics/Path;

    .line 9
    .line 10
    iput-object v9, v6, Lcom/intsig/office/fc/doc/VMLPathParser;->endArrowPath:Landroid/graphics/Path;

    .line 11
    .line 12
    new-instance v10, Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0, v7}, Lcom/intsig/office/fc/doc/VMLPathParser;->nextCommand(Ljava/lang/String;)B

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    sget-byte v1, Lcom/intsig/office/fc/doc/VMLPathParser;->NodeType_Start:B

    .line 22
    .line 23
    iput-byte v1, v6, Lcom/intsig/office/fc/doc/VMLPathParser;->currentNodeType:B

    .line 24
    .line 25
    sget-byte v1, Lcom/intsig/office/fc/doc/VMLPathParser;->NodeType_Invalidate:B

    .line 26
    .line 27
    iput-byte v1, v6, Lcom/intsig/office/fc/doc/VMLPathParser;->preNodeType:B

    .line 28
    .line 29
    const/4 v11, 0x1

    .line 30
    move v4, v0

    .line 31
    move-object v1, v9

    .line 32
    :goto_0
    const/4 v0, 0x1

    .line 33
    :goto_1
    const/4 v2, -0x1

    .line 34
    if-eq v4, v2, :cond_5

    .line 35
    .line 36
    const/4 v3, 0x4

    .line 37
    if-ne v4, v3, :cond_1

    .line 38
    .line 39
    invoke-direct {p0, v7}, Lcom/intsig/office/fc/doc/VMLPathParser;->nextCommand(Ljava/lang/String;)B

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-ne v0, v2, :cond_0

    .line 44
    .line 45
    sget-byte v2, Lcom/intsig/office/fc/doc/VMLPathParser;->NodeType_End:B

    .line 46
    .line 47
    iput-byte v2, v6, Lcom/intsig/office/fc/doc/VMLPathParser;->currentNodeType:B

    .line 48
    .line 49
    :cond_0
    move v4, v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    if-eqz v0, :cond_2

    .line 52
    .line 53
    new-instance v1, Landroid/graphics/Path;

    .line 54
    .line 55
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 56
    .line 57
    .line 58
    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    .line 60
    .line 61
    move-object v13, v1

    .line 62
    const/4 v12, 0x0

    .line 63
    goto :goto_2

    .line 64
    :cond_2
    move v12, v0

    .line 65
    move-object v13, v1

    .line 66
    :goto_2
    invoke-direct {p0, v7}, Lcom/intsig/office/fc/doc/VMLPathParser;->nextParameters(Ljava/lang/String;)[Ljava/lang/Integer;

    .line 67
    .line 68
    .line 69
    move-result-object v5

    .line 70
    invoke-direct {p0, v7}, Lcom/intsig/office/fc/doc/VMLPathParser;->nextCommand(Ljava/lang/String;)B

    .line 71
    .line 72
    .line 73
    move-result v14

    .line 74
    if-eq v14, v2, :cond_3

    .line 75
    .line 76
    if-ne v14, v3, :cond_4

    .line 77
    .line 78
    :cond_3
    sget-byte v0, Lcom/intsig/office/fc/doc/VMLPathParser;->NodeType_End:B

    .line 79
    .line 80
    iput-byte v0, v6, Lcom/intsig/office/fc/doc/VMLPathParser;->currentNodeType:B

    .line 81
    .line 82
    :cond_4
    move-object v0, p0

    .line 83
    move-object/from16 v1, p1

    .line 84
    .line 85
    move/from16 v2, p3

    .line 86
    .line 87
    move-object v3, v13

    .line 88
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/doc/VMLPathParser;->processPath(Lcom/intsig/office/common/shape/WPAutoShape;ILandroid/graphics/Path;B[Ljava/lang/Integer;)V

    .line 89
    .line 90
    .line 91
    iget-byte v0, v6, Lcom/intsig/office/fc/doc/VMLPathParser;->currentNodeType:B

    .line 92
    .line 93
    iput-byte v0, v6, Lcom/intsig/office/fc/doc/VMLPathParser;->preNodeType:B

    .line 94
    .line 95
    sget-byte v0, Lcom/intsig/office/fc/doc/VMLPathParser;->NodeType_Middle:B

    .line 96
    .line 97
    iput-byte v0, v6, Lcom/intsig/office/fc/doc/VMLPathParser;->currentNodeType:B

    .line 98
    .line 99
    move v0, v12

    .line 100
    move-object v1, v13

    .line 101
    move v4, v14

    .line 102
    goto :goto_1

    .line 103
    :cond_5
    new-instance v0, Lcom/intsig/office/fc/doc/PathWithArrow;

    .line 104
    .line 105
    invoke-interface {v10}, Ljava/util/List;->size()I

    .line 106
    .line 107
    .line 108
    move-result v1

    .line 109
    new-array v1, v1, [Landroid/graphics/Path;

    .line 110
    .line 111
    invoke-interface {v10, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 112
    .line 113
    .line 114
    move-result-object v1

    .line 115
    check-cast v1, [Landroid/graphics/Path;

    .line 116
    .line 117
    iget-object v2, v6, Lcom/intsig/office/fc/doc/VMLPathParser;->startArrowPath:Landroid/graphics/Path;

    .line 118
    .line 119
    iget-object v3, v6, Lcom/intsig/office/fc/doc/VMLPathParser;->endArrowPath:Landroid/graphics/Path;

    .line 120
    .line 121
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/doc/PathWithArrow;-><init>([Landroid/graphics/Path;Landroid/graphics/Path;Landroid/graphics/Path;)V

    .line 122
    .line 123
    .line 124
    iput-object v9, v6, Lcom/intsig/office/fc/doc/VMLPathParser;->startArrowPath:Landroid/graphics/Path;

    .line 125
    .line 126
    iput-object v9, v6, Lcom/intsig/office/fc/doc/VMLPathParser;->endArrowPath:Landroid/graphics/Path;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    .line 128
    return-object v0

    .line 129
    :catch_0
    return-object v9
.end method

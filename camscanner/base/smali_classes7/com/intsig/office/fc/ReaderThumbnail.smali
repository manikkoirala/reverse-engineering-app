.class public Lcom/intsig/office/fc/ReaderThumbnail;
.super Ljava/lang/Object;
.source "ReaderThumbnail.java"


# static fields
.field private static kit:Lcom/intsig/office/fc/ReaderThumbnail;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ReaderThumbnail;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/ReaderThumbnail;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/ReaderThumbnail;->kit:Lcom/intsig/office/fc/ReaderThumbnail;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private getThumbnailForPPT_Small(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    new-instance v1, Lcom/intsig/office/fc/ppt/PPTReader;

    .line 3
    .line 4
    const/4 v2, 0x1

    .line 5
    invoke-direct {v1, v0, p1, v2, v0}, Lcom/intsig/office/fc/ppt/PPTReader;-><init>(Lcom/intsig/office/system/IControl;Ljava/lang/String;ZLandroid/net/Uri;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/fc/ppt/PPTReader;->getModel()Ljava/lang/Object;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    check-cast p1, Lcom/intsig/office/pg/model/PGModel;

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/pg/model/PGModel;->getPageSize()Lcom/intsig/office/java/awt/Dimension;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    int-to-double v2, p2

    .line 21
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Dimension;->getWidth()D

    .line 22
    .line 23
    .line 24
    move-result-wide v4

    .line 25
    div-double/2addr v2, v4

    .line 26
    int-to-double p2, p3

    .line 27
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Dimension;->getHeight()D

    .line 28
    .line 29
    .line 30
    move-result-wide v4

    .line 31
    div-double/2addr p2, v4

    .line 32
    invoke-static {v2, v3, p2, p3}, Ljava/lang/Math;->min(DD)D

    .line 33
    .line 34
    .line 35
    move-result-wide p2

    .line 36
    double-to-float p2, p2

    .line 37
    new-instance p3, Lcom/intsig/office/pg/control/PGEditor;

    .line 38
    .line 39
    invoke-direct {p3, v0}, Lcom/intsig/office/pg/control/PGEditor;-><init>(Lcom/intsig/office/pg/control/Presentation;)V

    .line 40
    .line 41
    .line 42
    invoke-static {}, Lcom/intsig/office/pg/view/SlideDrawKit;->instance()Lcom/intsig/office/pg/view/SlideDrawKit;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    const/4 v2, 0x0

    .line 47
    invoke-virtual {p1, v2}, Lcom/intsig/office/pg/model/PGModel;->getSlide(I)Lcom/intsig/office/pg/model/PGSlide;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    invoke-virtual {v1, p1, p3, v2, p2}, Lcom/intsig/office/pg/view/SlideDrawKit;->getThumbnail(Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/control/PGEditor;Lcom/intsig/office/pg/model/PGSlide;F)Landroid/graphics/Bitmap;

    .line 52
    .line 53
    .line 54
    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    return-object p1

    .line 56
    :catch_0
    :cond_0
    return-object v0
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static instance()Lcom/intsig/office/fc/ReaderThumbnail;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ReaderThumbnail;->kit:Lcom/intsig/office/fc/ReaderThumbnail;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private readSection([BIII)Landroid/graphics/Bitmap;
    .locals 7

    .line 1
    add-int/lit8 p2, p2, 0x10

    .line 2
    .line 3
    invoke-static {p1, p2}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getUInt([BI)J

    .line 4
    .line 5
    .line 6
    move-result-wide p2

    .line 7
    long-to-int p3, p2

    .line 8
    invoke-static {p1, p3}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getUInt([BI)J

    .line 9
    .line 10
    .line 11
    add-int/lit8 p2, p3, 0x4

    .line 12
    .line 13
    invoke-static {p1, p2}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getUInt([BI)J

    .line 14
    .line 15
    .line 16
    move-result-wide v0

    .line 17
    long-to-int p4, v0

    .line 18
    add-int/lit8 p2, p2, 0x4

    .line 19
    .line 20
    const/4 v0, -0x1

    .line 21
    const/4 v1, 0x0

    .line 22
    const/4 v2, 0x0

    .line 23
    const/4 v3, -0x1

    .line 24
    :goto_0
    const/16 v4, 0x11

    .line 25
    .line 26
    if-ge v1, p4, :cond_1

    .line 27
    .line 28
    invoke-static {p1, p2}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getUInt([BI)J

    .line 29
    .line 30
    .line 31
    move-result-wide v2

    .line 32
    long-to-int v3, v2

    .line 33
    add-int/lit8 p2, p2, 0x4

    .line 34
    .line 35
    invoke-static {p1, p2}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getUInt([BI)J

    .line 36
    .line 37
    .line 38
    move-result-wide v5

    .line 39
    long-to-int v2, v5

    .line 40
    add-int/lit8 p2, p2, 0x4

    .line 41
    .line 42
    if-ne v3, v4, :cond_0

    .line 43
    .line 44
    goto :goto_1

    .line 45
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    :goto_1
    if-ne v3, v4, :cond_4

    .line 49
    .line 50
    add-int/2addr v2, p3

    .line 51
    invoke-static {p1, v2}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getUInt([BI)J

    .line 52
    .line 53
    .line 54
    add-int/lit8 p2, v2, 0x4

    .line 55
    .line 56
    invoke-static {p1, p2}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getUInt([BI)J

    .line 57
    .line 58
    .line 59
    add-int/lit8 p2, p2, 0x4

    .line 60
    .line 61
    invoke-static {p1, p2}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getUInt([BI)J

    .line 62
    .line 63
    .line 64
    move-result-wide p3

    .line 65
    long-to-int p4, p3

    .line 66
    add-int/lit8 p2, p2, 0x4

    .line 67
    .line 68
    invoke-static {p1, p2}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getUInt([BI)J

    .line 69
    .line 70
    .line 71
    move-result-wide p2

    .line 72
    long-to-int p3, p2

    .line 73
    if-ne p4, v0, :cond_4

    .line 74
    .line 75
    const/4 p2, 0x3

    .line 76
    if-ne p3, p2, :cond_2

    .line 77
    .line 78
    add-int/lit8 p4, v2, 0x18

    .line 79
    .line 80
    goto :goto_2

    .line 81
    :cond_2
    move p4, v2

    .line 82
    :goto_2
    if-le p4, v2, :cond_4

    .line 83
    .line 84
    if-ne p3, p2, :cond_3

    .line 85
    .line 86
    goto :goto_3

    .line 87
    :cond_3
    const/16 p2, 0x333

    .line 88
    .line 89
    if-ne p3, p2, :cond_4

    .line 90
    .line 91
    :try_start_0
    array-length p2, p1

    .line 92
    sub-int/2addr p2, p4

    .line 93
    invoke-static {p1, p4, p2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    .line 94
    .line 95
    .line 96
    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    return-object p1

    .line 98
    :catch_0
    move-exception p1

    .line 99
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 100
    .line 101
    .line 102
    :cond_4
    :goto_3
    const/4 p1, 0x0

    .line 103
    return-object p1
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method


# virtual methods
.method public getThumbnailForPDF(Ljava/lang/String;F)Landroid/graphics/Bitmap;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    invoke-static {}, Lcom/intsig/office/fc/pdf/PDFLib;->getPDFLib()Lcom/intsig/office/fc/pdf/PDFLib;

    .line 3
    .line 4
    .line 5
    move-result-object v1

    .line 6
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/pdf/PDFLib;->openFileSync(Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/pdf/PDFLib;->hasPasswordSync()Z

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    return-object v0

    .line 16
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/pdf/PDFLib;->getAllPagesSize()[Landroid/graphics/Rect;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    const/4 v2, 0x0

    .line 21
    aget-object p1, p1, v2

    .line 22
    .line 23
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    int-to-float v2, v2

    .line 28
    mul-float v2, v2, p2

    .line 29
    .line 30
    float-to-int v8, v2

    .line 31
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 32
    .line 33
    .line 34
    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 35
    int-to-float p1, p1

    .line 36
    mul-float p1, p1, p2

    .line 37
    .line 38
    float-to-int v9, p1

    .line 39
    :try_start_1
    sget-object p1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 40
    .line 41
    invoke-static {v8, v9, p1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 42
    .line 43
    .line 44
    move-result-object p1
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 45
    const/4 v3, 0x0

    .line 46
    int-to-float v4, v8

    .line 47
    int-to-float v5, v9

    .line 48
    const/4 v6, 0x0

    .line 49
    const/4 v7, 0x0

    .line 50
    const/4 v10, 0x1

    .line 51
    move-object v2, p1

    .line 52
    :try_start_2
    invoke-virtual/range {v1 .. v10}, Lcom/intsig/office/fc/pdf/PDFLib;->drawPageSync(Landroid/graphics/Bitmap;IFFIIIII)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :catch_0
    move-object v0, p1

    .line 57
    :catch_1
    move-object p1, v0

    .line 58
    :goto_0
    return-object p1

    .line 59
    :catch_2
    return-object v0
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public getThumbnailForPPT(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/io/FileInputStream;

    .line 2
    .line 3
    new-instance v1, Ljava/io/File;

    .line 4
    .line 5
    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 9
    .line 10
    .line 11
    new-instance p1, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    invoke-direct {p1, v0, v1}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;-><init>(Ljava/io/InputStream;Z)V

    .line 15
    .line 16
    .line 17
    const-string v0, "\u0005SummaryInformation"

    .line 18
    .line 19
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->getProperty(Ljava/lang/String;)Lcom/intsig/office/fc/fs/filesystem/Property;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    const/4 v0, 0x0

    .line 24
    if-eqz p1, :cond_2

    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/intsig/office/fc/fs/filesystem/Property;->getDocumentRawData()[B

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    const/4 v1, 0x0

    .line 31
    invoke-static {p1, v1}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getUShort([BI)I

    .line 32
    .line 33
    .line 34
    const/4 v2, 0x2

    .line 35
    invoke-static {p1, v2}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getUShort([BI)I

    .line 36
    .line 37
    .line 38
    const/4 v2, 0x4

    .line 39
    invoke-static {p1, v2}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getUInt([BI)J

    .line 40
    .line 41
    .line 42
    const/16 v2, 0x18

    .line 43
    .line 44
    invoke-static {p1, v2}, Lcom/intsig/office/fc/fs/storage/LittleEndian;->getInt([BI)I

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    if-gez v2, :cond_0

    .line 49
    .line 50
    return-object v0

    .line 51
    :cond_0
    const/16 v3, 0x1c

    .line 52
    .line 53
    :goto_0
    if-ge v1, v2, :cond_2

    .line 54
    .line 55
    invoke-direct {p0, p1, v3, p2, p3}, Lcom/intsig/office/fc/ReaderThumbnail;->readSection([BIII)Landroid/graphics/Bitmap;

    .line 56
    .line 57
    .line 58
    move-result-object v4

    .line 59
    if-eqz v4, :cond_1

    .line 60
    .line 61
    return-object v4

    .line 62
    :cond_1
    add-int/lit8 v3, v3, 0x14

    .line 63
    .line 64
    add-int/lit8 v1, v1, 0x1

    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_2
    return-object v0
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public getThumbnailForPPTX(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;-><init>(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "http://schemas.openxmlformats.org/package/2006/relationships/metadata/thumbnail"

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationship(I)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    const/4 v1, 0x0

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    return-object v1

    .line 21
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    if-nez p1, :cond_1

    .line 30
    .line 31
    return-object v1

    .line 32
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    return-object p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

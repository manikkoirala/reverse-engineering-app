.class public final Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BookSheetKey;
.super Ljava/lang/Object;
.source "FormulaUsedBlankCellSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BookSheetKey"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final _bookIndex:I

.field private final _sheetIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(II)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BookSheetKey;->_bookIndex:I

    .line 5
    .line 6
    iput p2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BookSheetKey;->_sheetIndex:I

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BookSheetKey;

    .line 2
    .line 3
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BookSheetKey;->_bookIndex:I

    .line 4
    .line 5
    iget v1, p1, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BookSheetKey;->_bookIndex:I

    .line 6
    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BookSheetKey;->_sheetIndex:I

    .line 10
    .line 11
    iget p1, p1, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BookSheetKey;->_sheetIndex:I

    .line 12
    .line 13
    if-ne v0, p1, :cond_0

    .line 14
    .line 15
    const/4 p1, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 p1, 0x0

    .line 18
    :goto_0
    return p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BookSheetKey;->_bookIndex:I

    .line 2
    .line 3
    mul-int/lit8 v0, v0, 0x11

    .line 4
    .line 5
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BookSheetKey;->_sheetIndex:I

    .line 6
    .line 7
    add-int/2addr v0, v1

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

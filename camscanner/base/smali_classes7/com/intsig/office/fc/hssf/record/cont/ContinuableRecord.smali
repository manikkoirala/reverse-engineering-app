.class public abstract Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecord;
.super Lcom/intsig/office/fc/hssf/record/Record;
.source "ContinuableRecord.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/Record;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final getRecordSize()I
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->createForCountingOnly()Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecord;->serialize(Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->terminate()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->getTotalSize()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final serialize(I[B)I
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;

    .line 2
    .line 3
    invoke-direct {v0, p2, p1}, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;-><init>([BI)V

    .line 4
    .line 5
    .line 6
    new-instance p1, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 9
    .line 10
    .line 11
    move-result p2

    .line 12
    invoke-direct {p1, v0, p2}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;-><init>(Lcom/intsig/office/fc/util/LittleEndianOutput;I)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecord;->serialize(Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->terminate()V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->getTotalSize()I

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    return p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected abstract serialize(Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;)V
.end method

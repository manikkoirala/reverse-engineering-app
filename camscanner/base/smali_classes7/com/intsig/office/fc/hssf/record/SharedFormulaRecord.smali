.class public final Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;
.super Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;
.source "SharedFormulaRecord.java"


# static fields
.field public static final sid:S = 0x4bcs


# instance fields
.field private field_5_reserved:I

.field private field_7_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;-><init>(IIII)V

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;-><init>(Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V
    .locals 2

    .line 4
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;->field_5_reserved:I

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->available()I

    move-result v1

    .line 8
    invoke-static {v0, p1, v1}, Lcom/intsig/office/fc/hssf/formula/Formula;->read(ILcom/intsig/office/fc/util/LittleEndianInput;I)Lcom/intsig/office/fc/hssf/formula/Formula;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;->field_7_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    return-void
.end method

.method private constructor <init>(Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;-><init>(Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;)V

    .line 3
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->EMPTY_PTG_ARRAY:[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->create([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Lcom/intsig/office/fc/hssf/formula/Formula;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;->field_7_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;->getRange()Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;-><init>(Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;)V

    .line 8
    .line 9
    .line 10
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;->field_5_reserved:I

    .line 11
    .line 12
    iput v1, v0, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;->field_5_reserved:I

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;->field_7_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 15
    .line 16
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/formula/Formula;->copy()Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;->field_7_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 21
    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected getExtraDataSize()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;->field_7_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/Formula;->getEncodedSize()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    add-int/lit8 v0, v0, 0x2

    .line 8
    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFormulaTokens(Lcom/intsig/office/fc/hssf/record/FormulaRecord;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/CellRecord;->getRow()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/CellRecord;->getColumn()S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;->isInRange(II)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/SharedFormula;

    .line 16
    .line 17
    sget-object v2, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->EXCEL97:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    .line 18
    .line 19
    invoke-direct {v1, v2}, Lcom/intsig/office/fc/hssf/formula/SharedFormula;-><init>(Lcom/intsig/office/fc/ss/SpreadsheetVersion;)V

    .line 20
    .line 21
    .line 22
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;->field_7_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 23
    .line 24
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/Formula;->getTokens()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-virtual {v1, v2, v0, p1}, Lcom/intsig/office/fc/hssf/formula/SharedFormula;->convertSharedFormulas([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;II)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    return-object p1

    .line 33
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 34
    .line 35
    const-string v0, "Shared Formula Conversion: Coding Error"

    .line 36
    .line 37
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    throw p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getSid()S
    .locals 1

    .line 1
    const/16 v0, 0x4bc

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isFormulaSame(Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;->field_7_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 2
    .line 3
    iget-object p1, p1, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;->field_7_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->isSame(Lcom/intsig/office/fc/hssf/formula/Formula;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected serializeExtraData(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;->field_5_reserved:I

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;->field_7_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "[SHARED FORMULA ("

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 9
    .line 10
    .line 11
    const/16 v1, 0x4bc

    .line 12
    .line 13
    invoke-static {v1}, Lcom/intsig/office/fc/util/HexDump;->intToHex(I)[C

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 18
    .line 19
    .line 20
    const-string v1, "]\n"

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 23
    .line 24
    .line 25
    const-string v1, "    .range      = "

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;->getRange()Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-virtual {v1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 39
    .line 40
    .line 41
    const-string v1, "\n"

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 44
    .line 45
    .line 46
    const-string v2, "    .reserved    = "

    .line 47
    .line 48
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 49
    .line 50
    .line 51
    iget v2, p0, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;->field_5_reserved:I

    .line 52
    .line 53
    invoke-static {v2}, Lcom/intsig/office/fc/util/HexDump;->shortToHex(I)[C

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 61
    .line 62
    .line 63
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;->field_7_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 64
    .line 65
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/Formula;->getTokens()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    const/4 v3, 0x0

    .line 70
    :goto_0
    array-length v4, v2

    .line 71
    if-ge v3, v4, :cond_0

    .line 72
    .line 73
    const-string v4, "Formula["

    .line 74
    .line 75
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 79
    .line 80
    .line 81
    const-string v4, "]"

    .line 82
    .line 83
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 84
    .line 85
    .line 86
    aget-object v4, v2, v3

    .line 87
    .line 88
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->toString()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v5

    .line 92
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 93
    .line 94
    .line 95
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getRVAType()C

    .line 96
    .line 97
    .line 98
    move-result v4

    .line 99
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 103
    .line 104
    .line 105
    add-int/lit8 v3, v3, 0x1

    .line 106
    .line 107
    goto :goto_0

    .line 108
    :cond_0
    const-string v1, "[/SHARED FORMULA]\n"

    .line 109
    .line 110
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 111
    .line 112
    .line 113
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    return-object v0
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

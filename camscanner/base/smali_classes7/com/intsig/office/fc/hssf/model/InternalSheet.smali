.class public final Lcom/intsig/office/fc/hssf/model/InternalSheet;
.super Ljava/lang/Object;
.source "InternalSheet.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/model/InternalSheet$RecordCloner;
    }
.end annotation


# static fields
.field public static final BottomMargin:S = 0x3s

.field public static final LeftMargin:S = 0x0s

.field public static final PANE_LOWER_LEFT:B = 0x2t

.field public static final PANE_LOWER_RIGHT:B = 0x0t

.field public static final PANE_UPPER_LEFT:B = 0x3t

.field public static final PANE_UPPER_RIGHT:B = 0x1t

.field public static final RightMargin:S = 0x1s

.field public static final TopMargin:S = 0x2s

.field private static log:Lcom/intsig/office/fc/util/POILogger;


# instance fields
.field _columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

.field private _dataValidityTable:Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;

.field private _dimensions:Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

.field private _gutsRecord:Lcom/intsig/office/fc/hssf/record/GutsRecord;

.field protected _isUncalced:Z

.field private final _mergedCellsTable:Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;

.field private final _protectionBlock:Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;

.field private _psBlock:Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

.field private _records:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/RecordBase;",
            ">;"
        }
    .end annotation
.end field

.field protected final _rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

.field protected _selection:Lcom/intsig/office/fc/hssf/record/SelectionRecord;

.field private condFormatting:Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

.field protected defaultcolwidth:Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;

.field protected defaultrowheight:Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;

.field protected gridset:Lcom/intsig/office/fc/hssf/record/GridsetRecord;

.field protected printGridlines:Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;

.field private rowRecIterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator<",
            "Lcom/intsig/office/fc/hssf/record/RowRecord;",
            ">;"
        }
    .end annotation
.end field

.field private sheetType:I

.field protected windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lcom/intsig/office/fc/util/POILogger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 6

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 85
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->printGridlines:Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;

    .line 86
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->gridset:Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    .line 87
    new-instance v1, Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;

    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;-><init>()V

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->defaultcolwidth:Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;

    .line 88
    new-instance v1, Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;

    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;-><init>()V

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->defaultrowheight:Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;

    .line 89
    new-instance v1, Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;

    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;-><init>()V

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_protectionBlock:Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;

    .line 90
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 91
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_selection:Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    .line 92
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dataValidityTable:Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;

    .line 93
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->rowRecIterator:Ljava/util/Iterator;

    const/4 v0, 0x0

    .line 94
    iput-boolean v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_isUncalced:Z

    const/16 v0, 0x10

    .line 95
    iput v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->sheetType:I

    .line 96
    new-instance v0, Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;

    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_mergedCellsTable:Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;

    .line 97
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0x20

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 98
    sget-object v3, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    sget v4, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/util/POILogger;->check(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 99
    sget-object v3, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    sget v4, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    const-string v5, "Sheet createsheet from scratch called"

    invoke-virtual {v3, v4, v5}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 100
    :cond_0
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createBOF()Lcom/intsig/office/fc/hssf/record/BOFRecord;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createCalcMode()Lcom/intsig/office/fc/hssf/record/CalcModeRecord;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createCalcCount()Lcom/intsig/office/fc/hssf/record/CalcCountRecord;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createRefMode()Lcom/intsig/office/fc/hssf/record/RefModeRecord;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createIteration()Lcom/intsig/office/fc/hssf/record/IterationRecord;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createDelta()Lcom/intsig/office/fc/hssf/record/DeltaRecord;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createSaveRecalc()Lcom/intsig/office/fc/hssf/record/SaveRecalcRecord;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createPrintHeaders()Lcom/intsig/office/fc/hssf/record/PrintHeadersRecord;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createPrintGridlines()Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;

    move-result-object v3

    iput-object v3, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->printGridlines:Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;

    .line 109
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createGridset()Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    move-result-object v3

    iput-object v3, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->gridset:Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    .line 111
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createGuts()Lcom/intsig/office/fc/hssf/record/GutsRecord;

    move-result-object v3

    iput-object v3, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_gutsRecord:Lcom/intsig/office/fc/hssf/record/GutsRecord;

    .line 113
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createDefaultRowHeight()Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;

    move-result-object v3

    iput-object v3, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->defaultrowheight:Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;

    .line 115
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createWSBool()Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    new-instance v3, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    invoke-direct {v3}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;-><init>()V

    iput-object v3, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_psBlock:Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 118
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createDefaultColWidth()Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->defaultcolwidth:Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;

    .line 121
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    new-instance v1, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;-><init>()V

    .line 123
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    .line 125
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createDimensions()Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dimensions:Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    .line 126
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    new-instance v1, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;-><init>()V

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 128
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createWindowTwo()Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createSelection()Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_selection:Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    .line 131
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    sget-object v0, Lcom/intsig/office/fc/hssf/record/EOFRecord;->instance:Lcom/intsig/office/fc/hssf/record/EOFRecord;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    iput-object v2, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 135
    sget-object v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    sget v1, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/POILogger;->check(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    sget-object v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    sget v1, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    const-string v2, "Sheet createsheet from scratch exit"

    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method private constructor <init>(Lcom/intsig/office/fc/hssf/model/RecordStream;Lcom/intsig/office/system/AbstractReader;)V
    .locals 10

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->printGridlines:Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;

    .line 3
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->gridset:Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    .line 4
    new-instance v1, Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;

    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;-><init>()V

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->defaultcolwidth:Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;

    .line 5
    new-instance v1, Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;

    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;-><init>()V

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->defaultrowheight:Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;

    .line 6
    new-instance v1, Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;

    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;-><init>()V

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_protectionBlock:Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;

    .line 7
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_selection:Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dataValidityTable:Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;

    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->rowRecIterator:Ljava/util/Iterator;

    const/4 v1, 0x0

    .line 11
    iput-boolean v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_isUncalced:Z

    const/16 v1, 0x10

    .line 12
    iput v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->sheetType:I

    .line 13
    new-instance v2, Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;

    invoke-direct {v2}, Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;-><init>()V

    iput-object v2, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_mergedCellsTable:Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;

    .line 14
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 15
    iput-object v2, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->peekNextSid()I

    move-result v4

    const/16 v5, 0x809

    if-ne v4, v5, :cond_22

    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->getNext()Lcom/intsig/office/fc/hssf/record/Record;

    move-result-object v4

    check-cast v4, Lcom/intsig/office/fc/hssf/record/BOFRecord;

    .line 18
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/record/BOFRecord;->getType()I

    move-result v6

    if-eq v6, v1, :cond_0

    .line 19
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/record/BOFRecord;->getType()I

    move-result v1

    iput v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->sheetType:I

    .line 20
    :cond_0
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, -0x1

    .line 21
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->hasNext()Z

    move-result v4

    const/16 v6, 0x23e

    const/4 v7, 0x1

    if-eqz v4, :cond_1c

    if-eqz p2, :cond_2

    .line 22
    invoke-virtual {p2}, Lcom/intsig/office/system/AbstractReader;->isAborted()Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_1

    .line 23
    :cond_1
    new-instance p1, Lcom/intsig/office/system/AbortReaderError;

    const-string p2, "abort Reader"

    invoke-direct {p1, p2}, Lcom/intsig/office/system/AbortReaderError;-><init>(Ljava/lang/String;)V

    throw p1

    .line 24
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->peekNextSid()I

    move-result v4

    const/16 v8, 0x1b0

    if-ne v4, v8, :cond_3

    .line 25
    new-instance v4, Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    invoke-direct {v4, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;-><init>(Lcom/intsig/office/fc/hssf/model/RecordStream;)V

    iput-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->condFormatting:Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    .line 26
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    const/16 v8, 0x7d

    if-ne v4, v8, :cond_4

    .line 27
    new-instance v4, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    invoke-direct {v4, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;-><init>(Lcom/intsig/office/fc/hssf/model/RecordStream;)V

    iput-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    .line 28
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    const/16 v8, 0x1b2

    if-ne v4, v8, :cond_5

    .line 29
    new-instance v4, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;

    invoke-direct {v4, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;-><init>(Lcom/intsig/office/fc/hssf/model/RecordStream;)V

    iput-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dataValidityTable:Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;

    .line 30
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 31
    :cond_5
    invoke-static {v4}, Lcom/intsig/office/fc/hssf/model/RecordOrderer;->〇O〇(I)Z

    move-result v8

    if-eqz v8, :cond_7

    if-nez v0, :cond_6

    .line 32
    new-instance v0, Lcom/intsig/office/fc/hssf/model/RowBlocksReader;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/model/RowBlocksReader;-><init>(Lcom/intsig/office/fc/hssf/model/RecordStream;)V

    .line 33
    iget-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_mergedCellsTable:Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;

    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/RowBlocksReader;->getLooseMergedCells()[Lcom/intsig/office/fc/hssf/record/MergeCellsRecord;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;->addRecords([Lcom/intsig/office/fc/hssf/record/MergeCellsRecord;)V

    .line 34
    new-instance v4, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/RowBlocksReader;->getPlainRecordStream()Lcom/intsig/office/fc/hssf/model/RecordStream;

    move-result-object v6

    .line 35
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/RowBlocksReader;->getSharedFormulaManager()Lcom/intsig/office/fc/hssf/record/aggregates/SharedValueManager;

    move-result-object v0

    invoke-direct {v4, v6, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;-><init>(Lcom/intsig/office/fc/hssf/model/RecordStream;Lcom/intsig/office/fc/hssf/record/aggregates/SharedValueManager;)V

    .line 36
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v4

    goto :goto_0

    .line 37
    :cond_6
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "row/cell records found in the wrong place"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 38
    :cond_7
    invoke-static {v4}, Lcom/intsig/office/fc/hssf/record/aggregates/CustomViewSettingsRecordAggregate;->isBeginRecord(I)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 39
    new-instance v4, Lcom/intsig/office/fc/hssf/record/aggregates/CustomViewSettingsRecordAggregate;

    invoke-direct {v4, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/CustomViewSettingsRecordAggregate;-><init>(Lcom/intsig/office/fc/hssf/model/RecordStream;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 40
    :cond_8
    invoke-static {v4}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->isComponentRecord(I)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 41
    iget-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_psBlock:Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    if-nez v4, :cond_9

    .line 42
    new-instance v4, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    invoke-direct {v4, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;-><init>(Lcom/intsig/office/fc/hssf/model/RecordStream;)V

    iput-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_psBlock:Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 43
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 44
    :cond_9
    invoke-virtual {v4, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->addLateRecords(Lcom/intsig/office/fc/hssf/model/RecordStream;)V

    .line 45
    :goto_2
    iget-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_psBlock:Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    invoke-virtual {v4, v2}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->positionRecords(Ljava/util/List;)V

    goto/16 :goto_0

    .line 46
    :cond_a
    invoke-static {v4}, Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;->isComponentRecord(I)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 47
    iget-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_protectionBlock:Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;

    invoke-virtual {v4, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;->addRecords(Lcom/intsig/office/fc/hssf/model/RecordStream;)V

    goto/16 :goto_0

    :cond_b
    const/16 v8, 0xe5

    if-ne v4, v8, :cond_c

    .line 48
    iget-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_mergedCellsTable:Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;

    invoke-virtual {v4, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;->read(Lcom/intsig/office/fc/hssf/model/RecordStream;)V

    goto/16 :goto_0

    :cond_c
    if-ne v4, v5, :cond_d

    .line 49
    new-instance v4, Lcom/intsig/office/fc/hssf/record/aggregates/ChartSubstreamRecordAggregate;

    invoke-direct {v4, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ChartSubstreamRecordAggregate;-><init>(Lcom/intsig/office/fc/hssf/model/RecordStream;)V

    .line 50
    invoke-static {v4, v2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->spillAggregate(Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;Ljava/util/List;)V

    goto/16 :goto_0

    .line 51
    :cond_d
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->getNext()Lcom/intsig/office/fc/hssf/record/Record;

    move-result-object v8

    const/16 v9, 0x20b

    if-ne v4, v9, :cond_e

    goto/16 :goto_0

    :cond_e
    const/16 v9, 0x5e

    if-ne v4, v9, :cond_f

    .line 52
    iput-boolean v7, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_isUncalced:Z

    goto/16 :goto_0

    :cond_f
    const/16 v9, 0x868

    if-eq v4, v9, :cond_1b

    const/16 v9, 0x867

    if-ne v4, v9, :cond_10

    goto/16 :goto_4

    :cond_10
    const/16 v9, 0xa

    if-ne v4, v9, :cond_11

    .line 53
    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    :cond_11
    const/16 v7, 0x200

    if-ne v4, v7, :cond_13

    .line 54
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    if-nez v1, :cond_12

    .line 55
    new-instance v1, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;-><init>()V

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    .line 56
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    :cond_12
    move-object v1, v8

    check-cast v1, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dimensions:Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    .line 58
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_3

    :cond_13
    const/16 v7, 0x55

    if-ne v4, v7, :cond_14

    .line 59
    move-object v4, v8

    check-cast v4, Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;

    iput-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->defaultcolwidth:Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;

    goto :goto_3

    :cond_14
    const/16 v7, 0x225

    if-ne v4, v7, :cond_15

    .line 60
    move-object v4, v8

    check-cast v4, Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;

    iput-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->defaultrowheight:Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;

    goto :goto_3

    :cond_15
    const/16 v7, 0x2b

    if-ne v4, v7, :cond_16

    .line 61
    move-object v4, v8

    check-cast v4, Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;

    iput-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->printGridlines:Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;

    goto :goto_3

    :cond_16
    const/16 v7, 0x82

    if-ne v4, v7, :cond_17

    .line 62
    move-object v4, v8

    check-cast v4, Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    iput-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->gridset:Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    goto :goto_3

    :cond_17
    const/16 v7, 0x1d

    if-ne v4, v7, :cond_18

    .line 63
    move-object v4, v8

    check-cast v4, Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    iput-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_selection:Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    goto :goto_3

    :cond_18
    if-ne v4, v6, :cond_19

    .line 64
    move-object v4, v8

    check-cast v4, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    iput-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    goto :goto_3

    :cond_19
    if-ne v4, v3, :cond_1a

    .line 65
    move-object v4, v8

    check-cast v4, Lcom/intsig/office/fc/hssf/record/GutsRecord;

    iput-object v4, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_gutsRecord:Lcom/intsig/office/fc/hssf/record/GutsRecord;

    .line 66
    :cond_1a
    :goto_3
    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 67
    :cond_1b
    :goto_4
    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 68
    :cond_1c
    :goto_5
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    if-eqz p1, :cond_21

    .line 69
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dimensions:Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    if-nez p1, :cond_1e

    if-nez v0, :cond_1d

    .line 70
    new-instance p1, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    invoke-direct {p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;-><init>()V

    move-object v0, p1

    goto :goto_6

    .line 71
    :cond_1d
    sget-object p1, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    sget p2, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    const-string v1, "DIMENSION record not found even though row/cells present"

    invoke-virtual {p1, p2, v1}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 72
    :goto_6
    invoke-virtual {p0, v6}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    move-result v1

    .line 73
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->createDimensions()Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dimensions:Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    .line 74
    invoke-interface {v2, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_1e
    if-nez v0, :cond_1f

    .line 75
    new-instance v0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;-><init>()V

    add-int/2addr v1, v7

    .line 76
    invoke-interface {v2, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 77
    :cond_1f
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 78
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_mergedCellsTable:Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;

    invoke-static {v2, p1}, Lcom/intsig/office/fc/hssf/model/RecordOrderer;->〇080(Ljava/util/List;Lcom/intsig/office/fc/hssf/record/RecordBase;)V

    .line 79
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_protectionBlock:Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;

    invoke-static {v2, p1}, Lcom/intsig/office/fc/hssf/model/RecordOrderer;->〇080(Ljava/util/List;Lcom/intsig/office/fc/hssf/record/RecordBase;)V

    .line 80
    sget-object p1, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    sget p2, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/util/POILogger;->check(I)Z

    move-result p1

    if-eqz p1, :cond_20

    .line 81
    sget-object p1, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    sget p2, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    const-string v0, "sheet createSheet (existing file) exited"

    invoke-virtual {p1, p2, v0}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    :cond_20
    return-void

    .line 82
    :cond_21
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "WINDOW2 was not found"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 83
    :cond_22
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "BOF record expected"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static createBOF()Lcom/intsig/office/fc/hssf/record/BOFRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/BOFRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/BOFRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0x600

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/BOFRecord;->setVersion(I)V

    .line 9
    .line 10
    .line 11
    const/16 v1, 0x10

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/BOFRecord;->setType(I)V

    .line 14
    .line 15
    .line 16
    const/16 v1, 0xdbb

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/BOFRecord;->setBuild(I)V

    .line 19
    .line 20
    .line 21
    const/16 v1, 0x7cc

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/BOFRecord;->setBuildYear(I)V

    .line 24
    .line 25
    .line 26
    const/16 v1, 0xc1

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/BOFRecord;->setHistoryBitMask(I)V

    .line 29
    .line 30
    .line 31
    const/4 v1, 0x6

    .line 32
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/BOFRecord;->setRequiredVersion(I)V

    .line 33
    .line 34
    .line 35
    return-object v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private static createCalcCount()Lcom/intsig/office/fc/hssf/record/CalcCountRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/CalcCountRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/CalcCountRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0x64

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/CalcCountRecord;->setIterations(S)V

    .line 9
    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static createCalcMode()Lcom/intsig/office/fc/hssf/record/CalcModeRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/CalcModeRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/CalcModeRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/CalcModeRecord;->setCalcMode(S)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static createDefaultColWidth()Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0x8

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;->setColWidth(I)V

    .line 9
    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static createDefaultRowHeight()Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;->setOptionFlags(S)V

    .line 8
    .line 9
    .line 10
    const/16 v1, 0xff

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;->setRowHeight(S)V

    .line 13
    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static createDelta()Lcom/intsig/office/fc/hssf/record/DeltaRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/DeltaRecord;

    .line 2
    .line 3
    const-wide v1, 0x3f50624dd2f1a9fcL    # 0.001

    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/hssf/record/DeltaRecord;-><init>(D)V

    .line 9
    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static createDimensions()Lcom/intsig/office/fc/hssf/record/DimensionsRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setFirstCol(S)V

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setLastRow(I)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setFirstRow(I)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setLastCol(S)V

    .line 18
    .line 19
    .line 20
    return-object v0
    .line 21
.end method

.method private static createGridset()Lcom/intsig/office/fc/hssf/record/GridsetRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/GridsetRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/GridsetRecord;->setGridset(Z)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static createGuts()Lcom/intsig/office/fc/hssf/record/GutsRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/GutsRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/GutsRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/GutsRecord;->setLeftRowGutter(S)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/GutsRecord;->setTopColGutter(S)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/GutsRecord;->setRowLevelMax(S)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/GutsRecord;->setColLevelMax(S)V

    .line 17
    .line 18
    .line 19
    return-object v0
    .line 20
    .line 21
.end method

.method private static createIteration()Lcom/intsig/office/fc/hssf/record/IterationRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/IterationRecord;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/record/IterationRecord;-><init>(Z)V

    .line 5
    .line 6
    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static createPrintGridlines()Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;->setPrintGridlines(Z)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static createPrintHeaders()Lcom/intsig/office/fc/hssf/record/PrintHeadersRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/PrintHeadersRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/PrintHeadersRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PrintHeadersRecord;->setPrintHeaders(Z)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static createRefMode()Lcom/intsig/office/fc/hssf/record/RefModeRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/RefModeRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/RefModeRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/RefModeRecord;->setMode(S)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static createSaveRecalc()Lcom/intsig/office/fc/hssf/record/SaveRecalcRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/SaveRecalcRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/SaveRecalcRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/SaveRecalcRecord;->setRecalc(Z)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static createSelection()Lcom/intsig/office/fc/hssf/record/SelectionRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1, v1}, Lcom/intsig/office/fc/hssf/record/SelectionRecord;-><init>(II)V

    .line 5
    .line 6
    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static createSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;
    .locals 1

    .line 3
    new-instance v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;

    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;-><init>()V

    return-object v0
.end method

.method public static createSheet(Lcom/intsig/office/fc/hssf/model/RecordStream;)Lcom/intsig/office/fc/hssf/model/InternalSheet;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;-><init>(Lcom/intsig/office/fc/hssf/model/RecordStream;Lcom/intsig/office/system/AbstractReader;)V

    return-object v0
.end method

.method public static createSheet(Lcom/intsig/office/fc/hssf/model/RecordStream;Lcom/intsig/office/system/AbstractReader;)Lcom/intsig/office/fc/hssf/model/InternalSheet;
    .locals 1

    .line 2
    new-instance v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;

    invoke-direct {v0, p0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;-><init>(Lcom/intsig/office/fc/hssf/model/RecordStream;Lcom/intsig/office/system/AbstractReader;)V

    return-object v0
.end method

.method private static createWSBool()Lcom/intsig/office/fc/hssf/record/WSBoolRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x4

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->setWSBool1(B)V

    .line 8
    .line 9
    .line 10
    const/16 v1, -0x3f

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->setWSBool2(B)V

    .line 13
    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static createWindowTwo()Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0x6b6

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setOptions(S)V

    .line 9
    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setTopRow(S)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setLeftCol(S)V

    .line 16
    .line 17
    .line 18
    const/16 v2, 0x40

    .line 19
    .line 20
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setHeaderColor(I)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setPageBreakZoom(S)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setNormalZoom(S)V

    .line 27
    .line 28
    .line 29
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private getGutsRecord()Lcom/intsig/office/fc/hssf/record/GutsRecord;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_gutsRecord:Lcom/intsig/office/fc/hssf/record/GutsRecord;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createGuts()Lcom/intsig/office/fc/hssf/record/GutsRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 10
    .line 11
    invoke-static {v1, v0}, Lcom/intsig/office/fc/hssf/model/RecordOrderer;->〇080(Ljava/util/List;Lcom/intsig/office/fc/hssf/record/RecordBase;)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_gutsRecord:Lcom/intsig/office/fc/hssf/record/GutsRecord;

    .line 15
    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_gutsRecord:Lcom/intsig/office/fc/hssf/record/GutsRecord;

    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
.end method

.method private getMergedRecords()Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_mergedCellsTable:Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private getSizeOfInitialSheetRecords(I)I
    .locals 3

    .line 1
    add-int/lit8 p1, p1, 0x1

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 5
    .line 6
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-ge p1, v1, :cond_1

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    check-cast v1, Lcom/intsig/office/fc/hssf/record/RecordBase;

    .line 19
    .line 20
    instance-of v2, v1, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 21
    .line 22
    if-eqz v2, :cond_0

    .line 23
    .line 24
    goto :goto_1

    .line 25
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/RecordBase;->getRecordSize()I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    add-int/2addr v0, v1

    .line 30
    add-int/lit8 p1, p1, 0x1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    :goto_1
    iget-boolean p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_isUncalced:Z

    .line 34
    .line 35
    if-eqz p1, :cond_2

    .line 36
    .line 37
    invoke-static {}, Lcom/intsig/office/fc/hssf/record/UncalcedRecord;->getStaticRecordSize()I

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    add-int/2addr v0, p1

    .line 42
    :cond_2
    return v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private recalcRowGutter()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getIterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    check-cast v2, Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 19
    .line 20
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getOutlineLevel()S

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    goto :goto_0

    .line 29
    :cond_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getGutsRecord()Lcom/intsig/office/fc/hssf/record/GutsRecord;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    add-int/lit8 v2, v1, 0x1

    .line 34
    .line 35
    int-to-short v2, v2

    .line 36
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/GutsRecord;->setRowLevelMax(S)V

    .line 37
    .line 38
    .line 39
    mul-int/lit8 v1, v1, 0xc

    .line 40
    .line 41
    add-int/lit8 v1, v1, 0x1d

    .line 42
    .line 43
    int-to-short v1, v1

    .line 44
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/GutsRecord;->setLeftRowGutter(S)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private setColumn(ILjava/lang/Short;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    .line 2
    .line 3
    move v1, p1

    .line 4
    move-object v2, p2

    .line 5
    move-object v3, p3

    .line 6
    move-object v4, p4

    .line 7
    move-object v5, p5

    .line 8
    move-object v6, p6

    .line 9
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;->setColumn(ILjava/lang/Short;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
.end method

.method private static spillAggregate(Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/RecordBase;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/model/InternalSheet$1;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet$1;-><init>(Ljava/util/List;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;->visitContainedRecords(Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public addMergedRegion(IIII)I
    .locals 2

    .line 1
    const-string v0, ")"

    .line 2
    .line 3
    if-lt p3, p1, :cond_1

    .line 4
    .line 5
    if-lt p4, p2, :cond_0

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getMergedRecords()Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;->addArea(IIII)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;->getNumberOfMergedRegions()I

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    add-int/lit8 p1, p1, -0x1

    .line 19
    .line 20
    return p1

    .line 21
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 22
    .line 23
    new-instance p3, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string v1, "The \'to\' col ("

    .line 29
    .line 30
    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string p4, ") must not be less than the \'from\' col ("

    .line 37
    .line 38
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object p2

    .line 51
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    throw p1

    .line 55
    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    .line 56
    .line 57
    new-instance p4, Ljava/lang/StringBuilder;

    .line 58
    .line 59
    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .line 61
    .line 62
    const-string v1, "The \'to\' row ("

    .line 63
    .line 64
    invoke-virtual {p4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    const-string p3, ") must not be less than the \'from\' row ("

    .line 71
    .line 72
    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    throw p2
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public addRow(Lcom/intsig/office/fc/hssf/record/RowRecord;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 2
    .line 3
    sget v1, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/POILogger;->check(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 12
    .line 13
    sget v1, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 14
    .line 15
    const-string v2, "addRow "

    .line 16
    .line 17
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dimensions:Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->getLastRow()I

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-lt v1, v2, :cond_1

    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    add-int/lit8 v1, v1, 0x1

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setLastRow(I)V

    .line 39
    .line 40
    .line 41
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->getFirstRow()I

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    if-ge v1, v2, :cond_2

    .line 50
    .line 51
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setFirstRow(I)V

    .line 56
    .line 57
    .line 58
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 59
    .line 60
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    if-eqz v0, :cond_3

    .line 69
    .line 70
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 71
    .line 72
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->removeRow(Lcom/intsig/office/fc/hssf/record/RowRecord;)V

    .line 73
    .line 74
    .line 75
    :cond_3
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 76
    .line 77
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->insertRow(Lcom/intsig/office/fc/hssf/record/RowRecord;)V

    .line 78
    .line 79
    .line 80
    sget-object p1, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 81
    .line 82
    sget v0, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 83
    .line 84
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/util/POILogger;->check(I)Z

    .line 85
    .line 86
    .line 87
    move-result p1

    .line 88
    if-eqz p1, :cond_4

    .line 89
    .line 90
    sget-object p1, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 91
    .line 92
    sget v0, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 93
    .line 94
    const-string v1, "exit addRow"

    .line 95
    .line 96
    invoke-virtual {p1, v0, v1}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 97
    .line 98
    .line 99
    :cond_4
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public addValueRecord(ILcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 2
    .line 3
    sget v1, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/POILogger;->check(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 12
    .line 13
    sget v1, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 14
    .line 15
    new-instance v2, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v3, "add value record  row"

    .line 21
    .line 22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 33
    .line 34
    .line 35
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dimensions:Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    .line 36
    .line 37
    invoke-interface {p2}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getColumn()S

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->getLastCol()S

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    if-le v0, v1, :cond_1

    .line 46
    .line 47
    invoke-interface {p2}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getColumn()S

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    add-int/lit8 v0, v0, 0x1

    .line 52
    .line 53
    int-to-short v0, v0

    .line 54
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setLastCol(S)V

    .line 55
    .line 56
    .line 57
    :cond_1
    invoke-interface {p2}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getColumn()S

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->getFirstCol()S

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    if-ge v0, v1, :cond_2

    .line 66
    .line 67
    invoke-interface {p2}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getColumn()S

    .line 68
    .line 69
    .line 70
    move-result v0

    .line 71
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setFirstCol(S)V

    .line 72
    .line 73
    .line 74
    :cond_2
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 75
    .line 76
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->insertCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V

    .line 77
    .line 78
    .line 79
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public aggregateDrawingRecords(Lcom/intsig/office/fc/hssf/model/DrawingManager2;Z)I
    .locals 6

    .line 1
    const/16 v0, 0xec

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    const/4 v2, 0x1

    .line 9
    const/4 v3, -0x1

    .line 10
    if-ne v0, v3, :cond_0

    .line 11
    .line 12
    const/4 v4, 0x1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v4, 0x0

    .line 15
    :goto_0
    if-eqz v4, :cond_3

    .line 16
    .line 17
    if-nez p2, :cond_1

    .line 18
    .line 19
    return v3

    .line 20
    :cond_1
    new-instance p2, Lcom/intsig/office/fc/hssf/record/EscherAggregate;

    .line 21
    .line 22
    invoke-direct {p2, p1}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;-><init>(Lcom/intsig/office/fc/hssf/model/DrawingManager2;)V

    .line 23
    .line 24
    .line 25
    const/16 p1, 0x2694

    .line 26
    .line 27
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    if-ne p1, v3, :cond_2

    .line 32
    .line 33
    const/16 p1, 0x23e

    .line 34
    .line 35
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    goto :goto_1

    .line 40
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 52
    .line 53
    .line 54
    return p1

    .line 55
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    .line 56
    .line 57
    .line 58
    move-result-object p2

    .line 59
    invoke-static {p2, v0, p1}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->createAggregate(Ljava/util/List;ILcom/intsig/office/fc/hssf/model/DrawingManager2;)Lcom/intsig/office/fc/hssf/record/EscherAggregate;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    move v3, v0

    .line 64
    :goto_2
    add-int/lit8 v4, v3, 0x1

    .line 65
    .line 66
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 67
    .line 68
    .line 69
    move-result v5

    .line 70
    if-ge v4, v5, :cond_6

    .line 71
    .line 72
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 73
    .line 74
    .line 75
    move-result-object v5

    .line 76
    instance-of v5, v5, Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 77
    .line 78
    if-nez v5, :cond_4

    .line 79
    .line 80
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 81
    .line 82
    .line 83
    move-result-object v5

    .line 84
    instance-of v5, v5, Lcom/intsig/office/fc/hssf/record/ContinueRecord;

    .line 85
    .line 86
    if-eqz v5, :cond_6

    .line 87
    .line 88
    :cond_4
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 89
    .line 90
    .line 91
    move-result-object v5

    .line 92
    instance-of v5, v5, Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 93
    .line 94
    if-nez v5, :cond_5

    .line 95
    .line 96
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 97
    .line 98
    .line 99
    move-result-object v4

    .line 100
    instance-of v4, v4, Lcom/intsig/office/fc/hssf/record/TextObjectRecord;

    .line 101
    .line 102
    if-eqz v4, :cond_6

    .line 103
    .line 104
    :cond_5
    invoke-static {p2, v3}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->shapeContainRecords(Ljava/util/List;I)I

    .line 105
    .line 106
    .line 107
    move-result v4

    .line 108
    add-int/2addr v3, v4

    .line 109
    goto :goto_2

    .line 110
    :cond_6
    sub-int/2addr v3, v2

    .line 111
    :goto_3
    sub-int v4, v3, v0

    .line 112
    .line 113
    add-int/2addr v4, v2

    .line 114
    if-ge v1, v4, :cond_7

    .line 115
    .line 116
    invoke-interface {p2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 117
    .line 118
    .line 119
    add-int/lit8 v1, v1, 0x1

    .line 120
    .line 121
    goto :goto_3

    .line 122
    :cond_7
    invoke-interface {p2, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 123
    .line 124
    .line 125
    return v0
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public cloneSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;
    .locals 5

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 10
    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    const/4 v2, 0x0

    .line 14
    :goto_0
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 15
    .line 16
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    if-ge v2, v3, :cond_1

    .line 21
    .line 22
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 23
    .line 24
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    check-cast v3, Lcom/intsig/office/fc/hssf/record/RecordBase;

    .line 29
    .line 30
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;

    .line 31
    .line 32
    if-eqz v4, :cond_0

    .line 33
    .line 34
    check-cast v3, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;

    .line 35
    .line 36
    new-instance v4, Lcom/intsig/office/fc/hssf/model/InternalSheet$RecordCloner;

    .line 37
    .line 38
    invoke-direct {v4, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet$RecordCloner;-><init>(Ljava/util/List;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;->visitContainedRecords(Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V

    .line 42
    .line 43
    .line 44
    goto :goto_1

    .line 45
    :cond_0
    check-cast v3, Lcom/intsig/office/fc/hssf/record/Record;

    .line 46
    .line 47
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/Record;->clone()Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    move-result-object v3

    .line 51
    check-cast v3, Lcom/intsig/office/fc/hssf/record/Record;

    .line 52
    .line 53
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    .line 55
    .line 56
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_1
    new-instance v2, Lcom/intsig/office/fc/hssf/model/RecordStream;

    .line 60
    .line 61
    invoke-direct {v2, v0, v1}, Lcom/intsig/office/fc/hssf/model/RecordStream;-><init>(Ljava/util/List;I)V

    .line 62
    .line 63
    .line 64
    invoke-static {v2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createSheet(Lcom/intsig/office/fc/hssf/model/RecordStream;)Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    return-object v0
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public createFreezePane(IIII)V
    .locals 5

    .line 1
    const/16 v0, 0x41

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, -0x1

    .line 8
    if-eq v0, v1, :cond_0

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    :cond_0
    const/16 v0, 0x1d

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    if-nez p1, :cond_1

    .line 19
    .line 20
    if-nez p2, :cond_1

    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 23
    .line 24
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setFreezePanes(Z)V

    .line 25
    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 28
    .line 29
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setFreezePanesNoSplit(Z)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    check-cast p1, Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    .line 37
    .line 38
    const/4 p2, 0x3

    .line 39
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/record/SelectionRecord;->setPane(B)V

    .line 40
    .line 41
    .line 42
    return-void

    .line 43
    :cond_1
    const/16 v2, 0x23e

    .line 44
    .line 45
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    new-instance v3, Lcom/intsig/office/fc/hssf/record/PaneRecord;

    .line 50
    .line 51
    invoke-direct {v3}, Lcom/intsig/office/fc/hssf/record/PaneRecord;-><init>()V

    .line 52
    .line 53
    .line 54
    int-to-short v4, p1

    .line 55
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->setX(S)V

    .line 56
    .line 57
    .line 58
    int-to-short v4, p2

    .line 59
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->setY(S)V

    .line 60
    .line 61
    .line 62
    int-to-short p3, p3

    .line 63
    invoke-virtual {v3, p3}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->setTopRow(S)V

    .line 64
    .line 65
    .line 66
    int-to-short p3, p4

    .line 67
    invoke-virtual {v3, p3}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->setLeftColumn(S)V

    .line 68
    .line 69
    .line 70
    const/4 p3, 0x1

    .line 71
    if-nez p2, :cond_2

    .line 72
    .line 73
    invoke-virtual {v3, v1}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->setTopRow(S)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v3, p3}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->setActivePane(S)V

    .line 77
    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_2
    if-nez p1, :cond_3

    .line 81
    .line 82
    invoke-virtual {v3, v1}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->setLeftColumn(S)V

    .line 83
    .line 84
    .line 85
    const/4 p1, 0x2

    .line 86
    invoke-virtual {v3, p1}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->setActivePane(S)V

    .line 87
    .line 88
    .line 89
    goto :goto_0

    .line 90
    :cond_3
    invoke-virtual {v3, v1}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->setActivePane(S)V

    .line 91
    .line 92
    .line 93
    :goto_0
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 94
    .line 95
    add-int/2addr v2, p3

    .line 96
    invoke-interface {p1, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 97
    .line 98
    .line 99
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 100
    .line 101
    invoke-virtual {p1, p3}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setFreezePanes(Z)V

    .line 102
    .line 103
    .line 104
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 105
    .line 106
    invoke-virtual {p1, p3}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setFreezePanesNoSplit(Z)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    check-cast p1, Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    .line 114
    .line 115
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->getActivePane()S

    .line 116
    .line 117
    .line 118
    move-result p2

    .line 119
    int-to-byte p2, p2

    .line 120
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/record/SelectionRecord;->setPane(B)V

    .line 121
    .line 122
    .line 123
    return-void
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public createSplitPane(IIIII)V
    .locals 2

    .line 1
    const/16 v0, 0x41

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, -0x1

    .line 8
    if-eq v0, v1, :cond_0

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    :cond_0
    const/16 v0, 0x23e

    .line 16
    .line 17
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    new-instance v1, Lcom/intsig/office/fc/hssf/record/PaneRecord;

    .line 22
    .line 23
    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/PaneRecord;-><init>()V

    .line 24
    .line 25
    .line 26
    int-to-short p1, p1

    .line 27
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->setX(S)V

    .line 28
    .line 29
    .line 30
    int-to-short p1, p2

    .line 31
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->setY(S)V

    .line 32
    .line 33
    .line 34
    int-to-short p1, p3

    .line 35
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->setTopRow(S)V

    .line 36
    .line 37
    .line 38
    int-to-short p1, p4

    .line 39
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->setLeftColumn(S)V

    .line 40
    .line 41
    .line 42
    int-to-short p1, p5

    .line 43
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->setActivePane(S)V

    .line 44
    .line 45
    .line 46
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 47
    .line 48
    add-int/lit8 v0, v0, 0x1

    .line 49
    .line 50
    invoke-interface {p1, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 51
    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 54
    .line 55
    const/4 p2, 0x0

    .line 56
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setFreezePanes(Z)V

    .line 57
    .line 58
    .line 59
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 60
    .line 61
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setFreezePanesNoSplit(Z)V

    .line 62
    .line 63
    .line 64
    const/16 p1, 0x1d

    .line 65
    .line 66
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    check-cast p1, Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    .line 71
    .line 72
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/record/SelectionRecord;->setPane(B)V

    .line 73
    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method public dispose()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->printGridlines:Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;

    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->gridset:Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_gutsRecord:Lcom/intsig/office/fc/hssf/record/GutsRecord;

    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->defaultcolwidth:Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;

    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->defaultrowheight:Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;

    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_psBlock:Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_selection:Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dimensions:Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dataValidityTable:Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;

    .line 26
    .line 27
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->condFormatting:Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    .line 28
    .line 29
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->rowRecIterator:Ljava/util/Iterator;

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->dispose()V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-gez p1, :cond_0

    .line 6
    .line 7
    const/4 p1, 0x0

    .line 8
    return-object p1

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 10
    .line 11
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    check-cast p1, Lcom/intsig/office/fc/hssf/record/Record;

    .line 16
    .line 17
    return-object p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public findFirstRecordLocBySid(S)I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    if-ge v1, v0, :cond_2

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    instance-of v3, v2, Lcom/intsig/office/fc/hssf/record/Record;

    .line 17
    .line 18
    if-nez v3, :cond_0

    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_0
    check-cast v2, Lcom/intsig/office/fc/hssf/record/Record;

    .line 22
    .line 23
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-ne v2, p1, :cond_1

    .line 28
    .line 29
    return v1

    .line 30
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_2
    const/4 p1, -0x1

    .line 34
    return p1
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getActiveCellCol()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_selection:Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/SelectionRecord;->getActiveCellCol()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    int-to-short v0, v0

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getActiveCellRow()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_selection:Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/SelectionRecord;->getActiveCellRow()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCellValueIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getCellValueIterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getChart()Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;
    .locals 5

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->sheetType:I

    .line 2
    .line 3
    const/16 v1, 0x20

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-ne v0, v1, :cond_1

    .line 7
    .line 8
    const/16 v0, 0x1002

    .line 9
    .line 10
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-ltz v0, :cond_1

    .line 15
    .line 16
    new-instance v1, Ljava/util/ArrayList;

    .line 17
    .line 18
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 19
    .line 20
    .line 21
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 22
    .line 23
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    check-cast v3, Lcom/intsig/office/fc/hssf/record/RecordBase;

    .line 28
    .line 29
    :goto_0
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;

    .line 30
    .line 31
    if-nez v4, :cond_0

    .line 32
    .line 33
    check-cast v3, Lcom/intsig/office/fc/hssf/record/Record;

    .line 34
    .line 35
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    add-int/lit8 v0, v0, 0x1

    .line 39
    .line 40
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 41
    .line 42
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    check-cast v3, Lcom/intsig/office/fc/hssf/record/RecordBase;

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;

    .line 50
    .line 51
    invoke-direct {v0, v2, v2, v2, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)V

    .line 52
    .line 53
    .line 54
    invoke-static {v1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->convertRecordsToChart(Ljava/util/List;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)V

    .line 55
    .line 56
    .line 57
    return-object v0

    .line 58
    :cond_1
    return-object v2
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getColumnInfo()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/util/ColumnInfo;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;->getNumColumns()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    new-instance v1, Ljava/util/ArrayList;

    .line 12
    .line 13
    const/4 v2, 0x5

    .line 14
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 15
    .line 16
    .line 17
    const/4 v2, 0x0

    .line 18
    :goto_0
    if-ge v2, v0, :cond_1

    .line 19
    .line 20
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    .line 21
    .line 22
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;->getColInfo(I)Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    new-instance v10, Lcom/intsig/office/fc/hssf/util/ColumnInfo;

    .line 27
    .line 28
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getFirstColumn()I

    .line 29
    .line 30
    .line 31
    move-result v5

    .line 32
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getLastColumn()I

    .line 33
    .line 34
    .line 35
    move-result v6

    .line 36
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getColumnWidth()I

    .line 37
    .line 38
    .line 39
    move-result v7

    .line 40
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getXFIndex()I

    .line 41
    .line 42
    .line 43
    move-result v8

    .line 44
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getHidden()Z

    .line 45
    .line 46
    .line 47
    move-result v9

    .line 48
    move-object v4, v10

    .line 49
    invoke-direct/range {v4 .. v9}, Lcom/intsig/office/fc/hssf/util/ColumnInfo;-><init>(IIIIZ)V

    .line 50
    .line 51
    .line 52
    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    .line 54
    .line 55
    add-int/lit8 v2, v2, 0x1

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_1
    return-object v1
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getColumnPixelWidth(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;->findColumnInfo(I)Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getColPixelWidth()I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    return p1

    .line 14
    :cond_0
    const/16 p1, 0x50

    .line 15
    .line 16
    return p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getColumnWidth(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;->findColumnInfo(I)Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getColumnWidth()I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    return p1

    .line 14
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->defaultcolwidth:Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;->getColWidth()I

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    mul-int/lit16 p1, p1, 0x100

    .line 21
    .line 22
    return p1
    .line 23
    .line 24
.end method

.method public getConditionalFormattingTable()Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->condFormatting:Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->condFormatting:Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 13
    .line 14
    invoke-static {v1, v0}, Lcom/intsig/office/fc/hssf/model/RecordOrderer;->〇080(Ljava/util/List;Lcom/intsig/office/fc/hssf/record/RecordBase;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->condFormatting:Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    .line 18
    .line 19
    return-object v0
    .line 20
    .line 21
.end method

.method public getDefaultColumnWidth()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->defaultcolwidth:Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;->getColWidth()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDefaultRowHeight()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->defaultrowheight:Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;->getRowHeight()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getGridsetRecord()Lcom/intsig/office/fc/hssf/record/GridsetRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->gridset:Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLeftCol()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->getLeftCol()S

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMergedRegionAt(I)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getMergedRecords()Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;->getNumberOfMergedRegions()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-lt p1, v1, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    return-object p1

    .line 13
    :cond_0
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;->get(I)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    return-object p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getNextRow()Lcom/intsig/office/fc/hssf/record/RowRecord;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->rowRecIterator:Ljava/util/Iterator;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getIterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->rowRecIterator:Ljava/util/Iterator;

    .line 12
    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->rowRecIterator:Ljava/util/Iterator;

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-nez v0, :cond_1

    .line 20
    .line 21
    const/4 v0, 0x0

    .line 22
    return-object v0

    .line 23
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->rowRecIterator:Ljava/util/Iterator;

    .line 24
    .line 25
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    check-cast v0, Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->rowRecIterator:Ljava/util/Iterator;

    .line 32
    .line 33
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 34
    .line 35
    .line 36
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getNoteRecords()[Lcom/intsig/office/fc/hssf/record/NoteRecord;
    .locals 5

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    const/4 v2, 0x1

    .line 13
    sub-int/2addr v1, v2

    .line 14
    :goto_0
    if-ltz v1, :cond_1

    .line 15
    .line 16
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 17
    .line 18
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v3

    .line 22
    check-cast v3, Lcom/intsig/office/fc/hssf/record/RecordBase;

    .line 23
    .line 24
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/record/NoteRecord;

    .line 25
    .line 26
    if-eqz v4, :cond_0

    .line 27
    .line 28
    check-cast v3, Lcom/intsig/office/fc/hssf/record/NoteRecord;

    .line 29
    .line 30
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    :cond_0
    add-int/lit8 v1, v1, -0x1

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    if-ge v1, v2, :cond_2

    .line 41
    .line 42
    sget-object v0, Lcom/intsig/office/fc/hssf/record/NoteRecord;->EMPTY_ARRAY:[Lcom/intsig/office/fc/hssf/record/NoteRecord;

    .line 43
    .line 44
    return-object v0

    .line 45
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    new-array v1, v1, [Lcom/intsig/office/fc/hssf/record/NoteRecord;

    .line 50
    .line 51
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    return-object v1
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getNumMergedRegions()I
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getMergedRecords()Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;->getNumberOfMergedRegions()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOrCreateDataValidityTable()Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dataValidityTable:Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;-><init>()V

    .line 8
    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 11
    .line 12
    invoke-static {v1, v0}, Lcom/intsig/office/fc/hssf/model/RecordOrderer;->〇080(Ljava/util/List;Lcom/intsig/office/fc/hssf/record/RecordBase;)V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dataValidityTable:Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;

    .line 16
    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dataValidityTable:Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;

    .line 18
    .line 19
    return-object v0
    .line 20
    .line 21
.end method

.method public getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_psBlock:Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_psBlock:Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 13
    .line 14
    invoke-static {v1, v0}, Lcom/intsig/office/fc/hssf/model/RecordOrderer;->〇080(Ljava/util/List;Lcom/intsig/office/fc/hssf/record/RecordBase;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_psBlock:Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 18
    .line 19
    return-object v0
    .line 20
    .line 21
.end method

.method public getPaneInformation()Lcom/intsig/office/fc/hssf/util/HSSFPaneInformation;
    .locals 9

    .line 1
    const/16 v0, 0x41

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/office/fc/hssf/record/PaneRecord;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    return-object v0

    .line 13
    :cond_0
    new-instance v8, Lcom/intsig/office/fc/hssf/util/HSSFPaneInformation;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->getX()S

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->getY()S

    .line 20
    .line 21
    .line 22
    move-result v3

    .line 23
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->getTopRow()S

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->getLeftColumn()S

    .line 28
    .line 29
    .line 30
    move-result v5

    .line 31
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/PaneRecord;->getActivePane()S

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    int-to-byte v6, v0

    .line 36
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->getFreezePanes()Z

    .line 39
    .line 40
    .line 41
    move-result v7

    .line 42
    move-object v1, v8

    .line 43
    invoke-direct/range {v1 .. v7}, Lcom/intsig/office/fc/hssf/util/HSSFPaneInformation;-><init>(SSSSBZ)V

    .line 44
    .line 45
    .line 46
    return-object v8
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getPrintGridlines()Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->printGridlines:Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getProtectionBlock()Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_protectionBlock:Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/RecordBase;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getRowsAggregate()Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSelection()Lcom/intsig/office/fc/hssf/record/SelectionRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_selection:Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTopRow()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->getTopRow()S

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getUncalced()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_isUncalced:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getValueRecords()[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getValueRecords()[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getWindowTwo()Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getXFIndexForColAt(S)S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;->findColumnInfo(I)Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getXFIndex()I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    int-to-short p1, p1

    .line 14
    return p1

    .line 15
    :cond_0
    const/16 p1, 0xf

    .line 16
    .line 17
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public groupColumnRange(IIZ)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;->groupColumnRange(IIZ)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;->getMaxOutlineLevel()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getGutsRecord()Lcom/intsig/office/fc/hssf/record/GutsRecord;

    .line 13
    .line 14
    .line 15
    move-result-object p2

    .line 16
    add-int/lit8 p3, p1, 0x1

    .line 17
    .line 18
    int-to-short p3, p3

    .line 19
    invoke-virtual {p2, p3}, Lcom/intsig/office/fc/hssf/record/GutsRecord;->setColLevelMax(S)V

    .line 20
    .line 21
    .line 22
    if-nez p1, :cond_0

    .line 23
    .line 24
    const/4 p1, 0x0

    .line 25
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/hssf/record/GutsRecord;->setTopColGutter(S)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 30
    .line 31
    mul-int/lit8 p1, p1, 0xc

    .line 32
    .line 33
    add-int/lit8 p1, p1, 0x1d

    .line 34
    .line 35
    int-to-short p1, p1

    .line 36
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/hssf/record/GutsRecord;->setTopColGutter(S)V

    .line 37
    .line 38
    .line 39
    :goto_0
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public groupRowRange(IIZ)V
    .locals 3

    .line 1
    :goto_0
    if-gt p1, p2, :cond_2

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->createRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->addRow(Lcom/intsig/office/fc/hssf/record/RowRecord;)V

    .line 14
    .line 15
    .line 16
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getOutlineLevel()S

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz p3, :cond_1

    .line 21
    .line 22
    add-int/lit8 v1, v1, 0x1

    .line 23
    .line 24
    goto :goto_1

    .line 25
    :cond_1
    add-int/lit8 v1, v1, -0x1

    .line 26
    .line 27
    :goto_1
    const/4 v2, 0x0

    .line 28
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    const/4 v2, 0x7

    .line 33
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    int-to-short v1, v1

    .line 38
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->setOutlineLevel(S)V

    .line 39
    .line 40
    .line 41
    add-int/lit8 p1, p1, 0x1

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_2
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->recalcRowGutter()V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public isChartSheet()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->sheetType:I

    .line 2
    .line 3
    const/16 v1, 0x20

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isColumnHidden(I)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;->findColumnInfo(I)Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-nez p1, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    return p1

    .line 11
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getHidden()Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    return p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public isDisplayFormulas()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->getDisplayFormulas()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isDisplayGridlines()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->getDisplayGridlines()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isDisplayRowColHeadings()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->getDisplayRowColHeadings()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isGridsPrinted()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->gridset:Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createGridset()Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->gridset:Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    .line 10
    .line 11
    const/16 v0, 0xa

    .line 12
    .line 13
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->gridset:Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    .line 20
    .line 21
    invoke-interface {v1, v0, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 22
    .line 23
    .line 24
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->gridset:Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/GridsetRecord;->getGridset()Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    xor-int/lit8 v0, v0, 0x1

    .line 31
    .line 32
    return v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public preSerialize()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_1

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    check-cast v1, Lcom/intsig/office/fc/hssf/record/RecordBase;

    .line 20
    .line 21
    instance-of v2, v1, Lcom/intsig/office/fc/hssf/record/EscherAggregate;

    .line 22
    .line 23
    if-eqz v2, :cond_0

    .line 24
    .line 25
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/RecordBase;->getRecordSize()I

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public removeMergedRegion(I)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getMergedRecords()Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;->getNumberOfMergedRegions()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-lt p1, v1, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/MergedCellsTable;->remove(I)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public removeRow(Lcom/intsig/office/fc/hssf/record/RowRecord;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->removeRow(Lcom/intsig/office/fc/hssf/record/RowRecord;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public removeValueRecord(ILcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 2
    .line 3
    sget v1, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    new-array v2, v2, [I

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    aput p1, v2, v3

    .line 10
    .line 11
    const-string p1, "remove value record row %"

    .line 12
    .line 13
    invoke-virtual {v0, v1, p1, v2}, Lcom/intsig/office/fc/util/POILogger;->logFormatted(ILjava/lang/String;Ljava/lang/Object;)V

    .line 14
    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 17
    .line 18
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->removeCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public replaceValueRecord(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 2
    .line 3
    sget v1, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/POILogger;->check(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 12
    .line 13
    sget v1, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 14
    .line 15
    const-string v2, "replaceValueRecord "

    .line 16
    .line 17
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 21
    .line 22
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->removeCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 26
    .line 27
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->insertCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setActiveCellCol(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_selection:Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/SelectionRecord;->setActiveCellCol(S)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setActiveCellRow(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_selection:Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/SelectionRecord;->setActiveCellRow(I)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setColumnGroupCollapsed(IZ)V
    .locals 0

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    .line 4
    .line 5
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;->collapseColumn(I)V

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    .line 10
    .line 11
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;->expandColumn(I)V

    .line 12
    .line 13
    .line 14
    :goto_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setColumnHidden(IZ)V
    .locals 7

    .line 1
    const/4 v2, 0x0

    .line 2
    const/4 v3, 0x0

    .line 3
    const/4 v4, 0x0

    .line 4
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 5
    .line 6
    .line 7
    move-result-object v5

    .line 8
    const/4 v6, 0x0

    .line 9
    move-object v0, p0

    .line 10
    move v1, p1

    .line 11
    invoke-direct/range {v0 .. v6}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setColumn(ILjava/lang/Short;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setColumnPixelWidth(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_columnInfos:Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ColumnInfoRecordsAggregate;->findColumnInfo(I)Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->setColPixelWidth(I)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setColumnWidth(II)V
    .locals 8

    .line 1
    const v0, 0xff00

    .line 2
    .line 3
    .line 4
    if-gt p2, v0, :cond_0

    .line 5
    .line 6
    const/4 v3, 0x0

    .line 7
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object v4

    .line 11
    const/4 v5, 0x0

    .line 12
    const/4 v6, 0x0

    .line 13
    const/4 v7, 0x0

    .line 14
    move-object v1, p0

    .line 15
    move v2, p1

    .line 16
    invoke-direct/range {v1 .. v7}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setColumn(ILjava/lang/Short;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 17
    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 21
    .line 22
    const-string p2, "The maximum column width for an individual cell is 255 characters."

    .line 23
    .line 24
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    throw p1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setDefaultColumnStyle(II)V
    .locals 7

    .line 1
    int-to-short p2, p2

    .line 2
    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    .line 3
    .line 4
    .line 5
    move-result-object v2

    .line 6
    const/4 v3, 0x0

    .line 7
    const/4 v4, 0x0

    .line 8
    const/4 v5, 0x0

    .line 9
    const/4 v6, 0x0

    .line 10
    move-object v0, p0

    .line 11
    move v1, p1

    .line 12
    invoke-direct/range {v0 .. v6}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setColumn(ILjava/lang/Short;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setDefaultColumnWidth(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->defaultcolwidth:Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;->setColWidth(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDefaultRowHeight(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->defaultrowheight:Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;->setRowHeight(S)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDimensions(ISIS)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 2
    .line 3
    sget v1, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/POILogger;->check(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 12
    .line 13
    sget v1, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 14
    .line 15
    const-string v2, "Sheet.setDimensions"

    .line 16
    .line 17
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 18
    .line 19
    .line 20
    sget-object v0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 21
    .line 22
    sget v1, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 23
    .line 24
    new-instance v2, Ljava/lang/StringBuffer;

    .line 25
    .line 26
    const-string v3, "firstrow"

    .line 27
    .line 28
    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 32
    .line 33
    .line 34
    const-string v3, "firstcol"

    .line 35
    .line 36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 40
    .line 41
    .line 42
    const-string v3, "lastrow"

    .line 43
    .line 44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 48
    .line 49
    .line 50
    const-string v3, "lastcol"

    .line 51
    .line 52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v2, p4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v2

    .line 62
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 63
    .line 64
    .line 65
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dimensions:Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    .line 66
    .line 67
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setFirstCol(S)V

    .line 68
    .line 69
    .line 70
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dimensions:Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    .line 71
    .line 72
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setFirstRow(I)V

    .line 73
    .line 74
    .line 75
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dimensions:Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    .line 76
    .line 77
    invoke-virtual {p1, p4}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setLastCol(S)V

    .line 78
    .line 79
    .line 80
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_dimensions:Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    .line 81
    .line 82
    invoke-virtual {p1, p3}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setLastRow(I)V

    .line 83
    .line 84
    .line 85
    sget-object p1, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 86
    .line 87
    sget p2, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 88
    .line 89
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/util/POILogger;->check(I)Z

    .line 90
    .line 91
    .line 92
    move-result p1

    .line 93
    if-eqz p1, :cond_1

    .line 94
    .line 95
    sget-object p1, Lcom/intsig/office/fc/hssf/model/InternalSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 96
    .line 97
    sget p2, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 98
    .line 99
    const-string p3, "Sheet.setDimensions exiting"

    .line 100
    .line 101
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 102
    .line 103
    .line 104
    :cond_1
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public setDisplayFormulas(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setDisplayFormulas(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDisplayGridlines(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setDisplayGridlines(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDisplayRowColHeadings(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setDisplayRowColHeadings(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setGridsPrinted(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->gridset:Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    .line 2
    .line 3
    xor-int/lit8 p1, p1, 0x1

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/GridsetRecord;->setGridset(Z)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLeftCol(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setLeftCol(S)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPrintGridlines(Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->printGridlines:Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSCLRecord(Lcom/intsig/office/fc/hssf/record/SCLRecord;)V
    .locals 2

    .line 1
    const/16 v0, 0xa0

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, -0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    const/16 v0, 0x23e

    .line 11
    .line 12
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 17
    .line 18
    add-int/lit8 v0, v0, 0x1

    .line 19
    .line 20
    invoke-interface {v1, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 25
    .line 26
    invoke-interface {v1, v0, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    :goto_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setSelected(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setSelected(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSelection(Lcom/intsig/office/fc/hssf/record/SelectionRecord;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_selection:Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTopRow(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->windowTwo:Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setTopRow(S)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setUncalced(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_isUncalced:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public updateFormulasAfterCellShift(Lcom/intsig/office/fc/hssf/formula/FormulaShifter;I)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getRowsAggregate()Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->updateFormulasAfterRowShift(Lcom/intsig/office/fc/hssf/formula/FormulaShifter;I)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->condFormatting:Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getConditionalFormattingTable()Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;->updateFormulasAfterCellShift(Lcom/intsig/office/fc/hssf/formula/FormulaShifter;I)V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public visitContainedRecords(Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;I)V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;-><init>(Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;I)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x0

    .line 7
    const/4 p2, 0x0

    .line 8
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 9
    .line 10
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-ge p1, v1, :cond_4

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_records:Ljava/util/List;

    .line 17
    .line 18
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Lcom/intsig/office/fc/hssf/record/RecordBase;

    .line 23
    .line 24
    instance-of v2, v1, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;

    .line 25
    .line 26
    if-eqz v2, :cond_0

    .line 27
    .line 28
    move-object v2, v1

    .line 29
    check-cast v2, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;

    .line 30
    .line 31
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;->visitContainedRecords(Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V

    .line 32
    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_0
    move-object v2, v1

    .line 36
    check-cast v2, Lcom/intsig/office/fc/hssf/record/Record;

    .line 37
    .line 38
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;->visitRecord(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 39
    .line 40
    .line 41
    :goto_1
    instance-of v1, v1, Lcom/intsig/office/fc/hssf/record/BOFRecord;

    .line 42
    .line 43
    if-eqz v1, :cond_3

    .line 44
    .line 45
    if-nez p2, :cond_3

    .line 46
    .line 47
    iget-boolean p2, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_isUncalced:Z

    .line 48
    .line 49
    if-eqz p2, :cond_1

    .line 50
    .line 51
    new-instance p2, Lcom/intsig/office/fc/hssf/record/UncalcedRecord;

    .line 52
    .line 53
    invoke-direct {p2}, Lcom/intsig/office/fc/hssf/record/UncalcedRecord;-><init>()V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;->visitRecord(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 57
    .line 58
    .line 59
    :cond_1
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 60
    .line 61
    if-eqz p2, :cond_2

    .line 62
    .line 63
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getSizeOfInitialSheetRecords(I)I

    .line 64
    .line 65
    .line 66
    move-result p2

    .line 67
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;->getPosition()I

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/model/InternalSheet;->_rowsAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 72
    .line 73
    invoke-virtual {v2, v1, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->createIndexRecord(II)Lcom/intsig/office/fc/hssf/record/IndexRecord;

    .line 74
    .line 75
    .line 76
    move-result-object p2

    .line 77
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;->visitRecord(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 78
    .line 79
    .line 80
    :cond_2
    const/4 p2, 0x1

    .line 81
    :cond_3
    add-int/lit8 p1, p1, 0x1

    .line 82
    .line 83
    goto :goto_0

    .line 84
    :cond_4
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

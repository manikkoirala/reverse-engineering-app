.class public final Lcom/intsig/office/fc/hssf/record/CFRuleRecord;
.super Lcom/intsig/office/fc/hssf/record/StandardRecord;
.source "CFRuleRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/record/CFRuleRecord$ComparisonOperator;
    }
.end annotation


# static fields
.field public static final CONDITION_TYPE_CELL_VALUE_IS:B = 0x1t

.field public static final CONDITION_TYPE_FORMULA:B = 0x2t

.field private static final align:Lcom/intsig/office/fc/util/BitField;

.field private static final alignHor:Lcom/intsig/office/fc/util/BitField;

.field private static final alignIndent:Lcom/intsig/office/fc/util/BitField;

.field private static final alignJustLast:Lcom/intsig/office/fc/util/BitField;

.field private static final alignRot:Lcom/intsig/office/fc/util/BitField;

.field private static final alignShrin:Lcom/intsig/office/fc/util/BitField;

.field private static final alignTextDir:Lcom/intsig/office/fc/util/BitField;

.field private static final alignVer:Lcom/intsig/office/fc/util/BitField;

.field private static final alignWrap:Lcom/intsig/office/fc/util/BitField;

.field private static final bord:Lcom/intsig/office/fc/util/BitField;

.field private static final bordBlTr:Lcom/intsig/office/fc/util/BitField;

.field private static final bordBot:Lcom/intsig/office/fc/util/BitField;

.field private static final bordLeft:Lcom/intsig/office/fc/util/BitField;

.field private static final bordRight:Lcom/intsig/office/fc/util/BitField;

.field private static final bordTlBr:Lcom/intsig/office/fc/util/BitField;

.field private static final bordTop:Lcom/intsig/office/fc/util/BitField;

.field private static final fmtBlockBits:Lcom/intsig/office/fc/util/BitField;

.field private static final font:Lcom/intsig/office/fc/util/BitField;

.field private static final modificationBits:Lcom/intsig/office/fc/util/BitField;

.field private static final notUsed1:Lcom/intsig/office/fc/util/BitField;

.field private static final notUsed2:Lcom/intsig/office/fc/util/BitField;

.field private static final patt:Lcom/intsig/office/fc/util/BitField;

.field private static final pattBgCol:Lcom/intsig/office/fc/util/BitField;

.field private static final pattCol:Lcom/intsig/office/fc/util/BitField;

.field private static final pattStyle:Lcom/intsig/office/fc/util/BitField;

.field private static final prot:Lcom/intsig/office/fc/util/BitField;

.field private static final protHidden:Lcom/intsig/office/fc/util/BitField;

.field private static final protLocked:Lcom/intsig/office/fc/util/BitField;

.field public static final sid:S = 0x1b1s

.field private static final undocumented:Lcom/intsig/office/fc/util/BitField;


# instance fields
.field private _borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

.field private _fontFormatting:Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;

.field private _patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

.field private field_17_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

.field private field_18_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

.field private field_1_condition_type:B

.field private field_2_comparison_operator:B

.field private field_5_options:I

.field private field_6_not_used:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const v0, 0x3fffff

    .line 2
    .line 3
    .line 4
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->modificationBits:Lcom/intsig/office/fc/util/BitField;

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->alignHor:Lcom/intsig/office/fc/util/BitField;

    .line 16
    .line 17
    const/4 v0, 0x2

    .line 18
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->alignVer:Lcom/intsig/office/fc/util/BitField;

    .line 23
    .line 24
    const/4 v0, 0x4

    .line 25
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->alignWrap:Lcom/intsig/office/fc/util/BitField;

    .line 30
    .line 31
    const/16 v0, 0x8

    .line 32
    .line 33
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->alignRot:Lcom/intsig/office/fc/util/BitField;

    .line 38
    .line 39
    const/16 v0, 0x10

    .line 40
    .line 41
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->alignJustLast:Lcom/intsig/office/fc/util/BitField;

    .line 46
    .line 47
    const/16 v0, 0x20

    .line 48
    .line 49
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->alignIndent:Lcom/intsig/office/fc/util/BitField;

    .line 54
    .line 55
    const/16 v0, 0x40

    .line 56
    .line 57
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->alignShrin:Lcom/intsig/office/fc/util/BitField;

    .line 62
    .line 63
    const/16 v0, 0x80

    .line 64
    .line 65
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->notUsed1:Lcom/intsig/office/fc/util/BitField;

    .line 70
    .line 71
    const/16 v0, 0x100

    .line 72
    .line 73
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->protLocked:Lcom/intsig/office/fc/util/BitField;

    .line 78
    .line 79
    const/16 v0, 0x200

    .line 80
    .line 81
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->protHidden:Lcom/intsig/office/fc/util/BitField;

    .line 86
    .line 87
    const/16 v0, 0x400

    .line 88
    .line 89
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordLeft:Lcom/intsig/office/fc/util/BitField;

    .line 94
    .line 95
    const/16 v0, 0x800

    .line 96
    .line 97
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordRight:Lcom/intsig/office/fc/util/BitField;

    .line 102
    .line 103
    const/16 v0, 0x1000

    .line 104
    .line 105
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordTop:Lcom/intsig/office/fc/util/BitField;

    .line 110
    .line 111
    const/16 v0, 0x2000

    .line 112
    .line 113
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordBot:Lcom/intsig/office/fc/util/BitField;

    .line 118
    .line 119
    const/16 v0, 0x4000

    .line 120
    .line 121
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordTlBr:Lcom/intsig/office/fc/util/BitField;

    .line 126
    .line 127
    const v0, 0x8000

    .line 128
    .line 129
    .line 130
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 131
    .line 132
    .line 133
    move-result-object v0

    .line 134
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordBlTr:Lcom/intsig/office/fc/util/BitField;

    .line 135
    .line 136
    const/high16 v0, 0x10000

    .line 137
    .line 138
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 139
    .line 140
    .line 141
    move-result-object v0

    .line 142
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->pattStyle:Lcom/intsig/office/fc/util/BitField;

    .line 143
    .line 144
    const/high16 v0, 0x20000

    .line 145
    .line 146
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->pattCol:Lcom/intsig/office/fc/util/BitField;

    .line 151
    .line 152
    const/high16 v0, 0x40000

    .line 153
    .line 154
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 155
    .line 156
    .line 157
    move-result-object v0

    .line 158
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->pattBgCol:Lcom/intsig/office/fc/util/BitField;

    .line 159
    .line 160
    const/high16 v0, 0x380000

    .line 161
    .line 162
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 163
    .line 164
    .line 165
    move-result-object v0

    .line 166
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->notUsed2:Lcom/intsig/office/fc/util/BitField;

    .line 167
    .line 168
    const/high16 v0, 0x3c00000

    .line 169
    .line 170
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 171
    .line 172
    .line 173
    move-result-object v0

    .line 174
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->undocumented:Lcom/intsig/office/fc/util/BitField;

    .line 175
    .line 176
    const/high16 v0, 0x7c000000

    .line 177
    .line 178
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 179
    .line 180
    .line 181
    move-result-object v0

    .line 182
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->fmtBlockBits:Lcom/intsig/office/fc/util/BitField;

    .line 183
    .line 184
    const/high16 v0, 0x4000000

    .line 185
    .line 186
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 187
    .line 188
    .line 189
    move-result-object v0

    .line 190
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->font:Lcom/intsig/office/fc/util/BitField;

    .line 191
    .line 192
    const/high16 v0, 0x8000000

    .line 193
    .line 194
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 195
    .line 196
    .line 197
    move-result-object v0

    .line 198
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->align:Lcom/intsig/office/fc/util/BitField;

    .line 199
    .line 200
    const/high16 v0, 0x10000000

    .line 201
    .line 202
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 203
    .line 204
    .line 205
    move-result-object v0

    .line 206
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bord:Lcom/intsig/office/fc/util/BitField;

    .line 207
    .line 208
    const/high16 v0, 0x20000000

    .line 209
    .line 210
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 211
    .line 212
    .line 213
    move-result-object v0

    .line 214
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->patt:Lcom/intsig/office/fc/util/BitField;

    .line 215
    .line 216
    const/high16 v0, 0x40000000    # 2.0f

    .line 217
    .line 218
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 219
    .line 220
    .line 221
    move-result-object v0

    .line 222
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->prot:Lcom/intsig/office/fc/util/BitField;

    .line 223
    .line 224
    const/high16 v0, -0x80000000

    .line 225
    .line 226
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bf(I)Lcom/intsig/office/fc/util/BitField;

    .line 227
    .line 228
    .line 229
    move-result-object v0

    .line 230
    sput-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->alignTextDir:Lcom/intsig/office/fc/util/BitField;

    .line 231
    .line 232
    return-void
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private constructor <init>(BB)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    .line 2
    iput-byte p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_1_condition_type:B

    .line 3
    iput-byte p2, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_2_comparison_operator:B

    .line 4
    sget-object p1, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->modificationBits:Lcom/intsig/office/fc/util/BitField;

    iget p2, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_5_options:I

    const/4 v0, -0x1

    invoke-virtual {p1, p2, v0}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_5_options:I

    .line 5
    sget-object p2, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->fmtBlockBits:Lcom/intsig/office/fc/util/BitField;

    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_5_options:I

    .line 6
    sget-object p2, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->undocumented:Lcom/intsig/office/fc/util/BitField;

    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/util/BitField;->clear(I)I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_5_options:I

    const/16 p1, -0x7ffe

    .line 7
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_6_not_used:S

    const/4 p1, 0x0

    .line 8
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_fontFormatting:Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;

    .line 9
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 10
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 11
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->EMPTY_PTG_ARRAY:[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->create([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Lcom/intsig/office/fc/hssf/formula/Formula;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_17_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 12
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->create([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Lcom/intsig/office/fc/hssf/formula/Formula;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_18_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

    return-void
.end method

.method private constructor <init>(BB[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;-><init>(BB)V

    .line 14
    invoke-static {p3}, Lcom/intsig/office/fc/hssf/formula/Formula;->create([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Lcom/intsig/office/fc/hssf/formula/Formula;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_17_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 15
    invoke-static {p4}, Lcom/intsig/office/fc/hssf/formula/Formula;->create([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Lcom/intsig/office/fc/hssf/formula/Formula;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_18_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V
    .locals 3

    .line 16
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_1_condition_type:B

    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_2_comparison_operator:B

    .line 19
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUShort()I

    move-result v0

    .line 20
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUShort()I

    move-result v1

    .line 21
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readInt()I

    move-result v2

    iput v2, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_5_options:I

    .line 22
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v2

    iput-short v2, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_6_not_used:S

    .line 23
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->containsFontFormattingBlock()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 24
    new-instance v2, Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;

    invoke-direct {v2, p1}, Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;-><init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V

    iput-object v2, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_fontFormatting:Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;

    .line 25
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->containsBorderFormattingBlock()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 26
    new-instance v2, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    invoke-direct {v2, p1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    iput-object v2, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 27
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->containsPatternFormattingBlock()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 28
    new-instance v2, Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    invoke-direct {v2, p1}, Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    iput-object v2, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 29
    :cond_2
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->read(ILcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/Formula;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_17_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 30
    invoke-static {v1, p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->read(ILcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/Formula;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_18_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

    return-void
.end method

.method private static bf(I)Lcom/intsig/office/fc/util/BitField;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static create(Lcom/intsig/office/ss/model/XLSModel/ASheet;BLjava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/hssf/record/CFRuleRecord;
    .locals 1

    .line 3
    invoke-static {p2, p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->parseFormula(Ljava/lang/String;Lcom/intsig/office/ss/model/XLSModel/ASheet;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    move-result-object p2

    .line 4
    invoke-static {p3, p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->parseFormula(Ljava/lang/String;Lcom/intsig/office/ss/model/XLSModel/ASheet;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    move-result-object p0

    .line 5
    new-instance p3, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    const/4 v0, 0x1

    invoke-direct {p3, v0, p1, p2, p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;-><init>(BB[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    return-object p3
.end method

.method public static create(Lcom/intsig/office/ss/model/XLSModel/ASheet;Ljava/lang/String;)Lcom/intsig/office/fc/hssf/record/CFRuleRecord;
    .locals 3

    .line 1
    invoke-static {p1, p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->parseFormula(Ljava/lang/String;Lcom/intsig/office/ss/model/XLSModel/ASheet;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    move-result-object p0

    .line 2
    new-instance p1, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p1, v2, v0, p0, v1}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;-><init>(BB[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    return-object p1
.end method

.method private static getFormulaSize(Lcom/intsig/office/fc/hssf/formula/Formula;)I
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/Formula;->getEncodedTokenSize()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private getOptionFlag(Lcom/intsig/office/fc/util/BitField;)Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_5_options:I

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private isModified(Lcom/intsig/office/fc/util/BitField;)Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_5_options:I

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    xor-int/lit8 p1, p1, 0x1

    .line 8
    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static parseFormula(Ljava/lang/String;Lcom/intsig/office/ss/model/XLSModel/ASheet;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 2

    .line 1
    if-nez p0, :cond_0

    .line 2
    .line 3
    const/4 p0, 0x0

    .line 4
    return-object p0

    .line 5
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getSheetIndex(Lcom/intsig/office/ss/model/baseModel/Sheet;)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    check-cast p1, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 20
    .line 21
    const/4 v1, 0x0

    .line 22
    invoke-static {p0, p1, v1, v0}, Lcom/intsig/office/fc/hssf/model/HSSFFormulaParser;->parse(Ljava/lang/String;Lcom/intsig/office/ss/model/XLSModel/AWorkbook;II)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 23
    .line 24
    .line 25
    move-result-object p0

    .line 26
    return-object p0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private setModified(ZLcom/intsig/office/fc/util/BitField;)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_5_options:I

    .line 2
    .line 3
    xor-int/lit8 p1, p1, 0x1

    .line 4
    .line 5
    invoke-virtual {p2, v0, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_5_options:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private setOptionFlag(ZLcom/intsig/office/fc/util/BitField;)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_5_options:I

    .line 2
    .line 3
    invoke-virtual {p2, v0, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_5_options:I

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_1_condition_type:B

    .line 4
    .line 5
    iget-byte v2, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_2_comparison_operator:B

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;-><init>(BB)V

    .line 8
    .line 9
    .line 10
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_5_options:I

    .line 11
    .line 12
    iput v1, v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_5_options:I

    .line 13
    .line 14
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_6_not_used:S

    .line 15
    .line 16
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_6_not_used:S

    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->containsFontFormattingBlock()Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-eqz v1, :cond_0

    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_fontFormatting:Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;

    .line 25
    .line 26
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;->clone()Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    check-cast v1, Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;

    .line 31
    .line 32
    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_fontFormatting:Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;

    .line 33
    .line 34
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->containsBorderFormattingBlock()Z

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    if-eqz v1, :cond_1

    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 41
    .line 42
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->clone()Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    check-cast v1, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 47
    .line 48
    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 49
    .line 50
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->containsPatternFormattingBlock()Z

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    if-eqz v1, :cond_2

    .line 55
    .line 56
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 57
    .line 58
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;->clone()Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    check-cast v1, Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 63
    .line 64
    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 65
    .line 66
    :cond_2
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_17_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 67
    .line 68
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/formula/Formula;->copy()Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_17_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 73
    .line 74
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_17_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 75
    .line 76
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/formula/Formula;->copy()Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_18_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 81
    .line 82
    return-object v0
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public containsAlignFormattingBlock()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->align:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getOptionFlag(Lcom/intsig/office/fc/util/BitField;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public containsBorderFormattingBlock()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bord:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getOptionFlag(Lcom/intsig/office/fc/util/BitField;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public containsFontFormattingBlock()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->font:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getOptionFlag(Lcom/intsig/office/fc/util/BitField;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public containsPatternFormattingBlock()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->patt:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getOptionFlag(Lcom/intsig/office/fc/util/BitField;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public containsProtectionFormattingBlock()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->prot:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getOptionFlag(Lcom/intsig/office/fc/util/BitField;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBorderFormatting()Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->containsBorderFormattingBlock()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 8
    .line 9
    return-object v0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getComparisonOperation()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_2_comparison_operator:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getConditionType()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_1_condition_type:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getDataSize()I
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->containsFontFormattingBlock()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_fontFormatting:Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;->getRawRecord()[B

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    array-length v0, v0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    :goto_0
    add-int/lit8 v0, v0, 0xc

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->containsBorderFormattingBlock()Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz v2, :cond_1

    .line 24
    .line 25
    const/16 v2, 0x8

    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_1
    const/4 v2, 0x0

    .line 29
    :goto_1
    add-int/2addr v0, v2

    .line 30
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->containsPatternFormattingBlock()Z

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    if-eqz v2, :cond_2

    .line 35
    .line 36
    const/4 v1, 0x4

    .line 37
    :cond_2
    add-int/2addr v0, v1

    .line 38
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_17_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 39
    .line 40
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getFormulaSize(Lcom/intsig/office/fc/hssf/formula/Formula;)I

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    add-int/2addr v0, v1

    .line 45
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_18_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 46
    .line 47
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getFormulaSize(Lcom/intsig/office/fc/hssf/formula/Formula;)I

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    add-int/2addr v0, v1

    .line 52
    return v0
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getFontFormatting()Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->containsFontFormattingBlock()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_fontFormatting:Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;

    .line 8
    .line 9
    return-object v0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOptions()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_5_options:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getParsedExpression1()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_17_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/Formula;->getTokens()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getParsedExpression2()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_18_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/Formula;->getTokens(Lcom/intsig/office/fc/hssf/formula/Formula;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPatternFormatting()Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->containsPatternFormattingBlock()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 8
    .line 9
    return-object v0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSid()S
    .locals 1

    .line 1
    const/16 v0, 0x1b1

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isBottomBorderModified()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordBot:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->isModified(Lcom/intsig/office/fc/util/BitField;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isBottomLeftTopRightBorderModified()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordBlTr:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->isModified(Lcom/intsig/office/fc/util/BitField;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isLeftBorderModified()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordLeft:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->isModified(Lcom/intsig/office/fc/util/BitField;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isPatternBackgroundColorModified()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->pattBgCol:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->isModified(Lcom/intsig/office/fc/util/BitField;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isPatternColorModified()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->pattCol:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->isModified(Lcom/intsig/office/fc/util/BitField;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isPatternStyleModified()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->pattStyle:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->isModified(Lcom/intsig/office/fc/util/BitField;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isRightBorderModified()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordRight:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->isModified(Lcom/intsig/office/fc/util/BitField;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isTopBorderModified()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordTop:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->isModified(Lcom/intsig/office/fc/util/BitField;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isTopLeftBottomRightBorderModified()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordTlBr:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->isModified(Lcom/intsig/office/fc/util/BitField;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_17_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getFormulaSize(Lcom/intsig/office/fc/hssf/formula/Formula;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_18_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 8
    .line 9
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getFormulaSize(Lcom/intsig/office/fc/hssf/formula/Formula;)I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    iget-byte v2, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_1_condition_type:B

    .line 14
    .line 15
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeByte(I)V

    .line 16
    .line 17
    .line 18
    iget-byte v2, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_2_comparison_operator:B

    .line 19
    .line 20
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeByte(I)V

    .line 21
    .line 22
    .line 23
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 24
    .line 25
    .line 26
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 27
    .line 28
    .line 29
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_5_options:I

    .line 30
    .line 31
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeInt(I)V

    .line 32
    .line 33
    .line 34
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_6_not_used:S

    .line 35
    .line 36
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->containsFontFormattingBlock()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_0

    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_fontFormatting:Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;->getRawRecord()[B

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->write([B)V

    .line 52
    .line 53
    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->containsBorderFormattingBlock()Z

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    if-eqz v0, :cond_1

    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 61
    .line 62
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 63
    .line 64
    .line 65
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->containsPatternFormattingBlock()Z

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    if-eqz v0, :cond_2

    .line 70
    .line 71
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 72
    .line 73
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;->serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 74
    .line 75
    .line 76
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_17_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 77
    .line 78
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->serializeTokens(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 79
    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_18_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 82
    .line 83
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->serializeTokens(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 84
    .line 85
    .line 86
    return-void
    .line 87
.end method

.method public setAlignFormattingUnchanged()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    sget-object v1, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->align:Lcom/intsig/office/fc/util/BitField;

    .line 3
    .line 4
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setOptionFlag(ZLcom/intsig/office/fc/util/BitField;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setBorderFormatting(Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 p1, 0x0

    .line 8
    :goto_0
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bord:Lcom/intsig/office/fc/util/BitField;

    .line 9
    .line 10
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setOptionFlag(ZLcom/intsig/office/fc/util/BitField;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBottomBorderModified(Z)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordBot:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setModified(ZLcom/intsig/office/fc/util/BitField;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBottomLeftTopRightBorderModified(Z)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordBlTr:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setModified(ZLcom/intsig/office/fc/util/BitField;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setComparisonOperation(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_2_comparison_operator:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFontFormatting(Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_fontFormatting:Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 p1, 0x0

    .line 8
    :goto_0
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->font:Lcom/intsig/office/fc/util/BitField;

    .line 9
    .line 10
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setOptionFlag(ZLcom/intsig/office/fc/util/BitField;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLeftBorderModified(Z)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordLeft:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setModified(ZLcom/intsig/office/fc/util/BitField;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setParsedExpression1([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->create([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_17_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setParsedExpression2([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->create([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_18_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPatternBackgroundColorModified(Z)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->pattBgCol:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setModified(ZLcom/intsig/office/fc/util/BitField;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPatternColorModified(Z)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->pattCol:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setModified(ZLcom/intsig/office/fc/util/BitField;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPatternFormatting(Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->_patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 p1, 0x0

    .line 8
    :goto_0
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->patt:Lcom/intsig/office/fc/util/BitField;

    .line 9
    .line 10
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setOptionFlag(ZLcom/intsig/office/fc/util/BitField;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPatternStyleModified(Z)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->pattStyle:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setModified(ZLcom/intsig/office/fc/util/BitField;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setProtectionFormattingUnchanged()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    sget-object v1, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->prot:Lcom/intsig/office/fc/util/BitField;

    .line 3
    .line 4
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setOptionFlag(ZLcom/intsig/office/fc/util/BitField;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setRightBorderModified(Z)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordRight:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setModified(ZLcom/intsig/office/fc/util/BitField;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTopBorderModified(Z)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordTop:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setModified(ZLcom/intsig/office/fc/util/BitField;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTopLeftBottomRightBorderModified(Z)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->bordTlBr:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setModified(ZLcom/intsig/office/fc/util/BitField;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "[CFRULE]\n"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 9
    .line 10
    .line 11
    new-instance v1, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v2, "    .condition_type   ="

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget-byte v2, p0, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->field_1_condition_type:B

    .line 22
    .line 23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 31
    .line 32
    .line 33
    new-instance v1, Ljava/lang/StringBuilder;

    .line 34
    .line 35
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .line 37
    .line 38
    const-string v2, "    OPTION FLAGS=0x"

    .line 39
    .line 40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getOptions()I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    return-object v0
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.class public final Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;
.super Ljava/lang/Object;
.source "HSSFSheetConditionalFormatting.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/SheetConditionalFormatting;


# instance fields
.field private final _conditionalFormattingTable:Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

.field private final _sheet:Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->_sheet:Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getConditionalFormattingTable()Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->_conditionalFormattingTable:Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public addConditionalFormatting(Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;)I
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;->getCFRecordsAggregate()Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;

    move-result-object p1

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;->cloneCFAggregate()Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;

    move-result-object p1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->_conditionalFormattingTable:Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;->add(Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;)I

    move-result p1

    return p1
.end method

.method public addConditionalFormatting(Lcom/intsig/office/fc/ss/usermodel/ConditionalFormatting;)I
    .locals 0

    .line 3
    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->addConditionalFormatting(Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;)I

    move-result p1

    return p1
.end method

.method public addConditionalFormatting([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;)I
    .locals 2

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    move-object p2, v0

    .line 21
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->addConditionalFormatting([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;[Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;)I

    move-result p1

    return p1
.end method

.method public addConditionalFormatting([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;)I
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 p2, 0x1

    aput-object p3, v0, p2

    .line 23
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->addConditionalFormatting([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;[Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;)I

    move-result p1

    return p1
.end method

.method public addConditionalFormatting([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;)I
    .locals 0

    .line 22
    check-cast p2, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->addConditionalFormatting([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;)I

    move-result p1

    return p1
.end method

.method public addConditionalFormatting([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;)I
    .locals 0

    .line 24
    check-cast p2, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;

    check-cast p3, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;

    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->addConditionalFormatting([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;)I

    move-result p1

    return p1
.end method

.method public addConditionalFormatting([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;[Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;)I
    .locals 5

    if-eqz p1, :cond_5

    .line 5
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget-object v3, p1, v2

    sget-object v4, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->EXCEL97:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->validate(Lcom/intsig/office/fc/ss/SpreadsheetVersion;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_4

    .line 6
    array-length v0, p2

    if-eqz v0, :cond_3

    .line 7
    array-length v0, p2

    const/4 v2, 0x3

    if-gt v0, v2, :cond_2

    .line 8
    array-length v0, p2

    new-array v0, v0, [Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    :goto_1
    array-length v2, p2

    if-eq v1, v2, :cond_1

    .line 10
    aget-object v2, p2, v1

    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->getCfRuleRecord()Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    move-result-object v2

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 11
    :cond_1
    new-instance p2, Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;

    invoke-direct {p2, p1, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;-><init>([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;[Lcom/intsig/office/fc/hssf/record/CFRuleRecord;)V

    .line 12
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->_conditionalFormattingTable:Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;->add(Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;)I

    move-result p1

    return p1

    .line 13
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Number of rules must not exceed 3"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 14
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "cfRules must not be empty"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 15
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "cfRules must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 16
    :cond_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "regions must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public addConditionalFormatting([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;[Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;)I
    .locals 3

    .line 17
    instance-of v0, p2, [Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;

    if-eqz v0, :cond_0

    check-cast p2, [Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;

    goto :goto_0

    .line 18
    :cond_0
    array-length v0, p2

    new-array v1, v0, [Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;

    const/4 v2, 0x0

    .line 19
    invoke-static {p2, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p2, v1

    .line 20
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->addConditionalFormatting([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;[Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;)I

    move-result p1

    return p1
.end method

.method public addConditionalFormatting([Lcom/intsig/office/fc/ss/util/Region;[Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;)I
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 4
    invoke-static {p1}, Lcom/intsig/office/fc/ss/util/Region;->convertRegionsToCellRanges([Lcom/intsig/office/fc/ss/util/Region;)[Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->addConditionalFormatting([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;[Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;)I

    move-result p1

    return p1
.end method

.method public createConditionalFormattingRule(BLjava/lang/String;)Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;
    .locals 0

    .line 1
    const/4 p1, 0x0

    return-object p1
.end method

.method public createConditionalFormattingRule(BLjava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;
    .locals 0

    .line 2
    const/4 p1, 0x0

    return-object p1
.end method

.method public createConditionalFormattingRule(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;
    .locals 0

    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->_sheet:Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getWorkbook()Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic createConditionalFormattingRule(BLjava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;
    .locals 0

    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->createConditionalFormattingRule(BLjava/lang/String;)Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic createConditionalFormattingRule(BLjava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;
    .locals 0

    .line 4
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->createConditionalFormattingRule(BLjava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic createConditionalFormattingRule(Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;
    .locals 0

    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->createConditionalFormattingRule(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;

    move-result-object p1

    return-object p1
.end method

.method public getConditionalFormattingAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->_conditionalFormattingTable:Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;->get(I)Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 3
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;

    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->_sheet:Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getWorkbook()Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;)V

    return-object v0
.end method

.method public bridge synthetic getConditionalFormattingAt(I)Lcom/intsig/office/fc/ss/usermodel/ConditionalFormatting;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->getConditionalFormattingAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;

    move-result-object p1

    return-object p1
.end method

.method public getNumConditionalFormattings()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->_conditionalFormattingTable:Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeConditionalFormatting(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;->_conditionalFormattingTable:Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ConditionalFormattingTable;->remove(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public final Lcom/intsig/office/fc/hssf/formula/function/Column;
.super Ljava/lang/Object;
.source "Column.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/function/Function0Arg;
.implements Lcom/intsig/office/fc/hssf/formula/function/Function1Arg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public evaluate(II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 2

    .line 1
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    add-int/lit8 p2, p2, 0x1

    int-to-double v0, p2

    invoke-direct {p1, v0, v1}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    return-object p1
.end method

.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 2

    .line 2
    instance-of p1, p3, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;

    if-eqz p1, :cond_0

    .line 3
    check-cast p3, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;

    invoke-interface {p3}, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;->getFirstColumn()I

    move-result p1

    goto :goto_0

    .line 4
    :cond_0
    instance-of p1, p3, Lcom/intsig/office/fc/hssf/formula/eval/RefEval;

    if-eqz p1, :cond_1

    .line 5
    check-cast p3, Lcom/intsig/office/fc/hssf/formula/eval/RefEval;

    invoke-interface {p3}, Lcom/intsig/office/fc/hssf/formula/eval/RefEval;->getColumn()I

    move-result p1

    .line 6
    :goto_0
    new-instance p2, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    add-int/lit8 p1, p1, 0x1

    int-to-double v0, p1

    invoke-direct {p2, v0, v1}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    return-object p2

    .line 7
    :cond_1
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    return-object p1
.end method

.method public evaluate([Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 2

    .line 8
    array-length v0, p1

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    if-eq v0, v1, :cond_0

    .line 9
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    return-object p1

    :cond_0
    const/4 v0, 0x0

    .line 10
    aget-object p1, p1, v0

    invoke-virtual {p0, p2, p3, p1}, Lcom/intsig/office/fc/hssf/formula/function/Column;->evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    return-object p1

    .line 11
    :cond_1
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    add-int/2addr p3, v1

    int-to-double p2, p3

    invoke-direct {p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    return-object p1
.end method

.class public interface abstract Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;
.super Ljava/lang/Object;
.source "CellValueRecordInterface.java"


# virtual methods
.method public abstract getColumn()S
.end method

.method public abstract getRow()I
.end method

.method public abstract getXFIndex()S
.end method

.method public abstract setColumn(S)V
.end method

.method public abstract setRow(I)V
.end method

.method public abstract setXFIndex(S)V
.end method

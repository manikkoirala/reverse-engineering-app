.class public final Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
.super Ljava/lang/Exception;
.source "EvaluationException.java"


# instance fields
.field private final _errorEval:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->_errorEval:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static invalidRef()Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->REF_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static invalidValue()Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static numberError()Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->NUM_ERROR:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->_errorEval:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;
.super Ljava/lang/Object;
.source "UnicodeString.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/record/common/UnicodeString;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExtRst"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;",
        ">;"
    }
.end annotation


# instance fields
.field private extraData:[B

.field private formattingFontIndex:S

.field private formattingOptions:S

.field private numberOfRuns:I

.field private phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

.field private phoneticText:Ljava/lang/String;

.field private reserved:S


# direct methods
.method protected constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->populateEmpty()V

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/util/LittleEndianInput;I)V
    .locals 6

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readShort()S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 5
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->populateEmpty()V

    return-void

    :cond_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_2

    .line 6
    invoke-static {}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;->〇080()Lcom/intsig/office/fc/util/POILogger;

    move-result-object v0

    sget v1, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Warning - ExtRst has wrong magic marker, expecting 1 but found "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-short v4, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " - ignoring"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    :goto_0
    add-int/lit8 v0, p2, -0x2

    if-ge v2, v0, :cond_1

    .line 7
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readByte()B

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 8
    :cond_1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->populateEmpty()V

    return-void

    .line 9
    :cond_2
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readShort()S

    move-result p2

    .line 10
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readShort()S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->formattingFontIndex:S

    .line 11
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readShort()S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->formattingOptions:S

    .line 12
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->numberOfRuns:I

    .line 13
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readShort()S

    move-result v0

    .line 14
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readShort()S

    move-result v1

    if-nez v0, :cond_3

    if-lez v1, :cond_3

    const/4 v1, 0x0

    :cond_3
    if-ne v0, v1, :cond_7

    .line 15
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/StringUtil;->readUnicodeLE(Lcom/intsig/office/fc/util/LittleEndianInput;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    add-int/lit8 p2, p2, -0x4

    add-int/lit8 p2, p2, -0x6

    .line 16
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr p2, v0

    .line 17
    div-int/lit8 v0, p2, 0x6

    .line 18
    new-array v1, v0, [Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    const/4 v1, 0x0

    .line 19
    :goto_1
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    array-length v4, v3

    if-ge v1, v4, :cond_4

    .line 20
    new-instance v4, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    const/4 v5, 0x0

    invoke-direct {v4, p1, v5}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;Lcom/intsig/office/fc/hssf/record/common/〇080;)V

    aput-object v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    mul-int/lit8 v0, v0, 0x6

    sub-int/2addr p2, v0

    if-gez p2, :cond_5

    .line 21
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Warning - ExtRst overran by "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    rsub-int/lit8 p2, p2, 0x0

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " bytes"

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 p2, 0x0

    .line 22
    :cond_5
    new-array p2, p2, [B

    iput-object p2, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->extraData:[B

    .line 23
    :goto_2
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->extraData:[B

    array-length v0, p2

    if-ge v2, v0, :cond_6

    .line 24
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readByte()B

    move-result v0

    aput-byte v0, p2, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_6
    return-void

    .line 25
    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The two length fields of the Phonetic Text don\'t agree! "

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " vs "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private populateEmpty()V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    .line 3
    .line 4
    const-string v0, ""

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    new-array v1, v0, [Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    .line 10
    .line 11
    iput-object v1, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    .line 12
    .line 13
    new-array v0, v0, [B

    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->extraData:[B

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method protected clone()Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;
    .locals 7

    .line 2
    new-instance v0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;

    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;-><init>()V

    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    .line 4
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->formattingFontIndex:S

    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->formattingFontIndex:S

    .line 5
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->formattingOptions:S

    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->formattingOptions:S

    .line 6
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->numberOfRuns:I

    iput v1, v0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->numberOfRuns:I

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    .line 8
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    array-length v1, v1

    new-array v1, v1, [Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    const/4 v1, 0x0

    .line 9
    :goto_0
    iget-object v2, v0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 10
    new-instance v3, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    iget-object v4, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    aget-object v4, v4, v1

    invoke-static {v4}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->〇080(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;)I

    move-result v4

    iget-object v5, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    aget-object v5, v5, v1

    invoke-static {v5}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;)I

    move-result v5

    iget-object v6, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    aget-object v6, v6, v1

    invoke-static {v6}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->〇o〇(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;)I

    move-result v6

    invoke-direct {v3, v4, v5, v6}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;-><init>(III)V

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->clone()Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;

    move-result-object v0

    return-object v0
.end method

.method public compareTo(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;)I
    .locals 4

    .line 2
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    iget-short v1, p1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    sub-int/2addr v0, v1

    if-eqz v0, :cond_0

    return v0

    .line 3
    :cond_0
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->formattingFontIndex:S

    iget-short v1, p1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->formattingFontIndex:S

    sub-int/2addr v0, v1

    if-eqz v0, :cond_1

    return v0

    .line 4
    :cond_1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->formattingOptions:S

    iget-short v1, p1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->formattingOptions:S

    sub-int/2addr v0, v1

    if-eqz v0, :cond_2

    return v0

    .line 5
    :cond_2
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->numberOfRuns:I

    iget v1, p1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->numberOfRuns:I

    sub-int/2addr v0, v1

    if-eqz v0, :cond_3

    return v0

    .line 6
    :cond_3
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    iget-object v1, p1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 7
    :cond_4
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    array-length v0, v0

    iget-object v1, p1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    array-length v1, v1

    sub-int/2addr v0, v1

    if-eqz v0, :cond_5

    return v0

    :cond_5
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 8
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    array-length v3, v2

    if-ge v1, v3, :cond_9

    .line 9
    aget-object v2, v2, v1

    invoke-static {v2}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->〇080(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;)I

    move-result v2

    iget-object v3, p1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    aget-object v3, v3, v1

    invoke-static {v3}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->〇080(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;)I

    move-result v3

    sub-int/2addr v2, v3

    if-eqz v2, :cond_6

    return v2

    .line 10
    :cond_6
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    aget-object v2, v2, v1

    invoke-static {v2}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;)I

    move-result v2

    iget-object v3, p1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    aget-object v3, v3, v1

    invoke-static {v3}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;)I

    move-result v3

    sub-int/2addr v2, v3

    if-eqz v2, :cond_7

    return v2

    .line 11
    :cond_7
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    aget-object v2, v2, v1

    invoke-static {v2}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;)I

    move-result v2

    iget-object v3, p1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    aget-object v3, v3, v1

    invoke-static {v3}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->〇o〇(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;)I

    move-result v3

    sub-int/2addr v2, v3

    if-eqz v2, :cond_8

    return v2

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 12
    :cond_9
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->extraData:[B

    array-length v1, v1

    iget-object p1, p1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->extraData:[B

    array-length p1, p1

    sub-int/2addr v1, p1

    if-eqz v1, :cond_a

    return v1

    :cond_a
    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->compareTo(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    check-cast p1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;

    .line 8
    .line 9
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->compareTo(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;)I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    if-nez p1, :cond_1

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    :cond_1
    return v1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected getDataSize()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    mul-int/lit8 v0, v0, 0x2

    .line 8
    .line 9
    add-int/lit8 v0, v0, 0xa

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    .line 12
    .line 13
    array-length v1, v1

    .line 14
    mul-int/lit8 v1, v1, 0x6

    .line 15
    .line 16
    add-int/2addr v0, v1

    .line 17
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->extraData:[B

    .line 18
    .line 19
    array-length v1, v1

    .line 20
    add-int/2addr v0, v1

    .line 21
    return v0
.end method

.method public getFormattingFontIndex()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->formattingFontIndex:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFormattingOptions()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->formattingOptions:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNumberOfRuns()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->numberOfRuns:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPhRuns()[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPhoneticText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected serialize(Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->getDataSize()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/16 v1, 0x8

    .line 6
    .line 7
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 8
    .line 9
    .line 10
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    .line 11
    .line 12
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 16
    .line 17
    .line 18
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->formattingFontIndex:S

    .line 19
    .line 20
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 21
    .line 22
    .line 23
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->formattingOptions:S

    .line 24
    .line 25
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 26
    .line 27
    .line 28
    const/4 v0, 0x6

    .line 29
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 30
    .line 31
    .line 32
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->numberOfRuns:I

    .line 33
    .line 34
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    .line 38
    .line 39
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    .line 47
    .line 48
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 53
    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    .line 56
    .line 57
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    mul-int/lit8 v0, v0, 0x2

    .line 62
    .line 63
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 64
    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    .line 67
    .line 68
    invoke-static {v0, p1}, Lcom/intsig/office/fc/util/StringUtil;->putUnicodeLE(Ljava/lang/String;Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 69
    .line 70
    .line 71
    const/4 v0, 0x0

    .line 72
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;

    .line 73
    .line 74
    array-length v2, v1

    .line 75
    if-ge v0, v2, :cond_0

    .line 76
    .line 77
    aget-object v1, v1, v0

    .line 78
    .line 79
    invoke-static {v1, p1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->O8(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;)V

    .line 80
    .line 81
    .line 82
    add-int/lit8 v0, v0, 0x1

    .line 83
    .line 84
    goto :goto_0

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$ExtRst;->extraData:[B

    .line 86
    .line 87
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->write([B)V

    .line 88
    .line 89
    .line 90
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.class public abstract Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;
.super Ljava/lang/Object;
.source "FinanceFunction.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/function/Function3Arg;
.implements Lcom/intsig/office/fc/hssf/formula/function/Function4Arg;


# static fields
.field private static final DEFAULT_ARG3:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

.field private static final DEFAULT_ARG4:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

.field public static final FV:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final NPER:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final PMT:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final PV:Lcom/intsig/office/fc/hssf/formula/function/Function;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;->ZERO:Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 2
    .line 3
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->DEFAULT_ARG3:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;->FALSE:Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 6
    .line 7
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->DEFAULT_ARG4:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction$1;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction$1;-><init>()V

    .line 12
    .line 13
    .line 14
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->FV:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 15
    .line 16
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction$2;

    .line 17
    .line 18
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction$2;-><init>()V

    .line 19
    .line 20
    .line 21
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->NPER:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 22
    .line 23
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction$3;

    .line 24
    .line 25
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction$3;-><init>()V

    .line 26
    .line 27
    .line 28
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->PMT:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 29
    .line 30
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction$4;

    .line 31
    .line 32
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction$4;-><init>()V

    .line 33
    .line 34
    .line 35
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->PV:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method protected abstract evaluate(DDDDZ)D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation
.end method

.method protected evaluate([D)D
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation

    move-object/from16 v0, p1

    .line 17
    array-length v1, v0

    const/4 v2, 0x3

    const-wide/16 v3, 0x0

    if-eq v1, v2, :cond_2

    const/4 v5, 0x4

    if-eq v1, v5, :cond_1

    const/4 v6, 0x5

    if-ne v1, v6, :cond_0

    .line 18
    aget-wide v5, v0, v5

    goto :goto_0

    .line 19
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Wrong number of arguments"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-wide v5, v3

    .line 20
    :goto_0
    aget-wide v1, v0, v2

    move-wide v14, v1

    goto :goto_1

    :cond_2
    move-wide v5, v3

    move-wide v14, v5

    :goto_1
    const/4 v1, 0x0

    .line 21
    aget-wide v8, v0, v1

    const/4 v2, 0x1

    aget-wide v10, v0, v2

    const/4 v7, 0x2

    aget-wide v12, v0, v7

    cmpl-double v0, v5, v3

    if-eqz v0, :cond_3

    const/16 v16, 0x1

    goto :goto_2

    :cond_3
    const/16 v16, 0x0

    :goto_2
    move-object/from16 v7, p0

    invoke-virtual/range {v7 .. v16}, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->evaluate(DDDDZ)D

    move-result-wide v0

    return-wide v0
.end method

.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 7

    .line 1
    sget-object v6, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->DEFAULT_ARG3:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    return-object p1
.end method

.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 8

    .line 2
    sget-object v7, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->DEFAULT_ARG4:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    return-object p1
.end method

.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 13

    move v0, p1

    move v1, p2

    move-object/from16 v2, p3

    .line 3
    :try_start_0
    invoke-static {v2, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    move-result-wide v2

    move-object/from16 v4, p4

    .line 4
    invoke-static {v4, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    move-result-wide v4

    move-object/from16 v6, p5

    .line 5
    invoke-static {v6, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    move-result-wide v6

    move-object/from16 v8, p6

    .line 6
    invoke-static {v8, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    move-result-wide v8

    move-object/from16 v10, p7

    .line 7
    invoke-static {v10, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    move-result-wide v0

    const-wide/16 v10, 0x0

    cmpl-double v12, v0, v10

    if-eqz v12, :cond_0

    const/4 v0, 0x1

    const/4 v10, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v10, 0x0

    :goto_0
    move-object v0, p0

    move-wide v1, v2

    move-wide v3, v4

    move-wide v5, v6

    move-wide v7, v8

    move v9, v10

    .line 8
    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->evaluate(DDDDZ)D

    move-result-wide v0

    .line 9
    invoke-static {v0, v1}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->checkValue(D)V
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    new-instance v2, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    invoke-direct {v2, v0, v1}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    return-object v2

    :catch_0
    move-exception v0

    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object v0

    return-object v0
.end method

.method public evaluate([Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 9

    .line 12
    array-length v0, p1

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x3

    if-eq v0, v4, :cond_2

    const/4 v5, 0x4

    if-eq v0, v5, :cond_1

    const/4 v6, 0x5

    if-eq v0, v6, :cond_0

    .line 13
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    return-object p1

    .line 14
    :cond_0
    aget-object v3, p1, v3

    aget-object v6, p1, v2

    aget-object v7, p1, v1

    aget-object v8, p1, v4

    aget-object p1, p1, v5

    move-object v0, p0

    move v1, p2

    move v2, p3

    move-object v4, v6

    move-object v5, v7

    move-object v6, v8

    move-object v7, p1

    invoke-virtual/range {v0 .. v7}, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    return-object p1

    .line 15
    :cond_1
    aget-object v3, p1, v3

    aget-object v5, p1, v2

    aget-object v6, p1, v1

    aget-object p1, p1, v4

    sget-object v7, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->DEFAULT_ARG4:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-object v0, p0

    move v1, p2

    move v2, p3

    move-object v4, v5

    move-object v5, v6

    move-object v6, p1

    invoke-virtual/range {v0 .. v7}, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    return-object p1

    .line 16
    :cond_2
    aget-object v3, p1, v3

    aget-object v4, p1, v2

    aget-object v5, p1, v1

    sget-object v6, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->DEFAULT_ARG3:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    sget-object v7, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->DEFAULT_ARG4:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-object v0, p0

    move v1, p2

    move v2, p3

    invoke-virtual/range {v0 .. v7}, Lcom/intsig/office/fc/hssf/formula/function/FinanceFunction;->evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    return-object p1
.end method

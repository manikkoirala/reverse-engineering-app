.class final Lcom/intsig/office/fc/hssf/formula/ParseNode;
.super Ljava/lang/Object;
.source "ParseNode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;
    }
.end annotation


# static fields
.field public static final Oo08:[Lcom/intsig/office/fc/hssf/formula/ParseNode;


# instance fields
.field private final O8:I

.field private final 〇080:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

.field private final 〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/formula/ParseNode;

.field private 〇o〇:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    new-array v0, v0, [Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 3
    .line 4
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->Oo08:[Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V
    .locals 1

    .line 11
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->Oo08:[Lcom/intsig/office/fc/hssf/formula/ParseNode;

    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Lcom/intsig/office/fc/hssf/formula/ParseNode;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/intsig/office/fc/hssf/formula/ParseNode;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 12
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Lcom/intsig/office/fc/hssf/formula/ParseNode;Lcom/intsig/office/fc/hssf/formula/ParseNode;)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/intsig/office/fc/hssf/formula/ParseNode;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 p2, 0x1

    aput-object p3, v0, p2

    .line 13
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[Lcom/intsig/office/fc/hssf/formula/ParseNode;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_2

    .line 2
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇080:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 3
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 4
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇〇888(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o〇:Z

    const/4 p1, 0x1

    const/4 v0, 0x0

    .line 5
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 6
    aget-object v1, p2, v0

    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->o〇0()I

    move-result v1

    add-int/2addr p1, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o〇:Z

    if-eqz v0, :cond_1

    .line 8
    array-length p2, p2

    add-int/2addr p1, p2

    .line 9
    :cond_1
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->O8:I

    return-void

    .line 10
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "token must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static oO80(Lcom/intsig/office/fc/hssf/formula/ParseNode;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->o〇0()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;-><init>(I)V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;->〇o〇()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    return-object p0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private o〇0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->O8:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇080(Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;)V
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o〇()[Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    aget-object v0, v0, v1

    .line 7
    .line 8
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;->〇o00〇〇Oo()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o〇()[Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    const/4 v2, 0x1

    .line 20
    aget-object v1, v1, v2

    .line 21
    .line 22
    invoke-direct {v1, p1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;->〇o00〇〇Oo()I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    add-int/lit8 v3, v0, 0x1

    .line 30
    .line 31
    invoke-virtual {p1, v3, v1}, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;->Oo08(II)I

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    add-int/lit8 v3, v3, 0x4

    .line 36
    .line 37
    invoke-static {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/AttrPtg;->createIf(I)Lcom/intsig/office/fc/hssf/formula/ptg/AttrPtg;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o〇()[Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 42
    .line 43
    .line 44
    move-result-object v4

    .line 45
    array-length v4, v4

    .line 46
    const/4 v5, 0x3

    .line 47
    const/4 v6, 0x2

    .line 48
    if-le v4, v6, :cond_0

    .line 49
    .line 50
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o〇()[Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    aget-object v4, v4, v6

    .line 55
    .line 56
    invoke-direct {v4, p1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;->〇o00〇〇Oo()I

    .line 60
    .line 61
    .line 62
    move-result v4

    .line 63
    add-int/lit8 v6, v1, 0x1

    .line 64
    .line 65
    invoke-virtual {p1, v6, v4}, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;->Oo08(II)I

    .line 66
    .line 67
    .line 68
    move-result v6

    .line 69
    add-int/lit8 v6, v6, 0x4

    .line 70
    .line 71
    add-int/lit8 v6, v6, 0x4

    .line 72
    .line 73
    sub-int/2addr v6, v2

    .line 74
    invoke-static {v6}, Lcom/intsig/office/fc/hssf/formula/ptg/AttrPtg;->createSkip(I)Lcom/intsig/office/fc/hssf/formula/ptg/AttrPtg;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    invoke-static {v5}, Lcom/intsig/office/fc/hssf/formula/ptg/AttrPtg;->createSkip(I)Lcom/intsig/office/fc/hssf/formula/ptg/AttrPtg;

    .line 79
    .line 80
    .line 81
    move-result-object v5

    .line 82
    invoke-virtual {p1, v0, v3}, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;->O8(ILcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {p1, v1, v2}, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;->O8(ILcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {p1, v4, v5}, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;->O8(ILcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 89
    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_0
    invoke-static {v5}, Lcom/intsig/office/fc/hssf/formula/ptg/AttrPtg;->createSkip(I)Lcom/intsig/office/fc/hssf/formula/ptg/AttrPtg;

    .line 93
    .line 94
    .line 95
    move-result-object v2

    .line 96
    invoke-virtual {p1, v0, v3}, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;->O8(ILcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 97
    .line 98
    .line 99
    invoke-virtual {p1, v1, v2}, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;->O8(ILcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 100
    .line 101
    .line 102
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇080:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 103
    .line 104
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;->〇080(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 105
    .line 106
    .line 107
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private 〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇080:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇〇888(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇080(Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇080:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 14
    .line 15
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/formula/ptg/MemFuncPtg;

    .line 16
    .line 17
    const/4 v2, 0x0

    .line 18
    if-nez v1, :cond_2

    .line 19
    .line 20
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/formula/ptg/MemAreaPtg;

    .line 21
    .line 22
    if-eqz v1, :cond_1

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    const/4 v1, 0x0

    .line 26
    goto :goto_1

    .line 27
    :cond_2
    :goto_0
    const/4 v1, 0x1

    .line 28
    :goto_1
    if-eqz v1, :cond_3

    .line 29
    .line 30
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;->〇080(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 31
    .line 32
    .line 33
    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o〇()[Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    array-length v0, v0

    .line 38
    if-ge v2, v0, :cond_4

    .line 39
    .line 40
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o〇()[Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    aget-object v0, v0, v2

    .line 45
    .line 46
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;)V

    .line 47
    .line 48
    .line 49
    add-int/lit8 v2, v2, 0x1

    .line 50
    .line 51
    goto :goto_2

    .line 52
    :cond_4
    if-nez v1, :cond_5

    .line 53
    .line 54
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇080:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 55
    .line 56
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/formula/ParseNode$TokenCollector;->〇080(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 57
    .line 58
    .line 59
    :cond_5
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private static 〇〇888(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Z
    .locals 1

    .line 1
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/FuncVarPtg;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/ptg/FuncVarPtg;

    .line 6
    .line 7
    const-string v0, "IF"

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/AbstractFunctionPtg;->getName()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object p0

    .line 13
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    move-result p0

    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    const/4 p0, 0x1

    .line 20
    return p0

    .line 21
    :cond_0
    const/4 p0, 0x0

    .line 22
    return p0
    .line 23
    .line 24
.end method


# virtual methods
.method public O8()I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇080:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg;

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    const/16 v0, 0x8

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getSize()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    :goto_0
    const/4 v1, 0x0

    .line 15
    :goto_1
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 16
    .line 17
    array-length v3, v2

    .line 18
    if-ge v1, v3, :cond_1

    .line 19
    .line 20
    aget-object v2, v2, v1

    .line 21
    .line 22
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->O8()I

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    add-int/2addr v0, v2

    .line 27
    add-int/lit8 v1, v1, 0x1

    .line 28
    .line 29
    goto :goto_1

    .line 30
    :cond_1
    return v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public Oo08()Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇080:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()[Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

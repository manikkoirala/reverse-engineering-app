.class public Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;
.super Ljava/lang/Object;
.source "HSSFPictureData.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/PictureData;


# static fields
.field public static final FORMAT_MASK:S = -0x10s

.field public static final MSOBI_DIB:S = 0x7a80s

.field public static final MSOBI_EMF:S = 0x3d40s

.field public static final MSOBI_JPEG:S = 0x46a0s

.field public static final MSOBI_PICT:S = 0x5420s

.field public static final MSOBI_PNG:S = 0x6e00s

.field public static final MSOBI_WMF:S = 0x2160s


# instance fields
.field private blip:Lcom/intsig/office/fc/ddf/EscherBlipRecord;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/ddf/EscherBlipRecord;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;->blip:Lcom/intsig/office/fc/ddf/EscherBlipRecord;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public getData()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;->blip:Lcom/intsig/office/fc/ddf/EscherBlipRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherBlipRecord;->getPicturedata()[B

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFormat()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;->blip:Lcom/intsig/office/fc/ddf/EscherBlipRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    add-int/lit16 v0, v0, 0xfe8

    .line 8
    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;->blip:Lcom/intsig/office/fc/ddf/EscherBlipRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    packed-switch v0, :pswitch_data_0

    .line 8
    .line 9
    .line 10
    const-string v0, "image/unknown"

    .line 11
    .line 12
    return-object v0

    .line 13
    :pswitch_0
    const-string v0, "image/bmp"

    .line 14
    .line 15
    return-object v0

    .line 16
    :pswitch_1
    const-string v0, "image/png"

    .line 17
    .line 18
    return-object v0

    .line 19
    :pswitch_2
    const-string v0, "image/jpeg"

    .line 20
    .line 21
    return-object v0

    .line 22
    :pswitch_3
    const-string v0, "image/x-pict"

    .line 23
    .line 24
    return-object v0

    .line 25
    :pswitch_4
    const-string v0, "image/x-wmf"

    .line 26
    .line 27
    return-object v0

    .line 28
    :pswitch_5
    const-string v0, "image/x-emf"

    .line 29
    .line 30
    return-object v0

    .line 31
    :pswitch_data_0
    .packed-switch -0xfe6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public suggestFileExtension()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;->blip:Lcom/intsig/office/fc/ddf/EscherBlipRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    packed-switch v0, :pswitch_data_0

    .line 8
    .line 9
    .line 10
    const-string v0, ""

    .line 11
    .line 12
    return-object v0

    .line 13
    :pswitch_0
    const-string v0, "dib"

    .line 14
    .line 15
    return-object v0

    .line 16
    :pswitch_1
    const-string v0, "png"

    .line 17
    .line 18
    return-object v0

    .line 19
    :pswitch_2
    const-string v0, "jpeg"

    .line 20
    .line 21
    return-object v0

    .line 22
    :pswitch_3
    const-string v0, "pict"

    .line 23
    .line 24
    return-object v0

    .line 25
    :pswitch_4
    const-string v0, "wmf"

    .line 26
    .line 27
    return-object v0

    .line 28
    :pswitch_5
    const-string v0, "emf"

    .line 29
    .line 30
    return-object v0

    .line 31
    :pswitch_data_0
    .packed-switch -0xfe6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.class public Lcom/intsig/office/fc/hssf/eventusermodel/HSSFEventFactory;
.super Ljava/lang/Object;
.source "HSSFEventFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private genericProcessEvents(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFRequest;Ljava/io/InputStream;)S
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/eventusermodel/HSSFUserException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p2, v1}, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;-><init>(Ljava/io/InputStream;Z)V

    .line 5
    .line 6
    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->nextRecord()Lcom/intsig/office/fc/hssf/record/Record;

    .line 8
    .line 9
    .line 10
    move-result-object p2

    .line 11
    if-nez p2, :cond_1

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_1
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/eventusermodel/HSSFRequest;->processRecord(Lcom/intsig/office/fc/hssf/record/Record;)S

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    :goto_0
    return v1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public abortableProcessEvents(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFRequest;Ljava/io/InputStream;)S
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/eventusermodel/HSSFUserException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hssf/eventusermodel/HSSFEventFactory;->genericProcessEvents(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFRequest;Ljava/io/InputStream;)S

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    return p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public abortableProcessWorkbookEvents(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFRequest;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)S
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/intsig/office/fc/hssf/eventusermodel/HSSFUserException;
        }
    .end annotation

    const-string v0, "Workbook"

    .line 2
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    move-result-object p2

    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/eventusermodel/HSSFEventFactory;->abortableProcessEvents(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFRequest;Ljava/io/InputStream;)S

    move-result p1

    return p1
.end method

.method public abortableProcessWorkbookEvents(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFRequest;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;)S
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/intsig/office/fc/hssf/eventusermodel/HSSFUserException;
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/eventusermodel/HSSFEventFactory;->abortableProcessWorkbookEvents(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFRequest;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)S

    move-result p1

    return p1
.end method

.method public processEvents(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFRequest;Ljava/io/InputStream;)V
    .locals 0

    .line 1
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hssf/eventusermodel/HSSFEventFactory;->genericProcessEvents(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFRequest;Ljava/io/InputStream;)S
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/eventusermodel/HSSFUserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    :catch_0
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public processWorkbookEvents(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFRequest;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "Workbook"

    .line 2
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    move-result-object p2

    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/eventusermodel/HSSFEventFactory;->processEvents(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFRequest;Ljava/io/InputStream;)V

    return-void
.end method

.method public processWorkbookEvents(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFRequest;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/eventusermodel/HSSFEventFactory;->processWorkbookEvents(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFRequest;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V

    return-void
.end method

.class final Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;
.super Ljava/lang/Object;
.source "ForkedEvaluationWorkbook.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;


# instance fields
.field private final 〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

.field private final 〇o00〇〇Oo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationSheet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    .line 5
    .line 6
    new-instance p1, Ljava/util/HashMap;

    .line 7
    .line 8
    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇o00〇〇Oo:Ljava/util/Map;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private O8(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationSheet;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇o00〇〇Oo:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationSheet;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationSheet;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    .line 14
    .line 15
    invoke-interface {v1, p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;->getSheetIndex(Ljava/lang/String;)I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;->getSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationSheet;-><init>(Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;)V

    .line 24
    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇o00〇〇Oo:Ljava/util/Map;

    .line 27
    .line 28
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    :cond_0
    return-object v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public convertFromExternSheetIndex(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;->convertFromExternSheetIndex(I)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getExternalName(II)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalName;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    .line 2
    .line 3
    invoke-interface {v0, p1, p2}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;->getExternalName(II)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalName;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public getExternalSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;->getExternalSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getFormulaTokens(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    .line 6
    .line 7
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;->getFormulaTokens(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    return-object p1

    .line 12
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 13
    .line 14
    const-string v0, "Updated formulas not supported yet"

    .line 15
    .line 16
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    throw p1
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getName(Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;)Lcom/intsig/office/fc/hssf/formula/EvaluationName;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    invoke-interface {v0, p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;->getName(Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;)Lcom/intsig/office/fc/hssf/formula/EvaluationName;

    move-result-object p1

    return-object p1
.end method

.method public getName(Ljava/lang/String;I)Lcom/intsig/office/fc/hssf/formula/EvaluationName;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    invoke-interface {v0, p1, p2}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;->getName(Ljava/lang/String;I)Lcom/intsig/office/fc/hssf/formula/EvaluationName;

    move-result-object p1

    return-object p1
.end method

.method public getSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->getSheetName(I)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->O8(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationSheet;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getSheetIndex(Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;)I
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationSheet;

    if-eqz v0, :cond_0

    .line 2
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationSheet;

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationSheet;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;)I

    move-result p1

    return p1

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    invoke-interface {v0, p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;->getSheetIndex(Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;)I

    move-result p1

    return p1
.end method

.method public getSheetIndex(Ljava/lang/String;)I
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    invoke-interface {v0, p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;->getSheetIndex(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getSheetName(I)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;->getSheetName(I)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getUDFFinder()Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;->getUDFFinder()Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public resolveNameXText(Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;->resolveNameXText(Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇080(Lcom/intsig/office/fc/ss/usermodel/Workbook;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇o00〇〇Oo(Ljava/lang/String;II)Lcom/intsig/office/fc/hssf/formula/EvaluationCell;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->O8(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationSheet;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationSheet;->getCell(II)Lcom/intsig/office/fc/hssf/formula/EvaluationCell;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public 〇o〇(Ljava/lang/String;II)Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->O8(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationSheet;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationSheet;->〇080(II)Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

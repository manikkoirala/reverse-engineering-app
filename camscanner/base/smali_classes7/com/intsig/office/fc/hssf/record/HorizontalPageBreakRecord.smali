.class public final Lcom/intsig/office/fc/hssf/record/HorizontalPageBreakRecord;
.super Lcom/intsig/office/fc/hssf/record/PageBreakRecord;
.source "HorizontalPageBreakRecord.java"


# static fields
.field public static final sid:S = 0x1bs


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/PageBreakRecord;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/record/PageBreakRecord;-><init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V

    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/HorizontalPageBreakRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/HorizontalPageBreakRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PageBreakRecord;->getBreaksIterator()Ljava/util/Iterator;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    if-eqz v2, :cond_0

    .line 15
    .line 16
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    check-cast v2, Lcom/intsig/office/fc/hssf/record/PageBreakRecord$Break;

    .line 21
    .line 22
    iget v3, v2, Lcom/intsig/office/fc/hssf/record/PageBreakRecord$Break;->main:I

    .line 23
    .line 24
    iget v4, v2, Lcom/intsig/office/fc/hssf/record/PageBreakRecord$Break;->subFrom:I

    .line 25
    .line 26
    iget v2, v2, Lcom/intsig/office/fc/hssf/record/PageBreakRecord$Break;->subTo:I

    .line 27
    .line 28
    invoke-virtual {v0, v3, v4, v2}, Lcom/intsig/office/fc/hssf/record/PageBreakRecord;->addBreak(III)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    return-object v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getSid()S
    .locals 1

    .line 1
    const/16 v0, 0x1b

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

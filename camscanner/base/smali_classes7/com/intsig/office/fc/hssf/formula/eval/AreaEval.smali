.class public interface abstract Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;
.super Ljava/lang/Object;
.source "AreaEval.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/TwoDEval;


# virtual methods
.method public abstract contains(II)Z
.end method

.method public abstract containsColumn(I)Z
.end method

.method public abstract containsRow(I)Z
.end method

.method public abstract getAbsoluteValue(II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
.end method

.method public abstract getFirstColumn()I
.end method

.method public abstract getFirstRow()I
.end method

.method public abstract getHeight()I
.end method

.method public abstract getLastColumn()I
.end method

.method public abstract getLastRow()I
.end method

.method public abstract getRelativeValue(II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
.end method

.method public abstract getWidth()I
.end method

.method public abstract offset(IIII)Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;
.end method

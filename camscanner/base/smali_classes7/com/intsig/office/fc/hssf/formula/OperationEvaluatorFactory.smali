.class final Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;
.super Ljava/lang/Object;
.source "OperationEvaluatorFactory.java"


# static fields
.field private static final 〇080:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;",
            "Lcom/intsig/office/fc/hssf/formula/function/Function;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o00〇〇Oo()Ljava/util/Map;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇080:Ljava/util/Map;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static 〇080(Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;[Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/OperationEvaluationContext;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 1

    .line 1
    if-eqz p0, :cond_4

    .line 2
    .line 3
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇080:Ljava/util/Map;

    .line 4
    .line 5
    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluationContext;->getRowIndex()I

    .line 14
    .line 15
    .line 16
    move-result p0

    .line 17
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluationContext;->getColumnIndex()I

    .line 18
    .line 19
    .line 20
    move-result p2

    .line 21
    int-to-short p2, p2

    .line 22
    invoke-interface {v0, p1, p0, p2}, Lcom/intsig/office/fc/hssf/formula/function/Function;->evaluate([Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 23
    .line 24
    .line 25
    move-result-object p0

    .line 26
    return-object p0

    .line 27
    :cond_0
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AbstractFunctionPtg;

    .line 28
    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/ptg/AbstractFunctionPtg;

    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/AbstractFunctionPtg;->getFunctionIndex()S

    .line 34
    .line 35
    .line 36
    move-result p0

    .line 37
    const/16 v0, 0x94

    .line 38
    .line 39
    if-eq p0, v0, :cond_2

    .line 40
    .line 41
    const/16 v0, 0xff

    .line 42
    .line 43
    if-eq p0, v0, :cond_1

    .line 44
    .line 45
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/eval/FunctionEval;->getBasicFunction(I)Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 46
    .line 47
    .line 48
    move-result-object p0

    .line 49
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluationContext;->getRowIndex()I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluationContext;->getColumnIndex()I

    .line 54
    .line 55
    .line 56
    move-result p2

    .line 57
    int-to-short p2, p2

    .line 58
    invoke-interface {p0, p1, v0, p2}, Lcom/intsig/office/fc/hssf/formula/function/Function;->evaluate([Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 59
    .line 60
    .line 61
    move-result-object p0

    .line 62
    return-object p0

    .line 63
    :cond_1
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/UserDefinedFunction;->〇080:Lcom/intsig/office/fc/hssf/formula/function/FreeRefFunction;

    .line 64
    .line 65
    invoke-interface {p0, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/FreeRefFunction;->evaluate([Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/OperationEvaluationContext;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 66
    .line 67
    .line 68
    move-result-object p0

    .line 69
    return-object p0

    .line 70
    :cond_2
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/function/Indirect;->instance:Lcom/intsig/office/fc/hssf/formula/function/FreeRefFunction;

    .line 71
    .line 72
    invoke-interface {p0, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/FreeRefFunction;->evaluate([Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/OperationEvaluationContext;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 73
    .line 74
    .line 75
    move-result-object p0

    .line 76
    return-object p0

    .line 77
    :cond_3
    new-instance p1, Ljava/lang/RuntimeException;

    .line 78
    .line 79
    new-instance p2, Ljava/lang/StringBuilder;

    .line 80
    .line 81
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 82
    .line 83
    .line 84
    const-string v0, "Unexpected operation ptg class ("

    .line 85
    .line 86
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 90
    .line 91
    .line 92
    move-result-object p0

    .line 93
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object p0

    .line 97
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    const-string p0, ")"

    .line 101
    .line 102
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object p0

    .line 109
    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    throw p1

    .line 113
    :cond_4
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 114
    .line 115
    const-string p1, "ptg must not be null"

    .line 116
    .line 117
    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 118
    .line 119
    .line 120
    throw p0
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private static 〇o00〇〇Oo()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;",
            "Lcom/intsig/office/fc/hssf/formula/function/Function;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/HashMap;

    .line 2
    .line 3
    const/16 v1, 0x20

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 6
    .line 7
    .line 8
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/EqualPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 9
    .line 10
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/RelationalOperationEval;->EqualEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 11
    .line 12
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 13
    .line 14
    .line 15
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/GreaterEqualPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 16
    .line 17
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/RelationalOperationEval;->GreaterEqualEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 18
    .line 19
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 20
    .line 21
    .line 22
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/GreaterThanPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 23
    .line 24
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/RelationalOperationEval;->GreaterThanEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 25
    .line 26
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 27
    .line 28
    .line 29
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/LessEqualPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 30
    .line 31
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/RelationalOperationEval;->LessEqualEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 32
    .line 33
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 34
    .line 35
    .line 36
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/LessThanPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 37
    .line 38
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/RelationalOperationEval;->LessThanEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 39
    .line 40
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 41
    .line 42
    .line 43
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/NotEqualPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 44
    .line 45
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/RelationalOperationEval;->NotEqualEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 46
    .line 47
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 48
    .line 49
    .line 50
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/ConcatPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 51
    .line 52
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/ConcatEval;->instance:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 53
    .line 54
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 55
    .line 56
    .line 57
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/AddPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 58
    .line 59
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation;->AddEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 60
    .line 61
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 62
    .line 63
    .line 64
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/DividePtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 65
    .line 66
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation;->DivideEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 67
    .line 68
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 69
    .line 70
    .line 71
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/MultiplyPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 72
    .line 73
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation;->MultiplyEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 74
    .line 75
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 76
    .line 77
    .line 78
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/PercentPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 79
    .line 80
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/PercentEval;->instance:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 81
    .line 82
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 83
    .line 84
    .line 85
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/PowerPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 86
    .line 87
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation;->PowerEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 88
    .line 89
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 90
    .line 91
    .line 92
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/SubtractPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 93
    .line 94
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation;->SubtractEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 95
    .line 96
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 97
    .line 98
    .line 99
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/UnaryMinusPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 100
    .line 101
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/UnaryMinusEval;->instance:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 102
    .line 103
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 104
    .line 105
    .line 106
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/UnaryPlusPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 107
    .line 108
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/UnaryPlusEval;->instance:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 109
    .line 110
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 111
    .line 112
    .line 113
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/RangePtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;

    .line 114
    .line 115
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/RangeEval;->instance:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 116
    .line 117
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 118
    .line 119
    .line 120
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/IntersectionPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;

    .line 121
    .line 122
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/IntersectionEval;->instance:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 123
    .line 124
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluatorFactory;->〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V

    .line 125
    .line 126
    .line 127
    return-object v0
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private static 〇o〇(Ljava/util/Map;Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;Lcom/intsig/office/fc/hssf/formula/function/Function;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;",
            "Lcom/intsig/office/fc/hssf/formula/function/Function;",
            ">;",
            "Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;",
            "Lcom/intsig/office/fc/hssf/formula/function/Function;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    array-length v1, v0

    .line 10
    const/4 v2, 0x1

    .line 11
    if-gt v1, v2, :cond_0

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    aget-object v0, v0, v1

    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/reflect/Constructor;->getModifiers()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPrivate(I)Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-eqz v0, :cond_0

    .line 25
    .line 26
    invoke-interface {p0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    return-void

    .line 30
    :cond_0
    new-instance p0, Ljava/lang/RuntimeException;

    .line 31
    .line 32
    new-instance p2, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v0, "Failed to verify instance ("

    .line 38
    .line 39
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    const-string p1, ") is a singleton."

    .line 54
    .line 55
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    throw p0
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

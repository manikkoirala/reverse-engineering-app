.class abstract Lcom/intsig/office/fc/hssf/formula/ptg/Ref2DPtgBase;
.super Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;
.source "Ref2DPtgBase.java"


# static fields
.field private static final SIZE:I = 0x5


# direct methods
.method protected constructor <init>(IIZZ)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;-><init>()V

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->setRow(I)V

    .line 3
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->setColumn(I)V

    .line 4
    invoke-virtual {p0, p3}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->setRowRelative(Z)V

    .line 5
    invoke-virtual {p0, p4}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->setColRelative(Z)V

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/ss/util/CellReference;)V
    .locals 0

    .line 8
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;-><init>(Lcom/intsig/office/fc/ss/util/CellReference;)V

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;-><init>()V

    .line 7
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->readCoordinates(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    return-void
.end method


# virtual methods
.method protected abstract getSid()B
.end method

.method public final getSize()I
    .locals 1

    .line 1
    const/4 v0, 0x5

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final toFormulaString()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->formatReferenceAsString()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 15
    .line 16
    .line 17
    const-string v1, " ["

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->formatReferenceAsString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 27
    .line 28
    .line 29
    const-string v1, "]"

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    return-object v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public write(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ref2DPtgBase;->getSid()B

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getPtgClass()B

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    add-int/2addr v0, v1

    .line 10
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeByte(I)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->writeCoordinates(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

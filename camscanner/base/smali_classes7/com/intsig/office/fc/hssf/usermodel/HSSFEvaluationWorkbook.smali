.class public final Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;
.super Ljava/lang/Object;
.source "HSSFEvaluationWorkbook.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/FormulaRenderingWorkbook;
.implements Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;
.implements Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook$Name;
    }
.end annotation


# instance fields
.field private final _iBook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

.field private final _uBook:Lcom/intsig/office/ss/model/XLSModel/AWorkbook;


# direct methods
.method private constructor <init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_uBook:Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getInternalWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_iBook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static create(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;
    .locals 1

    .line 1
    if-nez p0, :cond_0

    .line 2
    .line 3
    const/4 p0, 0x0

    .line 4
    return-object p0

    .line 5
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;

    .line 6
    .line 7
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public convertFromExternSheetIndex(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_iBook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSheetIndexFromExternSheetIndex(I)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getExternalName(II)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalName;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_iBook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getExternalName(II)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalName;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public getExternalSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_iBook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getExternalSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getExternalSheetIndex(Ljava/lang/String;)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_uBook:Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getSheetIndex(Ljava/lang/String;)I

    move-result p1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_iBook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->checkExternSheet(I)S

    move-result p1

    return p1
.end method

.method public getExternalSheetIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_iBook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getExternalSheetIndex(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getFormulaTokens(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;->〇080()Lcom/intsig/office/ss/model/XLSModel/ACell;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/XLSModel/ACell;->getCellValueRecord()Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    check-cast p1, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->getFormulaTokens()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    return-object p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getName(Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;)Lcom/intsig/office/fc/hssf/formula/EvaluationName;
    .locals 2

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;->getIndex()I

    move-result p1

    .line 7
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook$Name;

    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_iBook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNameRecord(I)Lcom/intsig/office/fc/hssf/record/NameRecord;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook$Name;-><init>(Lcom/intsig/office/fc/hssf/record/NameRecord;I)V

    return-object v0
.end method

.method public getName(Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;)Lcom/intsig/office/fc/hssf/formula/EvaluationName;
    .locals 2

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;->getNameIndex()I

    move-result p1

    .line 9
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook$Name;

    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_iBook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNameRecord(I)Lcom/intsig/office/fc/hssf/record/NameRecord;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook$Name;-><init>(Lcom/intsig/office/fc/hssf/record/NameRecord;I)V

    return-object v0
.end method

.method public getName(Ljava/lang/String;I)Lcom/intsig/office/fc/hssf/formula/EvaluationName;
    .locals 4

    const/4 v0, 0x0

    .line 1
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_iBook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNumNames()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_iBook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNameRecord(I)Lcom/intsig/office/fc/hssf/record/NameRecord;

    move-result-object v1

    .line 3
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/NameRecord;->getSheetNumber()I

    move-result v2

    add-int/lit8 v3, p2, 0x1

    if-ne v2, v3, :cond_0

    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/NameRecord;->getNameText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4
    new-instance p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook$Name;

    invoke-direct {p1, v1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook$Name;-><init>(Lcom/intsig/office/fc/hssf/record/NameRecord;I)V

    return-object p1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    const/4 p1, 0x0

    goto :goto_1

    .line 5
    :cond_2
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->getName(Ljava/lang/String;I)Lcom/intsig/office/fc/hssf/formula/EvaluationName;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public getNameText(Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_iBook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;->getIndex()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNameRecord(I)Lcom/intsig/office/fc/hssf/record/NameRecord;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/NameRecord;->getNameText()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getNameXPtg(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_iBook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_uBook:Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getUDFFinder()Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNameXPtg(Ljava/lang/String;Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;)Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    return-object p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationSheet;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_uBook:Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 4
    .line 5
    invoke-virtual {v1, p1}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getSheetAt(I)Lcom/intsig/office/ss/model/XLSModel/ASheet;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationSheet;-><init>(Lcom/intsig/office/ss/model/XLSModel/ASheet;)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getSheetIndex(Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;)I
    .locals 1

    .line 1
    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationSheet;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationSheet;->〇080()Lcom/intsig/office/ss/model/XLSModel/ASheet;

    move-result-object p1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_uBook:Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getSheetIndex(Lcom/intsig/office/ss/model/baseModel/Sheet;)I

    move-result p1

    return p1
.end method

.method public getSheetIndex(Ljava/lang/String;)I
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_uBook:Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getSheetIndex(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getSheetName(I)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_uBook:Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getSheetName()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    return-object p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getSheetNameByExternSheet(I)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_iBook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->findSheetNameFromExternSheet(I)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getSpreadsheetVersion()Lcom/intsig/office/fc/ss/SpreadsheetVersion;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->EXCEL97:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getUDFFinder()Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_uBook:Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getUDFFinder()Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public resolveNameXText(Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;)Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->_iBook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;->getSheetRefIndex()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;->getNameIndex()I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->resolveNameXText(II)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

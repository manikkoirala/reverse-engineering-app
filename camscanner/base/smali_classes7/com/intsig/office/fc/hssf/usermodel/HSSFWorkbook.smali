.class public final Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;
.super Lcom/intsig/office/fc/POIDocument;
.source "HSSFWorkbook.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/Workbook;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;
    }
.end annotation


# static fields
.field private static final COMMA_PATTERN:Ljava/util/regex/Pattern;

.field public static final INITIAL_CAPACITY:I = 0x3

.field private static final MAX_COLUMN:S = 0xffs

.field private static final MAX_ROW:I = 0xffff

.field private static final MAX_STYLES:I = 0xfbe

.field private static final WORKBOOK_DIR_ENTRY_NAMES:[Ljava/lang/String;


# instance fields
.field protected _sheets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;",
            ">;"
        }
    .end annotation
.end field

.field private _udfFinder:Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;

.field private fonts:Ljava/util/Hashtable;

.field private formatter:Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;

.field private missingCellPolicy:Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;

.field private names:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFName;",
            ">;"
        }
    .end annotation
.end field

.field private palette:Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;

.field private preserveNodes:Z

.field private workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const-string v0, ","

    .line 2
    .line 3
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->COMMA_PATTERN:Ljava/util/regex/Pattern;

    .line 8
    .line 9
    const-string v0, "Workbook"

    .line 10
    .line 11
    const-string v1, "WORKBOOK"

    .line 12
    .line 13
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    sput-object v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->WORKBOOK_DIR_ENTRY_NAMES:[Ljava/lang/String;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->createWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;-><init>(Lcom/intsig/office/fc/hssf/model/InternalWorkbook;)V

    return-void
.end method

.method private constructor <init>(Lcom/intsig/office/fc/hssf/model/InternalWorkbook;)V
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/POIDocument;-><init>(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V

    .line 3
    sget-object v0, Lcom/intsig/office/fc/ss/usermodel/IRow;->RETURN_NULL_AND_BLANK:Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->missingCellPolicy:Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;

    .line 4
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;->DEFAULT:Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_udfFinder:Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 6
    new-instance p1, Ljava/util/ArrayList;

    const/4 v0, 0x3

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 7
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 10
    invoke-direct {p0, p1, p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;-><init>(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/POIDocument;-><init>(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V

    .line 12
    sget-object v0, Lcom/intsig/office/fc/ss/usermodel/IRow;->RETURN_NULL_AND_BLANK:Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->missingCellPolicy:Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;

    .line 13
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;->DEFAULT:Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_udfFinder:Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;

    .line 14
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getWorkbookDirEntryName(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)Ljava/lang/String;

    move-result-object v0

    .line 15
    iput-boolean p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->preserveNodes:Z

    if-nez p2, :cond_0

    const/4 p2, 0x0

    .line 16
    iput-object p2, p0, Lcom/intsig/office/fc/POIDocument;->directory:Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 17
    :cond_0
    new-instance p2, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {p2, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 18
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    .line 19
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    move-result-object p1

    .line 20
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/record/RecordFactory;->createRecords(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object p1

    .line 21
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->createWorkbook(Ljava/util/List;)Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 22
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->setPropertiesFromWorkbook(Lcom/intsig/office/fc/hssf/model/InternalWorkbook;)V

    .line 23
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNumRecords()I

    move-result p2

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->convertLabelRecords(Ljava/util/List;I)V

    .line 25
    new-instance v0, Lcom/intsig/office/fc/hssf/model/RecordStream;

    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/hssf/model/RecordStream;-><init>(Ljava/util/List;I)V

    .line 26
    :goto_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/RecordStream;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 27
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createSheet(Lcom/intsig/office/fc/hssf/model/RecordStream;)Lcom/intsig/office/fc/hssf/model/InternalSheet;

    move-result-object p1

    .line 28
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    new-instance v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    invoke-direct {v1, p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;Lcom/intsig/office/fc/hssf/model/InternalSheet;)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 29
    :goto_1
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNumNames()I

    move-result p2

    if-ge p1, p2, :cond_2

    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 8
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;-><init>(Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;-><init>(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    .line 30
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;-><init>(Ljava/io/InputStream;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p0, v0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;-><init>(Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Z)V

    return-void
.end method

.method private convertLabelRecords(Ljava/util/List;I)V
    .locals 5

    .line 1
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-ge p2, v0, :cond_1

    .line 6
    .line 7
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Lcom/intsig/office/fc/hssf/record/Record;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    const/16 v2, 0x204

    .line 18
    .line 19
    if-ne v1, v2, :cond_0

    .line 20
    .line 21
    check-cast v0, Lcom/intsig/office/fc/hssf/record/LabelRecord;

    .line 22
    .line 23
    invoke-interface {p1, p2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    new-instance v1, Lcom/intsig/office/fc/hssf/record/LabelSSTRecord;

    .line 27
    .line 28
    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/LabelSSTRecord;-><init>()V

    .line 29
    .line 30
    .line 31
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 32
    .line 33
    new-instance v3, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/LabelRecord;->getValue()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v4

    .line 39
    invoke-direct {v3, v4}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;-><init>(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->addSSTString(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;)I

    .line 43
    .line 44
    .line 45
    move-result v2

    .line 46
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/LabelRecord;->getRow()I

    .line 47
    .line 48
    .line 49
    move-result v3

    .line 50
    invoke-virtual {v1, v3}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setRow(I)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/LabelRecord;->getColumn()S

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    invoke-virtual {v1, v3}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setColumn(S)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/LabelRecord;->getXFIndex()S

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setXFIndex(S)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hssf/record/LabelSSTRecord;->setSSTIndex(I)V

    .line 68
    .line 69
    .line 70
    invoke-interface {p1, p2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 71
    .line 72
    .line 73
    :cond_0
    add-int/lit8 p2, p2, 0x1

    .line 74
    .line 75
    goto :goto_0

    .line 76
    :cond_1
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static create(Lcom/intsig/office/fc/hssf/model/InternalWorkbook;)Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;-><init>(Lcom/intsig/office/fc/hssf/model/InternalWorkbook;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private findExistingBuiltinNameRecordIdx(IB)I
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    .line 3
    .line 4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    if-ge v0, v1, :cond_3

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 11
    .line 12
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNameRecord(I)Lcom/intsig/office/fc/hssf/record/NameRecord;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    if-eqz v1, :cond_2

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/NameRecord;->isBuiltInName()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_1

    .line 23
    .line 24
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/NameRecord;->getBuiltInName()B

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-eq v2, p2, :cond_0

    .line 29
    .line 30
    goto :goto_1

    .line 31
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/NameRecord;->getSheetNumber()I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    add-int/lit8 v1, v1, -0x1

    .line 36
    .line 37
    if-ne v1, p1, :cond_1

    .line 38
    .line 39
    return v0

    .line 40
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    .line 44
    .line 45
    const-string p2, "Unable to find all defined names to iterate over"

    .line 46
    .line 47
    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    throw p1

    .line 51
    :cond_3
    const/4 p1, -0x1

    .line 52
    return p1
    .line 53
.end method

.method private getAllEmbeddedObjects(Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/RecordBase;",
            ">;",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFObjectData;",
            ">;)V"
        }
    .end annotation

    .line 4
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/hssf/record/RecordBase;

    .line 5
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/record/ObjRecord;

    if-eqz v1, :cond_0

    .line 6
    check-cast v0, Lcom/intsig/office/fc/hssf/record/ObjRecord;

    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 7
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 8
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/intsig/office/fc/hssf/record/SubRecord;

    .line 9
    instance-of v2, v2, Lcom/intsig/office/fc/hssf/record/EmbeddedObjectRefSubRecord;

    if-eqz v2, :cond_1

    .line 10
    new-instance v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFObjectData;

    iget-object v3, p0, Lcom/intsig/office/fc/POIDocument;->directory:Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    invoke-direct {v2, v0, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFObjectData;-><init>(Lcom/intsig/office/fc/hssf/record/ObjRecord;Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;)V

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method private getSheets()[Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    new-array v0, v0, [Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 10
    .line 11
    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private getUniqueSheetName(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .line 1
    const/16 v0, 0x28

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    const-string v2, ")"

    .line 9
    .line 10
    const/4 v3, 0x2

    .line 11
    if-lez v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    .line 14
    .line 15
    .line 16
    move-result v4

    .line 17
    if-eqz v4, :cond_0

    .line 18
    .line 19
    add-int/lit8 v4, v0, 0x1

    .line 20
    .line 21
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 22
    .line 23
    .line 24
    move-result v5

    .line 25
    add-int/lit8 v5, v5, -0x1

    .line 26
    .line 27
    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v4

    .line 31
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v4

    .line 35
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 36
    .line 37
    .line 38
    move-result v4
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 39
    add-int/lit8 v4, v4, 0x1

    .line 40
    .line 41
    :try_start_1
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object p1
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 49
    goto :goto_0

    .line 50
    :catch_0
    nop

    .line 51
    goto :goto_0

    .line 52
    :catch_1
    nop

    .line 53
    :cond_0
    const/4 v4, 0x2

    .line 54
    :goto_0
    add-int/lit8 v0, v4, 0x1

    .line 55
    .line 56
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v4

    .line 60
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 61
    .line 62
    .line 63
    move-result v5

    .line 64
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 65
    .line 66
    .line 67
    move-result v6

    .line 68
    add-int/2addr v5, v6

    .line 69
    add-int/2addr v5, v3

    .line 70
    const/16 v6, 0x1f

    .line 71
    .line 72
    if-ge v5, v6, :cond_1

    .line 73
    .line 74
    new-instance v5, Ljava/lang/StringBuilder;

    .line 75
    .line 76
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    const-string v6, " ("

    .line 83
    .line 84
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v4

    .line 97
    goto :goto_1

    .line 98
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    .line 99
    .line 100
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    .line 102
    .line 103
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 104
    .line 105
    .line 106
    move-result v7

    .line 107
    sub-int/2addr v6, v7

    .line 108
    sub-int/2addr v6, v3

    .line 109
    invoke-virtual {p1, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 110
    .line 111
    .line 112
    move-result-object v6

    .line 113
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    const-string v6, "("

    .line 117
    .line 118
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v4

    .line 131
    :goto_1
    iget-object v5, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 132
    .line 133
    invoke-virtual {v5, v4}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSheetIndex(Ljava/lang/String;)I

    .line 134
    .line 135
    .line 136
    move-result v5

    .line 137
    const/4 v6, -0x1

    .line 138
    if-ne v5, v6, :cond_2

    .line 139
    .line 140
    return-object v4

    .line 141
    :cond_2
    move v4, v0

    .line 142
    goto :goto_0
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private static getWorkbookDirEntryName(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)Ljava/lang/String;
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->WORKBOOK_DIR_ENTRY_NAMES:[Ljava/lang/String;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    :goto_0
    array-length v2, v0

    .line 5
    if-ge v1, v2, :cond_0

    .line 6
    .line 7
    aget-object v2, v0, v1

    .line 8
    .line 9
    :try_start_0
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/Entry;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    .line 11
    .line 12
    return-object v2

    .line 13
    :catch_0
    add-int/lit8 v1, v1, 0x1

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    :try_start_1
    const-string v0, "Book"

    .line 17
    .line 18
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/Entry;

    .line 19
    .line 20
    .line 21
    new-instance p0, Lcom/intsig/office/fc/hssf/OldExcelFormatException;

    .line 22
    .line 23
    const-string v0, "The supplied spreadsheet seems to be Excel 5.0/7.0 (BIFF5) format. POI only supports BIFF8 format (from Excel versions 97/2000/XP/2003)"

    .line 24
    .line 25
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/OldExcelFormatException;-><init>(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    throw p0
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 29
    :catch_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 30
    .line 31
    const-string v0, "The supplied POIFSFileSystem does not contain a BIFF8 \'Workbook\' entry. Is it really an excel file?"

    .line 32
    .line 33
    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    throw p0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private searchForPictures(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ddf/EscherRecord;",
            ">;",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 16
    .line 17
    instance-of v1, v0, Lcom/intsig/office/fc/ddf/EscherBSERecord;

    .line 18
    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    move-object v1, v0

    .line 22
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherBSERecord;

    .line 23
    .line 24
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->getBlipRecord()Lcom/intsig/office/fc/ddf/EscherBlipRecord;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    if-eqz v1, :cond_0

    .line 29
    .line 30
    new-instance v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;

    .line 31
    .line 32
    invoke-direct {v2, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;-><init>(Lcom/intsig/office/fc/ddf/EscherBlipRecord;)V

    .line 33
    .line 34
    .line 35
    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherRecord;->getChildRecords()Ljava/util/List;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    invoke-direct {p0, v0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->searchForPictures(Ljava/util/List;Ljava/util/List;)V

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_1
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private setPropertiesFromWorkbook(Lcom/intsig/office/fc/hssf/model/InternalWorkbook;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private validateSheetIndex(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    add-int/lit8 v0, v0, -0x1

    .line 8
    .line 9
    if-ltz p1, :cond_0

    .line 10
    .line 11
    if-gt p1, v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 15
    .line 16
    new-instance v2, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v3, "Sheet index ("

    .line 22
    .line 23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    const-string p1, ") is out of range (0.."

    .line 30
    .line 31
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    const-string p1, ")"

    .line 38
    .line 39
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    throw v1
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public addPicture([BI)I
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->initDrawings()V

    .line 2
    .line 3
    .line 4
    invoke-static {p1}, Lcom/intsig/office/fc/codec/DigestUtils;->md5([B)[B

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    new-instance v1, Lcom/intsig/office/fc/ddf/EscherBitmapBlip;

    .line 9
    .line 10
    invoke-direct {v1}, Lcom/intsig/office/fc/ddf/EscherBitmapBlip;-><init>()V

    .line 11
    .line 12
    .line 13
    add-int/lit16 v2, p2, -0xfe8

    .line 14
    .line 15
    int-to-short v2, v2

    .line 16
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 17
    .line 18
    .line 19
    packed-switch p2, :pswitch_data_0

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :pswitch_0
    const/16 v2, 0x7a80

    .line 24
    .line 25
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :pswitch_1
    const/16 v2, 0x6e00

    .line 30
    .line 31
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :pswitch_2
    const/16 v2, 0x46a0

    .line 36
    .line 37
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :pswitch_3
    const/16 v2, 0x5420

    .line 42
    .line 43
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :pswitch_4
    const/16 v2, 0x2160

    .line 48
    .line 49
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :pswitch_5
    const/16 v2, 0x3d40

    .line 54
    .line 55
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 56
    .line 57
    .line 58
    :goto_0
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/ddf/EscherBitmapBlip;->setUID([B)V

    .line 59
    .line 60
    .line 61
    const/4 v2, -0x1

    .line 62
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/EscherBitmapBlip;->setMarker(B)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/ddf/EscherBlipRecord;->setPictureData([B)V

    .line 66
    .line 67
    .line 68
    new-instance v2, Lcom/intsig/office/fc/ddf/EscherBSERecord;

    .line 69
    .line 70
    invoke-direct {v2}, Lcom/intsig/office/fc/ddf/EscherBSERecord;-><init>()V

    .line 71
    .line 72
    .line 73
    const/16 v3, -0xff9

    .line 74
    .line 75
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 76
    .line 77
    .line 78
    shl-int/lit8 v3, p2, 0x4

    .line 79
    .line 80
    or-int/lit8 v3, v3, 0x2

    .line 81
    .line 82
    int-to-short v3, v3

    .line 83
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 84
    .line 85
    .line 86
    int-to-byte p2, p2

    .line 87
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->setBlipTypeMacOS(B)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->setBlipTypeWin32(B)V

    .line 91
    .line 92
    .line 93
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->setUid([B)V

    .line 94
    .line 95
    .line 96
    const/16 p2, 0xff

    .line 97
    .line 98
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->setTag(S)V

    .line 99
    .line 100
    .line 101
    array-length p1, p1

    .line 102
    add-int/lit8 p1, p1, 0x19

    .line 103
    .line 104
    invoke-virtual {v2, p1}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->setSize(I)V

    .line 105
    .line 106
    .line 107
    const/4 p1, 0x1

    .line 108
    invoke-virtual {v2, p1}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->setRef(I)V

    .line 109
    .line 110
    .line 111
    const/4 p1, 0x0

    .line 112
    invoke-virtual {v2, p1}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->setOffset(I)V

    .line 113
    .line 114
    .line 115
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->setBlipRecord(Lcom/intsig/office/fc/ddf/EscherBlipRecord;)V

    .line 116
    .line 117
    .line 118
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 119
    .line 120
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->addBSERecord(Lcom/intsig/office/fc/ddf/EscherBSERecord;)I

    .line 121
    .line 122
    .line 123
    move-result p1

    .line 124
    return p1

    .line 125
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public addSSTString(Ljava/lang/String;)I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 4
    .line 5
    invoke-direct {v1, p1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->addSSTString(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;)I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public addToolPack(Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_udfFinder:Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/office/fc/hssf/formula/udf/AggregatingUDFFinder;

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/formula/udf/AggregatingUDFFinder;->add(Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public cloneSheet(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;
    .locals 4

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 5
    .line 6
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 13
    .line 14
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSheetName(I)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->cloneSheet(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const/4 v2, 0x0

    .line 23
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setSelected(Z)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setActive(Z)V

    .line 27
    .line 28
    .line 29
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getUniqueSheetName(Ljava/lang/String;)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 34
    .line 35
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 40
    .line 41
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 45
    .line 46
    invoke-virtual {v3, v2, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->setSheetName(ILjava/lang/String;)V

    .line 47
    .line 48
    .line 49
    const/16 v1, 0xd

    .line 50
    .line 51
    invoke-direct {p0, p1, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->findExistingBuiltinNameRecordIdx(IB)I

    .line 52
    .line 53
    .line 54
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 55
    .line 56
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->cloneDrawings(Lcom/intsig/office/fc/hssf/model/InternalSheet;)V

    .line 61
    .line 62
    .line 63
    return-object v0
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public createCellStyle()Lcom/intsig/office/fc/hssf/usermodel/HSSFCellStyle;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNumExFormats()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0xfbe

    .line 8
    .line 9
    if-eq v0, v1, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->createCellXF()Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getNumCellStyles()S

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    add-int/lit8 v1, v1, -0x1

    .line 22
    .line 23
    int-to-short v1, v1

    .line 24
    new-instance v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFCellStyle;

    .line 25
    .line 26
    invoke-direct {v2, v1, v0, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCellStyle;-><init>(SLcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V

    .line 27
    .line 28
    .line 29
    return-object v2

    .line 30
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 31
    .line 32
    const-string v1, "The maximum number of cell styles was exceeded. You can define up to 4000 styles in a .xls workbook"

    .line 33
    .line 34
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    throw v0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public createDataFormat()Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->formatter:Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 8
    .line 9
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;-><init>(Lcom/intsig/office/fc/hssf/model/InternalWorkbook;)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->formatter:Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;

    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->formatter:Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;

    .line 15
    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public createFont()Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->createNewFont()Lcom/intsig/office/fc/hssf/record/FontRecord;

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getNumberOfFonts()S

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    add-int/lit8 v0, v0, -0x1

    .line 11
    .line 12
    int-to-short v0, v0

    .line 13
    const/4 v1, 0x3

    .line 14
    if-le v0, v1, :cond_0

    .line 15
    .line 16
    add-int/lit8 v0, v0, 0x1

    .line 17
    .line 18
    int-to-short v0, v0

    .line 19
    :cond_0
    const/16 v1, 0x7fff

    .line 20
    .line 21
    if-eq v0, v1, :cond_1

    .line 22
    .line 23
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getFontAt(S)Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    return-object v0

    .line 28
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 29
    .line 30
    const-string v1, "Maximum number of fonts was exceeded"

    .line 31
    .line 32
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    throw v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public createName()Lcom/intsig/office/fc/hssf/usermodel/HSSFName;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public createSheet()Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;
    .locals 6

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V

    .line 2
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    iget-object v2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sheet"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    sub-int/2addr v5, v3

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->setSheetName(ILjava/lang/String;)V

    .line 4
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 5
    :goto_0
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setSelected(Z)V

    .line 6
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setActive(Z)V

    return-object v0
.end method

.method public createSheet(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;
    .locals 3

    if-eqz p1, :cond_2

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->doesContainsSheetName(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 8
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V

    .line 9
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    iget-object v2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->setSheetName(ILjava/lang/String;)V

    .line 10
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 11
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 12
    :goto_0
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setSelected(Z)V

    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setActive(Z)V

    return-object v0

    .line 14
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The workbook already contains a sheet of this name"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 15
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "sheetName must not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public dumpDrawingGroupRecords(Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    const/16 v1, 0xeb

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/DrawingGroupRecord;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->decode()V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->getEscherRecords()Ljava/util/List;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    new-instance v1, Ljava/io/PrintWriter;

    .line 19
    .line 20
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 21
    .line 22
    invoke-direct {v1, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 23
    .line 24
    .line 25
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    if-eqz v2, :cond_1

    .line 34
    .line 35
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 40
    .line 41
    if-eqz p1, :cond_0

    .line 42
    .line 43
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 44
    .line 45
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    invoke-virtual {v3, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_0
    const/4 v3, 0x0

    .line 54
    invoke-virtual {v2, v1, v3}, Lcom/intsig/office/fc/ddf/EscherRecord;->display(Ljava/io/PrintWriter;I)V

    .line 55
    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_1
    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V

    .line 59
    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public findFont(SSSLjava/lang/String;ZZSB)Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getNumberOfFonts()S

    .line 3
    .line 4
    .line 5
    move-result v1

    .line 6
    if-gt v0, v1, :cond_2

    .line 7
    .line 8
    const/4 v1, 0x4

    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    goto :goto_1

    .line 12
    :cond_0
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getFontAt(S)Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;->getBoldweight()S

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    if-ne v2, p1, :cond_1

    .line 21
    .line 22
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;->getColor()S

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    if-ne v2, p2, :cond_1

    .line 27
    .line 28
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;->getFontHeight()S

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-ne v2, p3, :cond_1

    .line 33
    .line 34
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;->getFontName()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    if-eqz v2, :cond_1

    .line 43
    .line 44
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;->getItalic()Z

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    if-ne v2, p5, :cond_1

    .line 49
    .line 50
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;->getStrikeout()Z

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    if-ne v2, p6, :cond_1

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;->getTypeOffset()S

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    if-ne v2, p7, :cond_1

    .line 61
    .line 62
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;->getUnderline()B

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    if-ne v2, p8, :cond_1

    .line 67
    .line 68
    return-object v1

    .line 69
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    .line 70
    .line 71
    int-to-short v0, v0

    .line 72
    goto :goto_0

    .line 73
    :cond_2
    const/4 p1, 0x0

    .line 74
    return-object p1
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
.end method

.method public findSheetNameFromExternSheet(I)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->findSheetNameFromExternSheet(I)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getActiveSheetIndex()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getWindowOne()Lcom/intsig/office/fc/hssf/record/WindowOneRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WindowOneRecord;->getActiveSheetIndex()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAllEmbeddedObjects()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFObjectData;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 2
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getNumberOfSheets()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 3
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getAllEmbeddedObjects(Ljava/util/List;Ljava/util/List;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getAllPictures()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getRecords()Ljava/util/List;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    if-eqz v2, :cond_1

    .line 21
    .line 22
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    check-cast v2, Lcom/intsig/office/fc/hssf/record/Record;

    .line 27
    .line 28
    instance-of v3, v2, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;

    .line 29
    .line 30
    if-eqz v3, :cond_0

    .line 31
    .line 32
    check-cast v2, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;

    .line 33
    .line 34
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->decode()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->getEscherRecords()Ljava/util/List;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    invoke-direct {p0, v2, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->searchForPictures(Ljava/util/List;Ljava/util/List;)V

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getBackupFlag()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getBackupRecord()Lcom/intsig/office/fc/hssf/record/BackupRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/BackupRecord;->getBackup()S

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x1

    .line 16
    :goto_0
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBytes()[B
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheets()[Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    array-length v1, v0

    .line 6
    const/4 v2, 0x0

    .line 7
    const/4 v3, 0x0

    .line 8
    :goto_0
    if-ge v3, v1, :cond_0

    .line 9
    .line 10
    aget-object v4, v0, v3

    .line 11
    .line 12
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 13
    .line 14
    .line 15
    move-result-object v4

    .line 16
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->preSerialize()V

    .line 17
    .line 18
    .line 19
    add-int/lit8 v3, v3, 0x1

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 23
    .line 24
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSize()I

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    new-array v4, v1, [Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;

    .line 29
    .line 30
    const/4 v5, 0x0

    .line 31
    :goto_1
    if-ge v5, v1, :cond_1

    .line 32
    .line 33
    iget-object v6, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 34
    .line 35
    invoke-virtual {v6, v5, v3}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->setSheetBof(II)V

    .line 36
    .line 37
    .line 38
    new-instance v6, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;

    .line 39
    .line 40
    invoke-direct {v6}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;-><init>()V

    .line 41
    .line 42
    .line 43
    aget-object v7, v0, v5

    .line 44
    .line 45
    invoke-virtual {v7}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 46
    .line 47
    .line 48
    move-result-object v7

    .line 49
    invoke-virtual {v7, v6, v3}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->visitContainedRecords(Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;I)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v6}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;->〇080()I

    .line 53
    .line 54
    .line 55
    move-result v7

    .line 56
    add-int/2addr v3, v7

    .line 57
    aput-object v6, v4, v5

    .line 58
    .line 59
    add-int/lit8 v5, v5, 0x1

    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_1
    new-array v0, v3, [B

    .line 63
    .line 64
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 65
    .line 66
    invoke-virtual {v3, v2, v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->serialize(I[B)I

    .line 67
    .line 68
    .line 69
    move-result v3

    .line 70
    :goto_2
    if-ge v2, v1, :cond_3

    .line 71
    .line 72
    aget-object v5, v4, v2

    .line 73
    .line 74
    invoke-virtual {v5, v3, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;->〇o00〇〇Oo(I[B)I

    .line 75
    .line 76
    .line 77
    move-result v6

    .line 78
    invoke-virtual {v5}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;->〇080()I

    .line 79
    .line 80
    .line 81
    move-result v7

    .line 82
    if-ne v6, v7, :cond_2

    .line 83
    .line 84
    add-int/2addr v3, v6

    .line 85
    add-int/lit8 v2, v2, 0x1

    .line 86
    .line 87
    goto :goto_2

    .line 88
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 89
    .line 90
    new-instance v1, Ljava/lang/StringBuilder;

    .line 91
    .line 92
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    .line 94
    .line 95
    const-string v3, "Actual serialized sheet size ("

    .line 96
    .line 97
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    const-string v3, ") differs from pre-calculated size ("

    .line 104
    .line 105
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    invoke-virtual {v5}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook$SheetRecordCollector;->〇080()I

    .line 109
    .line 110
    .line 111
    move-result v3

    .line 112
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 113
    .line 114
    .line 115
    const-string v3, ") for sheet ("

    .line 116
    .line 117
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    const-string v2, ")"

    .line 124
    .line 125
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    .line 127
    .line 128
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v1

    .line 132
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 133
    .line 134
    .line 135
    throw v0

    .line 136
    :cond_3
    return-object v0
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public getCellStyleAt(S)Lcom/intsig/office/fc/hssf/usermodel/HSSFCellStyle;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getExFormatAt(I)Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    new-instance v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFCellStyle;

    .line 10
    .line 11
    invoke-direct {v1, p1, v0, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCellStyle;-><init>(SLcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V

    .line 12
    .line 13
    .line 14
    return-object v1

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    return-object p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getCreationHelper()Lcom/intsig/office/fc/hssf/usermodel/HSSFCreationHelper;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFCreationHelper;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCreationHelper;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCustomPalette()Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->palette:Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getCustomPalette()Lcom/intsig/office/fc/hssf/record/PaletteRecord;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;-><init>(Lcom/intsig/office/fc/hssf/record/PaletteRecord;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->palette:Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;

    .line 17
    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->palette:Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;

    .line 19
    .line 20
    return-object v0
    .line 21
.end method

.method public getDisplayedTab()S
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getFirstVisibleTab()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-short v0, v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getExternalSheetIndex(I)I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->checkExternSheet(I)S

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getFirstVisibleTab()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getWindowOne()Lcom/intsig/office/fc/hssf/record/WindowOneRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WindowOneRecord;->getFirstVisibleTab()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFontAt(S)Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->fonts:Ljava/util/Hashtable;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/Hashtable;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->fonts:Ljava/util/Hashtable;

    .line 11
    .line 12
    :cond_0
    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->fonts:Ljava/util/Hashtable;

    .line 17
    .line 18
    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-eqz v1, :cond_1

    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->fonts:Ljava/util/Hashtable;

    .line 25
    .line 26
    invoke-virtual {p1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;

    .line 31
    .line 32
    return-object p1

    .line 33
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 34
    .line 35
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getFontRecordAt(I)Lcom/intsig/office/fc/hssf/record/FontRecord;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    new-instance v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;

    .line 40
    .line 41
    invoke-direct {v2, p1, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFont;-><init>(SLcom/intsig/office/fc/hssf/record/FontRecord;)V

    .line 42
    .line 43
    .line 44
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->fonts:Ljava/util/Hashtable;

    .line 45
    .line 46
    invoke-virtual {p1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    return-object v2
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getForceFormulaRecalculation()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/16 v1, 0x1c1

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Lcom/intsig/office/fc/hssf/record/RecalcIdRecord;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RecalcIdRecord;->getEngineId()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    const/4 v0, 0x1

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v0, 0x0

    .line 24
    :goto_0
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getMissingCellPolicy()Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->missingCellPolicy:Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getName(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/usermodel/HSSFName;
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getNameIndex(Ljava/lang/String;)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-gez p1, :cond_0

    .line 6
    .line 7
    const/4 p1, 0x0

    .line 8
    return-object p1

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFName;

    .line 16
    .line 17
    return-object p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getNameAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFName;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-lt v0, v1, :cond_1

    .line 9
    .line 10
    if-ltz p1, :cond_0

    .line 11
    .line 12
    if-gt p1, v0, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFName;

    .line 21
    .line 22
    return-object p1

    .line 23
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    .line 24
    .line 25
    new-instance v3, Ljava/lang/StringBuilder;

    .line 26
    .line 27
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    .line 29
    .line 30
    const-string v4, "Specified name index "

    .line 31
    .line 32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    const-string p1, " is outside the allowable range (0.."

    .line 39
    .line 40
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    sub-int/2addr v0, v1

    .line 44
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    const-string p1, ")."

    .line 48
    .line 49
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    invoke-direct {v2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    throw v2

    .line 60
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 61
    .line 62
    const-string v0, "There are no defined names in this workbook"

    .line 63
    .line 64
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    throw p1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getNameIndex(Ljava/lang/String;)I
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    .line 3
    .line 4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    if-ge v0, v1, :cond_1

    .line 9
    .line 10
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getNameName(I)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    return v0

    .line 21
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    const/4 p1, -0x1

    .line 25
    return p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getNameName(I)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getNameAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFName;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFName;->getNameName()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getNameRecord(I)Lcom/intsig/office/fc/hssf/record/NameRecord;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNameRecord(I)Lcom/intsig/office/fc/hssf/record/NameRecord;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getNumCellStyles()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNumExFormats()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNumberOfFonts()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNumberOfFontRecords()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNumberOfNames()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNumberOfSheets()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPrintArea(I)Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    add-int/lit8 p1, p1, 0x1

    .line 4
    .line 5
    const/4 v1, 0x6

    .line 6
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSpecificBuiltinRecord(BI)Lcom/intsig/office/fc/hssf/record/NameRecord;

    .line 7
    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    return-object p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getSSTString(I)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSSTString(I)Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;->getString()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    return-object p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getSSTUniqueStringSize()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSSTUniqueStringSize()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSelectedTab()S
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getActiveSheetIndex()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-short v0, v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSheet(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    if-ge v1, v2, :cond_1

    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 12
    .line 13
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSheetName(I)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    if-eqz v2, :cond_0

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 24
    .line 25
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    check-cast v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 30
    .line 31
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_1
    return-object v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getSheetAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;
    .locals 1

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    return-object p1
.end method

.method public bridge synthetic getSheetAt(I)Lcom/intsig/office/fc/ss/usermodel/Sheet;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    move-result-object p1

    return-object p1
.end method

.method public getSheetIndex(Lcom/intsig/office/fc/ss/usermodel/Sheet;)I
    .locals 2

    const/4 v0, 0x0

    .line 2
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method public getSheetIndex(Ljava/lang/String;)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSheetIndex(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getSheetIndexFromExternSheetIndex(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSheetIndexFromExternSheetIndex(I)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getSheetName(I)Ljava/lang/String;
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSheetName(I)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    return-object p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method getUDFFinder()Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_udfFinder:Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method initDrawings()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->findDrawingGroup()Lcom/intsig/office/fc/hssf/model/DrawingManager2;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getNumberOfSheets()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-ge v0, v1, :cond_1

    .line 15
    .line 16
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getDrawingPatriarch()Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 21
    .line 22
    .line 23
    add-int/lit8 v0, v0, 0x1

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->createDrawingGroup()V

    .line 29
    .line 30
    .line 31
    :cond_1
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public insertChartRecord()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    const/16 v1, 0xfc

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->findFirstRecordLocBySid(S)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/16 v1, 0x5a

    .line 10
    .line 11
    new-array v1, v1, [B

    .line 12
    .line 13
    fill-array-data v1, :array_0

    .line 14
    .line 15
    .line 16
    new-instance v2, Lcom/intsig/office/fc/hssf/record/UnknownRecord;

    .line 17
    .line 18
    const/16 v3, 0xeb

    .line 19
    .line 20
    invoke-direct {v2, v3, v1}, Lcom/intsig/office/fc/hssf/record/UnknownRecord;-><init>(I[B)V

    .line 21
    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 24
    .line 25
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getRecords()Ljava/util/List;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-interface {v1, v0, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 30
    .line 31
    .line 32
    return-void

    .line 33
    :array_0
    .array-data 1
        0xft
        0x0t
        0x0t
        -0x10t
        0x52t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x6t
        -0x10t
        0x18t
        0x0t
        0x0t
        0x0t
        0x1t
        0x8t
        0x0t
        0x0t
        0x2t
        0x0t
        0x0t
        0x0t
        0x2t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x3t
        0x0t
        0x0t
        0x0t
        0x33t
        0x0t
        0xbt
        -0x10t
        0x12t
        0x0t
        0x0t
        0x0t
        -0x41t
        0x0t
        0x8t
        0x0t
        0x8t
        0x0t
        -0x7ft
        0x1t
        0x9t
        0x0t
        0x0t
        0x8t
        -0x40t
        0x1t
        0x40t
        0x0t
        0x0t
        0x8t
        0x40t
        0x0t
        0x1et
        -0xft
        0x10t
        0x0t
        0x0t
        0x0t
        0xdt
        0x0t
        0x0t
        0x8t
        0xct
        0x0t
        0x0t
        0x8t
        0x17t
        0x0t
        0x0t
        0x8t
        -0x9t
        0x0t
        0x0t
        0x10t
    .end array-data
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public isEmpty()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isHidden()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getWindowOne()Lcom/intsig/office/fc/hssf/record/WindowOneRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WindowOneRecord;->getHidden()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isSheetHidden(I)Z
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->isSheetHidden(I)Z

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    return p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public isSheetVeryHidden(I)Z
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->isSheetVeryHidden(I)Z

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    return p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public isUsing1904DateWindowing()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->isUsing1904DateWindowing()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isWriteProtected()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->isWriteProtected()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeName(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->names:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->removeName(I)V

    return-void
.end method

.method public removeName(Ljava/lang/String;)V
    .locals 0

    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getNameIndex(Ljava/lang/String;)I

    move-result p1

    .line 4
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->removeName(I)V

    return-void
.end method

.method public removePrintArea(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    add-int/lit8 p1, p1, 0x1

    .line 6
    .line 7
    const/4 v1, 0x6

    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->removeBuiltinRecord(BI)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public removeSheetAt(I)V
    .locals 5

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->isActive()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->isSelected()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 21
    .line 22
    invoke-interface {v2, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 26
    .line 27
    invoke-virtual {v2, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->removeSheet(I)V

    .line 28
    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 31
    .line 32
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    const/4 v3, 0x1

    .line 37
    if-ge v2, v3, :cond_0

    .line 38
    .line 39
    return-void

    .line 40
    :cond_0
    if-lt p1, v2, :cond_1

    .line 41
    .line 42
    add-int/lit8 p1, v2, -0x1

    .line 43
    .line 44
    :cond_1
    if-eqz v0, :cond_2

    .line 45
    .line 46
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->setActiveSheet(I)V

    .line 47
    .line 48
    .line 49
    :cond_2
    if-eqz v1, :cond_5

    .line 50
    .line 51
    const/4 v0, 0x0

    .line 52
    const/4 v1, 0x0

    .line 53
    :goto_0
    if-ge v1, v2, :cond_4

    .line 54
    .line 55
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 56
    .line 57
    .line 58
    move-result-object v4

    .line 59
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->isSelected()Z

    .line 60
    .line 61
    .line 62
    move-result v4

    .line 63
    if-eqz v4, :cond_3

    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_3
    add-int/lit8 v1, v1, 0x1

    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_4
    const/4 v3, 0x0

    .line 70
    :goto_1
    if-nez v3, :cond_5

    .line 71
    .line 72
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->setSelectedTab(I)V

    .line 73
    .line 74
    .line 75
    :cond_5
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected resetFontCache()V
    .locals 1

    .line 1
    new-instance v0, Ljava/util/Hashtable;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->fonts:Ljava/util/Hashtable;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public resolveNameXText(II)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->resolveNameXText(II)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setActiveSheet(I)V
    .locals 5

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 5
    .line 6
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, 0x0

    .line 11
    const/4 v2, 0x0

    .line 12
    :goto_0
    if-ge v2, v0, :cond_1

    .line 13
    .line 14
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 15
    .line 16
    .line 17
    move-result-object v3

    .line 18
    if-ne v2, p1, :cond_0

    .line 19
    .line 20
    const/4 v4, 0x1

    .line 21
    goto :goto_1

    .line 22
    :cond_0
    const/4 v4, 0x0

    .line 23
    :goto_1
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setActive(Z)V

    .line 24
    .line 25
    .line 26
    add-int/lit8 v2, v2, 0x1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getWindowOne()Lcom/intsig/office/fc/hssf/record/WindowOneRecord;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WindowOneRecord;->setActiveSheetIndex(I)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setBackupFlag(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getBackupRecord()Lcom/intsig/office/fc/hssf/record/BackupRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/BackupRecord;->setBackup(S)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDisplayedTab(S)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->setFirstVisibleTab(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFirstVisibleTab(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getWindowOne()Lcom/intsig/office/fc/hssf/record/WindowOneRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WindowOneRecord;->setFirstVisibleTab(I)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setForceFormulaRecalculation(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getRecalcId()Lcom/intsig/office/fc/hssf/record/RecalcIdRecord;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/RecalcIdRecord;->setEngineId(I)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setHidden(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getWindowOne()Lcom/intsig/office/fc/hssf/record/WindowOneRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WindowOneRecord;->setHidden(Z)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMissingCellPolicy(Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->missingCellPolicy:Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPrintArea(IIIII)V
    .locals 2

    .line 10
    new-instance v0, Lcom/intsig/office/fc/hssf/util/CellReference;

    const/4 v1, 0x1

    invoke-direct {v0, p4, p2, v1, v1}, Lcom/intsig/office/fc/hssf/util/CellReference;-><init>(IIZZ)V

    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellReference;->formatAsString()Ljava/lang/String;

    move-result-object p2

    .line 12
    new-instance p4, Lcom/intsig/office/fc/hssf/util/CellReference;

    invoke-direct {p4, p5, p3, v1, v1}, Lcom/intsig/office/fc/hssf/util/CellReference;-><init>(IIZZ)V

    .line 13
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ":"

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Lcom/intsig/office/fc/ss/util/CellReference;->formatAsString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 14
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->setPrintArea(ILjava/lang/String;)V

    return-void
.end method

.method public setPrintArea(ILjava/lang/String;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    add-int/lit8 v1, p1, 0x1

    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSpecificBuiltinRecord(BI)Lcom/intsig/office/fc/hssf/record/NameRecord;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {v0, v2, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->createBuiltInName(BI)Lcom/intsig/office/fc/hssf/record/NameRecord;

    .line 3
    :cond_0
    sget-object v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->COMMA_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object p2

    .line 4
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    const/4 v1, 0x0

    .line 5
    :goto_0
    array-length v2, p2

    if-ge v1, v2, :cond_2

    if-lez v1, :cond_1

    const-string v2, ","

    .line 6
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7
    :cond_1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetName(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/intsig/office/fc/hssf/formula/SheetNameFormatter;->appendFormat(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    const-string v2, "!"

    .line 8
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 9
    aget-object v2, p2, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public setRepeatingRowsAndColumns(IIIII)V
    .locals 22

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move/from16 v1, p1

    .line 4
    .line 5
    move/from16 v4, p2

    .line 6
    .line 7
    move/from16 v5, p3

    .line 8
    .line 9
    move/from16 v11, p4

    .line 10
    .line 11
    move/from16 v12, p5

    .line 12
    .line 13
    const-string v2, "Invalid column range specification"

    .line 14
    .line 15
    const/4 v3, -0x1

    .line 16
    if-ne v4, v3, :cond_1

    .line 17
    .line 18
    if-ne v5, v3, :cond_0

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 22
    .line 23
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    throw v1

    .line 27
    :cond_1
    :goto_0
    const-string v6, "Invalid row range specification"

    .line 28
    .line 29
    if-ne v11, v3, :cond_3

    .line 30
    .line 31
    if-ne v12, v3, :cond_2

    .line 32
    .line 33
    goto :goto_1

    .line 34
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 35
    .line 36
    invoke-direct {v1, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    throw v1

    .line 40
    :cond_3
    :goto_1
    if-lt v4, v3, :cond_12

    .line 41
    .line 42
    const/16 v7, 0xff

    .line 43
    .line 44
    if-ge v4, v7, :cond_12

    .line 45
    .line 46
    if-lt v5, v3, :cond_11

    .line 47
    .line 48
    if-ge v5, v7, :cond_11

    .line 49
    .line 50
    if-lt v11, v3, :cond_10

    .line 51
    .line 52
    const v7, 0xffff

    .line 53
    .line 54
    .line 55
    if-gt v11, v7, :cond_10

    .line 56
    .line 57
    if-lt v12, v3, :cond_f

    .line 58
    .line 59
    if-gt v12, v7, :cond_f

    .line 60
    .line 61
    if-gt v4, v5, :cond_e

    .line 62
    .line 63
    if-gt v11, v12, :cond_d

    .line 64
    .line 65
    invoke-virtual/range {p0 .. p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 66
    .line 67
    .line 68
    move-result-object v13

    .line 69
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->checkExternSheet(I)S

    .line 74
    .line 75
    .line 76
    move-result v14

    .line 77
    const/4 v10, 0x1

    .line 78
    if-eq v4, v3, :cond_4

    .line 79
    .line 80
    if-eq v5, v3, :cond_4

    .line 81
    .line 82
    if-eq v11, v3, :cond_4

    .line 83
    .line 84
    if-eq v12, v3, :cond_4

    .line 85
    .line 86
    const/16 v16, 0x1

    .line 87
    .line 88
    goto :goto_2

    .line 89
    :cond_4
    const/16 v16, 0x0

    .line 90
    .line 91
    :goto_2
    if-ne v4, v3, :cond_5

    .line 92
    .line 93
    if-ne v5, v3, :cond_5

    .line 94
    .line 95
    if-ne v11, v3, :cond_5

    .line 96
    .line 97
    if-ne v12, v3, :cond_5

    .line 98
    .line 99
    const/4 v2, 0x1

    .line 100
    goto :goto_3

    .line 101
    :cond_5
    const/4 v2, 0x0

    .line 102
    :goto_3
    const/4 v3, 0x7

    .line 103
    invoke-direct {v0, v1, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->findExistingBuiltinNameRecordIdx(IB)I

    .line 104
    .line 105
    .line 106
    move-result v6

    .line 107
    if-eqz v2, :cond_7

    .line 108
    .line 109
    if-ltz v6, :cond_6

    .line 110
    .line 111
    iget-object v1, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 112
    .line 113
    invoke-virtual {v1, v6}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->removeName(I)V

    .line 114
    .line 115
    .line 116
    :cond_6
    return-void

    .line 117
    :cond_7
    if-gez v6, :cond_8

    .line 118
    .line 119
    iget-object v2, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 120
    .line 121
    add-int/2addr v1, v10

    .line 122
    invoke-virtual {v2, v3, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->createBuiltInName(BI)Lcom/intsig/office/fc/hssf/record/NameRecord;

    .line 123
    .line 124
    .line 125
    move-result-object v1

    .line 126
    goto :goto_4

    .line 127
    :cond_8
    iget-object v1, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 128
    .line 129
    invoke-virtual {v1, v6}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNameRecord(I)Lcom/intsig/office/fc/hssf/record/NameRecord;

    .line 130
    .line 131
    .line 132
    move-result-object v1

    .line 133
    :goto_4
    move-object v9, v1

    .line 134
    new-instance v8, Ljava/util/ArrayList;

    .line 135
    .line 136
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 137
    .line 138
    .line 139
    if-eqz v16, :cond_9

    .line 140
    .line 141
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/ptg/MemFuncPtg;

    .line 142
    .line 143
    const/16 v2, 0x17

    .line 144
    .line 145
    invoke-direct {v1, v2}, Lcom/intsig/office/fc/hssf/formula/ptg/MemFuncPtg;-><init>(I)V

    .line 146
    .line 147
    .line 148
    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    .line 150
    .line 151
    :cond_9
    if-ltz v4, :cond_a

    .line 152
    .line 153
    new-instance v7, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 154
    .line 155
    const/4 v2, 0x0

    .line 156
    const v3, 0xffff

    .line 157
    .line 158
    .line 159
    const/4 v6, 0x0

    .line 160
    const/16 v17, 0x0

    .line 161
    .line 162
    const/16 v18, 0x0

    .line 163
    .line 164
    const/16 v19, 0x0

    .line 165
    .line 166
    move-object v1, v7

    .line 167
    move/from16 v4, p2

    .line 168
    .line 169
    move/from16 v5, p3

    .line 170
    .line 171
    move-object v15, v7

    .line 172
    move/from16 v7, v17

    .line 173
    .line 174
    move-object/from16 v20, v8

    .line 175
    .line 176
    move/from16 v8, v18

    .line 177
    .line 178
    move-object/from16 v21, v9

    .line 179
    .line 180
    move/from16 v9, v19

    .line 181
    .line 182
    const/4 v0, 0x1

    .line 183
    move v10, v14

    .line 184
    invoke-direct/range {v1 .. v10}, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;-><init>(IIIIZZZZI)V

    .line 185
    .line 186
    .line 187
    move-object/from16 v10, v20

    .line 188
    .line 189
    invoke-interface {v10, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    .line 191
    .line 192
    goto :goto_5

    .line 193
    :cond_a
    move-object v10, v8

    .line 194
    move-object/from16 v21, v9

    .line 195
    .line 196
    const/4 v0, 0x1

    .line 197
    :goto_5
    if-ltz v11, :cond_b

    .line 198
    .line 199
    new-instance v15, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 200
    .line 201
    const/4 v4, 0x0

    .line 202
    const/16 v5, 0xff

    .line 203
    .line 204
    const/4 v6, 0x0

    .line 205
    const/4 v7, 0x0

    .line 206
    const/4 v8, 0x0

    .line 207
    const/4 v9, 0x0

    .line 208
    move-object v1, v15

    .line 209
    move/from16 v2, p4

    .line 210
    .line 211
    move/from16 v3, p5

    .line 212
    .line 213
    move-object v11, v10

    .line 214
    move v10, v14

    .line 215
    invoke-direct/range {v1 .. v10}, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;-><init>(IIIIZZZZI)V

    .line 216
    .line 217
    .line 218
    invoke-interface {v11, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    .line 220
    .line 221
    goto :goto_6

    .line 222
    :cond_b
    move-object v11, v10

    .line 223
    :goto_6
    if-eqz v16, :cond_c

    .line 224
    .line 225
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/UnionPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;

    .line 226
    .line 227
    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    .line 229
    .line 230
    :cond_c
    invoke-interface {v11}, Ljava/util/List;->size()I

    .line 231
    .line 232
    .line 233
    move-result v1

    .line 234
    new-array v1, v1, [Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 235
    .line 236
    invoke-interface {v11, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 237
    .line 238
    .line 239
    move-object/from16 v2, v21

    .line 240
    .line 241
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/hssf/record/NameRecord;->setNameDefinition([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 242
    .line 243
    .line 244
    invoke-virtual {v13}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getPrintSetup()Lcom/intsig/office/fc/hssf/usermodel/HSSFPrintSetup;

    .line 245
    .line 246
    .line 247
    move-result-object v1

    .line 248
    const/4 v2, 0x0

    .line 249
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPrintSetup;->setValidSettings(Z)V

    .line 250
    .line 251
    .line 252
    invoke-virtual {v13, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setActive(Z)V

    .line 253
    .line 254
    .line 255
    return-void

    .line 256
    :cond_d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 257
    .line 258
    invoke-direct {v0, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 259
    .line 260
    .line 261
    throw v0

    .line 262
    :cond_e
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 263
    .line 264
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 265
    .line 266
    .line 267
    throw v0

    .line 268
    :cond_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 269
    .line 270
    invoke-direct {v0, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 271
    .line 272
    .line 273
    throw v0

    .line 274
    :cond_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 275
    .line 276
    invoke-direct {v0, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 277
    .line 278
    .line 279
    throw v0

    .line 280
    :cond_11
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 281
    .line 282
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 283
    .line 284
    .line 285
    throw v0

    .line 286
    :cond_12
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 287
    .line 288
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 289
    .line 290
    .line 291
    throw v0
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method public setSelectedTab(I)V
    .locals 5

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x1

    if-ge v2, v0, :cond_1

    .line 3
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    move-result-object v4

    if-ne v2, p1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v4, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setSelected(Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4
    :cond_1
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getWindowOne()Lcom/intsig/office/fc/hssf/record/WindowOneRecord;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/intsig/office/fc/hssf/record/WindowOneRecord;->setNumSelectedTabs(S)V

    return-void
.end method

.method public setSelectedTab(S)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->setSelectedTab(I)V

    return-void
.end method

.method public setSelectedTabs([I)V
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    array-length v2, p1

    .line 4
    if-ge v1, v2, :cond_0

    .line 5
    .line 6
    aget v2, p1, v1

    .line 7
    .line 8
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 9
    .line 10
    .line 11
    add-int/lit8 v1, v1, 0x1

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 15
    .line 16
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    const/4 v2, 0x0

    .line 21
    :goto_1
    if-ge v2, v1, :cond_3

    .line 22
    .line 23
    const/4 v3, 0x0

    .line 24
    :goto_2
    array-length v4, p1

    .line 25
    if-ge v3, v4, :cond_2

    .line 26
    .line 27
    aget v4, p1, v3

    .line 28
    .line 29
    if-ne v4, v2, :cond_1

    .line 30
    .line 31
    const/4 v3, 0x1

    .line 32
    goto :goto_3

    .line 33
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 34
    .line 35
    goto :goto_2

    .line 36
    :cond_2
    const/4 v3, 0x0

    .line 37
    :goto_3
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    invoke-virtual {v4, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setSelected(Z)V

    .line 42
    .line 43
    .line 44
    add-int/lit8 v2, v2, 0x1

    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_3
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getWindowOne()Lcom/intsig/office/fc/hssf/record/WindowOneRecord;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    array-length p1, p1

    .line 54
    int-to-short p1, p1

    .line 55
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WindowOneRecord;->setNumSelectedTabs(S)V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setSheetHidden(II)V
    .locals 1

    .line 3
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 4
    invoke-static {p2}, Lcom/intsig/office/fc/ss/util/WorkbookUtil;->validateSheetState(I)V

    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->setSheetHidden(II)V

    return-void
.end method

.method public setSheetHidden(IZ)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->setSheetHidden(IZ)V

    return-void
.end method

.method public setSheetName(ILjava/lang/String;)V
    .locals 1

    .line 1
    if-eqz p2, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 4
    .line 5
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->doesContainsSheetName(Ljava/lang/String;I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->validateSheetIndex(I)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 15
    .line 16
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->setSheetName(ILjava/lang/String;)V

    .line 17
    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 21
    .line 22
    const-string p2, "The workbook already contains a sheet with this name"

    .line 23
    .line 24
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    throw p1

    .line 28
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 29
    .line 30
    const-string p2, "sheetName must not be null"

    .line 31
    .line 32
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    throw p1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setSheetOrder(Ljava/lang/String;I)V
    .locals 3

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetIndex(Ljava/lang/String;)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 6
    .line 7
    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    check-cast v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 12
    .line 13
    invoke-interface {v1, p2, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 14
    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 17
    .line 18
    invoke-virtual {v1, p1, p2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->setSheetOrder(Ljava/lang/String;I)V

    .line 19
    .line 20
    .line 21
    invoke-static {v0, p2}, Lcom/intsig/office/fc/hssf/formula/FormulaShifter;->createForSheetShift(II)Lcom/intsig/office/fc/hssf/formula/FormulaShifter;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->_sheets:Ljava/util/List;

    .line 26
    .line 27
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-eqz v0, :cond_0

    .line 36
    .line 37
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    check-cast v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 42
    .line 43
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    const/4 v1, -0x1

    .line 48
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->updateFormulasAfterCellShift(Lcom/intsig/office/fc/hssf/formula/FormulaShifter;I)V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 53
    .line 54
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->updateNamesAfterCellShift(Lcom/intsig/office/fc/hssf/formula/FormulaShifter;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public unwriteProtectWorkbook()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->unwriteProtectWorkbook()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getBytes()[B

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;

    .line 6
    .line 7
    invoke-direct {v1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;-><init>()V

    .line 8
    .line 9
    .line 10
    new-instance v2, Ljava/util/ArrayList;

    .line 11
    .line 12
    const/4 v3, 0x1

    .line 13
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 14
    .line 15
    .line 16
    new-instance v3, Ljava/io/ByteArrayInputStream;

    .line 17
    .line 18
    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 19
    .line 20
    .line 21
    const-string v0, "Workbook"

    .line 22
    .line 23
    invoke-virtual {v1, v3, v0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->createDocument(Ljava/io/InputStream;Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0, v1, v2}, Lcom/intsig/office/fc/POIDocument;->writeProperties(Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Ljava/util/List;)V

    .line 27
    .line 28
    .line 29
    iget-boolean v3, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->preserveNodes:Z

    .line 30
    .line 31
    if-eqz v3, :cond_0

    .line 32
    .line 33
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    const-string v0, "WORKBOOK"

    .line 37
    .line 38
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/office/fc/POIDocument;->directory:Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 42
    .line 43
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 44
    .line 45
    .line 46
    move-result-object v3

    .line 47
    invoke-virtual {p0, v0, v3, v2}, Lcom/intsig/office/fc/POIDocument;->copyNodes(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;Ljava/util/List;)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    iget-object v2, p0, Lcom/intsig/office/fc/POIDocument;->directory:Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 55
    .line 56
    invoke-virtual {v2}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->getStorageClsid()Lcom/intsig/office/fc/hpsf/ClassID;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->setStorageClsid(Lcom/intsig/office/fc/hpsf/ClassID;)V

    .line 61
    .line 62
    .line 63
    :cond_0
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->writeFilesystem(Ljava/io/OutputStream;)V

    .line 64
    .line 65
    .line 66
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public writeProtectWorkbook(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->writeProtectWorkbook(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

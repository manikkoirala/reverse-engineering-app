.class public Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;
.super Ljava/lang/Object;
.source "ContinuableRecordInput.java"

# interfaces
.implements Lcom/intsig/office/fc/util/LittleEndianInput;


# instance fields
.field private final _in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public available()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->available()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public readByte()B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readByte()B

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public readDouble()D
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readDouble()D

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    return-wide v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public readFully([B)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readFully([B)V

    return-void
.end method

.method public readFully([BII)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readFully([BII)V

    return-void
.end method

.method public readInt()I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUByte()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUByte()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 14
    .line 15
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUByte()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 20
    .line 21
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUByte()I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    shl-int/lit8 v3, v3, 0x18

    .line 26
    .line 27
    shl-int/lit8 v2, v2, 0x10

    .line 28
    .line 29
    add-int/2addr v3, v2

    .line 30
    shl-int/lit8 v1, v1, 0x8

    .line 31
    .line 32
    add-int/2addr v3, v1

    .line 33
    shl-int/lit8 v0, v0, 0x0

    .line 34
    .line 35
    add-int/2addr v3, v0

    .line 36
    return v3
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public readLong()J
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUByte()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUByte()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 14
    .line 15
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUByte()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 20
    .line 21
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUByte()I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    iget-object v4, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 26
    .line 27
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUByte()I

    .line 28
    .line 29
    .line 30
    move-result v4

    .line 31
    iget-object v5, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 32
    .line 33
    invoke-virtual {v5}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUByte()I

    .line 34
    .line 35
    .line 36
    move-result v5

    .line 37
    iget-object v6, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 38
    .line 39
    invoke-virtual {v6}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUByte()I

    .line 40
    .line 41
    .line 42
    move-result v6

    .line 43
    iget-object v7, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 44
    .line 45
    invoke-virtual {v7}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUByte()I

    .line 46
    .line 47
    .line 48
    move-result v7

    .line 49
    int-to-long v7, v7

    .line 50
    const/16 v9, 0x38

    .line 51
    .line 52
    shl-long/2addr v7, v9

    .line 53
    int-to-long v9, v6

    .line 54
    const/16 v6, 0x30

    .line 55
    .line 56
    shl-long/2addr v9, v6

    .line 57
    add-long/2addr v7, v9

    .line 58
    int-to-long v5, v5

    .line 59
    const/16 v9, 0x28

    .line 60
    .line 61
    shl-long/2addr v5, v9

    .line 62
    add-long/2addr v7, v5

    .line 63
    int-to-long v4, v4

    .line 64
    const/16 v6, 0x20

    .line 65
    .line 66
    shl-long/2addr v4, v6

    .line 67
    add-long/2addr v7, v4

    .line 68
    int-to-long v3, v3

    .line 69
    const/16 v5, 0x18

    .line 70
    .line 71
    shl-long/2addr v3, v5

    .line 72
    add-long/2addr v7, v3

    .line 73
    shl-int/lit8 v2, v2, 0x10

    .line 74
    .line 75
    int-to-long v2, v2

    .line 76
    add-long/2addr v7, v2

    .line 77
    shl-int/lit8 v1, v1, 0x8

    .line 78
    .line 79
    int-to-long v1, v1

    .line 80
    add-long/2addr v7, v1

    .line 81
    shl-int/lit8 v0, v0, 0x0

    .line 82
    .line 83
    int-to-long v0, v0

    .line 84
    add-long/2addr v7, v0

    .line 85
    return-wide v7
    .line 86
.end method

.method public readShort()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public readUByte()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->_in:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUByte()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public readUShort()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->readUByte()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordInput;->readUByte()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    shl-int/lit8 v1, v1, 0x8

    .line 10
    .line 11
    shl-int/lit8 v0, v0, 0x0

    .line 12
    .line 13
    add-int/2addr v1, v0

    .line 14
    return v1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/office/fc/hssf/usermodel/HSSFFooter;
.super Lcom/intsig/office/fc/hssf/usermodel/HeaderFooter;
.source "HSSFFooter.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/Footer;


# instance fields
.field private final _psb:Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;


# direct methods
.method protected constructor <init>(Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HeaderFooter;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFooter;->_psb:Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method protected getRawText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFooter;->_psb:Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->getFooter()Lcom/intsig/office/fc/hssf/record/FooterRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const-string v0, ""

    .line 10
    .line 11
    return-object v0

    .line 12
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/HeaderFooterBase;->getText()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected setHeaderFooterText(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFooter;->_psb:Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->getFooter()Lcom/intsig/office/fc/hssf/record/FooterRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/office/fc/hssf/record/FooterRecord;

    .line 10
    .line 11
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/record/FooterRecord;-><init>(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFooter;->_psb:Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 15
    .line 16
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->setFooter(Lcom/intsig/office/fc/hssf/record/FooterRecord;)V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/HeaderFooterBase;->setText(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    :goto_0
    return-void
    .line 24
.end method

.class public final Lcom/intsig/office/fc/hssf/record/DVRecord;
.super Lcom/intsig/office/fc/hssf/record/StandardRecord;
.source "DVRecord.java"


# static fields
.field private static final NULL_TEXT_STRING:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

.field private static final opt_condition_operator:Lcom/intsig/office/fc/util/BitField;

.field private static final opt_data_type:Lcom/intsig/office/fc/util/BitField;

.field private static final opt_empty_cell_allowed:Lcom/intsig/office/fc/util/BitField;

.field private static final opt_error_style:Lcom/intsig/office/fc/util/BitField;

.field private static final opt_show_error_on_invalid_value:Lcom/intsig/office/fc/util/BitField;

.field private static final opt_show_prompt_on_cell_selected:Lcom/intsig/office/fc/util/BitField;

.field private static final opt_string_list_formula:Lcom/intsig/office/fc/util/BitField;

.field private static final opt_suppress_dropdown_arrow:Lcom/intsig/office/fc/util/BitField;

.field public static final sid:S = 0x1bes


# instance fields
.field private _errorText:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

.field private _errorTitle:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

.field private _formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

.field private _formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

.field private _not_used_1:S

.field private _not_used_2:S

.field private _option_flags:I

.field private _promptText:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

.field private _promptTitle:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

.field private _regions:Lcom/intsig/office/fc/ss/util/CellRangeAddressList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 2
    .line 3
    const-string v1, "\u0000"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->NULL_TEXT_STRING:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 9
    .line 10
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 11
    .line 12
    const/16 v1, 0xf

    .line 13
    .line 14
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 15
    .line 16
    .line 17
    sput-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_data_type:Lcom/intsig/office/fc/util/BitField;

    .line 18
    .line 19
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 20
    .line 21
    const/16 v1, 0x70

    .line 22
    .line 23
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 24
    .line 25
    .line 26
    sput-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_error_style:Lcom/intsig/office/fc/util/BitField;

    .line 27
    .line 28
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 29
    .line 30
    const/16 v1, 0x80

    .line 31
    .line 32
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 33
    .line 34
    .line 35
    sput-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_string_list_formula:Lcom/intsig/office/fc/util/BitField;

    .line 36
    .line 37
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 38
    .line 39
    const/16 v1, 0x100

    .line 40
    .line 41
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 42
    .line 43
    .line 44
    sput-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_empty_cell_allowed:Lcom/intsig/office/fc/util/BitField;

    .line 45
    .line 46
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 47
    .line 48
    const/16 v1, 0x200

    .line 49
    .line 50
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 51
    .line 52
    .line 53
    sput-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_suppress_dropdown_arrow:Lcom/intsig/office/fc/util/BitField;

    .line 54
    .line 55
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 56
    .line 57
    const/high16 v1, 0x40000

    .line 58
    .line 59
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 60
    .line 61
    .line 62
    sput-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_show_prompt_on_cell_selected:Lcom/intsig/office/fc/util/BitField;

    .line 63
    .line 64
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 65
    .line 66
    const/high16 v1, 0x80000

    .line 67
    .line 68
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 69
    .line 70
    .line 71
    sput-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_show_error_on_invalid_value:Lcom/intsig/office/fc/util/BitField;

    .line 72
    .line 73
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 74
    .line 75
    const/high16 v1, 0x700000

    .line 76
    .line 77
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 78
    .line 79
    .line 80
    sput-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_condition_operator:Lcom/intsig/office/fc/util/BitField;

    .line 81
    .line 82
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>(IIIZZZZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Lcom/intsig/office/fc/ss/util/CellRangeAddressList;)V
    .locals 4

    move-object v0, p0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    const/16 v1, 0x3fe0

    .line 2
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_not_used_1:S

    const/4 v1, 0x0

    .line 3
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_not_used_2:S

    .line 4
    sget-object v2, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_data_type:Lcom/intsig/office/fc/util/BitField;

    move v3, p1

    invoke-virtual {v2, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    move-result v1

    .line 5
    sget-object v2, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_condition_operator:Lcom/intsig/office/fc/util/BitField;

    move v3, p2

    invoke-virtual {v2, v1, p2}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    move-result v1

    .line 6
    sget-object v2, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_error_style:Lcom/intsig/office/fc/util/BitField;

    move v3, p3

    invoke-virtual {v2, v1, p3}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    move-result v1

    .line 7
    sget-object v2, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_empty_cell_allowed:Lcom/intsig/office/fc/util/BitField;

    move v3, p4

    invoke-virtual {v2, v1, p4}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    move-result v1

    .line 8
    sget-object v2, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_suppress_dropdown_arrow:Lcom/intsig/office/fc/util/BitField;

    move v3, p5

    invoke-virtual {v2, v1, p5}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    move-result v1

    .line 9
    sget-object v2, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_string_list_formula:Lcom/intsig/office/fc/util/BitField;

    move v3, p6

    invoke-virtual {v2, v1, p6}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    move-result v1

    .line 10
    sget-object v2, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_show_prompt_on_cell_selected:Lcom/intsig/office/fc/util/BitField;

    move v3, p7

    invoke-virtual {v2, v1, p7}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    move-result v1

    .line 11
    sget-object v2, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_show_error_on_invalid_value:Lcom/intsig/office/fc/util/BitField;

    move v3, p10

    invoke-virtual {v2, v1, p10}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    move-result v1

    .line 12
    iput v1, v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_option_flags:I

    .line 13
    invoke-static {p8}, Lcom/intsig/office/fc/hssf/record/DVRecord;->resolveTitleText(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    move-result-object v1

    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_promptTitle:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 14
    invoke-static {p9}, Lcom/intsig/office/fc/hssf/record/DVRecord;->resolveTitleText(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    move-result-object v1

    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_promptText:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 15
    invoke-static {p11}, Lcom/intsig/office/fc/hssf/record/DVRecord;->resolveTitleText(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    move-result-object v1

    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_errorTitle:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 16
    invoke-static/range {p12 .. p12}, Lcom/intsig/office/fc/hssf/record/DVRecord;->resolveTitleText(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    move-result-object v1

    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_errorText:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 17
    invoke-static/range {p13 .. p13}, Lcom/intsig/office/fc/hssf/formula/Formula;->create([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Lcom/intsig/office/fc/hssf/formula/Formula;

    move-result-object v1

    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 18
    invoke-static/range {p14 .. p14}, Lcom/intsig/office/fc/hssf/formula/Formula;->create([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Lcom/intsig/office/fc/hssf/formula/Formula;

    move-result-object v1

    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

    move-object/from16 v1, p15

    .line 19
    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_regions:Lcom/intsig/office/fc/ss/util/CellRangeAddressList;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V
    .locals 2

    .line 20
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    const/16 v0, 0x3fe0

    .line 21
    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_not_used_1:S

    const/4 v0, 0x0

    .line 22
    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_not_used_2:S

    .line 23
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_option_flags:I

    .line 24
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/record/DVRecord;->readUnicodeString(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_promptTitle:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 25
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/record/DVRecord;->readUnicodeString(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_errorTitle:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 26
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/record/DVRecord;->readUnicodeString(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_promptText:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 27
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/record/DVRecord;->readUnicodeString(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_errorText:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 28
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUShort()I

    move-result v0

    .line 29
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v1

    iput-short v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_not_used_1:S

    .line 30
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->read(ILcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/Formula;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 31
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUShort()I

    move-result v0

    .line 32
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v1

    iput-short v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_not_used_2:S

    .line 33
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->read(ILcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/Formula;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 34
    new-instance v0, Lcom/intsig/office/fc/ss/util/CellRangeAddressList;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressList;-><init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_regions:Lcom/intsig/office/fc/ss/util/CellRangeAddressList;

    return-void
.end method

.method private static appendFormula(Ljava/lang/StringBuffer;Ljava/lang/String;Lcom/intsig/office/fc/hssf/formula/Formula;)V
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2
    .line 3
    .line 4
    if-nez p2, :cond_0

    .line 5
    .line 6
    const-string p1, "<empty>\n"

    .line 7
    .line 8
    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 9
    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/Formula;->getTokens()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    const/16 p2, 0xa

    .line 17
    .line 18
    invoke-virtual {p0, p2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 19
    .line 20
    .line 21
    const/4 v0, 0x0

    .line 22
    :goto_0
    array-length v1, p1

    .line 23
    if-ge v0, v1, :cond_1

    .line 24
    .line 25
    const/16 v1, 0x9

    .line 26
    .line 27
    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 28
    .line 29
    .line 30
    aget-object v1, p1, v0

    .line 31
    .line 32
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0, p2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 40
    .line 41
    .line 42
    add-int/lit8 v0, v0, 0x1

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private static formatTextTitle(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;)Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;->getString()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x1

    .line 10
    if-ne v0, v1, :cond_0

    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    const-string p0, "\'\\0\'"

    .line 20
    .line 21
    :cond_0
    return-object p0
    .line 22
    .line 23
    .line 24
.end method

.method private static getUnicodeStringSize(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;)I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;->getString()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-static {p0}, Lcom/intsig/office/fc/util/StringUtil;->hasMultibyte(Ljava/lang/String;)Z

    .line 10
    .line 11
    .line 12
    move-result p0

    .line 13
    if-eqz p0, :cond_0

    .line 14
    .line 15
    const/4 p0, 0x2

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 p0, 0x1

    .line 18
    :goto_0
    mul-int v0, v0, p0

    .line 19
    .line 20
    add-int/lit8 v0, v0, 0x3

    .line 21
    .line 22
    return v0
    .line 23
    .line 24
.end method

.method private static readUnicodeString(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)Lcom/intsig/office/fc/hssf/record/common/UnicodeString;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;-><init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static resolveTitleText(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/record/common/UnicodeString;
    .locals 2

    .line 1
    if-eqz p0, :cond_1

    .line 2
    .line 3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-ge v0, v1, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 12
    .line 13
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;-><init>(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-object v0

    .line 17
    :cond_1
    :goto_0
    sget-object p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->NULL_TEXT_STRING:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 18
    .line 19
    return-object p0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static serializeUnicodeString(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;->getString()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-static {p1, p0}, Lcom/intsig/office/fc/util/StringUtil;->writeUnicodeString(Lcom/intsig/office/fc/util/LittleEndianOutput;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/Record;->cloneViaReserialise()Lcom/intsig/office/fc/hssf/record/Record;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCellRangeAddress()Lcom/intsig/office/fc/ss/util/CellRangeAddressList;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_regions:Lcom/intsig/office/fc/ss/util/CellRangeAddressList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getConditionOperator()I
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_condition_operator:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_option_flags:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getDataSize()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_promptTitle:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/DVRecord;->getUnicodeStringSize(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    add-int/lit8 v0, v0, 0xc

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_errorTitle:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 10
    .line 11
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/record/DVRecord;->getUnicodeStringSize(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;)I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    add-int/2addr v0, v1

    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_promptText:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 17
    .line 18
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/record/DVRecord;->getUnicodeStringSize(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;)I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    add-int/2addr v0, v1

    .line 23
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_errorText:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 24
    .line 25
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/record/DVRecord;->getUnicodeStringSize(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;)I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    add-int/2addr v0, v1

    .line 30
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 31
    .line 32
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/formula/Formula;->getEncodedTokenSize()I

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    add-int/2addr v0, v1

    .line 37
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 38
    .line 39
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/formula/Formula;->getEncodedTokenSize()I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    add-int/2addr v0, v1

    .line 44
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_regions:Lcom/intsig/office/fc/ss/util/CellRangeAddressList;

    .line 45
    .line 46
    invoke-virtual {v1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressList;->getSize()I

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    add-int/2addr v0, v1

    .line 51
    return v0
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getDataType()I
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_data_type:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_option_flags:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEmptyCellAllowed()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_empty_cell_allowed:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_option_flags:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getErrorStyle()I
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_error_style:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_option_flags:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getListExplicitFormula()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_string_list_formula:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_option_flags:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShowErrorOnInvalidValue()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_show_error_on_invalid_value:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_option_flags:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShowPromptOnCellSelected()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_show_prompt_on_cell_selected:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_option_flags:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSid()S
    .locals 1

    .line 1
    const/16 v0, 0x1be

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSuppressDropdownArrow()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/DVRecord;->opt_suppress_dropdown_arrow:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_option_flags:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_option_flags:I

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeInt(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_promptTitle:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hssf/record/DVRecord;->serializeUnicodeString(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_errorTitle:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 12
    .line 13
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hssf/record/DVRecord;->serializeUnicodeString(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_promptText:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 17
    .line 18
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hssf/record/DVRecord;->serializeUnicodeString(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_errorText:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 22
    .line 23
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hssf/record/DVRecord;->serializeUnicodeString(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/Formula;->getEncodedTokenSize()I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 33
    .line 34
    .line 35
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_not_used_1:S

    .line 36
    .line 37
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 41
    .line 42
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->serializeTokens(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 43
    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/Formula;->getEncodedTokenSize()I

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 52
    .line 53
    .line 54
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_not_used_2:S

    .line 55
    .line 56
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 57
    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 60
    .line 61
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->serializeTokens(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 62
    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_regions:Lcom/intsig/office/fc/ss/util/CellRangeAddressList;

    .line 65
    .line 66
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressList;->serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "[DV]\n"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 9
    .line 10
    .line 11
    const-string v1, " options="

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 14
    .line 15
    .line 16
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_option_flags:I

    .line 17
    .line 18
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 23
    .line 24
    .line 25
    const-string v1, " title-prompt="

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 28
    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_promptTitle:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 31
    .line 32
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/record/DVRecord;->formatTextTitle(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 37
    .line 38
    .line 39
    const-string v1, " title-error="

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 42
    .line 43
    .line 44
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_errorTitle:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 45
    .line 46
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/record/DVRecord;->formatTextTitle(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 51
    .line 52
    .line 53
    const-string v1, " text-prompt="

    .line 54
    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 56
    .line 57
    .line 58
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_promptText:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 59
    .line 60
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/record/DVRecord;->formatTextTitle(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;)Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 65
    .line 66
    .line 67
    const-string v1, " text-error="

    .line 68
    .line 69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 70
    .line 71
    .line 72
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_errorText:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 73
    .line 74
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/record/DVRecord;->formatTextTitle(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;)Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 79
    .line 80
    .line 81
    const-string v1, "\n"

    .line 82
    .line 83
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 84
    .line 85
    .line 86
    const-string v2, "Formula 1:"

    .line 87
    .line 88
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_formula1:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 89
    .line 90
    invoke-static {v0, v2, v3}, Lcom/intsig/office/fc/hssf/record/DVRecord;->appendFormula(Ljava/lang/StringBuffer;Ljava/lang/String;Lcom/intsig/office/fc/hssf/formula/Formula;)V

    .line 91
    .line 92
    .line 93
    const-string v2, "Formula 2:"

    .line 94
    .line 95
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_formula2:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 96
    .line 97
    invoke-static {v0, v2, v3}, Lcom/intsig/office/fc/hssf/record/DVRecord;->appendFormula(Ljava/lang/StringBuffer;Ljava/lang/String;Lcom/intsig/office/fc/hssf/formula/Formula;)V

    .line 98
    .line 99
    .line 100
    const-string v2, "Regions: "

    .line 101
    .line 102
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 103
    .line 104
    .line 105
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_regions:Lcom/intsig/office/fc/ss/util/CellRangeAddressList;

    .line 106
    .line 107
    invoke-virtual {v2}, Lcom/intsig/office/fc/ss/util/CellRangeAddressList;->countRanges()I

    .line 108
    .line 109
    .line 110
    move-result v2

    .line 111
    const/4 v3, 0x0

    .line 112
    :goto_0
    if-ge v3, v2, :cond_1

    .line 113
    .line 114
    if-lez v3, :cond_0

    .line 115
    .line 116
    const-string v4, ", "

    .line 117
    .line 118
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 119
    .line 120
    .line 121
    :cond_0
    iget-object v4, p0, Lcom/intsig/office/fc/hssf/record/DVRecord;->_regions:Lcom/intsig/office/fc/ss/util/CellRangeAddressList;

    .line 122
    .line 123
    invoke-virtual {v4, v3}, Lcom/intsig/office/fc/ss/util/CellRangeAddressList;->getCellRangeAddress(I)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 124
    .line 125
    .line 126
    move-result-object v4

    .line 127
    const/16 v5, 0x28

    .line 128
    .line 129
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 130
    .line 131
    .line 132
    invoke-virtual {v4}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 133
    .line 134
    .line 135
    move-result v5

    .line 136
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 137
    .line 138
    .line 139
    const/16 v5, 0x2c

    .line 140
    .line 141
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 142
    .line 143
    .line 144
    invoke-virtual {v4}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 145
    .line 146
    .line 147
    move-result v6

    .line 148
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 149
    .line 150
    .line 151
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 152
    .line 153
    .line 154
    invoke-virtual {v4}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 155
    .line 156
    .line 157
    move-result v6

    .line 158
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 159
    .line 160
    .line 161
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 162
    .line 163
    .line 164
    invoke-virtual {v4}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    .line 165
    .line 166
    .line 167
    move-result v4

    .line 168
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 169
    .line 170
    .line 171
    const/16 v4, 0x29

    .line 172
    .line 173
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 174
    .line 175
    .line 176
    add-int/lit8 v3, v3, 0x1

    .line 177
    .line 178
    goto :goto_0

    .line 179
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 180
    .line 181
    .line 182
    const-string v1, "[/DV]"

    .line 183
    .line 184
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 185
    .line 186
    .line 187
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 188
    .line 189
    .line 190
    move-result-object v0

    .line 191
    return-object v0
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

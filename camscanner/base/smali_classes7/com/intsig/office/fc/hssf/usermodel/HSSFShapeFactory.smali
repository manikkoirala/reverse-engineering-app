.class public final Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeFactory;
.super Ljava/lang/Object;
.source "HSSFShapeFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static createShape(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Ljava/util/Map;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;)Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/ss/model/XLSModel/AWorkbook;",
            "Ljava/util/Map<",
            "Lcom/intsig/office/fc/ddf/EscherRecord;",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;",
            "Lcom/intsig/office/fc/ddf/EscherContainerRecord;",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;",
            ")",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;"
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/16 v1, -0xffd

    .line 6
    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeFactory;->createShapeGroup(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Ljava/util/Map;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;)Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;

    .line 10
    .line 11
    .line 12
    move-result-object p0

    .line 13
    return-object p0

    .line 14
    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeFactory;->createSimpeShape(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Ljava/util/Map;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;)Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;

    .line 15
    .line 16
    .line 17
    move-result-object p0

    .line 18
    return-object p0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public static createShapeGroup(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Ljava/util/Map;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;)Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/ss/model/XLSModel/AWorkbook;",
            "Ljava/util/Map<",
            "Lcom/intsig/office/fc/ddf/EscherRecord;",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;",
            "Lcom/intsig/office/fc/ddf/EscherContainerRecord;",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;",
            ")",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;"
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x0

    .line 10
    if-lez v0, :cond_7

    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 18
    .line 19
    if-nez p3, :cond_0

    .line 20
    .line 21
    const/16 v3, -0xff0

    .line 22
    .line 23
    invoke-static {v2, v3}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;

    .line 28
    .line 29
    if-eqz v3, :cond_1

    .line 30
    .line 31
    invoke-virtual {v3}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getCol2()S

    .line 32
    .line 33
    .line 34
    move-result v4

    .line 35
    const/16 v5, 0xff

    .line 36
    .line 37
    if-gt v4, v5, :cond_1

    .line 38
    .line 39
    invoke-virtual {v3}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getRow2()S

    .line 40
    .line 41
    .line 42
    move-result v4

    .line 43
    const v5, 0xffff

    .line 44
    .line 45
    .line 46
    if-gt v4, v5, :cond_1

    .line 47
    .line 48
    invoke-static {v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->toClientAnchor(Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;)Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;

    .line 49
    .line 50
    .line 51
    move-result-object v3

    .line 52
    goto :goto_0

    .line 53
    :cond_0
    const/16 v3, -0xff1

    .line 54
    .line 55
    invoke-static {v2, v3}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 56
    .line 57
    .line 58
    move-result-object v3

    .line 59
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;

    .line 60
    .line 61
    if-eqz v3, :cond_1

    .line 62
    .line 63
    invoke-static {v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->toChildAnchor(Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;)Lcom/intsig/office/fc/hssf/usermodel/HSSFChildAnchor;

    .line 64
    .line 65
    .line 66
    move-result-object v3

    .line 67
    goto :goto_0

    .line 68
    :cond_1
    move-object v3, v1

    .line 69
    :goto_0
    if-nez v3, :cond_2

    .line 70
    .line 71
    new-instance v3, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;

    .line 72
    .line 73
    invoke-direct {v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;-><init>()V

    .line 74
    .line 75
    .line 76
    :cond_2
    const/16 v4, -0xede

    .line 77
    .line 78
    invoke-static {v2, v4}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 79
    .line 80
    .line 81
    move-result-object v4

    .line 82
    const/4 v5, 0x1

    .line 83
    if-eqz v4, :cond_4

    .line 84
    .line 85
    new-instance v6, Lcom/intsig/office/fc/ddf/EscherPropertyFactory;

    .line 86
    .line 87
    invoke-direct {v6}, Lcom/intsig/office/fc/ddf/EscherPropertyFactory;-><init>()V

    .line 88
    .line 89
    .line 90
    invoke-virtual {v4}, Lcom/intsig/office/fc/ddf/EscherRecord;->serialize()[B

    .line 91
    .line 92
    .line 93
    move-result-object v7

    .line 94
    const/16 v8, 0x8

    .line 95
    .line 96
    invoke-virtual {v4}, Lcom/intsig/office/fc/ddf/EscherRecord;->getInstance()S

    .line 97
    .line 98
    .line 99
    move-result v4

    .line 100
    invoke-virtual {v6, v7, v8, v4}, Lcom/intsig/office/fc/ddf/EscherPropertyFactory;->createProperties([BIS)Ljava/util/List;

    .line 101
    .line 102
    .line 103
    move-result-object v4

    .line 104
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 109
    .line 110
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherProperty;->getPropertyNumber()S

    .line 111
    .line 112
    .line 113
    move-result v4

    .line 114
    const/16 v6, 0x39f

    .line 115
    .line 116
    if-ne v4, v6, :cond_3

    .line 117
    .line 118
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    if-eq v0, v5, :cond_5

    .line 123
    .line 124
    :cond_3
    new-instance v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;

    .line 125
    .line 126
    invoke-direct {v1, v2, p3, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)V

    .line 127
    .line 128
    .line 129
    goto :goto_1

    .line 130
    :cond_4
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;

    .line 131
    .line 132
    invoke-direct {v0, v2, p3, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)V

    .line 133
    .line 134
    .line 135
    move-object v1, v0

    .line 136
    :cond_5
    :goto_1
    const/16 p3, -0xff7

    .line 137
    .line 138
    invoke-static {v2, p3}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 139
    .line 140
    .line 141
    move-result-object p3

    .line 142
    check-cast p3, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;

    .line 143
    .line 144
    if-eqz p3, :cond_6

    .line 145
    .line 146
    invoke-virtual {p3}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectX1()I

    .line 147
    .line 148
    .line 149
    move-result v0

    .line 150
    invoke-virtual {p3}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectY1()I

    .line 151
    .line 152
    .line 153
    move-result v2

    .line 154
    invoke-virtual {p3}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectX2()I

    .line 155
    .line 156
    .line 157
    move-result v3

    .line 158
    invoke-virtual {p3}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectY2()I

    .line 159
    .line 160
    .line 161
    move-result p3

    .line 162
    invoke-virtual {v1, v0, v2, v3, p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->setCoordinates(IIII)V

    .line 163
    .line 164
    .line 165
    :cond_6
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 166
    .line 167
    .line 168
    move-result p3

    .line 169
    if-ge v5, p3, :cond_7

    .line 170
    .line 171
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 172
    .line 173
    .line 174
    move-result-object p3

    .line 175
    check-cast p3, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 176
    .line 177
    invoke-static {p0, p1, p3, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeFactory;->createShape(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Ljava/util/Map;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;)Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;

    .line 178
    .line 179
    .line 180
    move-result-object p3

    .line 181
    invoke-virtual {v1, p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->addChildShape(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;)V

    .line 182
    .line 183
    .line 184
    add-int/lit8 v5, v5, 0x1

    .line 185
    .line 186
    goto :goto_2

    .line 187
    :cond_7
    return-object v1
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public static createSimpeShape(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Ljava/util/Map;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;)Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/ss/model/XLSModel/AWorkbook;",
            "Ljava/util/Map<",
            "Lcom/intsig/office/fc/ddf/EscherRecord;",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;",
            "Lcom/intsig/office/fc/ddf/EscherContainerRecord;",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;",
            ")",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p3, :cond_1

    .line 3
    .line 4
    const/16 v1, -0xff0

    .line 5
    .line 6
    invoke-static {p2, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;

    .line 11
    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getCol2()S

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    const/16 v3, 0xff

    .line 19
    .line 20
    if-gt v2, v3, :cond_0

    .line 21
    .line 22
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getRow2()S

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    const v3, 0xffff

    .line 27
    .line 28
    .line 29
    if-gt v2, v3, :cond_0

    .line 30
    .line 31
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->toClientAnchor(Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;)Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    move-object v1, v0

    .line 37
    goto :goto_0

    .line 38
    :cond_1
    const/16 v1, -0xff1

    .line 39
    .line 40
    invoke-static {p2, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;

    .line 45
    .line 46
    if-eqz v1, :cond_2

    .line 47
    .line 48
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->toChildAnchor(Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;)Lcom/intsig/office/fc/hssf/usermodel/HSSFChildAnchor;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    :goto_0
    move-object v6, v1

    .line 53
    goto :goto_1

    .line 54
    :cond_2
    move-object v6, v0

    .line 55
    :goto_1
    const/16 v1, -0xff6

    .line 56
    .line 57
    invoke-virtual {p2, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 62
    .line 63
    if-nez v1, :cond_3

    .line 64
    .line 65
    return-object v0

    .line 66
    :cond_3
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->getOptions()S

    .line 67
    .line 68
    .line 69
    move-result v1

    .line 70
    shr-int/lit8 v7, v1, 0x4

    .line 71
    .line 72
    if-eqz v7, :cond_8

    .line 73
    .line 74
    const/16 v1, 0x14

    .line 75
    .line 76
    if-eq v7, v1, :cond_7

    .line 77
    .line 78
    const/16 v1, 0x26

    .line 79
    .line 80
    if-eq v7, v1, :cond_7

    .line 81
    .line 82
    const/16 v1, 0x4b

    .line 83
    .line 84
    if-eq v7, v1, :cond_6

    .line 85
    .line 86
    const/16 v1, 0x64

    .line 87
    .line 88
    if-eq v7, v1, :cond_8

    .line 89
    .line 90
    const/16 v1, 0xc9

    .line 91
    .line 92
    if-eq v7, v1, :cond_5

    .line 93
    .line 94
    const/16 v1, 0xca

    .line 95
    .line 96
    if-eq v7, v1, :cond_4

    .line 97
    .line 98
    packed-switch v7, :pswitch_data_0

    .line 99
    .line 100
    .line 101
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFAutoShape;

    .line 102
    .line 103
    move-object v2, v0

    .line 104
    move-object v3, p0

    .line 105
    move-object v4, p2

    .line 106
    move-object v5, p3

    .line 107
    invoke-direct/range {v2 .. v7}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAutoShape;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;I)V

    .line 108
    .line 109
    .line 110
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAutoShape;->setAdjustmentValue(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)V

    .line 111
    .line 112
    .line 113
    goto/16 :goto_2

    .line 114
    .line 115
    :cond_4
    if-eqz p1, :cond_6

    .line 116
    .line 117
    invoke-interface {p1}, Ljava/util/Map;->size()I

    .line 118
    .line 119
    .line 120
    move-result v1

    .line 121
    if-lez v1, :cond_6

    .line 122
    .line 123
    const/16 v1, -0xfef

    .line 124
    .line 125
    invoke-static {p2, v1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 126
    .line 127
    .line 128
    move-result-object v1

    .line 129
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 130
    .line 131
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    .line 133
    .line 134
    move-result-object p1

    .line 135
    check-cast p1, Lcom/intsig/office/fc/hssf/record/Record;

    .line 136
    .line 137
    instance-of v1, p1, Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 138
    .line 139
    if-eqz v1, :cond_9

    .line 140
    .line 141
    check-cast p1, Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 142
    .line 143
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    .line 144
    .line 145
    .line 146
    move-result-object v1

    .line 147
    const/4 v2, 0x0

    .line 148
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 149
    .line 150
    .line 151
    move-result-object v1

    .line 152
    instance-of v1, v1, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;

    .line 153
    .line 154
    if-eqz v1, :cond_9

    .line 155
    .line 156
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    .line 157
    .line 158
    .line 159
    move-result-object p1

    .line 160
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 161
    .line 162
    .line 163
    move-result-object p1

    .line 164
    check-cast p1, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;

    .line 165
    .line 166
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->getObjectType()S

    .line 167
    .line 168
    .line 169
    move-result p1

    .line 170
    const/16 v1, 0x19

    .line 171
    .line 172
    if-eq p1, v1, :cond_9

    .line 173
    .line 174
    new-instance p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFAutoShape;

    .line 175
    .line 176
    move-object v2, p1

    .line 177
    move-object v3, p0

    .line 178
    move-object v4, p2

    .line 179
    move-object v5, p3

    .line 180
    invoke-direct/range {v2 .. v7}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAutoShape;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;I)V

    .line 181
    .line 182
    .line 183
    move-object v0, p1

    .line 184
    goto :goto_2

    .line 185
    :cond_5
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;

    .line 186
    .line 187
    invoke-direct {v0, p0, p2, p3, v6}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)V

    .line 188
    .line 189
    .line 190
    goto :goto_2

    .line 191
    :cond_6
    const/16 p1, -0xff5

    .line 192
    .line 193
    invoke-static {p2, p1}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 194
    .line 195
    .line 196
    move-result-object p1

    .line 197
    move-object v7, p1

    .line 198
    check-cast v7, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 199
    .line 200
    const/16 p1, 0x104

    .line 201
    .line 202
    invoke-virtual {v7, p1}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->lookup(I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 203
    .line 204
    .line 205
    move-result-object p1

    .line 206
    check-cast p1, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 207
    .line 208
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;

    .line 209
    .line 210
    move-object v2, v0

    .line 211
    move-object v3, p0

    .line 212
    move-object v4, p2

    .line 213
    move-object v5, p3

    .line 214
    invoke-direct/range {v2 .. v7}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;Lcom/intsig/office/fc/ddf/EscherOptRecord;)V

    .line 215
    .line 216
    .line 217
    if-eqz p1, :cond_9

    .line 218
    .line 219
    invoke-virtual {p1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 220
    .line 221
    .line 222
    move-result p0

    .line 223
    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->setPictureIndex(I)V

    .line 224
    .line 225
    .line 226
    goto :goto_2

    .line 227
    :cond_7
    :pswitch_0
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFLine;

    .line 228
    .line 229
    move-object v2, v0

    .line 230
    move-object v3, p0

    .line 231
    move-object v4, p2

    .line 232
    move-object v5, p3

    .line 233
    invoke-direct/range {v2 .. v7}, Lcom/intsig/office/fc/hssf/usermodel/HSSFLine;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;I)V

    .line 234
    .line 235
    .line 236
    goto :goto_2

    .line 237
    :cond_8
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFreeform;

    .line 238
    .line 239
    move-object v2, v0

    .line 240
    move-object v3, p0

    .line 241
    move-object v4, p2

    .line 242
    move-object v5, p3

    .line 243
    invoke-direct/range {v2 .. v7}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFreeform;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;I)V

    .line 244
    .line 245
    .line 246
    :cond_9
    :goto_2
    return-object v0

    .line 247
    :pswitch_data_0
    .packed-switch 0x20
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private static isEmbeddedObject(Lcom/intsig/office/fc/hssf/record/ObjRecord;)Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    check-cast v0, Lcom/intsig/office/fc/hssf/record/SubRecord;

    .line 20
    .line 21
    instance-of v0, v0, Lcom/intsig/office/fc/hssf/record/EmbeddedObjectRefSubRecord;

    .line 22
    .line 23
    if-eqz v0, :cond_0

    .line 24
    .line 25
    const/4 p0, 0x1

    .line 26
    return p0

    .line 27
    :cond_1
    const/4 p0, 0x0

    .line 28
    return p0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

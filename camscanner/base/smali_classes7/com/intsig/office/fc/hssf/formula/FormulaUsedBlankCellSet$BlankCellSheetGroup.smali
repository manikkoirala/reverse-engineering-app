.class final Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;
.super Ljava/lang/Object;
.source "FormulaUsedBlankCellSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BlankCellSheetGroup"
.end annotation


# instance fields
.field private O8:I

.field private Oo08:Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellRectangleGroup;

.field private final 〇080:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellRectangleGroup;",
            ">;"
        }
    .end annotation
.end field

.field private 〇o00〇〇Oo:I

.field private 〇o〇:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇080:Ljava/util/List;

    .line 10
    .line 11
    const/4 v0, -0x1

    .line 12
    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇o00〇〇Oo:I

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public 〇080(II)V
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇o00〇〇Oo:I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇o00〇〇Oo:I

    .line 7
    .line 8
    iput p2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇o〇:I

    .line 9
    .line 10
    iput p2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->O8:I

    .line 11
    .line 12
    goto :goto_1

    .line 13
    :cond_0
    if-ne v0, p1, :cond_1

    .line 14
    .line 15
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->O8:I

    .line 16
    .line 17
    add-int/lit8 v1, v1, 0x1

    .line 18
    .line 19
    if-ne v1, p2, :cond_1

    .line 20
    .line 21
    iput p2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->O8:I

    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->Oo08:Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellRectangleGroup;

    .line 25
    .line 26
    if-nez v1, :cond_2

    .line 27
    .line 28
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellRectangleGroup;

    .line 29
    .line 30
    iget v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇o〇:I

    .line 31
    .line 32
    iget v3, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->O8:I

    .line 33
    .line 34
    invoke-direct {v1, v0, v2, v3}, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellRectangleGroup;-><init>(III)V

    .line 35
    .line 36
    .line 37
    iput-object v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->Oo08:Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellRectangleGroup;

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_2
    iget v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇o〇:I

    .line 41
    .line 42
    iget v3, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->O8:I

    .line 43
    .line 44
    invoke-virtual {v1, v0, v2, v3}, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellRectangleGroup;->〇080(III)Z

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    if-nez v0, :cond_3

    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇080:Ljava/util/List;

    .line 51
    .line 52
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->Oo08:Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellRectangleGroup;

    .line 53
    .line 54
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    .line 56
    .line 57
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellRectangleGroup;

    .line 58
    .line 59
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇o00〇〇Oo:I

    .line 60
    .line 61
    iget v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇o〇:I

    .line 62
    .line 63
    iget v3, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->O8:I

    .line 64
    .line 65
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellRectangleGroup;-><init>(III)V

    .line 66
    .line 67
    .line 68
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->Oo08:Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellRectangleGroup;

    .line 69
    .line 70
    :cond_3
    :goto_0
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇o00〇〇Oo:I

    .line 71
    .line 72
    iput p2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇o〇:I

    .line 73
    .line 74
    iput p2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->O8:I

    .line 75
    .line 76
    :goto_1
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public 〇o00〇〇Oo(II)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇080:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    sub-int/2addr v0, v1

    .line 9
    :goto_0
    if-ltz v0, :cond_1

    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇080:Ljava/util/List;

    .line 12
    .line 13
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    check-cast v2, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellRectangleGroup;

    .line 18
    .line 19
    invoke-virtual {v2, p1, p2}, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellRectangleGroup;->〇o00〇〇Oo(II)Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz v2, :cond_0

    .line 24
    .line 25
    return v1

    .line 26
    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->Oo08:Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellRectangleGroup;

    .line 30
    .line 31
    if-eqz v0, :cond_2

    .line 32
    .line 33
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellRectangleGroup;->〇o00〇〇Oo(II)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_2

    .line 38
    .line 39
    return v1

    .line 40
    :cond_2
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇o00〇〇Oo:I

    .line 41
    .line 42
    const/4 v2, -0x1

    .line 43
    if-eq v0, v2, :cond_3

    .line 44
    .line 45
    if-ne v0, p1, :cond_3

    .line 46
    .line 47
    iget p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->〇o〇:I

    .line 48
    .line 49
    if-gt p1, p2, :cond_3

    .line 50
    .line 51
    iget p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BlankCellSheetGroup;->O8:I

    .line 52
    .line 53
    if-gt p2, p1, :cond_3

    .line 54
    .line 55
    return v1

    .line 56
    :cond_3
    const/4 p1, 0x0

    .line 57
    return p1
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

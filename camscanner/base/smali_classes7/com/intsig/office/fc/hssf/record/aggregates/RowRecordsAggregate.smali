.class public final Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;
.super Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;
.source "RowRecordsAggregate.java"


# instance fields
.field private _firstrow:I

.field private _lastrow:I

.field private _rowRecordValues:[Lcom/intsig/office/fc/hssf/record/RowRecord;

.field private final _rowRecords:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/office/fc/hssf/record/RowRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final _sharedValueManager:Lcom/intsig/office/fc/hssf/record/aggregates/SharedValueManager;

.field private final _unknownRecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;"
        }
    .end annotation
.end field

.field private final _valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hssf/record/aggregates/SharedValueManager;->createEmpty()Lcom/intsig/office/fc/hssf/record/aggregates/SharedValueManager;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;-><init>(Lcom/intsig/office/fc/hssf/record/aggregates/SharedValueManager;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/model/RecordStream;Lcom/intsig/office/fc/hssf/record/aggregates/SharedValueManager;)V
    .locals 3

    .line 11
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;-><init>(Lcom/intsig/office/fc/hssf/record/aggregates/SharedValueManager;)V

    .line 12
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->getNext()Lcom/intsig/office/fc/hssf/record/Record;

    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    move-result v1

    const/16 v2, 0xd7

    if-eq v1, v2, :cond_0

    const/16 v2, 0x208

    if-eq v1, v2, :cond_4

    .line 15
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/record/UnknownRecord;

    if-eqz v1, :cond_1

    .line 16
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->addUnknownRecord(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 17
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->peekNextSid()I

    move-result v0

    const/16 v1, 0x3c

    if-ne v0, v1, :cond_0

    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->getNext()Lcom/intsig/office/fc/hssf/record/Record;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->addUnknownRecord(Lcom/intsig/office/fc/hssf/record/Record;)V

    goto :goto_1

    .line 19
    :cond_1
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/record/MulBlankRecord;

    if-eqz v1, :cond_2

    .line 20
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    check-cast v0, Lcom/intsig/office/fc/hssf/record/MulBlankRecord;

    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->addMultipleBlanks(Lcom/intsig/office/fc/hssf/record/MulBlankRecord;)V

    goto :goto_0

    .line 21
    :cond_2
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    if-eqz v1, :cond_3

    .line 22
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    check-cast v0, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    invoke-virtual {v1, v0, p1, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->construct(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;Lcom/intsig/office/fc/hssf/model/RecordStream;Lcom/intsig/office/fc/hssf/record/aggregates/SharedValueManager;)V

    goto :goto_0

    .line 23
    :cond_3
    new-instance p1, Ljava/lang/RuntimeException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected record type ("

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 24
    :cond_4
    check-cast v0, Lcom/intsig/office/fc/hssf/record/RowRecord;

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->insertRow(Lcom/intsig/office/fc/hssf/record/RowRecord;)V

    goto :goto_0

    :cond_5
    return-void
.end method

.method private constructor <init>(Lcom/intsig/office/fc/hssf/record/aggregates/SharedValueManager;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;-><init>()V

    const/4 v0, -0x1

    .line 3
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_firstrow:I

    .line 4
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_lastrow:I

    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lcom/intsig/office/fc/hssf/record/RowRecord;

    if-eqz p1, :cond_0

    .line 6
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 7
    new-instance v0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_unknownRecords:Ljava/util/List;

    .line 9
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_sharedValueManager:Lcom/intsig/office/fc/hssf/record/aggregates/SharedValueManager;

    return-void

    .line 10
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "SharedValueManager must be provided."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private addUnknownRecord(Lcom/intsig/office/fc/hssf/record/Record;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_unknownRecords:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static createRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/record/RowRecord;-><init>(I)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private getEndRowNumberForBlock(I)I
    .locals 3

    .line 1
    add-int/lit8 v0, p1, 0x1

    .line 2
    .line 3
    mul-int/lit8 v0, v0, 0x20

    .line 4
    .line 5
    add-int/lit8 v0, v0, -0x1

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 8
    .line 9
    invoke-interface {v1}, Ljava/util/Map;->size()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-lt v0, v1, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    add-int/lit8 v0, v0, -0x1

    .line 22
    .line 23
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 24
    .line 25
    if-nez v1, :cond_1

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 28
    .line 29
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 34
    .line 35
    invoke-interface {v2}, Ljava/util/Map;->size()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    new-array v2, v2, [Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 40
    .line 41
    invoke-interface {v1, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    check-cast v1, [Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 46
    .line 47
    iput-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 48
    .line 49
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 50
    .line 51
    aget-object v0, v1, v0

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 54
    .line 55
    .line 56
    move-result p1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    return p1

    .line 58
    :catch_0
    new-instance v0, Ljava/lang/RuntimeException;

    .line 59
    .line 60
    new-instance v1, Ljava/lang/StringBuilder;

    .line 61
    .line 62
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 63
    .line 64
    .line 65
    const-string v2, "Did not find end row for block "

    .line 66
    .line 67
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    throw v0
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private getRowBlockSize(I)I
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRowCountForBlock(I)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    mul-int/lit8 p1, p1, 0x14

    .line 6
    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private getStartRowNumberForBlock(I)I
    .locals 3

    .line 1
    mul-int/lit8 v0, p1, 0x20

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 4
    .line 5
    if-nez v1, :cond_0

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 8
    .line 9
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 14
    .line 15
    invoke-interface {v2}, Ljava/util/Map;->size()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    new-array v2, v2, [Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 20
    .line 21
    invoke-interface {v1, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    check-cast v1, [Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 26
    .line 27
    iput-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 28
    .line 29
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 30
    .line 31
    aget-object v0, v1, v0

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 34
    .line 35
    .line 36
    move-result p1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    return p1

    .line 38
    :catch_0
    new-instance v0, Ljava/lang/RuntimeException;

    .line 39
    .line 40
    new-instance v1, Ljava/lang/StringBuilder;

    .line 41
    .line 42
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    .line 44
    .line 45
    const-string v2, "Did not find start row for block "

    .line 46
    .line 47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    throw v0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private visitRowRecordsForBlock(ILcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;)I
    .locals 5

    .line 1
    mul-int/lit8 p1, p1, 0x20

    .line 2
    .line 3
    add-int/lit8 v0, p1, 0x20

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 6
    .line 7
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    const/4 v2, 0x0

    .line 16
    const/4 v3, 0x0

    .line 17
    :goto_0
    if-ge v3, p1, :cond_0

    .line 18
    .line 19
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    add-int/lit8 v3, v3, 0x1

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    if-eqz p1, :cond_1

    .line 30
    .line 31
    add-int/lit8 p1, v3, 0x1

    .line 32
    .line 33
    if-ge v3, v0, :cond_1

    .line 34
    .line 35
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v3

    .line 39
    check-cast v3, Lcom/intsig/office/fc/hssf/record/Record;

    .line 40
    .line 41
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/RecordBase;->getRecordSize()I

    .line 42
    .line 43
    .line 44
    move-result v4

    .line 45
    add-int/2addr v2, v4

    .line 46
    invoke-interface {p2, v3}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 47
    .line 48
    .line 49
    move v3, p1

    .line 50
    goto :goto_1

    .line 51
    :cond_1
    return v2
    .line 52
    .line 53
.end method

.method private writeHidden(Lcom/intsig/office/fc/hssf/record/RowRecord;I)I
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getOutlineLevel()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    :goto_0
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getOutlineLevel()S

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-lt v1, v0, :cond_0

    .line 16
    .line 17
    const/4 v1, 0x1

    .line 18
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->setZeroHeight(Z)V

    .line 19
    .line 20
    .line 21
    add-int/lit8 p2, p2, 0x1

    .line 22
    .line 23
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    goto :goto_0

    .line 28
    :cond_0
    return p2
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public collapseRow(I)V
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->findStartOfRowOutlineGroup(I)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->writeHidden(Lcom/intsig/office/fc/hssf/record/RowRecord;I)I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->createRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->insertRow(Lcom/intsig/office/fc/hssf/record/RowRecord;)V

    .line 24
    .line 25
    .line 26
    :cond_0
    const/4 p1, 0x1

    .line 27
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->setColapsed(Z)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public createDimensions()Lcom/intsig/office/fc/hssf/record/DimensionsRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_firstrow:I

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setFirstRow(I)V

    .line 9
    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_lastrow:I

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setLastRow(I)V

    .line 14
    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->getFirstCellNum()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    int-to-short v1, v1

    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setFirstCol(S)V

    .line 24
    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    .line 27
    .line 28
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->getLastCellNum()I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    int-to-short v1, v1

    .line 33
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setLastCol(S)V

    .line 34
    .line 35
    .line 36
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public createFormula(II)Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setRow(I)V

    .line 7
    .line 8
    .line 9
    int-to-short p1, p2

    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setColumn(S)V

    .line 11
    .line 12
    .line 13
    new-instance p1, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 14
    .line 15
    const/4 p2, 0x0

    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_sharedValueManager:Lcom/intsig/office/fc/hssf/record/aggregates/SharedValueManager;

    .line 17
    .line 18
    invoke-direct {p1, v0, p2, v1}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;-><init>(Lcom/intsig/office/fc/hssf/record/FormulaRecord;Lcom/intsig/office/fc/hssf/record/StringRecord;Lcom/intsig/office/fc/hssf/record/aggregates/SharedValueManager;)V

    .line 19
    .line 20
    .line 21
    return-object p1
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public createIndexRecord(II)Lcom/intsig/office/fc/hssf/record/IndexRecord;
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/IndexRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/IndexRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_firstrow:I

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/IndexRecord;->setFirstRow(I)V

    .line 9
    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_lastrow:I

    .line 12
    .line 13
    add-int/lit8 v1, v1, 0x1

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/IndexRecord;->setLastRowAdd1(I)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRowBlockCount()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/record/IndexRecord;->getRecordSizeForBlockCount(I)I

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    add-int/2addr p1, v2

    .line 27
    add-int/2addr p1, p2

    .line 28
    const/4 p2, 0x0

    .line 29
    :goto_0
    if-ge p2, v1, :cond_0

    .line 30
    .line 31
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRowBlockSize(I)I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    add-int/2addr p1, v2

    .line 36
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    .line 37
    .line 38
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getStartRowNumberForBlock(I)I

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getEndRowNumberForBlock(I)I

    .line 43
    .line 44
    .line 45
    move-result v4

    .line 46
    invoke-virtual {v2, v3, v4}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->getRowCellBlockSize(II)I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    add-int/2addr p1, v2

    .line 51
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/IndexRecord;->addDbcell(I)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRowCountForBlock(I)I

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    mul-int/lit8 v2, v2, 0x2

    .line 59
    .line 60
    add-int/lit8 v2, v2, 0x8

    .line 61
    .line 62
    add-int/2addr p1, v2

    .line 63
    add-int/lit8 p2, p2, 0x1

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_0
    return-object v0
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public dispose()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->dispose()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_unknownRecords:Ljava/util/List;

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
.end method

.method public expandRow(I)V
    .locals 6

    .line 1
    const/4 v0, -0x1

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    return-void

    .line 5
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->isRowGroupCollapsed(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_1

    .line 10
    .line 11
    return-void

    .line 12
    :cond_1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->findStartOfRowOutlineGroup(I)I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->findEndOfRowOutlineGroup(I)I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->isRowGroupHiddenByParent(I)Z

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    const/4 v3, 0x0

    .line 29
    if-nez p1, :cond_4

    .line 30
    .line 31
    :goto_0
    if-gt v0, v2, :cond_4

    .line 32
    .line 33
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getOutlineLevel()S

    .line 38
    .line 39
    .line 40
    move-result v4

    .line 41
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getOutlineLevel()S

    .line 42
    .line 43
    .line 44
    move-result v5

    .line 45
    if-eq v4, v5, :cond_2

    .line 46
    .line 47
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->isRowGroupCollapsed(I)Z

    .line 48
    .line 49
    .line 50
    move-result v4

    .line 51
    if-nez v4, :cond_3

    .line 52
    .line 53
    :cond_2
    invoke-virtual {p1, v3}, Lcom/intsig/office/fc/hssf/record/RowRecord;->setZeroHeight(Z)V

    .line 54
    .line 55
    .line 56
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 60
    .line 61
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    invoke-virtual {p1, v3}, Lcom/intsig/office/fc/hssf/record/RowRecord;->setColapsed(Z)V

    .line 66
    .line 67
    .line 68
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public findEndOfRowOutlineGroup(I)I
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getOutlineLevel()S

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getLastRowNum()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-ge p1, v1, :cond_1

    .line 14
    .line 15
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getOutlineLevel()S

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    if-ge v1, v0, :cond_0

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_0
    add-int/lit8 p1, p1, 0x1

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    :goto_1
    add-int/lit8 p1, p1, -0x1

    .line 36
    .line 37
    return p1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public findStartOfRowOutlineGroup(I)I
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getOutlineLevel()S

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    :goto_0
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    if-eqz v1, :cond_1

    .line 14
    .line 15
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getOutlineLevel()S

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    if-ge v1, v0, :cond_0

    .line 24
    .line 25
    add-int/lit8 p1, p1, 0x1

    .line 26
    .line 27
    return p1

    .line 28
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    add-int/lit8 p1, p1, 0x1

    .line 32
    .line 33
    return p1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getCellValueIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFirstRowNum()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_firstrow:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/intsig/office/fc/hssf/record/RowRecord;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLastRowNum()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_lastrow:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPhysicalNumberOfRows()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->EXCEL97:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->getLastRowIndex()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-ltz p1, :cond_0

    .line 8
    .line 9
    if-gt p1, v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 12
    .line 13
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    check-cast p1, Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 22
    .line 23
    return-object p1

    .line 24
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 25
    .line 26
    new-instance v1, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v2, "The row number must be between 0 and "

    .line 32
    .line 33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    throw p1
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getRowBlockCount()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    div-int/lit8 v0, v0, 0x20

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 10
    .line 11
    invoke-interface {v1}, Ljava/util/Map;->size()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    rem-int/lit8 v1, v1, 0x20

    .line 16
    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    add-int/lit8 v0, v0, 0x1

    .line 20
    .line 21
    :cond_0
    return v0
.end method

.method public getRowCountForBlock(I)I
    .locals 2

    .line 1
    mul-int/lit8 p1, p1, 0x20

    .line 2
    .line 3
    add-int/lit8 v0, p1, 0x20

    .line 4
    .line 5
    add-int/lit8 v0, v0, -0x1

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 8
    .line 9
    invoke-interface {v1}, Ljava/util/Map;->size()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-lt v0, v1, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    add-int/lit8 v0, v0, -0x1

    .line 22
    .line 23
    :cond_0
    sub-int/2addr v0, p1

    .line 24
    add-int/lit8 v0, v0, 0x1

    .line 25
    .line 26
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getValueRecords()[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->getValueRecords()[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public insertCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->insertCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public insertRow(Lcom/intsig/office/fc/hssf/record/RowRecord;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_firstrow:I

    .line 22
    .line 23
    const/4 v2, -0x1

    .line 24
    if-lt v0, v1, :cond_0

    .line 25
    .line 26
    if-ne v1, v2, :cond_1

    .line 27
    .line 28
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_firstrow:I

    .line 33
    .line 34
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_lastrow:I

    .line 39
    .line 40
    if-gt v0, v1, :cond_2

    .line 41
    .line 42
    if-ne v1, v2, :cond_3

    .line 43
    .line 44
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_lastrow:I

    .line 49
    .line 50
    :cond_3
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public isRowGroupCollapsed(I)Z
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->findEndOfRowOutlineGroup(I)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    add-int/lit8 p1, p1, 0x1

    .line 6
    .line 7
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    const/4 p1, 0x0

    .line 14
    return p1

    .line 15
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getColapsed()Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    return p1
    .line 24
.end method

.method public isRowGroupHiddenByParent(I)Z
    .locals 4

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->findEndOfRowOutlineGroup(I)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    add-int/lit8 v0, v0, 0x1

    .line 6
    .line 7
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const/4 v2, 0x0

    .line 12
    if-nez v1, :cond_0

    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    const/4 v1, 0x0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getOutlineLevel()S

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getZeroHeight()Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    :goto_0
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->findStartOfRowOutlineGroup(I)I

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    add-int/lit8 p1, p1, -0x1

    .line 38
    .line 39
    if-ltz p1, :cond_2

    .line 40
    .line 41
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    if-nez v3, :cond_1

    .line 46
    .line 47
    goto :goto_1

    .line 48
    :cond_1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getOutlineLevel()S

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getZeroHeight()Z

    .line 61
    .line 62
    .line 63
    move-result p1

    .line 64
    goto :goto_2

    .line 65
    :cond_2
    :goto_1
    const/4 p1, 0x0

    .line 66
    :goto_2
    if-le v1, v2, :cond_3

    .line 67
    .line 68
    return v0

    .line 69
    :cond_3
    return p1
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public removeCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    move-object v0, p1

    .line 6
    check-cast v0, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->notifyFormulaChanging()V

    .line 9
    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->removeCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public removeRow(Lcom/intsig/office/fc/hssf/record/RowRecord;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    .line 6
    .line 7
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->removeAllCellsValuesForRow(I)V

    .line 8
    .line 9
    .line 10
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 15
    .line 16
    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    check-cast v1, Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 21
    .line 22
    if-eqz v1, :cond_1

    .line 23
    .line 24
    if-ne p1, v1, :cond_0

    .line 25
    .line 26
    const/4 p1, 0x0

    .line 27
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 28
    .line 29
    return-void

    .line 30
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 31
    .line 32
    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    new-instance p1, Ljava/lang/RuntimeException;

    .line 36
    .line 37
    const-string v0, "Attempt to remove row that does not belong to this sheet"

    .line 38
    .line 39
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    throw p1

    .line 43
    :cond_1
    new-instance p1, Ljava/lang/RuntimeException;

    .line 44
    .line 45
    new-instance v1, Ljava/lang/StringBuilder;

    .line 46
    .line 47
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 48
    .line 49
    .line 50
    const-string v2, "Invalid row index ("

    .line 51
    .line 52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    const-string v0, ")"

    .line 63
    .line 64
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    throw p1
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public updateFormulasAfterRowShift(Lcom/intsig/office/fc/hssf/formula/FormulaShifter;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->updateFormulasAfterRowShift(Lcom/intsig/office/fc/hssf/formula/FormulaShifter;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public visitContainedRecords(Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V
    .locals 10

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p1, v1}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;-><init>(Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;I)V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getRowBlockCount()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    const/4 v3, 0x0

    .line 12
    :goto_0
    if-ge v3, v2, :cond_2

    .line 13
    .line 14
    invoke-direct {p0, v3, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->visitRowRecordsForBlock(ILcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;)I

    .line 15
    .line 16
    .line 17
    move-result v4

    .line 18
    add-int/lit8 v5, v4, 0x0

    .line 19
    .line 20
    invoke-direct {p0, v3}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getStartRowNumberForBlock(I)I

    .line 21
    .line 22
    .line 23
    move-result v6

    .line 24
    invoke-direct {p0, v3}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->getEndRowNumberForBlock(I)I

    .line 25
    .line 26
    .line 27
    move-result v7

    .line 28
    new-instance v8, Lcom/intsig/office/fc/hssf/record/DBCellRecord$Builder;

    .line 29
    .line 30
    invoke-direct {v8}, Lcom/intsig/office/fc/hssf/record/DBCellRecord$Builder;-><init>()V

    .line 31
    .line 32
    .line 33
    add-int/lit8 v4, v4, -0x14

    .line 34
    .line 35
    :goto_1
    if-gt v6, v7, :cond_1

    .line 36
    .line 37
    iget-object v9, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    .line 38
    .line 39
    invoke-virtual {v9, v6}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->rowHasCells(I)Z

    .line 40
    .line 41
    .line 42
    move-result v9

    .line 43
    if-eqz v9, :cond_0

    .line 44
    .line 45
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;->setPosition(I)V

    .line 46
    .line 47
    .line 48
    iget-object v9, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;

    .line 49
    .line 50
    invoke-virtual {v9, v6, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->visitCellsForRow(ILcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;->getPosition()I

    .line 54
    .line 55
    .line 56
    move-result v9

    .line 57
    add-int/2addr v5, v9

    .line 58
    invoke-virtual {v8, v4}, Lcom/intsig/office/fc/hssf/record/DBCellRecord$Builder;->addCellOffset(I)V

    .line 59
    .line 60
    .line 61
    move v4, v9

    .line 62
    :cond_0
    add-int/lit8 v6, v6, 0x1

    .line 63
    .line 64
    goto :goto_1

    .line 65
    :cond_1
    invoke-virtual {v8, v5}, Lcom/intsig/office/fc/hssf/record/DBCellRecord$Builder;->build(I)Lcom/intsig/office/fc/hssf/record/DBCellRecord;

    .line 66
    .line 67
    .line 68
    move-result-object v4

    .line 69
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 70
    .line 71
    .line 72
    add-int/lit8 v3, v3, 0x1

    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_unknownRecords:Ljava/util/List;

    .line 76
    .line 77
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    if-ge v1, v0, :cond_3

    .line 82
    .line 83
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->_unknownRecords:Ljava/util/List;

    .line 84
    .line 85
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    check-cast v0, Lcom/intsig/office/fc/hssf/record/Record;

    .line 90
    .line 91
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 92
    .line 93
    .line 94
    add-int/lit8 v1, v1, 0x1

    .line 95
    .line 96
    goto :goto_2

    .line 97
    :cond_3
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

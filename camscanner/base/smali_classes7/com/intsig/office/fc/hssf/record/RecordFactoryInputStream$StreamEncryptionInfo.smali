.class final Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;
.super Ljava/lang/Object;
.source "RecordFactoryInputStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "StreamEncryptionInfo"
.end annotation


# instance fields
.field private final O8:Z

.field private final 〇080:I

.field private final 〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/record/FilePassRecord;

.field private final 〇o〇:Lcom/intsig/office/fc/hssf/record/Record;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/hssf/record/RecordInputStream;",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->nextRecord()V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->remaining()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    add-int/lit8 v0, v0, 0x4

    .line 12
    .line 13
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/record/RecordFactory;->createSingleRecord(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)Lcom/intsig/office/fc/hssf/record/Record;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    instance-of v2, v1, Lcom/intsig/office/fc/hssf/record/BOFRecord;

    .line 21
    .line 22
    const/4 v3, 0x0

    .line 23
    const/4 v4, 0x0

    .line 24
    if-eqz v2, :cond_2

    .line 25
    .line 26
    const/4 v2, 0x1

    .line 27
    iput-boolean v2, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->O8:Z

    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->hasNextRecord()Z

    .line 30
    .line 31
    .line 32
    move-result v5

    .line 33
    if-eqz v5, :cond_3

    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->nextRecord()V

    .line 36
    .line 37
    .line 38
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/record/RecordFactory;->createSingleRecord(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)Lcom/intsig/office/fc/hssf/record/Record;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/RecordBase;->getRecordSize()I

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    add-int/2addr v0, p1

    .line 47
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    .line 49
    .line 50
    instance-of p1, v1, Lcom/intsig/office/fc/hssf/record/FilePassRecord;

    .line 51
    .line 52
    if-eqz p1, :cond_0

    .line 53
    .line 54
    move-object v4, v1

    .line 55
    check-cast v4, Lcom/intsig/office/fc/hssf/record/FilePassRecord;

    .line 56
    .line 57
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 58
    .line 59
    .line 60
    move-result p1

    .line 61
    sub-int/2addr p1, v2

    .line 62
    invoke-interface {p2, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    move-object v1, p1

    .line 70
    check-cast v1, Lcom/intsig/office/fc/hssf/record/Record;

    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_0
    instance-of p1, v1, Lcom/intsig/office/fc/hssf/record/EOFRecord;

    .line 74
    .line 75
    if-nez p1, :cond_1

    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 79
    .line 80
    const-string p2, "Nothing between BOF and EOF"

    .line 81
    .line 82
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    throw p1

    .line 86
    :cond_2
    iput-boolean v3, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->O8:Z

    .line 87
    .line 88
    :cond_3
    :goto_0
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->〇080:I

    .line 89
    .line 90
    iput-object v4, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/record/FilePassRecord;

    .line 91
    .line 92
    iput-object v1, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->〇o〇:Lcom/intsig/office/fc/hssf/record/Record;

    .line 93
    .line 94
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method


# virtual methods
.method public O8()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/record/FilePassRecord;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080(Ljava/io/InputStream;)Lcom/intsig/office/fc/hssf/record/RecordInputStream;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/record/FilePassRecord;

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8EncryptionKey;->getCurrentUserPassword()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    if-nez v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FilePassRecord;->getDocId()[B

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8EncryptionKey;->create([B)Lcom/intsig/office/fc/hssf/record/crypto/Biff8EncryptionKey;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FilePassRecord;->getDocId()[B

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    invoke-static {v1, v2}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8EncryptionKey;->create(Ljava/lang/String;[B)Lcom/intsig/office/fc/hssf/record/crypto/Biff8EncryptionKey;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    :goto_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FilePassRecord;->getSaltData()[B

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FilePassRecord;->getSaltHash()[B

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-virtual {v1, v2, v0}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8EncryptionKey;->validate([B[B)Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    if-eqz v0, :cond_1

    .line 39
    .line 40
    new-instance v0, Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 41
    .line 42
    iget v2, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->〇080:I

    .line 43
    .line 44
    invoke-direct {v0, p1, v1, v2}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;-><init>(Ljava/io/InputStream;Lcom/intsig/office/fc/hssf/record/crypto/Biff8EncryptionKey;I)V

    .line 45
    .line 46
    .line 47
    return-object v0

    .line 48
    :cond_1
    new-instance p1, Lcom/intsig/office/fc/EncryptedDocumentException;

    .line 49
    .line 50
    const-string v0, "Cannot process encrypted office files!"

    .line 51
    .line 52
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    throw p1
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇o00〇〇Oo()Lcom/intsig/office/fc/hssf/record/Record;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->〇o〇:Lcom/intsig/office/fc/hssf/record/Record;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->O8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

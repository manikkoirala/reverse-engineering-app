.class public final Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;
.super Ljava/lang/Object;
.source "FunctionMetadataRegistry.java"


# static fields
.field public static final FUNCTION_INDEX_CHOOSE:I = 0x64

.field public static final FUNCTION_INDEX_EXTERNAL:S = 0xffs

.field public static final FUNCTION_INDEX_IF:I = 0x1

.field public static final FUNCTION_INDEX_INDIRECT:S = 0x94s

.field public static final FUNCTION_INDEX_SUM:S = 0x4s

.field public static final FUNCTION_NAME_IF:Ljava/lang/String; = "IF"

.field private static _instance:Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;


# instance fields
.field private final _functionDataByIndex:[Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;

.field private final _functionDataByName:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>([Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->_functionDataByIndex:[Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->_functionDataByName:Ljava/util/Map;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static getFunctionByIndex(I)Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->getInstance()Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->getFunctionByIndexInternal(I)Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private getFunctionByIndexInternal(I)Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->_functionDataByIndex:[Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;

    .line 2
    .line 3
    aget-object p1, v0, p1

    .line 4
    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static getFunctionByName(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->getInstance()Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->getFunctionByNameInternal(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private getFunctionByNameInternal(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->_functionDataByName:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;

    .line 8
    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static getInstance()Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->_instance:Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataReader;->createRegistry()Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->_instance:Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;

    .line 10
    .line 11
    :cond_0
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->_instance:Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;

    .line 12
    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static lookupIndexByName(Ljava/lang/String;)S
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->getInstance()Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->getFunctionByNameInternal(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    if-nez p0, :cond_0

    .line 10
    .line 11
    const/4 p0, -0x1

    .line 12
    return p0

    .line 13
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->getIndex()I

    .line 14
    .line 15
    .line 16
    move-result p0

    .line 17
    int-to-short p0, p0

    .line 18
    return p0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method getAllFunctionNames()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->_functionDataByName:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

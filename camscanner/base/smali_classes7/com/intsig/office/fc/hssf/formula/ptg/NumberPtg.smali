.class public final Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;
.super Lcom/intsig/office/fc/hssf/formula/ptg/ScalarConstantPtg;
.source "NumberPtg.java"


# static fields
.field public static final SIZE:I = 0x9

.field public static final sid:B = 0x1ft


# instance fields
.field private final field_1_value:D


# direct methods
.method public constructor <init>(D)V
    .locals 0

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/ScalarConstantPtg;-><init>()V

    .line 4
    iput-wide p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;->field_1_value:D

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V
    .locals 2

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readDouble()D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;-><init>(D)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .line 2
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;-><init>(D)V

    return-void
.end method


# virtual methods
.method public getSize()I
    .locals 1

    .line 1
    const/16 v0, 0x9

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getValue()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;->field_1_value:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public toFormulaString()Ljava/lang/String;
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;->field_1_value:D

    .line 2
    .line 3
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ss/util/NumberToTextConverter;->toText(D)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public write(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getPtgClass()B

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    add-int/lit8 v0, v0, 0x1f

    .line 6
    .line 7
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeByte(I)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;->getValue()D

    .line 11
    .line 12
    .line 13
    move-result-wide v0

    .line 14
    invoke-interface {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeDouble(D)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

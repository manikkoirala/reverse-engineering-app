.class public final Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;
.super Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;
.source "HSSFPicture.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/Picture;


# static fields
.field public static final PICTURE_TYPE_DIB:I = 0x7

.field public static final PICTURE_TYPE_EMF:I = 0x2

.field public static final PICTURE_TYPE_JPEG:I = 0x5

.field public static final PICTURE_TYPE_PICT:I = 0x4

.field public static final PICTURE_TYPE_PNG:I = 0x6

.field public static final PICTURE_TYPE_WMF:I = 0x3

.field private static final PX_DEFAULT:F = 32.0f

.field private static final PX_MODIFIED:F = 36.56f

.field private static final PX_ROW:I = 0xf


# instance fields
.field private _pictureIndex:I

.field private opt:Lcom/intsig/office/fc/ddf/EscherOptRecord;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)V

    const/16 p1, 0x8

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setShapeType(I)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;Lcom/intsig/office/fc/ddf/EscherOptRecord;)V
    .locals 0

    .line 3
    invoke-direct {p0, p2, p3, p4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)V

    const/16 p3, 0x8

    .line 4
    invoke-virtual {p0, p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setShapeType(I)V

    .line 5
    iput-object p5, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->opt:Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->processLineWidth()V

    .line 7
    invoke-virtual {p0, p2, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;->processLine(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V

    .line 8
    invoke-virtual {p0, p2, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->processSimpleBackground(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V

    .line 9
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;->processRotationAndFlip(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)V

    return-void
.end method

.method private getColumnWidthInPixels(I)F
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->checkPatriarch()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 8
    .line 9
    iget-object v0, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;->_sheet:Lcom/intsig/office/ss/model/XLSModel/ASheet;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnPixelWidth(I)F

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    return p1

    .line 16
    :cond_0
    const/4 p1, 0x0

    .line 17
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private getPixelWidth(I)F
    .locals 0

    .line 1
    const/high16 p1, 0x42000000    # 32.0f

    .line 2
    .line 3
    return p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private getRowHeightInPixels(I)F
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->checkPatriarch()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 8
    .line 9
    iget-object v0, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;->_sheet:Lcom/intsig/office/ss/model/XLSModel/ASheet;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowPixelHeight()F

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    return p1

    .line 22
    :cond_0
    const/high16 p1, 0x41900000    # 18.0f

    .line 23
    .line 24
    return p1
.end method


# virtual methods
.method public getEscherOptRecord()Lcom/intsig/office/fc/ddf/EscherOptRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->opt:Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getImageDimension()Lcom/intsig/office/java/awt/Dimension;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->checkPatriarch()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 8
    .line 9
    iget-object v0, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;->_sheet:Lcom/intsig/office/ss/model/XLSModel/ASheet;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->getAWorkbook()Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getInternalWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iget v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->_pictureIndex:I

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getBSERecord(I)Lcom/intsig/office/fc/ddf/EscherBSERecord;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->getBlipRecord()Lcom/intsig/office/fc/ddf/EscherBlipRecord;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherBlipRecord;->getPicturedata()[B

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->getBlipTypeWin32()B

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    new-instance v2, Ljava/io/ByteArrayInputStream;

    .line 38
    .line 39
    invoke-direct {v2, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 40
    .line 41
    .line 42
    invoke-static {v2, v0}, Lcom/intsig/office/fc/ss/util/ImageUtils;->getImageDimension(Ljava/io/InputStream;I)Lcom/intsig/office/java/awt/Dimension;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    return-object v0

    .line 47
    :cond_0
    const/4 v0, 0x0

    .line 48
    return-object v0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getPictureData()Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;
    .locals 2

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->checkPatriarch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->_pictureIndex:I

    if-lez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    iget-object v0, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;->_sheet:Lcom/intsig/office/ss/model/XLSModel/ASheet;

    invoke-virtual {v0}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->getAWorkbook()Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getInternalWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    move-result-object v0

    .line 4
    iget v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->_pictureIndex:I

    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getBSERecord(I)Lcom/intsig/office/fc/ddf/EscherBSERecord;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->getBlipRecord()Lcom/intsig/office/fc/ddf/EscherBlipRecord;

    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;

    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;-><init>(Lcom/intsig/office/fc/ddf/EscherBlipRecord;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic getPictureData()Lcom/intsig/office/fc/ss/usermodel/PictureData;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->getPictureData()Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;

    move-result-object v0

    return-object v0
.end method

.method public getPictureIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->_pictureIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPreferredSize()Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;
    .locals 2

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 2
    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->getPreferredSize(D)Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;

    move-result-object v0

    return-object v0
.end method

.method public getPreferredSize(D)Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;
    .locals 11

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getAnchor()Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;

    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->getImageDimension()Lcom/intsig/office/java/awt/Dimension;

    move-result-object v1

    .line 5
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Dimension;->getWidth()D

    move-result-wide v2

    mul-double v2, v2, p1

    .line 6
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Dimension;->getHeight()D

    move-result-wide v4

    mul-double v4, v4, p1

    .line 7
    iget-short p1, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->col1:S

    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->getColumnWidthInPixels(I)F

    move-result p1

    iget p2, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->dx1:I

    int-to-float p2, p2

    const/high16 v1, 0x44800000    # 1024.0f

    div-float/2addr p2, v1

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float p2, v1, p2

    mul-float p1, p1, p2

    const/4 p2, 0x0

    add-float/2addr p1, p2

    .line 8
    iget-short v6, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->col1:S

    add-int/lit8 v6, v6, 0x1

    int-to-short v6, v6

    :goto_0
    float-to-double v7, p1

    cmpg-double v9, v7, v2

    if-gez v9, :cond_0

    add-int/lit8 v7, v6, 0x1

    int-to-short v7, v7

    .line 9
    invoke-direct {p0, v6}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->getColumnWidthInPixels(I)F

    move-result v6

    add-float/2addr p1, v6

    move v6, v7

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    cmpl-double v9, v7, v2

    if-lez v9, :cond_1

    add-int/lit8 v6, v6, -0x1

    int-to-short v6, v6

    .line 10
    invoke-direct {p0, v6}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->getColumnWidthInPixels(I)F

    move-result v9

    float-to-double v9, v9

    sub-double/2addr v7, v2

    sub-double v2, v9, v7

    div-double/2addr v2, v9

    const-wide/high16 v7, 0x4090000000000000L    # 1024.0

    mul-double v2, v2, v7

    double-to-int v2, v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    .line 11
    :goto_1
    iput-short v6, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->col2:S

    .line 12
    iput v2, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->dx2:I

    .line 13
    iget v2, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->dy1:I

    int-to-float v2, v2

    const/high16 v3, 0x43800000    # 256.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->row1:I

    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->getRowHeightInPixels(I)F

    move-result v2

    mul-float v1, v1, v2

    add-float/2addr v1, p2

    .line 14
    iget p2, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->row1:I

    add-int/lit8 p2, p2, 0x1

    :goto_2
    float-to-double v2, v1

    cmpg-double v6, v2, v4

    if-gez v6, :cond_2

    add-int/lit8 v2, p2, 0x1

    .line 15
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->getRowHeightInPixels(I)F

    move-result p2

    add-float/2addr v1, p2

    move p2, v2

    goto :goto_2

    :cond_2
    cmpl-double v1, v2, v4

    if-lez v1, :cond_3

    add-int/lit8 p2, p2, -0x1

    .line 16
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->getRowHeightInPixels(I)F

    move-result p1

    float-to-double v6, p1

    sub-double/2addr v2, v4

    sub-double v1, v6, v2

    div-double/2addr v1, v6

    const-wide/high16 v3, 0x4070000000000000L    # 256.0

    mul-double v1, v1, v3

    double-to-int p1, v1

    .line 17
    :cond_3
    iput p2, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->row2:I

    .line 18
    iput p1, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->dy2:I

    return-object v0
.end method

.method public bridge synthetic getPreferredSize()Lcom/intsig/office/fc/hssf/usermodel/IClientAnchor;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->getPreferredSize()Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;

    move-result-object v0

    return-object v0
.end method

.method public resize()V
    .locals 2

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 12
    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->resize(D)V

    return-void
.end method

.method public resize(D)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getAnchor()Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;

    const/4 v1, 0x2

    .line 2
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->setAnchorType(I)V

    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->getPreferredSize(D)Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;

    move-result-object p1

    .line 4
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->getRow1()I

    move-result p2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->getRow2()I

    move-result v1

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->getRow1()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/2addr p2, v1

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->getCol1()S

    move-result v1

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->getCol2()S

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->getCol1()S

    move-result v3

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    int-to-short v1, v1

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->setCol2(S)V

    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->setDx1(I)V

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->getDx2()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->setDx2(I)V

    .line 9
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->setRow2(I)V

    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->setDy1(I)V

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->getDy2()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->setDy2(I)V

    return-void
.end method

.method public setPictureIndex(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->_pictureIndex:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

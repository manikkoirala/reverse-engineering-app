.class final Lcom/intsig/office/fc/hssf/formula/function/TextFunction$SearchFind;
.super Lcom/intsig/office/fc/hssf/formula/function/Var2or3ArgFunction;
.source "TextFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/formula/function/TextFunction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SearchFind"
.end annotation


# instance fields
.field private final 〇080:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/function/Var2or3ArgFunction;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-boolean p1, p0, Lcom/intsig/office/fc/hssf/formula/function/TextFunction$SearchFind;->〇080:Z

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇080(Ljava/lang/String;Ljava/lang/String;I)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hssf/formula/function/TextFunction$SearchFind;->〇080:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-virtual {p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p2

    .line 18
    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    :goto_0
    const/4 p2, -0x1

    .line 23
    if-ne p1, p2, :cond_1

    .line 24
    .line 25
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 26
    .line 27
    return-object p1

    .line 28
    :cond_1
    new-instance p2, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 29
    .line 30
    add-int/lit8 p1, p1, 0x1

    .line 31
    .line 32
    int-to-double v0, p1

    .line 33
    invoke-direct {p2, v0, v1}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    .line 34
    .line 35
    .line 36
    return-object p2
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 0

    .line 1
    :try_start_0
    invoke-static {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/TextFunction;->evaluateStringArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Ljava/lang/String;

    move-result-object p3

    .line 2
    invoke-static {p4, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/TextFunction;->evaluateStringArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    .line 3
    invoke-direct {p0, p1, p3, p2}, Lcom/intsig/office/fc/hssf/formula/function/TextFunction$SearchFind;->〇080(Ljava/lang/String;Ljava/lang/String;I)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object p1

    return-object p1
.end method

.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 0

    .line 5
    :try_start_0
    invoke-static {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/TextFunction;->evaluateStringArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Ljava/lang/String;

    move-result-object p3

    .line 6
    invoke-static {p4, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/TextFunction;->evaluateStringArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Ljava/lang/String;

    move-result-object p4

    .line 7
    invoke-static {p5, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/TextFunction;->evaluateIntArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    if-gez p1, :cond_0

    .line 8
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    return-object p1

    .line 9
    :cond_0
    invoke-direct {p0, p4, p3, p1}, Lcom/intsig/office/fc/hssf/formula/function/TextFunction$SearchFind;->〇080(Ljava/lang/String;Ljava/lang/String;I)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object p1

    return-object p1
.end method

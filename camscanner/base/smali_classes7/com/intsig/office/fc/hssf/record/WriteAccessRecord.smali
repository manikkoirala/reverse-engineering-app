.class public final Lcom/intsig/office/fc/hssf/record/WriteAccessRecord;
.super Lcom/intsig/office/fc/hssf/record/StandardRecord;
.source "WriteAccessRecord.java"


# static fields
.field private static final DATA_SIZE:I = 0x70

.field private static final PADDING:[B

.field private static final PAD_CHAR:B = 0x20t

.field public static final sid:S = 0x5cs


# instance fields
.field private field_1_username:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const/16 v0, 0x70

    .line 2
    .line 3
    new-array v0, v0, [B

    .line 4
    .line 5
    sput-object v0, Lcom/intsig/office/fc/hssf/record/WriteAccessRecord;->PADDING:[B

    .line 6
    .line 7
    const/16 v1, 0x20

    .line 8
    .line 9
    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    const-string v0, ""

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/WriteAccessRecord;->setUsername(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V
    .locals 6

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUShort()I

    move-result v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUByte()I

    move-result v1

    const/16 v2, 0x70

    if-gt v0, v2, :cond_3

    and-int/lit16 v2, v1, 0xfe

    if-eqz v2, :cond_0

    goto :goto_2

    :cond_0
    and-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_1

    .line 6
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/StringUtil;->readCompressedUnicode(Lcom/intsig/office/fc/util/LittleEndianInput;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 7
    :cond_1
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/StringUtil;->readUnicodeLE(Lcom/intsig/office/fc/util/LittleEndianInput;I)Ljava/lang/String;

    move-result-object v0

    .line 8
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/WriteAccessRecord;->field_1_username:Ljava/lang/String;

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->remaining()I

    move-result v0

    :goto_1
    if-lez v0, :cond_2

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUByte()I

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_2
    return-void

    .line 11
    :cond_3
    :goto_2
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->remaining()I

    move-result v2

    const/4 v3, 0x3

    add-int/2addr v2, v3

    new-array v4, v2, [B

    const/4 v5, 0x0

    .line 12
    invoke-static {v4, v5, v0}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    const/4 v0, 0x2

    .line 13
    invoke-static {v4, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putByte([BII)V

    sub-int/2addr v2, v3

    .line 14
    invoke-virtual {p1, v4, v3, v2}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readFully([BII)V

    .line 15
    new-instance p1, Ljava/lang/String;

    invoke-direct {p1, v4}, Ljava/lang/String;-><init>([B)V

    .line 16
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/WriteAccessRecord;->setUsername(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected getDataSize()I
    .locals 1

    .line 1
    const/16 v0, 0x70

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSid()S
    .locals 1

    .line 1
    const/16 v0, 0x5c

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/WriteAccessRecord;->field_1_username:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/WriteAccessRecord;->getUsername()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/util/StringUtil;->hasMultibyte(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 14
    .line 15
    .line 16
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeByte(I)V

    .line 17
    .line 18
    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    invoke-static {v0, p1}, Lcom/intsig/office/fc/util/StringUtil;->putUnicodeLE(Ljava/lang/String;Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-static {v0, p1}, Lcom/intsig/office/fc/util/StringUtil;->putCompressedUnicode(Ljava/lang/String;Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 26
    .line 27
    .line 28
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-eqz v1, :cond_1

    .line 33
    .line 34
    const/4 v1, 0x2

    .line 35
    goto :goto_1

    .line 36
    :cond_1
    const/4 v1, 0x1

    .line 37
    :goto_1
    mul-int v0, v0, v1

    .line 38
    .line 39
    add-int/lit8 v0, v0, 0x3

    .line 40
    .line 41
    rsub-int/lit8 v0, v0, 0x70

    .line 42
    .line 43
    sget-object v1, Lcom/intsig/office/fc/hssf/record/WriteAccessRecord;->PADDING:[B

    .line 44
    .line 45
    const/4 v2, 0x0

    .line 46
    invoke-interface {p1, v1, v2, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->write([BII)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setUsername(Ljava/lang/String;)V
    .locals 3

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/util/StringUtil;->hasMultibyte(Ljava/lang/String;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x2

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x1

    .line 14
    :goto_0
    mul-int v1, v1, v0

    .line 15
    .line 16
    add-int/lit8 v1, v1, 0x3

    .line 17
    .line 18
    rsub-int/lit8 v0, v1, 0x70

    .line 19
    .line 20
    if-ltz v0, :cond_1

    .line 21
    .line 22
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/WriteAccessRecord;->field_1_username:Ljava/lang/String;

    .line 23
    .line 24
    return-void

    .line 25
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 26
    .line 27
    new-instance v1, Ljava/lang/StringBuilder;

    .line 28
    .line 29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 30
    .line 31
    .line 32
    const-string v2, "Name is too long: "

    .line 33
    .line 34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    throw v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "[WRITEACCESS]\n"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 9
    .line 10
    .line 11
    const-string v1, "    .name = "

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 14
    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/WriteAccessRecord;->field_1_username:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 23
    .line 24
    .line 25
    const-string v1, "\n"

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 28
    .line 29
    .line 30
    const-string v1, "[/WRITEACCESS]\n"

    .line 31
    .line 32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    return-object v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.class public Lcom/intsig/office/fc/hssf/model/TextboxShape;
.super Lcom/intsig/office/fc/hssf/model/AbstractShape;
.source "TextboxShape.java"


# instance fields
.field private escherTextbox:Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

.field private objRecord:Lcom/intsig/office/fc/hssf/record/ObjRecord;

.field private spContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

.field private textObjectRecord:Lcom/intsig/office/fc/hssf/record/TextObjectRecord;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/model/AbstractShape;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hssf/model/TextboxShape;->createSpContainer(Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;I)Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/TextboxShape;->spContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 9
    .line 10
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hssf/model/TextboxShape;->createObjRecord(Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;I)Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/TextboxShape;->objRecord:Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 15
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hssf/model/TextboxShape;->createTextObjectRecord(Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;I)Lcom/intsig/office/fc/hssf/record/TextObjectRecord;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/model/TextboxShape;->textObjectRecord:Lcom/intsig/office/fc/hssf/record/TextObjectRecord;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private createObjRecord(Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;I)Lcom/intsig/office/fc/hssf/record/ObjRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/ObjRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;

    .line 7
    .line 8
    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;-><init>()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getShapeType()I

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    int-to-short p1, p1

    .line 16
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setObjectType(S)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hssf/model/AbstractShape;->getCmoObjectId(I)I

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setObjectId(I)V

    .line 24
    .line 25
    .line 26
    const/4 p1, 0x1

    .line 27
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setLocked(Z)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setPrintable(Z)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setAutofill(Z)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setAutoline(Z)V

    .line 37
    .line 38
    .line 39
    new-instance p1, Lcom/intsig/office/fc/hssf/record/EndSubRecord;

    .line 40
    .line 41
    invoke-direct {p1}, Lcom/intsig/office/fc/hssf/record/EndSubRecord;-><init>()V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->addSubRecord(Lcom/intsig/office/fc/hssf/record/SubRecord;)Z

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->addSubRecord(Lcom/intsig/office/fc/hssf/record/SubRecord;)Z

    .line 48
    .line 49
    .line 50
    return-object v0
    .line 51
    .line 52
    .line 53
.end method

.method private createSpContainer(Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;I)Lcom/intsig/office/fc/ddf/EscherContainerRecord;
    .locals 7

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 7
    .line 8
    invoke-direct {v1}, Lcom/intsig/office/fc/ddf/EscherSpRecord;-><init>()V

    .line 9
    .line 10
    .line 11
    new-instance v2, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 12
    .line 13
    invoke-direct {v2}, Lcom/intsig/office/fc/ddf/EscherOptRecord;-><init>()V

    .line 14
    .line 15
    .line 16
    new-instance v3, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;

    .line 17
    .line 18
    invoke-direct {v3}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;-><init>()V

    .line 19
    .line 20
    .line 21
    new-instance v3, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 22
    .line 23
    invoke-direct {v3}, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;-><init>()V

    .line 24
    .line 25
    .line 26
    new-instance v4, Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 27
    .line 28
    invoke-direct {v4}, Lcom/intsig/office/fc/ddf/EscherTextboxRecord;-><init>()V

    .line 29
    .line 30
    .line 31
    iput-object v4, p0, Lcom/intsig/office/fc/hssf/model/TextboxShape;->escherTextbox:Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 32
    .line 33
    const/16 v4, -0xffc

    .line 34
    .line 35
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 36
    .line 37
    .line 38
    const/16 v4, 0xf

    .line 39
    .line 40
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 41
    .line 42
    .line 43
    const/16 v4, -0xff6

    .line 44
    .line 45
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 46
    .line 47
    .line 48
    const/16 v4, 0xca2

    .line 49
    .line 50
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v1, p2}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setShapeId(I)V

    .line 54
    .line 55
    .line 56
    const/16 p2, 0xa00

    .line 57
    .line 58
    invoke-virtual {v1, p2}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setFlags(I)V

    .line 59
    .line 60
    .line 61
    const/16 p2, -0xff5

    .line 62
    .line 63
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 64
    .line 65
    .line 66
    new-instance p2, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 67
    .line 68
    const/16 v4, 0x80

    .line 69
    .line 70
    const/4 v5, 0x0

    .line 71
    invoke-direct {p2, v4, v5}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 75
    .line 76
    .line 77
    new-instance p2, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 78
    .line 79
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->getMarginLeft()I

    .line 80
    .line 81
    .line 82
    move-result v4

    .line 83
    const/16 v6, 0x81

    .line 84
    .line 85
    invoke-direct {p2, v6, v4}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 89
    .line 90
    .line 91
    new-instance p2, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 92
    .line 93
    const/16 v4, 0x83

    .line 94
    .line 95
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->getMarginRight()I

    .line 96
    .line 97
    .line 98
    move-result v6

    .line 99
    invoke-direct {p2, v4, v6}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 103
    .line 104
    .line 105
    new-instance p2, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 106
    .line 107
    const/16 v4, 0x84

    .line 108
    .line 109
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->getMarginBottom()I

    .line 110
    .line 111
    .line 112
    move-result v6

    .line 113
    invoke-direct {p2, v4, v6}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 117
    .line 118
    .line 119
    new-instance p2, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 120
    .line 121
    const/16 v4, 0x82

    .line 122
    .line 123
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->getMarginTop()I

    .line 124
    .line 125
    .line 126
    move-result v6

    .line 127
    invoke-direct {p2, v4, v6}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 128
    .line 129
    .line 130
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 131
    .line 132
    .line 133
    new-instance p2, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 134
    .line 135
    const/16 v4, 0x85

    .line 136
    .line 137
    invoke-direct {p2, v4, v5}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 141
    .line 142
    .line 143
    new-instance p2, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 144
    .line 145
    const/16 v4, 0x87

    .line 146
    .line 147
    invoke-direct {p2, v4, v5}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 148
    .line 149
    .line 150
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 151
    .line 152
    .line 153
    new-instance p2, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 154
    .line 155
    const/16 v4, 0x3bf

    .line 156
    .line 157
    const/high16 v6, 0x80000

    .line 158
    .line 159
    invoke-direct {p2, v4, v6}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 160
    .line 161
    .line 162
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 163
    .line 164
    .line 165
    invoke-virtual {p0, p1, v2}, Lcom/intsig/office/fc/hssf/model/AbstractShape;->addStandardOptions(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/ddf/EscherOptRecord;)I

    .line 166
    .line 167
    .line 168
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getAnchor()Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;

    .line 169
    .line 170
    .line 171
    move-result-object p1

    .line 172
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/model/AbstractShape;->createAnchor(Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 173
    .line 174
    .line 175
    move-result-object p1

    .line 176
    const/16 p2, -0xfef

    .line 177
    .line 178
    invoke-virtual {v3, p2}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 179
    .line 180
    .line 181
    invoke-virtual {v3, v5}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 182
    .line 183
    .line 184
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/model/TextboxShape;->escherTextbox:Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 185
    .line 186
    const/16 v4, -0xff3

    .line 187
    .line 188
    invoke-virtual {p2, v4}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 189
    .line 190
    .line 191
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/model/TextboxShape;->escherTextbox:Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 192
    .line 193
    invoke-virtual {p2, v5}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 194
    .line 195
    .line 196
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 197
    .line 198
    .line 199
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 200
    .line 201
    .line 202
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 203
    .line 204
    .line 205
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 206
    .line 207
    .line 208
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/model/TextboxShape;->escherTextbox:Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 209
    .line 210
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 211
    .line 212
    .line 213
    return-object v0
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private createTextObjectRecord(Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;I)Lcom/intsig/office/fc/hssf/record/TextObjectRecord;
    .locals 1

    .line 1
    new-instance p2, Lcom/intsig/office/fc/hssf/record/TextObjectRecord;

    .line 2
    .line 3
    invoke-direct {p2}, Lcom/intsig/office/fc/hssf/record/TextObjectRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->getHorizontalAlignment()S

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/hssf/record/TextObjectRecord;->setHorizontalTextAlignment(I)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->getVerticalAlignment()S

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/hssf/record/TextObjectRecord;->setVerticalTextAlignment(I)V

    .line 18
    .line 19
    .line 20
    const/4 v0, 0x1

    .line 21
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/hssf/record/TextObjectRecord;->setTextLocked(Z)V

    .line 22
    .line 23
    .line 24
    const/4 v0, 0x0

    .line 25
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/hssf/record/TextObjectRecord;->setTextOrientation(I)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->getString()Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/hssf/record/TextObjectRecord;->setStr(Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;)V

    .line 33
    .line 34
    .line 35
    return-object p2
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public getEscherTextbox()Lcom/intsig/office/fc/ddf/EscherRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/TextboxShape;->escherTextbox:Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getObjRecord()Lcom/intsig/office/fc/hssf/record/ObjRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/TextboxShape;->objRecord:Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/TextboxShape;->spContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTextObjectRecord()Lcom/intsig/office/fc/hssf/record/TextObjectRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/TextboxShape;->textObjectRecord:Lcom/intsig/office/fc/hssf/record/TextObjectRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

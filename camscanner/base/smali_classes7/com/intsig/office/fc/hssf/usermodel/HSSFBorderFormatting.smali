.class public final Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;
.super Ljava/lang/Object;
.source "HSSFBorderFormatting.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/BorderFormatting;


# instance fields
.field private final borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

.field private final cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;


# direct methods
.method protected constructor <init>(Lcom/intsig/office/fc/hssf/record/CFRuleRecord;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getBorderFormatting()Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public getBorderBottom()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->getBorderBottom()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBorderDiagonal()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->getBorderDiagonal()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getBorderFormattingBlock()Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBorderLeft()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->getBorderLeft()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBorderRight()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->getBorderRight()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBorderTop()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->getBorderTop()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBottomBorderColor()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->getBottomBorderColor()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDiagonalBorderColor()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->getDiagonalBorderColor()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLeftBorderColor()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->getLeftBorderColor()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRightBorderColor()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->getRightBorderColor()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTopBorderColor()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->getTopBorderColor()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isBackwardDiagonalOn()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->isBackwardDiagonalOn()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isForwardDiagonalOn()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->isForwardDiagonalOn()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setBackwardDiagonalOn(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->setBackwardDiagonalOn(Z)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setTopLeftBottomRightBorderModified(Z)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBorderBottom(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->setBorderBottom(I)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setBottomBorderModified(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBorderDiagonal(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->setBorderDiagonal(I)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setBottomLeftTopRightBorderModified(Z)V

    .line 12
    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 15
    .line 16
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setTopLeftBottomRightBorderModified(Z)V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBorderLeft(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->setBorderLeft(I)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setLeftBorderModified(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBorderRight(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->setBorderRight(I)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setRightBorderModified(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBorderTop(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->setBorderTop(I)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setTopBorderModified(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setBottomBorderColor(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->setBottomBorderColor(I)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setBottomBorderModified(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDiagonalBorderColor(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->setDiagonalBorderColor(I)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setBottomLeftTopRightBorderModified(Z)V

    .line 12
    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 15
    .line 16
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setTopLeftBottomRightBorderModified(Z)V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setForwardDiagonalOn(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->setForwardDiagonalOn(Z)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setBottomLeftTopRightBorderModified(Z)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLeftBorderColor(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->setLeftBorderColor(I)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setLeftBorderModified(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRightBorderColor(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->setRightBorderColor(I)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setRightBorderModified(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTopBorderColor(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->borderFormatting:Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;->setTopBorderColor(I)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setTopBorderModified(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

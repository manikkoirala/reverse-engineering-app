.class public Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;
.super Ljava/lang/Object;
.source "HSSFFormulaEvaluator.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/FormulaEvaluator;


# instance fields
.field private _book:Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

.field private _bookEvaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

.field private hssfEvaluationCell:Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;


# direct methods
.method public constructor <init>(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V

    .line 2
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->_book:Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V
    .locals 1

    const/4 v0, 0x0

    .line 3
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/hssf/formula/IStabilityClassifier;)V

    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->_book:Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/hssf/formula/IStabilityClassifier;)V
    .locals 1

    const/4 v0, 0x0

    .line 5
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/hssf/formula/IStabilityClassifier;Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;)V

    return-void
.end method

.method private constructor <init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/hssf/formula/IStabilityClassifier;Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;)V
    .locals 1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 7
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->hssfEvaluationCell:Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;

    .line 8
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    invoke-static {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->create(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;

    move-result-object p1

    invoke-direct {v0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;-><init>(Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;Lcom/intsig/office/fc/hssf/formula/IStabilityClassifier;Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;)V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->_bookEvaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    return-void
.end method

.method public static create(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/hssf/formula/IStabilityClassifier;Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;)Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/hssf/formula/IStabilityClassifier;Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static evaluateAllFormulaCells(Lcom/intsig/office/fc/ss/usermodel/Workbook;)V
    .locals 0

    .line 1
    return-void
.end method

.method private static evaluateAllFormulaCells(Lcom/intsig/office/fc/ss/usermodel/Workbook;Lcom/intsig/office/fc/ss/usermodel/FormulaEvaluator;)V
    .locals 0

    .line 2
    return-void
.end method

.method public static evaluateAllFormulaCells(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V
    .locals 1

    .line 3
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;

    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V

    invoke-static {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->evaluateAllFormulaCells(Lcom/intsig/office/fc/ss/usermodel/Workbook;Lcom/intsig/office/fc/ss/usermodel/FormulaEvaluator;)V

    return-void
.end method

.method private static setCellType(Lcom/intsig/office/fc/ss/usermodel/ICell;Lcom/intsig/office/fc/ss/usermodel/CellValue;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/usermodel/CellValue;->getCellType()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_1

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    if-eq p1, v0, :cond_1

    .line 9
    .line 10
    const/4 v0, 0x4

    .line 11
    if-eq p1, v0, :cond_1

    .line 12
    .line 13
    const/4 v0, 0x5

    .line 14
    if-ne p1, v0, :cond_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    .line 18
    .line 19
    new-instance v0, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    const-string v1, "Unexpected cell value type ("

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    const-string p1, ")"

    .line 33
    .line 34
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    throw p0

    .line 45
    :cond_1
    :goto_0
    invoke-interface {p0, p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->setCellType(I)V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static setCellValue(Lcom/intsig/office/fc/ss/usermodel/ICell;Lcom/intsig/office/fc/ss/usermodel/CellValue;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/usermodel/CellValue;->getCellType()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    if-eq v0, v1, :cond_2

    .line 9
    .line 10
    const/4 v1, 0x4

    .line 11
    if-eq v0, v1, :cond_1

    .line 12
    .line 13
    const/4 v1, 0x5

    .line 14
    if-ne v0, v1, :cond_0

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/usermodel/CellValue;->getErrorValue()B

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    invoke-interface {p0, p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->setCellErrorValue(B)V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    .line 25
    .line 26
    new-instance p1, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v1, "Unexpected cell value type ("

    .line 32
    .line 33
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    const-string v0, ")"

    .line 40
    .line 41
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    throw p0

    .line 52
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/usermodel/CellValue;->getBooleanValue()Z

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    invoke-interface {p0, p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->setCellValue(Z)V

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_2
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;

    .line 61
    .line 62
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/usermodel/CellValue;->getStringValue()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;-><init>(Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    invoke-interface {p0, v0}, Lcom/intsig/office/fc/ss/usermodel/ICell;->setCellValue(Lcom/intsig/office/fc/ss/usermodel/RichTextString;)V

    .line 70
    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/usermodel/CellValue;->getNumberValue()D

    .line 74
    .line 75
    .line 76
    move-result-wide v0

    .line 77
    invoke-interface {p0, v0, v1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->setCellValue(D)V

    .line 78
    .line 79
    .line 80
    :goto_0
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static setupEnvironment([Ljava/lang/String;[Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;)V
    .locals 4

    .line 1
    array-length v0, p1

    .line 2
    new-array v1, v0, [Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 3
    .line 4
    const/4 v2, 0x0

    .line 5
    :goto_0
    if-ge v2, v0, :cond_0

    .line 6
    .line 7
    aget-object v3, p1, v2

    .line 8
    .line 9
    iget-object v3, v3, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->_bookEvaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 10
    .line 11
    aput-object v3, v1, v2

    .line 12
    .line 13
    add-int/lit8 v2, v2, 0x1

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-static {p0, v1}, Lcom/intsig/office/fc/hssf/formula/CollaboratingWorkbooksEnvironment;->setup([Ljava/lang/String;[Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public clearAllCachedResultValues()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->_bookEvaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->clearAllCachedResultValues()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public evaluate(Lcom/intsig/office/fc/ss/usermodel/ICell;)Lcom/intsig/office/fc/ss/usermodel/CellValue;
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return-object v0

    .line 5
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getCellType()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-eqz v1, :cond_6

    .line 10
    .line 11
    const/4 v2, 0x1

    .line 12
    if-eq v1, v2, :cond_5

    .line 13
    .line 14
    const/4 v2, 0x2

    .line 15
    if-eq v1, v2, :cond_4

    .line 16
    .line 17
    const/4 v2, 0x3

    .line 18
    if-eq v1, v2, :cond_3

    .line 19
    .line 20
    const/4 v0, 0x4

    .line 21
    if-eq v1, v0, :cond_2

    .line 22
    .line 23
    const/4 v0, 0x5

    .line 24
    if-ne v1, v0, :cond_1

    .line 25
    .line 26
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getErrorCellValue()B

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    invoke-static {p1}, Lcom/intsig/office/fc/ss/usermodel/CellValue;->getError(I)Lcom/intsig/office/fc/ss/usermodel/CellValue;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    return-object p1

    .line 35
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 36
    .line 37
    new-instance v1, Ljava/lang/StringBuilder;

    .line 38
    .line 39
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    .line 41
    .line 42
    const-string v2, "Bad cell type ("

    .line 43
    .line 44
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getCellType()I

    .line 48
    .line 49
    .line 50
    move-result p1

    .line 51
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    const-string p1, ")"

    .line 55
    .line 56
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    throw v0

    .line 67
    :cond_2
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getBooleanCellValue()Z

    .line 68
    .line 69
    .line 70
    move-result p1

    .line 71
    invoke-static {p1}, Lcom/intsig/office/fc/ss/usermodel/CellValue;->valueOf(Z)Lcom/intsig/office/fc/ss/usermodel/CellValue;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    return-object p1

    .line 76
    :cond_3
    return-object v0

    .line 77
    :cond_4
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->evaluateFormulaCellValue(Lcom/intsig/office/fc/ss/usermodel/ICell;)Lcom/intsig/office/fc/ss/usermodel/CellValue;

    .line 78
    .line 79
    .line 80
    move-result-object p1

    .line 81
    return-object p1

    .line 82
    :cond_5
    new-instance v0, Lcom/intsig/office/fc/ss/usermodel/CellValue;

    .line 83
    .line 84
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getRichStringCellValue()Lcom/intsig/office/fc/ss/usermodel/RichTextString;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/RichTextString;->getString()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/ss/usermodel/CellValue;-><init>(Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    return-object v0

    .line 96
    :cond_6
    new-instance v0, Lcom/intsig/office/fc/ss/usermodel/CellValue;

    .line 97
    .line 98
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getNumericCellValue()D

    .line 99
    .line 100
    .line 101
    move-result-wide v1

    .line 102
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/ss/usermodel/CellValue;-><init>(D)V

    .line 103
    .line 104
    .line 105
    return-object v0
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public evaluateAll()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->_book:Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 2
    .line 3
    invoke-static {v0, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->evaluateAllFormulaCells(Lcom/intsig/office/fc/ss/usermodel/Workbook;Lcom/intsig/office/fc/ss/usermodel/FormulaEvaluator;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public evaluateFormulaCell(Lcom/intsig/office/fc/ss/usermodel/ICell;)I
    .locals 2

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getCellType()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x2

    .line 8
    if-eq v0, v1, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->evaluateFormulaCellValue(Lcom/intsig/office/fc/ss/usermodel/ICell;)Lcom/intsig/office/fc/ss/usermodel/CellValue;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-static {p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->setCellValue(Lcom/intsig/office/fc/ss/usermodel/ICell;Lcom/intsig/office/fc/ss/usermodel/CellValue;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/usermodel/CellValue;->getCellType()I

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    return p1

    .line 23
    :cond_1
    :goto_0
    const/4 p1, -0x1

    .line 24
    return p1
.end method

.method public evaluateFormulaCellValue(Lcom/intsig/office/fc/ss/usermodel/ICell;)Lcom/intsig/office/fc/ss/usermodel/CellValue;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->hssfEvaluationCell:Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/ss/model/XLSModel/ACell;

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;->〇o00〇〇Oo(Lcom/intsig/office/ss/model/XLSModel/ACell;)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;

    .line 12
    .line 13
    check-cast p1, Lcom/intsig/office/ss/model/XLSModel/ACell;

    .line 14
    .line 15
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;-><init>(Lcom/intsig/office/ss/model/XLSModel/ACell;)V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->hssfEvaluationCell:Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;

    .line 19
    .line 20
    :goto_0
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->_bookEvaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->clearAllCachedResultValues()V

    .line 23
    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->_bookEvaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->hssfEvaluationCell:Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->evaluate(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 34
    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 38
    .line 39
    new-instance v0, Lcom/intsig/office/fc/ss/usermodel/CellValue;

    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;->getNumberValue()D

    .line 42
    .line 43
    .line 44
    move-result-wide v1

    .line 45
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/ss/usermodel/CellValue;-><init>(D)V

    .line 46
    .line 47
    .line 48
    return-object v0

    .line 49
    :cond_1
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 50
    .line 51
    if-eqz v0, :cond_2

    .line 52
    .line 53
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 54
    .line 55
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;->getBooleanValue()Z

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    invoke-static {p1}, Lcom/intsig/office/fc/ss/usermodel/CellValue;->valueOf(Z)Lcom/intsig/office/fc/ss/usermodel/CellValue;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    return-object p1

    .line 64
    :cond_2
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;

    .line 65
    .line 66
    if-eqz v0, :cond_3

    .line 67
    .line 68
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;

    .line 69
    .line 70
    new-instance v0, Lcom/intsig/office/fc/ss/usermodel/CellValue;

    .line 71
    .line 72
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;->getStringValue()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/ss/usermodel/CellValue;-><init>(Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    return-object v0

    .line 80
    :cond_3
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 81
    .line 82
    if-eqz v0, :cond_4

    .line 83
    .line 84
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 85
    .line 86
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->getErrorCode()I

    .line 87
    .line 88
    .line 89
    move-result p1

    .line 90
    invoke-static {p1}, Lcom/intsig/office/fc/ss/usermodel/CellValue;->getError(I)Lcom/intsig/office/fc/ss/usermodel/CellValue;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    return-object p1

    .line 95
    :cond_4
    if-nez p1, :cond_5

    .line 96
    .line 97
    const/4 p1, 0x0

    .line 98
    return-object p1

    .line 99
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    .line 100
    .line 101
    new-instance v1, Ljava/lang/StringBuilder;

    .line 102
    .line 103
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .line 105
    .line 106
    const-string v2, "Unexpected eval class ("

    .line 107
    .line 108
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 112
    .line 113
    .line 114
    move-result-object p1

    .line 115
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object p1

    .line 119
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    const-string p1, ")"

    .line 123
    .line 124
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object p1

    .line 131
    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    throw v0
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public evaluateFormulaValueEval(Lcom/intsig/office/fc/hssf/usermodel/HSSFName;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 0

    .line 1
    const/4 p1, 0x0

    return-object p1
.end method

.method public evaluateFormulaValueEval(Lcom/intsig/office/ss/model/XLSModel/ACell;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->hssfEvaluationCell:Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;->〇o00〇〇Oo(Lcom/intsig/office/ss/model/XLSModel/ACell;)V

    goto :goto_0

    .line 4
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;-><init>(Lcom/intsig/office/ss/model/XLSModel/ACell;)V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->hssfEvaluationCell:Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;

    .line 5
    :goto_0
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->_bookEvaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->clearAllCachedResultValues()V

    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->_bookEvaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->hssfEvaluationCell:Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;

    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->evaluate(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    return-object p1
.end method

.method public evaluateInCell(Lcom/intsig/office/fc/ss/usermodel/ICell;)Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;
    .locals 3

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 2
    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;

    .line 3
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getCellType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 4
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->evaluateFormulaCellValue(Lcom/intsig/office/fc/ss/usermodel/ICell;)Lcom/intsig/office/fc/ss/usermodel/CellValue;

    move-result-object v1

    .line 5
    invoke-static {p1, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->setCellValue(Lcom/intsig/office/fc/ss/usermodel/ICell;Lcom/intsig/office/fc/ss/usermodel/CellValue;)V

    .line 6
    invoke-static {p1, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->setCellType(Lcom/intsig/office/fc/ss/usermodel/ICell;Lcom/intsig/office/fc/ss/usermodel/CellValue;)V

    :cond_1
    return-object v0
.end method

.method public bridge synthetic evaluateInCell(Lcom/intsig/office/fc/ss/usermodel/ICell;)Lcom/intsig/office/fc/ss/usermodel/ICell;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->evaluateInCell(Lcom/intsig/office/fc/ss/usermodel/ICell;)Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;

    move-result-object p1

    return-object p1
.end method

.method public notifyDeleteCell(Lcom/intsig/office/fc/ss/usermodel/ICell;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->_bookEvaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    new-instance v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;

    check-cast p1, Lcom/intsig/office/ss/model/XLSModel/ACell;

    invoke-direct {v1, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;-><init>(Lcom/intsig/office/ss/model/XLSModel/ACell;)V

    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->notifyDeleteCell(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)V

    return-void
.end method

.method public notifyDeleteCell(Lcom/intsig/office/ss/model/XLSModel/ACell;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->_bookEvaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    new-instance v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;

    invoke-direct {v1, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;-><init>(Lcom/intsig/office/ss/model/XLSModel/ACell;)V

    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->notifyDeleteCell(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)V

    return-void
.end method

.method public notifySetFormula(Lcom/intsig/office/fc/ss/usermodel/ICell;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->_bookEvaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/ss/model/XLSModel/ACell;

    .line 6
    .line 7
    invoke-direct {v1, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;-><init>(Lcom/intsig/office/ss/model/XLSModel/ACell;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->notifyUpdateCell(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public notifyUpdateCell(Lcom/intsig/office/fc/ss/usermodel/ICell;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->_bookEvaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    new-instance v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;

    check-cast p1, Lcom/intsig/office/ss/model/XLSModel/ACell;

    invoke-direct {v1, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;-><init>(Lcom/intsig/office/ss/model/XLSModel/ACell;)V

    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->notifyUpdateCell(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)V

    return-void
.end method

.method public notifyUpdateCell(Lcom/intsig/office/ss/model/XLSModel/ACell;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->_bookEvaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    new-instance v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;

    invoke-direct {v1, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationCell;-><init>(Lcom/intsig/office/ss/model/XLSModel/ACell;)V

    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->notifyUpdateCell(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)V

    return-void
.end method

.method public setCurrentRow(Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public abstract Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;
.super Lcom/intsig/office/fc/hssf/formula/function/MultiOperandNumericFunction;
.source "AggregateFunction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$ValueCollector;,
        Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$LargeSmall;
    }
.end annotation


# static fields
.field public static final AVEDEV:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final AVERAGE:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final DB:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final DDB:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final DEVSQ:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final LARGE:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final MAX:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final MEDIAN:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final MIN:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final PRODUCT:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final SMALL:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final STDEV:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final SUM:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final SUMSQ:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final VAR:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final VARP:Lcom/intsig/office/fc/hssf/formula/function/Function;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$2;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$2;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->AVEDEV:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$3;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$3;-><init>()V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->AVERAGE:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 14
    .line 15
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$4;

    .line 16
    .line 17
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$4;-><init>()V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->DEVSQ:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 21
    .line 22
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$LargeSmall;

    .line 23
    .line 24
    const/4 v1, 0x1

    .line 25
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$LargeSmall;-><init>(Z)V

    .line 26
    .line 27
    .line 28
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->LARGE:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 29
    .line 30
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$5;

    .line 31
    .line 32
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$5;-><init>()V

    .line 33
    .line 34
    .line 35
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->MAX:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 36
    .line 37
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$6;

    .line 38
    .line 39
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$6;-><init>()V

    .line 40
    .line 41
    .line 42
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->MEDIAN:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 43
    .line 44
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$7;

    .line 45
    .line 46
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$7;-><init>()V

    .line 47
    .line 48
    .line 49
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->MIN:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 50
    .line 51
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$8;

    .line 52
    .line 53
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$8;-><init>()V

    .line 54
    .line 55
    .line 56
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->PRODUCT:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 57
    .line 58
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$LargeSmall;

    .line 59
    .line 60
    const/4 v1, 0x0

    .line 61
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$LargeSmall;-><init>(Z)V

    .line 62
    .line 63
    .line 64
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->SMALL:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 65
    .line 66
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$9;

    .line 67
    .line 68
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$9;-><init>()V

    .line 69
    .line 70
    .line 71
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->STDEV:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 72
    .line 73
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$10;

    .line 74
    .line 75
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$10;-><init>()V

    .line 76
    .line 77
    .line 78
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->SUM:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 79
    .line 80
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$11;

    .line 81
    .line 82
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$11;-><init>()V

    .line 83
    .line 84
    .line 85
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->SUMSQ:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 86
    .line 87
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$12;

    .line 88
    .line 89
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$12;-><init>()V

    .line 90
    .line 91
    .line 92
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->VAR:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 93
    .line 94
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$13;

    .line 95
    .line 96
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$13;-><init>()V

    .line 97
    .line 98
    .line 99
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->VARP:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 100
    .line 101
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$14;

    .line 102
    .line 103
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$14;-><init>()V

    .line 104
    .line 105
    .line 106
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->DB:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 107
    .line 108
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$15;

    .line 109
    .line 110
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$15;-><init>()V

    .line 111
    .line 112
    .line 113
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->DDB:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 114
    .line 115
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method protected constructor <init>()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0, v0}, Lcom/intsig/office/fc/hssf/formula/function/MultiOperandNumericFunction;-><init>(ZZ)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static subtotalInstance(Lcom/intsig/office/fc/hssf/formula/function/Function;)Lcom/intsig/office/fc/hssf/formula/function/Function;
    .locals 1

    .line 1
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;

    .line 2
    .line 3
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$1;

    .line 4
    .line 5
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$1;-><init>(Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

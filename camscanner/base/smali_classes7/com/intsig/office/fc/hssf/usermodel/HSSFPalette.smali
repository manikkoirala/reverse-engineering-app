.class public final Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;
.super Ljava/lang/Object;
.source "HSSFPalette.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;
    }
.end annotation


# instance fields
.field private _palette:Lcom/intsig/office/fc/hssf/record/PaletteRecord;


# direct methods
.method protected constructor <init>(Lcom/intsig/office/fc/hssf/record/PaletteRecord;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->_palette:Lcom/intsig/office/fc/hssf/record/PaletteRecord;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private unsignedInt(B)I
    .locals 0

    .line 1
    and-int/lit16 p1, p1, 0xff

    .line 2
    .line 3
    return p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public addColor(BBB)Lcom/intsig/office/fc/hssf/util/HSSFColor;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->_palette:Lcom/intsig/office/fc/hssf/record/PaletteRecord;

    .line 2
    .line 3
    const/16 v1, 0x8

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PaletteRecord;->getColor(I)[B

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :goto_0
    const/16 v2, 0x40

    .line 10
    .line 11
    if-ge v1, v2, :cond_1

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    invoke-virtual {p0, v1, p1, p2, p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->setColorAtIndex(SBBB)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->getColor(S)Lcom/intsig/office/fc/hssf/util/HSSFColor;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    return-object p1

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->_palette:Lcom/intsig/office/fc/hssf/record/PaletteRecord;

    .line 24
    .line 25
    add-int/lit8 v1, v1, 0x1

    .line 26
    .line 27
    int-to-short v1, v1

    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PaletteRecord;->getColor(I)[B

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    goto :goto_0

    .line 33
    :cond_1
    new-instance p1, Ljava/lang/RuntimeException;

    .line 34
    .line 35
    const-string p2, "Could not find free color index"

    .line 36
    .line 37
    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    throw p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public findColor(BBB)Lcom/intsig/office/fc/hssf/util/HSSFColor;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->_palette:Lcom/intsig/office/fc/hssf/record/PaletteRecord;

    .line 2
    .line 3
    const/16 v1, 0x8

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PaletteRecord;->getColor(I)[B

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :goto_0
    if-eqz v0, :cond_1

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    aget-byte v2, v0, v2

    .line 13
    .line 14
    if-ne v2, p1, :cond_0

    .line 15
    .line 16
    const/4 v2, 0x1

    .line 17
    aget-byte v2, v0, v2

    .line 18
    .line 19
    if-ne v2, p2, :cond_0

    .line 20
    .line 21
    const/4 v2, 0x2

    .line 22
    aget-byte v2, v0, v2

    .line 23
    .line 24
    if-ne v2, p3, :cond_0

    .line 25
    .line 26
    new-instance p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;

    .line 27
    .line 28
    invoke-direct {p1, v1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;-><init>(S[B)V

    .line 29
    .line 30
    .line 31
    return-object p1

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->_palette:Lcom/intsig/office/fc/hssf/record/PaletteRecord;

    .line 33
    .line 34
    add-int/lit8 v1, v1, 0x1

    .line 35
    .line 36
    int-to-short v1, v1

    .line 37
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PaletteRecord;->getColor(I)[B

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    goto :goto_0

    .line 42
    :cond_1
    const/4 p1, 0x0

    .line 43
    return-object p1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public findSimilarColor(BBB)Lcom/intsig/office/fc/hssf/util/HSSFColor;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->unsignedInt(B)I

    move-result p1

    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->unsignedInt(B)I

    move-result p2

    invoke-direct {p0, p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->unsignedInt(B)I

    move-result p3

    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->findSimilarColor(III)Lcom/intsig/office/fc/hssf/util/HSSFColor;

    move-result-object p1

    return-object p1
.end method

.method public findSimilarColor(III)Lcom/intsig/office/fc/hssf/util/HSSFColor;
    .locals 6

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->_palette:Lcom/intsig/office/fc/hssf/record/PaletteRecord;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PaletteRecord;->getColor(I)[B

    move-result-object v0

    const/4 v2, 0x0

    const v3, 0x7fffffff

    :goto_0
    if-eqz v0, :cond_1

    const/4 v4, 0x0

    .line 3
    aget-byte v4, v0, v4

    invoke-direct {p0, v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->unsignedInt(B)I

    move-result v4

    sub-int v4, p1, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    const/4 v5, 0x1

    aget-byte v5, v0, v5

    .line 4
    invoke-direct {p0, v5}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->unsignedInt(B)I

    move-result v5

    sub-int v5, p2, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    add-int/2addr v4, v5

    const/4 v5, 0x2

    aget-byte v0, v0, v5

    .line 5
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->unsignedInt(B)I

    move-result v0

    sub-int v0, p3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    add-int/2addr v4, v0

    if-ge v4, v3, :cond_0

    .line 6
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->getColor(S)Lcom/intsig/office/fc/hssf/util/HSSFColor;

    move-result-object v0

    move-object v2, v0

    move v3, v4

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->_palette:Lcom/intsig/office/fc/hssf/record/PaletteRecord;

    add-int/lit8 v1, v1, 0x1

    int-to-short v1, v1

    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PaletteRecord;->getColor(I)[B

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public getColor(I)Lcom/intsig/office/fc/hssf/util/HSSFColor;
    .locals 0

    int-to-short p1, p1

    .line 4
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->getColor(S)Lcom/intsig/office/fc/hssf/util/HSSFColor;

    move-result-object p1

    return-object p1
.end method

.method public getColor(S)Lcom/intsig/office/fc/hssf/util/HSSFColor;
    .locals 2

    const/16 v0, 0x40

    if-ne p1, v0, :cond_0

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hssf/util/HSSFColor$AUTOMATIC;->getInstance()Lcom/intsig/office/fc/hssf/util/HSSFColor;

    move-result-object p1

    return-object p1

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->_palette:Lcom/intsig/office/fc/hssf/record/PaletteRecord;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/PaletteRecord;->getColor(I)[B

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3
    new-instance v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;

    invoke-direct {v1, p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;-><init>(S[B)V

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public setColorAtIndex(SBBB)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;->_palette:Lcom/intsig/office/fc/hssf/record/PaletteRecord;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/intsig/office/fc/hssf/record/PaletteRecord;->setColor(SBBB)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

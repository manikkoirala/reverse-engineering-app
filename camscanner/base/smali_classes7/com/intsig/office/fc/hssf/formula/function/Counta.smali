.class public final Lcom/intsig/office/fc/hssf/formula/function/Counta;
.super Ljava/lang/Object;
.source "Counta.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/function/Function;


# static fields
.field private static final defaultPredicate:Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;

.field private static final subtotalPredicate:Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;


# instance fields
.field private final _predicate:Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/Counta$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/Counta$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/Counta;->defaultPredicate:Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/Counta$2;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/Counta$2;-><init>()V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/Counta;->subtotalPredicate:Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/function/Counta;->defaultPredicate:Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/formula/function/Counta;->_predicate:Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;

    return-void
.end method

.method private constructor <init>(Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;)V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/formula/function/Counta;->_predicate:Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;

    return-void
.end method

.method public static subtotalInstance()Lcom/intsig/office/fc/hssf/formula/function/Counta;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/Counta;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/function/Counta;->subtotalPredicate:Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/function/Counta;-><init>(Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇080()Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/function/Counta;->defaultPredicate:Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public evaluate([Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 3

    .line 1
    array-length p2, p1

    .line 2
    const/4 p3, 0x1

    .line 3
    if-ge p2, p3, :cond_0

    .line 4
    .line 5
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 6
    .line 7
    return-object p1

    .line 8
    :cond_0
    const/16 p3, 0x1e

    .line 9
    .line 10
    if-le p2, p3, :cond_1

    .line 11
    .line 12
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 13
    .line 14
    return-object p1

    .line 15
    :cond_1
    const/4 p3, 0x0

    .line 16
    const/4 v0, 0x0

    .line 17
    :goto_0
    if-ge p3, p2, :cond_2

    .line 18
    .line 19
    aget-object v1, p1, p3

    .line 20
    .line 21
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/formula/function/Counta;->_predicate:Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;

    .line 22
    .line 23
    invoke-static {v1, v2}, Lcom/intsig/office/fc/hssf/formula/function/CountUtils;->〇080(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;)I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    add-int/2addr v0, v1

    .line 28
    add-int/lit8 p3, p3, 0x1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_2
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 32
    .line 33
    int-to-double p2, v0

    .line 34
    invoke-direct {p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    .line 35
    .line 36
    .line 37
    return-object p1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.class public final Lcom/intsig/office/fc/hssf/record/SSTRecord;
.super Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecord;
.source "SSTRecord.java"


# static fields
.field private static final EMPTY_STRING:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

.field static final MAX_DATA_SPACE:I = 0x2018

.field static final SST_RECORD_OVERHEAD:I = 0xc

.field static final STD_RECORD_OVERHEAD:I = 0x4

.field public static final sid:S = 0xfcs


# instance fields
.field bucketAbsoluteOffsets:[I

.field bucketRelativeOffsets:[I

.field private deserializer:Lcom/intsig/office/fc/hssf/record/SSTDeserializer;

.field private field_1_num_strings:I

.field private field_2_num_unique_strings:I

.field private field_3_strings:Lcom/intsig/office/fc/util/IntMapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/office/fc/util/IntMapper<",
            "Lcom/intsig/office/fc/hssf/record/common/UnicodeString;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 2
    .line 3
    const-string v1, ""

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->EMPTY_STRING:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecord;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_1_num_strings:I

    .line 3
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_2_num_unique_strings:I

    .line 4
    new-instance v0, Lcom/intsig/office/fc/util/IntMapper;

    invoke-direct {v0}, Lcom/intsig/office/fc/util/IntMapper;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_3_strings:Lcom/intsig/office/fc/util/IntMapper;

    .line 5
    new-instance v1, Lcom/intsig/office/fc/hssf/record/SSTDeserializer;

    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hssf/record/SSTDeserializer;-><init>(Lcom/intsig/office/fc/util/IntMapper;)V

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->deserializer:Lcom/intsig/office/fc/hssf/record/SSTDeserializer;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V
    .locals 2

    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecord;-><init>()V

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_1_num_strings:I

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_2_num_unique_strings:I

    .line 9
    new-instance v0, Lcom/intsig/office/fc/util/IntMapper;

    invoke-direct {v0}, Lcom/intsig/office/fc/util/IntMapper;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_3_strings:Lcom/intsig/office/fc/util/IntMapper;

    .line 10
    new-instance v1, Lcom/intsig/office/fc/hssf/record/SSTDeserializer;

    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hssf/record/SSTDeserializer;-><init>(Lcom/intsig/office/fc/util/IntMapper;)V

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->deserializer:Lcom/intsig/office/fc/hssf/record/SSTDeserializer;

    .line 11
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_2_num_unique_strings:I

    invoke-virtual {v1, v0, p1}, Lcom/intsig/office/fc/hssf/record/SSTDeserializer;->〇o00〇〇Oo(ILcom/intsig/office/fc/hssf/record/RecordInputStream;)V

    return-void
.end method


# virtual methods
.method public addString(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;)I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_1_num_strings:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_1_num_strings:I

    .line 6
    .line 7
    if-nez p1, :cond_0

    .line 8
    .line 9
    sget-object p1, Lcom/intsig/office/fc/hssf/record/SSTRecord;->EMPTY_STRING:Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_3_strings:Lcom/intsig/office/fc/util/IntMapper;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/util/IntMapper;->getIndex(Ljava/lang/Object;)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, -0x1

    .line 18
    if-eq v0, v1, :cond_1

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_3_strings:Lcom/intsig/office/fc/util/IntMapper;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/office/fc/util/IntMapper;->size()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_2_num_unique_strings:I

    .line 28
    .line 29
    add-int/lit8 v1, v1, 0x1

    .line 30
    .line 31
    iput v1, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_2_num_unique_strings:I

    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_3_strings:Lcom/intsig/office/fc/util/IntMapper;

    .line 34
    .line 35
    invoke-static {v1, p1}, Lcom/intsig/office/fc/hssf/record/SSTDeserializer;->〇080(Lcom/intsig/office/fc/util/IntMapper;Lcom/intsig/office/fc/hssf/record/common/UnicodeString;)V

    .line 36
    .line 37
    .line 38
    :goto_0
    return v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public calcExtSSTRecordSize()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_3_strings:Lcom/intsig/office/fc/util/IntMapper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/util/IntMapper;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/ExtSSTRecord;->getRecordSizeForStrings(I)I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method countStrings()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_3_strings:Lcom/intsig/office/fc/util/IntMapper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/util/IntMapper;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public createExtSSTRecord(I)Lcom/intsig/office/fc/hssf/record/ExtSSTRecord;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->bucketAbsoluteOffsets:[I

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/office/fc/hssf/record/ExtSSTRecord;

    .line 8
    .line 9
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/ExtSSTRecord;-><init>()V

    .line 10
    .line 11
    .line 12
    const/16 v1, 0x8

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/ExtSSTRecord;->setNumStringsPerBucket(S)V

    .line 15
    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->bucketAbsoluteOffsets:[I

    .line 18
    .line 19
    invoke-virtual {v1}, [I->clone()Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    check-cast v1, [I

    .line 24
    .line 25
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->bucketRelativeOffsets:[I

    .line 26
    .line 27
    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    check-cast v2, [I

    .line 32
    .line 33
    const/4 v3, 0x0

    .line 34
    :goto_0
    array-length v4, v1

    .line 35
    if-ge v3, v4, :cond_0

    .line 36
    .line 37
    aget v4, v1, v3

    .line 38
    .line 39
    add-int/2addr v4, p1

    .line 40
    aput v4, v1, v3

    .line 41
    .line 42
    add-int/lit8 v3, v3, 0x1

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_0
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hssf/record/ExtSSTRecord;->setBucketOffsets([I[I)V

    .line 46
    .line 47
    .line 48
    return-object v0

    .line 49
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 50
    .line 51
    const-string v0, "SST record has not yet been serialized."

    .line 52
    .line 53
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    throw p1
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method getDeserializer()Lcom/intsig/office/fc/hssf/record/SSTDeserializer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->deserializer:Lcom/intsig/office/fc/hssf/record/SSTDeserializer;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNumStrings()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_1_num_strings:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNumUniqueStrings()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_2_num_unique_strings:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSid()S
    .locals 1

    .line 1
    const/16 v0, 0xfc

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getString(I)Lcom/intsig/office/fc/hssf/record/common/UnicodeString;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_3_strings:Lcom/intsig/office/fc/util/IntMapper;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/util/IntMapper;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 8
    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method getStrings()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/intsig/office/fc/hssf/record/common/UnicodeString;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_3_strings:Lcom/intsig/office/fc/util/IntMapper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/util/IntMapper;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected serialize(Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;)V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/SSTSerializer;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_3_strings:Lcom/intsig/office/fc/util/IntMapper;

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/SSTRecord;->getNumStrings()I

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/SSTRecord;->getNumUniqueStrings()I

    .line 10
    .line 11
    .line 12
    move-result v3

    .line 13
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/hssf/record/SSTSerializer;-><init>(Lcom/intsig/office/fc/util/IntMapper;II)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/SSTSerializer;->Oo08(Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/SSTSerializer;->〇080()[I

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->bucketAbsoluteOffsets:[I

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/SSTSerializer;->〇o00〇〇Oo()[I

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->bucketRelativeOffsets:[I

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "[SST]\n"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 9
    .line 10
    .line 11
    const-string v1, "    .numstrings     = "

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/SSTRecord;->getNumStrings()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 25
    .line 26
    .line 27
    const-string v1, "\n"

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 30
    .line 31
    .line 32
    const-string v2, "    .uniquestrings  = "

    .line 33
    .line 34
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/SSTRecord;->getNumUniqueStrings()I

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 49
    .line 50
    .line 51
    const/4 v2, 0x0

    .line 52
    :goto_0
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_3_strings:Lcom/intsig/office/fc/util/IntMapper;

    .line 53
    .line 54
    invoke-virtual {v3}, Lcom/intsig/office/fc/util/IntMapper;->size()I

    .line 55
    .line 56
    .line 57
    move-result v3

    .line 58
    if-ge v2, v3, :cond_0

    .line 59
    .line 60
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/SSTRecord;->field_3_strings:Lcom/intsig/office/fc/util/IntMapper;

    .line 61
    .line 62
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/util/IntMapper;->get(I)Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    move-result-object v3

    .line 66
    check-cast v3, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 67
    .line 68
    new-instance v4, Ljava/lang/StringBuilder;

    .line 69
    .line 70
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 71
    .line 72
    .line 73
    const-string v5, "    .string_"

    .line 74
    .line 75
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    const-string v5, "      = "

    .line 82
    .line 83
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v4

    .line 90
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;->getDebugInfo()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v3

    .line 97
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 98
    .line 99
    .line 100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 101
    .line 102
    .line 103
    add-int/lit8 v2, v2, 0x1

    .line 104
    .line 105
    goto :goto_0

    .line 106
    :cond_0
    const-string v1, "[/SST]\n"

    .line 107
    .line 108
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 109
    .line 110
    .line 111
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object v0

    .line 115
    return-object v0
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.class public final Lcom/intsig/office/fc/hssf/model/HSSFFormulaParser;
.super Ljava/lang/Object;
.source "HSSFFormulaParser.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static createParsingWorkbook(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->create(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static parse(Ljava/lang/String;Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/FormulaParseException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    invoke-static {p0, p1, v0}, Lcom/intsig/office/fc/hssf/model/HSSFFormulaParser;->parse(Ljava/lang/String;Lcom/intsig/office/ss/model/XLSModel/AWorkbook;I)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    move-result-object p0

    return-object p0
.end method

.method public static parse(Ljava/lang/String;Lcom/intsig/office/ss/model/XLSModel/AWorkbook;I)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/FormulaParseException;
        }
    .end annotation

    const/4 v0, -0x1

    .line 2
    invoke-static {p0, p1, p2, v0}, Lcom/intsig/office/fc/hssf/model/HSSFFormulaParser;->parse(Ljava/lang/String;Lcom/intsig/office/ss/model/XLSModel/AWorkbook;II)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    move-result-object p0

    return-object p0
.end method

.method public static parse(Ljava/lang/String;Lcom/intsig/office/ss/model/XLSModel/AWorkbook;II)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/FormulaParseException;
        }
    .end annotation

    .line 3
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/model/HSSFFormulaParser;->createParsingWorkbook(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;

    move-result-object p1

    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parse(Ljava/lang/String;Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;II)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    move-result-object p0

    return-object p0
.end method

.method public static toFormulaString(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->create(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaRenderer;->toFormulaString(Lcom/intsig/office/fc/hssf/formula/FormulaRenderingWorkbook;[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

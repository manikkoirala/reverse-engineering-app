.class public final Lcom/intsig/office/fc/hssf/formula/function/DateFunc;
.super Lcom/intsig/office/fc/hssf/formula/function/Fixed3ArgFunction;
.source "DateFunc.java"


# static fields
.field public static final instance:Lcom/intsig/office/fc/hssf/formula/function/Function;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/DateFunc;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/DateFunc;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/DateFunc;->instance:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/function/Fixed3ArgFunction;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static evaluate(III)D
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation

    if-ltz p0, :cond_4

    if-ltz p1, :cond_4

    if-ltz p2, :cond_4

    const/4 v0, 0x1

    const/16 v1, 0x76c

    if-ne p0, v1, :cond_0

    if-ne p1, v0, :cond_0

    const/16 v2, 0x1d

    if-ne p2, v2, :cond_0

    const-wide/high16 p0, 0x404e000000000000L    # 60.0

    return-wide p0

    :cond_0
    if-ne p0, v1, :cond_3

    if-nez p1, :cond_1

    const/16 v1, 0x3c

    if-ge p2, v1, :cond_2

    :cond_1
    if-ne p1, v0, :cond_3

    const/16 v0, 0x1e

    if-lt p2, v0, :cond_3

    :cond_2
    add-int/lit8 p2, p2, -0x1

    :cond_3
    move v3, p2

    .line 8
    new-instance p2, Ljava/util/GregorianCalendar;

    invoke-direct {p2}, Ljava/util/GregorianCalendar;-><init>()V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p2

    move v1, p0

    move v2, p1

    .line 9
    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    const/16 p0, 0xe

    const/4 p1, 0x0

    .line 10
    invoke-virtual {p2, p0, p1}, Ljava/util/Calendar;->set(II)V

    .line 11
    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/intsig/office/ss/util/DateUtil;->getExcelDate(Ljava/util/Date;Z)D

    move-result-wide p0

    return-wide p0

    .line 12
    :cond_4
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;)V

    throw p0
.end method

.method private static getYear(D)I
    .locals 0

    .line 1
    double-to-int p0, p0

    .line 2
    if-gez p0, :cond_0

    .line 3
    .line 4
    const/4 p0, -0x1

    .line 5
    return p0

    .line 6
    :cond_0
    const/16 p1, 0x76c

    .line 7
    .line 8
    if-ge p0, p1, :cond_1

    .line 9
    .line 10
    add-int/lit16 p0, p0, 0x76c

    .line 11
    .line 12
    :cond_1
    return p0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 2

    .line 1
    :try_start_0
    invoke-static {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    move-result-wide v0

    .line 2
    invoke-static {p4, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    move-result-wide p3

    .line 3
    invoke-static {p5, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    move-result-wide p1

    .line 4
    invoke-static {v0, v1}, Lcom/intsig/office/fc/hssf/formula/function/DateFunc;->getYear(D)I

    move-result p5

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    sub-double/2addr p3, v0

    double-to-int p3, p3

    double-to-int p1, p1

    invoke-static {p5, p3, p1}, Lcom/intsig/office/fc/hssf/formula/function/DateFunc;->evaluate(III)D

    move-result-wide p1

    .line 5
    invoke-static {p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->checkValue(D)V
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    new-instance p3, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    invoke-direct {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    return-object p3

    :catch_0
    move-exception p1

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object p1

    return-object p1
.end method

.class public final Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;
.super Ljava/lang/Object;
.source "HSSFConditionalFormattingRule.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;


# static fields
.field private static final CELL_COMPARISON:B = 0x1t


# instance fields
.field private final cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

.field private final workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;Lcom/intsig/office/fc/hssf/record/CFRuleRecord;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_1

    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    .line 9
    .line 10
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 14
    .line 15
    const-string p2, "pRuleRecord must not be null"

    .line 16
    .line 17
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    throw p1

    .line 21
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 22
    .line 23
    const-string p2, "pWorkbook must not be null"

    .line 24
    .line 25
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    throw p1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private getBorderFormatting(Z)Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getBorderFormatting()Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setBorderFormatting(Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;)V

    .line 4
    new-instance p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;

    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;-><init>(Lcom/intsig/office/fc/hssf/record/CFRuleRecord;)V

    return-object p1

    :cond_0
    if-eqz p1, :cond_1

    .line 5
    new-instance p1, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;

    invoke-direct {p1}, Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;-><init>()V

    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setBorderFormatting(Lcom/intsig/office/fc/hssf/record/cf/BorderFormatting;)V

    .line 7
    new-instance p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;

    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;-><init>(Lcom/intsig/office/fc/hssf/record/CFRuleRecord;)V

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private getFontFormatting(Z)Lcom/intsig/office/fc/hssf/usermodel/HSSFFontFormatting;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getFontFormatting()Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setFontFormatting(Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;)V

    .line 4
    new-instance p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFFontFormatting;

    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFontFormatting;-><init>(Lcom/intsig/office/fc/hssf/record/CFRuleRecord;)V

    return-object p1

    :cond_0
    if-eqz p1, :cond_1

    .line 5
    new-instance p1, Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;

    invoke-direct {p1}, Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;-><init>()V

    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setFontFormatting(Lcom/intsig/office/fc/hssf/record/cf/FontFormatting;)V

    .line 7
    new-instance p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFFontFormatting;

    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFontFormatting;-><init>(Lcom/intsig/office/fc/hssf/record/CFRuleRecord;)V

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private getPatternFormatting(Z)Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getPatternFormatting()Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setPatternFormatting(Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;)V

    .line 4
    new-instance p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;

    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;-><init>(Lcom/intsig/office/fc/hssf/record/CFRuleRecord;)V

    return-object p1

    :cond_0
    if-eqz p1, :cond_1

    .line 5
    new-instance p1, Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    invoke-direct {p1}, Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;-><init>()V

    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setPatternFormatting(Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;)V

    .line 7
    new-instance p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;

    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;-><init>(Lcom/intsig/office/fc/hssf/record/CFRuleRecord;)V

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private toFormulaString([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Ljava/lang/String;
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    return-object p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public createBorderFormatting()Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;
    .locals 1

    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->getBorderFormatting(Z)Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createBorderFormatting()Lcom/intsig/office/fc/ss/usermodel/BorderFormatting;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->createBorderFormatting()Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;

    move-result-object v0

    return-object v0
.end method

.method public createFontFormatting()Lcom/intsig/office/fc/hssf/usermodel/HSSFFontFormatting;
    .locals 1

    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->getFontFormatting(Z)Lcom/intsig/office/fc/hssf/usermodel/HSSFFontFormatting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createFontFormatting()Lcom/intsig/office/fc/ss/usermodel/FontFormatting;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->createFontFormatting()Lcom/intsig/office/fc/hssf/usermodel/HSSFFontFormatting;

    move-result-object v0

    return-object v0
.end method

.method public createPatternFormatting()Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;
    .locals 1

    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->getPatternFormatting(Z)Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createPatternFormatting()Lcom/intsig/office/fc/ss/usermodel/PatternFormatting;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->createPatternFormatting()Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;

    move-result-object v0

    return-object v0
.end method

.method public getBorderFormatting()Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;
    .locals 1

    const/4 v0, 0x0

    .line 8
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->getBorderFormatting(Z)Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getBorderFormatting()Lcom/intsig/office/fc/ss/usermodel/BorderFormatting;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->getBorderFormatting()Lcom/intsig/office/fc/hssf/usermodel/HSSFBorderFormatting;

    move-result-object v0

    return-object v0
.end method

.method getCfRuleRecord()Lcom/intsig/office/fc/hssf/record/CFRuleRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getComparisonOperation()B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getComparisonOperation()B

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getConditionType()B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getConditionType()B

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFontFormatting()Lcom/intsig/office/fc/hssf/usermodel/HSSFFontFormatting;
    .locals 1

    const/4 v0, 0x0

    .line 8
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->getFontFormatting(Z)Lcom/intsig/office/fc/hssf/usermodel/HSSFFontFormatting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getFontFormatting()Lcom/intsig/office/fc/ss/usermodel/FontFormatting;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->getFontFormatting()Lcom/intsig/office/fc/hssf/usermodel/HSSFFontFormatting;

    move-result-object v0

    return-object v0
.end method

.method public getFormula1()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getParsedExpression1()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->toFormulaString([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFormula2()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getConditionType()B

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-ne v0, v1, :cond_1

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getComparisonOperation()B

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eq v0, v1, :cond_0

    .line 17
    .line 18
    const/4 v1, 0x2

    .line 19
    if-eq v0, v1, :cond_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getParsedExpression2()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->toFormulaString([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    return-object v0

    .line 33
    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 34
    return-object v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getPatternFormatting()Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;
    .locals 1

    const/4 v0, 0x0

    .line 8
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->getPatternFormatting(Z)Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPatternFormatting()Lcom/intsig/office/fc/ss/usermodel/PatternFormatting;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->getPatternFormatting()Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;

    move-result-object v0

    return-object v0
.end method

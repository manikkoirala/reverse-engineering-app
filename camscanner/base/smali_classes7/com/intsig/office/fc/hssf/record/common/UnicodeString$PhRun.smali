.class public Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;
.super Ljava/lang/Object;
.source "UnicodeString.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/record/common/UnicodeString;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhRun"
.end annotation


# instance fields
.field private phoneticTextFirstCharacterOffset:I

.field private realTextFirstCharacterOffset:I

.field private realTextLength:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->phoneticTextFirstCharacterOffset:I

    .line 4
    iput p2, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->realTextFirstCharacterOffset:I

    .line 5
    iput p3, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->realTextLength:I

    return-void
.end method

.method private constructor <init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V
    .locals 1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->phoneticTextFirstCharacterOffset:I

    .line 8
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->realTextFirstCharacterOffset:I

    .line 9
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->realTextLength:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/office/fc/util/LittleEndianInput;Lcom/intsig/office/fc/hssf/record/common/〇080;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    return-void
.end method

.method static bridge synthetic O8(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->serialize(Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private serialize(Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;)V
    .locals 1

    .line 1
    const/4 v0, 0x6

    .line 2
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 3
    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->phoneticTextFirstCharacterOffset:I

    .line 6
    .line 7
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 8
    .line 9
    .line 10
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->realTextFirstCharacterOffset:I

    .line 11
    .line 12
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 13
    .line 14
    .line 15
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->realTextLength:I

    .line 16
    .line 17
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->phoneticTextFirstCharacterOffset:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->realTextFirstCharacterOffset:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$PhRun;->realTextLength:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

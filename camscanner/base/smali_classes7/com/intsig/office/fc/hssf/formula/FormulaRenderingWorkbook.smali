.class public interface abstract Lcom/intsig/office/fc/hssf/formula/FormulaRenderingWorkbook;
.super Ljava/lang/Object;
.source "FormulaRenderingWorkbook.java"


# virtual methods
.method public abstract getExternalSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;
.end method

.method public abstract getNameText(Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;)Ljava/lang/String;
.end method

.method public abstract getSheetNameByExternSheet(I)Ljava/lang/String;
.end method

.method public abstract resolveNameXText(Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;)Ljava/lang/String;
.end method

.class public Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;
.super Ljava/lang/Object;
.source "HSSFChart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "HSSFSeries"
.end annotation


# instance fields
.field private areaFormatRecord:Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

.field private dataCategoryLabels:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

.field private dataName:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

.field private dataSecondaryCategoryLabels:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

.field private dataValues:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

.field private dleRec:Lcom/intsig/office/fc/hssf/record/chart/DataLabelExtensionRecord;

.field private series:Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

.field private seriesTitleText:Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

.field private textRecord:Lcom/intsig/office/fc/hssf/record/chart/TextRecord;

.field final synthetic this$0:Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->this$0:Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->series:Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private getCellRange(Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;)Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;
    .locals 8

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    return-object p1

    .line 5
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->getFormulaOfLink()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    array-length v0, p1

    .line 10
    const/4 v1, 0x0

    .line 11
    const/4 v2, 0x0

    .line 12
    const/4 v3, 0x0

    .line 13
    const/4 v4, 0x0

    .line 14
    const/4 v5, 0x0

    .line 15
    :goto_0
    if-ge v1, v0, :cond_2

    .line 16
    .line 17
    aget-object v6, p1, v1

    .line 18
    .line 19
    instance-of v7, v6, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;

    .line 20
    .line 21
    if-eqz v7, :cond_1

    .line 22
    .line 23
    check-cast v6, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;

    .line 24
    .line 25
    invoke-virtual {v6}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    invoke-virtual {v6}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastRow()I

    .line 30
    .line 31
    .line 32
    move-result v3

    .line 33
    invoke-virtual {v6}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    invoke-virtual {v6}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastColumn()I

    .line 38
    .line 39
    .line 40
    move-result v5

    .line 41
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_2
    new-instance p1, Lcom/intsig/office/fc/hssf/util/CellRangeAddress;

    .line 45
    .line 46
    invoke-direct {p1, v2, v3, v4, v5}, Lcom/intsig/office/fc/hssf/util/CellRangeAddress;-><init>(IIII)V

    .line 47
    .line 48
    .line 49
    return-object p1
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private setVerticalCellRange(Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;)Ljava/lang/Integer;
    .locals 8

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    return-object p1

    .line 5
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p2}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    invoke-virtual {p2}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    sub-int/2addr v1, v2

    .line 19
    add-int/lit8 v1, v1, 0x1

    .line 20
    .line 21
    invoke-virtual {p2}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    invoke-virtual {p2}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    sub-int/2addr v2, v3

    .line 30
    add-int/lit8 v2, v2, 0x1

    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->getFormulaOfLink()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    array-length v4, v3

    .line 37
    const/4 v5, 0x0

    .line 38
    :goto_0
    if-ge v5, v4, :cond_2

    .line 39
    .line 40
    aget-object v6, v3, v5

    .line 41
    .line 42
    instance-of v7, v6, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;

    .line 43
    .line 44
    if-eqz v7, :cond_1

    .line 45
    .line 46
    check-cast v6, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;

    .line 47
    .line 48
    invoke-virtual {p2}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 49
    .line 50
    .line 51
    move-result v7

    .line 52
    invoke-virtual {v6, v7}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setFirstRow(I)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p2}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 56
    .line 57
    .line 58
    move-result v7

    .line 59
    invoke-virtual {v6, v7}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setLastRow(I)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {p2}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 63
    .line 64
    .line 65
    move-result v7

    .line 66
    invoke-virtual {v6, v7}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setFirstColumn(I)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {p2}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    .line 70
    .line 71
    .line 72
    move-result v7

    .line 73
    invoke-virtual {v6, v7}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setLastColumn(I)V

    .line 74
    .line 75
    .line 76
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    .line 78
    .line 79
    :cond_1
    add-int/lit8 v5, v5, 0x1

    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 83
    .line 84
    .line 85
    move-result p2

    .line 86
    new-array p2, p2, [Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 87
    .line 88
    invoke-interface {v0, p2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 89
    .line 90
    .line 91
    move-result-object p2

    .line 92
    check-cast p2, [Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 93
    .line 94
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setFormulaOfLink([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 95
    .line 96
    .line 97
    mul-int v1, v1, v2

    .line 98
    .line 99
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    return-object p1
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->seriesTitleText:Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public getAreaFormat()Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->areaFormatRecord:Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCategoryLabelsCellRange()Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->dataCategoryLabels:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->getCellRange(Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;)Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDataCategoryLabels()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->dataCategoryLabels:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDataLabelExtensionRecord()Lcom/intsig/office/fc/hssf/record/chart/DataLabelExtensionRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->dleRec:Lcom/intsig/office/fc/hssf/record/chart/DataLabelExtensionRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDataName()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->dataName:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDataSecondaryCategoryLabels()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->dataSecondaryCategoryLabels:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDataValues()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->dataValues:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNumValues()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->series:Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;->getNumValues()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSeries()Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->series:Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSeriesTextRecord()Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->seriesTitleText:Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSeriesTitle()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->seriesTitleText:Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;->getText()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTextRecord()Lcom/intsig/office/fc/hssf/record/chart/TextRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->textRecord:Lcom/intsig/office/fc/hssf/record/chart/TextRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getValueType()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->series:Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;->getValuesDataType()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getValuesCellRange()Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->dataValues:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->getCellRange(Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;)Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method insertData(Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->getLinkType()B

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    if-eq v0, v1, :cond_2

    .line 9
    .line 10
    const/4 v1, 0x2

    .line 11
    if-eq v0, v1, :cond_1

    .line 12
    .line 13
    const/4 v1, 0x3

    .line 14
    if-eq v0, v1, :cond_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->dataSecondaryCategoryLabels:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->dataCategoryLabels:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_2
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->dataValues:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_3
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->dataName:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 27
    .line 28
    :goto_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setAreaFormat(Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->areaFormatRecord:Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCategoryLabelsCellRange(Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->dataCategoryLabels:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 2
    .line 3
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->setVerticalCellRange(Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-nez p1, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->series:Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 11
    .line 12
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    int-to-short p1, p1

    .line 17
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;->setNumCategories(S)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDataLabelExtensionRecord(Lcom/intsig/office/fc/hssf/record/chart/DataLabelExtensionRecord;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->dleRec:Lcom/intsig/office/fc/hssf/record/chart/DataLabelExtensionRecord;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSeriesTitle(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->seriesTitleText:Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;->setText(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 10
    .line 11
    const-string v0, "No series title found to change"

    .line 12
    .line 13
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    throw p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method setSeriesTitleText(Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->seriesTitleText:Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setTextRecord(Lcom/intsig/office/fc/hssf/record/chart/TextRecord;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->textRecord:Lcom/intsig/office/fc/hssf/record/chart/TextRecord;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setValuesCellRange(Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->dataValues:Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 2
    .line 3
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->setVerticalCellRange(Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-nez p1, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->series:Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 11
    .line 12
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    int-to-short p1, p1

    .line 17
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;->setNumValues(S)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
.end method

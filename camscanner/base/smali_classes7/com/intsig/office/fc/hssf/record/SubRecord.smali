.class public abstract Lcom/intsig/office/fc/hssf/record/SubRecord;
.super Ljava/lang/Object;
.source "SubRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/record/SubRecord$UnknownSubRecord;
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static createSubRecord(Lcom/intsig/office/fc/util/LittleEndianInput;I)Lcom/intsig/office/fc/hssf/record/SubRecord;
    .locals 3

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-interface {p0}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-eqz v0, :cond_6

    .line 10
    .line 11
    const/4 v2, 0x6

    .line 12
    if-eq v0, v2, :cond_5

    .line 13
    .line 14
    const/16 v2, 0x9

    .line 15
    .line 16
    if-eq v0, v2, :cond_4

    .line 17
    .line 18
    const/16 v2, 0x13

    .line 19
    .line 20
    if-eq v0, v2, :cond_3

    .line 21
    .line 22
    const/16 p1, 0x15

    .line 23
    .line 24
    if-eq v0, p1, :cond_2

    .line 25
    .line 26
    const/16 p1, 0xc

    .line 27
    .line 28
    if-eq v0, p1, :cond_1

    .line 29
    .line 30
    const/16 p1, 0xd

    .line 31
    .line 32
    if-eq v0, p1, :cond_0

    .line 33
    .line 34
    new-instance p1, Lcom/intsig/office/fc/hssf/record/SubRecord$UnknownSubRecord;

    .line 35
    .line 36
    invoke-direct {p1, p0, v0, v1}, Lcom/intsig/office/fc/hssf/record/SubRecord$UnknownSubRecord;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;II)V

    .line 37
    .line 38
    .line 39
    return-object p1

    .line 40
    :cond_0
    new-instance p1, Lcom/intsig/office/fc/hssf/record/NoteStructureSubRecord;

    .line 41
    .line 42
    invoke-direct {p1, p0, v1}, Lcom/intsig/office/fc/hssf/record/NoteStructureSubRecord;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;I)V

    .line 43
    .line 44
    .line 45
    return-object p1

    .line 46
    :cond_1
    new-instance p1, Lcom/intsig/office/fc/hssf/record/FtCblsSubRecord;

    .line 47
    .line 48
    invoke-direct {p1, p0, v1}, Lcom/intsig/office/fc/hssf/record/FtCblsSubRecord;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;I)V

    .line 49
    .line 50
    .line 51
    return-object p1

    .line 52
    :cond_2
    new-instance p1, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;

    .line 53
    .line 54
    invoke-direct {p1, p0, v1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;I)V

    .line 55
    .line 56
    .line 57
    return-object p1

    .line 58
    :cond_3
    new-instance v0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;

    .line 59
    .line 60
    invoke-direct {v0, p0, v1, p1}, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;II)V

    .line 61
    .line 62
    .line 63
    return-object v0

    .line 64
    :cond_4
    new-instance p1, Lcom/intsig/office/fc/hssf/record/EmbeddedObjectRefSubRecord;

    .line 65
    .line 66
    invoke-direct {p1, p0, v1}, Lcom/intsig/office/fc/hssf/record/EmbeddedObjectRefSubRecord;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;I)V

    .line 67
    .line 68
    .line 69
    return-object p1

    .line 70
    :cond_5
    new-instance p1, Lcom/intsig/office/fc/hssf/record/GroupMarkerSubRecord;

    .line 71
    .line 72
    invoke-direct {p1, p0, v1}, Lcom/intsig/office/fc/hssf/record/GroupMarkerSubRecord;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;I)V

    .line 73
    .line 74
    .line 75
    return-object p1

    .line 76
    :cond_6
    new-instance p1, Lcom/intsig/office/fc/hssf/record/EndSubRecord;

    .line 77
    .line 78
    invoke-direct {p1, p0, v1}, Lcom/intsig/office/fc/hssf/record/EndSubRecord;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;I)V

    .line 79
    .line 80
    .line 81
    return-object p1
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method


# virtual methods
.method public abstract clone()Ljava/lang/Object;
.end method

.method protected abstract getDataSize()I
.end method

.method public isTerminating()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public abstract serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
.end method

.method public serialize()[B
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/SubRecord;->getDataSize()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    add-int/lit8 v0, v0, 0x4

    .line 6
    .line 7
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    .line 8
    .line 9
    invoke-direct {v1, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 10
    .line 11
    .line 12
    new-instance v2, Lcom/intsig/office/fc/util/LittleEndianOutputStream;

    .line 13
    .line 14
    invoke-direct {v2, v1}, Lcom/intsig/office/fc/util/LittleEndianOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hssf/record/SubRecord;->serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->size()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    if-ne v2, v0, :cond_0

    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    return-object v0

    .line 31
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    .line 32
    .line 33
    const-string v1, "write size mismatch"

    .line 34
    .line 35
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    throw v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

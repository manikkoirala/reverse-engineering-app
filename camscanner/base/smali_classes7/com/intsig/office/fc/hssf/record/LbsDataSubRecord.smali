.class public Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;
.super Lcom/intsig/office/fc/hssf/record/SubRecord;
.source "LbsDataSubRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;
    }
.end annotation


# static fields
.field public static final sid:I = 0x13


# instance fields
.field private _bsels:[Z

.field private _cLines:I

.field private _cbFContinued:I

.field private _dropData:Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;

.field private _flags:I

.field private _iSel:I

.field private _idEdit:I

.field private _linkPtg:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

.field private _rgLines:[Ljava/lang/String;

.field private _unknownPostFormulaByte:Ljava/lang/Byte;

.field private _unknownPreFormulaInt:I


# direct methods
.method constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/SubRecord;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/util/LittleEndianInput;II)V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/SubRecord;-><init>()V

    .line 2
    iput p2, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_cbFContinued:I

    .line 3
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    move-result p2

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-lez p2, :cond_3

    .line 4
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    move-result v2

    .line 5
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readInt()I

    move-result v3

    iput v3, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_unknownPreFormulaInt:I

    .line 6
    invoke-static {v2, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->readTokens(ILcom/intsig/office/fc/util/LittleEndianInput;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    move-result-object v3

    .line 7
    array-length v4, v3

    if-ne v4, v1, :cond_2

    .line 8
    aget-object v3, v3, v0

    iput-object v3, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_linkPtg:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    sub-int/2addr p2, v2

    add-int/lit8 p2, p2, -0x6

    if-eqz p2, :cond_1

    if-ne p2, v1, :cond_0

    .line 9
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readByte()B

    move-result p2

    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_unknownPostFormulaByte:Ljava/lang/Byte;

    goto :goto_0

    .line 10
    :cond_0
    new-instance p1, Lcom/intsig/office/fc/hssf/record/RecordFormatException;

    const-string p2, "Unexpected leftover bytes"

    invoke-direct {p1, p2}, Lcom/intsig/office/fc/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const/4 p2, 0x0

    .line 11
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_unknownPostFormulaByte:Ljava/lang/Byte;

    goto :goto_0

    .line 12
    :cond_2
    new-instance p1, Lcom/intsig/office/fc/hssf/record/RecordFormatException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Read "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p3, v3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, " tokens but expected exactly 1"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/intsig/office/fc/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 13
    :cond_3
    :goto_0
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    move-result p2

    iput p2, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_cLines:I

    .line 14
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    move-result p2

    iput p2, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_iSel:I

    .line 15
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    move-result p2

    iput p2, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_flags:I

    .line 16
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    move-result p2

    iput p2, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_idEdit:I

    const/16 p2, 0x14

    if-ne p3, p2, :cond_4

    .line 17
    new-instance p2, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;

    invoke-direct {p2, p1}, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    iput-object p2, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_dropData:Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;

    .line 18
    :cond_4
    iget p2, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_flags:I

    and-int/lit8 p2, p2, 0x2

    if-eqz p2, :cond_5

    .line 19
    iget p2, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_cLines:I

    new-array p2, p2, [Ljava/lang/String;

    iput-object p2, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_rgLines:[Ljava/lang/String;

    const/4 p2, 0x0

    .line 20
    :goto_1
    iget p3, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_cLines:I

    if-ge p2, p3, :cond_5

    .line 21
    iget-object p3, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_rgLines:[Ljava/lang/String;

    invoke-static {p1}, Lcom/intsig/office/fc/util/StringUtil;->readUnicodeString(Lcom/intsig/office/fc/util/LittleEndianInput;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, p3, p2

    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    .line 22
    :cond_5
    iget p2, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_flags:I

    shr-int/lit8 p2, p2, 0x4

    and-int/lit8 p2, p2, 0x2

    if-eqz p2, :cond_7

    .line 23
    iget p2, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_cLines:I

    new-array p2, p2, [Z

    iput-object p2, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_bsels:[Z

    const/4 p2, 0x0

    .line 24
    :goto_2
    iget p3, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_cLines:I

    if-ge p2, p3, :cond_7

    .line 25
    iget-object p3, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_bsels:[Z

    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readByte()B

    move-result v2

    if-ne v2, v1, :cond_6

    const/4 v2, 0x1

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    :goto_3
    aput-boolean v2, p3, p2

    add-int/lit8 p2, p2, 0x1

    goto :goto_2

    :cond_7
    return-void
.end method

.method public static newAutoFilterInstance()Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0x1fee

    .line 7
    .line 8
    iput v1, v0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_cbFContinued:I

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    iput v1, v0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_iSel:I

    .line 12
    .line 13
    const/16 v1, 0x301

    .line 14
    .line 15
    iput v1, v0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_flags:I

    .line 16
    .line 17
    new-instance v1, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;

    .line 18
    .line 19
    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;-><init>()V

    .line 20
    .line 21
    .line 22
    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_dropData:Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;

    .line 23
    .line 24
    sget v2, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;->STYLE_COMBO_SIMPLE_DROPDOWN:I

    .line 25
    .line 26
    invoke-static {v1, v2}, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;I)V

    .line 27
    .line 28
    .line 29
    iget-object v1, v0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_dropData:Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;

    .line 30
    .line 31
    const/16 v2, 0x8

    .line 32
    .line 33
    invoke-static {v1, v2}, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;->〇080(Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;I)V

    .line 34
    .line 35
    .line 36
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 0

    .line 1
    return-object p0
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getDataSize()I
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_linkPtg:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 2
    .line 3
    const/16 v1, 0x8

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getSize()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    add-int/2addr v0, v1

    .line 12
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_unknownPostFormulaByte:Ljava/lang/Byte;

    .line 13
    .line 14
    if-eqz v2, :cond_1

    .line 15
    .line 16
    add-int/lit8 v0, v0, 0x1

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v0, 0x2

    .line 20
    :cond_1
    :goto_0
    add-int/2addr v0, v1

    .line 21
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_dropData:Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;

    .line 22
    .line 23
    if-eqz v1, :cond_2

    .line 24
    .line 25
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;->getDataSize()I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    add-int/2addr v0, v1

    .line 30
    :cond_2
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_rgLines:[Ljava/lang/String;

    .line 31
    .line 32
    if-eqz v1, :cond_3

    .line 33
    .line 34
    array-length v2, v1

    .line 35
    const/4 v3, 0x0

    .line 36
    :goto_1
    if-ge v3, v2, :cond_3

    .line 37
    .line 38
    aget-object v4, v1, v3

    .line 39
    .line 40
    invoke-static {v4}, Lcom/intsig/office/fc/util/StringUtil;->getEncodedSize(Ljava/lang/String;)I

    .line 41
    .line 42
    .line 43
    move-result v4

    .line 44
    add-int/2addr v0, v4

    .line 45
    add-int/lit8 v3, v3, 0x1

    .line 46
    .line 47
    goto :goto_1

    .line 48
    :cond_3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_bsels:[Z

    .line 49
    .line 50
    if-eqz v1, :cond_4

    .line 51
    .line 52
    array-length v1, v1

    .line 53
    add-int/2addr v0, v1

    .line 54
    :cond_4
    return v0
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getFormula()Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_linkPtg:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNumberOfItems()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_cLines:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isTerminating()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 5

    .line 1
    const/16 v0, 0x13

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 4
    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_cbFContinued:I

    .line 7
    .line 8
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_linkPtg:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getSize()I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    add-int/lit8 v2, v0, 0x6

    .line 25
    .line 26
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_unknownPostFormulaByte:Ljava/lang/Byte;

    .line 27
    .line 28
    if-eqz v3, :cond_1

    .line 29
    .line 30
    add-int/lit8 v2, v2, 0x1

    .line 31
    .line 32
    :cond_1
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 33
    .line 34
    .line 35
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 36
    .line 37
    .line 38
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_unknownPreFormulaInt:I

    .line 39
    .line 40
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeInt(I)V

    .line 41
    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_linkPtg:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 44
    .line 45
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->write(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_unknownPostFormulaByte:Ljava/lang/Byte;

    .line 49
    .line 50
    if-eqz v0, :cond_2

    .line 51
    .line 52
    invoke-virtual {v0}, Ljava/lang/Byte;->intValue()I

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeByte(I)V

    .line 57
    .line 58
    .line 59
    :cond_2
    :goto_0
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_cLines:I

    .line 60
    .line 61
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 62
    .line 63
    .line 64
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_iSel:I

    .line 65
    .line 66
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 67
    .line 68
    .line 69
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_flags:I

    .line 70
    .line 71
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 72
    .line 73
    .line 74
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_idEdit:I

    .line 75
    .line 76
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 77
    .line 78
    .line 79
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_dropData:Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;

    .line 80
    .line 81
    if-eqz v0, :cond_3

    .line 82
    .line 83
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;->serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 84
    .line 85
    .line 86
    :cond_3
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_rgLines:[Ljava/lang/String;

    .line 87
    .line 88
    if-eqz v0, :cond_4

    .line 89
    .line 90
    array-length v2, v0

    .line 91
    const/4 v3, 0x0

    .line 92
    :goto_1
    if-ge v3, v2, :cond_4

    .line 93
    .line 94
    aget-object v4, v0, v3

    .line 95
    .line 96
    invoke-static {p1, v4}, Lcom/intsig/office/fc/util/StringUtil;->writeUnicodeString(Lcom/intsig/office/fc/util/LittleEndianOutput;Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    add-int/lit8 v3, v3, 0x1

    .line 100
    .line 101
    goto :goto_1

    .line 102
    :cond_4
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_bsels:[Z

    .line 103
    .line 104
    if-eqz v0, :cond_5

    .line 105
    .line 106
    array-length v2, v0

    .line 107
    :goto_2
    if-ge v1, v2, :cond_5

    .line 108
    .line 109
    aget-boolean v3, v0, v1

    .line 110
    .line 111
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeByte(I)V

    .line 112
    .line 113
    .line 114
    add-int/lit8 v1, v1, 0x1

    .line 115
    .line 116
    goto :goto_2

    .line 117
    :cond_5
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    const/16 v1, 0x100

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 6
    .line 7
    .line 8
    const-string v1, "[ftLbsData]\n"

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 11
    .line 12
    .line 13
    const-string v1, "    .unknownShort1 ="

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 16
    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_cbFContinued:I

    .line 19
    .line 20
    invoke-static {v1}, Lcom/intsig/office/fc/util/HexDump;->shortToHex(I)[C

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 25
    .line 26
    .line 27
    const-string v1, "\n"

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 30
    .line 31
    .line 32
    const-string v2, "    .formula        = "

    .line 33
    .line 34
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 35
    .line 36
    .line 37
    const/16 v2, 0xa

    .line 38
    .line 39
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 40
    .line 41
    .line 42
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_linkPtg:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 43
    .line 44
    if-eqz v3, :cond_0

    .line 45
    .line 46
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->toString()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 51
    .line 52
    .line 53
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_linkPtg:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 54
    .line 55
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getRVAType()C

    .line 56
    .line 57
    .line 58
    move-result v3

    .line 59
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 63
    .line 64
    .line 65
    :cond_0
    const-string v3, "    .nEntryCount   ="

    .line 66
    .line 67
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 68
    .line 69
    .line 70
    iget v3, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_cLines:I

    .line 71
    .line 72
    invoke-static {v3}, Lcom/intsig/office/fc/util/HexDump;->shortToHex(I)[C

    .line 73
    .line 74
    .line 75
    move-result-object v3

    .line 76
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 80
    .line 81
    .line 82
    const-string v3, "    .selEntryIx    ="

    .line 83
    .line 84
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 85
    .line 86
    .line 87
    iget v3, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_iSel:I

    .line 88
    .line 89
    invoke-static {v3}, Lcom/intsig/office/fc/util/HexDump;->shortToHex(I)[C

    .line 90
    .line 91
    .line 92
    move-result-object v3

    .line 93
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 97
    .line 98
    .line 99
    const-string v3, "    .style         ="

    .line 100
    .line 101
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 102
    .line 103
    .line 104
    iget v3, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_flags:I

    .line 105
    .line 106
    invoke-static {v3}, Lcom/intsig/office/fc/util/HexDump;->shortToHex(I)[C

    .line 107
    .line 108
    .line 109
    move-result-object v3

    .line 110
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 111
    .line 112
    .line 113
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 114
    .line 115
    .line 116
    const-string v3, "    .unknownShort10="

    .line 117
    .line 118
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 119
    .line 120
    .line 121
    iget v3, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_idEdit:I

    .line 122
    .line 123
    invoke-static {v3}, Lcom/intsig/office/fc/util/HexDump;->shortToHex(I)[C

    .line 124
    .line 125
    .line 126
    move-result-object v3

    .line 127
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 128
    .line 129
    .line 130
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 131
    .line 132
    .line 133
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_dropData:Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;

    .line 134
    .line 135
    if-eqz v1, :cond_1

    .line 136
    .line 137
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 138
    .line 139
    .line 140
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord;->_dropData:Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;

    .line 141
    .line 142
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/LbsDataSubRecord$LbsDropData;->toString()Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 147
    .line 148
    .line 149
    :cond_1
    const-string v1, "[/ftLbsData]\n"

    .line 150
    .line 151
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 152
    .line 153
    .line 154
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v0

    .line 158
    return-object v0
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

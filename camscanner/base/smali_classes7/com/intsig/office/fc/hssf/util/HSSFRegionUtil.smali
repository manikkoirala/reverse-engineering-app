.class public final Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;
.super Ljava/lang/Object;
.source "HSSFRegionUtil.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static setBorderBottom(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0

    .line 2
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/ss/util/RegionUtil;->setBorderBottom(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/ss/usermodel/Sheet;Lcom/intsig/office/fc/ss/usermodel/Workbook;)V

    return-void
.end method

.method public static setBorderBottom(SLcom/intsig/office/fc/ss/util/Region;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->toCRA(Lcom/intsig/office/fc/ss/util/Region;)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    move-result-object p1

    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->setBorderBottom(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V

    return-void
.end method

.method public static setBorderLeft(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0

    .line 2
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/ss/util/RegionUtil;->setBorderLeft(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/ss/usermodel/Sheet;Lcom/intsig/office/fc/ss/usermodel/Workbook;)V

    return-void
.end method

.method public static setBorderLeft(SLcom/intsig/office/fc/ss/util/Region;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->toCRA(Lcom/intsig/office/fc/ss/util/Region;)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    move-result-object p1

    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->setBorderLeft(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V

    return-void
.end method

.method public static setBorderRight(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0

    .line 2
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/ss/util/RegionUtil;->setBorderRight(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/ss/usermodel/Sheet;Lcom/intsig/office/fc/ss/usermodel/Workbook;)V

    return-void
.end method

.method public static setBorderRight(SLcom/intsig/office/fc/ss/util/Region;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->toCRA(Lcom/intsig/office/fc/ss/util/Region;)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    move-result-object p1

    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->setBorderRight(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V

    return-void
.end method

.method public static setBorderTop(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0

    .line 2
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/ss/util/RegionUtil;->setBorderTop(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/ss/usermodel/Sheet;Lcom/intsig/office/fc/ss/usermodel/Workbook;)V

    return-void
.end method

.method public static setBorderTop(SLcom/intsig/office/fc/ss/util/Region;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->toCRA(Lcom/intsig/office/fc/ss/util/Region;)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    move-result-object p1

    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->setBorderTop(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V

    return-void
.end method

.method public static setBottomBorderColor(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0

    .line 2
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/ss/util/RegionUtil;->setBottomBorderColor(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/ss/usermodel/Sheet;Lcom/intsig/office/fc/ss/usermodel/Workbook;)V

    return-void
.end method

.method public static setBottomBorderColor(SLcom/intsig/office/fc/ss/util/Region;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->toCRA(Lcom/intsig/office/fc/ss/util/Region;)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    move-result-object p1

    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->setBottomBorderColor(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V

    return-void
.end method

.method public static setLeftBorderColor(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0

    .line 2
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/ss/util/RegionUtil;->setLeftBorderColor(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/ss/usermodel/Sheet;Lcom/intsig/office/fc/ss/usermodel/Workbook;)V

    return-void
.end method

.method public static setLeftBorderColor(SLcom/intsig/office/fc/ss/util/Region;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->toCRA(Lcom/intsig/office/fc/ss/util/Region;)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    move-result-object p1

    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->setLeftBorderColor(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V

    return-void
.end method

.method public static setRightBorderColor(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0

    .line 2
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/ss/util/RegionUtil;->setRightBorderColor(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/ss/usermodel/Sheet;Lcom/intsig/office/fc/ss/usermodel/Workbook;)V

    return-void
.end method

.method public static setRightBorderColor(SLcom/intsig/office/fc/ss/util/Region;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->toCRA(Lcom/intsig/office/fc/ss/util/Region;)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    move-result-object p1

    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->setRightBorderColor(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V

    return-void
.end method

.method public static setTopBorderColor(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0

    .line 2
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/ss/util/RegionUtil;->setTopBorderColor(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/ss/usermodel/Sheet;Lcom/intsig/office/fc/ss/usermodel/Workbook;)V

    return-void
.end method

.method public static setTopBorderColor(SLcom/intsig/office/fc/ss/util/Region;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->toCRA(Lcom/intsig/office/fc/ss/util/Region;)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    move-result-object p1

    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/util/HSSFRegionUtil;->setTopBorderColor(ILcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V

    return-void
.end method

.method private static toCRA(Lcom/intsig/office/fc/ss/util/Region;)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/ss/util/Region;->convertToCellRangeAddress(Lcom/intsig/office/fc/ss/util/Region;)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

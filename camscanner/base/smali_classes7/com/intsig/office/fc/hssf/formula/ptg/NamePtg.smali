.class public final Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;
.super Lcom/intsig/office/fc/hssf/formula/ptg/OperandPtg;
.source "NamePtg.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/WorkbookDependentFormula;


# static fields
.field private static final SIZE:I = 0x5

.field public static final sid:S = 0x23s


# instance fields
.field private field_1_label_index:I

.field private field_2_zero:S


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/OperandPtg;-><init>()V

    add-int/lit8 p1, p1, 0x1

    .line 2
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;->field_1_label_index:I

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V
    .locals 1

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/OperandPtg;-><init>()V

    .line 4
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readShort()S

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;->field_1_label_index:I

    .line 5
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readShort()S

    move-result p1

    iput-short p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;->field_2_zero:S

    return-void
.end method


# virtual methods
.method public getDefaultOperandClass()B
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;->field_1_label_index:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, -0x1

    .line 4
    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSize()I
    .locals 1

    .line 1
    const/4 v0, 0x5

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public toFormulaString()Ljava/lang/String;
    .locals 2

    .line 2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "3D references need a workbook to determine formula text"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toFormulaString(Lcom/intsig/office/fc/hssf/formula/FormulaRenderingWorkbook;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/hssf/formula/FormulaRenderingWorkbook;->getNameText(Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getPtgClass()B

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    add-int/lit8 v0, v0, 0x23

    .line 6
    .line 7
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeByte(I)V

    .line 8
    .line 9
    .line 10
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;->field_1_label_index:I

    .line 11
    .line 12
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 13
    .line 14
    .line 15
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;->field_2_zero:S

    .line 16
    .line 17
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public Lcom/intsig/office/fc/hssf/formula/function/Weekday;
.super Lcom/intsig/office/fc/hssf/formula/function/Var1or2ArgFunction;
.source "Weekday.java"


# static fields
.field private static final DEFAULT_ARG1:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 2
    .line 3
    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    .line 4
    .line 5
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/Weekday;->DEFAULT_ARG1:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/function/Var1or2ArgFunction;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/function/Weekday;->DEFAULT_ARG1:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/intsig/office/fc/hssf/formula/function/Weekday;->evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    return-object p1
.end method

.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 2

    .line 2
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;->WEEKDAY:Lcom/intsig/office/fc/hssf/formula/function/Function;

    check-cast v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;

    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;->evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    .line 3
    instance-of p2, p4, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    if-eqz p2, :cond_4

    .line 4
    check-cast p4, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 5
    move-object p2, p1

    check-cast p2, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;->getNumberValue()D

    move-result-wide p2

    invoke-static {p2, p3}, Ljava/lang/Math;->round(D)J

    move-result-wide p2

    long-to-int p3, p2

    .line 6
    invoke-virtual {p4}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;->getNumberValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int p2, v0

    const/4 p4, 0x1

    if-eq p2, p4, :cond_4

    const/4 p1, 0x2

    if-eq p2, p1, :cond_2

    const/4 p4, 0x3

    if-eq p2, p4, :cond_0

    const/16 p1, 0x24

    .line 7
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->valueOf(I)Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object p1

    goto :goto_1

    :cond_0
    sub-int/2addr p3, p1

    if-ltz p3, :cond_1

    goto :goto_0

    :cond_1
    const/4 p3, 0x6

    .line 8
    :goto_0
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    int-to-double p2, p3

    invoke-direct {p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    goto :goto_1

    :cond_2
    sub-int/2addr p3, p4

    if-nez p3, :cond_3

    const/4 p3, 0x7

    .line 9
    :cond_3
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    int-to-double p2, p3

    invoke-direct {p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    :cond_4
    :goto_1
    return-object p1
.end method

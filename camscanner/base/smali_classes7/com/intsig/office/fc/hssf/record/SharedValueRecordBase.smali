.class public abstract Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;
.super Lcom/intsig/office/fc/hssf/record/StandardRecord;
.source "SharedValueRecordBase.java"


# instance fields
.field private _range:Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;


# direct methods
.method protected constructor <init>()V
    .locals 2

    .line 4
    new-instance v0, Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;-><init>(IIII)V

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;-><init>(Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;)V

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    if-eqz p1, :cond_0

    .line 2
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;->_range:Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    return-void

    .line 3
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "range must be supplied."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V
    .locals 1

    .line 5
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    .line 6
    new-instance v0, Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;->_range:Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    return-void
.end method


# virtual methods
.method protected getDataSize()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;->getExtraDataSize()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    add-int/lit8 v0, v0, 0x6

    .line 6
    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected abstract getExtraDataSize()I
.end method

.method public final getFirstColumn()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;->_range:Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getFirstRow()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;->_range:Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getLastColumn()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;->_range:Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getLastRow()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;->_range:Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getRange()Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;->_range:Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final isFirstCell(II)Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;->getRange()Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-ne v1, p1, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-ne p1, p2, :cond_0

    .line 16
    .line 17
    const/4 p1, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    :goto_0
    return p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public final isInRange(II)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;->_range:Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-gt v1, p1, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-lt v1, p1, :cond_0

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-gt p1, p2, :cond_0

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    if-lt p1, p2, :cond_0

    .line 26
    .line 27
    const/4 p1, 0x1

    .line 28
    goto :goto_0

    .line 29
    :cond_0
    const/4 p1, 0x0

    .line 30
    :goto_0
    return p1
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;->_range:Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;->serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/SharedValueRecordBase;->serializeExtraData(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method protected abstract serializeExtraData(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
.end method

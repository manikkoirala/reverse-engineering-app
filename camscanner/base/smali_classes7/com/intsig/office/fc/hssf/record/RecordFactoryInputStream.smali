.class public final Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;
.super Ljava/lang/Object;
.source "RecordFactoryInputStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;
    }
.end annotation


# instance fields
.field private _bofDepth:I

.field private _lastDrawingRecord:Lcom/intsig/office/fc/hssf/record/DrawingRecord;

.field private _lastRecord:Lcom/intsig/office/fc/hssf/record/Record;

.field private _lastRecordWasEOFLevelZero:Z

.field private _recStream:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

.field private final _shouldIncludeContinueRecords:Z

.field private _unreadRecordBuffer:[Lcom/intsig/office/fc/hssf/record/Record;

.field private _unreadRecordIndex:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Z)V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, -0x1

    .line 5
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastRecord:Lcom/intsig/office/fc/hssf/record/Record;

    .line 9
    .line 10
    new-instance v0, Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 11
    .line 12
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/DrawingRecord;-><init>()V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastDrawingRecord:Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 16
    .line 17
    new-instance v0, Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 18
    .line 19
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;-><init>(Ljava/io/InputStream;)V

    .line 20
    .line 21
    .line 22
    new-instance v1, Ljava/util/ArrayList;

    .line 23
    .line 24
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 25
    .line 26
    .line 27
    new-instance v2, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;

    .line 28
    .line 29
    invoke-direct {v2, v0, v1}, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;-><init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;Ljava/util/List;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->O8()Z

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    if-eqz v3, :cond_0

    .line 37
    .line 38
    invoke-virtual {v2, p1}, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->〇080(Ljava/io/InputStream;)Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    const/4 v3, 0x0

    .line 47
    if-nez p1, :cond_1

    .line 48
    .line 49
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    new-array p1, p1, [Lcom/intsig/office/fc/hssf/record/Record;

    .line 54
    .line 55
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lcom/intsig/office/fc/hssf/record/Record;

    .line 56
    .line 57
    invoke-interface {v1, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    iput v3, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 61
    .line 62
    :cond_1
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_recStream:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 63
    .line 64
    iput-boolean p2, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_shouldIncludeContinueRecords:Z

    .line 65
    .line 66
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->〇o00〇〇Oo()Lcom/intsig/office/fc/hssf/record/Record;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastRecord:Lcom/intsig/office/fc/hssf/record/Record;

    .line 71
    .line 72
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->〇o〇()Z

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    .line 77
    .line 78
    iput-boolean v3, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastRecordWasEOFLevelZero:Z

    .line 79
    .line 80
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private getNextUnreadRecord()Lcom/intsig/office/fc/hssf/record/Record;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lcom/intsig/office/fc/hssf/record/Record;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    iget v2, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 7
    .line 8
    array-length v3, v0

    .line 9
    if-ge v2, v3, :cond_0

    .line 10
    .line 11
    aget-object v0, v0, v2

    .line 12
    .line 13
    add-int/lit8 v2, v2, 0x1

    .line 14
    .line 15
    iput v2, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 16
    .line 17
    return-object v0

    .line 18
    :cond_0
    const/4 v0, -0x1

    .line 19
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 20
    .line 21
    iput-object v1, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lcom/intsig/office/fc/hssf/record/Record;

    .line 22
    .line 23
    :cond_1
    return-object v1
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private readNextRecord()Lcom/intsig/office/fc/hssf/record/Record;
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_recStream:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/RecordFactory;->createSingleRecord(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)Lcom/intsig/office/fc/hssf/record/Record;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    iput-boolean v1, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastRecordWasEOFLevelZero:Z

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastDrawingRecord:Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 11
    .line 12
    const/16 v3, 0x3c

    .line 13
    .line 14
    const/4 v4, 0x0

    .line 15
    if-eqz v2, :cond_0

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    if-eq v2, v3, :cond_0

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    const/16 v5, 0x5d

    .line 28
    .line 29
    if-eq v2, v5, :cond_0

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    const/16 v5, 0x1b6

    .line 36
    .line 37
    if-eq v2, v5, :cond_0

    .line 38
    .line 39
    iput-object v4, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastDrawingRecord:Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 40
    .line 41
    :cond_0
    instance-of v2, v0, Lcom/intsig/office/fc/hssf/record/BOFRecord;

    .line 42
    .line 43
    const/4 v5, 0x1

    .line 44
    if-eqz v2, :cond_1

    .line 45
    .line 46
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    .line 47
    .line 48
    add-int/2addr v1, v5

    .line 49
    iput v1, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    .line 50
    .line 51
    return-object v0

    .line 52
    :cond_1
    instance-of v2, v0, Lcom/intsig/office/fc/hssf/record/EOFRecord;

    .line 53
    .line 54
    if-eqz v2, :cond_3

    .line 55
    .line 56
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    .line 57
    .line 58
    sub-int/2addr v1, v5

    .line 59
    iput v1, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    .line 60
    .line 61
    if-ge v1, v5, :cond_2

    .line 62
    .line 63
    iput-boolean v5, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastRecordWasEOFLevelZero:Z

    .line 64
    .line 65
    :cond_2
    return-object v0

    .line 66
    :cond_3
    instance-of v2, v0, Lcom/intsig/office/fc/hssf/record/DBCellRecord;

    .line 67
    .line 68
    if-eqz v2, :cond_4

    .line 69
    .line 70
    return-object v4

    .line 71
    :cond_4
    instance-of v2, v0, Lcom/intsig/office/fc/hssf/record/RKRecord;

    .line 72
    .line 73
    if-eqz v2, :cond_5

    .line 74
    .line 75
    check-cast v0, Lcom/intsig/office/fc/hssf/record/RKRecord;

    .line 76
    .line 77
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/RecordFactory;->convertToNumberRecord(Lcom/intsig/office/fc/hssf/record/RKRecord;)Lcom/intsig/office/fc/hssf/record/NumberRecord;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    return-object v0

    .line 82
    :cond_5
    instance-of v2, v0, Lcom/intsig/office/fc/hssf/record/MulRKRecord;

    .line 83
    .line 84
    if-eqz v2, :cond_6

    .line 85
    .line 86
    check-cast v0, Lcom/intsig/office/fc/hssf/record/MulRKRecord;

    .line 87
    .line 88
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/record/RecordFactory;->convertRKRecords(Lcom/intsig/office/fc/hssf/record/MulRKRecord;)[Lcom/intsig/office/fc/hssf/record/NumberRecord;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lcom/intsig/office/fc/hssf/record/Record;

    .line 93
    .line 94
    iput v5, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 95
    .line 96
    aget-object v0, v0, v1

    .line 97
    .line 98
    return-object v0

    .line 99
    :cond_6
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 100
    .line 101
    .line 102
    move-result v1

    .line 103
    const/16 v2, 0xeb

    .line 104
    .line 105
    if-ne v1, v2, :cond_7

    .line 106
    .line 107
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastRecord:Lcom/intsig/office/fc/hssf/record/Record;

    .line 108
    .line 109
    instance-of v2, v1, Lcom/intsig/office/fc/hssf/record/DrawingGroupRecord;

    .line 110
    .line 111
    if-eqz v2, :cond_7

    .line 112
    .line 113
    check-cast v1, Lcom/intsig/office/fc/hssf/record/DrawingGroupRecord;

    .line 114
    .line 115
    check-cast v0, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;

    .line 116
    .line 117
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->join(Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;)V

    .line 118
    .line 119
    .line 120
    return-object v4

    .line 121
    :cond_7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 122
    .line 123
    .line 124
    move-result v1

    .line 125
    if-ne v1, v3, :cond_10

    .line 126
    .line 127
    move-object v1, v0

    .line 128
    check-cast v1, Lcom/intsig/office/fc/hssf/record/ContinueRecord;

    .line 129
    .line 130
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastRecord:Lcom/intsig/office/fc/hssf/record/Record;

    .line 131
    .line 132
    instance-of v3, v2, Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 133
    .line 134
    if-nez v3, :cond_d

    .line 135
    .line 136
    instance-of v3, v2, Lcom/intsig/office/fc/hssf/record/TextObjectRecord;

    .line 137
    .line 138
    if-eqz v3, :cond_8

    .line 139
    .line 140
    goto :goto_0

    .line 141
    :cond_8
    instance-of v3, v2, Lcom/intsig/office/fc/hssf/record/DrawingGroupRecord;

    .line 142
    .line 143
    if-eqz v3, :cond_9

    .line 144
    .line 145
    check-cast v2, Lcom/intsig/office/fc/hssf/record/DrawingGroupRecord;

    .line 146
    .line 147
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/ContinueRecord;->getData()[B

    .line 148
    .line 149
    .line 150
    move-result-object v0

    .line 151
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->processContinueRecord([B)V

    .line 152
    .line 153
    .line 154
    return-object v4

    .line 155
    :cond_9
    instance-of v3, v2, Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 156
    .line 157
    if-eqz v3, :cond_a

    .line 158
    .line 159
    check-cast v2, Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 160
    .line 161
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/ContinueRecord;->getData()[B

    .line 162
    .line 163
    .line 164
    move-result-object v0

    .line 165
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/hssf/record/DrawingRecord;->processContinueRecord([B)V

    .line 166
    .line 167
    .line 168
    return-object v4

    .line 169
    :cond_a
    instance-of v1, v2, Lcom/intsig/office/fc/hssf/record/UnknownRecord;

    .line 170
    .line 171
    if-eqz v1, :cond_b

    .line 172
    .line 173
    return-object v0

    .line 174
    :cond_b
    instance-of v1, v2, Lcom/intsig/office/fc/hssf/record/EOFRecord;

    .line 175
    .line 176
    if-eqz v1, :cond_c

    .line 177
    .line 178
    return-object v0

    .line 179
    :cond_c
    new-instance v0, Lcom/intsig/office/fc/hssf/record/RecordFormatException;

    .line 180
    .line 181
    new-instance v1, Ljava/lang/StringBuilder;

    .line 182
    .line 183
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    .line 185
    .line 186
    const-string v2, "Unhandled Continue Record followining "

    .line 187
    .line 188
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    .line 190
    .line 191
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastRecord:Lcom/intsig/office/fc/hssf/record/Record;

    .line 192
    .line 193
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 194
    .line 195
    .line 196
    move-result-object v2

    .line 197
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 198
    .line 199
    .line 200
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 201
    .line 202
    .line 203
    move-result-object v1

    .line 204
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    throw v0

    .line 208
    :cond_d
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastDrawingRecord:Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 209
    .line 210
    if-eqz v2, :cond_e

    .line 211
    .line 212
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/ContinueRecord;->getData()[B

    .line 213
    .line 214
    .line 215
    move-result-object v3

    .line 216
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/hssf/record/DrawingRecord;->processContinueRecord([B)V

    .line 217
    .line 218
    .line 219
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/ContinueRecord;->resetData()V

    .line 220
    .line 221
    .line 222
    :cond_e
    iget-boolean v1, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_shouldIncludeContinueRecords:Z

    .line 223
    .line 224
    if-eqz v1, :cond_f

    .line 225
    .line 226
    return-object v0

    .line 227
    :cond_f
    return-object v4

    .line 228
    :cond_10
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastRecord:Lcom/intsig/office/fc/hssf/record/Record;

    .line 229
    .line 230
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 231
    .line 232
    if-eqz v1, :cond_11

    .line 233
    .line 234
    move-object v1, v0

    .line 235
    check-cast v1, Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 236
    .line 237
    iput-object v1, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastDrawingRecord:Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 238
    .line 239
    :cond_11
    return-object v0
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_recStream:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lcom/intsig/office/fc/hssf/record/Record;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastRecord:Lcom/intsig/office/fc/hssf/record/Record;

    .line 7
    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastDrawingRecord:Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public nextRecord()Lcom/intsig/office/fc/hssf/record/Record;
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->getNextUnreadRecord()Lcom/intsig/office/fc/hssf/record/Record;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-object v0

    .line 8
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_recStream:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->hasNextRecord()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    const/4 v1, 0x0

    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    return-object v1

    .line 18
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_lastRecordWasEOFLevelZero:Z

    .line 19
    .line 20
    if-eqz v0, :cond_2

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_recStream:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->getNextSid()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    const/16 v2, 0x809

    .line 29
    .line 30
    if-eq v0, v2, :cond_2

    .line 31
    .line 32
    return-object v1

    .line 33
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->_recStream:Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->nextRecord()V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->readNextRecord()Lcom/intsig/office/fc/hssf/record/Record;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    if-nez v0, :cond_3

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_3
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.class public final Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;
.super Ljava/lang/Object;
.source "ContinuableRecordOutput.java"

# interfaces
.implements Lcom/intsig/office/fc/util/LittleEndianOutput;


# static fields
.field private static final NOPOutput:Lcom/intsig/office/fc/util/LittleEndianOutput;


# instance fields
.field private final _out:Lcom/intsig/office/fc/util/LittleEndianOutput;

.field private _totalPreviousRecordsSize:I

.field private _ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->NOPOutput:Lcom/intsig/office/fc/util/LittleEndianOutput;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/office/fc/util/LittleEndianOutput;I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 5
    .line 6
    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;-><init>(Lcom/intsig/office/fc/util/LittleEndianOutput;I)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_out:Lcom/intsig/office/fc/util/LittleEndianOutput;

    .line 12
    .line 13
    const/4 p1, 0x0

    .line 14
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_totalPreviousRecordsSize:I

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static createForCountingOnly()Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->NOPOutput:Lcom/intsig/office/fc/util/LittleEndianOutput;

    .line 4
    .line 5
    const/16 v2, -0x309

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;-><init>(Lcom/intsig/office/fc/util/LittleEndianOutput;I)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private writeCharacterData(Ljava/lang/String;Z)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    const/4 v2, 0x0

    .line 7
    if-eqz p2, :cond_2

    .line 8
    .line 9
    :goto_0
    sub-int p2, v0, v2

    .line 10
    .line 11
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 12
    .line 13
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->〇080()I

    .line 14
    .line 15
    .line 16
    move-result v3

    .line 17
    div-int/lit8 v3, v3, 0x2

    .line 18
    .line 19
    invoke-static {p2, v3}, Ljava/lang/Math;->min(II)I

    .line 20
    .line 21
    .line 22
    move-result p2

    .line 23
    :goto_1
    if-lez p2, :cond_0

    .line 24
    .line 25
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 26
    .line 27
    add-int/lit8 v4, v2, 0x1

    .line 28
    .line 29
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->writeShort(I)V

    .line 34
    .line 35
    .line 36
    add-int/lit8 p2, p2, -0x1

    .line 37
    .line 38
    move v2, v4

    .line 39
    goto :goto_1

    .line 40
    :cond_0
    if-lt v2, v0, :cond_1

    .line 41
    .line 42
    goto :goto_4

    .line 43
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinue()V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeByte(I)V

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_2
    const/4 p2, 0x0

    .line 51
    :goto_2
    sub-int v3, v0, p2

    .line 52
    .line 53
    iget-object v4, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 54
    .line 55
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->〇080()I

    .line 56
    .line 57
    .line 58
    move-result v4

    .line 59
    div-int/2addr v4, v1

    .line 60
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    :goto_3
    if-lez v3, :cond_3

    .line 65
    .line 66
    iget-object v4, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 67
    .line 68
    add-int/lit8 v5, p2, 0x1

    .line 69
    .line 70
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    .line 71
    .line 72
    .line 73
    move-result p2

    .line 74
    invoke-virtual {v4, p2}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->writeByte(I)V

    .line 75
    .line 76
    .line 77
    add-int/lit8 v3, v3, -0x1

    .line 78
    .line 79
    move p2, v5

    .line 80
    goto :goto_3

    .line 81
    :cond_3
    if-lt p2, v0, :cond_4

    .line 82
    .line 83
    :goto_4
    return-void

    .line 84
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinue()V

    .line 85
    .line 86
    .line 87
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeByte(I)V

    .line 88
    .line 89
    .line 90
    goto :goto_2
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method


# virtual methods
.method public getAvailableSpace()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->〇080()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTotalSize()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_totalPreviousRecordsSize:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->〇o00〇〇Oo()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    add-int/2addr v0, v1

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method terminate()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->〇o〇()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public write([B)V
    .locals 1

    .line 1
    array-length v0, p1

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->write([B)V

    return-void
.end method

.method public write([BII)V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    sub-int v1, p3, v0

    .line 3
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->〇080()I

    move-result v2

    div-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    :goto_1
    if-lez v1, :cond_0

    .line 4
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    add-int/lit8 v3, v0, 0x1

    add-int/2addr v0, p2

    aget-byte v0, p1, v0

    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->writeByte(I)V

    add-int/lit8 v1, v1, -0x1

    move v0, v3

    goto :goto_1

    :cond_0
    if-lt v0, p3, :cond_1

    return-void

    .line 5
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinue()V

    goto :goto_0
.end method

.method public writeByte(I)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 3
    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->writeByte(I)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public writeContinue()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->〇o〇()V

    .line 4
    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_totalPreviousRecordsSize:I

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->〇o00〇〇Oo()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    add-int/2addr v0, v1

    .line 15
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_totalPreviousRecordsSize:I

    .line 16
    .line 17
    new-instance v0, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_out:Lcom/intsig/office/fc/util/LittleEndianOutput;

    .line 20
    .line 21
    const/16 v2, 0x3c

    .line 22
    .line 23
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;-><init>(Lcom/intsig/office/fc/util/LittleEndianOutput;I)V

    .line 24
    .line 25
    .line 26
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public writeContinueIfRequired(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->〇080()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-ge v0, p1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinue()V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public writeDouble(D)V
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 7
    .line 8
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->writeDouble(D)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public writeInt(I)V
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 3
    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->writeInt(I)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public writeLong(J)V
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 7
    .line 8
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->writeLong(J)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public writeShort(I)V
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 3
    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cont/UnknownLengthRecordOutput;->writeShort(I)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public writeString(Ljava/lang/String;II)V
    .locals 3

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/util/StringUtil;->hasMultibyte(Ljava/lang/String;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    const/4 v2, 0x5

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v2, 0x4

    .line 11
    const/4 v1, 0x0

    .line 12
    :goto_0
    if-lez p2, :cond_1

    .line 13
    .line 14
    or-int/lit8 v1, v1, 0x8

    .line 15
    .line 16
    add-int/lit8 v2, v2, 0x2

    .line 17
    .line 18
    :cond_1
    if-lez p3, :cond_2

    .line 19
    .line 20
    or-int/lit8 v1, v1, 0x4

    .line 21
    .line 22
    add-int/lit8 v2, v2, 0x4

    .line 23
    .line 24
    :cond_2
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeByte(I)V

    .line 35
    .line 36
    .line 37
    if-lez p2, :cond_3

    .line 38
    .line 39
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 40
    .line 41
    .line 42
    :cond_3
    if-lez p3, :cond_4

    .line 43
    .line 44
    invoke-virtual {p0, p3}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeInt(I)V

    .line 45
    .line 46
    .line 47
    :cond_4
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeCharacterData(Ljava/lang/String;Z)V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public writeStringData(Ljava/lang/String;)V
    .locals 3

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/util/StringUtil;->hasMultibyte(Ljava/lang/String;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    const/4 v2, 0x3

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v2, 0x2

    .line 11
    const/4 v1, 0x0

    .line 12
    :goto_0
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeByte(I)V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/record/cont/ContinuableRecordOutput;->writeCharacterData(Ljava/lang/String;Z)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
.end method

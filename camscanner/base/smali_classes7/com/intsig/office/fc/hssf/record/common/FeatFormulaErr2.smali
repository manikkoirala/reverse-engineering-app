.class public final Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;
.super Ljava/lang/Object;
.source "FeatFormulaErr2.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/record/common/SharedFeature;


# static fields
.field static checkCalculationErrors:Lcom/intsig/office/fc/util/BitField;

.field static checkDateTimeFormats:Lcom/intsig/office/fc/util/BitField;

.field static checkEmptyCellRef:Lcom/intsig/office/fc/util/BitField;

.field static checkInconsistentFormulas:Lcom/intsig/office/fc/util/BitField;

.field static checkInconsistentRanges:Lcom/intsig/office/fc/util/BitField;

.field static checkNumbersAsText:Lcom/intsig/office/fc/util/BitField;

.field static checkUnprotectedFormulas:Lcom/intsig/office/fc/util/BitField;

.field static performDataValidation:Lcom/intsig/office/fc/util/BitField;


# instance fields
.field private errorCheck:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    sput-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkCalculationErrors:Lcom/intsig/office/fc/util/BitField;

    .line 7
    .line 8
    const/4 v0, 0x2

    .line 9
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    sput-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkEmptyCellRef:Lcom/intsig/office/fc/util/BitField;

    .line 14
    .line 15
    const/4 v0, 0x4

    .line 16
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    sput-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkNumbersAsText:Lcom/intsig/office/fc/util/BitField;

    .line 21
    .line 22
    const/16 v0, 0x8

    .line 23
    .line 24
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    sput-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkInconsistentRanges:Lcom/intsig/office/fc/util/BitField;

    .line 29
    .line 30
    const/16 v0, 0x10

    .line 31
    .line 32
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    sput-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkInconsistentFormulas:Lcom/intsig/office/fc/util/BitField;

    .line 37
    .line 38
    const/16 v0, 0x20

    .line 39
    .line 40
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    sput-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkDateTimeFormats:Lcom/intsig/office/fc/util/BitField;

    .line 45
    .line 46
    const/16 v0, 0x40

    .line 47
    .line 48
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    sput-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkUnprotectedFormulas:Lcom/intsig/office/fc/util/BitField;

    .line 53
    .line 54
    const/16 v0, 0x80

    .line 55
    .line 56
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    sput-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->performDataValidation:Lcom/intsig/office/fc/util/BitField;

    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readInt()I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    return-void
.end method


# virtual methods
.method public _getRawErrorCheckValue()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCheckCalculationErrors()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkCalculationErrors:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCheckDateTimeFormats()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkDateTimeFormats:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCheckEmptyCellRef()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkEmptyCellRef:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCheckInconsistentFormulas()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkInconsistentFormulas:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCheckInconsistentRanges()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkInconsistentRanges:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCheckNumbersAsText()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkNumbersAsText:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCheckUnprotectedFormulas()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkUnprotectedFormulas:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDataSize()I
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPerformDataValidation()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->performDataValidation:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeInt(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCheckCalculationErrors(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkCalculationErrors:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCheckDateTimeFormats(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkDateTimeFormats:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCheckEmptyCellRef(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkEmptyCellRef:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCheckInconsistentFormulas(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkInconsistentFormulas:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCheckInconsistentRanges(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkInconsistentRanges:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCheckNumbersAsText(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkNumbersAsText:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCheckUnprotectedFormulas(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->checkUnprotectedFormulas:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPerformDataValidation(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->performDataValidation:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;->errorCheck:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, " [FEATURE FORMULA ERRORS]\n"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 9
    .line 10
    .line 11
    const-string v1, "  checkCalculationErrors    = "

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 14
    .line 15
    .line 16
    const-string v1, "  checkEmptyCellRef         = "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 19
    .line 20
    .line 21
    const-string v1, "  checkNumbersAsText        = "

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 24
    .line 25
    .line 26
    const-string v1, "  checkInconsistentRanges   = "

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 29
    .line 30
    .line 31
    const-string v1, "  checkInconsistentFormulas = "

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 34
    .line 35
    .line 36
    const-string v1, "  checkDateTimeFormats      = "

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 39
    .line 40
    .line 41
    const-string v1, "  checkUnprotectedFormulas  = "

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 44
    .line 45
    .line 46
    const-string v1, "  performDataValidation     = "

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 49
    .line 50
    .line 51
    const-string v1, " [/FEATURE FORMULA ERRORS]\n"

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    return-object v0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

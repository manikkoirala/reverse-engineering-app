.class public Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;
.super Ljava/lang/Object;
.source "EventWorkbookBuilder.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/eventusermodel/HSSFListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SheetRecordCollectingListener"
.end annotation


# instance fields
.field private boundSheetRecords:Ljava/util/List;

.field private childListener:Lcom/intsig/office/fc/hssf/eventusermodel/HSSFListener;

.field private externSheetRecords:Ljava/util/List;

.field private sstRecord:Lcom/intsig/office/fc/hssf/record/SSTRecord;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->boundSheetRecords:Ljava/util/List;

    .line 10
    .line 11
    new-instance v0, Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->externSheetRecords:Ljava/util/List;

    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->sstRecord:Lcom/intsig/office/fc/hssf/record/SSTRecord;

    .line 20
    .line 21
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->childListener:Lcom/intsig/office/fc/hssf/eventusermodel/HSSFListener;

    .line 22
    .line 23
    return-void
    .line 24
.end method


# virtual methods
.method public getBoundSheetRecords()[Lcom/intsig/office/fc/hssf/record/BoundSheetRecord;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->boundSheetRecords:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    new-array v1, v1, [Lcom/intsig/office/fc/hssf/record/BoundSheetRecord;

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, [Lcom/intsig/office/fc/hssf/record/BoundSheetRecord;

    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getExternSheetRecords()[Lcom/intsig/office/fc/hssf/record/ExternSheetRecord;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->externSheetRecords:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    new-array v1, v1, [Lcom/intsig/office/fc/hssf/record/ExternSheetRecord;

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, [Lcom/intsig/office/fc/hssf/record/ExternSheetRecord;

    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSSTRecord()Lcom/intsig/office/fc/hssf/record/SSTRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->sstRecord:Lcom/intsig/office/fc/hssf/record/SSTRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStubHSSFWorkbook()Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->getStubWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->create(Lcom/intsig/office/fc/hssf/model/InternalWorkbook;)Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStubWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->getExternSheetRecords()[Lcom/intsig/office/fc/hssf/record/ExternSheetRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->getBoundSheetRecords()[Lcom/intsig/office/fc/hssf/record/BoundSheetRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->getSSTRecord()Lcom/intsig/office/fc/hssf/record/SSTRecord;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder;->createStubWorkbook([Lcom/intsig/office/fc/hssf/record/ExternSheetRecord;[Lcom/intsig/office/fc/hssf/record/BoundSheetRecord;Lcom/intsig/office/fc/hssf/record/SSTRecord;)Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public processRecord(Lcom/intsig/office/fc/hssf/record/Record;)V
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->processRecordInternally(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->childListener:Lcom/intsig/office/fc/hssf/eventusermodel/HSSFListener;

    .line 5
    .line 6
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/hssf/eventusermodel/HSSFListener;->processRecord(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public processRecordInternally(Lcom/intsig/office/fc/hssf/record/Record;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/record/BoundSheetRecord;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->boundSheetRecords:Ljava/util/List;

    .line 6
    .line 7
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/record/ExternSheetRecord;

    .line 12
    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->externSheetRecords:Ljava/util/List;

    .line 16
    .line 17
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_1
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/record/SSTRecord;

    .line 22
    .line 23
    if-eqz v0, :cond_2

    .line 24
    .line 25
    check-cast p1, Lcom/intsig/office/fc/hssf/record/SSTRecord;

    .line 26
    .line 27
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;->sstRecord:Lcom/intsig/office/fc/hssf/record/SSTRecord;

    .line 28
    .line 29
    :cond_2
    :goto_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

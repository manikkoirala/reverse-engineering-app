.class final Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;
.super Ljava/lang/Object;
.source "Offset.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/formula/function/Offset;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "LinearOffsetRange"
.end annotation


# instance fields
.field private final 〇080:I

.field private final 〇o00〇〇Oo:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    if-eqz p2, :cond_0

    .line 5
    .line 6
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;->〇080:I

    .line 7
    .line 8
    iput p2, p0, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;->〇o00〇〇Oo:I

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 12
    .line 13
    const-string p2, "length may not be zero"

    .line 14
    .line 15
    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    throw p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public O8(I)Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;->〇o00〇〇Oo:I

    .line 2
    .line 3
    if-lez v0, :cond_1

    .line 4
    .line 5
    if-nez p1, :cond_0

    .line 6
    .line 7
    return-object p0

    .line 8
    :cond_0
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;

    .line 9
    .line 10
    iget v2, p0, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;->〇080:I

    .line 11
    .line 12
    add-int/2addr p1, v2

    .line 13
    invoke-direct {v1, p1, v0}, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;-><init>(II)V

    .line 14
    .line 15
    .line 16
    return-object v1

    .line 17
    :cond_1
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;

    .line 18
    .line 19
    iget v2, p0, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;->〇080:I

    .line 20
    .line 21
    add-int/2addr p1, v2

    .line 22
    add-int/2addr p1, v0

    .line 23
    add-int/lit8 p1, p1, 0x1

    .line 24
    .line 25
    neg-int v0, v0

    .line 26
    invoke-direct {v1, p1, v0}, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;-><init>(II)V

    .line 27
    .line 28
    .line 29
    return-object v1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    const/16 v1, 0x40

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 6
    .line 7
    .line 8
    const-class v1, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;

    .line 9
    .line 10
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 15
    .line 16
    .line 17
    const-string v1, " ["

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 20
    .line 21
    .line 22
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;->〇080:I

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 25
    .line 26
    .line 27
    const-string v1, "..."

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;->〇o00〇〇Oo()S

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 37
    .line 38
    .line 39
    const-string v1, "]"

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    return-object v0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇080()S
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;->〇080:I

    .line 2
    .line 3
    int-to-short v0, v0

    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo()S
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;->〇080:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;->〇o00〇〇Oo:I

    .line 4
    .line 5
    add-int/2addr v0, v1

    .line 6
    add-int/lit8 v0, v0, -0x1

    .line 7
    .line 8
    int-to-short v0, v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇(II)Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;->〇080:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ge v0, p1, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/function/Offset$LinearOffsetRange;->〇o00〇〇Oo()S

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    if-le p1, p2, :cond_1

    .line 12
    .line 13
    return v1

    .line 14
    :cond_1
    const/4 p1, 0x0

    .line 15
    return p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

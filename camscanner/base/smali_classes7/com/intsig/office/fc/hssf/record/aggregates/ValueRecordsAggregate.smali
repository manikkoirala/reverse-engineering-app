.class public final Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;
.super Ljava/lang/Object;
.source "ValueRecordsAggregate.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate$ValueIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;",
        ">;"
    }
.end annotation


# static fields
.field private static final INDEX_NOT_SET:I = -0x1

.field private static final MAX_ROW_INDEX:I = 0xffff


# instance fields
.field private firstcell:I

.field private lastcell:I

.field private records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v0, 0x1e

    new-array v0, v0, [[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    const/4 v1, -0x1

    .line 1
    invoke-direct {p0, v1, v1, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;-><init>(II[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V

    return-void
.end method

.method private constructor <init>(II[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->firstcell:I

    .line 4
    iput p2, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->lastcell:I

    .line 5
    iput-object p3, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    return-void
.end method

.method private static countBlanks([Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;I)I
    .locals 2

    .line 1
    move v0, p1

    .line 2
    :goto_0
    array-length v1, p0

    .line 3
    if-ge v0, v1, :cond_1

    .line 4
    .line 5
    aget-object v1, p0, v0

    .line 6
    .line 7
    instance-of v1, v1, Lcom/intsig/office/fc/hssf/record/BlankRecord;

    .line 8
    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    goto :goto_1

    .line 12
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_1
    :goto_1
    sub-int/2addr v0, p1

    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private createMBR([Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;II)Lcom/intsig/office/fc/hssf/record/MulBlankRecord;
    .locals 3

    .line 1
    new-array v0, p3, [S

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    :goto_0
    if-ge v1, p3, :cond_0

    .line 5
    .line 6
    add-int v2, p2, v1

    .line 7
    .line 8
    aget-object v2, p1, v2

    .line 9
    .line 10
    check-cast v2, Lcom/intsig/office/fc/hssf/record/BlankRecord;

    .line 11
    .line 12
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->getXFIndex()S

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    aput-short v2, v0, v1

    .line 17
    .line 18
    add-int/lit8 v1, v1, 0x1

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    aget-object p1, p1, p2

    .line 22
    .line 23
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getRow()I

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    new-instance p3, Lcom/intsig/office/fc/hssf/record/MulBlankRecord;

    .line 28
    .line 29
    invoke-direct {p3, p1, p2, v0}, Lcom/intsig/office/fc/hssf/record/MulBlankRecord;-><init>(II[S)V

    .line 30
    .line 31
    .line 32
    return-object p3
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private static getRowSerializedSize([Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)I
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p0, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    const/4 v1, 0x0

    .line 6
    :goto_0
    array-length v2, p0

    .line 7
    if-ge v0, v2, :cond_3

    .line 8
    .line 9
    aget-object v2, p0, v0

    .line 10
    .line 11
    check-cast v2, Lcom/intsig/office/fc/hssf/record/RecordBase;

    .line 12
    .line 13
    const/4 v3, 0x1

    .line 14
    if-nez v2, :cond_1

    .line 15
    .line 16
    goto :goto_1

    .line 17
    :cond_1
    invoke-static {p0, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->countBlanks([Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;I)I

    .line 18
    .line 19
    .line 20
    move-result v4

    .line 21
    if-le v4, v3, :cond_2

    .line 22
    .line 23
    mul-int/lit8 v2, v4, 0x2

    .line 24
    .line 25
    add-int/lit8 v2, v2, 0xa

    .line 26
    .line 27
    add-int/2addr v1, v2

    .line 28
    add-int/lit8 v4, v4, -0x1

    .line 29
    .line 30
    add-int/2addr v0, v4

    .line 31
    goto :goto_1

    .line 32
    :cond_2
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/RecordBase;->getRecordSize()I

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    add-int/2addr v1, v2

    .line 37
    :goto_1
    add-int/2addr v0, v3

    .line 38
    goto :goto_0

    .line 39
    :cond_3
    return v1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;)[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public addMultipleBlanks(Lcom/intsig/office/fc/hssf/record/MulBlankRecord;)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/MulBlankRecord;->getNumColumns()I

    .line 3
    .line 4
    .line 5
    move-result v1

    .line 6
    if-ge v0, v1, :cond_0

    .line 7
    .line 8
    new-instance v1, Lcom/intsig/office/fc/hssf/record/BlankRecord;

    .line 9
    .line 10
    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/BlankRecord;-><init>()V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/MulBlankRecord;->getFirstColumn()I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    add-int/2addr v2, v0

    .line 18
    int-to-short v2, v2

    .line 19
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->setColumn(S)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/MulBlankRecord;->getRow()I

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->setRow(I)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/MulBlankRecord;->getXFAt(I)S

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->setXFIndex(S)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->insertCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V

    .line 37
    .line 38
    .line 39
    add-int/lit8 v0, v0, 0x1

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/RuntimeException;

    .line 2
    .line 3
    const-string v1, "clone() should not be called.  ValueRecordsAggregate should be copied via Sheet.cloneSheet()"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    throw v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public construct(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;Lcom/intsig/office/fc/hssf/model/RecordStream;Lcom/intsig/office/fc/hssf/record/aggregates/SharedValueManager;)V
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/record/FormulaRecord;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/fc/hssf/record/FormulaRecord;

    .line 6
    .line 7
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/model/RecordStream;->peekNextClass()Ljava/lang/Class;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const-class v1, Lcom/intsig/office/fc/hssf/record/StringRecord;

    .line 12
    .line 13
    if-ne v0, v1, :cond_0

    .line 14
    .line 15
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/model/RecordStream;->getNext()Lcom/intsig/office/fc/hssf/record/Record;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    check-cast p2, Lcom/intsig/office/fc/hssf/record/StringRecord;

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 p2, 0x0

    .line 23
    :goto_0
    new-instance v0, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 24
    .line 25
    invoke-direct {v0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;-><init>(Lcom/intsig/office/fc/hssf/record/FormulaRecord;Lcom/intsig/office/fc/hssf/record/StringRecord;Lcom/intsig/office/fc/hssf/record/aggregates/SharedValueManager;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->insertCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V

    .line 29
    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->insertCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V

    .line 33
    .line 34
    .line 35
    :goto_1
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFirstCellNum()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->firstcell:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLastCellNum()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->lastcell:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPhysicalNumberOfCells()I
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    const/4 v2, 0x0

    .line 4
    :goto_0
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 5
    .line 6
    array-length v4, v3

    .line 7
    if-ge v1, v4, :cond_2

    .line 8
    .line 9
    aget-object v3, v3, v1

    .line 10
    .line 11
    if-eqz v3, :cond_1

    .line 12
    .line 13
    const/4 v4, 0x0

    .line 14
    :goto_1
    array-length v5, v3

    .line 15
    if-ge v4, v5, :cond_1

    .line 16
    .line 17
    aget-object v5, v3, v4

    .line 18
    .line 19
    if-eqz v5, :cond_0

    .line 20
    .line 21
    add-int/lit8 v2, v2, 0x1

    .line 22
    .line 23
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_2
    return v2
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getRowCellBlockSize(II)I
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    if-gt p1, p2, :cond_0

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 5
    .line 6
    array-length v2, v1

    .line 7
    if-ge p1, v2, :cond_0

    .line 8
    .line 9
    aget-object v1, v1, p1

    .line 10
    .line 11
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->getRowSerializedSize([Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    add-int/2addr v0, v1

    .line 16
    add-int/lit8 p1, p1, 0x1

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    return v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public getValueRecords()[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    const/4 v2, 0x0

    .line 8
    :goto_0
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 9
    .line 10
    array-length v4, v3

    .line 11
    if-ge v2, v4, :cond_3

    .line 12
    .line 13
    aget-object v3, v3, v2

    .line 14
    .line 15
    if-nez v3, :cond_0

    .line 16
    .line 17
    goto :goto_2

    .line 18
    :cond_0
    const/4 v4, 0x0

    .line 19
    :goto_1
    array-length v5, v3

    .line 20
    if-ge v4, v5, :cond_2

    .line 21
    .line 22
    aget-object v5, v3, v4

    .line 23
    .line 24
    if-eqz v5, :cond_1

    .line 25
    .line 26
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    :cond_1
    add-int/lit8 v4, v4, 0x1

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    new-array v1, v1, [Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 40
    .line 41
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    return-object v1
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public insertCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V
    .locals 6

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getColumn()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getRow()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 10
    .line 11
    array-length v3, v2

    .line 12
    const/4 v4, 0x0

    .line 13
    if-lt v1, v3, :cond_1

    .line 14
    .line 15
    array-length v3, v2

    .line 16
    mul-int/lit8 v3, v3, 0x2

    .line 17
    .line 18
    add-int/lit8 v5, v1, 0x1

    .line 19
    .line 20
    if-ge v3, v5, :cond_0

    .line 21
    .line 22
    move v3, v5

    .line 23
    :cond_0
    new-array v3, v3, [[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 24
    .line 25
    iput-object v3, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 26
    .line 27
    array-length v5, v2

    .line 28
    invoke-static {v2, v4, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 29
    .line 30
    .line 31
    :cond_1
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 32
    .line 33
    aget-object v3, v2, v1

    .line 34
    .line 35
    if-nez v3, :cond_3

    .line 36
    .line 37
    add-int/lit8 v3, v0, 0x1

    .line 38
    .line 39
    const/16 v5, 0xa

    .line 40
    .line 41
    if-ge v3, v5, :cond_2

    .line 42
    .line 43
    const/16 v3, 0xa

    .line 44
    .line 45
    :cond_2
    new-array v3, v3, [Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 46
    .line 47
    aput-object v3, v2, v1

    .line 48
    .line 49
    :cond_3
    array-length v2, v3

    .line 50
    if-lt v0, v2, :cond_5

    .line 51
    .line 52
    array-length v2, v3

    .line 53
    mul-int/lit8 v2, v2, 0x2

    .line 54
    .line 55
    add-int/lit8 v5, v0, 0x1

    .line 56
    .line 57
    if-ge v2, v5, :cond_4

    .line 58
    .line 59
    move v2, v5

    .line 60
    :cond_4
    new-array v2, v2, [Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 61
    .line 62
    array-length v5, v3

    .line 63
    invoke-static {v3, v4, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 64
    .line 65
    .line 66
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 67
    .line 68
    aput-object v2, v3, v1

    .line 69
    .line 70
    move-object v3, v2

    .line 71
    :cond_5
    aput-object p1, v3, v0

    .line 72
    .line 73
    iget p1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->firstcell:I

    .line 74
    .line 75
    const/4 v1, -0x1

    .line 76
    if-lt v0, p1, :cond_6

    .line 77
    .line 78
    if-ne p1, v1, :cond_7

    .line 79
    .line 80
    :cond_6
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->firstcell:I

    .line 81
    .line 82
    :cond_7
    iget p1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->lastcell:I

    .line 83
    .line 84
    if-gt v0, p1, :cond_8

    .line 85
    .line 86
    if-ne p1, v1, :cond_9

    .line 87
    .line 88
    :cond_8
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->lastcell:I

    .line 89
    .line 90
    :cond_9
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate$ValueIterator;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate$ValueIterator;-><init>(Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeAllCellsValuesForRow(I)V
    .locals 4

    .line 1
    const v0, 0xffff

    .line 2
    .line 3
    .line 4
    if-ltz p1, :cond_1

    .line 5
    .line 6
    if-gt p1, v0, :cond_1

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 9
    .line 10
    array-length v1, v0

    .line 11
    if-lt p1, v1, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    const/4 v1, 0x0

    .line 15
    aput-object v1, v0, p1

    .line 16
    .line 17
    return-void

    .line 18
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 19
    .line 20
    new-instance v2, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v3, "Specified rowIndex "

    .line 26
    .line 27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string p1, " is outside the allowable range (0.."

    .line 34
    .line 35
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    const-string p1, ")"

    .line 42
    .line 43
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    throw v1
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public removeCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V
    .locals 3

    .line 1
    if-eqz p1, :cond_3

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getRow()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 8
    .line 9
    array-length v2, v1

    .line 10
    if-ge v0, v2, :cond_2

    .line 11
    .line 12
    aget-object v0, v1, v0

    .line 13
    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getColumn()S

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    array-length v1, v0

    .line 21
    if-ge p1, v1, :cond_0

    .line 22
    .line 23
    const/4 v1, 0x0

    .line 24
    aput-object v1, v0, p1

    .line 25
    .line 26
    return-void

    .line 27
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 28
    .line 29
    const-string v0, "cell column is out of range"

    .line 30
    .line 31
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    throw p1

    .line 35
    :cond_1
    new-instance p1, Ljava/lang/RuntimeException;

    .line 36
    .line 37
    const-string v0, "cell row is already empty"

    .line 38
    .line 39
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    throw p1

    .line 43
    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    .line 44
    .line 45
    const-string v0, "cell row is out of range"

    .line 46
    .line 47
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    throw p1

    .line 51
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 52
    .line 53
    const-string v0, "cell must not be null"

    .line 54
    .line 55
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    throw p1
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public rowHasCells(I)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    const/4 v2, 0x0

    .line 5
    if-lt p1, v1, :cond_0

    .line 6
    .line 7
    return v2

    .line 8
    :cond_0
    aget-object p1, v0, p1

    .line 9
    .line 10
    if-nez p1, :cond_1

    .line 11
    .line 12
    return v2

    .line 13
    :cond_1
    const/4 v0, 0x0

    .line 14
    :goto_0
    array-length v1, p1

    .line 15
    if-ge v0, v1, :cond_3

    .line 16
    .line 17
    aget-object v1, p1, v0

    .line 18
    .line 19
    if-eqz v1, :cond_2

    .line 20
    .line 21
    const/4 p1, 0x1

    .line 22
    return p1

    .line 23
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_3
    return v2
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public updateFormulasAfterRowShift(Lcom/intsig/office/fc/hssf/formula/FormulaShifter;I)V
    .locals 7

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 4
    .line 5
    array-length v3, v2

    .line 6
    if-ge v1, v3, :cond_3

    .line 7
    .line 8
    aget-object v2, v2, v1

    .line 9
    .line 10
    if-nez v2, :cond_0

    .line 11
    .line 12
    goto :goto_2

    .line 13
    :cond_0
    const/4 v3, 0x0

    .line 14
    :goto_1
    array-length v4, v2

    .line 15
    if-ge v3, v4, :cond_2

    .line 16
    .line 17
    aget-object v4, v2, v3

    .line 18
    .line 19
    instance-of v5, v4, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 20
    .line 21
    if-eqz v5, :cond_1

    .line 22
    .line 23
    check-cast v4, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 24
    .line 25
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->getFormulaRecord()Lcom/intsig/office/fc/hssf/record/FormulaRecord;

    .line 26
    .line 27
    .line 28
    move-result-object v4

    .line 29
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->getParsedExpression()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 30
    .line 31
    .line 32
    move-result-object v5

    .line 33
    invoke-virtual {p1, v5, p2}, Lcom/intsig/office/fc/hssf/formula/FormulaShifter;->adjustFormula([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;I)Z

    .line 34
    .line 35
    .line 36
    move-result v6

    .line 37
    if-eqz v6, :cond_1

    .line 38
    .line 39
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->setParsedExpression([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 40
    .line 41
    .line 42
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 43
    .line 44
    goto :goto_1

    .line 45
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_3
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public visitCellsForRow(ILcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->records:[[Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 2
    .line 3
    aget-object v0, v0, p1

    .line 4
    .line 5
    if-eqz v0, :cond_4

    .line 6
    .line 7
    const/4 p1, 0x0

    .line 8
    :goto_0
    array-length v1, v0

    .line 9
    if-ge p1, v1, :cond_3

    .line 10
    .line 11
    aget-object v1, v0, p1

    .line 12
    .line 13
    check-cast v1, Lcom/intsig/office/fc/hssf/record/RecordBase;

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    if-nez v1, :cond_0

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_0
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->countBlanks([Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;I)I

    .line 20
    .line 21
    .line 22
    move-result v3

    .line 23
    if-le v3, v2, :cond_1

    .line 24
    .line 25
    invoke-direct {p0, v0, p1, v3}, Lcom/intsig/office/fc/hssf/record/aggregates/ValueRecordsAggregate;->createMBR([Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;II)Lcom/intsig/office/fc/hssf/record/MulBlankRecord;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-interface {p2, v1}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 30
    .line 31
    .line 32
    add-int/lit8 v3, v3, -0x1

    .line 33
    .line 34
    add-int/2addr p1, v3

    .line 35
    goto :goto_1

    .line 36
    :cond_1
    instance-of v3, v1, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;

    .line 37
    .line 38
    if-eqz v3, :cond_2

    .line 39
    .line 40
    check-cast v1, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;

    .line 41
    .line 42
    invoke-virtual {v1, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;->visitContainedRecords(Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V

    .line 43
    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_2
    check-cast v1, Lcom/intsig/office/fc/hssf/record/Record;

    .line 47
    .line 48
    invoke-interface {p2, v1}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 49
    .line 50
    .line 51
    :goto_1
    add-int/2addr p1, v2

    .line 52
    goto :goto_0

    .line 53
    :cond_3
    return-void

    .line 54
    :cond_4
    new-instance p2, Ljava/lang/IllegalArgumentException;

    .line 55
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    .line 57
    .line 58
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    .line 60
    .line 61
    const-string v1, "Row ["

    .line 62
    .line 63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    const-string p1, "] is empty"

    .line 70
    .line 71
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object p1

    .line 78
    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    throw p2
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

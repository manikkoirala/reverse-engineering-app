.class public final Lcom/intsig/office/fc/hssf/formula/FormulaParser;
.super Ljava/lang/Object;
.source "FormulaParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;,
        Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;,
        Lcom/intsig/office/fc/hssf/formula/FormulaParser$Identifier;
    }
.end annotation


# static fields
.field private static final CELL_REF_PATTERN:Ljava/util/regex/Pattern;

.field private static TAB:C = '\t'


# instance fields
.field private _book:Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;

.field private final _formulaLength:I

.field private final _formulaString:Ljava/lang/String;

.field private _pointer:I

.field private _rootNode:Lcom/intsig/office/fc/hssf/formula/ParseNode;

.field private _sheetIndex:I

.field private _ssVersion:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

.field private look:C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-string v0, "(\\$?[A-Za-z]+)?(\\$?[0-9]+)?"

    .line 2
    .line 3
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->CELL_REF_PATTERN:Ljava/util/regex/Pattern;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 8
    .line 9
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_book:Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;

    .line 10
    .line 11
    if-nez p2, :cond_0

    .line 12
    .line 13
    sget-object p2, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->EXCEL97:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-interface {p2}, Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;->getSpreadsheetVersion()Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    .line 17
    .line 18
    .line 19
    move-result-object p2

    .line 20
    :goto_0
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_ssVersion:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    .line 21
    .line 22
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaLength:I

    .line 27
    .line 28
    iput p3, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_sheetIndex:I

    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private Arguments()[Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 5

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 8
    .line 9
    .line 10
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 11
    .line 12
    const/16 v2, 0x29

    .line 13
    .line 14
    if-ne v1, v2, :cond_0

    .line 15
    .line 16
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ParseNode;->Oo08:[Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 17
    .line 18
    return-object v0

    .line 19
    :cond_0
    const/4 v1, 0x1

    .line 20
    :goto_0
    const/4 v3, 0x1

    .line 21
    :goto_1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 22
    .line 23
    .line 24
    iget-char v4, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 25
    .line 26
    invoke-static {v4}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->isArgumentDelimiter(C)Z

    .line 27
    .line 28
    .line 29
    move-result v4

    .line 30
    if-eqz v4, :cond_3

    .line 31
    .line 32
    if-eqz v3, :cond_1

    .line 33
    .line 34
    new-instance v3, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 35
    .line 36
    sget-object v4, Lcom/intsig/office/fc/hssf/formula/ptg/MissingArgPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 37
    .line 38
    invoke-direct {v3, v4}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 39
    .line 40
    .line 41
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    :cond_1
    iget-char v3, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 45
    .line 46
    if-ne v3, v2, :cond_2

    .line 47
    .line 48
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    new-array v1, v1, [Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 53
    .line 54
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 55
    .line 56
    .line 57
    return-object v1

    .line 58
    :cond_2
    const/16 v3, 0x2c

    .line 59
    .line 60
    invoke-direct {p0, v3}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 61
    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_3
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->comparisonExpression()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 65
    .line 66
    .line 67
    move-result-object v3

    .line 68
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    .line 70
    .line 71
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 72
    .line 73
    .line 74
    iget-char v3, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 75
    .line 76
    invoke-static {v3}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->isArgumentDelimiter(C)Z

    .line 77
    .line 78
    .line 79
    move-result v3

    .line 80
    if-eqz v3, :cond_4

    .line 81
    .line 82
    const/4 v3, 0x0

    .line 83
    goto :goto_1

    .line 84
    :cond_4
    const-string v0, "\',\' or \')\'"

    .line 85
    .line 86
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    throw v0
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private GetChar()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaLength:I

    .line 4
    .line 5
    if-gt v0, v1, :cond_1

    .line 6
    .line 7
    if-ge v0, v1, :cond_0

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    .line 10
    .line 11
    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    iput-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    iput-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 20
    .line 21
    :goto_0
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 22
    .line 23
    add-int/lit8 v0, v0, 0x1

    .line 24
    .line 25
    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 26
    .line 27
    return-void

    .line 28
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    .line 29
    .line 30
    const-string v1, "too far"

    .line 31
    .line 32
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    throw v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private GetNum()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    :goto_0
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 7
    .line 8
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->IsDigit(C)Z

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-nez v1, :cond_1

    .line 28
    .line 29
    const/4 v0, 0x0

    .line 30
    goto :goto_1

    .line 31
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    :goto_1
    return-object v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private static IsAlpha(C)Z
    .locals 1

    .line 1
    invoke-static {p0}, Ljava/lang/Character;->isLetter(C)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    const/16 v0, 0x24

    .line 8
    .line 9
    if-eq p0, v0, :cond_1

    .line 10
    .line 11
    const/16 v0, 0x5f

    .line 12
    .line 13
    if-ne p0, v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 p0, 0x0

    .line 17
    goto :goto_1

    .line 18
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 19
    :goto_1
    return p0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static IsDigit(C)Z
    .locals 0

    .line 1
    invoke-static {p0}, Ljava/lang/Character;->isDigit(C)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static IsWhite(C)Z
    .locals 1

    .line 1
    const/16 v0, 0x20

    .line 2
    .line 3
    if-eq p0, v0, :cond_1

    .line 4
    .line 5
    sget-char v0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->TAB:C

    .line 6
    .line 7
    if-ne p0, v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 13
    :goto_1
    return p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private Match(C)V
    .locals 2

    .line 1
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 2
    .line 3
    if-ne v0, p1, :cond_0

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 6
    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v1, "\'"

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    throw p1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private SkipWhite()V
    .locals 1

    .line 1
    :goto_0
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->IsWhite(C)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private Term()Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->powerFactor()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    :goto_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 6
    .line 7
    .line 8
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 9
    .line 10
    const/16 v2, 0x2a

    .line 11
    .line 12
    if-eq v1, v2, :cond_1

    .line 13
    .line 14
    const/16 v2, 0x2f

    .line 15
    .line 16
    if-eq v1, v2, :cond_0

    .line 17
    .line 18
    return-object v0

    .line 19
    :cond_0
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 20
    .line 21
    .line 22
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/DividePtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 23
    .line 24
    goto :goto_1

    .line 25
    :cond_1
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 26
    .line 27
    .line 28
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/MultiplyPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 29
    .line 30
    :goto_1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->powerFactor()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    new-instance v3, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 35
    .line 36
    invoke-direct {v3, v1, v0, v2}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Lcom/intsig/office/fc/hssf/formula/ParseNode;Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 37
    .line 38
    .line 39
    move-object v0, v3

    .line 40
    goto :goto_0
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private additiveExpression()Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Term()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    :goto_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 6
    .line 7
    .line 8
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 9
    .line 10
    const/16 v2, 0x2b

    .line 11
    .line 12
    if-eq v1, v2, :cond_1

    .line 13
    .line 14
    const/16 v2, 0x2d

    .line 15
    .line 16
    if-eq v1, v2, :cond_0

    .line 17
    .line 18
    return-object v0

    .line 19
    :cond_0
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 20
    .line 21
    .line 22
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/SubtractPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 23
    .line 24
    goto :goto_1

    .line 25
    :cond_1
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 26
    .line 27
    .line 28
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/ptg/AddPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 29
    .line 30
    :goto_1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Term()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    new-instance v3, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 35
    .line 36
    invoke-direct {v3, v1, v0, v2}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Lcom/intsig/office/fc/hssf/formula/ParseNode;Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 37
    .line 38
    .line 39
    move-object v0, v3

    .line 40
    goto :goto_0
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private static augmentWithMemPtg(Lcom/intsig/office/fc/hssf/formula/ParseNode;)Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 2

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->needsMemFunc(Lcom/intsig/office/fc/hssf/formula/ParseNode;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/ptg/MemFuncPtg;

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->O8()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/MemFuncPtg;-><init>(I)V

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/ptg/MemAreaPtg;

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->O8()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/MemAreaPtg;-><init>(I)V

    .line 24
    .line 25
    .line 26
    :goto_0
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 27
    .line 28
    invoke-direct {v1, v0, p0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 29
    .line 30
    .line 31
    return-object v1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private checkRowLengths([[Ljava/lang/Object;I)V
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    array-length v1, p1

    .line 3
    if-ge v0, v1, :cond_1

    .line 4
    .line 5
    aget-object v1, p1, v0

    .line 6
    .line 7
    array-length v1, v1

    .line 8
    if-ne v1, p2, :cond_0

    .line 9
    .line 10
    add-int/lit8 v0, v0, 0x1

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 14
    .line 15
    new-instance v2, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v3, "Array row "

    .line 21
    .line 22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string v0, " has length "

    .line 29
    .line 30
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string v0, " but row 0 has length "

    .line 37
    .line 38
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p2

    .line 48
    invoke-direct {p1, p2}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    throw p1

    .line 52
    :cond_1
    return-void
    .line 53
.end method

.method private static checkValidRangeOperand(Ljava/lang/String;ILcom/intsig/office/fc/hssf/formula/ParseNode;)V
    .locals 2

    .line 1
    invoke-static {p2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->isValidRangeOperand(Lcom/intsig/office/fc/hssf/formula/ParseNode;)Z

    .line 2
    .line 3
    .line 4
    move-result p2

    .line 5
    if-eqz p2, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    new-instance p2, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 9
    .line 10
    new-instance v0, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v1, "The "

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-string p0, " of the range operator \':\' at position "

    .line 24
    .line 25
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string p0, " is not a proper reference."

    .line 32
    .line 33
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p0

    .line 40
    invoke-direct {p2, p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    throw p2
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private comparisonExpression()Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->concatExpression()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    :goto_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 6
    .line 7
    .line 8
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 9
    .line 10
    packed-switch v1, :pswitch_data_0

    .line 11
    .line 12
    .line 13
    return-object v0

    .line 14
    :pswitch_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->getComparisonToken()Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->concatExpression()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    new-instance v3, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 23
    .line 24
    invoke-direct {v3, v1, v0, v2}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Lcom/intsig/office/fc/hssf/formula/ParseNode;Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 25
    .line 26
    .line 27
    move-object v0, v3

    .line 28
    goto :goto_0

    .line 29
    :pswitch_data_0
    .packed-switch 0x3c
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private concatExpression()Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->additiveExpression()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    :goto_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 6
    .line 7
    .line 8
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 9
    .line 10
    const/16 v2, 0x26

    .line 11
    .line 12
    if-eq v1, v2, :cond_0

    .line 13
    .line 14
    return-object v0

    .line 15
    :cond_0
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->additiveExpression()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    new-instance v2, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 23
    .line 24
    sget-object v3, Lcom/intsig/office/fc/hssf/formula/ptg/ConcatPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 25
    .line 26
    invoke-direct {v2, v3, v0, v1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Lcom/intsig/office/fc/hssf/formula/ParseNode;Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 27
    .line 28
    .line 29
    move-object v0, v2

    .line 30
    goto :goto_0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private static convertArrayNumber(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Z)Ljava/lang/Double;
    .locals 2

    .line 1
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/IntPtg;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/ptg/IntPtg;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/IntPtg;->getValue()I

    .line 8
    .line 9
    .line 10
    move-result p0

    .line 11
    int-to-double v0, p0

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;

    .line 14
    .line 15
    if-eqz v0, :cond_2

    .line 16
    .line 17
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;->getValue()D

    .line 20
    .line 21
    .line 22
    move-result-wide v0

    .line 23
    :goto_0
    if-nez p1, :cond_1

    .line 24
    .line 25
    neg-double v0, v0

    .line 26
    :cond_1
    new-instance p0, Ljava/lang/Double;

    .line 27
    .line 28
    invoke-direct {p0, v0, v1}, Ljava/lang/Double;-><init>(D)V

    .line 29
    .line 30
    .line 31
    return-object p0

    .line 32
    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    .line 33
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v1, "Unexpected ptg ("

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 45
    .line 46
    .line 47
    move-result-object p0

    .line 48
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p0

    .line 52
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    const-string p0, ")"

    .line 56
    .line 57
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object p0

    .line 64
    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    throw p1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static createAreaRef(Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;)Lcom/intsig/office/fc/ss/util/AreaReference;
    .locals 3

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->Oo08(Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->o〇0()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇o00〇〇Oo()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇o00〇〇Oo()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    invoke-static {p0, p1}, Lcom/intsig/office/fc/ss/util/AreaReference;->getWholeRow(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/ss/util/AreaReference;

    .line 22
    .line 23
    .line 24
    move-result-object p0

    .line 25
    return-object p0

    .line 26
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->O8()Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eqz v0, :cond_1

    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇o00〇〇Oo()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p0

    .line 36
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇o00〇〇Oo()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    invoke-static {p0, p1}, Lcom/intsig/office/fc/ss/util/AreaReference;->getWholeColumn(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/ss/util/AreaReference;

    .line 41
    .line 42
    .line 43
    move-result-object p0

    .line 44
    return-object p0

    .line 45
    :cond_1
    new-instance v0, Lcom/intsig/office/fc/ss/util/AreaReference;

    .line 46
    .line 47
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇080()Lcom/intsig/office/fc/ss/util/CellReference;

    .line 48
    .line 49
    .line 50
    move-result-object p0

    .line 51
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇080()Lcom/intsig/office/fc/ss/util/CellReference;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/fc/ss/util/AreaReference;-><init>(Lcom/intsig/office/fc/ss/util/CellReference;Lcom/intsig/office/fc/ss/util/CellReference;)V

    .line 56
    .line 57
    .line 58
    return-object v0

    .line 59
    :cond_2
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 60
    .line 61
    new-instance v1, Ljava/lang/StringBuilder;

    .line 62
    .line 63
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 64
    .line 65
    .line 66
    const-string v2, "has incompatible parts: \'"

    .line 67
    .line 68
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇o00〇〇Oo()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object p0

    .line 75
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    const-string p0, "\' and \'"

    .line 79
    .line 80
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇o00〇〇Oo()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object p0

    .line 87
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    const-string p0, "\'."

    .line 91
    .line 92
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object p0

    .line 99
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    throw v0
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private createAreaRefParseNode(Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;)Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/FormulaParseException;
        }
    .end annotation

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/high16 v0, -0x80000000

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;->〇o00〇〇Oo()Lcom/intsig/office/fc/hssf/formula/FormulaParser$Identifier;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$Identifier;->〇080()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;->〇080()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    if-nez v1, :cond_1

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_book:Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;

    .line 21
    .line 22
    invoke-interface {v1, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;->getExternalSheetIndex(Ljava/lang/String;)I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    goto :goto_0

    .line 27
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_book:Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;

    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;->〇080()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    invoke-interface {v1, v2, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;->getExternalSheetIndex(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    :goto_0
    if-nez p3, :cond_3

    .line 38
    .line 39
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇080()Lcom/intsig/office/fc/ss/util/CellReference;

    .line 40
    .line 41
    .line 42
    move-result-object p2

    .line 43
    if-nez p1, :cond_2

    .line 44
    .line 45
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtg;

    .line 46
    .line 47
    invoke-direct {p1, p2}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtg;-><init>(Lcom/intsig/office/fc/ss/util/CellReference;)V

    .line 48
    .line 49
    .line 50
    goto :goto_1

    .line 51
    :cond_2
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;

    .line 52
    .line 53
    invoke-direct {p1, p2, v0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;-><init>(Lcom/intsig/office/fc/ss/util/CellReference;I)V

    .line 54
    .line 55
    .line 56
    goto :goto_1

    .line 57
    :cond_3
    invoke-static {p2, p3}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->createAreaRef(Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;)Lcom/intsig/office/fc/ss/util/AreaReference;

    .line 58
    .line 59
    .line 60
    move-result-object p2

    .line 61
    if-nez p1, :cond_4

    .line 62
    .line 63
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtg;

    .line 64
    .line 65
    invoke-direct {p1, p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtg;-><init>(Lcom/intsig/office/fc/ss/util/AreaReference;)V

    .line 66
    .line 67
    .line 68
    goto :goto_1

    .line 69
    :cond_4
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 70
    .line 71
    invoke-direct {p1, p2, v0}, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;-><init>(Lcom/intsig/office/fc/ss/util/AreaReference;I)V

    .line 72
    .line 73
    .line 74
    :goto_1
    new-instance p2, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 75
    .line 76
    invoke-direct {p2, p1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 77
    .line 78
    .line 79
    return-object p2
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private expected(Ljava/lang/String;)Ljava/lang/RuntimeException;
    .locals 4

    .line 1
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 2
    .line 3
    const/16 v1, 0x3d

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    .line 9
    .line 10
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 11
    .line 12
    sub-int/2addr v1, v2

    .line 13
    const/4 v3, 0x0

    .line 14
    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-ge v0, v2, :cond_0

    .line 27
    .line 28
    new-instance p1, Ljava/lang/StringBuilder;

    .line 29
    .line 30
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    const-string v0, "The specified formula \'"

    .line 34
    .line 35
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    .line 39
    .line 40
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    const-string v0, "\' starts with an equals sign which is not allowed."

    .line 44
    .line 45
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    goto :goto_0

    .line 53
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 54
    .line 55
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .line 57
    .line 58
    const-string v1, "Parse error near char "

    .line 59
    .line 60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 64
    .line 65
    sub-int/2addr v1, v2

    .line 66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    const-string v1, " \'"

    .line 70
    .line 71
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 75
    .line 76
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    const-string v1, "\' in specified formula \'"

    .line 80
    .line 81
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    .line 85
    .line 86
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    const-string v1, "\'. Expected "

    .line 90
    .line 91
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    :goto_0
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 102
    .line 103
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    return-object v0
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private function(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 3

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/ptg/AbstractFunctionPtg;->isBuiltInFunctionName(Ljava/lang/String;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_4

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_book:Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;

    .line 8
    .line 9
    if-eqz v0, :cond_3

    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_sheetIndex:I

    .line 12
    .line 13
    invoke-interface {v0, p1, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;->getName(Ljava/lang/String;I)Lcom/intsig/office/fc/hssf/formula/EvaluationName;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-nez v0, :cond_1

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_book:Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;

    .line 20
    .line 21
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;->getNameXPtg(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    if-eqz v0, :cond_0

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 29
    .line 30
    new-instance v1, Ljava/lang/StringBuilder;

    .line 31
    .line 32
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    const-string v2, "Name \'"

    .line 36
    .line 37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    const-string p1, "\' is completely unknown in the current workbook"

    .line 44
    .line 45
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    throw v0

    .line 56
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/formula/EvaluationName;->isFunctionName()Z

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    if-eqz v1, :cond_2

    .line 61
    .line 62
    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/formula/EvaluationName;->createPtg()Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    goto :goto_0

    .line 67
    :cond_2
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 68
    .line 69
    new-instance v1, Ljava/lang/StringBuilder;

    .line 70
    .line 71
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    .line 73
    .line 74
    const-string v2, "Attempt to use name \'"

    .line 75
    .line 76
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    const-string p1, "\' as a function, but defined name in workbook does not refer to a function"

    .line 83
    .line 84
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    throw v0

    .line 95
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 96
    .line 97
    new-instance v1, Ljava/lang/StringBuilder;

    .line 98
    .line 99
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    .line 101
    .line 102
    const-string v2, "Need book to evaluate name \'"

    .line 103
    .line 104
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    const-string p1, "\'"

    .line 111
    .line 112
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    .line 114
    .line 115
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object p1

    .line 119
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    throw v0

    .line 123
    :cond_4
    const/4 v0, 0x0

    .line 124
    :goto_0
    const/16 v1, 0x28

    .line 125
    .line 126
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 127
    .line 128
    .line 129
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Arguments()[Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 130
    .line 131
    .line 132
    move-result-object v1

    .line 133
    const/16 v2, 0x29

    .line 134
    .line 135
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 136
    .line 137
    .line 138
    invoke-direct {p0, p1, v0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->getFunction(Ljava/lang/String;Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[Lcom/intsig/office/fc/hssf/formula/ParseNode;)Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 139
    .line 140
    .line 141
    move-result-object p1

    .line 142
    return-object p1
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private getComparisonToken()Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 4

    .line 1
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 2
    .line 3
    const/16 v1, 0x3d

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 8
    .line 9
    .line 10
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/EqualPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 11
    .line 12
    return-object v0

    .line 13
    :cond_0
    const/16 v2, 0x3e

    .line 14
    .line 15
    if-ne v0, v2, :cond_1

    .line 16
    .line 17
    const/4 v3, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_1
    const/4 v3, 0x0

    .line 20
    :goto_0
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 21
    .line 22
    .line 23
    if-eqz v3, :cond_3

    .line 24
    .line 25
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 26
    .line 27
    if-ne v0, v1, :cond_2

    .line 28
    .line 29
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 30
    .line 31
    .line 32
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/GreaterEqualPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 33
    .line 34
    return-object v0

    .line 35
    :cond_2
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/GreaterThanPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 36
    .line 37
    return-object v0

    .line 38
    :cond_3
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 39
    .line 40
    if-eq v0, v1, :cond_5

    .line 41
    .line 42
    if-eq v0, v2, :cond_4

    .line 43
    .line 44
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/LessThanPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 45
    .line 46
    return-object v0

    .line 47
    :cond_4
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 48
    .line 49
    .line 50
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/NotEqualPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 51
    .line 52
    return-object v0

    .line 53
    :cond_5
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 54
    .line 55
    .line 56
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/LessEqualPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 57
    .line 58
    return-object v0
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private getFunction(Ljava/lang/String;Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[Lcom/intsig/office/fc/hssf/formula/ParseNode;)Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 5

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->getFunctionByName(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    array-length v1, p3

    .line 10
    const/4 v2, 0x1

    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    if-eqz p2, :cond_0

    .line 14
    .line 15
    add-int/lit8 v0, v1, 0x1

    .line 16
    .line 17
    new-array v3, v0, [Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 18
    .line 19
    new-instance v4, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 20
    .line 21
    invoke-direct {v4, p2}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 22
    .line 23
    .line 24
    const/4 p2, 0x0

    .line 25
    aput-object v4, v3, p2

    .line 26
    .line 27
    invoke-static {p3, p2, v3, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 28
    .line 29
    .line 30
    new-instance p2, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 31
    .line 32
    invoke-static {p1, v0}, Lcom/intsig/office/fc/hssf/formula/ptg/FuncVarPtg;->create(Ljava/lang/String;I)Lcom/intsig/office/fc/hssf/formula/ptg/FuncVarPtg;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    invoke-direct {p2, p1, v3}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 37
    .line 38
    .line 39
    return-object p2

    .line 40
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 41
    .line 42
    const-string p2, "NamePtg must be supplied for external functions"

    .line 43
    .line 44
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    throw p1

    .line 48
    :cond_1
    if-nez p2, :cond_4

    .line 49
    .line 50
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->hasFixedArgsLength()Z

    .line 51
    .line 52
    .line 53
    move-result p2

    .line 54
    xor-int/2addr p2, v2

    .line 55
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->getIndex()I

    .line 56
    .line 57
    .line 58
    move-result v3

    .line 59
    const/4 v4, 0x4

    .line 60
    if-ne v3, v4, :cond_2

    .line 61
    .line 62
    array-length v4, p3

    .line 63
    if-ne v4, v2, :cond_2

    .line 64
    .line 65
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 66
    .line 67
    invoke-static {}, Lcom/intsig/office/fc/hssf/formula/ptg/AttrPtg;->getSumSingle()Lcom/intsig/office/fc/hssf/formula/ptg/AttrPtg;

    .line 68
    .line 69
    .line 70
    move-result-object p2

    .line 71
    invoke-direct {p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 72
    .line 73
    .line 74
    return-object p1

    .line 75
    :cond_2
    array-length v2, p3

    .line 76
    invoke-direct {p0, v2, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->validateNumArgs(ILcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;)V

    .line 77
    .line 78
    .line 79
    if-eqz p2, :cond_3

    .line 80
    .line 81
    invoke-static {p1, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/FuncVarPtg;->create(Ljava/lang/String;I)Lcom/intsig/office/fc/hssf/formula/ptg/FuncVarPtg;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    goto :goto_0

    .line 86
    :cond_3
    invoke-static {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/FuncPtg;->create(I)Lcom/intsig/office/fc/hssf/formula/ptg/FuncPtg;

    .line 87
    .line 88
    .line 89
    move-result-object p1

    .line 90
    :goto_0
    new-instance p2, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 91
    .line 92
    invoke-direct {p2, p1, p3}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 93
    .line 94
    .line 95
    return-object p2

    .line 96
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 97
    .line 98
    const-string p2, "NamePtg no applicable to internal functions"

    .line 99
    .line 100
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    throw p1
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private static getNumberPtgFromString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0x45

    .line 7
    .line 8
    if-nez p1, :cond_2

    .line 9
    .line 10
    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 11
    .line 12
    .line 13
    if-eqz p2, :cond_0

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 19
    .line 20
    .line 21
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object p0

    .line 25
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 26
    .line 27
    .line 28
    move-result p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/ptg/IntPtg;->isInRange(I)Z

    .line 30
    .line 31
    .line 32
    move-result p2

    .line 33
    if-eqz p2, :cond_1

    .line 34
    .line 35
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/IntPtg;

    .line 36
    .line 37
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/IntPtg;-><init>(I)V

    .line 38
    .line 39
    .line 40
    return-object p0

    .line 41
    :cond_1
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;

    .line 42
    .line 43
    invoke-direct {p1, p0}, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;-><init>(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    return-object p1

    .line 47
    :catch_0
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;

    .line 48
    .line 49
    invoke-direct {p1, p0}, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;-><init>(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    return-object p1

    .line 53
    :cond_2
    if-eqz p0, :cond_3

    .line 54
    .line 55
    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 56
    .line 57
    .line 58
    :cond_3
    const/16 p0, 0x2e

    .line 59
    .line 60
    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 64
    .line 65
    .line 66
    if-eqz p2, :cond_4

    .line 67
    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 72
    .line 73
    .line 74
    :cond_4
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;

    .line 75
    .line 76
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;-><init>(Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    return-object p0
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private getRPNPtg(I)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/OperandClassTransformer;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/formula/OperandClassTransformer;-><init>(I)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_rootNode:Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/formula/OperandClassTransformer;->Oo08(Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 9
    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_rootNode:Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 12
    .line 13
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->oO80(Lcom/intsig/office/fc/hssf/formula/ParseNode;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    return-object p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static isArgumentDelimiter(C)Z
    .locals 1

    .line 1
    const/16 v0, 0x2c

    .line 2
    .line 3
    if-eq p0, v0, :cond_1

    .line 4
    .line 5
    const/16 v0, 0x29

    .line 6
    .line 7
    if-ne p0, v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 13
    :goto_1
    return p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static isUnquotedSheetNameChar(C)Z
    .locals 2

    .line 1
    invoke-static {p0}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    const/16 v0, 0x2e

    .line 10
    .line 11
    if-eq p0, v0, :cond_1

    .line 12
    .line 13
    const/16 v0, 0x5f

    .line 14
    .line 15
    if-eq p0, v0, :cond_1

    .line 16
    .line 17
    const/4 p0, 0x0

    .line 18
    return p0

    .line 19
    :cond_1
    return v1
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private isValidCellReference(Ljava/lang/String;)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_ssVersion:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    .line 2
    .line 3
    invoke-static {p1, v0}, Lcom/intsig/office/fc/ss/util/CellReference;->classifyCellReference(Ljava/lang/String;Lcom/intsig/office/fc/ss/SpreadsheetVersion;)Lcom/intsig/office/fc/ss/util/CellReference$NameType;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sget-object v1, Lcom/intsig/office/fc/ss/util/CellReference$NameType;->CELL:Lcom/intsig/office/fc/ss/util/CellReference$NameType;

    .line 8
    .line 9
    const/4 v2, 0x1

    .line 10
    const/4 v3, 0x0

    .line 11
    if-ne v0, v1, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    if-eqz v0, :cond_3

    .line 17
    .line 18
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->getFunctionByName(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    if-eqz v1, :cond_1

    .line 27
    .line 28
    const/4 v1, 0x1

    .line 29
    goto :goto_1

    .line 30
    :cond_1
    const/4 v1, 0x0

    .line 31
    :goto_1
    if-eqz v1, :cond_3

    .line 32
    .line 33
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 34
    .line 35
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    add-int/2addr p1, v0

    .line 40
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->resetPointer(I)V

    .line 41
    .line 42
    .line 43
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 44
    .line 45
    .line 46
    iget-char p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 47
    .line 48
    const/16 v1, 0x28

    .line 49
    .line 50
    if-eq p1, v1, :cond_2

    .line 51
    .line 52
    goto :goto_2

    .line 53
    :cond_2
    const/4 v2, 0x0

    .line 54
    :goto_2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->resetPointer(I)V

    .line 55
    .line 56
    .line 57
    move v0, v2

    .line 58
    :cond_3
    return v0
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private static isValidDefinedNameChar(C)Z
    .locals 2

    .line 1
    invoke-static {p0}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    const/16 v0, 0x2e

    .line 10
    .line 11
    if-eq p0, v0, :cond_1

    .line 12
    .line 13
    const/16 v0, 0x3f

    .line 14
    .line 15
    if-eq p0, v0, :cond_1

    .line 16
    .line 17
    const/16 v0, 0x5c

    .line 18
    .line 19
    if-eq p0, v0, :cond_1

    .line 20
    .line 21
    const/16 v0, 0x5f

    .line 22
    .line 23
    if-eq p0, v0, :cond_1

    .line 24
    .line 25
    const/4 p0, 0x0

    .line 26
    return p0

    .line 27
    :cond_1
    return v1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private static isValidRangeOperand(Lcom/intsig/office/fc/hssf/formula/ParseNode;)Z
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->Oo08()Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/formula/ptg/OperandPtg;

    .line 6
    .line 7
    const/4 v2, 0x1

    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    return v2

    .line 11
    :cond_0
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/formula/ptg/AbstractFunctionPtg;

    .line 12
    .line 13
    const/4 v3, 0x0

    .line 14
    if-eqz v1, :cond_2

    .line 15
    .line 16
    check-cast v0, Lcom/intsig/office/fc/hssf/formula/ptg/AbstractFunctionPtg;

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/ptg/AbstractFunctionPtg;->getDefaultOperandClass()B

    .line 19
    .line 20
    .line 21
    move-result p0

    .line 22
    if-nez p0, :cond_1

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    const/4 v2, 0x0

    .line 26
    :goto_0
    return v2

    .line 27
    :cond_2
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 28
    .line 29
    if-eqz v1, :cond_3

    .line 30
    .line 31
    return v3

    .line 32
    :cond_3
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;

    .line 33
    .line 34
    if-eqz v1, :cond_4

    .line 35
    .line 36
    return v2

    .line 37
    :cond_4
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/formula/ptg/ParenthesisPtg;

    .line 38
    .line 39
    if-eqz v1, :cond_5

    .line 40
    .line 41
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o〇()[Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 42
    .line 43
    .line 44
    move-result-object p0

    .line 45
    aget-object p0, p0, v3

    .line 46
    .line 47
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->isValidRangeOperand(Lcom/intsig/office/fc/hssf/formula/ParseNode;)Z

    .line 48
    .line 49
    .line 50
    move-result p0

    .line 51
    return p0

    .line 52
    :cond_5
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/ErrPtg;->REF_INVALID:Lcom/intsig/office/fc/hssf/formula/ptg/ErrPtg;

    .line 53
    .line 54
    if-ne v0, p0, :cond_6

    .line 55
    .line 56
    return v2

    .line 57
    :cond_6
    return v3
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private static needsMemFunc(Lcom/intsig/office/fc/hssf/formula/ParseNode;)Z
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->Oo08()Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/formula/ptg/AbstractFunctionPtg;

    .line 6
    .line 7
    const/4 v2, 0x1

    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    return v2

    .line 11
    :cond_0
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/formula/ExternSheetReferenceToken;

    .line 12
    .line 13
    if-eqz v1, :cond_1

    .line 14
    .line 15
    return v2

    .line 16
    :cond_1
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;

    .line 17
    .line 18
    if-nez v1, :cond_9

    .line 19
    .line 20
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;

    .line 21
    .line 22
    if-eqz v1, :cond_2

    .line 23
    .line 24
    goto :goto_2

    .line 25
    :cond_2
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;

    .line 26
    .line 27
    const/4 v3, 0x0

    .line 28
    if-nez v1, :cond_6

    .line 29
    .line 30
    instance-of v4, v0, Lcom/intsig/office/fc/hssf/formula/ptg/ParenthesisPtg;

    .line 31
    .line 32
    if-eqz v4, :cond_3

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_3
    instance-of p0, v0, Lcom/intsig/office/fc/hssf/formula/ptg/OperandPtg;

    .line 36
    .line 37
    if-eqz p0, :cond_4

    .line 38
    .line 39
    return v3

    .line 40
    :cond_4
    if-eqz v1, :cond_5

    .line 41
    .line 42
    return v2

    .line 43
    :cond_5
    return v3

    .line 44
    :cond_6
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->〇o〇()[Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 45
    .line 46
    .line 47
    move-result-object p0

    .line 48
    array-length v0, p0

    .line 49
    const/4 v1, 0x0

    .line 50
    :goto_1
    if-ge v1, v0, :cond_8

    .line 51
    .line 52
    aget-object v4, p0, v1

    .line 53
    .line 54
    invoke-static {v4}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->needsMemFunc(Lcom/intsig/office/fc/hssf/formula/ParseNode;)Z

    .line 55
    .line 56
    .line 57
    move-result v4

    .line 58
    if-eqz v4, :cond_7

    .line 59
    .line 60
    return v2

    .line 61
    :cond_7
    add-int/lit8 v1, v1, 0x1

    .line 62
    .line 63
    goto :goto_1

    .line 64
    :cond_8
    return v3

    .line 65
    :cond_9
    :goto_2
    return v2
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private parse()V
    .locals 3

    const/4 v0, 0x0

    .line 4
    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 5
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->unionExpression()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_rootNode:Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 7
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaLength:I

    if-le v0, v1, :cond_0

    return-void

    .line 8
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unused input ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    iget v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] after attempting to parse the formula ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 9
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static parse(Ljava/lang/String;Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;II)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;

    invoke-direct {v0, p0, p1, p3}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;I)V

    .line 2
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parse()V

    .line 3
    invoke-direct {v0, p2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->getRPNPtg(I)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    move-result-object p0

    return-object p0
.end method

.method private parseArray()Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    :goto_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseArrayRow()[Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 11
    .line 12
    .line 13
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 14
    .line 15
    const/16 v2, 0x7d

    .line 16
    .line 17
    if-ne v1, v2, :cond_0

    .line 18
    .line 19
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    new-array v1, v1, [[Ljava/lang/Object;

    .line 24
    .line 25
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    const/4 v0, 0x0

    .line 29
    aget-object v0, v1, v0

    .line 30
    .line 31
    array-length v0, v0

    .line 32
    invoke-direct {p0, v1, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->checkRowLengths([[Ljava/lang/Object;I)V

    .line 33
    .line 34
    .line 35
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 36
    .line 37
    new-instance v2, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg;

    .line 38
    .line 39
    invoke-direct {v2, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg;-><init>([[Ljava/lang/Object;)V

    .line 40
    .line 41
    .line 42
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 43
    .line 44
    .line 45
    return-object v0

    .line 46
    :cond_0
    const/16 v2, 0x3b

    .line 47
    .line 48
    if-ne v1, v2, :cond_1

    .line 49
    .line 50
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_1
    const-string v0, "\'}\' or \';\'"

    .line 55
    .line 56
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    throw v0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private parseArrayItem()Ljava/lang/Object;
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 2
    .line 3
    .line 4
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 5
    .line 6
    const/16 v1, 0x22

    .line 7
    .line 8
    if-eq v0, v1, :cond_3

    .line 9
    .line 10
    const/16 v1, 0x23

    .line 11
    .line 12
    if-eq v0, v1, :cond_2

    .line 13
    .line 14
    const/16 v1, 0x2d

    .line 15
    .line 16
    if-eq v0, v1, :cond_1

    .line 17
    .line 18
    const/16 v1, 0x46

    .line 19
    .line 20
    if-eq v0, v1, :cond_0

    .line 21
    .line 22
    const/16 v1, 0x54

    .line 23
    .line 24
    if-eq v0, v1, :cond_0

    .line 25
    .line 26
    const/16 v1, 0x66

    .line 27
    .line 28
    if-eq v0, v1, :cond_0

    .line 29
    .line 30
    const/16 v1, 0x74

    .line 31
    .line 32
    if-eq v0, v1, :cond_0

    .line 33
    .line 34
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseNumber()Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    const/4 v1, 0x1

    .line 39
    invoke-static {v0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->convertArrayNumber(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Z)Ljava/lang/Double;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    return-object v0

    .line 44
    :cond_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseBooleanLiteral()Ljava/lang/Boolean;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    return-object v0

    .line 49
    :cond_1
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 50
    .line 51
    .line 52
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 53
    .line 54
    .line 55
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseNumber()Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    const/4 v1, 0x0

    .line 60
    invoke-static {v0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->convertArrayNumber(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Z)Ljava/lang/Double;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    return-object v0

    .line 65
    :cond_2
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseErrorLiteral()I

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    invoke-static {v0}, Lcom/intsig/office/constant/fc/ErrorConstant;->valueOf(I)Lcom/intsig/office/constant/fc/ErrorConstant;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    return-object v0

    .line 74
    :cond_3
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseStringLiteral()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    return-object v0
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private parseArrayRow()[Ljava/lang/Object;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    :goto_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseArrayItem()Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 14
    .line 15
    .line 16
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 17
    .line 18
    const/16 v2, 0x2c

    .line 19
    .line 20
    if-eq v1, v2, :cond_2

    .line 21
    .line 22
    const/16 v2, 0x3b

    .line 23
    .line 24
    if-eq v1, v2, :cond_1

    .line 25
    .line 26
    const/16 v2, 0x7d

    .line 27
    .line 28
    if-ne v1, v2, :cond_0

    .line 29
    .line 30
    goto :goto_1

    .line 31
    :cond_0
    const-string v0, "\'}\' or \',\'"

    .line 32
    .line 33
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    throw v0

    .line 38
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    new-array v1, v1, [Ljava/lang/Object;

    .line 43
    .line 44
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    return-object v1

    .line 48
    :cond_2
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 49
    .line 50
    .line 51
    goto :goto_0
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private parseBooleanLiteral()Ljava/lang/Boolean;
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseUnquotedIdentifier()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "TRUE"

    .line 6
    .line 7
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 14
    .line 15
    return-object v0

    .line 16
    :cond_0
    const-string v1, "FALSE"

    .line 17
    .line 18
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 25
    .line 26
    return-object v0

    .line 27
    :cond_1
    const-string v0, "\'TRUE\' or \'FALSE\'"

    .line 28
    .line 29
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    throw v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private parseErrorLiteral()I
    .locals 6

    .line 1
    const/16 v0, 0x23

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseUnquotedIdentifier()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_d

    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    const/16 v3, 0x44

    .line 22
    .line 23
    const/16 v4, 0x2f

    .line 24
    .line 25
    const/16 v5, 0x21

    .line 26
    .line 27
    if-eq v2, v3, :cond_b

    .line 28
    .line 29
    const/16 v3, 0x4e

    .line 30
    .line 31
    if-eq v2, v3, :cond_4

    .line 32
    .line 33
    const/16 v1, 0x52

    .line 34
    .line 35
    if-eq v2, v1, :cond_2

    .line 36
    .line 37
    const/16 v1, 0x56

    .line 38
    .line 39
    if-ne v2, v1, :cond_1

    .line 40
    .line 41
    const-string v1, "VALUE"

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-eqz v0, :cond_0

    .line 48
    .line 49
    invoke-direct {p0, v5}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 50
    .line 51
    .line 52
    const/16 v0, 0xf

    .line 53
    .line 54
    return v0

    .line 55
    :cond_0
    const-string v0, "#VALUE!"

    .line 56
    .line 57
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    throw v0

    .line 62
    :cond_1
    const-string v0, "#VALUE!, #REF!, #DIV/0!, #NAME?, #NUM!, #NULL! or #N/A"

    .line 63
    .line 64
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    throw v0

    .line 69
    :cond_2
    const-string v1, "REF"

    .line 70
    .line 71
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    if-eqz v0, :cond_3

    .line 76
    .line 77
    invoke-direct {p0, v5}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 78
    .line 79
    .line 80
    const/16 v0, 0x17

    .line 81
    .line 82
    return v0

    .line 83
    :cond_3
    const-string v0, "#REF!"

    .line 84
    .line 85
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    throw v0

    .line 90
    :cond_4
    const-string v2, "NAME"

    .line 91
    .line 92
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 93
    .line 94
    .line 95
    move-result v2

    .line 96
    if-eqz v2, :cond_5

    .line 97
    .line 98
    const/16 v0, 0x3f

    .line 99
    .line 100
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 101
    .line 102
    .line 103
    const/16 v0, 0x1d

    .line 104
    .line 105
    return v0

    .line 106
    :cond_5
    const-string v2, "NUM"

    .line 107
    .line 108
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 109
    .line 110
    .line 111
    move-result v2

    .line 112
    if-eqz v2, :cond_6

    .line 113
    .line 114
    invoke-direct {p0, v5}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 115
    .line 116
    .line 117
    const/16 v0, 0x24

    .line 118
    .line 119
    return v0

    .line 120
    :cond_6
    const-string v2, "NULL"

    .line 121
    .line 122
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 123
    .line 124
    .line 125
    move-result v2

    .line 126
    if-eqz v2, :cond_7

    .line 127
    .line 128
    invoke-direct {p0, v5}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 129
    .line 130
    .line 131
    return v1

    .line 132
    :cond_7
    const-string v1, "N"

    .line 133
    .line 134
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 135
    .line 136
    .line 137
    move-result v0

    .line 138
    if-eqz v0, :cond_a

    .line 139
    .line 140
    invoke-direct {p0, v4}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 141
    .line 142
    .line 143
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 144
    .line 145
    const/16 v1, 0x41

    .line 146
    .line 147
    if-eq v0, v1, :cond_9

    .line 148
    .line 149
    const/16 v1, 0x61

    .line 150
    .line 151
    if-ne v0, v1, :cond_8

    .line 152
    .line 153
    goto :goto_0

    .line 154
    :cond_8
    const-string v0, "#N/A"

    .line 155
    .line 156
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 157
    .line 158
    .line 159
    move-result-object v0

    .line 160
    throw v0

    .line 161
    :cond_9
    :goto_0
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 162
    .line 163
    .line 164
    const/16 v0, 0x2a

    .line 165
    .line 166
    return v0

    .line 167
    :cond_a
    const-string v0, "#NAME?, #NUM!, #NULL! or #N/A"

    .line 168
    .line 169
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 170
    .line 171
    .line 172
    move-result-object v0

    .line 173
    throw v0

    .line 174
    :cond_b
    const-string v1, "DIV"

    .line 175
    .line 176
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 177
    .line 178
    .line 179
    move-result v0

    .line 180
    if-eqz v0, :cond_c

    .line 181
    .line 182
    invoke-direct {p0, v4}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 183
    .line 184
    .line 185
    const/16 v0, 0x30

    .line 186
    .line 187
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 188
    .line 189
    .line 190
    invoke-direct {p0, v5}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 191
    .line 192
    .line 193
    const/4 v0, 0x7

    .line 194
    return v0

    .line 195
    :cond_c
    const-string v0, "#DIV/0!"

    .line 196
    .line 197
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 198
    .line 199
    .line 200
    move-result-object v0

    .line 201
    throw v0

    .line 202
    :cond_d
    const-string v0, "remainder of error constant literal"

    .line 203
    .line 204
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 205
    .line 206
    .line 207
    move-result-object v0

    .line 208
    throw v0
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private parseNonRange(I)Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 3

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->resetPointer(I)V

    .line 2
    .line 3
    .line 4
    iget-char p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 5
    .line 6
    invoke-static {p1}, Ljava/lang/Character;->isDigit(C)Z

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseNumber()Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 19
    .line 20
    .line 21
    return-object p1

    .line 22
    :cond_0
    iget-char p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 23
    .line 24
    const/16 v0, 0x22

    .line 25
    .line 26
    if-ne p1, v0, :cond_1

    .line 27
    .line 28
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 29
    .line 30
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/ptg/StringPtg;

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseStringLiteral()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/StringPtg;-><init>(Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 40
    .line 41
    .line 42
    return-object p1

    .line 43
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 49
    .line 50
    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    if-nez v0, :cond_3

    .line 55
    .line 56
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 57
    .line 58
    const/16 v1, 0x5f

    .line 59
    .line 60
    if-ne v0, v1, :cond_2

    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_2
    const-string p1, "number, string, or defined name"

    .line 64
    .line 65
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    throw p1

    .line 70
    :cond_3
    :goto_0
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 71
    .line 72
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->isValidDefinedNameChar(C)Z

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    if-eqz v0, :cond_4

    .line 77
    .line 78
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 79
    .line 80
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 84
    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_4
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 88
    .line 89
    .line 90
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 95
    .line 96
    const/16 v1, 0x28

    .line 97
    .line 98
    if-ne v0, v1, :cond_5

    .line 99
    .line 100
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->function(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    return-object p1

    .line 105
    :cond_5
    const-string v0, "TRUE"

    .line 106
    .line 107
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 108
    .line 109
    .line 110
    move-result v1

    .line 111
    if-nez v1, :cond_a

    .line 112
    .line 113
    const-string v1, "FALSE"

    .line 114
    .line 115
    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 116
    .line 117
    .line 118
    move-result v1

    .line 119
    if-eqz v1, :cond_6

    .line 120
    .line 121
    goto :goto_1

    .line 122
    :cond_6
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_book:Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;

    .line 123
    .line 124
    if-eqz v0, :cond_9

    .line 125
    .line 126
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_sheetIndex:I

    .line 127
    .line 128
    invoke-interface {v0, p1, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;->getName(Ljava/lang/String;I)Lcom/intsig/office/fc/hssf/formula/EvaluationName;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    if-eqz v0, :cond_8

    .line 133
    .line 134
    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/formula/EvaluationName;->isRange()Z

    .line 135
    .line 136
    .line 137
    move-result v1

    .line 138
    if-eqz v1, :cond_7

    .line 139
    .line 140
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 141
    .line 142
    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/formula/EvaluationName;->createPtg()Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 147
    .line 148
    .line 149
    return-object p1

    .line 150
    :cond_7
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 151
    .line 152
    new-instance v1, Ljava/lang/StringBuilder;

    .line 153
    .line 154
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 155
    .line 156
    .line 157
    const-string v2, "Specified name \'"

    .line 158
    .line 159
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    .line 161
    .line 162
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    .line 164
    .line 165
    const-string p1, "\' is not a range as expected."

    .line 166
    .line 167
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    .line 169
    .line 170
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 171
    .line 172
    .line 173
    move-result-object p1

    .line 174
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 175
    .line 176
    .line 177
    throw v0

    .line 178
    :cond_8
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 179
    .line 180
    new-instance v1, Ljava/lang/StringBuilder;

    .line 181
    .line 182
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 183
    .line 184
    .line 185
    const-string v2, "Specified named range \'"

    .line 186
    .line 187
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    .line 189
    .line 190
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    .line 192
    .line 193
    const-string p1, "\' does not exist in the current workbook."

    .line 194
    .line 195
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object p1

    .line 202
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 203
    .line 204
    .line 205
    throw v0

    .line 206
    :cond_9
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 207
    .line 208
    new-instance v1, Ljava/lang/StringBuilder;

    .line 209
    .line 210
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 211
    .line 212
    .line 213
    const-string v2, "Need book to evaluate name \'"

    .line 214
    .line 215
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    .line 217
    .line 218
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    .line 220
    .line 221
    const-string p1, "\'"

    .line 222
    .line 223
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    .line 225
    .line 226
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 227
    .line 228
    .line 229
    move-result-object p1

    .line 230
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 231
    .line 232
    .line 233
    throw v0

    .line 234
    :cond_a
    :goto_1
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 235
    .line 236
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 237
    .line 238
    .line 239
    move-result p1

    .line 240
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/ptg/BoolPtg;->valueOf(Z)Lcom/intsig/office/fc/hssf/formula/ptg/BoolPtg;

    .line 241
    .line 242
    .line 243
    move-result-object p1

    .line 244
    invoke-direct {v1, p1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 245
    .line 246
    .line 247
    return-object v1
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private parseNumber()Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetNum()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 6
    .line 7
    const/16 v2, 0x2e

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    if-ne v1, v2, :cond_0

    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 13
    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetNum()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    move-object v1, v3

    .line 21
    :goto_0
    iget-char v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 22
    .line 23
    const/16 v4, 0x45

    .line 24
    .line 25
    const-string v5, "Integer"

    .line 26
    .line 27
    if-ne v2, v4, :cond_4

    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 30
    .line 31
    .line 32
    iget-char v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 33
    .line 34
    const/16 v3, 0x2b

    .line 35
    .line 36
    if-ne v2, v3, :cond_1

    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 39
    .line 40
    .line 41
    goto :goto_1

    .line 42
    :cond_1
    const/16 v3, 0x2d

    .line 43
    .line 44
    if-ne v2, v3, :cond_2

    .line 45
    .line 46
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 47
    .line 48
    .line 49
    const-string v2, "-"

    .line 50
    .line 51
    goto :goto_2

    .line 52
    :cond_2
    :goto_1
    const-string v2, ""

    .line 53
    .line 54
    :goto_2
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetNum()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v3

    .line 58
    if-eqz v3, :cond_3

    .line 59
    .line 60
    new-instance v4, Ljava/lang/StringBuilder;

    .line 61
    .line 62
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v3

    .line 75
    goto :goto_3

    .line 76
    :cond_3
    invoke-direct {p0, v5}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    throw v0

    .line 81
    :cond_4
    :goto_3
    if-nez v0, :cond_6

    .line 82
    .line 83
    if-eqz v1, :cond_5

    .line 84
    .line 85
    goto :goto_4

    .line 86
    :cond_5
    invoke-direct {p0, v5}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    throw v0

    .line 91
    :cond_6
    :goto_4
    invoke-static {v0, v1, v3}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->getNumberPtgFromString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    return-object v0
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private parseRangeExpression()Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseRangeable()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    :goto_0
    iget-char v3, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 8
    .line 9
    const/16 v4, 0x3a

    .line 10
    .line 11
    if-ne v3, v4, :cond_0

    .line 12
    .line 13
    iget v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseRangeable()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 19
    .line 20
    .line 21
    move-result-object v3

    .line 22
    const-string v4, "LHS"

    .line 23
    .line 24
    invoke-static {v4, v2, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->checkValidRangeOperand(Ljava/lang/String;ILcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 25
    .line 26
    .line 27
    const-string v4, "RHS"

    .line 28
    .line 29
    invoke-static {v4, v2, v3}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->checkValidRangeOperand(Ljava/lang/String;ILcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 30
    .line 31
    .line 32
    const/4 v2, 0x2

    .line 33
    new-array v2, v2, [Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 34
    .line 35
    aput-object v0, v2, v1

    .line 36
    .line 37
    const/4 v0, 0x1

    .line 38
    aput-object v3, v2, v0

    .line 39
    .line 40
    new-instance v3, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 41
    .line 42
    sget-object v4, Lcom/intsig/office/fc/hssf/formula/ptg/RangePtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;

    .line 43
    .line 44
    invoke-direct {v3, v4, v2}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 45
    .line 46
    .line 47
    move-object v0, v3

    .line 48
    const/4 v2, 0x1

    .line 49
    goto :goto_0

    .line 50
    :cond_0
    if-eqz v2, :cond_1

    .line 51
    .line 52
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->augmentWithMemPtg(Lcom/intsig/office/fc/hssf/formula/ParseNode;)Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    :cond_1
    return-object v0
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private parseRangeable()Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 12

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 2
    .line 3
    .line 4
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseSheetName()Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    if-nez v1, :cond_0

    .line 11
    .line 12
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->resetPointer(I)V

    .line 13
    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 17
    .line 18
    .line 19
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 20
    .line 21
    :goto_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseSimpleRangePart()Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    const-string v3, "."

    .line 26
    .line 27
    if-nez v2, :cond_3

    .line 28
    .line 29
    if-eqz v1, :cond_2

    .line 30
    .line 31
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 32
    .line 33
    const/16 v1, 0x23

    .line 34
    .line 35
    if-ne v0, v1, :cond_1

    .line 36
    .line 37
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 38
    .line 39
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseErrorLiteral()I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/formula/ptg/ErrPtg;->valueOf(I)Lcom/intsig/office/fc/hssf/formula/ptg/ErrPtg;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 48
    .line 49
    .line 50
    return-object v0

    .line 51
    :cond_1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 52
    .line 53
    new-instance v1, Ljava/lang/StringBuilder;

    .line 54
    .line 55
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .line 57
    .line 58
    const-string v2, "Cell reference expected after sheet name at index "

    .line 59
    .line 60
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    iget v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 64
    .line 65
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    throw v0

    .line 79
    :cond_2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseNonRange(I)Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    return-object v0

    .line 84
    :cond_3
    iget-char v4, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 85
    .line 86
    invoke-static {v4}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->IsWhite(C)Z

    .line 87
    .line 88
    .line 89
    move-result v4

    .line 90
    if-eqz v4, :cond_4

    .line 91
    .line 92
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 93
    .line 94
    .line 95
    :cond_4
    iget-char v5, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 96
    .line 97
    const/16 v6, 0x3a

    .line 98
    .line 99
    const/4 v7, 0x0

    .line 100
    if-ne v5, v6, :cond_9

    .line 101
    .line 102
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 103
    .line 104
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 105
    .line 106
    .line 107
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 108
    .line 109
    .line 110
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseSimpleRangePart()Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;

    .line 111
    .line 112
    .line 113
    move-result-object v3

    .line 114
    if-eqz v3, :cond_5

    .line 115
    .line 116
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->Oo08(Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;)Z

    .line 117
    .line 118
    .line 119
    move-result v4

    .line 120
    if-nez v4, :cond_5

    .line 121
    .line 122
    goto :goto_1

    .line 123
    :cond_5
    move-object v7, v3

    .line 124
    :goto_1
    if-nez v7, :cond_8

    .line 125
    .line 126
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->resetPointer(I)V

    .line 127
    .line 128
    .line 129
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇o〇()Z

    .line 130
    .line 131
    .line 132
    move-result v0

    .line 133
    if-nez v0, :cond_7

    .line 134
    .line 135
    if-nez v1, :cond_6

    .line 136
    .line 137
    const-string v0, ""

    .line 138
    .line 139
    goto :goto_2

    .line 140
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    .line 141
    .line 142
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 143
    .line 144
    .line 145
    const-string v3, "\'"

    .line 146
    .line 147
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    .line 149
    .line 150
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;->〇o00〇〇Oo()Lcom/intsig/office/fc/hssf/formula/FormulaParser$Identifier;

    .line 151
    .line 152
    .line 153
    move-result-object v1

    .line 154
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$Identifier;->〇080()Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v1

    .line 158
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    .line 160
    .line 161
    const/16 v1, 0x21

    .line 162
    .line 163
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 164
    .line 165
    .line 166
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 167
    .line 168
    .line 169
    move-result-object v0

    .line 170
    :goto_2
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 171
    .line 172
    new-instance v3, Ljava/lang/StringBuilder;

    .line 173
    .line 174
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 175
    .line 176
    .line 177
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    .line 179
    .line 180
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇o00〇〇Oo()Ljava/lang/String;

    .line 181
    .line 182
    .line 183
    move-result-object v0

    .line 184
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    .line 186
    .line 187
    const-string v0, "\' is not a proper reference."

    .line 188
    .line 189
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    .line 191
    .line 192
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 193
    .line 194
    .line 195
    move-result-object v0

    .line 196
    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 197
    .line 198
    .line 199
    throw v1

    .line 200
    :cond_7
    invoke-direct {p0, v1, v2, v7}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->createAreaRefParseNode(Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;)Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 201
    .line 202
    .line 203
    move-result-object v0

    .line 204
    return-object v0

    .line 205
    :cond_8
    invoke-direct {p0, v1, v2, v7}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->createAreaRefParseNode(Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;)Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 206
    .line 207
    .line 208
    move-result-object v0

    .line 209
    return-object v0

    .line 210
    :cond_9
    const/16 v6, 0x2e

    .line 211
    .line 212
    if-ne v5, v6, :cond_14

    .line 213
    .line 214
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 215
    .line 216
    .line 217
    const/4 v5, 0x1

    .line 218
    const/4 v7, 0x1

    .line 219
    :goto_3
    iget-char v8, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 220
    .line 221
    if-ne v8, v6, :cond_a

    .line 222
    .line 223
    add-int/lit8 v7, v7, 0x1

    .line 224
    .line 225
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 226
    .line 227
    .line 228
    goto :goto_3

    .line 229
    :cond_a
    invoke-static {v8}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->IsWhite(C)Z

    .line 230
    .line 231
    .line 232
    move-result v6

    .line 233
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 234
    .line 235
    .line 236
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseSimpleRangePart()Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;

    .line 237
    .line 238
    .line 239
    move-result-object v8

    .line 240
    iget-object v9, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    .line 241
    .line 242
    add-int/lit8 v10, v0, -0x1

    .line 243
    .line 244
    iget v11, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 245
    .line 246
    sub-int/2addr v11, v5

    .line 247
    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 248
    .line 249
    .line 250
    move-result-object v9

    .line 251
    if-nez v8, :cond_c

    .line 252
    .line 253
    if-nez v1, :cond_b

    .line 254
    .line 255
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseNonRange(I)Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 256
    .line 257
    .line 258
    move-result-object v0

    .line 259
    return-object v0

    .line 260
    :cond_b
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 261
    .line 262
    new-instance v1, Ljava/lang/StringBuilder;

    .line 263
    .line 264
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 265
    .line 266
    .line 267
    const-string v2, "Complete area reference expected after sheet name at index "

    .line 268
    .line 269
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    .line 271
    .line 272
    iget v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 273
    .line 274
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 275
    .line 276
    .line 277
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    .line 279
    .line 280
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 281
    .line 282
    .line 283
    move-result-object v1

    .line 284
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 285
    .line 286
    .line 287
    throw v0

    .line 288
    :cond_c
    const-string v3, "Dotted range (full row or column) expression \'"

    .line 289
    .line 290
    if-nez v4, :cond_12

    .line 291
    .line 292
    if-eqz v6, :cond_d

    .line 293
    .line 294
    goto :goto_4

    .line 295
    :cond_d
    if-ne v7, v5, :cond_e

    .line 296
    .line 297
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->o〇0()Z

    .line 298
    .line 299
    .line 300
    move-result v4

    .line 301
    if-eqz v4, :cond_e

    .line 302
    .line 303
    invoke-virtual {v8}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->o〇0()Z

    .line 304
    .line 305
    .line 306
    move-result v4

    .line 307
    if-eqz v4, :cond_e

    .line 308
    .line 309
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseNonRange(I)Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 310
    .line 311
    .line 312
    move-result-object v0

    .line 313
    return-object v0

    .line 314
    :cond_e
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇〇888()Z

    .line 315
    .line 316
    .line 317
    move-result v0

    .line 318
    if-nez v0, :cond_f

    .line 319
    .line 320
    invoke-virtual {v8}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇〇888()Z

    .line 321
    .line 322
    .line 323
    move-result v0

    .line 324
    if-eqz v0, :cond_10

    .line 325
    .line 326
    :cond_f
    const/4 v0, 0x2

    .line 327
    if-ne v7, v0, :cond_11

    .line 328
    .line 329
    :cond_10
    invoke-direct {p0, v1, v2, v8}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->createAreaRefParseNode(Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;)Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 330
    .line 331
    .line 332
    move-result-object v0

    .line 333
    return-object v0

    .line 334
    :cond_11
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 335
    .line 336
    new-instance v1, Ljava/lang/StringBuilder;

    .line 337
    .line 338
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 339
    .line 340
    .line 341
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    .line 343
    .line 344
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    .line 346
    .line 347
    const-string v2, "\' must have exactly 2 dots."

    .line 348
    .line 349
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 350
    .line 351
    .line 352
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 353
    .line 354
    .line 355
    move-result-object v1

    .line 356
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 357
    .line 358
    .line 359
    throw v0

    .line 360
    :cond_12
    :goto_4
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇〇888()Z

    .line 361
    .line 362
    .line 363
    move-result v0

    .line 364
    if-nez v0, :cond_13

    .line 365
    .line 366
    invoke-virtual {v8}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇〇888()Z

    .line 367
    .line 368
    .line 369
    move-result v0

    .line 370
    if-nez v0, :cond_13

    .line 371
    .line 372
    invoke-direct {p0, v1, v2, v8}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->createAreaRefParseNode(Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;)Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 373
    .line 374
    .line 375
    move-result-object v0

    .line 376
    return-object v0

    .line 377
    :cond_13
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 378
    .line 379
    new-instance v1, Ljava/lang/StringBuilder;

    .line 380
    .line 381
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 382
    .line 383
    .line 384
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 385
    .line 386
    .line 387
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 388
    .line 389
    .line 390
    const-string v2, "\' must not contain whitespace."

    .line 391
    .line 392
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    .line 394
    .line 395
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 396
    .line 397
    .line 398
    move-result-object v1

    .line 399
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 400
    .line 401
    .line 402
    throw v0

    .line 403
    :cond_14
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇o〇()Z

    .line 404
    .line 405
    .line 406
    move-result v4

    .line 407
    if-eqz v4, :cond_15

    .line 408
    .line 409
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;->〇o00〇〇Oo()Ljava/lang/String;

    .line 410
    .line 411
    .line 412
    move-result-object v4

    .line 413
    invoke-direct {p0, v4}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->isValidCellReference(Ljava/lang/String;)Z

    .line 414
    .line 415
    .line 416
    move-result v4

    .line 417
    if-eqz v4, :cond_15

    .line 418
    .line 419
    invoke-direct {p0, v1, v2, v7}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->createAreaRefParseNode(Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;)Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 420
    .line 421
    .line 422
    move-result-object v0

    .line 423
    return-object v0

    .line 424
    :cond_15
    if-nez v1, :cond_16

    .line 425
    .line 426
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseNonRange(I)Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 427
    .line 428
    .line 429
    move-result-object v0

    .line 430
    return-object v0

    .line 431
    :cond_16
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 432
    .line 433
    new-instance v1, Ljava/lang/StringBuilder;

    .line 434
    .line 435
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 436
    .line 437
    .line 438
    const-string v2, "Second part of cell reference expected after sheet name at index "

    .line 439
    .line 440
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 441
    .line 442
    .line 443
    iget v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 444
    .line 445
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 446
    .line 447
    .line 448
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 449
    .line 450
    .line 451
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 452
    .line 453
    .line 454
    move-result-object v1

    .line 455
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 456
    .line 457
    .line 458
    throw v0
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
    .line 1825
    .line 1826
    .line 1827
    .line 1828
    .line 1829
    .line 1830
    .line 1831
    .line 1832
    .line 1833
    .line 1834
    .line 1835
    .line 1836
    .line 1837
    .line 1838
    .line 1839
    .line 1840
    .line 1841
    .line 1842
    .line 1843
    .line 1844
    .line 1845
    .line 1846
    .line 1847
    .line 1848
    .line 1849
    .line 1850
    .line 1851
    .line 1852
    .line 1853
    .line 1854
    .line 1855
    .line 1856
    .line 1857
    .line 1858
    .line 1859
    .line 1860
    .line 1861
    .line 1862
    .line 1863
    .line 1864
    .line 1865
    .line 1866
    .line 1867
    .line 1868
    .line 1869
    .line 1870
    .line 1871
    .line 1872
    .line 1873
    .line 1874
    .line 1875
    .line 1876
    .line 1877
    .line 1878
    .line 1879
    .line 1880
    .line 1881
    .line 1882
    .line 1883
    .line 1884
    .line 1885
    .line 1886
    .line 1887
    .line 1888
    .line 1889
    .line 1890
    .line 1891
    .line 1892
    .line 1893
    .line 1894
    .line 1895
    .line 1896
    .line 1897
    .line 1898
    .line 1899
    .line 1900
    .line 1901
    .line 1902
    .line 1903
    .line 1904
    .line 1905
    .line 1906
    .line 1907
    .line 1908
    .line 1909
    .line 1910
    .line 1911
    .line 1912
    .line 1913
    .line 1914
    .line 1915
    .line 1916
    .line 1917
    .line 1918
    .line 1919
    .line 1920
    .line 1921
    .line 1922
    .line 1923
    .line 1924
    .line 1925
    .line 1926
    .line 1927
    .line 1928
    .line 1929
    .line 1930
    .line 1931
    .line 1932
    .line 1933
    .line 1934
    .line 1935
    .line 1936
    .line 1937
    .line 1938
    .line 1939
    .line 1940
    .line 1941
    .line 1942
    .line 1943
    .line 1944
    .line 1945
    .line 1946
    .line 1947
    .line 1948
    .line 1949
    .line 1950
    .line 1951
    .line 1952
    .line 1953
    .line 1954
    .line 1955
    .line 1956
    .line 1957
    .line 1958
    .line 1959
    .line 1960
    .line 1961
    .line 1962
    .line 1963
    .line 1964
    .line 1965
    .line 1966
    .line 1967
    .line 1968
    .line 1969
    .line 1970
    .line 1971
    .line 1972
    .line 1973
    .line 1974
    .line 1975
    .line 1976
    .line 1977
    .line 1978
    .line 1979
    .line 1980
    .line 1981
    .line 1982
    .line 1983
    .line 1984
    .line 1985
    .line 1986
    .line 1987
    .line 1988
    .line 1989
    .line 1990
    .line 1991
    .line 1992
    .line 1993
    .line 1994
    .line 1995
    .line 1996
    .line 1997
    .line 1998
    .line 1999
    .line 2000
    .line 2001
    .line 2002
    .line 2003
    .line 2004
    .line 2005
    .line 2006
    .line 2007
    .line 2008
    .line 2009
    .line 2010
    .line 2011
    .line 2012
    .line 2013
    .line 2014
    .line 2015
    .line 2016
    .line 2017
    .line 2018
    .line 2019
    .line 2020
    .line 2021
    .line 2022
    .line 2023
    .line 2024
    .line 2025
    .line 2026
    .line 2027
    .line 2028
    .line 2029
    .line 2030
    .line 2031
    .line 2032
    .line 2033
    .line 2034
    .line 2035
    .line 2036
    .line 2037
    .line 2038
    .line 2039
    .line 2040
    .line 2041
    .line 2042
    .line 2043
    .line 2044
    .line 2045
    .line 2046
    .line 2047
    .line 2048
    .line 2049
    .line 2050
    .line 2051
    .line 2052
    .line 2053
    .line 2054
    .line 2055
    .line 2056
    .line 2057
    .line 2058
    .line 2059
    .line 2060
    .line 2061
    .line 2062
    .line 2063
    .line 2064
    .line 2065
    .line 2066
    .line 2067
    .line 2068
    .line 2069
    .line 2070
    .line 2071
    .line 2072
    .line 2073
    .line 2074
    .line 2075
    .line 2076
    .line 2077
    .line 2078
    .line 2079
    .line 2080
    .line 2081
    .line 2082
    .line 2083
    .line 2084
    .line 2085
    .line 2086
.end method

.method private parseSheetName()Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;
    .locals 9

    .line 1
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 2
    .line 3
    const/16 v1, 0x5b

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-ne v0, v1, :cond_1

    .line 7
    .line 8
    new-instance v0, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 14
    .line 15
    .line 16
    :goto_0
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 17
    .line 18
    const/16 v3, 0x5d

    .line 19
    .line 20
    if-eq v1, v3, :cond_0

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    goto :goto_1

    .line 37
    :cond_1
    move-object v0, v2

    .line 38
    :goto_1
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 39
    .line 40
    const/16 v3, 0x21

    .line 41
    .line 42
    const/4 v4, 0x0

    .line 43
    const/16 v5, 0x27

    .line 44
    .line 45
    if-ne v1, v5, :cond_6

    .line 46
    .line 47
    new-instance v1, Ljava/lang/StringBuffer;

    .line 48
    .line 49
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 50
    .line 51
    .line 52
    invoke-direct {p0, v5}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 53
    .line 54
    .line 55
    iget-char v6, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 56
    .line 57
    const/4 v7, 0x1

    .line 58
    if-ne v6, v5, :cond_2

    .line 59
    .line 60
    :goto_2
    const/4 v6, 0x1

    .line 61
    goto :goto_3

    .line 62
    :cond_2
    const/4 v6, 0x0

    .line 63
    :cond_3
    :goto_3
    if-nez v6, :cond_4

    .line 64
    .line 65
    iget-char v8, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 66
    .line 67
    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 68
    .line 69
    .line 70
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 71
    .line 72
    .line 73
    iget-char v8, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 74
    .line 75
    if-ne v8, v5, :cond_3

    .line 76
    .line 77
    invoke-direct {p0, v5}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 78
    .line 79
    .line 80
    iget-char v6, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 81
    .line 82
    if-eq v6, v5, :cond_2

    .line 83
    .line 84
    goto :goto_2

    .line 85
    :cond_4
    new-instance v4, Lcom/intsig/office/fc/hssf/formula/FormulaParser$Identifier;

    .line 86
    .line 87
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    invoke-direct {v4, v1, v7}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$Identifier;-><init>(Ljava/lang/String;Z)V

    .line 92
    .line 93
    .line 94
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 95
    .line 96
    .line 97
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 98
    .line 99
    if-ne v1, v3, :cond_5

    .line 100
    .line 101
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 102
    .line 103
    .line 104
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;

    .line 105
    .line 106
    invoke-direct {v1, v0, v4}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/hssf/formula/FormulaParser$Identifier;)V

    .line 107
    .line 108
    .line 109
    return-object v1

    .line 110
    :cond_5
    return-object v2

    .line 111
    :cond_6
    const/16 v5, 0x5f

    .line 112
    .line 113
    if-eq v1, v5, :cond_8

    .line 114
    .line 115
    invoke-static {v1}, Ljava/lang/Character;->isLetter(C)Z

    .line 116
    .line 117
    .line 118
    move-result v1

    .line 119
    if-eqz v1, :cond_7

    .line 120
    .line 121
    goto :goto_4

    .line 122
    :cond_7
    return-object v2

    .line 123
    :cond_8
    :goto_4
    new-instance v1, Ljava/lang/StringBuilder;

    .line 124
    .line 125
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 126
    .line 127
    .line 128
    :goto_5
    iget-char v5, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 129
    .line 130
    invoke-static {v5}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->isUnquotedSheetNameChar(C)Z

    .line 131
    .line 132
    .line 133
    move-result v5

    .line 134
    if-eqz v5, :cond_9

    .line 135
    .line 136
    iget-char v5, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 137
    .line 138
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 139
    .line 140
    .line 141
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 142
    .line 143
    .line 144
    goto :goto_5

    .line 145
    :cond_9
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 146
    .line 147
    .line 148
    iget-char v5, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 149
    .line 150
    if-ne v5, v3, :cond_a

    .line 151
    .line 152
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 153
    .line 154
    .line 155
    new-instance v2, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;

    .line 156
    .line 157
    new-instance v3, Lcom/intsig/office/fc/hssf/formula/FormulaParser$Identifier;

    .line 158
    .line 159
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 160
    .line 161
    .line 162
    move-result-object v1

    .line 163
    invoke-direct {v3, v1, v4}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$Identifier;-><init>(Ljava/lang/String;Z)V

    .line 164
    .line 165
    .line 166
    invoke-direct {v2, v0, v3}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SheetIdentifier;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/hssf/formula/FormulaParser$Identifier;)V

    .line 167
    .line 168
    .line 169
    :cond_a
    return-object v2
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private parseSimpleFactor()Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 2
    .line 3
    .line 4
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 5
    .line 6
    const/16 v1, 0x22

    .line 7
    .line 8
    if-eq v0, v1, :cond_8

    .line 9
    .line 10
    const/16 v1, 0x23

    .line 11
    .line 12
    if-eq v0, v1, :cond_7

    .line 13
    .line 14
    const/16 v1, 0x28

    .line 15
    .line 16
    if-eq v0, v1, :cond_6

    .line 17
    .line 18
    const/16 v1, 0x2b

    .line 19
    .line 20
    if-eq v0, v1, :cond_5

    .line 21
    .line 22
    const/16 v1, 0x2d

    .line 23
    .line 24
    if-eq v0, v1, :cond_4

    .line 25
    .line 26
    const/16 v1, 0x7b

    .line 27
    .line 28
    if-eq v0, v1, :cond_3

    .line 29
    .line 30
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->IsAlpha(C)Z

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    if-nez v0, :cond_2

    .line 35
    .line 36
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 37
    .line 38
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    if-nez v0, :cond_2

    .line 43
    .line 44
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 45
    .line 46
    const/16 v1, 0x27

    .line 47
    .line 48
    if-eq v0, v1, :cond_2

    .line 49
    .line 50
    const/16 v1, 0x5b

    .line 51
    .line 52
    if-ne v0, v1, :cond_0

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    const/16 v1, 0x2e

    .line 56
    .line 57
    if-ne v0, v1, :cond_1

    .line 58
    .line 59
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 60
    .line 61
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseNumber()Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 66
    .line 67
    .line 68
    return-object v0

    .line 69
    :cond_1
    const-string v0, "cell ref or constant literal"

    .line 70
    .line 71
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    throw v0

    .line 76
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseRangeExpression()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    return-object v0

    .line 81
    :cond_3
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 82
    .line 83
    .line 84
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseArray()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    const/16 v1, 0x7d

    .line 89
    .line 90
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 91
    .line 92
    .line 93
    return-object v0

    .line 94
    :cond_4
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 95
    .line 96
    .line 97
    const/4 v0, 0x0

    .line 98
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseUnary(Z)Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    return-object v0

    .line 103
    :cond_5
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 104
    .line 105
    .line 106
    const/4 v0, 0x1

    .line 107
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseUnary(Z)Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    return-object v0

    .line 112
    :cond_6
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 113
    .line 114
    .line 115
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->comparisonExpression()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    const/16 v1, 0x29

    .line 120
    .line 121
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 122
    .line 123
    .line 124
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 125
    .line 126
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/ptg/ParenthesisPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ControlPtg;

    .line 127
    .line 128
    invoke-direct {v1, v2, v0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 129
    .line 130
    .line 131
    return-object v1

    .line 132
    :cond_7
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 133
    .line 134
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseErrorLiteral()I

    .line 135
    .line 136
    .line 137
    move-result v1

    .line 138
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/formula/ptg/ErrPtg;->valueOf(I)Lcom/intsig/office/fc/hssf/formula/ptg/ErrPtg;

    .line 139
    .line 140
    .line 141
    move-result-object v1

    .line 142
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 143
    .line 144
    .line 145
    return-object v0

    .line 146
    :cond_8
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 147
    .line 148
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/ptg/StringPtg;

    .line 149
    .line 150
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseStringLiteral()Ljava/lang/String;

    .line 151
    .line 152
    .line 153
    move-result-object v2

    .line 154
    invoke-direct {v1, v2}, Lcom/intsig/office/fc/hssf/formula/ptg/StringPtg;-><init>(Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 158
    .line 159
    .line 160
    return-object v0
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private parseSimpleRangePart()Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;
    .locals 8

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    sub-int/2addr v0, v1

    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x0

    .line 7
    :goto_0
    iget v4, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaLength:I

    .line 8
    .line 9
    if-ge v0, v4, :cond_3

    .line 10
    .line 11
    iget-object v4, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    .line 14
    .line 15
    .line 16
    move-result v4

    .line 17
    invoke-static {v4}, Ljava/lang/Character;->isDigit(C)Z

    .line 18
    .line 19
    .line 20
    move-result v5

    .line 21
    if-eqz v5, :cond_0

    .line 22
    .line 23
    const/4 v2, 0x1

    .line 24
    goto :goto_1

    .line 25
    :cond_0
    invoke-static {v4}, Ljava/lang/Character;->isLetter(C)Z

    .line 26
    .line 27
    .line 28
    move-result v5

    .line 29
    if-eqz v5, :cond_1

    .line 30
    .line 31
    const/4 v3, 0x1

    .line 32
    goto :goto_1

    .line 33
    :cond_1
    const/16 v5, 0x24

    .line 34
    .line 35
    if-eq v4, v5, :cond_2

    .line 36
    .line 37
    const/16 v5, 0x5f

    .line 38
    .line 39
    if-ne v4, v5, :cond_3

    .line 40
    .line 41
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_3
    iget v4, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 45
    .line 46
    add-int/lit8 v5, v4, -0x1

    .line 47
    .line 48
    const/4 v6, 0x0

    .line 49
    if-gt v0, v5, :cond_4

    .line 50
    .line 51
    return-object v6

    .line 52
    :cond_4
    iget-object v5, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    .line 53
    .line 54
    sub-int/2addr v4, v1

    .line 55
    invoke-virtual {v5, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v4

    .line 59
    sget-object v5, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->CELL_REF_PATTERN:Ljava/util/regex/Pattern;

    .line 60
    .line 61
    invoke-virtual {v5, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    .line 62
    .line 63
    .line 64
    move-result-object v5

    .line 65
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    .line 66
    .line 67
    .line 68
    move-result v5

    .line 69
    if-nez v5, :cond_5

    .line 70
    .line 71
    return-object v6

    .line 72
    :cond_5
    if-eqz v3, :cond_6

    .line 73
    .line 74
    if-eqz v2, :cond_6

    .line 75
    .line 76
    invoke-direct {p0, v4}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->isValidCellReference(Ljava/lang/String;)Z

    .line 77
    .line 78
    .line 79
    move-result v5

    .line 80
    if-nez v5, :cond_8

    .line 81
    .line 82
    return-object v6

    .line 83
    :cond_6
    const-string v5, ""

    .line 84
    .line 85
    const-string v7, "$"

    .line 86
    .line 87
    if-eqz v3, :cond_7

    .line 88
    .line 89
    invoke-virtual {v4, v7, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v5

    .line 93
    iget-object v7, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_ssVersion:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    .line 94
    .line 95
    invoke-static {v5, v7}, Lcom/intsig/office/fc/ss/util/CellReference;->isColumnWithnRange(Ljava/lang/String;Lcom/intsig/office/fc/ss/SpreadsheetVersion;)Z

    .line 96
    .line 97
    .line 98
    move-result v5

    .line 99
    if-nez v5, :cond_8

    .line 100
    .line 101
    return-object v6

    .line 102
    :cond_7
    if-eqz v2, :cond_9

    .line 103
    .line 104
    :try_start_0
    invoke-virtual {v4, v7, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object v5

    .line 108
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 109
    .line 110
    .line 111
    move-result v5
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    if-lt v5, v1, :cond_9

    .line 113
    .line 114
    const/high16 v7, 0x10000

    .line 115
    .line 116
    if-le v5, v7, :cond_8

    .line 117
    .line 118
    goto :goto_2

    .line 119
    :cond_8
    add-int/2addr v0, v1

    .line 120
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->resetPointer(I)V

    .line 121
    .line 122
    .line 123
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;

    .line 124
    .line 125
    invoke-direct {v0, v4, v3, v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser$SimpleRangePart;-><init>(Ljava/lang/String;ZZ)V

    .line 126
    .line 127
    .line 128
    return-object v0

    .line 129
    :catch_0
    :cond_9
    :goto_2
    return-object v6
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private parseStringLiteral()Ljava/lang/String;
    .locals 3

    .line 1
    const/16 v0, 0x22

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 4
    .line 5
    .line 6
    new-instance v1, Ljava/lang/StringBuffer;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 9
    .line 10
    .line 11
    :goto_0
    iget-char v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 12
    .line 13
    if-ne v2, v0, :cond_0

    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 16
    .line 17
    .line 18
    iget-char v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 19
    .line 20
    if-eq v2, v0, :cond_0

    .line 21
    .line 22
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    return-object v0

    .line 27
    :cond_0
    iget-char v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 28
    .line 29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 30
    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 33
    .line 34
    .line 35
    goto :goto_0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private parseUnary(Z)Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 3

    .line 1
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->IsDigit(C)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 10
    .line 11
    const/16 v1, 0x2e

    .line 12
    .line 13
    if-ne v0, v1, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    goto :goto_1

    .line 18
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 19
    :goto_1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->powerFactor()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    if-eqz v0, :cond_5

    .line 24
    .line 25
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;->Oo08()Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    instance-of v2, v0, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;

    .line 30
    .line 31
    if-eqz v2, :cond_3

    .line 32
    .line 33
    if-eqz p1, :cond_2

    .line 34
    .line 35
    return-object v1

    .line 36
    :cond_2
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;

    .line 37
    .line 38
    check-cast v0, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;

    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;->getValue()D

    .line 41
    .line 42
    .line 43
    move-result-wide v0

    .line 44
    neg-double v0, v0

    .line 45
    invoke-direct {p1, v0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;-><init>(D)V

    .line 46
    .line 47
    .line 48
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 49
    .line 50
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 51
    .line 52
    .line 53
    return-object v0

    .line 54
    :cond_3
    instance-of v2, v0, Lcom/intsig/office/fc/hssf/formula/ptg/IntPtg;

    .line 55
    .line 56
    if-eqz v2, :cond_5

    .line 57
    .line 58
    if-eqz p1, :cond_4

    .line 59
    .line 60
    return-object v1

    .line 61
    :cond_4
    check-cast v0, Lcom/intsig/office/fc/hssf/formula/ptg/IntPtg;

    .line 62
    .line 63
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/ptg/IntPtg;->getValue()I

    .line 64
    .line 65
    .line 66
    move-result p1

    .line 67
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;

    .line 68
    .line 69
    neg-int p1, p1

    .line 70
    int-to-double v1, p1

    .line 71
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;-><init>(D)V

    .line 72
    .line 73
    .line 74
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 75
    .line 76
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 77
    .line 78
    .line 79
    return-object p1

    .line 80
    :cond_5
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 81
    .line 82
    if-eqz p1, :cond_6

    .line 83
    .line 84
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/ptg/UnaryPlusPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 85
    .line 86
    goto :goto_2

    .line 87
    :cond_6
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/ptg/UnaryMinusPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 88
    .line 89
    :goto_2
    invoke-direct {v0, p1, v1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 90
    .line 91
    .line 92
    return-object v0
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private parseUnquotedIdentifier()Ljava/lang/String;
    .locals 3

    .line 1
    iget-char v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 2
    .line 3
    const/16 v1, 0x27

    .line 4
    .line 5
    if-eq v0, v1, :cond_3

    .line 6
    .line 7
    new-instance v0, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    :goto_0
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 13
    .line 14
    invoke-static {v1}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-nez v1, :cond_2

    .line 19
    .line 20
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 21
    .line 22
    const/16 v2, 0x2e

    .line 23
    .line 24
    if-ne v1, v2, :cond_0

    .line 25
    .line 26
    goto :goto_1

    .line 27
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    const/4 v2, 0x1

    .line 32
    if-ge v1, v2, :cond_1

    .line 33
    .line 34
    const/4 v0, 0x0

    .line 35
    return-object v0

    .line 36
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    return-object v0

    .line 41
    :cond_2
    :goto_1
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_3
    const-string v0, "unquoted identifier"

    .line 51
    .line 52
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->expected(Ljava/lang/String;)Ljava/lang/RuntimeException;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    throw v0
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private percentFactor()Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->parseSimpleFactor()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    :goto_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 6
    .line 7
    .line 8
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 9
    .line 10
    const/16 v2, 0x25

    .line 11
    .line 12
    if-eq v1, v2, :cond_0

    .line 13
    .line 14
    return-object v0

    .line 15
    :cond_0
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 16
    .line 17
    .line 18
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 19
    .line 20
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/ptg/PercentPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 21
    .line 22
    invoke-direct {v1, v2, v0}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 23
    .line 24
    .line 25
    move-object v0, v1

    .line 26
    goto :goto_0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private powerFactor()Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->percentFactor()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    :goto_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 6
    .line 7
    .line 8
    iget-char v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 9
    .line 10
    const/16 v2, 0x5e

    .line 11
    .line 12
    if-eq v1, v2, :cond_0

    .line 13
    .line 14
    return-object v0

    .line 15
    :cond_0
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->Match(C)V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->percentFactor()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    new-instance v2, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 23
    .line 24
    sget-object v3, Lcom/intsig/office/fc/hssf/formula/ptg/PowerPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 25
    .line 26
    invoke-direct {v2, v3, v0, v1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Lcom/intsig/office/fc/hssf/formula/ParseNode;Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 27
    .line 28
    .line 29
    move-object v0, v2

    .line 30
    goto :goto_0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private resetPointer(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_pointer:I

    .line 2
    .line 3
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaLength:I

    .line 4
    .line 5
    if-gt p1, v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_formulaString:Ljava/lang/String;

    .line 8
    .line 9
    add-int/lit8 p1, p1, -0x1

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    iput-char p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 p1, 0x0

    .line 19
    iput-char p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 20
    .line 21
    :goto_0
    return-void
    .line 22
    .line 23
    .line 24
.end method

.method private unionExpression()Lcom/intsig/office/fc/hssf/formula/ParseNode;
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->comparisonExpression()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    :goto_0
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->SkipWhite()V

    .line 7
    .line 8
    .line 9
    iget-char v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->look:C

    .line 10
    .line 11
    const/16 v3, 0x2c

    .line 12
    .line 13
    if-eq v2, v3, :cond_1

    .line 14
    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->augmentWithMemPtg(Lcom/intsig/office/fc/hssf/formula/ParseNode;)Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    :cond_0
    return-object v0

    .line 22
    :cond_1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->GetChar()V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->comparisonExpression()Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    new-instance v2, Lcom/intsig/office/fc/hssf/formula/ParseNode;

    .line 30
    .line 31
    sget-object v3, Lcom/intsig/office/fc/hssf/formula/ptg/UnionPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;

    .line 32
    .line 33
    invoke-direct {v2, v3, v0, v1}, Lcom/intsig/office/fc/hssf/formula/ParseNode;-><init>(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Lcom/intsig/office/fc/hssf/formula/ParseNode;Lcom/intsig/office/fc/hssf/formula/ParseNode;)V

    .line 34
    .line 35
    .line 36
    const/4 v1, 0x1

    .line 37
    move-object v0, v2

    .line 38
    goto :goto_0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private validateNumArgs(ILcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;)V
    .locals 8

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->getMinParams()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-string v1, "."

    .line 6
    .line 7
    const-string v2, " but got "

    .line 8
    .line 9
    const-string v3, "Expected "

    .line 10
    .line 11
    const-string v4, " were expected"

    .line 12
    .line 13
    const-string v5, "\'. "

    .line 14
    .line 15
    if-ge p1, v0, :cond_1

    .line 16
    .line 17
    new-instance v0, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    const-string v6, "Too few arguments to function \'"

    .line 23
    .line 24
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->getName()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v6

    .line 31
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->hasFixedArgsLength()Z

    .line 42
    .line 43
    .line 44
    move-result v5

    .line 45
    if-eqz v5, :cond_0

    .line 46
    .line 47
    new-instance v4, Ljava/lang/StringBuilder;

    .line 48
    .line 49
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->getMinParams()I

    .line 59
    .line 60
    .line 61
    move-result p2

    .line 62
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object p2

    .line 69
    goto :goto_0

    .line 70
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    .line 71
    .line 72
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 73
    .line 74
    .line 75
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    const-string v0, "At least "

    .line 79
    .line 80
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->getMinParams()I

    .line 84
    .line 85
    .line 86
    move-result p2

    .line 87
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object p2

    .line 97
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 98
    .line 99
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object p1

    .line 118
    new-instance p2, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 119
    .line 120
    invoke-direct {p2, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    throw p2

    .line 124
    :cond_1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->hasUnlimitedVarags()Z

    .line 125
    .line 126
    .line 127
    move-result v0

    .line 128
    if-eqz v0, :cond_3

    .line 129
    .line 130
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaParser;->_book:Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;

    .line 131
    .line 132
    if-eqz v0, :cond_2

    .line 133
    .line 134
    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;->getSpreadsheetVersion()Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    .line 135
    .line 136
    .line 137
    move-result-object v0

    .line 138
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->getMaxFunctionArgs()I

    .line 139
    .line 140
    .line 141
    move-result v0

    .line 142
    goto :goto_1

    .line 143
    :cond_2
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->getMaxParams()I

    .line 144
    .line 145
    .line 146
    move-result v0

    .line 147
    goto :goto_1

    .line 148
    :cond_3
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->getMaxParams()I

    .line 149
    .line 150
    .line 151
    move-result v0

    .line 152
    :goto_1
    if-le p1, v0, :cond_5

    .line 153
    .line 154
    new-instance v6, Ljava/lang/StringBuilder;

    .line 155
    .line 156
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 157
    .line 158
    .line 159
    const-string v7, "Too many arguments to function \'"

    .line 160
    .line 161
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    .line 163
    .line 164
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->getName()Ljava/lang/String;

    .line 165
    .line 166
    .line 167
    move-result-object v7

    .line 168
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    .line 170
    .line 171
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    .line 173
    .line 174
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 175
    .line 176
    .line 177
    move-result-object v5

    .line 178
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->hasFixedArgsLength()Z

    .line 179
    .line 180
    .line 181
    move-result p2

    .line 182
    if-eqz p2, :cond_4

    .line 183
    .line 184
    new-instance p2, Ljava/lang/StringBuilder;

    .line 185
    .line 186
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 187
    .line 188
    .line 189
    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    .line 191
    .line 192
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object p2

    .line 202
    goto :goto_2

    .line 203
    :cond_4
    new-instance p2, Ljava/lang/StringBuilder;

    .line 204
    .line 205
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 206
    .line 207
    .line 208
    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    .line 210
    .line 211
    const-string v3, "At most "

    .line 212
    .line 213
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    .line 215
    .line 216
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 217
    .line 218
    .line 219
    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    .line 221
    .line 222
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 223
    .line 224
    .line 225
    move-result-object p2

    .line 226
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    .line 227
    .line 228
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 229
    .line 230
    .line 231
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    .line 233
    .line 234
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    .line 236
    .line 237
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 238
    .line 239
    .line 240
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    .line 242
    .line 243
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 244
    .line 245
    .line 246
    move-result-object p1

    .line 247
    new-instance p2, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;

    .line 248
    .line 249
    invoke-direct {p2, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaParseException;-><init>(Ljava/lang/String;)V

    .line 250
    .line 251
    .line 252
    throw p2

    .line 253
    :cond_5
    return-void
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

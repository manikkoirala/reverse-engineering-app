.class final Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;
.super Ljava/lang/Object;
.source "YearFracCalculator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;
    }
.end annotation


# static fields
.field private static final 〇080:Ljava/util/TimeZone;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-string v0, "UTC"

    .line 2
    .line 3
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->〇080:Ljava/util/TimeZone;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static O8(II)D
    .locals 2

    .line 1
    sub-int/2addr p1, p0

    .line 2
    int-to-double p0, p1

    .line 3
    const-wide v0, 0x4076800000000000L    # 360.0

    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    div-double/2addr p0, v0

    .line 9
    return-wide p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static OO0o〇〇(Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;)Z
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o〇:I

    .line 2
    .line 3
    const/16 v1, 0x1c

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-ge v0, v1, :cond_0

    .line 7
    .line 8
    return v2

    .line 9
    :cond_0
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->〇8o8o〇(Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;)I

    .line 10
    .line 11
    .line 12
    move-result p0

    .line 13
    if-ne v0, p0, :cond_1

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    :cond_1
    return v2
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static OO0o〇〇〇〇0(JJ)I
    .locals 6

    .line 1
    sub-long v0, p2, p0

    .line 2
    .line 3
    const-wide/32 v2, 0x5265c00

    .line 4
    .line 5
    .line 6
    rem-long v2, v0, v2

    .line 7
    .line 8
    const-wide/32 v4, 0x36ee80

    .line 9
    .line 10
    .line 11
    div-long/2addr v2, v4

    .line 12
    long-to-int v3, v2

    .line 13
    if-nez v3, :cond_0

    .line 14
    .line 15
    long-to-double p0, v0

    .line 16
    const-wide p2, 0x4194997000000000L    # 8.64E7

    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    div-double/2addr p0, p2

    .line 22
    const-wide/high16 p2, 0x3fe0000000000000L    # 0.5

    .line 23
    .line 24
    add-double/2addr p0, p2

    .line 25
    double-to-int p0, p0

    .line 26
    return p0

    .line 27
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    .line 28
    .line 29
    new-instance v1, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v2, "Unexpected date diff between "

    .line 35
    .line 36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    const-string p0, " and "

    .line 43
    .line 44
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object p0

    .line 54
    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    throw v0
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static Oo08(DD)D
    .locals 0

    .line 1
    sub-double/2addr p2, p0

    .line 2
    const-wide p0, 0x4076d00000000000L    # 365.0

    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    div-double/2addr p2, p0

    .line 8
    return-wide p2
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static Oooo8o0〇(I)Z
    .locals 3

    .line 1
    rem-int/lit8 v0, p0, 0x4

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    rem-int/lit16 v0, p0, 0x190

    .line 8
    .line 9
    const/4 v2, 0x1

    .line 10
    if-nez v0, :cond_1

    .line 11
    .line 12
    return v2

    .line 13
    :cond_1
    rem-int/lit8 p0, p0, 0x64

    .line 14
    .line 15
    if-nez p0, :cond_2

    .line 16
    .line 17
    return v1

    .line 18
    :cond_2
    return v2
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static oO80(Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;II)D
    .locals 2

    .line 1
    iget v0, p1, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇080:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇080:I

    .line 4
    .line 5
    sub-int/2addr v0, v1

    .line 6
    mul-int/lit16 v0, v0, 0x168

    .line 7
    .line 8
    iget p1, p1, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o00〇〇Oo:I

    .line 9
    .line 10
    iget p0, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o00〇〇Oo:I

    .line 11
    .line 12
    sub-int/2addr p1, p0

    .line 13
    mul-int/lit8 p1, p1, 0x1e

    .line 14
    .line 15
    add-int/2addr v0, p1

    .line 16
    sub-int/2addr p3, p2

    .line 17
    mul-int/lit8 p3, p3, 0x1

    .line 18
    .line 19
    add-int/2addr v0, p3

    .line 20
    int-to-double p0, v0

    .line 21
    const-wide p2, 0x4076800000000000L    # 360.0

    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    div-double/2addr p0, p2

    .line 27
    return-wide p0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public static o〇0(II)D
    .locals 4

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->〇80〇808〇O(I)Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->〇80〇808〇O(I)Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o〇:I

    .line 10
    .line 11
    iget v1, p1, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o〇:I

    .line 12
    .line 13
    const/16 v2, 0x1e

    .line 14
    .line 15
    const/16 v3, 0x1f

    .line 16
    .line 17
    if-ne v0, v3, :cond_0

    .line 18
    .line 19
    const/16 v0, 0x1e

    .line 20
    .line 21
    :cond_0
    if-ne v1, v3, :cond_1

    .line 22
    .line 23
    const/16 v1, 0x1e

    .line 24
    .line 25
    :cond_1
    invoke-static {p0, p1, v0, v1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->oO80(Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;II)D

    .line 26
    .line 27
    .line 28
    move-result-wide p0

    .line 29
    return-wide p0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static 〇080(II)D
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    move v1, p0

    .line 3
    :goto_0
    if-gt v1, p1, :cond_1

    .line 4
    .line 5
    add-int/lit16 v0, v0, 0x16d

    .line 6
    .line 7
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->Oooo8o0〇(I)Z

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    if-eqz v2, :cond_0

    .line 12
    .line 13
    add-int/lit8 v0, v0, 0x1

    .line 14
    .line 15
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    sub-int/2addr p1, p0

    .line 19
    add-int/lit8 p1, p1, 0x1

    .line 20
    .line 21
    int-to-double p0, p1

    .line 22
    int-to-double v0, v0

    .line 23
    div-double/2addr v0, p0

    .line 24
    return-wide v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static 〇80〇808〇O(I)Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;
    .locals 2

    .line 1
    new-instance v0, Ljava/util/GregorianCalendar;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->〇080:Ljava/util/TimeZone;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 6
    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    invoke-static {v0, p0, v1, v1}, Lcom/intsig/office/ss/util/DateUtil;->setCalendar(Ljava/util/Calendar;IIZ)V

    .line 10
    .line 11
    .line 12
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;

    .line 13
    .line 14
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;-><init>(Ljava/util/Calendar;)V

    .line 15
    .line 16
    .line 17
    return-object p0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static 〇8o8o〇(Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;)I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o00〇〇Oo:I

    .line 2
    .line 3
    packed-switch v0, :pswitch_data_0

    .line 4
    .line 5
    .line 6
    :pswitch_0
    iget p0, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇080:I

    .line 7
    .line 8
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->Oooo8o0〇(I)Z

    .line 9
    .line 10
    .line 11
    move-result p0

    .line 12
    if-eqz p0, :cond_0

    .line 13
    .line 14
    const/16 p0, 0x1d

    .line 15
    .line 16
    return p0

    .line 17
    :pswitch_1
    const/16 p0, 0x1e

    .line 18
    .line 19
    return p0

    .line 20
    :pswitch_2
    const/16 p0, 0x1f

    .line 21
    .line 22
    return p0

    .line 23
    :cond_0
    const/16 p0, 0x1c

    .line 24
    .line 25
    return p0

    .line 26
    nop

    .line 27
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private static 〇O8o08O(Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;)Z
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇080:I

    .line 2
    .line 3
    iget v1, p1, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇080:I

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    return v2

    .line 9
    :cond_0
    const/4 v3, 0x1

    .line 10
    add-int/2addr v0, v3

    .line 11
    if-eq v0, v1, :cond_1

    .line 12
    .line 13
    return v3

    .line 14
    :cond_1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o00〇〇Oo:I

    .line 15
    .line 16
    iget v1, p1, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o00〇〇Oo:I

    .line 17
    .line 18
    if-le v0, v1, :cond_2

    .line 19
    .line 20
    return v2

    .line 21
    :cond_2
    if-ge v0, v1, :cond_3

    .line 22
    .line 23
    return v3

    .line 24
    :cond_3
    iget p0, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o〇:I

    .line 25
    .line 26
    iget p1, p1, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o〇:I

    .line 27
    .line 28
    if-ge p0, p1, :cond_4

    .line 29
    .line 30
    const/4 v2, 0x1

    .line 31
    :cond_4
    return v2
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static 〇o00〇〇Oo(II)D
    .locals 5

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->〇80〇808〇O(I)Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->〇80〇808〇O(I)Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o〇:I

    .line 10
    .line 11
    iget v1, p1, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o〇:I

    .line 12
    .line 13
    const/16 v2, 0x1e

    .line 14
    .line 15
    const/16 v3, 0x1f

    .line 16
    .line 17
    if-ne v0, v3, :cond_0

    .line 18
    .line 19
    if-ne v1, v3, :cond_0

    .line 20
    .line 21
    :goto_0
    const/16 v0, 0x1e

    .line 22
    .line 23
    :goto_1
    const/16 v1, 0x1e

    .line 24
    .line 25
    goto :goto_2

    .line 26
    :cond_0
    if-ne v0, v3, :cond_2

    .line 27
    .line 28
    :cond_1
    const/16 v0, 0x1e

    .line 29
    .line 30
    goto :goto_2

    .line 31
    :cond_2
    if-ne v0, v2, :cond_3

    .line 32
    .line 33
    if-ne v1, v3, :cond_3

    .line 34
    .line 35
    goto :goto_1

    .line 36
    :cond_3
    iget v3, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o00〇〇Oo:I

    .line 37
    .line 38
    const/4 v4, 0x2

    .line 39
    if-ne v3, v4, :cond_4

    .line 40
    .line 41
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->OO0o〇〇(Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;)Z

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    if-eqz v3, :cond_4

    .line 46
    .line 47
    iget v0, p1, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o00〇〇Oo:I

    .line 48
    .line 49
    if-ne v0, v4, :cond_1

    .line 50
    .line 51
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->OO0o〇〇(Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;)Z

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-eqz v0, :cond_1

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_4
    :goto_2
    invoke-static {p0, p1, v0, v1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->oO80(Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;II)D

    .line 59
    .line 60
    .line 61
    move-result-wide p0

    .line 62
    return-wide p0
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static 〇o〇(II)D
    .locals 4

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->〇80〇808〇O(I)Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->〇80〇808〇O(I)Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->〇O8o08O(Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇080:I

    .line 16
    .line 17
    iget v1, p1, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇080:I

    .line 18
    .line 19
    invoke-static {v0, v1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->〇080(II)D

    .line 20
    .line 21
    .line 22
    move-result-wide v0

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->〇〇808〇(Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;)Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    const-wide v0, 0x4076e00000000000L    # 366.0

    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    const-wide v0, 0x4076d00000000000L    # 365.0

    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    :goto_0
    iget-wide v2, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->O8:J

    .line 42
    .line 43
    iget-wide p0, p1, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->O8:J

    .line 44
    .line 45
    invoke-static {v2, v3, p0, p1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->OO0o〇〇〇〇0(JJ)I

    .line 46
    .line 47
    .line 48
    move-result p0

    .line 49
    int-to-double p0, p0

    .line 50
    div-double/2addr p0, v0

    .line 51
    return-wide p0
    .line 52
    .line 53
.end method

.method private static 〇〇808〇(Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;)Z
    .locals 5

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇080:I

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->Oooo8o0〇(I)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget v2, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇080:I

    .line 11
    .line 12
    iget v3, p1, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇080:I

    .line 13
    .line 14
    if-ne v2, v3, :cond_0

    .line 15
    .line 16
    return v1

    .line 17
    :cond_0
    iget v2, p1, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇080:I

    .line 18
    .line 19
    invoke-static {v2}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->Oooo8o0〇(I)Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    const/4 v3, 0x0

    .line 24
    if-nez v0, :cond_1

    .line 25
    .line 26
    if-nez v2, :cond_1

    .line 27
    .line 28
    return v3

    .line 29
    :cond_1
    const/4 v4, 0x2

    .line 30
    if-eqz v0, :cond_3

    .line 31
    .line 32
    iget p0, p0, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o00〇〇Oo:I

    .line 33
    .line 34
    if-eq p0, v1, :cond_2

    .line 35
    .line 36
    if-eq p0, v4, :cond_2

    .line 37
    .line 38
    return v3

    .line 39
    :cond_2
    return v1

    .line 40
    :cond_3
    if-eqz v2, :cond_6

    .line 41
    .line 42
    iget p0, p1, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o00〇〇Oo:I

    .line 43
    .line 44
    if-eq p0, v1, :cond_6

    .line 45
    .line 46
    if-eq p0, v4, :cond_4

    .line 47
    .line 48
    return v1

    .line 49
    :cond_4
    iget p0, p1, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator$SimpleDate;->〇o〇:I

    .line 50
    .line 51
    const/16 p1, 0x1d

    .line 52
    .line 53
    if-ne p0, p1, :cond_5

    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_5
    const/4 v1, 0x0

    .line 57
    :goto_0
    return v1

    .line 58
    :cond_6
    return v3
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static 〇〇888(DDI)D
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation

    .line 1
    if-ltz p4, :cond_7

    .line 2
    .line 3
    const/4 v0, 0x5

    .line 4
    if-ge p4, v0, :cond_7

    .line 5
    .line 6
    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    .line 7
    .line 8
    .line 9
    move-result-wide p0

    .line 10
    double-to-int p0, p0

    .line 11
    invoke-static {p2, p3}, Ljava/lang/Math;->floor(D)D

    .line 12
    .line 13
    .line 14
    move-result-wide p1

    .line 15
    double-to-int p1, p1

    .line 16
    if-ne p0, p1, :cond_0

    .line 17
    .line 18
    const-wide/16 p0, 0x0

    .line 19
    .line 20
    return-wide p0

    .line 21
    :cond_0
    if-le p0, p1, :cond_1

    .line 22
    .line 23
    move v1, p1

    .line 24
    move p1, p0

    .line 25
    move p0, v1

    .line 26
    :cond_1
    if-eqz p4, :cond_6

    .line 27
    .line 28
    const/4 p2, 0x1

    .line 29
    if-eq p4, p2, :cond_5

    .line 30
    .line 31
    const/4 p2, 0x2

    .line 32
    if-eq p4, p2, :cond_4

    .line 33
    .line 34
    const/4 p2, 0x3

    .line 35
    if-eq p4, p2, :cond_3

    .line 36
    .line 37
    const/4 p2, 0x4

    .line 38
    if-ne p4, p2, :cond_2

    .line 39
    .line 40
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->o〇0(II)D

    .line 41
    .line 42
    .line 43
    move-result-wide p0

    .line 44
    return-wide p0

    .line 45
    :cond_2
    new-instance p0, Ljava/lang/IllegalStateException;

    .line 46
    .line 47
    const-string p1, "cannot happen"

    .line 48
    .line 49
    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    throw p0

    .line 53
    :cond_3
    int-to-double p2, p0

    .line 54
    int-to-double p0, p1

    .line 55
    invoke-static {p2, p3, p0, p1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->Oo08(DD)D

    .line 56
    .line 57
    .line 58
    move-result-wide p0

    .line 59
    return-wide p0

    .line 60
    :cond_4
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->O8(II)D

    .line 61
    .line 62
    .line 63
    move-result-wide p0

    .line 64
    return-wide p0

    .line 65
    :cond_5
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->〇o〇(II)D

    .line 66
    .line 67
    .line 68
    move-result-wide p0

    .line 69
    return-wide p0

    .line 70
    :cond_6
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/formula/atp/YearFracCalculator;->〇o00〇〇Oo(II)D

    .line 71
    .line 72
    .line 73
    move-result-wide p0

    .line 74
    return-wide p0

    .line 75
    :cond_7
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    .line 76
    .line 77
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->NUM_ERROR:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 78
    .line 79
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;)V

    .line 80
    .line 81
    .line 82
    throw p0
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

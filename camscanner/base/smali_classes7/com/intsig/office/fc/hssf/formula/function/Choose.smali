.class public final Lcom/intsig/office/fc/hssf/formula/function/Choose;
.super Ljava/lang/Object;
.source "Choose.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/function/Function;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static evaluateFirstArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->getSingleValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->coerceValueToInt(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)I

    .line 6
    .line 7
    .line 8
    move-result p0

    .line 9
    return p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public evaluate([Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 2

    .line 1
    array-length v0, p1

    .line 2
    const/4 v1, 0x2

    .line 3
    if-ge v0, v1, :cond_0

    .line 4
    .line 5
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 6
    .line 7
    return-object p1

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :try_start_0
    aget-object v0, p1, v0

    .line 10
    .line 11
    invoke-static {v0, p2, p3}, Lcom/intsig/office/fc/hssf/formula/function/Choose;->evaluateFirstArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    const/4 v1, 0x1

    .line 16
    if-lt v0, v1, :cond_3

    .line 17
    .line 18
    array-length v1, p1

    .line 19
    if-lt v0, v1, :cond_1

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_1
    aget-object p1, p1, v0

    .line 23
    .line 24
    invoke-static {p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->getSingleValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    sget-object p2, Lcom/intsig/office/fc/hssf/formula/eval/MissingArgEval;->instance:Lcom/intsig/office/fc/hssf/formula/eval/MissingArgEval;

    .line 29
    .line 30
    if-ne p1, p2, :cond_2

    .line 31
    .line 32
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;->instance:Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;

    .line 33
    .line 34
    :cond_2
    return-object p1

    .line 35
    :cond_3
    :goto_0
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    .line 37
    return-object p1

    .line 38
    :catch_0
    move-exception p1

    .line 39
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    return-object p1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

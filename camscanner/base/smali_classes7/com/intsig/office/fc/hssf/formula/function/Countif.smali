.class public final Lcom/intsig/office/fc/hssf/formula/function/Countif;
.super Lcom/intsig/office/fc/hssf/formula/function/Fixed2ArgFunction;
.source "Countif.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/formula/function/Countif$StringMatcher;,
        Lcom/intsig/office/fc/hssf/formula/function/Countif$ErrorMatcher;,
        Lcom/intsig/office/fc/hssf/formula/function/Countif$BooleanMatcher;,
        Lcom/intsig/office/fc/hssf/formula/function/Countif$NumberMatcher;,
        Lcom/intsig/office/fc/hssf/formula/function/Countif$MatcherBase;,
        Lcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/function/Fixed2ArgFunction;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private countMatchingCellsInArea(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;)D
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/formula/eval/RefEval;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/RefEval;

    .line 6
    .line 7
    invoke-static {p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/CountUtils;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/eval/RefEval;Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;)I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    :goto_0
    int-to-double p1, p1

    .line 12
    return-wide p1

    .line 13
    :cond_0
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/formula/TwoDEval;

    .line 14
    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/TwoDEval;

    .line 18
    .line 19
    invoke-static {p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/CountUtils;->〇o〇(Lcom/intsig/office/fc/hssf/formula/TwoDEval;Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;)I

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    goto :goto_0

    .line 24
    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    .line 25
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v1, "Bad range arg type ("

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    const-string p1, ")"

    .line 48
    .line 49
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    throw p2
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method static createCriteriaPredicate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;
    .locals 2

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/Countif;->evaluateCriteriaArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    instance-of p1, p0, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 6
    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/function/Countif$NumberMatcher;

    .line 10
    .line 11
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;->getNumberValue()D

    .line 14
    .line 15
    .line 16
    move-result-wide v0

    .line 17
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;->〇o〇:Lcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;

    .line 18
    .line 19
    invoke-direct {p1, v0, v1, p0}, Lcom/intsig/office/fc/hssf/formula/function/Countif$NumberMatcher;-><init>(DLcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;)V

    .line 20
    .line 21
    .line 22
    return-object p1

    .line 23
    :cond_0
    instance-of p1, p0, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 24
    .line 25
    if-eqz p1, :cond_1

    .line 26
    .line 27
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/function/Countif$BooleanMatcher;

    .line 28
    .line 29
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;->getBooleanValue()Z

    .line 32
    .line 33
    .line 34
    move-result p0

    .line 35
    sget-object p2, Lcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;->〇o〇:Lcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;

    .line 36
    .line 37
    invoke-direct {p1, p0, p2}, Lcom/intsig/office/fc/hssf/formula/function/Countif$BooleanMatcher;-><init>(ZLcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;)V

    .line 38
    .line 39
    .line 40
    return-object p1

    .line 41
    :cond_1
    instance-of p1, p0, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;

    .line 42
    .line 43
    if-eqz p1, :cond_2

    .line 44
    .line 45
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;

    .line 46
    .line 47
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/Countif;->createGeneralMatchPredicate(Lcom/intsig/office/fc/hssf/formula/eval/StringEval;)Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;

    .line 48
    .line 49
    .line 50
    move-result-object p0

    .line 51
    return-object p0

    .line 52
    :cond_2
    instance-of p1, p0, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 53
    .line 54
    if-eqz p1, :cond_3

    .line 55
    .line 56
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/function/Countif$ErrorMatcher;

    .line 57
    .line 58
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 59
    .line 60
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->getErrorCode()I

    .line 61
    .line 62
    .line 63
    move-result p0

    .line 64
    sget-object p2, Lcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;->〇o〇:Lcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;

    .line 65
    .line 66
    invoke-direct {p1, p0, p2}, Lcom/intsig/office/fc/hssf/formula/function/Countif$ErrorMatcher;-><init>(ILcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;)V

    .line 67
    .line 68
    .line 69
    return-object p1

    .line 70
    :cond_3
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;->instance:Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;

    .line 71
    .line 72
    if-ne p0, p1, :cond_4

    .line 73
    .line 74
    const/4 p0, 0x0

    .line 75
    return-object p0

    .line 76
    :cond_4
    new-instance p1, Ljava/lang/RuntimeException;

    .line 77
    .line 78
    new-instance p2, Ljava/lang/StringBuilder;

    .line 79
    .line 80
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 81
    .line 82
    .line 83
    const-string v0, "Unexpected type for criteria ("

    .line 84
    .line 85
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 89
    .line 90
    .line 91
    move-result-object p0

    .line 92
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object p0

    .line 96
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    const-string p0, ")"

    .line 100
    .line 101
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object p0

    .line 108
    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    throw p1
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private static createGeneralMatchPredicate(Lcom/intsig/office/fc/hssf/formula/eval/StringEval;)Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;->getStringValue()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;->Oo08(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;->O8()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/Countif;->parseBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    if-eqz v1, :cond_0

    .line 22
    .line 23
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/function/Countif$BooleanMatcher;

    .line 24
    .line 25
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    invoke-direct {p0, v1, v0}, Lcom/intsig/office/fc/hssf/formula/function/Countif$BooleanMatcher;-><init>(ZLcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;)V

    .line 30
    .line 31
    .line 32
    return-object p0

    .line 33
    :cond_0
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->parseDouble(Ljava/lang/String;)Ljava/lang/Double;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    if-eqz v1, :cond_1

    .line 38
    .line 39
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/function/Countif$NumberMatcher;

    .line 40
    .line 41
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    .line 42
    .line 43
    .line 44
    move-result-wide v1

    .line 45
    invoke-direct {p0, v1, v2, v0}, Lcom/intsig/office/fc/hssf/formula/function/Countif$NumberMatcher;-><init>(DLcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;)V

    .line 46
    .line 47
    .line 48
    return-object p0

    .line 49
    :cond_1
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/Countif;->parseError(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    if-eqz v1, :cond_2

    .line 54
    .line 55
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/function/Countif$ErrorMatcher;

    .line 56
    .line 57
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->getErrorCode()I

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    invoke-direct {p0, v1, v0}, Lcom/intsig/office/fc/hssf/formula/function/Countif$ErrorMatcher;-><init>(ILcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;)V

    .line 62
    .line 63
    .line 64
    return-object p0

    .line 65
    :cond_2
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/function/Countif$StringMatcher;

    .line 66
    .line 67
    invoke-direct {v1, p0, v0}, Lcom/intsig/office/fc/hssf/formula/function/Countif$StringMatcher;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/hssf/formula/function/Countif$CmpOp;)V

    .line 68
    .line 69
    .line 70
    return-object v1
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private static evaluateCriteriaArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 0

    .line 1
    int-to-short p2, p2

    .line 2
    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->getSingleValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 3
    .line 4
    .line 5
    move-result-object p0
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    return-object p0

    .line 7
    :catch_0
    move-exception p0

    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 9
    .line 10
    .line 11
    move-result-object p0

    .line 12
    return-object p0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method static parseBoolean(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 3

    .line 1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    const/4 v2, 0x0

    .line 7
    if-ge v0, v1, :cond_0

    .line 8
    .line 9
    return-object v2

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    const/16 v1, 0x46

    .line 16
    .line 17
    if-eq v0, v1, :cond_2

    .line 18
    .line 19
    const/16 v1, 0x54

    .line 20
    .line 21
    if-eq v0, v1, :cond_1

    .line 22
    .line 23
    const/16 v1, 0x66

    .line 24
    .line 25
    if-eq v0, v1, :cond_2

    .line 26
    .line 27
    const/16 v1, 0x74

    .line 28
    .line 29
    if-eq v0, v1, :cond_1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    const-string v0, "TRUE"

    .line 33
    .line 34
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 35
    .line 36
    .line 37
    move-result p0

    .line 38
    if-eqz p0, :cond_3

    .line 39
    .line 40
    sget-object p0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 41
    .line 42
    return-object p0

    .line 43
    :cond_2
    const-string v0, "FALSE"

    .line 44
    .line 45
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 46
    .line 47
    .line 48
    move-result p0

    .line 49
    if-eqz p0, :cond_3

    .line 50
    .line 51
    sget-object p0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 52
    .line 53
    return-object p0

    .line 54
    :cond_3
    :goto_0
    return-object v2
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private static parseError(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;
    .locals 3

    .line 1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x4

    .line 6
    const/4 v2, 0x0

    .line 7
    if-lt v0, v1, :cond_7

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    const/16 v1, 0x23

    .line 15
    .line 16
    if-eq v0, v1, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const-string v0, "#NULL!"

    .line 20
    .line 21
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->NULL_INTERSECTION:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 28
    .line 29
    return-object p0

    .line 30
    :cond_1
    const-string v0, "#DIV/0!"

    .line 31
    .line 32
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->DIV_ZERO:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 39
    .line 40
    return-object p0

    .line 41
    :cond_2
    const-string v0, "#VALUE!"

    .line 42
    .line 43
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-eqz v0, :cond_3

    .line 48
    .line 49
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 50
    .line 51
    return-object p0

    .line 52
    :cond_3
    const-string v0, "#REF!"

    .line 53
    .line 54
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    if-eqz v0, :cond_4

    .line 59
    .line 60
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->REF_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 61
    .line 62
    return-object p0

    .line 63
    :cond_4
    const-string v0, "#NAME?"

    .line 64
    .line 65
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    if-eqz v0, :cond_5

    .line 70
    .line 71
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->NAME_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 72
    .line 73
    return-object p0

    .line 74
    :cond_5
    const-string v0, "#NUM!"

    .line 75
    .line 76
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    if-eqz v0, :cond_6

    .line 81
    .line 82
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->NUM_ERROR:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 83
    .line 84
    return-object p0

    .line 85
    :cond_6
    const-string v0, "#N/A"

    .line 86
    .line 87
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 88
    .line 89
    .line 90
    move-result p0

    .line 91
    if-eqz p0, :cond_7

    .line 92
    .line 93
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->NA:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 94
    .line 95
    return-object p0

    .line 96
    :cond_7
    :goto_0
    return-object v2
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method


# virtual methods
.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 0

    .line 1
    invoke-static {p4, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/Countif;->createCriteriaPredicate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-nez p1, :cond_0

    .line 6
    .line 7
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;->ZERO:Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 8
    .line 9
    return-object p1

    .line 10
    :cond_0
    invoke-direct {p0, p3, p1}, Lcom/intsig/office/fc/hssf/formula/function/Countif;->countMatchingCellsInArea(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;)D

    .line 11
    .line 12
    .line 13
    move-result-wide p1

    .line 14
    new-instance p3, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 15
    .line 16
    invoke-direct {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    .line 17
    .line 18
    .line 19
    return-object p3
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

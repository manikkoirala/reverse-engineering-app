.class public final Lcom/intsig/office/fc/hssf/formula/function/Irr;
.super Ljava/lang/Object;
.source "Irr.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/function/Function;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static irr([D)D
    .locals 2

    const-wide v0, 0x3fb999999999999aL    # 0.1

    .line 1
    invoke-static {p0, v0, v1}, Lcom/intsig/office/fc/hssf/formula/function/Irr;->irr([DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static irr([DD)D
    .locals 13

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    const/16 v2, 0x14

    if-ge v1, v2, :cond_2

    const-wide/16 v2, 0x0

    move-wide v4, v2

    const/4 v6, 0x0

    .line 2
    :goto_1
    array-length v7, p0

    if-ge v6, v7, :cond_0

    .line 3
    aget-wide v7, p0, v6

    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    add-double/2addr v9, p1

    int-to-double v11, v6

    invoke-static {v9, v10, v11, v12}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v11

    div-double/2addr v7, v11

    add-double/2addr v2, v7

    neg-int v7, v6

    int-to-double v7, v7

    .line 4
    aget-wide v11, p0, v6

    mul-double v7, v7, v11

    add-int/lit8 v6, v6, 0x1

    int-to-double v11, v6

    invoke-static {v9, v10, v11, v12}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    div-double/2addr v7, v9

    add-double/2addr v4, v7

    goto :goto_1

    :cond_0
    div-double/2addr v2, v4

    sub-double v2, p1, v2

    sub-double p1, v2, p1

    .line 5
    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    const-wide v4, 0x3e7ad7f29abcaf48L    # 1.0E-7

    cmpg-double v6, p1, v4

    if-gtz v6, :cond_1

    return-wide v2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    move-wide p1, v2

    goto :goto_0

    :cond_2
    const-wide/high16 p0, 0x7ff8000000000000L    # Double.NaN

    return-wide p0
.end method


# virtual methods
.method public evaluate([Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 5

    .line 1
    array-length v0, p1

    .line 2
    if-eqz v0, :cond_2

    .line 3
    .line 4
    array-length v0, p1

    .line 5
    const/4 v1, 0x2

    .line 6
    if-le v0, v1, :cond_0

    .line 7
    .line 8
    goto :goto_1

    .line 9
    :cond_0
    const/4 v0, 0x1

    .line 10
    :try_start_0
    new-array v2, v0, [Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    aget-object v4, p1, v3

    .line 14
    .line 15
    aput-object v4, v2, v3

    .line 16
    .line 17
    invoke-static {v2}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction$ValueCollector;->〇080([Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)[D

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    array-length v3, p1

    .line 22
    if-ne v3, v1, :cond_1

    .line 23
    .line 24
    aget-object p1, p1, v0

    .line 25
    .line 26
    invoke-static {p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    .line 27
    .line 28
    .line 29
    move-result-wide p1

    .line 30
    goto :goto_0

    .line 31
    :cond_1
    const-wide p1, 0x3fb999999999999aL    # 0.1

    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    :goto_0
    invoke-static {v2, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/Irr;->irr([DD)D

    .line 37
    .line 38
    .line 39
    move-result-wide p1

    .line 40
    invoke-static {p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->checkValue(D)V

    .line 41
    .line 42
    .line 43
    new-instance p3, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 44
    .line 45
    invoke-direct {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    .line 47
    .line 48
    return-object p3

    .line 49
    :catch_0
    move-exception p1

    .line 50
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    return-object p1

    .line 55
    :cond_2
    :goto_1
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 56
    .line 57
    return-object p1
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.class final Lcom/intsig/office/fc/hssf/formula/function/StatsLib;
.super Ljava/lang/Object;
.source "StatsLib.java"


# direct methods
.method public static O8([DI)D
    .locals 2

    .line 1
    add-int/lit8 p1, p1, -0x1

    .line 2
    .line 3
    if-eqz p0, :cond_0

    .line 4
    .line 5
    array-length v0, p0

    .line 6
    if-le v0, p1, :cond_0

    .line 7
    .line 8
    if-ltz p1, :cond_0

    .line 9
    .line 10
    invoke-static {p0}, Ljava/util/Arrays;->sort([D)V

    .line 11
    .line 12
    .line 13
    aget-wide v0, p0, p1

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const-wide/high16 v0, 0x7ff8000000000000L    # Double.NaN

    .line 17
    .line 18
    :goto_0
    return-wide v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static Oo08([D)D
    .locals 4

    .line 1
    if-eqz p0, :cond_1

    .line 2
    .line 3
    array-length v0, p0

    .line 4
    const/4 v1, 0x1

    .line 5
    if-lt v0, v1, :cond_1

    .line 6
    .line 7
    array-length v0, p0

    .line 8
    invoke-static {p0}, Ljava/util/Arrays;->sort([D)V

    .line 9
    .line 10
    .line 11
    rem-int/lit8 v2, v0, 0x2

    .line 12
    .line 13
    if-nez v2, :cond_0

    .line 14
    .line 15
    div-int/lit8 v0, v0, 0x2

    .line 16
    .line 17
    aget-wide v2, p0, v0

    .line 18
    .line 19
    sub-int/2addr v0, v1

    .line 20
    aget-wide v0, p0, v0

    .line 21
    .line 22
    add-double/2addr v2, v0

    .line 23
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    .line 24
    .line 25
    div-double/2addr v2, v0

    .line 26
    goto :goto_0

    .line 27
    :cond_0
    div-int/lit8 v0, v0, 0x2

    .line 28
    .line 29
    aget-wide v2, p0, v0

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    const-wide/high16 v2, 0x7ff8000000000000L    # Double.NaN

    .line 33
    .line 34
    :goto_0
    return-wide v2
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static oO80([D)D
    .locals 4

    .line 1
    if-eqz p0, :cond_0

    .line 2
    .line 3
    array-length v0, p0

    .line 4
    const/4 v1, 0x1

    .line 5
    if-le v0, v1, :cond_0

    .line 6
    .line 7
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/StatsLib;->〇o00〇〇Oo([D)D

    .line 8
    .line 9
    .line 10
    move-result-wide v0

    .line 11
    array-length p0, p0

    .line 12
    int-to-double v2, p0

    .line 13
    div-double/2addr v0, v2

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const-wide/high16 v0, 0x7ff8000000000000L    # Double.NaN

    .line 16
    .line 17
    :goto_0
    return-wide v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static o〇0([D)D
    .locals 4

    .line 1
    if-eqz p0, :cond_0

    .line 2
    .line 3
    array-length v0, p0

    .line 4
    const/4 v1, 0x1

    .line 5
    if-le v0, v1, :cond_0

    .line 6
    .line 7
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/StatsLib;->〇o00〇〇Oo([D)D

    .line 8
    .line 9
    .line 10
    move-result-wide v2

    .line 11
    array-length p0, p0

    .line 12
    sub-int/2addr p0, v1

    .line 13
    int-to-double v0, p0

    .line 14
    div-double/2addr v2, v0

    .line 15
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    .line 16
    .line 17
    .line 18
    move-result-wide v0

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const-wide/high16 v0, 0x7ff8000000000000L    # Double.NaN

    .line 21
    .line 22
    :goto_0
    return-wide v0
    .line 23
    .line 24
.end method

.method public static 〇080([D)D
    .locals 9

    .line 1
    array-length v0, p0

    .line 2
    const-wide/16 v1, 0x0

    .line 3
    .line 4
    const/4 v3, 0x0

    .line 5
    move-wide v5, v1

    .line 6
    const/4 v4, 0x0

    .line 7
    :goto_0
    if-ge v4, v0, :cond_0

    .line 8
    .line 9
    aget-wide v7, p0, v4

    .line 10
    .line 11
    add-double/2addr v5, v7

    .line 12
    add-int/lit8 v4, v4, 0x1

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    array-length v0, p0

    .line 16
    int-to-double v7, v0

    .line 17
    div-double/2addr v5, v7

    .line 18
    array-length v0, p0

    .line 19
    :goto_1
    if-ge v3, v0, :cond_1

    .line 20
    .line 21
    aget-wide v7, p0, v3

    .line 22
    .line 23
    sub-double/2addr v7, v5

    .line 24
    invoke-static {v7, v8}, Ljava/lang/Math;->abs(D)D

    .line 25
    .line 26
    .line 27
    move-result-wide v7

    .line 28
    add-double/2addr v1, v7

    .line 29
    add-int/lit8 v3, v3, 0x1

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_1
    array-length p0, p0

    .line 33
    int-to-double v3, p0

    .line 34
    div-double/2addr v1, v3

    .line 35
    return-wide v1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static 〇o00〇〇Oo([D)D
    .locals 14

    .line 1
    if-eqz p0, :cond_3

    .line 2
    .line 3
    array-length v0, p0

    .line 4
    const/4 v1, 0x1

    .line 5
    if-lt v0, v1, :cond_3

    .line 6
    .line 7
    array-length v0, p0

    .line 8
    const/4 v2, 0x0

    .line 9
    const-wide/16 v3, 0x0

    .line 10
    .line 11
    move-wide v6, v3

    .line 12
    const/4 v5, 0x0

    .line 13
    :goto_0
    if-ge v5, v0, :cond_0

    .line 14
    .line 15
    aget-wide v8, p0, v5

    .line 16
    .line 17
    add-double/2addr v6, v8

    .line 18
    add-int/lit8 v5, v5, 0x1

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    int-to-double v8, v0

    .line 22
    div-double/2addr v6, v8

    .line 23
    move-wide v8, v3

    .line 24
    :goto_1
    if-ge v2, v0, :cond_1

    .line 25
    .line 26
    aget-wide v10, p0, v2

    .line 27
    .line 28
    sub-double v12, v10, v6

    .line 29
    .line 30
    sub-double/2addr v10, v6

    .line 31
    mul-double v12, v12, v10

    .line 32
    .line 33
    add-double/2addr v8, v12

    .line 34
    add-int/lit8 v2, v2, 0x1

    .line 35
    .line 36
    goto :goto_1

    .line 37
    :cond_1
    if-ne v0, v1, :cond_2

    .line 38
    .line 39
    goto :goto_2

    .line 40
    :cond_2
    move-wide v3, v8

    .line 41
    goto :goto_2

    .line 42
    :cond_3
    const-wide/high16 v3, 0x7ff8000000000000L    # Double.NaN

    .line 43
    .line 44
    :goto_2
    return-wide v3
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static 〇o〇([DI)D
    .locals 2

    .line 1
    add-int/lit8 p1, p1, -0x1

    .line 2
    .line 3
    if-eqz p0, :cond_0

    .line 4
    .line 5
    array-length v0, p0

    .line 6
    if-le v0, p1, :cond_0

    .line 7
    .line 8
    if-ltz p1, :cond_0

    .line 9
    .line 10
    invoke-static {p0}, Ljava/util/Arrays;->sort([D)V

    .line 11
    .line 12
    .line 13
    array-length v0, p0

    .line 14
    sub-int/2addr v0, p1

    .line 15
    add-int/lit8 v0, v0, -0x1

    .line 16
    .line 17
    aget-wide v0, p0, v0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const-wide/high16 v0, 0x7ff8000000000000L    # Double.NaN

    .line 21
    .line 22
    :goto_0
    return-wide v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static 〇〇888([D)D
    .locals 4

    .line 1
    if-eqz p0, :cond_0

    .line 2
    .line 3
    array-length v0, p0

    .line 4
    const/4 v1, 0x1

    .line 5
    if-le v0, v1, :cond_0

    .line 6
    .line 7
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/StatsLib;->〇o00〇〇Oo([D)D

    .line 8
    .line 9
    .line 10
    move-result-wide v2

    .line 11
    array-length p0, p0

    .line 12
    sub-int/2addr p0, v1

    .line 13
    int-to-double v0, p0

    .line 14
    div-double/2addr v2, v0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const-wide/high16 v2, 0x7ff8000000000000L    # Double.NaN

    .line 17
    .line 18
    :goto_0
    return-wide v2
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class final Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;
.super Ljava/lang/Object;
.source "ForkedEvaluationCell.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/EvaluationCell;


# instance fields
.field private O8:I

.field private Oo08:I

.field private o〇0:D

.field private final 〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;

.field private final 〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/EvaluationCell;

.field private 〇o〇:Z

.field private 〇〇888:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationSheet;Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/EvaluationCell;

    .line 7
    .line 8
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;->instance:Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;

    .line 9
    .line 10
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private 〇080(I)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->O8:I

    .line 2
    .line 3
    if-ne v0, p1, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 7
    .line 8
    new-instance v0, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    const-string v1, "Wrong data type ("

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->O8:I

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-string v1, ")"

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    throw p1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public getBooleanCellValue()Z
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇080(I)V

    .line 3
    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇o〇:Z

    .line 6
    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCellType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->O8:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getColumnIndex()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/EvaluationCell;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getColumnIndex()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getErrorCellValue()I
    .locals 1

    .line 1
    const/4 v0, 0x5

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇080(I)V

    .line 3
    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->Oo08:I

    .line 6
    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getIdentityKey()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/EvaluationCell;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getIdentityKey()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNumericCellValue()D
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇080(I)V

    .line 3
    .line 4
    .line 5
    iget-wide v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->o〇0:D

    .line 6
    .line 7
    return-wide v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRowIndex()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/EvaluationCell;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getRowIndex()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSheet()Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇080:Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStringCellValue()Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇080(I)V

    .line 3
    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇〇888:Ljava/lang/String;

    .line 6
    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-class v1, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 6
    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->O8:I

    .line 11
    .line 12
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;->getNumberValue()D

    .line 15
    .line 16
    .line 17
    move-result-wide v0

    .line 18
    iput-wide v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->o〇0:D

    .line 19
    .line 20
    return-void

    .line 21
    :cond_0
    const-class v1, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;

    .line 22
    .line 23
    if-ne v0, v1, :cond_1

    .line 24
    .line 25
    const/4 v0, 0x1

    .line 26
    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->O8:I

    .line 27
    .line 28
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;

    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;->getStringValue()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇〇888:Ljava/lang/String;

    .line 35
    .line 36
    return-void

    .line 37
    :cond_1
    const-class v1, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 38
    .line 39
    if-ne v0, v1, :cond_2

    .line 40
    .line 41
    const/4 v0, 0x4

    .line 42
    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->O8:I

    .line 43
    .line 44
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;->getBooleanValue()Z

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    iput-boolean p1, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇o〇:Z

    .line 51
    .line 52
    return-void

    .line 53
    :cond_2
    const-class v1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 54
    .line 55
    if-ne v0, v1, :cond_3

    .line 56
    .line 57
    const/4 v0, 0x5

    .line 58
    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->O8:I

    .line 59
    .line 60
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 61
    .line 62
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->getErrorCode()I

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->Oo08:I

    .line 67
    .line 68
    return-void

    .line 69
    :cond_3
    const-class p1, Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;

    .line 70
    .line 71
    if-ne v0, p1, :cond_4

    .line 72
    .line 73
    const/4 p1, 0x3

    .line 74
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->O8:I

    .line 75
    .line 76
    return-void

    .line 77
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 78
    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    .line 80
    .line 81
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 82
    .line 83
    .line 84
    const-string v2, "Unexpected value class ("

    .line 85
    .line 86
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    const-string v0, ")"

    .line 97
    .line 98
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    throw p1
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

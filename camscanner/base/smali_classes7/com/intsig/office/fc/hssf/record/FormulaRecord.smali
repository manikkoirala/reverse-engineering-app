.class public final Lcom/intsig/office/fc/hssf/record/FormulaRecord;
.super Lcom/intsig/office/fc/hssf/record/CellRecord;
.source "FormulaRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;
    }
.end annotation


# static fields
.field private static FIXED_SIZE:I = 0xe

.field private static final alwaysCalc:Lcom/intsig/office/fc/util/BitField;

.field private static final calcOnLoad:Lcom/intsig/office/fc/util/BitField;

.field private static final sharedFormula:Lcom/intsig/office/fc/util/BitField;

.field public static final sid:S = 0x6s


# instance fields
.field private field_4_value:D

.field private field_5_options:S

.field private field_6_zero:I

.field private field_8_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

.field private specialCachedValue:Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    sput-object v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->alwaysCalc:Lcom/intsig/office/fc/util/BitField;

    .line 7
    .line 8
    const/4 v0, 0x2

    .line 9
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    sput-object v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->calcOnLoad:Lcom/intsig/office/fc/util/BitField;

    .line 14
    .line 15
    const/16 v0, 0x8

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    sput-object v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->sharedFormula:Lcom/intsig/office/fc/util/BitField;

    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/CellRecord;-><init>()V

    .line 2
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->EMPTY_PTG_ARRAY:[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/Formula;->create([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Lcom/intsig/office/fc/hssf/formula/Formula;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_8_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V
    .locals 3

    .line 3
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/record/CellRecord;-><init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V

    .line 4
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readLong()J

    move-result-wide v0

    .line 5
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readShort()S

    move-result v2

    iput-short v2, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_5_options:S

    .line 6
    invoke-static {v0, v1}, Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;->〇o00〇〇Oo(J)Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    move-result-object v2

    iput-object v2, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->specialCachedValue:Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    if-nez v2, :cond_0

    .line 7
    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_4_value:D

    .line 8
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_6_zero:I

    .line 9
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readShort()S

    move-result v0

    .line 10
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->available()I

    move-result v1

    .line 11
    invoke-static {v0, p1, v1}, Lcom/intsig/office/fc/hssf/formula/Formula;->read(ILcom/intsig/office/fc/util/LittleEndianInput;I)Lcom/intsig/office/fc/hssf/formula/Formula;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_8_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    return-void
.end method


# virtual methods
.method protected appendValueText(Ljava/lang/StringBuilder;)V
    .locals 5

    .line 1
    const-string v0, "  .value\t = "

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->specialCachedValue:Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 7
    .line 8
    const-string v1, "\n"

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    iget-wide v2, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_4_value:D

    .line 13
    .line 14
    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;->〇〇888()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    :goto_0
    const-string v0, "  .options   = "

    .line 32
    .line 33
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->getOptions()S

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    invoke-static {v0}, Lcom/intsig/office/fc/util/HexDump;->shortToHex(I)[C

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    const-string v0, "    .alwaysCalc= "

    .line 51
    .line 52
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->isAlwaysCalc()Z

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    const-string v0, "    .calcOnLoad= "

    .line 66
    .line 67
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->isCalcOnLoad()Z

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    const-string v0, "    .shared    = "

    .line 81
    .line 82
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->isSharedFormula()Z

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    const-string v0, "  .zero      = "

    .line 96
    .line 97
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_6_zero:I

    .line 101
    .line 102
    invoke-static {v0}, Lcom/intsig/office/fc/util/HexDump;->intToHex(I)[C

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_8_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 113
    .line 114
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/Formula;->getTokens()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    const/4 v2, 0x0

    .line 119
    :goto_1
    array-length v3, v0

    .line 120
    if-ge v2, v3, :cond_2

    .line 121
    .line 122
    if-lez v2, :cond_1

    .line 123
    .line 124
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    :cond_1
    const-string v3, "    Ptg["

    .line 128
    .line 129
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 133
    .line 134
    .line 135
    const-string v3, "]="

    .line 136
    .line 137
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    aget-object v3, v0, v2

    .line 141
    .line 142
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->toString()Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object v4

    .line 146
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    .line 148
    .line 149
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getRVAType()C

    .line 150
    .line 151
    .line 152
    move-result v3

    .line 153
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 154
    .line 155
    .line 156
    add-int/lit8 v2, v2, 0x1

    .line 157
    .line 158
    goto :goto_1

    .line 159
    :cond_2
    return-void
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public clone()Ljava/lang/Object;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/CellRecord;->copyBaseFields(Lcom/intsig/office/fc/hssf/record/CellRecord;)V

    .line 7
    .line 8
    .line 9
    iget-wide v1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_4_value:D

    .line 10
    .line 11
    iput-wide v1, v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_4_value:D

    .line 12
    .line 13
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_5_options:S

    .line 14
    .line 15
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_5_options:S

    .line 16
    .line 17
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_6_zero:I

    .line 18
    .line 19
    iput v1, v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_6_zero:I

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_8_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 22
    .line 23
    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_8_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->specialCachedValue:Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 26
    .line 27
    iput-object v1, v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->specialCachedValue:Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 28
    .line 29
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getCachedBooleanValue()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->specialCachedValue:Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;->〇80〇808〇O()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCachedErrorValue()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->specialCachedValue:Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;->〇8o8o〇()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCachedResultType()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->specialCachedValue:Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;->OO0o〇〇()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFormula()Lcom/intsig/office/fc/hssf/formula/Formula;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_8_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOptions()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_5_options:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getParsedExpression()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_8_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/Formula;->getTokens()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getRecordName()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "FORMULA"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSid()S
    .locals 1

    .line 1
    const/4 v0, 0x6

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getValue()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_4_value:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getValueDataSize()I
    .locals 2

    .line 1
    sget v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->FIXED_SIZE:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_8_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/formula/Formula;->getEncodedSize()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    add-int/2addr v0, v1

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public hasCachedResultString()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->specialCachedValue:Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;->〇O8o08O()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    :cond_1
    return v1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isAlwaysCalc()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->alwaysCalc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_5_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isCalcOnLoad()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->calcOnLoad:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_5_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isSharedFormula()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->sharedFormula:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_5_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected serializeValue(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->specialCachedValue:Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-wide v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_4_value:D

    .line 6
    .line 7
    invoke-interface {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeDouble(D)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;->Oooo8o0〇(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 12
    .line 13
    .line 14
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->getOptions()S

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 19
    .line 20
    .line 21
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_6_zero:I

    .line 22
    .line 23
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeInt(I)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_8_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 27
    .line 28
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setAlwaysCalc(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->alwaysCalc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_5_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setShortBoolean(SZ)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_5_options:S

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCachedResultBoolean(Z)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;->〇o〇(Z)Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->specialCachedValue:Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCachedResultErrorCode(I)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;->Oo08(I)Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->specialCachedValue:Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCachedResultTypeEmptyString()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;->O8()Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->specialCachedValue:Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setCachedResultTypeString()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;->o〇0()Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->specialCachedValue:Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setCalcOnLoad(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->calcOnLoad:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_5_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setShortBoolean(SZ)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_5_options:S

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOptions(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_5_options:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setParsedExpression([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/Formula;->create([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_8_parsed_expr:Lcom/intsig/office/fc/hssf/formula/Formula;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSharedFormula(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->sharedFormula:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_5_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setShortBoolean(SZ)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_5_options:S

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setValue(D)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->field_4_value:D

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->specialCachedValue:Lcom/intsig/office/fc/hssf/record/FormulaRecord$SpecialCachedValue;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

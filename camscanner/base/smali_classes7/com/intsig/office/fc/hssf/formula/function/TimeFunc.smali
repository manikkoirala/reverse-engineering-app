.class public final Lcom/intsig/office/fc/hssf/formula/function/TimeFunc;
.super Lcom/intsig/office/fc/hssf/formula/function/Fixed3ArgFunction;
.source "TimeFunc.java"


# static fields
.field private static final HOURS_PER_DAY:I = 0x18

.field private static final SECONDS_PER_DAY:I = 0x15180

.field private static final SECONDS_PER_HOUR:I = 0xe10

.field private static final SECONDS_PER_MINUTE:I = 0x3c


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/function/Fixed3ArgFunction;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static evalArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/eval/MissingArgEval;->instance:Lcom/intsig/office/fc/hssf/formula/eval/MissingArgEval;

    .line 2
    .line 3
    if-ne p0, v0, :cond_0

    .line 4
    .line 5
    const/4 p0, 0x0

    .line 6
    return p0

    .line 7
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->getSingleValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->coerceValueToInt(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)I

    .line 12
    .line 13
    .line 14
    move-result p0

    .line 15
    return p0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private static evaluate(III)D
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation

    const/16 v0, 0x7fff

    if-gt p0, v0, :cond_1

    if-gt p1, v0, :cond_1

    if-gt p2, v0, :cond_1

    mul-int/lit16 p0, p0, 0xe10

    mul-int/lit8 p1, p1, 0x3c

    add-int/2addr p0, p1

    add-int/2addr p0, p2

    if-ltz p0, :cond_0

    const p1, 0x15180

    .line 4
    rem-int/2addr p0, p1

    int-to-double p0, p0

    const-wide v0, 0x40f5180000000000L    # 86400.0

    div-double/2addr p0, v0

    return-wide p0

    .line 5
    :cond_0
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;)V

    throw p0

    .line 6
    :cond_1
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;)V

    throw p0
.end method


# virtual methods
.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 0

    .line 1
    :try_start_0
    invoke-static {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/TimeFunc;->evalArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)I

    move-result p3

    invoke-static {p4, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/TimeFunc;->evalArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)I

    move-result p4

    invoke-static {p5, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/TimeFunc;->evalArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)I

    move-result p1

    invoke-static {p3, p4, p1}, Lcom/intsig/office/fc/hssf/formula/function/TimeFunc;->evaluate(III)D

    move-result-wide p1
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    new-instance p3, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    invoke-direct {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    return-object p3

    :catch_0
    move-exception p1

    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object p1

    return-object p1
.end method

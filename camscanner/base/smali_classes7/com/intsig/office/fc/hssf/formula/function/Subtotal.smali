.class public Lcom/intsig/office/fc/hssf/formula/function/Subtotal;
.super Ljava/lang/Object;
.source "Subtotal.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/function/Function;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static findFunction(I)Lcom/intsig/office/fc/hssf/formula/function/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation

    .line 1
    packed-switch p0, :pswitch_data_0

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x64

    .line 5
    .line 6
    if-le p0, v0, :cond_0

    .line 7
    .line 8
    const/16 v0, 0x70

    .line 9
    .line 10
    if-ge p0, v0, :cond_0

    .line 11
    .line 12
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/eval/NotImplementedException;

    .line 13
    .line 14
    const-string v0, "SUBTOTAL - with \'exclude hidden values\' option"

    .line 15
    .line 16
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/eval/NotImplementedException;-><init>(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    throw p0

    .line 20
    :pswitch_0
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/eval/NotImplementedException;

    .line 21
    .line 22
    const-string v0, "VARP"

    .line 23
    .line 24
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/eval/NotImplementedException;-><init>(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    throw p0

    .line 28
    :pswitch_1
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/eval/NotImplementedException;

    .line 29
    .line 30
    const-string v0, "VAR"

    .line 31
    .line 32
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/eval/NotImplementedException;-><init>(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    throw p0

    .line 36
    :pswitch_2
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->SUM:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 37
    .line 38
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->subtotalInstance(Lcom/intsig/office/fc/hssf/formula/function/Function;)Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 39
    .line 40
    .line 41
    move-result-object p0

    .line 42
    return-object p0

    .line 43
    :pswitch_3
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/eval/NotImplementedException;

    .line 44
    .line 45
    const-string v0, "STDEVP"

    .line 46
    .line 47
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/eval/NotImplementedException;-><init>(Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    throw p0

    .line 51
    :pswitch_4
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->STDEV:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 52
    .line 53
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->subtotalInstance(Lcom/intsig/office/fc/hssf/formula/function/Function;)Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 54
    .line 55
    .line 56
    move-result-object p0

    .line 57
    return-object p0

    .line 58
    :pswitch_5
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->PRODUCT:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 59
    .line 60
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->subtotalInstance(Lcom/intsig/office/fc/hssf/formula/function/Function;)Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 61
    .line 62
    .line 63
    move-result-object p0

    .line 64
    return-object p0

    .line 65
    :pswitch_6
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->MIN:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 66
    .line 67
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->subtotalInstance(Lcom/intsig/office/fc/hssf/formula/function/Function;)Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 68
    .line 69
    .line 70
    move-result-object p0

    .line 71
    return-object p0

    .line 72
    :pswitch_7
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->MAX:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 73
    .line 74
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->subtotalInstance(Lcom/intsig/office/fc/hssf/formula/function/Function;)Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 75
    .line 76
    .line 77
    move-result-object p0

    .line 78
    return-object p0

    .line 79
    :pswitch_8
    invoke-static {}, Lcom/intsig/office/fc/hssf/formula/function/Counta;->subtotalInstance()Lcom/intsig/office/fc/hssf/formula/function/Counta;

    .line 80
    .line 81
    .line 82
    move-result-object p0

    .line 83
    return-object p0

    .line 84
    :pswitch_9
    invoke-static {}, Lcom/intsig/office/fc/hssf/formula/function/Count;->subtotalInstance()Lcom/intsig/office/fc/hssf/formula/function/Count;

    .line 85
    .line 86
    .line 87
    move-result-object p0

    .line 88
    return-object p0

    .line 89
    :pswitch_a
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->AVERAGE:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 90
    .line 91
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/AggregateFunction;->subtotalInstance(Lcom/intsig/office/fc/hssf/formula/function/Function;)Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 92
    .line 93
    .line 94
    move-result-object p0

    .line 95
    return-object p0

    .line 96
    :cond_0
    invoke-static {}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->invalidValue()Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    .line 97
    .line 98
    .line 99
    move-result-object p0

    .line 100
    throw p0

    .line 101
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method


# virtual methods
.method public evaluate([Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 5

    .line 1
    array-length v0, p1

    .line 2
    const/4 v1, 0x1

    .line 3
    sub-int/2addr v0, v1

    .line 4
    if-ge v0, v1, :cond_0

    .line 5
    .line 6
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 7
    .line 8
    return-object p1

    .line 9
    :cond_0
    const/4 v2, 0x0

    .line 10
    :try_start_0
    aget-object v3, p1, v2

    .line 11
    .line 12
    invoke-static {v3, p2, p3}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->getSingleValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    invoke-static {v3}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->coerceValueToInt(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    invoke-static {v3}, Lcom/intsig/office/fc/hssf/formula/function/Subtotal;->findFunction(I)Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 21
    .line 22
    .line 23
    move-result-object v3
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    new-array v4, v0, [Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 25
    .line 26
    invoke-static {p1, v1, v4, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v3, v4, p2, p3}, Lcom/intsig/office/fc/hssf/formula/function/Function;->evaluate([Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    return-object p1

    .line 34
    :catch_0
    move-exception p1

    .line 35
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    return-object p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

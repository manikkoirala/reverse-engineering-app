.class public final Lcom/intsig/office/fc/hssf/formula/ptg/FuncPtg;
.super Lcom/intsig/office/fc/hssf/formula/ptg/AbstractFunctionPtg;
.source "FuncPtg.java"


# static fields
.field public static final SIZE:I = 0x3

.field public static final sid:B = 0x21t


# direct methods
.method private constructor <init>(ILcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;)V
    .locals 2

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->getReturnClassCode()B

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->getParameterClassCodes()[B

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;->getMinParams()I

    .line 10
    .line 11
    .line 12
    move-result p2

    .line 13
    invoke-direct {p0, p1, v0, v1, p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AbstractFunctionPtg;-><init>(II[BI)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static create(I)Lcom/intsig/office/fc/hssf/formula/ptg/FuncPtg;
    .locals 3

    .line 2
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadataRegistry;->getFunctionByIndex(I)Lcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/ptg/FuncPtg;

    invoke-direct {v1, p0, v0}, Lcom/intsig/office/fc/hssf/formula/ptg/FuncPtg;-><init>(ILcom/intsig/office/fc/hssf/formula/function/FunctionMetadata;)V

    return-object v1

    .line 4
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid built-in function index ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static create(Lcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/ptg/FuncPtg;
    .locals 0

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    move-result p0

    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/FuncPtg;->create(I)Lcom/intsig/office/fc/hssf/formula/ptg/FuncPtg;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getSize()I
    .locals 1

    .line 1
    const/4 v0, 0x3

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public write(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getPtgClass()B

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    add-int/lit8 v0, v0, 0x21

    .line 6
    .line 7
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeByte(I)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/AbstractFunctionPtg;->getFunctionIndex()S

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

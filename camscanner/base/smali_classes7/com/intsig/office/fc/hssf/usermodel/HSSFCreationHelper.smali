.class public Lcom/intsig/office/fc/hssf/usermodel/HSSFCreationHelper;
.super Ljava/lang/Object;
.source "HSSFCreationHelper.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/CreationHelper;


# instance fields
.field private dataFormat:Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;

.field private workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFCreationHelper;->workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    .line 5
    .line 6
    new-instance p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFCreationHelper;->workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;-><init>(Lcom/intsig/office/fc/hssf/model/InternalWorkbook;)V

    .line 15
    .line 16
    .line 17
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFCreationHelper;->dataFormat:Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public createClientAnchor()Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;
    .locals 1

    .line 2
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;

    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;-><init>()V

    return-object v0
.end method

.method public bridge synthetic createClientAnchor()Lcom/intsig/office/fc/hssf/usermodel/IClientAnchor;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCreationHelper;->createClientAnchor()Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;

    move-result-object v0

    return-object v0
.end method

.method public createDataFormat()Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFCreationHelper;->dataFormat:Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;

    return-object v0
.end method

.method public bridge synthetic createDataFormat()Lcom/intsig/office/fc/ss/usermodel/DataFormat;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCreationHelper;->createDataFormat()Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;

    move-result-object v0

    return-object v0
.end method

.method public createFormulaEvaluator()Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;
    .locals 1

    .line 1
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic createFormulaEvaluator()Lcom/intsig/office/fc/ss/usermodel/FormulaEvaluator;
    .locals 1

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCreationHelper;->createFormulaEvaluator()Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;

    move-result-object v0

    return-object v0
.end method

.method public createHyperlink(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFHyperlink;
    .locals 1

    .line 2
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFHyperlink;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFHyperlink;-><init>(I)V

    return-object v0
.end method

.method public bridge synthetic createHyperlink(I)Lcom/intsig/office/fc/ss/usermodel/IHyperlink;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCreationHelper;->createHyperlink(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFHyperlink;

    move-result-object p1

    return-object p1
.end method

.method public createRichTextString(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;
    .locals 1

    .line 2
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic createRichTextString(Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/RichTextString;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCreationHelper;->createRichTextString(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;

    move-result-object p1

    return-object p1
.end method

.class public final Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;
.super Ljava/lang/Object;
.source "Biff8DecryptingStream.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/record/BiffHeaderInput;
.implements Lcom/intsig/office/fc/util/LittleEndianInput;


# instance fields
.field private final _le:Lcom/intsig/office/fc/util/LittleEndianInput;

.field private final _rc4:Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;ILcom/intsig/office/fc/hssf/record/crypto/Biff8EncryptionKey;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;

    .line 5
    .line 6
    invoke-direct {v0, p2, p3}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;-><init>(ILcom/intsig/office/fc/hssf/record/crypto/Biff8EncryptionKey;)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;

    .line 10
    .line 11
    instance-of p2, p1, Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 12
    .line 13
    if-eqz p2, :cond_0

    .line 14
    .line 15
    check-cast p1, Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 16
    .line 17
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_le:Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    new-instance p2, Lcom/intsig/office/fc/util/LittleEndianInputStream;

    .line 21
    .line 22
    invoke-direct {p2, p1}, Lcom/intsig/office/fc/util/LittleEndianInputStream;-><init>(Ljava/io/InputStream;)V

    .line 23
    .line 24
    .line 25
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_le:Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 26
    .line 27
    :goto_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public available()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_le:Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/util/LittleEndianInput;->available()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public readByte()B
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_le:Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 4
    .line 5
    invoke-interface {v1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUByte()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;->〇〇888(I)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-byte v0, v0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public readDataSize()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_le:Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;->O8()V

    .line 10
    .line 11
    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public readDouble()D
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->readLong()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    if-nez v2, :cond_0

    .line 14
    .line 15
    return-wide v0

    .line 16
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    .line 17
    .line 18
    const-string v1, "Did not expect to read NaN"

    .line 19
    .line 20
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    throw v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public readFully([B)V
    .locals 2

    .line 1
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->readFully([BII)V

    return-void
.end method

.method public readFully([BII)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_le:Lcom/intsig/office/fc/util/LittleEndianInput;

    invoke-interface {v0, p1, p2, p3}, Lcom/intsig/office/fc/util/LittleEndianInput;->readFully([BII)V

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;

    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;->o〇0([BII)V

    return-void
.end method

.method public readInt()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_le:Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 4
    .line 5
    invoke-interface {v1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readInt()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;->oO80(I)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public readLong()J
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_le:Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 4
    .line 5
    invoke-interface {v1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readLong()J

    .line 6
    .line 7
    .line 8
    move-result-wide v1

    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;->〇80〇808〇O(J)J

    .line 10
    .line 11
    .line 12
    move-result-wide v0

    .line 13
    return-wide v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public readRecordSID()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_le:Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;->O8()V

    .line 10
    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;

    .line 13
    .line 14
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;->Oo08(I)V

    .line 15
    .line 16
    .line 17
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public readShort()S
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_le:Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 4
    .line 5
    invoke-interface {v1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;->OO0o〇〇〇〇0(I)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-short v0, v0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public readUByte()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_le:Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 4
    .line 5
    invoke-interface {v1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUByte()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;->〇〇888(I)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public readUShort()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_rc4:Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/crypto/Biff8DecryptingStream;->_le:Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 4
    .line 5
    invoke-interface {v1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/crypto/Biff8RC4;->OO0o〇〇〇〇0(I)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

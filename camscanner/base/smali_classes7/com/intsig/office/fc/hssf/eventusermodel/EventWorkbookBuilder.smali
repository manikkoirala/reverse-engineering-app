.class public Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder;
.super Ljava/lang/Object;
.source "EventWorkbookBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder$SheetRecordCollectingListener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static createStubWorkbook([Lcom/intsig/office/fc/hssf/record/ExternSheetRecord;[Lcom/intsig/office/fc/hssf/record/BoundSheetRecord;)Lcom/intsig/office/fc/hssf/model/InternalWorkbook;
    .locals 1

    const/4 v0, 0x0

    .line 10
    invoke-static {p0, p1, v0}, Lcom/intsig/office/fc/hssf/eventusermodel/EventWorkbookBuilder;->createStubWorkbook([Lcom/intsig/office/fc/hssf/record/ExternSheetRecord;[Lcom/intsig/office/fc/hssf/record/BoundSheetRecord;Lcom/intsig/office/fc/hssf/record/SSTRecord;)Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    move-result-object p0

    return-object p0
.end method

.method public static createStubWorkbook([Lcom/intsig/office/fc/hssf/record/ExternSheetRecord;[Lcom/intsig/office/fc/hssf/record/BoundSheetRecord;Lcom/intsig/office/fc/hssf/record/SSTRecord;)Lcom/intsig/office/fc/hssf/model/InternalWorkbook;
    .locals 4

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const/4 v2, 0x0

    .line 2
    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_0

    .line 3
    aget-object v3, p1, v2

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 4
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz p0, :cond_2

    .line 5
    array-length p1, p0

    int-to-short p1, p1

    invoke-static {p1}, Lcom/intsig/office/fc/hssf/record/SupBookRecord;->createInternalReferences(S)Lcom/intsig/office/fc/hssf/record/SupBookRecord;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6
    :goto_1
    array-length p1, p0

    if-ge v1, p1, :cond_2

    .line 7
    aget-object p1, p0, v1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 8
    :cond_2
    sget-object p0, Lcom/intsig/office/fc/hssf/record/EOFRecord;->instance:Lcom/intsig/office/fc/hssf/record/EOFRecord;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 9
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->createWorkbook(Ljava/util/List;)Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    move-result-object p0

    return-object p0
.end method

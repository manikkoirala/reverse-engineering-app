.class public final Lcom/intsig/office/fc/hssf/record/DrawingRecordForBiffViewer;
.super Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;
.source "DrawingRecordForBiffViewer.java"


# static fields
.field public static final sid:S = 0xecs


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/DrawingRecord;)V
    .locals 0

    .line 3
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/record/DrawingRecordForBiffViewer;->convertToInputStream(Lcom/intsig/office/fc/hssf/record/DrawingRecord;)Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;-><init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V

    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->convertRawBytesToEscherRecords()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;-><init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V

    return-void
.end method

.method private static convertToInputStream(Lcom/intsig/office/fc/hssf/record/DrawingRecord;)Lcom/intsig/office/fc/hssf/record/RecordInputStream;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/Record;->serialize()[B

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    new-instance v0, Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 6
    .line 7
    new-instance v1, Ljava/io/ByteArrayInputStream;

    .line 8
    .line 9
    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 10
    .line 11
    .line 12
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;-><init>(Ljava/io/InputStream;)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->nextRecord()V

    .line 16
    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method protected getRecordName()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "MSODRAWING"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSid()S
    .locals 1

    .line 1
    const/16 v0, 0xec

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

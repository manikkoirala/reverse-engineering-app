.class public abstract Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;
.super Ljava/lang/Object;
.source "HSSFShape.java"


# instance fields
.field private _fillColor:I

.field private _fillType:I

.field private _lineStyle:I

.field private _lineStyleColor:I

.field private _lineWidth:I

.field private _noBorder:Z

.field private _noFill:Z

.field _patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

.field private anchor:Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;

.field private angle:I

.field private bgPictureData:[B

.field private endArrow:Lcom/intsig/office/common/shape/Arrow;

.field protected escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

.field private flipH:Z

.field private flipV:Z

.field final parent:Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;

.field private shapeType:I

.field private startArrow:Lcom/intsig/office/common/shape/Arrow;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->shapeType:I

    .line 6
    .line 7
    iput-boolean v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_noBorder:Z

    .line 8
    .line 9
    const v1, 0x8000040

    .line 10
    .line 11
    .line 12
    iput v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_lineStyleColor:I

    .line 13
    .line 14
    const/16 v1, 0x2535

    .line 15
    .line 16
    iput v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_lineWidth:I

    .line 17
    .line 18
    iput v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_lineStyle:I

    .line 19
    .line 20
    iput-boolean v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_noFill:Z

    .line 21
    .line 22
    const v0, 0x8000009

    .line 23
    .line 24
    .line 25
    iput v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_fillColor:I

    .line 26
    .line 27
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 28
    .line 29
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->parent:Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;

    .line 30
    .line 31
    iput-object p3, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->anchor:Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;

    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static toChildAnchor(Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;)Lcom/intsig/office/fc/hssf/usermodel/HSSFChildAnchor;
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChildAnchor;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDx1()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDy1()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDx2()I

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherChildAnchorRecord;->getDy2()I

    .line 16
    .line 17
    .line 18
    move-result p0

    .line 19
    invoke-direct {v0, v1, v2, v3, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChildAnchor;-><init>(IIII)V

    .line 20
    .line 21
    .line 22
    return-object v0
    .line 23
    .line 24
.end method

.method public static toClientAnchor(Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;)Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getFlag()S

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->setAnchorType(I)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getCol1()S

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->setCol1(S)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getCol2()S

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->setCol2(S)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getDx1()S

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->setDx1(I)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getDx2()S

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->setDx2(I)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getDy1()S

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->setDy1(I)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getDy2()S

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->setDy2(I)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getRow1()S

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->setRow1(I)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;->getRow2()S

    .line 63
    .line 64
    .line 65
    move-result p0

    .line 66
    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->setRow2(I)V

    .line 67
    .line 68
    .line 69
    return-object v0
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public checkPatriarch()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->parent:Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;

    .line 2
    .line 3
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 4
    .line 5
    if-nez v1, :cond_0

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v1, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 10
    .line 11
    iput-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getParent()Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    if-eqz v1, :cond_1

    .line 19
    .line 20
    const/4 v0, 0x1

    .line 21
    goto :goto_1

    .line 22
    :cond_1
    const/4 v0, 0x0

    .line 23
    :goto_1
    return v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public countOfAllChildren()I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAnchor()Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->anchor:Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBGPictureData()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->bgPictureData:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEndArrowLength()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->endArrow:Lcom/intsig/office/common/shape/Arrow;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getLength()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEndArrowType()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->endArrow:Lcom/intsig/office/common/shape/Arrow;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEndArrowWidth()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->endArrow:Lcom/intsig/office/common/shape/Arrow;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getWidth()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFillColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_fillColor:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFillType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_fillType:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFlipH()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->flipH:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFlipV()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->flipV:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getGradientTileBackground(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/system/IControl;)Lcom/intsig/office/common/bg/BackgroundAndFill;
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getFillType()I

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    const/4 v3, 0x0

    .line 10
    const/4 v4, 0x6

    .line 11
    const/4 v5, 0x5

    .line 12
    const/4 v6, 0x4

    .line 13
    const/4 v7, 0x2

    .line 14
    const/4 v9, 0x7

    .line 15
    if-eq v2, v9, :cond_2

    .line 16
    .line 17
    if-eq v2, v6, :cond_2

    .line 18
    .line 19
    if-eq v2, v5, :cond_2

    .line 20
    .line 21
    if-ne v2, v4, :cond_0

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    if-ne v2, v7, :cond_1

    .line 25
    .line 26
    new-instance v8, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 27
    .line 28
    invoke-direct {v8}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 29
    .line 30
    .line 31
    iget-object v2, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 32
    .line 33
    const/16 v4, -0xff5

    .line 34
    .line 35
    invoke-static {v2, v4}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 40
    .line 41
    const/16 v4, 0x186

    .line 42
    .line 43
    invoke-static {v2, v4}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 48
    .line 49
    if-eqz v2, :cond_f

    .line 50
    .line 51
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getInternalWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 56
    .line 57
    .line 58
    move-result v2

    .line 59
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getBSERecord(I)Lcom/intsig/office/fc/ddf/EscherBSERecord;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    if-eqz v1, :cond_f

    .line 64
    .line 65
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->getBlipRecord()Lcom/intsig/office/fc/ddf/EscherBlipRecord;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    if-eqz v1, :cond_f

    .line 70
    .line 71
    invoke-virtual {v8, v7}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 72
    .line 73
    .line 74
    new-instance v2, Lcom/intsig/office/common/picture/Picture;

    .line 75
    .line 76
    invoke-direct {v2}, Lcom/intsig/office/common/picture/Picture;-><init>()V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherBlipRecord;->getPicturedata()[B

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    invoke-virtual {v2, v1}, Lcom/intsig/office/common/picture/Picture;->setData([B)V

    .line 84
    .line 85
    .line 86
    invoke-interface/range {p2 .. p2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 87
    .line 88
    .line 89
    move-result-object v1

    .line 90
    invoke-virtual {v1}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 91
    .line 92
    .line 93
    move-result-object v1

    .line 94
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/common/picture/Picture;)I

    .line 95
    .line 96
    .line 97
    new-instance v1, Lcom/intsig/office/common/bg/TileShader;

    .line 98
    .line 99
    const/high16 v4, 0x3f800000    # 1.0f

    .line 100
    .line 101
    invoke-direct {v1, v2, v3, v4, v4}, Lcom/intsig/office/common/bg/TileShader;-><init>(Lcom/intsig/office/common/picture/Picture;IFF)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {v8, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setShader(Lcom/intsig/office/common/bg/AShader;)V

    .line 105
    .line 106
    .line 107
    goto/16 :goto_8

    .line 108
    .line 109
    :cond_1
    const/4 v8, 0x0

    .line 110
    goto/16 :goto_8

    .line 111
    .line 112
    :cond_2
    :goto_0
    new-instance v10, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 113
    .line 114
    invoke-direct {v10}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 115
    .line 116
    .line 117
    iget-object v11, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 118
    .line 119
    invoke-static {v11}, Lcom/intsig/office/fc/ShapeKit;->getFillAngle(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 120
    .line 121
    .line 122
    move-result v11

    .line 123
    const/16 v12, -0x87

    .line 124
    .line 125
    if-eq v11, v12, :cond_5

    .line 126
    .line 127
    const/16 v12, -0x5a

    .line 128
    .line 129
    if-eq v11, v12, :cond_4

    .line 130
    .line 131
    const/16 v12, -0x2d

    .line 132
    .line 133
    if-eq v11, v12, :cond_3

    .line 134
    .line 135
    if-eqz v11, :cond_4

    .line 136
    .line 137
    goto :goto_1

    .line 138
    :cond_3
    const/16 v11, 0x87

    .line 139
    .line 140
    goto :goto_1

    .line 141
    :cond_4
    add-int/lit8 v11, v11, 0x5a

    .line 142
    .line 143
    goto :goto_1

    .line 144
    :cond_5
    const/16 v11, 0x2d

    .line 145
    .line 146
    :goto_1
    iget-object v12, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 147
    .line 148
    invoke-static {v12}, Lcom/intsig/office/fc/ShapeKit;->getFillFocus(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 149
    .line 150
    .line 151
    move-result v12

    .line 152
    iget-object v13, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 153
    .line 154
    const/4 v14, 0x1

    .line 155
    invoke-static {v13, v1, v14}, Lcom/intsig/office/fc/ShapeKit;->getForegroundColor(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/lang/Object;I)Lcom/intsig/office/java/awt/Color;

    .line 156
    .line 157
    .line 158
    move-result-object v13

    .line 159
    iget-object v15, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 160
    .line 161
    invoke-static {v15, v1, v14}, Lcom/intsig/office/fc/ShapeKit;->getFillbackColor(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/lang/Object;I)Lcom/intsig/office/java/awt/Color;

    .line 162
    .line 163
    .line 164
    move-result-object v1

    .line 165
    iget-object v15, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 166
    .line 167
    invoke-static {v15}, Lcom/intsig/office/fc/ShapeKit;->isShaderPreset(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z

    .line 168
    .line 169
    .line 170
    move-result v15

    .line 171
    if-eqz v15, :cond_6

    .line 172
    .line 173
    iget-object v15, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 174
    .line 175
    invoke-static {v15}, Lcom/intsig/office/fc/ShapeKit;->getShaderColors(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)[I

    .line 176
    .line 177
    .line 178
    move-result-object v15

    .line 179
    iget-object v8, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 180
    .line 181
    invoke-static {v8}, Lcom/intsig/office/fc/ShapeKit;->getShaderPositions(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)[F

    .line 182
    .line 183
    .line 184
    move-result-object v8

    .line 185
    goto :goto_2

    .line 186
    :cond_6
    const/4 v8, 0x0

    .line 187
    const/4 v15, 0x0

    .line 188
    :goto_2
    if-nez v15, :cond_9

    .line 189
    .line 190
    new-array v15, v7, [I

    .line 191
    .line 192
    const/16 v16, -0x1

    .line 193
    .line 194
    if-nez v13, :cond_7

    .line 195
    .line 196
    const/4 v13, -0x1

    .line 197
    goto :goto_3

    .line 198
    :cond_7
    invoke-virtual {v13}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 199
    .line 200
    .line 201
    move-result v13

    .line 202
    :goto_3
    aput v13, v15, v3

    .line 203
    .line 204
    if-nez v1, :cond_8

    .line 205
    .line 206
    goto :goto_4

    .line 207
    :cond_8
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 208
    .line 209
    .line 210
    move-result v16

    .line 211
    :goto_4
    aput v16, v15, v14

    .line 212
    .line 213
    :cond_9
    if-nez v8, :cond_a

    .line 214
    .line 215
    new-array v8, v7, [F

    .line 216
    .line 217
    fill-array-data v8, :array_0

    .line 218
    .line 219
    .line 220
    :cond_a
    if-ne v2, v9, :cond_b

    .line 221
    .line 222
    new-instance v1, Lcom/intsig/office/common/bg/LinearGradientShader;

    .line 223
    .line 224
    int-to-float v3, v11

    .line 225
    invoke-direct {v1, v3, v15, v8}, Lcom/intsig/office/common/bg/LinearGradientShader;-><init>(F[I[F)V

    .line 226
    .line 227
    .line 228
    :goto_5
    move-object v8, v1

    .line 229
    goto :goto_7

    .line 230
    :cond_b
    if-eq v2, v6, :cond_d

    .line 231
    .line 232
    if-eq v2, v5, :cond_d

    .line 233
    .line 234
    if-ne v2, v4, :cond_c

    .line 235
    .line 236
    goto :goto_6

    .line 237
    :cond_c
    const/4 v8, 0x0

    .line 238
    goto :goto_7

    .line 239
    :cond_d
    :goto_6
    new-instance v1, Lcom/intsig/office/common/bg/RadialGradientShader;

    .line 240
    .line 241
    iget-object v3, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 242
    .line 243
    invoke-static {v3}, Lcom/intsig/office/fc/ShapeKit;->getRadialGradientPositionType(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 244
    .line 245
    .line 246
    move-result v3

    .line 247
    invoke-direct {v1, v3, v15, v8}, Lcom/intsig/office/common/bg/RadialGradientShader;-><init>(I[I[F)V

    .line 248
    .line 249
    .line 250
    goto :goto_5

    .line 251
    :goto_7
    if-eqz v8, :cond_e

    .line 252
    .line 253
    invoke-virtual {v8, v12}, Lcom/intsig/office/common/bg/Gradient;->setFocus(I)V

    .line 254
    .line 255
    .line 256
    :cond_e
    int-to-byte v1, v2

    .line 257
    invoke-virtual {v10, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 258
    .line 259
    .line 260
    invoke-virtual {v10, v8}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setShader(Lcom/intsig/office/common/bg/AShader;)V

    .line 261
    .line 262
    .line 263
    move-object v8, v10

    .line 264
    :cond_f
    :goto_8
    return-object v8

    .line 265
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method public getLine()Lcom/intsig/office/common/borders/Line;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 4
    .line 5
    .line 6
    iget v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_lineStyleColor:I

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 9
    .line 10
    .line 11
    new-instance v1, Lcom/intsig/office/common/borders/Line;

    .line 12
    .line 13
    invoke-direct {v1}, Lcom/intsig/office/common/borders/Line;-><init>()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/borders/Line;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 17
    .line 18
    .line 19
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_lineWidth:I

    .line 20
    .line 21
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/borders/Border;->setLineWidth(I)V

    .line 22
    .line 23
    .line 24
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_lineStyle:I

    .line 25
    .line 26
    if-lez v0, :cond_0

    .line 27
    .line 28
    const/4 v0, 0x1

    .line 29
    goto :goto_0

    .line 30
    :cond_0
    const/4 v0, 0x0

    .line 31
    :goto_0
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/borders/Line;->setDash(Z)V

    .line 32
    .line 33
    .line 34
    return-object v1
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getLineStyle()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_lineStyle:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLineStyleColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_lineStyleColor:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLineWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_lineWidth:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getParent()Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->parent:Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRotation()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->angle:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShapeType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->shapeType:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStartArrowLength()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->startArrow:Lcom/intsig/office/common/shape/Arrow;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getLength()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStartArrowType()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->startArrow:Lcom/intsig/office/common/shape/Arrow;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getType()B

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getStartArrowWidth()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->startArrow:Lcom/intsig/office/common/shape/Arrow;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/Arrow;->getWidth()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isGradientTile()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getFillType(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x7

    .line 8
    if-eq v0, v1, :cond_1

    .line 9
    .line 10
    const/4 v1, 0x4

    .line 11
    if-eq v0, v1, :cond_1

    .line 12
    .line 13
    const/4 v1, 0x5

    .line 14
    if-eq v0, v1, :cond_1

    .line 15
    .line 16
    const/4 v1, 0x6

    .line 17
    if-eq v0, v1, :cond_1

    .line 18
    .line 19
    const/4 v1, 0x2

    .line 20
    if-ne v0, v1, :cond_0

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v0, 0x0

    .line 24
    goto :goto_1

    .line 25
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 26
    :goto_1
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public isNoBorder()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_noBorder:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isNoFill()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_noFill:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public processLineWidth()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->escherContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/ShapeKit;->getLineWidth(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iput v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_lineWidth:I

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public processSimpleBackground(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V
    .locals 5

    .line 1
    const/16 v0, -0xff5

    .line 2
    .line 3
    invoke-static {p1, v0}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 8
    .line 9
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getFillType(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, 0x1

    .line 14
    const/4 v3, 0x3

    .line 15
    if-ne v1, v3, :cond_0

    .line 16
    .line 17
    const/16 p1, 0x186

    .line 18
    .line 19
    invoke-static {v0, p1}, Lcom/intsig/office/fc/ShapeKit;->getEscherProperty(Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;I)Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    check-cast p1, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 24
    .line 25
    if-eqz p1, :cond_3

    .line 26
    .line 27
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getInternalWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    invoke-virtual {p1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getBSERecord(I)Lcom/intsig/office/fc/ddf/EscherBSERecord;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    if-eqz p1, :cond_3

    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/office/fc/ddf/EscherBSERecord;->getBlipRecord()Lcom/intsig/office/fc/ddf/EscherBlipRecord;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    if-eqz p1, :cond_3

    .line 46
    .line 47
    invoke-virtual {p0, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setFillType(I)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {p1}, Lcom/intsig/office/fc/ddf/EscherBlipRecord;->getPicturedata()[B

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setBGPictureData([B)V

    .line 55
    .line 56
    .line 57
    return-void

    .line 58
    :cond_0
    const/16 v0, 0xff

    .line 59
    .line 60
    const/4 v3, 0x0

    .line 61
    if-ne v1, v2, :cond_1

    .line 62
    .line 63
    invoke-static {p1, p2, v2}, Lcom/intsig/office/fc/ShapeKit;->getFillbackColor(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/lang/Object;I)Lcom/intsig/office/java/awt/Color;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    if-eqz p1, :cond_3

    .line 68
    .line 69
    invoke-virtual {p0, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setFillType(I)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setFillColor(II)V

    .line 77
    .line 78
    .line 79
    return-void

    .line 80
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isGradientTile()Z

    .line 81
    .line 82
    .line 83
    move-result v4

    .line 84
    if-eqz v4, :cond_2

    .line 85
    .line 86
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setFillType(I)V

    .line 87
    .line 88
    .line 89
    return-void

    .line 90
    :cond_2
    invoke-static {p1, p2, v2}, Lcom/intsig/office/fc/ShapeKit;->getForegroundColor(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/lang/Object;I)Lcom/intsig/office/java/awt/Color;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    if-eqz p1, :cond_3

    .line 95
    .line 96
    invoke-virtual {p0, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setFillType(I)V

    .line 97
    .line 98
    .line 99
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 100
    .line 101
    .line 102
    move-result p1

    .line 103
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setFillColor(II)V

    .line 104
    .line 105
    .line 106
    return-void

    .line 107
    :cond_3
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setNoFill(Z)V

    .line 108
    .line 109
    .line 110
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public setAnchor(Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->parent:Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChildAnchor;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 11
    .line 12
    const-string v0, "Must use client anchors for shapes directly attached to sheet."

    .line 13
    .line 14
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    throw p1

    .line 18
    :cond_1
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;

    .line 19
    .line 20
    if-nez v0, :cond_2

    .line 21
    .line 22
    :goto_0
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->anchor:Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;

    .line 23
    .line 24
    return-void

    .line 25
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 26
    .line 27
    const-string v0, "Must use child anchors for shapes attached to groups."

    .line 28
    .line 29
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    throw p1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setBGPictureData([B)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->bgPictureData:[B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setEndArrow(BII)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/common/shape/Arrow;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2, p3}, Lcom/intsig/office/common/shape/Arrow;-><init>(BII)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->endArrow:Lcom/intsig/office/common/shape/Arrow;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public setFillColor(II)V
    .locals 1

    const v0, 0xffffff

    and-int/2addr p1, v0

    shl-int/lit8 p2, p2, 0x18

    or-int/2addr p1, p2

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_fillColor:I

    return-void
.end method

.method public setFillColor(IIII)V
    .locals 0

    and-int/lit16 p4, p4, 0xff

    shl-int/lit8 p4, p4, 0x18

    and-int/lit16 p1, p1, 0xff

    shl-int/lit8 p1, p1, 0x10

    or-int/2addr p1, p4

    and-int/lit16 p2, p2, 0xff

    shl-int/lit8 p2, p2, 0x8

    or-int/2addr p1, p2

    and-int/lit16 p2, p3, 0xff

    shl-int/lit8 p2, p2, 0x0

    or-int/2addr p1, p2

    .line 2
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_fillColor:I

    return-void
.end method

.method public setFillType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_fillType:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFilpH(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->flipH:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFlipV(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->flipV:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLineStyle(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_lineStyle:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLineStyleColor(I)V
    .locals 1

    const v0, 0xffffff

    and-int/2addr p1, v0

    const/high16 v0, -0x1000000

    or-int/2addr p1, v0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_lineStyleColor:I

    return-void
.end method

.method public setLineStyleColor(III)V
    .locals 1

    and-int/lit16 p1, p1, 0xff

    shl-int/lit8 p1, p1, 0x10

    const/high16 v0, -0x1000000

    or-int/2addr p1, v0

    and-int/lit16 p2, p2, 0xff

    shl-int/lit8 p2, p2, 0x8

    or-int/2addr p1, p2

    and-int/lit16 p2, p3, 0xff

    shl-int/lit8 p2, p2, 0x0

    or-int/2addr p1, p2

    .line 2
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_lineStyleColor:I

    return-void
.end method

.method public setLineWidth(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_lineWidth:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setNoBorder(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_noBorder:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setNoFill(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->_noFill:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRotation(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->angle:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setShapeType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->shapeType:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setStartArrow(BII)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/common/shape/Arrow;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2, p3}, Lcom/intsig/office/common/shape/Arrow;-><init>(BII)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->startArrow:Lcom/intsig/office/common/shape/Arrow;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

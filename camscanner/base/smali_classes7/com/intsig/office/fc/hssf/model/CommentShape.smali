.class public final Lcom/intsig/office/fc/hssf/model/CommentShape;
.super Lcom/intsig/office/fc/hssf/model/TextboxShape;
.source "CommentShape.java"


# instance fields
.field private _note:Lcom/intsig/office/fc/hssf/record/NoteRecord;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;I)V
    .locals 5

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hssf/model/TextboxShape;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;I)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hssf/model/CommentShape;->createNoteRecord(Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;I)Lcom/intsig/office/fc/hssf/record/NoteRecord;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/model/CommentShape;->_note:Lcom/intsig/office/fc/hssf/record/NoteRecord;

    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/model/TextboxShape;->getObjRecord()Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    .line 15
    .line 16
    .line 17
    move-result-object p2

    .line 18
    const/4 v0, 0x0

    .line 19
    const/4 v1, 0x0

    .line 20
    const/4 v2, 0x0

    .line 21
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    if-ge v1, v3, :cond_1

    .line 26
    .line 27
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;

    .line 32
    .line 33
    if-eqz v4, :cond_0

    .line 34
    .line 35
    check-cast v3, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;

    .line 36
    .line 37
    invoke-virtual {v3, v0}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setAutofill(Z)V

    .line 38
    .line 39
    .line 40
    move v2, v1

    .line 41
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    new-instance p2, Lcom/intsig/office/fc/hssf/record/NoteStructureSubRecord;

    .line 45
    .line 46
    invoke-direct {p2}, Lcom/intsig/office/fc/hssf/record/NoteStructureSubRecord;-><init>()V

    .line 47
    .line 48
    .line 49
    add-int/lit8 v2, v2, 0x1

    .line 50
    .line 51
    invoke-virtual {p1, v2, p2}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->addSubRecord(ILcom/intsig/office/fc/hssf/record/SubRecord;)V

    .line 52
    .line 53
    .line 54
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private createNoteRecord(Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;I)Lcom/intsig/office/fc/hssf/record/NoteRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/NoteRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/NoteRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;->getColumn()I

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/NoteRecord;->setColumn(I)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;->getRow()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/NoteRecord;->setRow(I)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;->isVisible()Z

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-eqz v1, :cond_0

    .line 25
    .line 26
    const/4 v1, 0x2

    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const/4 v1, 0x0

    .line 29
    :goto_0
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/NoteRecord;->setFlags(S)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hssf/record/NoteRecord;->setShapeId(I)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;->getAuthor()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p2

    .line 39
    if-nez p2, :cond_1

    .line 40
    .line 41
    const-string p1, ""

    .line 42
    .line 43
    goto :goto_1

    .line 44
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;->getAuthor()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    :goto_1
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/NoteRecord;->setAuthor(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    return-object v0
    .line 52
    .line 53
.end method


# virtual methods
.method protected addStandardOptions(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/ddf/EscherOptRecord;)I
    .locals 4

    .line 1
    invoke-super {p0, p1, p2}, Lcom/intsig/office/fc/hssf/model/AbstractShape;->addStandardOptions(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/ddf/EscherOptRecord;)I

    .line 2
    .line 3
    .line 4
    invoke-virtual {p2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->getEscherProperties()Ljava/util/List;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    const/16 v2, 0x3bf

    .line 17
    .line 18
    if-eqz v1, :cond_1

    .line 19
    .line 20
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherProperty;

    .line 25
    .line 26
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherProperty;->getId()S

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    const/16 v3, 0x183

    .line 31
    .line 32
    if-eq v1, v3, :cond_0

    .line 33
    .line 34
    const/16 v3, 0x1c0

    .line 35
    .line 36
    if-eq v1, v3, :cond_0

    .line 37
    .line 38
    if-eq v1, v2, :cond_0

    .line 39
    .line 40
    packed-switch v1, :pswitch_data_0

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_0
    :pswitch_0
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;

    .line 49
    .line 50
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 51
    .line 52
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;->isVisible()Z

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    if-eqz p1, :cond_2

    .line 57
    .line 58
    const/high16 p1, 0xa0000

    .line 59
    .line 60
    goto :goto_1

    .line 61
    :cond_2
    const p1, 0xa0002

    .line 62
    .line 63
    .line 64
    :goto_1
    invoke-direct {v0, v2, p1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 68
    .line 69
    .line 70
    new-instance p1, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 71
    .line 72
    const/16 v0, 0x23f

    .line 73
    .line 74
    const v1, 0x30003

    .line 75
    .line 76
    .line 77
    invoke-direct {p1, v0, v1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 81
    .line 82
    .line 83
    new-instance p1, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 84
    .line 85
    const/16 v0, 0x201

    .line 86
    .line 87
    const/4 v1, 0x0

    .line 88
    invoke-direct {p1, v0, v1}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {p2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->sortProperties()V

    .line 95
    .line 96
    .line 97
    invoke-virtual {p2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->getEscherProperties()Ljava/util/List;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 102
    .line 103
    .line 104
    move-result p1

    .line 105
    return p1

    .line 106
    nop

    .line 107
    :pswitch_data_0
    .packed-switch 0x81
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method getCmoObjectId(I)I
    .locals 0

    .line 1
    return p1
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getNoteRecord()Lcom/intsig/office/fc/hssf/record/NoteRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/CommentShape;->_note:Lcom/intsig/office/fc/hssf/record/NoteRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

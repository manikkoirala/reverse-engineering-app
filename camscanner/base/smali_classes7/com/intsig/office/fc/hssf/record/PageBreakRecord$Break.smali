.class public final Lcom/intsig/office/fc/hssf/record/PageBreakRecord$Break;
.super Ljava/lang/Object;
.source "PageBreakRecord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/record/PageBreakRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Break"
.end annotation


# static fields
.field public static final ENCODED_SIZE:I = 0x6


# instance fields
.field public main:I

.field public subFrom:I

.field public subTo:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/PageBreakRecord$Break;->main:I

    .line 3
    iput p2, p0, Lcom/intsig/office/fc/hssf/record/PageBreakRecord$Break;->subFrom:I

    .line 4
    iput p3, p0, Lcom/intsig/office/fc/hssf/record/PageBreakRecord$Break;->subTo:I

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V
    .locals 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUShort()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/PageBreakRecord$Break;->main:I

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUShort()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/PageBreakRecord$Break;->subFrom:I

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUShort()I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/hssf/record/PageBreakRecord$Break;->subTo:I

    return-void
.end method


# virtual methods
.method public serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/PageBreakRecord$Break;->main:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 6
    .line 7
    .line 8
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/PageBreakRecord$Break;->subFrom:I

    .line 9
    .line 10
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 11
    .line 12
    .line 13
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/PageBreakRecord$Break;->subTo:I

    .line 14
    .line 15
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

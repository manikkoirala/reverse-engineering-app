.class public Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;
.super Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;
.source "HSSFSimpleShape.java"


# static fields
.field public static final OBJECT_TYPE_COMBO_BOX:S = 0x14s

.field public static final OBJECT_TYPE_COMMENT:S = 0x19s

.field public static final OBJECT_TYPE_LINE:S = 0x1s

.field public static final OBJECT_TYPE_OVAL:S = 0x3s

.field public static final OBJECT_TYPE_PICTURE:S = 0x8s

.field public static final OBJECT_TYPE_RECTANGLE:S = 0x2s


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public processArrow(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)V
    .locals 3

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getStartArrowType(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-byte v0, v0

    .line 6
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getStartArrowWidth(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getStartArrowLength(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    invoke-virtual {p0, v0, v1, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setStartArrow(BII)V

    .line 15
    .line 16
    .line 17
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getEndArrowType(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    int-to-byte v0, v0

    .line 22
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getEndArrowWidth(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getEndArrowLength(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    invoke-virtual {p0, v0, v1, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setEndArrow(BII)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public processLine(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V
    .locals 2

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->hasLine(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_1

    .line 7
    .line 8
    invoke-static {p1, p2, v1}, Lcom/intsig/office/fc/ShapeKit;->getLineColor(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/lang/Object;I)Lcom/intsig/office/java/awt/Color;

    .line 9
    .line 10
    .line 11
    move-result-object p2

    .line 12
    if-eqz p2, :cond_0

    .line 13
    .line 14
    invoke-virtual {p2}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 15
    .line 16
    .line 17
    move-result p2

    .line 18
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setLineStyleColor(I)V

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setNoBorder(Z)V

    .line 23
    .line 24
    .line 25
    :goto_0
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getLineDashing(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setLineStyle(I)V

    .line 30
    .line 31
    .line 32
    goto :goto_1

    .line 33
    :cond_1
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setNoBorder(Z)V

    .line 34
    .line 35
    .line 36
    :goto_1
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public processRotationAndFlip(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)V
    .locals 1

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getRotation(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setRotation(I)V

    .line 6
    .line 7
    .line 8
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getFlipHorizontal(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setFilpH(Z)V

    .line 13
    .line 14
    .line 15
    invoke-static {p1}, Lcom/intsig/office/fc/ShapeKit;->getFlipVertical(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setFlipV(Z)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
.end method

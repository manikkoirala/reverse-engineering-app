.class public Lcom/intsig/office/fc/hssf/formula/function/Days360;
.super Lcom/intsig/office/fc/hssf/formula/function/Var2or3ArgFunction;
.source "Days360.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/function/Var2or3ArgFunction;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static evaluate(DDZ)D
    .locals 3

    .line 13
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/formula/function/Days360;->getStartingDate(D)Ljava/util/Calendar;

    move-result-object p0

    .line 14
    invoke-static {p2, p3, p0}, Lcom/intsig/office/fc/hssf/formula/function/Days360;->getEndingDateAccordingToStartingDate(DLjava/util/Calendar;)Ljava/util/Calendar;

    move-result-object p1

    const/4 p2, 0x2

    .line 15
    invoke-virtual {p0, p2}, Ljava/util/Calendar;->get(I)I

    move-result p3

    mul-int/lit8 p3, p3, 0x1e

    const/4 p4, 0x5

    invoke-virtual {p0, p4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/2addr p3, v0

    int-to-long v0, p3

    const/4 p3, 0x1

    .line 16
    invoke-virtual {p1, p3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {p0, p3}, Ljava/util/Calendar;->get(I)I

    move-result p0

    sub-int/2addr v2, p0

    mul-int/lit16 v2, v2, 0x168

    .line 17
    invoke-virtual {p1, p2}, Ljava/util/Calendar;->get(I)I

    move-result p0

    mul-int/lit8 p0, p0, 0x1e

    add-int/2addr v2, p0

    invoke-virtual {p1, p4}, Ljava/util/Calendar;->get(I)I

    move-result p0

    add-int/2addr v2, p0

    int-to-long p0, v2

    sub-long/2addr p0, v0

    long-to-double p0, p0

    return-wide p0
.end method

.method private static getDate(D)Ljava/util/Calendar;
    .locals 2

    .line 1
    new-instance v0, Ljava/util/GregorianCalendar;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-static {p0, p1, v1}, Lcom/intsig/office/ss/util/DateUtil;->getJavaDate(DZ)Ljava/util/Date;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 12
    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static getEndingDateAccordingToStartingDate(DLjava/util/Calendar;)Ljava/util/Calendar;
    .locals 2

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/formula/function/Days360;->getDate(D)Ljava/util/Calendar;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-static {p0, p1, v1}, Lcom/intsig/office/ss/util/DateUtil;->getJavaDate(DZ)Ljava/util/Date;

    .line 7
    .line 8
    .line 9
    move-result-object p0

    .line 10
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 11
    .line 12
    .line 13
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/function/Days360;->isLastDayOfMonth(Ljava/util/Calendar;)Z

    .line 14
    .line 15
    .line 16
    move-result p0

    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    const/4 p0, 0x5

    .line 20
    invoke-virtual {p2, p0}, Ljava/util/Calendar;->get(I)I

    .line 21
    .line 22
    .line 23
    move-result p0

    .line 24
    const/16 p1, 0x1e

    .line 25
    .line 26
    if-ge p0, p1, :cond_0

    .line 27
    .line 28
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/function/Days360;->getFirstDayOfNextMonth(Ljava/util/Calendar;)Ljava/util/Calendar;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    :cond_0
    return-object v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static getFirstDayOfNextMonth(Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 5

    .line 1
    invoke-virtual {p0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Ljava/util/Calendar;

    .line 6
    .line 7
    const/4 v1, 0x2

    .line 8
    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    const/16 v3, 0xb

    .line 13
    .line 14
    const/4 v4, 0x1

    .line 15
    if-ge v2, v3, :cond_0

    .line 16
    .line 17
    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    .line 18
    .line 19
    .line 20
    move-result p0

    .line 21
    add-int/2addr p0, v4

    .line 22
    invoke-virtual {v0, v1, p0}, Ljava/util/Calendar;->set(II)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    invoke-virtual {v0, v1, v4}, Ljava/util/Calendar;->set(II)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p0, v4}, Ljava/util/Calendar;->get(I)I

    .line 30
    .line 31
    .line 32
    move-result p0

    .line 33
    add-int/2addr p0, v4

    .line 34
    invoke-virtual {v0, v4, p0}, Ljava/util/Calendar;->set(II)V

    .line 35
    .line 36
    .line 37
    :goto_0
    const/4 p0, 0x5

    .line 38
    invoke-virtual {v0, p0, v4}, Ljava/util/Calendar;->set(II)V

    .line 39
    .line 40
    .line 41
    return-object v0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private static getStartingDate(D)Ljava/util/Calendar;
    .locals 1

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/formula/function/Days360;->getDate(D)Ljava/util/Calendar;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/Days360;->isLastDayOfMonth(Ljava/util/Calendar;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x5

    .line 12
    const/16 v0, 0x1e

    .line 13
    .line 14
    invoke-virtual {p0, p1, v0}, Ljava/util/Calendar;->set(II)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-object p0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static isLastDayOfMonth(Ljava/util/Calendar;)Z
    .locals 4

    .line 1
    invoke-virtual {p0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Ljava/util/Calendar;

    .line 6
    .line 7
    const/4 v1, 0x2

    .line 8
    const/4 v2, 0x1

    .line 9
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 10
    .line 11
    .line 12
    const/4 v1, -0x1

    .line 13
    const/4 v3, 0x5

    .line 14
    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->add(II)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    .line 22
    .line 23
    .line 24
    move-result p0

    .line 25
    if-ne p0, v0, :cond_0

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const/4 v2, 0x0

    .line 29
    :goto_0
    return v2
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 2

    .line 1
    :try_start_0
    invoke-static {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    move-result-wide v0

    .line 2
    invoke-static {p4, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    move-result-wide p1

    const/4 p3, 0x0

    .line 3
    invoke-static {v0, v1, p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/function/Days360;->evaluate(DDZ)D

    move-result-wide p1
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4
    new-instance p3, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    invoke-direct {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    return-object p3

    :catch_0
    move-exception p1

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object p1

    return-object p1
.end method

.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 2

    .line 6
    :try_start_0
    invoke-static {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    move-result-wide v0

    .line 7
    invoke-static {p4, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    move-result-wide p3

    .line 8
    invoke-static {p5, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->getSingleValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    const/4 p2, 0x0

    .line 9
    invoke-static {p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->coerceValueToBoolean(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Z)Ljava/lang/Boolean;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 10
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    :goto_0
    invoke-static {v0, v1, p3, p4, p2}, Lcom/intsig/office/fc/hssf/formula/function/Days360;->evaluate(DDZ)D

    move-result-wide p1
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    new-instance p3, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    invoke-direct {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    return-object p3

    :catch_0
    move-exception p1

    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object p1

    return-object p1
.end method

.class public abstract Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction;
.super Lcom/intsig/office/fc/hssf/formula/function/Fixed1ArgFunction;
.source "LogicalFunction.java"


# static fields
.field public static final ISBLANK:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final ISERROR:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final ISLOGICAL:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final ISNA:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final ISNONTEXT:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final ISNUMBER:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final ISREF:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final ISTEXT:Lcom/intsig/office/fc/hssf/formula/function/Function;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction;->ISLOGICAL:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$2;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$2;-><init>()V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction;->ISNONTEXT:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 14
    .line 15
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$3;

    .line 16
    .line 17
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$3;-><init>()V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction;->ISNUMBER:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 21
    .line 22
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$4;

    .line 23
    .line 24
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$4;-><init>()V

    .line 25
    .line 26
    .line 27
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction;->ISTEXT:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 28
    .line 29
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$5;

    .line 30
    .line 31
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$5;-><init>()V

    .line 32
    .line 33
    .line 34
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction;->ISBLANK:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 35
    .line 36
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$6;

    .line 37
    .line 38
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$6;-><init>()V

    .line 39
    .line 40
    .line 41
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction;->ISERROR:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 42
    .line 43
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$7;

    .line 44
    .line 45
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$7;-><init>()V

    .line 46
    .line 47
    .line 48
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction;->ISNA:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 49
    .line 50
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$8;

    .line 51
    .line 52
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction$8;-><init>()V

    .line 53
    .line 54
    .line 55
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction;->ISREF:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/function/Fixed1ArgFunction;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 0

    .line 1
    :try_start_0
    invoke-static {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->getSingleValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 2
    .line 3
    .line 4
    move-result-object p1
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5
    goto :goto_0

    .line 6
    :catch_0
    move-exception p1

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    :goto_0
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/formula/function/LogicalFunction;->evaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;->valueOf(Z)Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    return-object p1
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method protected abstract evaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Z
.end method

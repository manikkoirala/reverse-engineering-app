.class public Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;
.super Ljava/lang/Object;
.source "FormatTrackingHSSFListener.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/eventusermodel/HSSFListener;


# instance fields
.field private final _childListener:Lcom/intsig/office/fc/hssf/eventusermodel/HSSFListener;

.field private final _customFormatRecords:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/office/fc/hssf/record/FormatRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final _defaultFormat:Ljava/text/NumberFormat;

.field private final _formatter:Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormatter;

.field private final _xfRecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFListener;)V
    .locals 1

    .line 1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;-><init>(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFListener;Ljava/util/Locale;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/eventusermodel/HSSFListener;Ljava/util/Locale;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->_customFormatRecords:Ljava/util/Map;

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->_xfRecords:Ljava/util/List;

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->_childListener:Lcom/intsig/office/fc/hssf/eventusermodel/HSSFListener;

    .line 6
    new-instance p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormatter;

    invoke-direct {p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormatter;-><init>(Ljava/util/Locale;)V

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->_formatter:Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormatter;

    .line 7
    invoke-static {p2}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->_defaultFormat:Ljava/text/NumberFormat;

    return-void
.end method


# virtual methods
.method public formatNumberDateCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)Ljava/lang/String;
    .locals 4

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/record/NumberRecord;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    move-object v0, p1

    .line 6
    check-cast v0, Lcom/intsig/office/fc/hssf/record/NumberRecord;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/NumberRecord;->getValue()D

    .line 9
    .line 10
    .line 11
    move-result-wide v0

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/record/FormulaRecord;

    .line 14
    .line 15
    if-eqz v0, :cond_2

    .line 16
    .line 17
    move-object v0, p1

    .line 18
    check-cast v0, Lcom/intsig/office/fc/hssf/record/FormulaRecord;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->getValue()D

    .line 21
    .line 22
    .line 23
    move-result-wide v0

    .line 24
    :goto_0
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->getFormatIndex(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->getFormatString(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    if-nez p1, :cond_1

    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->_defaultFormat:Ljava/text/NumberFormat;

    .line 35
    .line 36
    invoke-virtual {p1, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    return-object p1

    .line 41
    :cond_1
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->_formatter:Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormatter;

    .line 42
    .line 43
    invoke-virtual {v3, v0, v1, v2, p1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->formatRawCellContents(DILjava/lang/String;)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    return-object p1

    .line 48
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 49
    .line 50
    new-instance v1, Ljava/lang/StringBuilder;

    .line 51
    .line 52
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 53
    .line 54
    .line 55
    const-string v2, "Unsupported CellValue Record passed in "

    .line 56
    .line 57
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    throw v0
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getFormatIndex(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->_xfRecords:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getXFIndex()S

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    .line 16
    .line 17
    new-instance v1, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    const-string v2, "Cell "

    .line 23
    .line 24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getRow()I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    const-string v2, ","

    .line 35
    .line 36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getColumn()S

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v2, " uses XF with index "

    .line 47
    .line 48
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getXFIndex()S

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    const-string p1, ", but we don\'t have that"

    .line 59
    .line 60
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    const/4 p1, -0x1

    .line 71
    return p1

    .line 72
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getFormatIndex()S

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    return p1
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getFormatString(I)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;->getNumberOfBuiltinBuiltinFormats()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->_customFormatRecords:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/hssf/record/FormatRecord;

    if-nez v0, :cond_0

    .line 3
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Requested format at index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", but it wasn\'t found"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 p1, 0x0

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FormatRecord;->getFormatString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    int-to-short p1, p1

    .line 5
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;->getBuiltinFormat(S)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public getFormatString(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)Ljava/lang/String;
    .locals 1

    .line 6
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->getFormatIndex(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)I

    move-result p1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 7
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->getFormatString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getNumberOfCustomFormats()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->_customFormatRecords:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getNumberOfExtendedFormats()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->_xfRecords:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public processRecord(Lcom/intsig/office/fc/hssf/record/Record;)V
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->processRecordInternally(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->_childListener:Lcom/intsig/office/fc/hssf/eventusermodel/HSSFListener;

    .line 5
    .line 6
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/hssf/eventusermodel/HSSFListener;->processRecord(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public processRecordInternally(Lcom/intsig/office/fc/hssf/record/Record;)V
    .locals 3

    .line 1
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/record/FormatRecord;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    move-object v0, p1

    .line 6
    check-cast v0, Lcom/intsig/office/fc/hssf/record/FormatRecord;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->_customFormatRecords:Ljava/util/Map;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FormatRecord;->getIndexCode()I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    :cond_0
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;

    .line 22
    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    check-cast p1, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/eventusermodel/FormatTrackingHSSFListener;->_xfRecords:Ljava/util/List;

    .line 28
    .line 29
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    :cond_1
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

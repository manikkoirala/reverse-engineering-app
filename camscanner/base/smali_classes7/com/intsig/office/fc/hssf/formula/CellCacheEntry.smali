.class abstract Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;
.super Ljava/lang/Object;
.source "CellCacheEntry.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/IEvaluationListener$ICacheEntry;


# static fields
.field public static final 〇o〇:[Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;


# instance fields
.field private final 〇080:Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;

.field private 〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    new-array v0, v0, [Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;

    .line 3
    .line 4
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇o〇:[Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇080:Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static 〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Z
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p0, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    if-eq v1, v2, :cond_1

    .line 14
    .line 15
    return v0

    .line 16
    :cond_1
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;->instance:Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;

    .line 17
    .line 18
    const/4 v3, 0x1

    .line 19
    if-ne p0, v2, :cond_3

    .line 20
    .line 21
    if-ne p1, p0, :cond_2

    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    :cond_2
    return v0

    .line 25
    :cond_3
    const-class v2, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 26
    .line 27
    if-ne v1, v2, :cond_5

    .line 28
    .line 29
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;->getNumberValue()D

    .line 32
    .line 33
    .line 34
    move-result-wide v1

    .line 35
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 36
    .line 37
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;->getNumberValue()D

    .line 38
    .line 39
    .line 40
    move-result-wide p0

    .line 41
    cmpl-double v4, v1, p0

    .line 42
    .line 43
    if-nez v4, :cond_4

    .line 44
    .line 45
    const/4 v0, 0x1

    .line 46
    :cond_4
    return v0

    .line 47
    :cond_5
    const-class v2, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;

    .line 48
    .line 49
    if-ne v1, v2, :cond_6

    .line 50
    .line 51
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;

    .line 52
    .line 53
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;->getStringValue()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p0

    .line 57
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;->getStringValue()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    move-result p0

    .line 67
    return p0

    .line 68
    :cond_6
    const-class v2, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 69
    .line 70
    if-ne v1, v2, :cond_8

    .line 71
    .line 72
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 73
    .line 74
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;->getBooleanValue()Z

    .line 75
    .line 76
    .line 77
    move-result p0

    .line 78
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 79
    .line 80
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;->getBooleanValue()Z

    .line 81
    .line 82
    .line 83
    move-result p1

    .line 84
    if-ne p0, p1, :cond_7

    .line 85
    .line 86
    const/4 v0, 0x1

    .line 87
    :cond_7
    return v0

    .line 88
    :cond_8
    const-class v2, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 89
    .line 90
    if-ne v1, v2, :cond_a

    .line 91
    .line 92
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 93
    .line 94
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->getErrorCode()I

    .line 95
    .line 96
    .line 97
    move-result p0

    .line 98
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 99
    .line 100
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->getErrorCode()I

    .line 101
    .line 102
    .line 103
    move-result p1

    .line 104
    if-ne p0, p1, :cond_9

    .line 105
    .line 106
    const/4 v0, 0x1

    .line 107
    :cond_9
    return v0

    .line 108
    :cond_a
    new-instance p0, Ljava/lang/IllegalStateException;

    .line 109
    .line 110
    new-instance p1, Ljava/lang/StringBuilder;

    .line 111
    .line 112
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 113
    .line 114
    .line 115
    const-string v0, "Unexpected value class ("

    .line 116
    .line 117
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    const-string v0, ")"

    .line 128
    .line 129
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 133
    .line 134
    .line 135
    move-result-object p1

    .line 136
    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    throw p0
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method


# virtual methods
.method protected final O8()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oo08()[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇080:Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->O8()[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getValue()Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected final oO80(Lcom/intsig/office/fc/hssf/formula/IEvaluationListener;I)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->Oo08()[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/hssf/formula/IEvaluationListener;->〇o〇([Lcom/intsig/office/fc/hssf/formula/IEvaluationListener$ICacheEntry;)V

    .line 6
    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    :goto_0
    array-length v2, v0

    .line 10
    if-ge v1, v2, :cond_0

    .line 11
    .line 12
    aget-object v2, v0, v1

    .line 13
    .line 14
    invoke-interface {p1, v2, p2}, Lcom/intsig/office/fc/hssf/formula/IEvaluationListener;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/IEvaluationListener$ICacheEntry;I)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;->〇8o8o〇()V

    .line 18
    .line 19
    .line 20
    add-int/lit8 v3, p2, 0x1

    .line 21
    .line 22
    invoke-virtual {v2, p1, v3}, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->oO80(Lcom/intsig/office/fc/hssf/formula/IEvaluationListener;I)V

    .line 23
    .line 24
    .line 25
    add-int/lit8 v1, v1, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected final o〇0()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->Oo08()[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    :goto_0
    array-length v2, v0

    .line 7
    if-ge v1, v2, :cond_0

    .line 8
    .line 9
    aget-object v2, v0, v1

    .line 10
    .line 11
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;->〇8o8o〇()V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->o〇0()V

    .line 15
    .line 16
    .line 17
    add-int/lit8 v1, v1, 0x1

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    return-void
    .line 21
.end method

.method public final 〇080(Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇080:Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇080(Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇80〇808〇O(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Z
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 4
    .line 5
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    xor-int/lit8 v0, v0, 0x1

    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 12
    .line 13
    return v0

    .line 14
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 15
    .line 16
    const-string v0, "Did not expect to update to null"

    .line 17
    .line 18
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    throw p1
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇o〇(Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇080:Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o〇(Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 11
    .line 12
    const-string v0, "Specified formula cell is not consumed by this cell"

    .line 13
    .line 14
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    throw p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇〇888(Lcom/intsig/office/fc/hssf/formula/IEvaluationListener;)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->o〇0()V

    .line 4
    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/hssf/formula/IEvaluationListener;->〇080(Lcom/intsig/office/fc/hssf/formula/IEvaluationListener$ICacheEntry;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->oO80(Lcom/intsig/office/fc/hssf/formula/IEvaluationListener;I)V

    .line 12
    .line 13
    .line 14
    :goto_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

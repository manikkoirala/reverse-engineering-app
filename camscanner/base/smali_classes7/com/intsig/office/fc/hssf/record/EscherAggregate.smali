.class public final Lcom/intsig/office/fc/hssf/record/EscherAggregate;
.super Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;
.source "EscherAggregate.java"


# static fields
.field private static log:Lcom/intsig/office/fc/util/POILogger; = null

.field public static final sid:S = 0x2694s


# instance fields
.field private chartToObj:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/intsig/office/fc/ddf/EscherRecord;",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;>;"
        }
    .end annotation
.end field

.field private drawingGroupId:S

.field private drawingManager:Lcom/intsig/office/fc/hssf/model/DrawingManager2;

.field protected patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

.field private shapeToObj:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/intsig/office/fc/ddf/EscherRecord;",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;"
        }
    .end annotation
.end field

.field private tailRec:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lcom/intsig/office/fc/util/POILogger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/model/DrawingManager2;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/HashMap;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    .line 10
    .line 11
    new-instance v0, Ljava/util/HashMap;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->chartToObj:Ljava/util/Map;

    .line 17
    .line 18
    new-instance v0, Ljava/util/ArrayList;

    .line 19
    .line 20
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->tailRec:Ljava/util/List;

    .line 24
    .line 25
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->drawingManager:Lcom/intsig/office/fc/hssf/model/DrawingManager2;

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private convertGroup(Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/util/Map;)V
    .locals 11

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 7
    .line 8
    invoke-direct {v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;-><init>()V

    .line 9
    .line 10
    .line 11
    new-instance v2, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;

    .line 12
    .line 13
    invoke-direct {v2}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;-><init>()V

    .line 14
    .line 15
    .line 16
    new-instance v3, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 17
    .line 18
    invoke-direct {v3}, Lcom/intsig/office/fc/ddf/EscherSpRecord;-><init>()V

    .line 19
    .line 20
    .line 21
    new-instance v4, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 22
    .line 23
    invoke-direct {v4}, Lcom/intsig/office/fc/ddf/EscherOptRecord;-><init>()V

    .line 24
    .line 25
    .line 26
    new-instance v5, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 27
    .line 28
    invoke-direct {v5}, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;-><init>()V

    .line 29
    .line 30
    .line 31
    const/16 v6, -0xffd

    .line 32
    .line 33
    invoke-virtual {v0, v6}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 34
    .line 35
    .line 36
    const/16 v6, 0xf

    .line 37
    .line 38
    invoke-virtual {v0, v6}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 39
    .line 40
    .line 41
    const/16 v7, -0xffc

    .line 42
    .line 43
    invoke-virtual {v1, v7}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v1, v6}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 47
    .line 48
    .line 49
    const/16 v6, -0xff7

    .line 50
    .line 51
    invoke-virtual {v2, v6}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 52
    .line 53
    .line 54
    const/4 v6, 0x1

    .line 55
    invoke-virtual {v2, v6}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->getX1()I

    .line 59
    .line 60
    .line 61
    move-result v7

    .line 62
    invoke-virtual {v2, v7}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectX1(I)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->getY1()I

    .line 66
    .line 67
    .line 68
    move-result v7

    .line 69
    invoke-virtual {v2, v7}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectY1(I)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->getX2()I

    .line 73
    .line 74
    .line 75
    move-result v7

    .line 76
    invoke-virtual {v2, v7}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectX2(I)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->getY2()I

    .line 80
    .line 81
    .line 82
    move-result v7

    .line 83
    invoke-virtual {v2, v7}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectY2(I)V

    .line 84
    .line 85
    .line 86
    const/16 v7, -0xff6

    .line 87
    .line 88
    invoke-virtual {v3, v7}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 89
    .line 90
    .line 91
    const/4 v7, 0x2

    .line 92
    invoke-virtual {v3, v7}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 93
    .line 94
    .line 95
    iget-object v7, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->drawingManager:Lcom/intsig/office/fc/hssf/model/DrawingManager2;

    .line 96
    .line 97
    iget-short v8, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->drawingGroupId:S

    .line 98
    .line 99
    invoke-virtual {v7, v8}, Lcom/intsig/office/fc/hssf/model/DrawingManager2;->allocateShapeId(S)I

    .line 100
    .line 101
    .line 102
    move-result v7

    .line 103
    invoke-virtual {v3, v7}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setShapeId(I)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getAnchor()Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;

    .line 107
    .line 108
    .line 109
    move-result-object v8

    .line 110
    instance-of v8, v8, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;

    .line 111
    .line 112
    if-eqz v8, :cond_0

    .line 113
    .line 114
    const/16 v8, 0x201

    .line 115
    .line 116
    invoke-virtual {v3, v8}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setFlags(I)V

    .line 117
    .line 118
    .line 119
    goto :goto_0

    .line 120
    :cond_0
    const/16 v8, 0x203

    .line 121
    .line 122
    invoke-virtual {v3, v8}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setFlags(I)V

    .line 123
    .line 124
    .line 125
    :goto_0
    const/16 v8, -0xff5

    .line 126
    .line 127
    invoke-virtual {v4, v8}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 128
    .line 129
    .line 130
    const/16 v8, 0x23

    .line 131
    .line 132
    invoke-virtual {v4, v8}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 133
    .line 134
    .line 135
    new-instance v8, Lcom/intsig/office/fc/ddf/EscherBoolProperty;

    .line 136
    .line 137
    const/16 v9, 0x7f

    .line 138
    .line 139
    const v10, 0x40004

    .line 140
    .line 141
    .line 142
    invoke-direct {v8, v9, v10}, Lcom/intsig/office/fc/ddf/EscherBoolProperty;-><init>(SI)V

    .line 143
    .line 144
    .line 145
    invoke-virtual {v4, v8}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 146
    .line 147
    .line 148
    new-instance v8, Lcom/intsig/office/fc/ddf/EscherBoolProperty;

    .line 149
    .line 150
    const/16 v9, 0x3bf

    .line 151
    .line 152
    const/high16 v10, 0x80000

    .line 153
    .line 154
    invoke-direct {v8, v9, v10}, Lcom/intsig/office/fc/ddf/EscherBoolProperty;-><init>(SI)V

    .line 155
    .line 156
    .line 157
    invoke-virtual {v4, v8}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 158
    .line 159
    .line 160
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getAnchor()Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;

    .line 161
    .line 162
    .line 163
    move-result-object v8

    .line 164
    invoke-static {v8}, Lcom/intsig/office/fc/hssf/model/ConvertAnchor;->createAnchor(Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 165
    .line 166
    .line 167
    move-result-object v8

    .line 168
    const/16 v9, -0xfef

    .line 169
    .line 170
    invoke-virtual {v5, v9}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 171
    .line 172
    .line 173
    const/4 v9, 0x0

    .line 174
    invoke-virtual {v5, v9}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 175
    .line 176
    .line 177
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 178
    .line 179
    .line 180
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 181
    .line 182
    .line 183
    invoke-virtual {v1, v3}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 184
    .line 185
    .line 186
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 187
    .line 188
    .line 189
    invoke-virtual {v1, v8}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 190
    .line 191
    .line 192
    invoke-virtual {v1, v5}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 193
    .line 194
    .line 195
    new-instance v1, Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 196
    .line 197
    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/ObjRecord;-><init>()V

    .line 198
    .line 199
    .line 200
    new-instance v2, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;

    .line 201
    .line 202
    invoke-direct {v2}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;-><init>()V

    .line 203
    .line 204
    .line 205
    invoke-virtual {v2, v9}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setObjectType(S)V

    .line 206
    .line 207
    .line 208
    invoke-virtual {v2, v7}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setObjectId(I)V

    .line 209
    .line 210
    .line 211
    invoke-virtual {v2, v6}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setLocked(Z)V

    .line 212
    .line 213
    .line 214
    invoke-virtual {v2, v6}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setPrintable(Z)V

    .line 215
    .line 216
    .line 217
    invoke-virtual {v2, v6}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setAutofill(Z)V

    .line 218
    .line 219
    .line 220
    invoke-virtual {v2, v6}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setAutoline(Z)V

    .line 221
    .line 222
    .line 223
    new-instance v3, Lcom/intsig/office/fc/hssf/record/GroupMarkerSubRecord;

    .line 224
    .line 225
    invoke-direct {v3}, Lcom/intsig/office/fc/hssf/record/GroupMarkerSubRecord;-><init>()V

    .line 226
    .line 227
    .line 228
    new-instance v4, Lcom/intsig/office/fc/hssf/record/EndSubRecord;

    .line 229
    .line 230
    invoke-direct {v4}, Lcom/intsig/office/fc/hssf/record/EndSubRecord;-><init>()V

    .line 231
    .line 232
    .line 233
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->addSubRecord(Lcom/intsig/office/fc/hssf/record/SubRecord;)Z

    .line 234
    .line 235
    .line 236
    invoke-virtual {v1, v3}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->addSubRecord(Lcom/intsig/office/fc/hssf/record/SubRecord;)Z

    .line 237
    .line 238
    .line 239
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->addSubRecord(Lcom/intsig/office/fc/hssf/record/SubRecord;)Z

    .line 240
    .line 241
    .line 242
    invoke-interface {p3, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    .line 244
    .line 245
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 246
    .line 247
    .line 248
    invoke-direct {p0, p1, v0, p3}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->convertShapes(Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeContainer;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/util/Map;)V

    .line 249
    .line 250
    .line 251
    return-void
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private convertPatriarch(Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;)V
    .locals 8

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 7
    .line 8
    invoke-direct {v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;-><init>()V

    .line 9
    .line 10
    .line 11
    new-instance v2, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 12
    .line 13
    invoke-direct {v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;-><init>()V

    .line 14
    .line 15
    .line 16
    new-instance v3, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;

    .line 17
    .line 18
    invoke-direct {v3}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;-><init>()V

    .line 19
    .line 20
    .line 21
    new-instance v4, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 22
    .line 23
    invoke-direct {v4}, Lcom/intsig/office/fc/ddf/EscherSpRecord;-><init>()V

    .line 24
    .line 25
    .line 26
    const/16 v5, -0xffe

    .line 27
    .line 28
    invoke-virtual {v0, v5}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 29
    .line 30
    .line 31
    const/16 v5, 0xf

    .line 32
    .line 33
    invoke-virtual {v0, v5}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 34
    .line 35
    .line 36
    iget-object v6, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->drawingManager:Lcom/intsig/office/fc/hssf/model/DrawingManager2;

    .line 37
    .line 38
    invoke-virtual {v6}, Lcom/intsig/office/fc/hssf/model/DrawingManager2;->createDgRecord()Lcom/intsig/office/fc/ddf/EscherDgRecord;

    .line 39
    .line 40
    .line 41
    move-result-object v6

    .line 42
    invoke-virtual {v6}, Lcom/intsig/office/fc/ddf/EscherDgRecord;->getDrawingGroupId()S

    .line 43
    .line 44
    .line 45
    move-result v7

    .line 46
    iput-short v7, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->drawingGroupId:S

    .line 47
    .line 48
    const/16 v7, -0xffd

    .line 49
    .line 50
    invoke-virtual {v1, v7}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v1, v5}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 54
    .line 55
    .line 56
    const/16 v7, -0xffc

    .line 57
    .line 58
    invoke-virtual {v2, v7}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 62
    .line 63
    .line 64
    const/16 v5, -0xff7

    .line 65
    .line 66
    invoke-virtual {v3, v5}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 67
    .line 68
    .line 69
    const/4 v5, 0x1

    .line 70
    invoke-virtual {v3, v5}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;->getX1()I

    .line 74
    .line 75
    .line 76
    move-result v5

    .line 77
    invoke-virtual {v3, v5}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectX1(I)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;->getY1()I

    .line 81
    .line 82
    .line 83
    move-result v5

    .line 84
    invoke-virtual {v3, v5}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectY1(I)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;->getX2()I

    .line 88
    .line 89
    .line 90
    move-result v5

    .line 91
    invoke-virtual {v3, v5}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectX2(I)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;->getY2()I

    .line 95
    .line 96
    .line 97
    move-result p1

    .line 98
    invoke-virtual {v3, p1}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->setRectY2(I)V

    .line 99
    .line 100
    .line 101
    const/16 p1, -0xff6

    .line 102
    .line 103
    invoke-virtual {v4, p1}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 104
    .line 105
    .line 106
    const/4 p1, 0x2

    .line 107
    invoke-virtual {v4, p1}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 108
    .line 109
    .line 110
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->drawingManager:Lcom/intsig/office/fc/hssf/model/DrawingManager2;

    .line 111
    .line 112
    invoke-virtual {v6}, Lcom/intsig/office/fc/ddf/EscherDgRecord;->getDrawingGroupId()S

    .line 113
    .line 114
    .line 115
    move-result v5

    .line 116
    invoke-virtual {p1, v5}, Lcom/intsig/office/fc/hssf/model/DrawingManager2;->allocateShapeId(S)I

    .line 117
    .line 118
    .line 119
    move-result p1

    .line 120
    invoke-virtual {v4, p1}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setShapeId(I)V

    .line 121
    .line 122
    .line 123
    const/4 p1, 0x5

    .line 124
    invoke-virtual {v4, p1}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setFlags(I)V

    .line 125
    .line 126
    .line 127
    invoke-virtual {v0, v6}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 128
    .line 129
    .line 130
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 131
    .line 132
    .line 133
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 134
    .line 135
    .line 136
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 137
    .line 138
    .line 139
    invoke-virtual {v2, v4}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 140
    .line 141
    .line 142
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->addEscherRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)Z

    .line 143
    .line 144
    .line 145
    return-void
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private convertRecordsToUserModel(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/lang/Object;)V
    .locals 5

    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 31
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 32
    instance-of v1, v0, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;

    if-eqz v1, :cond_2

    .line 33
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;

    .line 34
    instance-of v1, p2, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;

    if-eqz v1, :cond_1

    .line 35
    move-object v1, p2

    check-cast v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;

    .line 36
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectX1()I

    move-result v2

    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectY1()I

    move-result v3

    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectX2()I

    move-result v4

    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectY2()I

    move-result v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->setCoordinates(IIII)V

    goto :goto_0

    .line 37
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Got top level anchor but not processing a group"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 38
    :cond_2
    instance-of v1, v0, Lcom/intsig/office/fc/ddf/EscherClientAnchorRecord;

    if-eqz v1, :cond_3

    goto :goto_0

    .line 39
    :cond_3
    instance-of v1, v0, Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    if-eqz v1, :cond_5

    .line 40
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherTextboxRecord;

    .line 41
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/hssf/record/Record;

    .line 42
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/record/TextObjectRecord;

    if-eqz v1, :cond_0

    instance-of v1, p2, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;

    if-eqz v1, :cond_0

    .line 43
    check-cast v0, Lcom/intsig/office/fc/hssf/record/TextObjectRecord;

    .line 44
    move-object v1, p2

    check-cast v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;

    .line 45
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->isWordArt()Z

    move-result v2

    if-nez v2, :cond_4

    .line 46
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/TextObjectRecord;->getStr()Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->setString(Lcom/intsig/office/fc/ss/usermodel/RichTextString;)V

    .line 47
    :cond_4
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/TextObjectRecord;->getHorizontalTextAlignment()I

    move-result v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->setHorizontalAlignment(S)V

    .line 48
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/TextObjectRecord;->getVerticalTextAlignment()I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->setVerticalAlignment(S)V

    goto :goto_0

    .line 49
    :cond_5
    instance-of v1, v0, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    if-eqz v1, :cond_6

    instance-of v1, p2, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;

    if-eqz v1, :cond_6

    .line 50
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 51
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->chartToObj:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 52
    move-object v1, p2

    check-cast v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;

    invoke-static {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->convertRecordsToChart(Ljava/util/List;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)V

    goto/16 :goto_0

    .line 53
    :cond_6
    instance-of v1, v0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    if-eqz v1, :cond_7

    goto/16 :goto_0

    .line 54
    :cond_7
    instance-of v0, v0, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    goto/16 :goto_0

    :cond_8
    return-void
.end method

.method private convertShapes(Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeContainer;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/util/Map;)V
    .locals 3

    .line 1
    if-eqz p2, :cond_3

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeContainer;->getChildren()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_2

    .line 16
    .line 17
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;

    .line 22
    .line 23
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;

    .line 24
    .line 25
    if-eqz v1, :cond_0

    .line 26
    .line 27
    check-cast v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;

    .line 28
    .line 29
    invoke-direct {p0, v0, p2, p3}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->convertGroup(Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/util/Map;)V

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->drawingManager:Lcom/intsig/office/fc/hssf/model/DrawingManager2;

    .line 34
    .line 35
    iget-short v2, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->drawingGroupId:S

    .line 36
    .line 37
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hssf/model/DrawingManager2;->allocateShapeId(S)I

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    invoke-static {v0, v1}, Lcom/intsig/office/fc/hssf/model/AbstractShape;->createShape(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;I)Lcom/intsig/office/fc/hssf/model/AbstractShape;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/AbstractShape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->findClientData(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/AbstractShape;->getObjRecord()Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    invoke-interface {p3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/model/TextboxShape;

    .line 61
    .line 62
    if-eqz v1, :cond_1

    .line 63
    .line 64
    move-object v1, v0

    .line 65
    check-cast v1, Lcom/intsig/office/fc/hssf/model/TextboxShape;

    .line 66
    .line 67
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/TextboxShape;->getEscherTextbox()Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 68
    .line 69
    .line 70
    move-result-object v2

    .line 71
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/TextboxShape;->getTextObjectRecord()Lcom/intsig/office/fc/hssf/record/TextObjectRecord;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    invoke-interface {p3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    .line 77
    .line 78
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/model/CommentShape;

    .line 79
    .line 80
    if-eqz v1, :cond_1

    .line 81
    .line 82
    move-object v1, v0

    .line 83
    check-cast v1, Lcom/intsig/office/fc/hssf/model/CommentShape;

    .line 84
    .line 85
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->tailRec:Ljava/util/List;

    .line 86
    .line 87
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/CommentShape;->getNoteRecord()Lcom/intsig/office/fc/hssf/record/NoteRecord;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    .line 93
    .line 94
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/AbstractShape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 99
    .line 100
    .line 101
    goto :goto_0

    .line 102
    :cond_2
    return-void

    .line 103
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 104
    .line 105
    const-string p2, "Parent record required"

    .line 106
    .line 107
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    throw p1
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private convertUserModelToRecords()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    .line 6
    .line 7
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->tailRec:Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->chartToObj:Ljava/util/Map;

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->clearEscherRecords()V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;->getChildren()Ljava/util/List;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-eqz v0, :cond_2

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 36
    .line 37
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->convertPatriarch(Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;)V

    .line 38
    .line 39
    .line 40
    const/4 v0, 0x0

    .line 41
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->getEscherRecord(I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    const/4 v1, 0x0

    .line 52
    move-object v2, v1

    .line 53
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    if-eqz v3, :cond_1

    .line 58
    .line 59
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    move-result-object v3

    .line 63
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 64
    .line 65
    invoke-virtual {v3}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    .line 66
    .line 67
    .line 68
    move-result v4

    .line 69
    const/16 v5, -0xffd

    .line 70
    .line 71
    if-ne v4, v5, :cond_0

    .line 72
    .line 73
    move-object v2, v3

    .line 74
    check-cast v2, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 78
    .line 79
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    .line 80
    .line 81
    invoke-direct {p0, v0, v2, v3}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->convertShapes(Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeContainer;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/util/Map;)V

    .line 82
    .line 83
    .line 84
    iput-object v1, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 85
    .line 86
    :cond_2
    return-void
.end method

.method public static createAggregate(Ljava/util/List;ILcom/intsig/office/fc/hssf/model/DrawingManager2;)Lcom/intsig/office/fc/hssf/record/EscherAggregate;
    .locals 12

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/office/fc/hssf/record/EscherAggregate$1;

    .line 7
    .line 8
    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hssf/record/EscherAggregate$1;-><init>(Ljava/util/List;)V

    .line 9
    .line 10
    .line 11
    new-instance v2, Lcom/intsig/office/fc/hssf/record/EscherAggregate;

    .line 12
    .line 13
    invoke-direct {v2, p2}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;-><init>(Lcom/intsig/office/fc/hssf/model/DrawingManager2;)V

    .line 14
    .line 15
    .line 16
    const/4 p2, 0x0

    .line 17
    move v3, p1

    .line 18
    const/4 v4, 0x0

    .line 19
    :goto_0
    const/16 v5, 0xec

    .line 20
    .line 21
    const/4 v6, -0x1

    .line 22
    const/16 v7, 0x3c

    .line 23
    .line 24
    if-le v3, v6, :cond_3

    .line 25
    .line 26
    add-int/lit8 v8, v3, 0x1

    .line 27
    .line 28
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 29
    .line 30
    .line 31
    move-result v9

    .line 32
    if-ge v8, v9, :cond_3

    .line 33
    .line 34
    invoke-static {p0, v3}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    .line 35
    .line 36
    .line 37
    move-result v9

    .line 38
    if-eq v9, v5, :cond_0

    .line 39
    .line 40
    invoke-static {p0, v3}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    .line 41
    .line 42
    .line 43
    move-result v9

    .line 44
    if-ne v9, v7, :cond_3

    .line 45
    .line 46
    :cond_0
    invoke-static {p0, v8}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->isObjectRecord(Ljava/util/List;I)Z

    .line 47
    .line 48
    .line 49
    move-result v5

    .line 50
    if-eqz v5, :cond_2

    .line 51
    .line 52
    invoke-static {p0, v3}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    .line 53
    .line 54
    .line 55
    move-result v5

    .line 56
    if-ne v5, v7, :cond_1

    .line 57
    .line 58
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    move-result-object v5

    .line 62
    check-cast v5, Lcom/intsig/office/fc/hssf/record/ContinueRecord;

    .line 63
    .line 64
    invoke-virtual {v5}, Lcom/intsig/office/fc/hssf/record/ContinueRecord;->getDataSize()I

    .line 65
    .line 66
    .line 67
    move-result v5

    .line 68
    goto :goto_1

    .line 69
    :cond_1
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 70
    .line 71
    .line 72
    move-result-object v5

    .line 73
    check-cast v5, Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 74
    .line 75
    invoke-virtual {v5}, Lcom/intsig/office/fc/hssf/record/DrawingRecord;->getData()[B

    .line 76
    .line 77
    .line 78
    move-result-object v5

    .line 79
    array-length v5, v5

    .line 80
    :goto_1
    add-int/2addr v4, v5

    .line 81
    :cond_2
    invoke-static {p0, v3}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->nextDrawingRecord(Ljava/util/List;I)I

    .line 82
    .line 83
    .line 84
    move-result v3

    .line 85
    goto :goto_0

    .line 86
    :cond_3
    new-array v3, v4, [B

    .line 87
    .line 88
    move v8, p1

    .line 89
    const/4 v9, 0x0

    .line 90
    :goto_2
    if-le v8, v6, :cond_7

    .line 91
    .line 92
    add-int/lit8 v10, v8, 0x1

    .line 93
    .line 94
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 95
    .line 96
    .line 97
    move-result v11

    .line 98
    if-ge v10, v11, :cond_7

    .line 99
    .line 100
    invoke-static {p0, v8}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    .line 101
    .line 102
    .line 103
    move-result v11

    .line 104
    if-eq v11, v5, :cond_4

    .line 105
    .line 106
    invoke-static {p0, v8}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    .line 107
    .line 108
    .line 109
    move-result v11

    .line 110
    if-ne v11, v7, :cond_7

    .line 111
    .line 112
    :cond_4
    invoke-static {p0, v10}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->isObjectRecord(Ljava/util/List;I)Z

    .line 113
    .line 114
    .line 115
    move-result v10

    .line 116
    if-eqz v10, :cond_6

    .line 117
    .line 118
    invoke-static {p0, v8}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    .line 119
    .line 120
    .line 121
    move-result v10

    .line 122
    if-ne v10, v7, :cond_5

    .line 123
    .line 124
    invoke-interface {p0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 125
    .line 126
    .line 127
    move-result-object v10

    .line 128
    check-cast v10, Lcom/intsig/office/fc/hssf/record/ContinueRecord;

    .line 129
    .line 130
    invoke-virtual {v10}, Lcom/intsig/office/fc/hssf/record/ContinueRecord;->getData()[B

    .line 131
    .line 132
    .line 133
    move-result-object v10

    .line 134
    goto :goto_3

    .line 135
    :cond_5
    invoke-interface {p0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 136
    .line 137
    .line 138
    move-result-object v10

    .line 139
    check-cast v10, Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 140
    .line 141
    invoke-virtual {v10}, Lcom/intsig/office/fc/hssf/record/DrawingRecord;->getData()[B

    .line 142
    .line 143
    .line 144
    move-result-object v10

    .line 145
    :goto_3
    if-eqz v10, :cond_6

    .line 146
    .line 147
    array-length v11, v10

    .line 148
    invoke-static {v10, p2, v3, v9, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 149
    .line 150
    .line 151
    array-length v10, v10

    .line 152
    add-int/2addr v9, v10

    .line 153
    :cond_6
    invoke-static {p0, v8}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->nextDrawingRecord(Ljava/util/List;I)I

    .line 154
    .line 155
    .line 156
    move-result v8

    .line 157
    goto :goto_2

    .line 158
    :cond_7
    const/4 v8, 0x0

    .line 159
    :goto_4
    if-ge v8, v4, :cond_8

    .line 160
    .line 161
    :try_start_0
    invoke-interface {v1, v3, v8}, Lcom/intsig/office/fc/ddf/EscherRecordFactory;->createRecord([BI)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 162
    .line 163
    .line 164
    move-result-object v9

    .line 165
    invoke-virtual {v9, v3, v8, v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->fillFields([BILcom/intsig/office/fc/ddf/EscherRecordFactory;)I

    .line 166
    .line 167
    .line 168
    move-result v10

    .line 169
    invoke-virtual {v2, v9}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->addEscherRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    .line 171
    .line 172
    add-int/2addr v8, v10

    .line 173
    goto :goto_4

    .line 174
    :catch_0
    :cond_8
    new-instance v1, Ljava/util/HashMap;

    .line 175
    .line 176
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 177
    .line 178
    .line 179
    iput-object v1, v2, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    .line 180
    .line 181
    const/4 v1, 0x0

    .line 182
    :goto_5
    if-le p1, v6, :cond_e

    .line 183
    .line 184
    add-int/lit8 v3, p1, 0x1

    .line 185
    .line 186
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 187
    .line 188
    .line 189
    move-result v4

    .line 190
    if-ge v3, v4, :cond_e

    .line 191
    .line 192
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    .line 193
    .line 194
    .line 195
    move-result v4

    .line 196
    if-eq v4, v5, :cond_9

    .line 197
    .line 198
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    .line 199
    .line 200
    .line 201
    move-result v4

    .line 202
    if-ne v4, v7, :cond_e

    .line 203
    .line 204
    :cond_9
    invoke-static {p0, v3}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->isObjectRecord(Ljava/util/List;I)Z

    .line 205
    .line 206
    .line 207
    move-result v4

    .line 208
    if-nez v4, :cond_a

    .line 209
    .line 210
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->nextDrawingRecord(Ljava/util/List;I)I

    .line 211
    .line 212
    .line 213
    move-result p1

    .line 214
    goto :goto_5

    .line 215
    :cond_a
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 216
    .line 217
    .line 218
    move-result-object v3

    .line 219
    check-cast v3, Lcom/intsig/office/fc/hssf/record/Record;

    .line 220
    .line 221
    :try_start_1
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 222
    .line 223
    if-eqz v4, :cond_d

    .line 224
    .line 225
    move-object v4, v3

    .line 226
    check-cast v4, Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 227
    .line 228
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    .line 229
    .line 230
    .line 231
    move-result-object v4

    .line 232
    invoke-interface {v4, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 233
    .line 234
    .line 235
    move-result-object v4

    .line 236
    instance-of v4, v4, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;

    .line 237
    .line 238
    if-eqz v4, :cond_d

    .line 239
    .line 240
    move-object v4, v3

    .line 241
    check-cast v4, Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 242
    .line 243
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    .line 244
    .line 245
    .line 246
    move-result-object v4

    .line 247
    invoke-interface {v4, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 248
    .line 249
    .line 250
    move-result-object v4

    .line 251
    check-cast v4, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;

    .line 252
    .line 253
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->getObjectType()S

    .line 254
    .line 255
    .line 256
    move-result v4

    .line 257
    const/4 v8, 0x5

    .line 258
    if-ne v4, v8, :cond_c

    .line 259
    .line 260
    new-instance v3, Ljava/util/ArrayList;

    .line 261
    .line 262
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 263
    .line 264
    .line 265
    add-int/lit8 p1, p1, 0x2

    .line 266
    .line 267
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 268
    .line 269
    .line 270
    move-result-object v4

    .line 271
    check-cast v4, Lcom/intsig/office/fc/hssf/record/Record;

    .line 272
    .line 273
    :goto_6
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 274
    .line 275
    .line 276
    move-result v8

    .line 277
    const/16 v9, 0xa

    .line 278
    .line 279
    if-eq v8, v9, :cond_b

    .line 280
    .line 281
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    .line 283
    .line 284
    add-int/lit8 p1, p1, 0x1

    .line 285
    .line 286
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 287
    .line 288
    .line 289
    move-result-object v4

    .line 290
    check-cast v4, Lcom/intsig/office/fc/hssf/record/Record;

    .line 291
    .line 292
    goto :goto_6

    .line 293
    :cond_b
    iget-object v4, v2, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->chartToObj:Ljava/util/Map;

    .line 294
    .line 295
    add-int/lit8 v8, v1, 0x1

    .line 296
    .line 297
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 298
    .line 299
    .line 300
    move-result-object v1

    .line 301
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 302
    .line 303
    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    .line 305
    .line 306
    add-int/lit8 p1, p1, 0x1

    .line 307
    .line 308
    goto :goto_8

    .line 309
    :cond_c
    iget-object v4, v2, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    .line 310
    .line 311
    add-int/lit8 v8, v1, 0x1

    .line 312
    .line 313
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 314
    .line 315
    .line 316
    move-result-object v1

    .line 317
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 318
    .line 319
    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    .line 321
    .line 322
    goto :goto_7

    .line 323
    :cond_d
    iget-object v4, v2, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    .line 324
    .line 325
    add-int/lit8 v8, v1, 0x1

    .line 326
    .line 327
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 328
    .line 329
    .line 330
    move-result-object v1

    .line 331
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 332
    .line 333
    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 334
    .line 335
    .line 336
    :goto_7
    add-int/lit8 p1, p1, 0x2

    .line 337
    .line 338
    :goto_8
    move v1, v8

    .line 339
    goto/16 :goto_5

    .line 340
    .line 341
    :catch_1
    :cond_e
    return-object v2
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private findClientData(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)Lcom/intsig/office/fc/ddf/EscherRecord;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    const/16 v2, -0xfef

    .line 22
    .line 23
    if-ne v1, v2, :cond_0

    .line 24
    .line 25
    return-object v0

    .line 26
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 27
    .line 28
    const-string v0, "Can not find client data record"

    .line 29
    .line 30
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    throw p1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private getEscherRecordSize(Ljava/util/List;)I
    .locals 2

    .line 1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const/4 v0, 0x0

    .line 6
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordSize()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    add-int/2addr v0, v1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    return v0
.end method

.method private static isObjectRecord(Ljava/util/List;I)Z
    .locals 2

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/16 v1, 0x5d

    .line 6
    .line 7
    if-eq v0, v1, :cond_1

    .line 8
    .line 9
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    .line 10
    .line 11
    .line 12
    move-result p0

    .line 13
    const/16 p1, 0x1b6

    .line 14
    .line 15
    if-ne p0, p1, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 p0, 0x0

    .line 19
    goto :goto_1

    .line 20
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 21
    :goto_1
    return p0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static nextDrawingRecord(Ljava/util/List;I)I
    .locals 4

    .line 1
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    :cond_0
    :goto_0
    add-int/lit8 p1, p1, 0x1

    .line 6
    .line 7
    if-ge p1, v0, :cond_3

    .line 8
    .line 9
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    instance-of v2, v1, Lcom/intsig/office/fc/hssf/record/Record;

    .line 14
    .line 15
    if-nez v2, :cond_1

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    check-cast v1, Lcom/intsig/office/fc/hssf/record/Record;

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    const/16 v3, 0xec

    .line 25
    .line 26
    if-eq v2, v3, :cond_2

    .line 27
    .line 28
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    const/16 v2, 0x3c

    .line 33
    .line 34
    if-ne v1, v2, :cond_0

    .line 35
    .line 36
    :cond_2
    return p1

    .line 37
    :cond_3
    const/4 p0, -0x1

    .line 38
    return p0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static shapeContainRecords(Ljava/util/List;I)I
    .locals 5

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/16 v1, 0xec

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-eq v0, v1, :cond_0

    .line 9
    .line 10
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    const/16 v1, 0x3c

    .line 15
    .line 16
    if-ne v0, v1, :cond_4

    .line 17
    .line 18
    :cond_0
    add-int/lit8 v0, p1, 0x1

    .line 19
    .line 20
    invoke-static {p0, v0}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->isObjectRecord(Ljava/util/List;I)Z

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-eqz v1, :cond_4

    .line 25
    .line 26
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    check-cast v0, Lcom/intsig/office/fc/hssf/record/Record;

    .line 31
    .line 32
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 33
    .line 34
    const/4 v3, 0x2

    .line 35
    if-eqz v1, :cond_2

    .line 36
    .line 37
    check-cast v0, Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    instance-of v1, v1, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;

    .line 48
    .line 49
    if-eqz v1, :cond_2

    .line 50
    .line 51
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    check-cast v0, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;

    .line 60
    .line 61
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->getObjectType()S

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    const/4 v1, 0x5

    .line 66
    if-ne v0, v1, :cond_2

    .line 67
    .line 68
    new-instance v0, Ljava/util/ArrayList;

    .line 69
    .line 70
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 71
    .line 72
    .line 73
    add-int/2addr p1, v3

    .line 74
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    check-cast v1, Lcom/intsig/office/fc/hssf/record/Record;

    .line 79
    .line 80
    :goto_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 81
    .line 82
    .line 83
    move-result v2

    .line 84
    const/16 v4, 0xa

    .line 85
    .line 86
    if-eq v2, v4, :cond_1

    .line 87
    .line 88
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    .line 90
    .line 91
    add-int/lit8 p1, p1, 0x1

    .line 92
    .line 93
    add-int/lit8 v3, v3, 0x1

    .line 94
    .line 95
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 96
    .line 97
    .line 98
    move-result-object v1

    .line 99
    check-cast v1, Lcom/intsig/office/fc/hssf/record/Record;

    .line 100
    .line 101
    goto :goto_0

    .line 102
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 103
    .line 104
    return v3

    .line 105
    :cond_2
    add-int/2addr p1, v3

    .line 106
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 107
    .line 108
    .line 109
    move-result-object p0

    .line 110
    instance-of p0, p0, Lcom/intsig/office/fc/hssf/record/NoteRecord;

    .line 111
    .line 112
    if-eqz p0, :cond_3

    .line 113
    .line 114
    const/4 v2, 0x3

    .line 115
    goto :goto_1

    .line 116
    :cond_3
    const/4 v2, 0x2

    .line 117
    :cond_4
    :goto_1
    return v2
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static sid(Ljava/util/List;I)S
    .locals 0

    .line 1
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    check-cast p0, Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 8
    .line 9
    .line 10
    move-result p0

    .line 11
    return p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method associateShapeToObjRecord(Lcom/intsig/office/fc/ddf/EscherRecord;Lcom/intsig/office/fc/hssf/record/ObjRecord;)Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public clear()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->clearEscherRecords()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    .line 5
    .line 6
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->chartToObj:Ljava/util/Map;

    .line 10
    .line 11
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public convertRecordsToUserModel(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    if-eqz v0, :cond_a

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->getgetEscherContainers()Ljava/util/List;

    move-result-object v0

    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 4
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildContainers()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    if-lez v2, :cond_6

    .line 5
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildContainers()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 6
    invoke-virtual {v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildContainers()Ljava/util/List;

    move-result-object v2

    .line 7
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_5

    .line 8
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 9
    invoke-virtual {v4}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 10
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 11
    instance-of v6, v5, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;

    if-eqz v6, :cond_1

    .line 12
    check-cast v5, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;

    goto :goto_0

    :cond_2
    move-object v5, v3

    :goto_0
    if-eqz v5, :cond_3

    .line 13
    iget-object v4, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 14
    invoke-virtual {v5}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectX1()I

    move-result v6

    invoke-virtual {v5}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectY1()I

    move-result v7

    .line 15
    invoke-virtual {v5}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectX2()I

    move-result v8

    invoke-virtual {v5}, Lcom/intsig/office/fc/ddf/EscherSpgrRecord;->getRectY2()I

    move-result v5

    .line 16
    invoke-virtual {v4, v6, v7, v8, v5}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;->setCoordinates(IIII)V

    :cond_3
    const/4 v4, 0x1

    const/4 v5, 0x1

    .line 17
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_7

    .line 18
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 19
    iget-object v7, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    invoke-static {p1, v7, v6, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeFactory;->createShape(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Ljava/util/Map;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;)Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 20
    invoke-direct {p0, v6, v7}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->convertRecordsToUserModel(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/lang/Object;)V

    .line 21
    iget-object v6, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    invoke-virtual {v6, v7}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;->addShape(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;)V

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 22
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No child escher containers at the point that should hold the patriach data, and one container per top level shape!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    const/4 v4, 0x0

    .line 23
    :cond_7
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_9

    .line 24
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 25
    iget-object v5, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    invoke-static {p1, v5, v2, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeFactory;->createShape(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Ljava/util/Map;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;)Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;

    move-result-object v5

    if-eqz v5, :cond_8

    .line 26
    invoke-direct {p0, v2, v5}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->convertRecordsToUserModel(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Ljava/lang/Object;)V

    .line 27
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;->addShape(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;)V

    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 28
    :cond_9
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->drawingManager:Lcom/intsig/office/fc/hssf/model/DrawingManager2;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/DrawingManager2;->getDgg()Lcom/intsig/office/fc/ddf/EscherDggRecord;

    move-result-object p1

    new-array v0, v1, [Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;

    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/ddf/EscherDggRecord;->setFileIdClusters([Lcom/intsig/office/fc/ddf/EscherDggRecord$FileIdCluster;)V

    return-void

    .line 29
    :cond_a
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Must call setPatriarch() first"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getPatriarch()Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getRecordName()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "ESCHERAGGREGATE"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRecordSize()I
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->convertUserModelToRecords()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->getEscherRecords()Ljava/util/List;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->getEscherRecordSize(Ljava/util/List;)I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    .line 13
    .line 14
    invoke-interface {v1}, Ljava/util/Map;->size()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    mul-int/lit8 v1, v1, 0x4

    .line 19
    .line 20
    add-int/2addr v0, v1

    .line 21
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    .line 22
    .line 23
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    const/4 v2, 0x0

    .line 32
    const/4 v3, 0x0

    .line 33
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    if-eqz v4, :cond_0

    .line 38
    .line 39
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object v4

    .line 43
    check-cast v4, Lcom/intsig/office/fc/hssf/record/Record;

    .line 44
    .line 45
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/record/RecordBase;->getRecordSize()I

    .line 46
    .line 47
    .line 48
    move-result v4

    .line 49
    add-int/2addr v3, v4

    .line 50
    goto :goto_0

    .line 51
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->tailRec:Ljava/util/List;

    .line 52
    .line 53
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 58
    .line 59
    .line 60
    move-result v4

    .line 61
    if-eqz v4, :cond_1

    .line 62
    .line 63
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    move-result-object v4

    .line 67
    check-cast v4, Lcom/intsig/office/fc/hssf/record/Record;

    .line 68
    .line 69
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/record/RecordBase;->getRecordSize()I

    .line 70
    .line 71
    .line 72
    move-result v4

    .line 73
    add-int/2addr v2, v4

    .line 74
    goto :goto_1

    .line 75
    :cond_1
    add-int/2addr v0, v3

    .line 76
    add-int/2addr v0, v2

    .line 77
    return v0
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getSid()S
    .locals 1

    .line 1
    const/16 v0, 0x2694

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public serialize(I[B)I
    .locals 11

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->convertUserModelToRecords()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->getEscherRecords()Ljava/util/List;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->getEscherRecordSize(Ljava/util/List;)I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    new-array v1, v1, [B

    .line 13
    .line 14
    new-instance v2, Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 17
    .line 18
    .line 19
    new-instance v3, Ljava/util/ArrayList;

    .line 20
    .line 21
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 22
    .line 23
    .line 24
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const/4 v4, 0x0

    .line 29
    const/4 v5, 0x0

    .line 30
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 31
    .line 32
    .line 33
    move-result v6

    .line 34
    if-eqz v6, :cond_0

    .line 35
    .line 36
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v6

    .line 40
    check-cast v6, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 41
    .line 42
    new-instance v7, Lcom/intsig/office/fc/hssf/record/EscherAggregate$2;

    .line 43
    .line 44
    invoke-direct {v7, p0, v2, v3}, Lcom/intsig/office/fc/hssf/record/EscherAggregate$2;-><init>(Lcom/intsig/office/fc/hssf/record/EscherAggregate;Ljava/util/List;Ljava/util/List;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v6, v5, v1, v7}, Lcom/intsig/office/fc/ddf/EscherRecord;->serialize(I[BLcom/intsig/office/fc/ddf/EscherSerializationListener;)I

    .line 48
    .line 49
    .line 50
    move-result v6

    .line 51
    add-int/2addr v5, v6

    .line 52
    goto :goto_0

    .line 53
    :cond_0
    const/4 v0, 0x0

    .line 54
    invoke-interface {v3, v4, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 55
    .line 56
    .line 57
    invoke-interface {v2, v4, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 58
    .line 59
    .line 60
    const/4 v0, 0x1

    .line 61
    move v6, p1

    .line 62
    const/4 v5, 0x1

    .line 63
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 64
    .line 65
    .line 66
    move-result v7

    .line 67
    if-ge v5, v7, :cond_2

    .line 68
    .line 69
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 70
    .line 71
    .line 72
    move-result-object v7

    .line 73
    check-cast v7, Ljava/lang/Integer;

    .line 74
    .line 75
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    .line 76
    .line 77
    .line 78
    move-result v7

    .line 79
    sub-int/2addr v7, v0

    .line 80
    if-ne v5, v0, :cond_1

    .line 81
    .line 82
    const/4 v8, 0x0

    .line 83
    goto :goto_2

    .line 84
    :cond_1
    add-int/lit8 v8, v5, -0x1

    .line 85
    .line 86
    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 87
    .line 88
    .line 89
    move-result-object v8

    .line 90
    check-cast v8, Ljava/lang/Integer;

    .line 91
    .line 92
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    .line 93
    .line 94
    .line 95
    move-result v8

    .line 96
    :goto_2
    new-instance v9, Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 97
    .line 98
    invoke-direct {v9}, Lcom/intsig/office/fc/hssf/record/DrawingRecord;-><init>()V

    .line 99
    .line 100
    .line 101
    sub-int/2addr v7, v8

    .line 102
    add-int/2addr v7, v0

    .line 103
    new-array v10, v7, [B

    .line 104
    .line 105
    invoke-static {v1, v8, v10, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 106
    .line 107
    .line 108
    invoke-virtual {v9, v10}, Lcom/intsig/office/fc/hssf/record/DrawingRecord;->setData([B)V

    .line 109
    .line 110
    .line 111
    invoke-virtual {v9, v6, p2}, Lcom/intsig/office/fc/hssf/record/StandardRecord;->serialize(I[B)I

    .line 112
    .line 113
    .line 114
    move-result v7

    .line 115
    add-int/2addr v6, v7

    .line 116
    iget-object v7, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    .line 117
    .line 118
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 119
    .line 120
    .line 121
    move-result-object v8

    .line 122
    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    .line 124
    .line 125
    move-result-object v7

    .line 126
    check-cast v7, Lcom/intsig/office/fc/hssf/record/Record;

    .line 127
    .line 128
    invoke-virtual {v7, v6, p2}, Lcom/intsig/office/fc/hssf/record/RecordBase;->serialize(I[B)I

    .line 129
    .line 130
    .line 131
    move-result v7

    .line 132
    add-int/2addr v6, v7

    .line 133
    add-int/lit8 v5, v5, 0x1

    .line 134
    .line 135
    goto :goto_1

    .line 136
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->tailRec:Ljava/util/List;

    .line 137
    .line 138
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 139
    .line 140
    .line 141
    move-result v0

    .line 142
    if-ge v4, v0, :cond_3

    .line 143
    .line 144
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->tailRec:Ljava/util/List;

    .line 145
    .line 146
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    check-cast v0, Lcom/intsig/office/fc/hssf/record/Record;

    .line 151
    .line 152
    invoke-virtual {v0, v6, p2}, Lcom/intsig/office/fc/hssf/record/RecordBase;->serialize(I[B)I

    .line 153
    .line 154
    .line 155
    move-result v0

    .line 156
    add-int/2addr v6, v0

    .line 157
    add-int/lit8 v4, v4, 0x1

    .line 158
    .line 159
    goto :goto_3

    .line 160
    :cond_3
    sub-int/2addr v6, p1

    .line 161
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->getRecordSize()I

    .line 162
    .line 163
    .line 164
    move-result p1

    .line 165
    if-ne v6, p1, :cond_4

    .line 166
    .line 167
    return v6

    .line 168
    :cond_4
    new-instance p1, Lcom/intsig/office/fc/hssf/record/RecordFormatException;

    .line 169
    .line 170
    new-instance p2, Ljava/lang/StringBuilder;

    .line 171
    .line 172
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 173
    .line 174
    .line 175
    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    const-string v0, " bytes written but getRecordSize() reports "

    .line 179
    .line 180
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    .line 182
    .line 183
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->getRecordSize()I

    .line 184
    .line 185
    .line 186
    move-result v0

    .line 187
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 188
    .line 189
    .line 190
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 191
    .line 192
    .line 193
    move-result-object p2

    .line 194
    invoke-direct {p1, p2}, Lcom/intsig/office/fc/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    .line 195
    .line 196
    .line 197
    throw p1
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public setPatriarch(Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 1
    const-string v0, "line.separtor"

    .line 2
    .line 3
    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    new-instance v1, Ljava/lang/StringBuffer;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 10
    .line 11
    .line 12
    const/16 v2, 0x5b

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->getRecordName()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 22
    .line 23
    .line 24
    new-instance v2, Ljava/lang/StringBuilder;

    .line 25
    .line 26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 27
    .line 28
    .line 29
    const/16 v3, 0x5d

    .line 30
    .line 31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->getEscherRecords()Ljava/util/List;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 53
    .line 54
    .line 55
    move-result v4

    .line 56
    if-eqz v4, :cond_0

    .line 57
    .line 58
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    move-result-object v4

    .line 62
    check-cast v4, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 63
    .line 64
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v4

    .line 68
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 69
    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_0
    const-string v2, "[/"

    .line 73
    .line 74
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 75
    .line 76
    .line 77
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->getRecordName()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 82
    .line 83
    .line 84
    new-instance v2, Ljava/lang/StringBuilder;

    .line 85
    .line 86
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    .line 88
    .line 89
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    return-object v0
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

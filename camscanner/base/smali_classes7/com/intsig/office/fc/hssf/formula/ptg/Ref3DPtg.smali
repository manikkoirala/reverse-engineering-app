.class public final Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;
.super Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;
.source "Ref3DPtg.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/WorkbookDependentFormula;
.implements Lcom/intsig/office/fc/hssf/formula/ExternSheetReferenceToken;


# static fields
.field private static final SIZE:I = 0x7

.field public static final sid:B = 0x3at


# instance fields
.field private field_1_index_extern_sheet:I


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/ss/util/CellReference;I)V
    .locals 0

    .line 5
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;-><init>(Lcom/intsig/office/fc/ss/util/CellReference;)V

    .line 6
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;->setExternSheetIndex(I)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;-><init>()V

    .line 2
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readShort()S

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;->field_1_index_extern_sheet:I

    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->readCoordinates(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .line 4
    new-instance v0, Lcom/intsig/office/fc/ss/util/CellReference;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/ss/util/CellReference;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;-><init>(Lcom/intsig/office/fc/ss/util/CellReference;I)V

    return-void
.end method


# virtual methods
.method public format2DRefAsString()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->formatReferenceAsString()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getExternSheetIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;->field_1_index_extern_sheet:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSize()I
    .locals 1

    .line 1
    const/4 v0, 0x7

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setExternSheetIndex(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;->field_1_index_extern_sheet:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toFormulaString()Ljava/lang/String;
    .locals 2

    .line 2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "3D references need a workbook to determine formula text"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toFormulaString(Lcom/intsig/office/fc/hssf/formula/FormulaRenderingWorkbook;)Ljava/lang/String;
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;->field_1_index_extern_sheet:I

    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->formatReferenceAsString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/ExternSheetNameResolver;->〇080(Lcom/intsig/office/fc/hssf/formula/FormulaRenderingWorkbook;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    const-class v1, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;

    .line 7
    .line 8
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 13
    .line 14
    .line 15
    const-string v1, " ["

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 18
    .line 19
    .line 20
    const-string v1, "sheetIx="

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;->getExternSheetIndex()I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 30
    .line 31
    .line 32
    const-string v1, " ! "

    .line 33
    .line 34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->formatReferenceAsString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 42
    .line 43
    .line 44
    const-string v1, "]"

    .line 45
    .line 46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    return-object v0
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public write(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getPtgClass()B

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    add-int/lit8 v0, v0, 0x3a

    .line 6
    .line 7
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeByte(I)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;->getExternSheetIndex()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->writeCoordinates(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
.end method

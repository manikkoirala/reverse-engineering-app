.class final Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;
.super Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
.source "ArrayPtg.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Initial"
.end annotation


# instance fields
.field private final OO:I

.field private final o0:I

.field private final 〇OOo8〇0:I


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readInt()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;->o0:I

    .line 9
    .line 10
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;->〇OOo8〇0:I

    .line 15
    .line 16
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUByte()I

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;->OO:I

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
.end method

.method private static 〇o00〇〇Oo()Ljava/lang/RuntimeException;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 2
    .line 3
    const-string v1, "This object is a partially initialised tArray, and cannot be used as a Ptg"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    throw v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public getDefaultOperandClass()B
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;->〇o00〇〇Oo()Ljava/lang/RuntimeException;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    throw v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSize()I
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isBaseToken()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public toFormulaString()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;->〇o00〇〇Oo()Ljava/lang/RuntimeException;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    throw v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public write(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 0

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;->〇o00〇〇Oo()Ljava/lang/RuntimeException;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    throw p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇080(Lcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg;
    .locals 9

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUByte()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readShort()S

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    add-int/lit8 v6, v0, 0x1

    .line 10
    .line 11
    add-int/lit8 v1, v1, 0x1

    .line 12
    .line 13
    int-to-short v7, v1

    .line 14
    mul-int v0, v7, v6

    .line 15
    .line 16
    invoke-static {p1, v0}, Lcom/intsig/office/constant/fc/ConstantValueParser;->parse(Lcom/intsig/office/fc/util/LittleEndianInput;I)[Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v8

    .line 20
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg;

    .line 21
    .line 22
    iget v3, p0, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;->o0:I

    .line 23
    .line 24
    iget v4, p0, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;->〇OOo8〇0:I

    .line 25
    .line 26
    iget v5, p0, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;->OO:I

    .line 27
    .line 28
    move-object v2, p1

    .line 29
    invoke-direct/range {v2 .. v8}, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg;-><init>(IIIII[Ljava/lang/Object;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getPtgClass()B

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->setClass(B)V

    .line 37
    .line 38
    .line 39
    return-object p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

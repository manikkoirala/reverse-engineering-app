.class final Lcom/intsig/office/fc/hssf/record/RecordInputStream$SimpleHeaderInput;
.super Ljava/lang/Object;
.source "RecordInputStream.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/record/BiffHeaderInput;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/record/RecordInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SimpleHeaderInput"
.end annotation


# instance fields
.field private final o0:Lcom/intsig/office/fc/util/LittleEndianInput;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->getLEI(Ljava/io/InputStream;)Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/RecordInputStream$SimpleHeaderInput;->o0:Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public available()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordInputStream$SimpleHeaderInput;->o0:Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/util/LittleEndianInput;->available()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public readDataSize()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordInputStream$SimpleHeaderInput;->o0:Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public readRecordSID()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/RecordInputStream$SimpleHeaderInput;->o0:Lcom/intsig/office/fc/util/LittleEndianInput;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

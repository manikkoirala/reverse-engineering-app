.class public final Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;
.super Lcom/intsig/office/fc/hssf/record/StandardRecord;
.source "PrintSetupRecord.java"


# static fields
.field private static final draft:Lcom/intsig/office/fc/util/BitField;

.field private static final landscape:Lcom/intsig/office/fc/util/BitField;

.field private static final lefttoright:Lcom/intsig/office/fc/util/BitField;

.field private static final noOrientation:Lcom/intsig/office/fc/util/BitField;

.field private static final nocolor:Lcom/intsig/office/fc/util/BitField;

.field private static final notes:Lcom/intsig/office/fc/util/BitField;

.field public static final sid:S = 0xa1s

.field private static final usepage:Lcom/intsig/office/fc/util/BitField;

.field private static final validsettings:Lcom/intsig/office/fc/util/BitField;


# instance fields
.field private field_10_footermargin:D

.field private field_11_copies:S

.field private field_1_paper_size:S

.field private field_2_scale:S

.field private field_3_page_start:S

.field private field_4_fit_width:S

.field private field_5_fit_height:S

.field private field_6_options:S

.field private field_7_hresolution:S

.field private field_8_vresolution:S

.field private field_9_headermargin:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    sput-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->lefttoright:Lcom/intsig/office/fc/util/BitField;

    .line 7
    .line 8
    const/4 v0, 0x2

    .line 9
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    sput-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->landscape:Lcom/intsig/office/fc/util/BitField;

    .line 14
    .line 15
    const/4 v0, 0x4

    .line 16
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    sput-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->validsettings:Lcom/intsig/office/fc/util/BitField;

    .line 21
    .line 22
    const/16 v0, 0x8

    .line 23
    .line 24
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    sput-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->nocolor:Lcom/intsig/office/fc/util/BitField;

    .line 29
    .line 30
    const/16 v0, 0x10

    .line 31
    .line 32
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    sput-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->draft:Lcom/intsig/office/fc/util/BitField;

    .line 37
    .line 38
    const/16 v0, 0x20

    .line 39
    .line 40
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    sput-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->notes:Lcom/intsig/office/fc/util/BitField;

    .line 45
    .line 46
    const/16 v0, 0x40

    .line 47
    .line 48
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    sput-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->noOrientation:Lcom/intsig/office/fc/util/BitField;

    .line 53
    .line 54
    const/16 v0, 0x80

    .line 55
    .line 56
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    sput-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->usepage:Lcom/intsig/office/fc/util/BitField;

    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V
    .locals 2

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_1_paper_size:S

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_2_scale:S

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_3_page_start:S

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_4_fit_width:S

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_5_fit_height:S

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_7_hresolution:S

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_8_vresolution:S

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_9_headermargin:D

    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_10_footermargin:D

    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result p1

    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_11_copies:S

    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_1_paper_size:S

    .line 7
    .line 8
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_1_paper_size:S

    .line 9
    .line 10
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_2_scale:S

    .line 11
    .line 12
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_2_scale:S

    .line 13
    .line 14
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_3_page_start:S

    .line 15
    .line 16
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_3_page_start:S

    .line 17
    .line 18
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_4_fit_width:S

    .line 19
    .line 20
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_4_fit_width:S

    .line 21
    .line 22
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_5_fit_height:S

    .line 23
    .line 24
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_5_fit_height:S

    .line 25
    .line 26
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 27
    .line 28
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 29
    .line 30
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_7_hresolution:S

    .line 31
    .line 32
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_7_hresolution:S

    .line 33
    .line 34
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_8_vresolution:S

    .line 35
    .line 36
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_8_vresolution:S

    .line 37
    .line 38
    iget-wide v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_9_headermargin:D

    .line 39
    .line 40
    iput-wide v1, v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_9_headermargin:D

    .line 41
    .line 42
    iget-wide v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_10_footermargin:D

    .line 43
    .line 44
    iput-wide v1, v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_10_footermargin:D

    .line 45
    .line 46
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_11_copies:S

    .line 47
    .line 48
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_11_copies:S

    .line 49
    .line 50
    return-object v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getCopies()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_11_copies:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getDataSize()I
    .locals 1

    .line 1
    const/16 v0, 0x22

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDraft()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->draft:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFitHeight()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_5_fit_height:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFitWidth()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_4_fit_width:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFooterMargin()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_10_footermargin:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHResolution()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_7_hresolution:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHeaderMargin()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_9_headermargin:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLandscape()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->landscape:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLeftToRight()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->lefttoright:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNoColor()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->nocolor:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNoOrientation()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->noOrientation:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNotes()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->notes:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOptions()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPageStart()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_3_page_start:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPaperSize()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_1_paper_size:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getScale()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_2_scale:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSid()S
    .locals 1

    .line 1
    const/16 v0, 0xa1

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getUsePage()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->usepage:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getVResolution()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_8_vresolution:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getValidSettings()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->validsettings:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getPaperSize()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getScale()S

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getPageStart()S

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getFitWidth()S

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getFitHeight()S

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getOptions()S

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getHResolution()S

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getVResolution()S

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getHeaderMargin()D

    .line 58
    .line 59
    .line 60
    move-result-wide v0

    .line 61
    invoke-interface {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeDouble(D)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getFooterMargin()D

    .line 65
    .line 66
    .line 67
    move-result-wide v0

    .line 68
    invoke-interface {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeDouble(D)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getCopies()S

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 76
    .line 77
    .line 78
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setCopies(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_11_copies:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDraft(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->draft:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setShortBoolean(SZ)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFitHeight(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_5_fit_height:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFitWidth(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_4_fit_width:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFooterMargin(D)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_10_footermargin:D

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setHResolution(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_7_hresolution:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setHeaderMargin(D)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_9_headermargin:D

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLandscape(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->landscape:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setShortBoolean(SZ)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLeftToRight(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->lefttoright:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setShortBoolean(SZ)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setNoColor(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->nocolor:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setShortBoolean(SZ)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setNoOrientation(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->noOrientation:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setShortBoolean(SZ)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setNotes(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->notes:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setShortBoolean(SZ)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOptions(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPageStart(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_3_page_start:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPaperSize(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_1_paper_size:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setScale(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_2_scale:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setUsePage(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->usepage:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setShortBoolean(SZ)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setVResolution(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_8_vresolution:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setValidSettings(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->validsettings:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setShortBoolean(SZ)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->field_6_options:S

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "[PRINTSETUP]\n"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 9
    .line 10
    .line 11
    const-string v1, "    .papersize      = "

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getPaperSize()S

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 21
    .line 22
    .line 23
    const-string v1, "\n"

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 26
    .line 27
    .line 28
    const-string v2, "    .scale          = "

    .line 29
    .line 30
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 31
    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getScale()S

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 41
    .line 42
    .line 43
    const-string v2, "    .pagestart      = "

    .line 44
    .line 45
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 46
    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getPageStart()S

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 56
    .line 57
    .line 58
    const-string v2, "    .fitwidth       = "

    .line 59
    .line 60
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 61
    .line 62
    .line 63
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getFitWidth()S

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 71
    .line 72
    .line 73
    const-string v2, "    .fitheight      = "

    .line 74
    .line 75
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 76
    .line 77
    .line 78
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getFitHeight()S

    .line 79
    .line 80
    .line 81
    move-result v2

    .line 82
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 86
    .line 87
    .line 88
    const-string v2, "    .options        = "

    .line 89
    .line 90
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 91
    .line 92
    .line 93
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getOptions()S

    .line 94
    .line 95
    .line 96
    move-result v2

    .line 97
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 98
    .line 99
    .line 100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 101
    .line 102
    .line 103
    const-string v2, "        .ltor       = "

    .line 104
    .line 105
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 106
    .line 107
    .line 108
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getLeftToRight()Z

    .line 109
    .line 110
    .line 111
    move-result v2

    .line 112
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 113
    .line 114
    .line 115
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 116
    .line 117
    .line 118
    const-string v2, "        .landscape  = "

    .line 119
    .line 120
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 121
    .line 122
    .line 123
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getLandscape()Z

    .line 124
    .line 125
    .line 126
    move-result v2

    .line 127
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 128
    .line 129
    .line 130
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 131
    .line 132
    .line 133
    const-string v2, "        .valid      = "

    .line 134
    .line 135
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 136
    .line 137
    .line 138
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getValidSettings()Z

    .line 139
    .line 140
    .line 141
    move-result v2

    .line 142
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 143
    .line 144
    .line 145
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 146
    .line 147
    .line 148
    const-string v2, "        .mono       = "

    .line 149
    .line 150
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 151
    .line 152
    .line 153
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getNoColor()Z

    .line 154
    .line 155
    .line 156
    move-result v2

    .line 157
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 158
    .line 159
    .line 160
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 161
    .line 162
    .line 163
    const-string v2, "        .draft      = "

    .line 164
    .line 165
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 166
    .line 167
    .line 168
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getDraft()Z

    .line 169
    .line 170
    .line 171
    move-result v2

    .line 172
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 173
    .line 174
    .line 175
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 176
    .line 177
    .line 178
    const-string v2, "        .notes      = "

    .line 179
    .line 180
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 181
    .line 182
    .line 183
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getNotes()Z

    .line 184
    .line 185
    .line 186
    move-result v2

    .line 187
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 188
    .line 189
    .line 190
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 191
    .line 192
    .line 193
    const-string v2, "        .noOrientat = "

    .line 194
    .line 195
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 196
    .line 197
    .line 198
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getNoOrientation()Z

    .line 199
    .line 200
    .line 201
    move-result v2

    .line 202
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 203
    .line 204
    .line 205
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 206
    .line 207
    .line 208
    const-string v2, "        .usepage    = "

    .line 209
    .line 210
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 211
    .line 212
    .line 213
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getUsePage()Z

    .line 214
    .line 215
    .line 216
    move-result v2

    .line 217
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 218
    .line 219
    .line 220
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 221
    .line 222
    .line 223
    const-string v2, "    .hresolution    = "

    .line 224
    .line 225
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 226
    .line 227
    .line 228
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getHResolution()S

    .line 229
    .line 230
    .line 231
    move-result v2

    .line 232
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 233
    .line 234
    .line 235
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 236
    .line 237
    .line 238
    const-string v2, "    .vresolution    = "

    .line 239
    .line 240
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 241
    .line 242
    .line 243
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getVResolution()S

    .line 244
    .line 245
    .line 246
    move-result v2

    .line 247
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 248
    .line 249
    .line 250
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 251
    .line 252
    .line 253
    const-string v2, "    .headermargin   = "

    .line 254
    .line 255
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 256
    .line 257
    .line 258
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getHeaderMargin()D

    .line 259
    .line 260
    .line 261
    move-result-wide v2

    .line 262
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    .line 263
    .line 264
    .line 265
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 266
    .line 267
    .line 268
    const-string v2, "    .footermargin   = "

    .line 269
    .line 270
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 271
    .line 272
    .line 273
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getFooterMargin()D

    .line 274
    .line 275
    .line 276
    move-result-wide v2

    .line 277
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    .line 278
    .line 279
    .line 280
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 281
    .line 282
    .line 283
    const-string v2, "    .copies         = "

    .line 284
    .line 285
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 286
    .line 287
    .line 288
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->getCopies()S

    .line 289
    .line 290
    .line 291
    move-result v2

    .line 292
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 293
    .line 294
    .line 295
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 296
    .line 297
    .line 298
    const-string v1, "[/PRINTSETUP]\n"

    .line 299
    .line 300
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 301
    .line 302
    .line 303
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 304
    .line 305
    .line 306
    move-result-object v0

    .line 307
    return-object v0
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.class public final Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;
.super Lcom/intsig/office/fc/hssf/formula/function/Fixed1ArgFunction;
.source "CalendarFieldFunction.java"


# static fields
.field public static final DAY:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final HOUR:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final MINUTE:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final MONTH:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final SECOND:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final WEEKDAY:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final YEAR:Lcom/intsig/office/fc/hssf/formula/function/Function;


# instance fields
.field private final _dateFieldId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;-><init>(I)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;->YEAR:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;

    .line 10
    .line 11
    const/4 v1, 0x2

    .line 12
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;-><init>(I)V

    .line 13
    .line 14
    .line 15
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;->MONTH:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 16
    .line 17
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;

    .line 18
    .line 19
    const/4 v1, 0x7

    .line 20
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;-><init>(I)V

    .line 21
    .line 22
    .line 23
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;->WEEKDAY:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 24
    .line 25
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;

    .line 26
    .line 27
    const/4 v1, 0x5

    .line 28
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;-><init>(I)V

    .line 29
    .line 30
    .line 31
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;->DAY:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 32
    .line 33
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;

    .line 34
    .line 35
    const/16 v1, 0xb

    .line 36
    .line 37
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;-><init>(I)V

    .line 38
    .line 39
    .line 40
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;->HOUR:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 41
    .line 42
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;

    .line 43
    .line 44
    const/16 v1, 0xc

    .line 45
    .line 46
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;-><init>(I)V

    .line 47
    .line 48
    .line 49
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;->MINUTE:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 50
    .line 51
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;

    .line 52
    .line 53
    const/16 v1, 0xd

    .line 54
    .line 55
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;-><init>(I)V

    .line 56
    .line 57
    .line 58
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;->SECOND:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private constructor <init>(I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/function/Fixed1ArgFunction;-><init>()V

    .line 2
    .line 3
    .line 4
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;->_dateFieldId:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private getCalField(D)I
    .locals 4

    .line 1
    double-to-int v0, p1

    .line 2
    const/4 v1, 0x2

    .line 3
    const/4 v2, 0x0

    .line 4
    if-nez v0, :cond_3

    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;->_dateFieldId:I

    .line 7
    .line 8
    const/4 v3, 0x1

    .line 9
    if-eq v0, v3, :cond_2

    .line 10
    .line 11
    if-eq v0, v1, :cond_1

    .line 12
    .line 13
    const/4 v3, 0x5

    .line 14
    if-eq v0, v3, :cond_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    return v2

    .line 18
    :cond_1
    return v3

    .line 19
    :cond_2
    const/16 p1, 0x76c

    .line 20
    .line 21
    return p1

    .line 22
    :cond_3
    :goto_0
    invoke-static {p1, p2, v2}, Lcom/intsig/office/ss/util/DateUtil;->getJavaDate(DZ)Ljava/util/Date;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    new-instance p2, Ljava/util/GregorianCalendar;

    .line 27
    .line 28
    invoke-direct {p2}, Ljava/util/GregorianCalendar;-><init>()V

    .line 29
    .line 30
    .line 31
    invoke-virtual {p2, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 32
    .line 33
    .line 34
    iget p1, p0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;->_dateFieldId:I

    .line 35
    .line 36
    invoke-virtual {p2, p1}, Ljava/util/Calendar;->get(I)I

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    iget p2, p0, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;->_dateFieldId:I

    .line 41
    .line 42
    if-ne p2, v1, :cond_4

    .line 43
    .line 44
    add-int/lit8 p1, p1, 0x1

    .line 45
    .line 46
    :cond_4
    return p1
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public final evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 2

    .line 1
    :try_start_0
    invoke-static {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->getSingleValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->coerceValueToDouble(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)D

    .line 6
    .line 7
    .line 8
    move-result-wide p1
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 9
    const-wide/16 v0, 0x0

    .line 10
    .line 11
    cmpg-double p3, p1, v0

    .line 12
    .line 13
    if-gez p3, :cond_0

    .line 14
    .line 15
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->NUM_ERROR:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 16
    .line 17
    return-object p1

    .line 18
    :cond_0
    new-instance p3, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 19
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/CalendarFieldFunction;->getCalField(D)I

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    int-to-double p1, p1

    .line 25
    invoke-direct {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    .line 26
    .line 27
    .line 28
    return-object p3

    .line 29
    :catch_0
    move-exception p1

    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    return-object p1
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

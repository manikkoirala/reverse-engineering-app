.class public final Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;
.super Ljava/lang/Object;
.source "RecordAggregate.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PositionTrackingVisitor"
.end annotation


# instance fields
.field private _position:I

.field private final _rv:Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;->_rv:Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;

    .line 5
    .line 6
    iput p2, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;->_position:I

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public getPosition()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;->_position:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setPosition(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;->_position:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public visitRecord(Lcom/intsig/office/fc/hssf/record/Record;)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;->_position:I

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordBase;->getRecordSize()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    add-int/2addr v0, v1

    .line 8
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;->_position:I

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;->_rv:Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;

    .line 11
    .line 12
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class final Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;
.super Ljava/lang/Object;
.source "LinkTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/model/LinkTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ExternalBookBlock"
.end annotation


# instance fields
.field private final 〇080:Lcom/intsig/office/fc/hssf/record/SupBookRecord;

.field private 〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

.field private final 〇o〇:[Lcom/intsig/office/fc/hssf/model/LinkTable$CRNBlock;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {}, Lcom/intsig/office/fc/hssf/record/SupBookRecord;->createAddInFunctions()Lcom/intsig/office/fc/hssf/record/SupBookRecord;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇080:Lcom/intsig/office/fc/hssf/record/SupBookRecord;

    const/4 v0, 0x0

    new-array v1, v0, [Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

    .line 19
    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

    new-array v0, v0, [Lcom/intsig/office/fc/hssf/model/LinkTable$CRNBlock;

    .line 20
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇o〇:[Lcom/intsig/office/fc/hssf/model/LinkTable$CRNBlock;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    int-to-short p1, p1

    .line 14
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/record/SupBookRecord;->createInternalReferences(S)Lcom/intsig/office/fc/hssf/record/SupBookRecord;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇080:Lcom/intsig/office/fc/hssf/record/SupBookRecord;

    const/4 p1, 0x0

    new-array v0, p1, [Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

    .line 15
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

    new-array p1, p1, [Lcom/intsig/office/fc/hssf/model/LinkTable$CRNBlock;

    .line 16
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇o〇:[Lcom/intsig/office/fc/hssf/model/LinkTable$CRNBlock;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/model/RecordStream;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->getNext()Lcom/intsig/office/fc/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/hssf/record/SupBookRecord;

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇080:Lcom/intsig/office/fc/hssf/record/SupBookRecord;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->peekNextClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

    if-ne v1, v2, :cond_0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->getNext()Lcom/intsig/office/fc/hssf/record/Record;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 6
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

    iput-object v1, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

    .line 7
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 8
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 9
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->peekNextClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/intsig/office/fc/hssf/record/CRNCountRecord;

    if-ne v1, v2, :cond_1

    .line 10
    new-instance v1, Lcom/intsig/office/fc/hssf/model/LinkTable$CRNBlock;

    invoke-direct {v1, p1}, Lcom/intsig/office/fc/hssf/model/LinkTable$CRNBlock;-><init>(Lcom/intsig/office/fc/hssf/model/RecordStream;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 11
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [Lcom/intsig/office/fc/hssf/model/LinkTable$CRNBlock;

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇o〇:[Lcom/intsig/office/fc/hssf/model/LinkTable$CRNBlock;

    .line 12
    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public O8(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

    .line 2
    .line 3
    aget-object p1, v0, p1

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;->getIx()S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public Oo08(I)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

    .line 2
    .line 3
    aget-object p1, v0, p1

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;->getText()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public o〇0()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080(Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;)I
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    add-int/lit8 v1, v1, 0x1

    .line 5
    .line 6
    new-array v2, v1, [Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    array-length v4, v0

    .line 10
    invoke-static {v0, v3, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 11
    .line 12
    .line 13
    add-int/lit8 v1, v1, -0x1

    .line 14
    .line 15
    aput-object p1, v2, v1

    .line 16
    .line 17
    iput-object v2, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

    .line 18
    .line 19
    array-length p1, v2

    .line 20
    add-int/lit8 p1, p1, -0x1

    .line 21
    .line 22
    return p1
    .line 23
    .line 24
.end method

.method public 〇o00〇〇Oo()Lcom/intsig/office/fc/hssf/record/SupBookRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇080:Lcom/intsig/office/fc/hssf/record/SupBookRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇(Ljava/lang/String;)I
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/model/LinkTable$ExternalBookBlock;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

    .line 3
    .line 4
    array-length v2, v1

    .line 5
    if-ge v0, v2, :cond_1

    .line 6
    .line 7
    aget-object v1, v1, v0

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;->getText()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    return v0

    .line 20
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    const/4 p1, -0x1

    .line 24
    return p1
.end method

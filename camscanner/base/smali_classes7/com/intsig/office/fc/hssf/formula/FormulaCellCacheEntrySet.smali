.class final Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;
.super Ljava/lang/Object;
.source "FormulaCellCacheEntrySet.java"


# static fields
.field private static final 〇o〇:[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;


# instance fields
.field private 〇080:I

.field private 〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    new-array v0, v0, [Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 3
    .line 4
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o〇:[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o〇:[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static 〇o00〇〇Oo([Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;)Z
    .locals 5

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    array-length v1, p0

    .line 6
    rem-int/2addr v0, v1

    .line 7
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    move v1, v0

    .line 12
    :goto_0
    array-length v2, p0

    .line 13
    const/4 v3, 0x0

    .line 14
    const/4 v4, 0x1

    .line 15
    if-ge v1, v2, :cond_2

    .line 16
    .line 17
    aget-object v2, p0, v1

    .line 18
    .line 19
    if-ne v2, p1, :cond_0

    .line 20
    .line 21
    return v3

    .line 22
    :cond_0
    if-nez v2, :cond_1

    .line 23
    .line 24
    aput-object p1, p0, v1

    .line 25
    .line 26
    return v4

    .line 27
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_2
    const/4 v1, 0x0

    .line 31
    :goto_1
    if-ge v1, v0, :cond_5

    .line 32
    .line 33
    aget-object v2, p0, v1

    .line 34
    .line 35
    if-ne v2, p1, :cond_3

    .line 36
    .line 37
    return v3

    .line 38
    :cond_3
    if-nez v2, :cond_4

    .line 39
    .line 40
    aput-object p1, p0, v1

    .line 41
    .line 42
    return v4

    .line 43
    :cond_4
    add-int/lit8 v1, v1, 0x1

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_5
    new-instance p0, Ljava/lang/IllegalStateException;

    .line 47
    .line 48
    const-string p1, "No empty space found"

    .line 49
    .line 50
    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    throw p0
.end method


# virtual methods
.method public O8()[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;
    .locals 6

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇080:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ge v0, v1, :cond_0

    .line 5
    .line 6
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o〇:[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 7
    .line 8
    return-object v0

    .line 9
    :cond_0
    new-array v1, v0, [Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    const/4 v3, 0x0

    .line 13
    :goto_0
    iget-object v4, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 14
    .line 15
    array-length v5, v4

    .line 16
    if-ge v2, v5, :cond_2

    .line 17
    .line 18
    aget-object v4, v4, v2

    .line 19
    .line 20
    if-eqz v4, :cond_1

    .line 21
    .line 22
    add-int/lit8 v5, v3, 0x1

    .line 23
    .line 24
    aput-object v4, v1, v3

    .line 25
    .line 26
    move v3, v5

    .line 27
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_2
    if-ne v3, v0, :cond_3

    .line 31
    .line 32
    return-object v1

    .line 33
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 34
    .line 35
    const-string v1, "size mismatch"

    .line 36
    .line 37
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    throw v0
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇080(Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;)V
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇080:I

    .line 2
    .line 3
    mul-int/lit8 v0, v0, 0x3

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 6
    .line 7
    array-length v2, v1

    .line 8
    mul-int/lit8 v2, v2, 0x2

    .line 9
    .line 10
    if-lt v0, v2, :cond_2

    .line 11
    .line 12
    array-length v0, v1

    .line 13
    mul-int/lit8 v0, v0, 0x3

    .line 14
    .line 15
    div-int/lit8 v0, v0, 0x2

    .line 16
    .line 17
    add-int/lit8 v0, v0, 0x4

    .line 18
    .line 19
    new-array v0, v0, [Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 20
    .line 21
    const/4 v2, 0x0

    .line 22
    :goto_0
    array-length v3, v1

    .line 23
    if-ge v2, v3, :cond_1

    .line 24
    .line 25
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 26
    .line 27
    aget-object v3, v3, v2

    .line 28
    .line 29
    if-eqz v3, :cond_0

    .line 30
    .line 31
    invoke-static {v0, v3}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o00〇〇Oo([Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;)Z

    .line 32
    .line 33
    .line 34
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_1
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 38
    .line 39
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 40
    .line 41
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o00〇〇Oo([Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;)Z

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    if-eqz p1, :cond_3

    .line 46
    .line 47
    iget p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇080:I

    .line 48
    .line 49
    add-int/lit8 p1, p1, 0x1

    .line 50
    .line 51
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇080:I

    .line 52
    .line 53
    :cond_3
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇o〇(Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;)Z
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇080:I

    .line 4
    .line 5
    mul-int/lit8 v1, v1, 0x3

    .line 6
    .line 7
    array-length v2, v0

    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x1

    .line 10
    if-ge v1, v2, :cond_3

    .line 11
    .line 12
    array-length v1, v0

    .line 13
    const/16 v2, 0x8

    .line 14
    .line 15
    if-le v1, v2, :cond_3

    .line 16
    .line 17
    array-length v1, v0

    .line 18
    div-int/lit8 v1, v1, 0x2

    .line 19
    .line 20
    new-array v1, v1, [Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    :goto_0
    array-length v5, v0

    .line 24
    if-ge v3, v5, :cond_2

    .line 25
    .line 26
    iget-object v5, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 27
    .line 28
    aget-object v5, v5, v3

    .line 29
    .line 30
    if-eqz v5, :cond_1

    .line 31
    .line 32
    if-ne v5, p1, :cond_0

    .line 33
    .line 34
    iget v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇080:I

    .line 35
    .line 36
    sub-int/2addr v2, v4

    .line 37
    iput v2, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇080:I

    .line 38
    .line 39
    const/4 v2, 0x1

    .line 40
    goto :goto_1

    .line 41
    :cond_0
    invoke-static {v1, v5}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o00〇〇Oo([Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;)Z

    .line 42
    .line 43
    .line 44
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_2
    iput-object v1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇o00〇〇Oo:[Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 48
    .line 49
    return v2

    .line 50
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    array-length v2, v0

    .line 55
    rem-int/2addr v1, v2

    .line 56
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    move v2, v1

    .line 61
    :goto_2
    array-length v5, v0

    .line 62
    const/4 v6, 0x0

    .line 63
    if-ge v2, v5, :cond_5

    .line 64
    .line 65
    aget-object v5, v0, v2

    .line 66
    .line 67
    if-ne v5, p1, :cond_4

    .line 68
    .line 69
    aput-object v6, v0, v2

    .line 70
    .line 71
    iget p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇080:I

    .line 72
    .line 73
    sub-int/2addr p1, v4

    .line 74
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇080:I

    .line 75
    .line 76
    return v4

    .line 77
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 78
    .line 79
    goto :goto_2

    .line 80
    :cond_5
    const/4 v2, 0x0

    .line 81
    :goto_3
    if-ge v2, v1, :cond_7

    .line 82
    .line 83
    aget-object v5, v0, v2

    .line 84
    .line 85
    if-ne v5, p1, :cond_6

    .line 86
    .line 87
    aput-object v6, v0, v2

    .line 88
    .line 89
    iget p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇080:I

    .line 90
    .line 91
    sub-int/2addr p1, v4

    .line 92
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntrySet;->〇080:I

    .line 93
    .line 94
    return v4

    .line 95
    :cond_6
    add-int/lit8 v2, v2, 0x1

    .line 96
    .line 97
    goto :goto_3

    .line 98
    :cond_7
    return v3
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

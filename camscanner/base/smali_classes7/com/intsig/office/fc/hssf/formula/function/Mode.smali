.class public final Lcom/intsig/office/fc/hssf/formula/function/Mode;
.super Ljava/lang/Object;
.source "Mode.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/function/Function;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static collectValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Ljava/util/List;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;",
            "Ljava/util/List<",
            "Ljava/lang/Double;",
            ">;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation

    .line 1
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 2
    .line 3
    if-nez v0, :cond_4

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;->instance:Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;

    .line 6
    .line 7
    if-eq p0, v0, :cond_2

    .line 8
    .line 9
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 10
    .line 11
    if-nez v0, :cond_2

    .line 12
    .line 13
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    instance-of p2, p0, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 19
    .line 20
    if-eqz p2, :cond_1

    .line 21
    .line 22
    new-instance p2, Ljava/lang/Double;

    .line 23
    .line 24
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;->getNumberValue()D

    .line 27
    .line 28
    .line 29
    move-result-wide v0

    .line 30
    invoke-direct {p2, v0, v1}, Ljava/lang/Double;-><init>(D)V

    .line 31
    .line 32
    .line 33
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    return-void

    .line 37
    :cond_1
    new-instance p1, Ljava/lang/RuntimeException;

    .line 38
    .line 39
    new-instance p2, Ljava/lang/StringBuilder;

    .line 40
    .line 41
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .line 43
    .line 44
    const-string v0, "Unexpected value type ("

    .line 45
    .line 46
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 50
    .line 51
    .line 52
    move-result-object p0

    .line 53
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p0

    .line 57
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    const-string p0, ")"

    .line 61
    .line 62
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object p0

    .line 69
    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    throw p1

    .line 73
    :cond_2
    :goto_0
    if-nez p2, :cond_3

    .line 74
    .line 75
    return-void

    .line 76
    :cond_3
    invoke-static {}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->invalidValue()Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    .line 77
    .line 78
    .line 79
    move-result-object p0

    .line 80
    throw p0

    .line 81
    :cond_4
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    .line 82
    .line 83
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 84
    .line 85
    invoke-direct {p1, p0}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;)V

    .line 86
    .line 87
    .line 88
    throw p1
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private static collectValues(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;",
            "Ljava/util/List<",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation

    .line 1
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/TwoDEval;

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/TwoDEval;

    .line 6
    .line 7
    invoke-interface {p0}, Lcom/intsig/office/fc/hssf/formula/TwoDEval;->getWidth()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    invoke-interface {p0}, Lcom/intsig/office/fc/hssf/formula/TwoDEval;->getHeight()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    const/4 v2, 0x0

    .line 16
    const/4 v3, 0x0

    .line 17
    :goto_0
    if-ge v3, v1, :cond_1

    .line 18
    .line 19
    const/4 v4, 0x0

    .line 20
    :goto_1
    if-ge v4, v0, :cond_0

    .line 21
    .line 22
    invoke-interface {p0, v3, v4}, Lcom/intsig/office/fc/hssf/formula/TwoDEval;->getValue(II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 23
    .line 24
    .line 25
    move-result-object v5

    .line 26
    invoke-static {v5, p1, v2}, Lcom/intsig/office/fc/hssf/formula/function/Mode;->collectValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Ljava/util/List;Z)V

    .line 27
    .line 28
    .line 29
    add-int/lit8 v4, v4, 0x1

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    return-void

    .line 36
    :cond_2
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/RefEval;

    .line 37
    .line 38
    const/4 v1, 0x1

    .line 39
    if-eqz v0, :cond_3

    .line 40
    .line 41
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/eval/RefEval;

    .line 42
    .line 43
    invoke-interface {p0}, Lcom/intsig/office/fc/hssf/formula/eval/RefEval;->getInnerValueEval()Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 44
    .line 45
    .line 46
    move-result-object p0

    .line 47
    invoke-static {p0, p1, v1}, Lcom/intsig/office/fc/hssf/formula/function/Mode;->collectValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Ljava/util/List;Z)V

    .line 48
    .line 49
    .line 50
    return-void

    .line 51
    :cond_3
    invoke-static {p0, p1, v1}, Lcom/intsig/office/fc/hssf/formula/function/Mode;->collectValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Ljava/util/List;Z)V

    .line 52
    .line 53
    .line 54
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static evaluate([D)D
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation

    .line 1
    array-length v0, p0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_6

    .line 2
    array-length v0, p0

    new-array v1, v0, [I

    const/4 v2, 0x1

    .line 3
    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    .line 4
    array-length v3, p0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_2

    add-int/lit8 v6, v5, 0x1

    .line 5
    array-length v7, p0

    move v8, v6

    :goto_1
    if-ge v8, v7, :cond_1

    .line 6
    aget-wide v9, p0, v5

    aget-wide v11, p0, v8

    cmpl-double v13, v9, v11

    if-nez v13, :cond_0

    .line 7
    aget v9, v1, v5

    add-int/2addr v9, v2

    aput v9, v1, v5

    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_1
    move v5, v6

    goto :goto_0

    :cond_2
    const-wide/16 v5, 0x0

    const/4 v3, 0x0

    :goto_2
    if-ge v4, v0, :cond_4

    .line 8
    aget v7, v1, v4

    if-le v7, v3, :cond_3

    .line 9
    aget-wide v5, p0, v4

    move v3, v7

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_4
    if-le v3, v2, :cond_5

    return-wide v5

    .line 10
    :cond_5
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    sget-object v0, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->NA:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;)V

    throw p0

    .line 11
    :cond_6
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    sget-object v0, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->NA:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;)V

    throw p0
.end method


# virtual methods
.method public evaluate([Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 3

    .line 12
    :try_start_0
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    const/4 p3, 0x0

    const/4 v0, 0x0

    .line 13
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 14
    aget-object v1, p1, v0

    invoke-static {v1, p2}, Lcom/intsig/office/fc/hssf/formula/function/Mode;->collectValues(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Ljava/util/List;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p1

    new-array v0, p1, [D

    :goto_1
    if-ge p3, p1, :cond_1

    .line 16
    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    aput-wide v1, v0, p3

    add-int/lit8 p3, p3, 0x1

    goto :goto_1

    .line 17
    :cond_1
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/function/Mode;->evaluate([D)D

    move-result-wide p1
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 18
    new-instance p3, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    invoke-direct {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    return-object p3

    :catch_0
    move-exception p1

    .line 19
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object p1

    return-object p1
.end method

.class public final Lcom/intsig/office/fc/hssf/formula/function/Index;
.super Ljava/lang/Object;
.source "Index.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/function/Function2Arg;
.implements Lcom/intsig/office/fc/hssf/formula/function/Function3Arg;
.implements Lcom/intsig/office/fc/hssf/formula/function/Function4Arg;


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static convertFirstArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/TwoDEval;
    .locals 3

    .line 1
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/RefEval;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/eval/RefEval;

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    invoke-interface {p0, v0, v0, v0, v0}, Lcom/intsig/office/fc/hssf/formula/eval/RefEval;->offset(IIII)Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;

    .line 9
    .line 10
    .line 11
    move-result-object p0

    .line 12
    return-object p0

    .line 13
    :cond_0
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/TwoDEval;

    .line 14
    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/TwoDEval;

    .line 18
    .line 19
    return-object p0

    .line 20
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    .line 21
    .line 22
    new-instance v1, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string v2, "Incomplete code - cannot handle first arg of type ("

    .line 28
    .line 29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 33
    .line 34
    .line 35
    move-result-object p0

    .line 36
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p0

    .line 40
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    const-string p0, ")"

    .line 44
    .line 45
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p0

    .line 52
    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    throw v0
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private static getValueFromArea(Lcom/intsig/office/fc/hssf/formula/TwoDEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-interface {p0}, Lcom/intsig/office/fc/hssf/formula/TwoDEval;->getHeight()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-gt p1, v0, :cond_0

    .line 8
    .line 9
    add-int/lit8 p1, p1, -0x1

    .line 10
    .line 11
    invoke-interface {p0, p1}, Lcom/intsig/office/fc/hssf/formula/TwoDEval;->getRow(I)Lcom/intsig/office/fc/hssf/formula/TwoDEval;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    .line 17
    .line 18
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->REF_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 19
    .line 20
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;)V

    .line 21
    .line 22
    .line 23
    throw p0

    .line 24
    :cond_1
    move-object p1, p0

    .line 25
    :goto_0
    if-eqz p2, :cond_3

    .line 26
    .line 27
    invoke-interface {p0}, Lcom/intsig/office/fc/hssf/formula/TwoDEval;->getWidth()I

    .line 28
    .line 29
    .line 30
    move-result p0

    .line 31
    if-gt p2, p0, :cond_2

    .line 32
    .line 33
    add-int/lit8 p2, p2, -0x1

    .line 34
    .line 35
    invoke-interface {p1, p2}, Lcom/intsig/office/fc/hssf/formula/TwoDEval;->getColumn(I)Lcom/intsig/office/fc/hssf/formula/TwoDEval;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    goto :goto_1

    .line 40
    :cond_2
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    .line 41
    .line 42
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->REF_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 43
    .line 44
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;)V

    .line 45
    .line 46
    .line 47
    throw p0

    .line 48
    :cond_3
    :goto_1
    return-object p1
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private static resolveIndexArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->getSingleValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/MissingArgEval;->instance:Lcom/intsig/office/fc/hssf/formula/eval/MissingArgEval;

    .line 6
    .line 7
    const/4 p2, 0x0

    .line 8
    if-ne p0, p1, :cond_0

    .line 9
    .line 10
    return p2

    .line 11
    :cond_0
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;->instance:Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;

    .line 12
    .line 13
    if-ne p0, p1, :cond_1

    .line 14
    .line 15
    return p2

    .line 16
    :cond_1
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->coerceValueToInt(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)I

    .line 17
    .line 18
    .line 19
    move-result p0

    .line 20
    if-ltz p0, :cond_2

    .line 21
    .line 22
    return p0

    .line 23
    :cond_2
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    .line 24
    .line 25
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 26
    .line 27
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;)V

    .line 28
    .line 29
    .line 30
    throw p0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method


# virtual methods
.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 0

    .line 1
    invoke-static {p3}, Lcom/intsig/office/fc/hssf/formula/function/Index;->convertFirstArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/TwoDEval;

    move-result-object p3

    .line 2
    :try_start_0
    invoke-static {p4, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/Index;->resolveIndexArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)I

    move-result p1

    .line 3
    invoke-interface {p3}, Lcom/intsig/office/fc/hssf/formula/TwoDEval;->isColumn()Z

    move-result p2

    const/4 p4, 0x0

    if-nez p2, :cond_1

    .line 4
    invoke-interface {p3}, Lcom/intsig/office/fc/hssf/formula/TwoDEval;->isRow()Z

    move-result p2

    if-nez p2, :cond_0

    .line 5
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->REF_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    return-object p1

    :cond_0
    move p4, p1

    const/4 p1, 0x0

    .line 6
    :cond_1
    invoke-static {p3, p1, p4}, Lcom/intsig/office/fc/hssf/formula/function/Index;->getValueFromArea(Lcom/intsig/office/fc/hssf/formula/TwoDEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object p1

    return-object p1
.end method

.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 0

    .line 8
    invoke-static {p3}, Lcom/intsig/office/fc/hssf/formula/function/Index;->convertFirstArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/TwoDEval;

    move-result-object p3

    .line 9
    :try_start_0
    invoke-static {p5, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/Index;->resolveIndexArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)I

    move-result p5

    .line 10
    invoke-static {p4, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/Index;->resolveIndexArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)I

    move-result p1

    .line 11
    invoke-static {p3, p1, p5}, Lcom/intsig/office/fc/hssf/formula/function/Index;->getValueFromArea(Lcom/intsig/office/fc/hssf/formula/TwoDEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object p1

    return-object p1
.end method

.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 0

    .line 13
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Incomplete code - don\'t know how to support the \'area_num\' parameter yet)"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public evaluate([Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 8

    .line 14
    array-length v0, p1

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eq v0, v3, :cond_2

    const/4 v4, 0x3

    if-eq v0, v4, :cond_1

    const/4 v5, 0x4

    if-eq v0, v5, :cond_0

    .line 15
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    return-object p1

    .line 16
    :cond_0
    aget-object v5, p1, v2

    aget-object v6, p1, v1

    aget-object v7, p1, v3

    aget-object p1, p1, v4

    move-object v0, p0

    move v1, p2

    move v2, p3

    move-object v3, v5

    move-object v4, v6

    move-object v5, v7

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/fc/hssf/formula/function/Index;->evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    return-object p1

    .line 17
    :cond_1
    aget-object v4, p1, v2

    aget-object v5, p1, v1

    aget-object p1, p1, v3

    move-object v0, p0

    move v1, p2

    move v2, p3

    move-object v3, v4

    move-object v4, v5

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/fc/hssf/formula/function/Index;->evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    return-object p1

    .line 18
    :cond_2
    aget-object v0, p1, v2

    aget-object p1, p1, v1

    invoke-virtual {p0, p2, p3, v0, p1}, Lcom/intsig/office/fc/hssf/formula/function/Index;->evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    return-object p1
.end method

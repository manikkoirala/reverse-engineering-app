.class public Lcom/intsig/office/fc/hssf/model/PictureShape;
.super Lcom/intsig/office/fc/hssf/model/AbstractShape;
.source "PictureShape.java"


# instance fields
.field private objRecord:Lcom/intsig/office/fc/hssf/record/ObjRecord;

.field private spContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/model/AbstractShape;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hssf/model/PictureShape;->createSpContainer(Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;I)Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/model/PictureShape;->spContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 9
    .line 10
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hssf/model/PictureShape;->createObjRecord(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;I)Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/model/PictureShape;->objRecord:Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private createObjRecord(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;I)Lcom/intsig/office/fc/hssf/record/ObjRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/ObjRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;

    .line 7
    .line 8
    invoke-direct {v1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;-><init>()V

    .line 9
    .line 10
    .line 11
    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getShapeType()I

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    int-to-short p1, p1

    .line 18
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setObjectType(S)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hssf/model/AbstractShape;->getCmoObjectId(I)I

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setObjectId(I)V

    .line 26
    .line 27
    .line 28
    const/4 p1, 0x1

    .line 29
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setLocked(Z)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setPrintable(Z)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setAutofill(Z)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setAutoline(Z)V

    .line 39
    .line 40
    .line 41
    const/4 p1, 0x0

    .line 42
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hssf/record/CommonObjectDataSubRecord;->setReserved2(I)V

    .line 43
    .line 44
    .line 45
    new-instance p1, Lcom/intsig/office/fc/hssf/record/EndSubRecord;

    .line 46
    .line 47
    invoke-direct {p1}, Lcom/intsig/office/fc/hssf/record/EndSubRecord;-><init>()V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->addSubRecord(Lcom/intsig/office/fc/hssf/record/SubRecord;)Z

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/ObjRecord;->addSubRecord(Lcom/intsig/office/fc/hssf/record/SubRecord;)Z

    .line 54
    .line 55
    .line 56
    return-object v0
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private createSpContainer(Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;I)Lcom/intsig/office/fc/ddf/EscherContainerRecord;
    .locals 8

    .line 1
    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;

    .line 2
    .line 3
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 4
    .line 5
    invoke-direct {v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;-><init>()V

    .line 6
    .line 7
    .line 8
    new-instance v1, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 9
    .line 10
    invoke-direct {v1}, Lcom/intsig/office/fc/ddf/EscherSpRecord;-><init>()V

    .line 11
    .line 12
    .line 13
    new-instance v2, Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 14
    .line 15
    invoke-direct {v2}, Lcom/intsig/office/fc/ddf/EscherOptRecord;-><init>()V

    .line 16
    .line 17
    .line 18
    new-instance v3, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;

    .line 19
    .line 20
    invoke-direct {v3}, Lcom/intsig/office/fc/ddf/EscherClientDataRecord;-><init>()V

    .line 21
    .line 22
    .line 23
    const/16 v4, -0xffc

    .line 24
    .line 25
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 26
    .line 27
    .line 28
    const/16 v4, 0xf

    .line 29
    .line 30
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 31
    .line 32
    .line 33
    const/16 v4, -0xff6

    .line 34
    .line 35
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 36
    .line 37
    .line 38
    const/16 v4, 0x4b2

    .line 39
    .line 40
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1, p2}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setShapeId(I)V

    .line 44
    .line 45
    .line 46
    const/16 p2, 0xa00

    .line 47
    .line 48
    invoke-virtual {v1, p2}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setFlags(I)V

    .line 49
    .line 50
    .line 51
    const/16 p2, -0xff5

    .line 52
    .line 53
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 54
    .line 55
    .line 56
    new-instance p2, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 57
    .line 58
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->getPictureIndex()I

    .line 59
    .line 60
    .line 61
    move-result v4

    .line 62
    const/16 v5, 0x104

    .line 63
    .line 64
    const/4 v6, 0x0

    .line 65
    const/4 v7, 0x1

    .line 66
    invoke-direct {p2, v5, v6, v7, v4}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SZZI)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {p0, p1, v2}, Lcom/intsig/office/fc/hssf/model/AbstractShape;->addStandardOptions(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/ddf/EscherOptRecord;)I

    .line 73
    .line 74
    .line 75
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getAnchor()Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->isHorizontallyFlipped()Z

    .line 80
    .line 81
    .line 82
    move-result p2

    .line 83
    if-eqz p2, :cond_0

    .line 84
    .line 85
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->getFlags()I

    .line 86
    .line 87
    .line 88
    move-result p2

    .line 89
    or-int/lit8 p2, p2, 0x40

    .line 90
    .line 91
    invoke-virtual {v1, p2}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setFlags(I)V

    .line 92
    .line 93
    .line 94
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->isVerticallyFlipped()Z

    .line 95
    .line 96
    .line 97
    move-result p2

    .line 98
    if-eqz p2, :cond_1

    .line 99
    .line 100
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->getFlags()I

    .line 101
    .line 102
    .line 103
    move-result p2

    .line 104
    or-int/lit16 p2, p2, 0x80

    .line 105
    .line 106
    invoke-virtual {v1, p2}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setFlags(I)V

    .line 107
    .line 108
    .line 109
    :cond_1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/model/AbstractShape;->createAnchor(Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    const/16 p2, -0xfef

    .line 114
    .line 115
    invoke-virtual {v3, p2}, Lcom/intsig/office/fc/ddf/EscherRecord;->setRecordId(S)V

    .line 116
    .line 117
    .line 118
    invoke-virtual {v3, v6}, Lcom/intsig/office/fc/ddf/EscherRecord;->setOptions(S)V

    .line 119
    .line 120
    .line 121
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 122
    .line 123
    .line 124
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 125
    .line 126
    .line 127
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 128
    .line 129
    .line 130
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->addChildRecord(Lcom/intsig/office/fc/ddf/EscherRecord;)V

    .line 131
    .line 132
    .line 133
    return-object v0
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method


# virtual methods
.method public getObjRecord()Lcom/intsig/office/fc/hssf/record/ObjRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/PictureShape;->objRecord:Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/model/PictureShape;->spContainer:Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

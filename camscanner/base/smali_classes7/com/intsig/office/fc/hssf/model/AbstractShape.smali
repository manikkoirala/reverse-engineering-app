.class public abstract Lcom/intsig/office/fc/hssf/model/AbstractShape;
.super Ljava/lang/Object;
.source "AbstractShape.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static createShape(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;I)Lcom/intsig/office/fc/hssf/model/AbstractShape;
    .locals 4

    .line 1
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    new-instance v0, Lcom/intsig/office/fc/hssf/model/CommentShape;

    .line 7
    .line 8
    move-object v2, p0

    .line 9
    check-cast v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;

    .line 10
    .line 11
    invoke-direct {v0, v2, p1}, Lcom/intsig/office/fc/hssf/model/CommentShape;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;I)V

    .line 12
    .line 13
    .line 14
    goto :goto_1

    .line 15
    :cond_0
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;

    .line 16
    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    new-instance v0, Lcom/intsig/office/fc/hssf/model/TextboxShape;

    .line 20
    .line 21
    move-object v2, p0

    .line 22
    check-cast v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;

    .line 23
    .line 24
    invoke-direct {v0, v2, p1}, Lcom/intsig/office/fc/hssf/model/TextboxShape;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;I)V

    .line 25
    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_1
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPolygon;

    .line 29
    .line 30
    if-eqz v0, :cond_2

    .line 31
    .line 32
    new-instance v0, Lcom/intsig/office/fc/hssf/model/PolygonShape;

    .line 33
    .line 34
    move-object v2, p0

    .line 35
    check-cast v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFPolygon;

    .line 36
    .line 37
    invoke-direct {v0, v2, p1}, Lcom/intsig/office/fc/hssf/model/PolygonShape;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFPolygon;I)V

    .line 38
    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_2
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;

    .line 42
    .line 43
    if-eqz v0, :cond_8

    .line 44
    .line 45
    move-object v0, p0

    .line 46
    check-cast v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;

    .line 47
    .line 48
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getShapeType()I

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    const/4 v3, 0x1

    .line 53
    if-eq v2, v3, :cond_6

    .line 54
    .line 55
    if-eq v2, v1, :cond_5

    .line 56
    .line 57
    const/4 v3, 0x3

    .line 58
    if-eq v2, v3, :cond_5

    .line 59
    .line 60
    const/16 v3, 0x8

    .line 61
    .line 62
    if-eq v2, v3, :cond_4

    .line 63
    .line 64
    const/16 v3, 0x14

    .line 65
    .line 66
    if-ne v2, v3, :cond_3

    .line 67
    .line 68
    new-instance v2, Lcom/intsig/office/fc/hssf/model/ComboboxShape;

    .line 69
    .line 70
    invoke-direct {v2, v0, p1}, Lcom/intsig/office/fc/hssf/model/ComboboxShape;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;I)V

    .line 71
    .line 72
    .line 73
    goto :goto_0

    .line 74
    :cond_3
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 75
    .line 76
    const-string p1, "Do not know how to handle this type of shape"

    .line 77
    .line 78
    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    throw p0

    .line 82
    :cond_4
    new-instance v2, Lcom/intsig/office/fc/hssf/model/PictureShape;

    .line 83
    .line 84
    invoke-direct {v2, v0, p1}, Lcom/intsig/office/fc/hssf/model/PictureShape;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;I)V

    .line 85
    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_5
    new-instance v2, Lcom/intsig/office/fc/hssf/model/SimpleFilledShape;

    .line 89
    .line 90
    invoke-direct {v2, v0, p1}, Lcom/intsig/office/fc/hssf/model/SimpleFilledShape;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;I)V

    .line 91
    .line 92
    .line 93
    goto :goto_0

    .line 94
    :cond_6
    new-instance v2, Lcom/intsig/office/fc/hssf/model/LineShape;

    .line 95
    .line 96
    invoke-direct {v2, v0, p1}, Lcom/intsig/office/fc/hssf/model/LineShape;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;I)V

    .line 97
    .line 98
    .line 99
    :goto_0
    move-object v0, v2

    .line 100
    :goto_1
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/AbstractShape;->getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    const/16 v2, -0xff6

    .line 105
    .line 106
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 107
    .line 108
    .line 109
    move-result-object p1

    .line 110
    check-cast p1, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 111
    .line 112
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getParent()Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;

    .line 113
    .line 114
    .line 115
    move-result-object p0

    .line 116
    if-eqz p0, :cond_7

    .line 117
    .line 118
    invoke-virtual {p1}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->getFlags()I

    .line 119
    .line 120
    .line 121
    move-result p0

    .line 122
    or-int/2addr p0, v1

    .line 123
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/ddf/EscherSpRecord;->setFlags(I)V

    .line 124
    .line 125
    .line 126
    :cond_7
    return-object v0

    .line 127
    :cond_8
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 128
    .line 129
    const-string p1, "Unknown shape type"

    .line 130
    .line 131
    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    throw p0
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method


# virtual methods
.method protected addStandardOptions(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/ddf/EscherOptRecord;)I
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherBoolProperty;

    .line 2
    .line 3
    const/16 v1, 0xbf

    .line 4
    .line 5
    const/high16 v2, 0x80000

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherBoolProperty;-><init>(SI)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoFill()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/16 v1, 0x1bf

    .line 18
    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherBoolProperty;

    .line 22
    .line 23
    const/high16 v3, 0x110000

    .line 24
    .line 25
    invoke-direct {v0, v1, v3}, Lcom/intsig/office/fc/ddf/EscherBoolProperty;-><init>(SI)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherBoolProperty;

    .line 33
    .line 34
    const/high16 v3, 0x10000

    .line 35
    .line 36
    invoke-direct {v0, v1, v3}, Lcom/intsig/office/fc/ddf/EscherBoolProperty;-><init>(SI)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 40
    .line 41
    .line 42
    :goto_0
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherRGBProperty;

    .line 43
    .line 44
    const/16 v1, 0x181

    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getFillColor()I

    .line 47
    .line 48
    .line 49
    move-result v3

    .line 50
    invoke-direct {v0, v1, v3}, Lcom/intsig/office/fc/ddf/EscherRGBProperty;-><init>(SI)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 54
    .line 55
    .line 56
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherBoolProperty;

    .line 57
    .line 58
    const/16 v1, 0x3bf

    .line 59
    .line 60
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/ddf/EscherBoolProperty;-><init>(SI)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 64
    .line 65
    .line 66
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherRGBProperty;

    .line 67
    .line 68
    const/16 v1, 0x1c0

    .line 69
    .line 70
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getLineStyleColor()I

    .line 71
    .line 72
    .line 73
    move-result v3

    .line 74
    invoke-direct {v0, v1, v3}, Lcom/intsig/office/fc/ddf/EscherRGBProperty;-><init>(SI)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getLineWidth()I

    .line 81
    .line 82
    .line 83
    move-result v0

    .line 84
    const/16 v1, 0x2535

    .line 85
    .line 86
    if-eq v0, v1, :cond_1

    .line 87
    .line 88
    new-instance v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 89
    .line 90
    const/16 v1, 0x1cb

    .line 91
    .line 92
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getLineWidth()I

    .line 93
    .line 94
    .line 95
    move-result v3

    .line 96
    invoke-direct {v0, v1, v3}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 97
    .line 98
    .line 99
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 100
    .line 101
    .line 102
    const/4 v0, 0x6

    .line 103
    goto :goto_1

    .line 104
    :cond_1
    const/4 v0, 0x5

    .line 105
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getLineStyle()I

    .line 106
    .line 107
    .line 108
    move-result v1

    .line 109
    if-eqz v1, :cond_3

    .line 110
    .line 111
    new-instance v1, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 112
    .line 113
    const/16 v3, 0x1ce

    .line 114
    .line 115
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getLineStyle()I

    .line 116
    .line 117
    .line 118
    move-result v4

    .line 119
    invoke-direct {v1, v3, v4}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {p2, v1}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 123
    .line 124
    .line 125
    new-instance v1, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 126
    .line 127
    const/16 v3, 0x1d7

    .line 128
    .line 129
    const/4 v4, 0x0

    .line 130
    invoke-direct {v1, v3, v4}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 131
    .line 132
    .line 133
    invoke-virtual {p2, v1}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 134
    .line 135
    .line 136
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getLineStyle()I

    .line 137
    .line 138
    .line 139
    move-result p1

    .line 140
    const/4 v1, -0x1

    .line 141
    const/16 v3, 0x1ff

    .line 142
    .line 143
    if-ne p1, v1, :cond_2

    .line 144
    .line 145
    new-instance p1, Lcom/intsig/office/fc/ddf/EscherBoolProperty;

    .line 146
    .line 147
    invoke-direct {p1, v3, v2}, Lcom/intsig/office/fc/ddf/EscherBoolProperty;-><init>(SI)V

    .line 148
    .line 149
    .line 150
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 151
    .line 152
    .line 153
    goto :goto_2

    .line 154
    :cond_2
    new-instance p1, Lcom/intsig/office/fc/ddf/EscherBoolProperty;

    .line 155
    .line 156
    const v1, 0x80008

    .line 157
    .line 158
    .line 159
    invoke-direct {p1, v3, v1}, Lcom/intsig/office/fc/ddf/EscherBoolProperty;-><init>(SI)V

    .line 160
    .line 161
    .line 162
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->addEscherProperty(Lcom/intsig/office/fc/ddf/EscherProperty;)V

    .line 163
    .line 164
    .line 165
    :goto_2
    add-int/lit8 v0, v0, 0x3

    .line 166
    .line 167
    :cond_3
    invoke-virtual {p2}, Lcom/intsig/office/fc/ddf/AbstractEscherOptRecord;->sortProperties()V

    .line 168
    .line 169
    .line 170
    return v0
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method protected createAnchor(Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)Lcom/intsig/office/fc/ddf/EscherRecord;
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/model/ConvertAnchor;->createAnchor(Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method getCmoObjectId(I)I
    .locals 0

    .line 1
    add-int/lit16 p1, p1, -0x400

    .line 2
    .line 3
    return p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public abstract getObjRecord()Lcom/intsig/office/fc/hssf/record/ObjRecord;
.end method

.method public abstract getSpContainer()Lcom/intsig/office/fc/ddf/EscherContainerRecord;
.end method

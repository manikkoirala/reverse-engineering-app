.class final Lcom/intsig/office/fc/hssf/formula/function/NumericFunction$Log;
.super Lcom/intsig/office/fc/hssf/formula/function/Var1or2ArgFunction;
.source "NumericFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Log"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/function/Var1or2ArgFunction;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 2

    .line 1
    :try_start_0
    invoke-static {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    move-result-wide p1

    .line 2
    invoke-static {p1, p2}, Ljava/lang/Math;->log(D)D

    move-result-wide p1

    sget-wide v0, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->LOG_10_TO_BASE_e:D

    div-double/2addr p1, v0

    .line 3
    invoke-static {p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->checkValue(D)V
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4
    new-instance p3, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    invoke-direct {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    return-object p3

    :catch_0
    move-exception p1

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object p1

    return-object p1
.end method

.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 3

    .line 6
    :try_start_0
    invoke-static {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    move-result-wide v0

    .line 7
    invoke-static {p4, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    move-result-wide p1

    .line 8
    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide p3

    const-wide v0, 0x4005bf0a8b145769L    # Math.E

    cmpl-double v2, p1, v0

    if-nez v2, :cond_0

    goto :goto_0

    .line 9
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Math;->log(D)D

    move-result-wide p1

    div-double/2addr p3, p1

    .line 10
    :goto_0
    invoke-static {p3, p4}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->checkValue(D)V
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    invoke-direct {p1, p3, p4}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    return-object p1

    :catch_0
    move-exception p1

    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object p1

    return-object p1
.end method

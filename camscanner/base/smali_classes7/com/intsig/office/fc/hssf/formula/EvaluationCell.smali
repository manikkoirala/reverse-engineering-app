.class public interface abstract Lcom/intsig/office/fc/hssf/formula/EvaluationCell;
.super Ljava/lang/Object;
.source "EvaluationCell.java"


# virtual methods
.method public abstract getBooleanCellValue()Z
.end method

.method public abstract getCellType()I
.end method

.method public abstract getColumnIndex()I
.end method

.method public abstract getErrorCellValue()I
.end method

.method public abstract getIdentityKey()Ljava/lang/Object;
.end method

.method public abstract getNumericCellValue()D
.end method

.method public abstract getRowIndex()I
.end method

.method public abstract getSheet()Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;
.end method

.method public abstract getStringCellValue()Ljava/lang/String;
.end method

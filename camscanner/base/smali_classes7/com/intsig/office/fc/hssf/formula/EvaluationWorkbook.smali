.class public interface abstract Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;
.super Ljava/lang/Object;
.source "EvaluationWorkbook.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalName;,
        Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;
    }
.end annotation


# virtual methods
.method public abstract convertFromExternSheetIndex(I)I
.end method

.method public abstract getExternalName(II)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalName;
.end method

.method public abstract getExternalSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;
.end method

.method public abstract getFormulaTokens(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
.end method

.method public abstract getName(Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;)Lcom/intsig/office/fc/hssf/formula/EvaluationName;
.end method

.method public abstract getName(Ljava/lang/String;I)Lcom/intsig/office/fc/hssf/formula/EvaluationName;
.end method

.method public abstract getSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;
.end method

.method public abstract getSheetIndex(Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;)I
.end method

.method public abstract getSheetIndex(Ljava/lang/String;)I
.end method

.method public abstract getSheetName(I)Ljava/lang/String;
.end method

.method public abstract getUDFFinder()Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;
.end method

.method public abstract resolveNameXText(Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;)Ljava/lang/String;
.end method

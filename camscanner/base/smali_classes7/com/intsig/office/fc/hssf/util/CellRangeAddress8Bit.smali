.class public final Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;
.super Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;
.source "CellRangeAddress8Bit.java"


# static fields
.field public static final ENCODED_SIZE:I = 0x6


# direct methods
.method public constructor <init>(IIII)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;-><init>(IIII)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V
    .locals 3

    .line 2
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;->readUShortAndCheck(Lcom/intsig/office/fc/util/LittleEndianInput;)I

    move-result v0

    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    move-result v1

    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUByte()I

    move-result v2

    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUByte()I

    move-result p1

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;-><init>(IIII)V

    return-void
.end method

.method public static getEncodedSize(I)I
    .locals 0

    .line 1
    mul-int/lit8 p0, p0, 0x6

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static readUShortAndCheck(Lcom/intsig/office/fc/util/LittleEndianInput;)I
    .locals 2

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/util/LittleEndianInput;->available()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x6

    .line 6
    if-lt v0, v1, :cond_0

    .line 7
    .line 8
    invoke-interface {p0}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    .line 9
    .line 10
    .line 11
    move-result p0

    .line 12
    return p0

    .line 13
    :cond_0
    new-instance p0, Ljava/lang/RuntimeException;

    .line 14
    .line 15
    const-string v0, "Ran out of data reading CellRangeAddress"

    .line 16
    .line 17
    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    throw p0
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public copy()Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    .line 16
    .line 17
    .line 18
    move-result v4

    .line 19
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;-><init>(IIII)V

    .line 20
    .line 21
    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public serialize(I[B)I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;

    const/4 v1, 0x6

    invoke-direct {v0, p2, p1, v1}, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;-><init>([BII)V

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/util/CellRangeAddress8Bit;->serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    return v1
.end method

.method public serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 1

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeByte(I)V

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeByte(I)V

    return-void
.end method

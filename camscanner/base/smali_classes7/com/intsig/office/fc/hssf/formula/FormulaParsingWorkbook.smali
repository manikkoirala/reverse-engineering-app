.class public interface abstract Lcom/intsig/office/fc/hssf/formula/FormulaParsingWorkbook;
.super Ljava/lang/Object;
.source "FormulaParsingWorkbook.java"


# virtual methods
.method public abstract getExternalSheetIndex(Ljava/lang/String;)I
.end method

.method public abstract getExternalSheetIndex(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract getName(Ljava/lang/String;I)Lcom/intsig/office/fc/hssf/formula/EvaluationName;
.end method

.method public abstract getNameXPtg(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;
.end method

.method public abstract getSpreadsheetVersion()Lcom/intsig/office/fc/ss/SpreadsheetVersion;
.end method

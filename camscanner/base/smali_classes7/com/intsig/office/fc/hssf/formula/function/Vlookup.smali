.class public final Lcom/intsig/office/fc/hssf/formula/function/Vlookup;
.super Lcom/intsig/office/fc/hssf/formula/function/Var3or4ArgFunction;
.source "Vlookup.java"


# static fields
.field private static final DEFAULT_ARG3:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;->TRUE:Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 2
    .line 3
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/Vlookup;->DEFAULT_ARG3:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/function/Var3or4ArgFunction;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private createResultColumnVector(Lcom/intsig/office/fc/hssf/formula/TwoDEval;I)Lcom/intsig/office/fc/hssf/formula/function/LookupUtils$ValueVector;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/TwoDEval;->getWidth()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-ge p2, v0, :cond_0

    .line 6
    .line 7
    invoke-static {p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/LookupUtils;->〇080(Lcom/intsig/office/fc/hssf/formula/TwoDEval;I)Lcom/intsig/office/fc/hssf/formula/function/LookupUtils$ValueVector;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    return-object p1

    .line 12
    :cond_0
    invoke-static {}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->invalidRef()Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    throw p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 7

    .line 1
    sget-object v6, Lcom/intsig/office/fc/hssf/formula/function/Vlookup;->DEFAULT_ARG3:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/fc/hssf/formula/function/Vlookup;->evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    return-object p1
.end method

.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 1

    .line 2
    :try_start_0
    invoke-static {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->getSingleValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p3

    .line 3
    invoke-static {p4}, Lcom/intsig/office/fc/hssf/formula/function/LookupUtils;->Oooo8o0〇(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/TwoDEval;

    move-result-object p4

    .line 4
    invoke-static {p6, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/LookupUtils;->〇O8o08O(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Z

    move-result p6

    const/4 v0, 0x0

    .line 5
    invoke-static {p4, v0}, Lcom/intsig/office/fc/hssf/formula/function/LookupUtils;->〇080(Lcom/intsig/office/fc/hssf/formula/TwoDEval;I)Lcom/intsig/office/fc/hssf/formula/function/LookupUtils$ValueVector;

    move-result-object v0

    invoke-static {p1, p2, p3, v0, p6}, Lcom/intsig/office/fc/hssf/formula/function/LookupUtils;->〇80〇808〇O(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/function/LookupUtils$ValueVector;Z)I

    move-result p3

    .line 6
    invoke-static {p5, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/LookupUtils;->OO0o〇〇(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)I

    move-result p1

    .line 7
    invoke-direct {p0, p4, p1}, Lcom/intsig/office/fc/hssf/formula/function/Vlookup;->createResultColumnVector(Lcom/intsig/office/fc/hssf/formula/TwoDEval;I)Lcom/intsig/office/fc/hssf/formula/function/LookupUtils$ValueVector;

    move-result-object p1

    .line 8
    invoke-interface {p1, p3}, Lcom/intsig/office/fc/hssf/formula/function/LookupUtils$ValueVector;->getItem(I)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object p1

    return-object p1
.end method

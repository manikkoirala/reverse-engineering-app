.class public final Lcom/intsig/office/fc/hssf/record/FeatRecord;
.super Lcom/intsig/office/fc/hssf/record/StandardRecord;
.source "FeatRecord.java"


# static fields
.field public static final sid:S = 0x868s


# instance fields
.field private cbFeatData:J

.field private cellRefs:[Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

.field private futureHeader:Lcom/intsig/office/fc/hssf/record/common/FtrHeader;

.field private isf_sharedFeatureType:I

.field private reserved1:B

.field private reserved2:J

.field private reserved3:I

.field private sharedFeature:Lcom/intsig/office/fc/hssf/record/common/SharedFeature;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    .line 2
    new-instance v0, Lcom/intsig/office/fc/hssf/record/common/FtrHeader;

    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/common/FtrHeader;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->futureHeader:Lcom/intsig/office/fc/hssf/record/common/FtrHeader;

    const/16 v1, 0x868

    .line 3
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/common/FtrHeader;->setRecordType(S)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V
    .locals 3

    .line 4
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    .line 5
    new-instance v0, Lcom/intsig/office/fc/hssf/record/common/FtrHeader;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/record/common/FtrHeader;-><init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->futureHeader:Lcom/intsig/office/fc/hssf/record/common/FtrHeader;

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->isf_sharedFeatureType:I

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->reserved1:B

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readInt()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->reserved2:J

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUShort()I

    move-result v0

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readInt()I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->cbFeatData:J

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v1

    iput v1, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->reserved3:I

    .line 12
    new-array v0, v0, [Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->cellRefs:[Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    const/4 v0, 0x0

    .line 13
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->cellRefs:[Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 14
    new-instance v2, Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    invoke-direct {v2, p1}, Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;-><init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15
    :cond_0
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->isf_sharedFeatureType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    .line 16
    sget-object p1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown Shared Feature "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->isf_sharedFeatureType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " found!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 17
    :cond_1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/common/FeatSmartTag;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/record/common/FeatSmartTag;-><init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->sharedFeature:Lcom/intsig/office/fc/hssf/record/common/SharedFeature;

    goto :goto_1

    .line 18
    :cond_2
    new-instance v0, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;-><init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->sharedFeature:Lcom/intsig/office/fc/hssf/record/common/SharedFeature;

    goto :goto_1

    .line 19
    :cond_3
    new-instance v0, Lcom/intsig/office/fc/hssf/record/common/FeatProtection;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/record/common/FeatProtection;-><init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->sharedFeature:Lcom/intsig/office/fc/hssf/record/common/SharedFeature;

    :goto_1
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/Record;->cloneViaReserialise()Lcom/intsig/office/fc/hssf/record/Record;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCbFeatData()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->cbFeatData:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCellRefs()[Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->cellRefs:[Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getDataSize()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->cellRefs:[Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    mul-int/lit8 v0, v0, 0x8

    .line 5
    .line 6
    add-int/lit8 v0, v0, 0x1b

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->sharedFeature:Lcom/intsig/office/fc/hssf/record/common/SharedFeature;

    .line 9
    .line 10
    invoke-interface {v1}, Lcom/intsig/office/fc/hssf/record/common/SharedFeature;->getDataSize()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    add-int/2addr v0, v1

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getIsf_sharedFeatureType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->isf_sharedFeatureType:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSharedFeature()Lcom/intsig/office/fc/hssf/record/common/SharedFeature;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->sharedFeature:Lcom/intsig/office/fc/hssf/record/common/SharedFeature;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSid()S
    .locals 1

    .line 1
    const/16 v0, 0x868

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->futureHeader:Lcom/intsig/office/fc/hssf/record/common/FtrHeader;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/common/FtrHeader;->serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 4
    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->isf_sharedFeatureType:I

    .line 7
    .line 8
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 9
    .line 10
    .line 11
    iget-byte v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->reserved1:B

    .line 12
    .line 13
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeByte(I)V

    .line 14
    .line 15
    .line 16
    iget-wide v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->reserved2:J

    .line 17
    .line 18
    long-to-int v1, v0

    .line 19
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeInt(I)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->cellRefs:[Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 23
    .line 24
    array-length v0, v0

    .line 25
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 26
    .line 27
    .line 28
    iget-wide v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->cbFeatData:J

    .line 29
    .line 30
    long-to-int v1, v0

    .line 31
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeInt(I)V

    .line 32
    .line 33
    .line 34
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->reserved3:I

    .line 35
    .line 36
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 37
    .line 38
    .line 39
    const/4 v0, 0x0

    .line 40
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->cellRefs:[Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 41
    .line 42
    array-length v2, v1

    .line 43
    if-ge v0, v2, :cond_0

    .line 44
    .line 45
    aget-object v1, v1, v0

    .line 46
    .line 47
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;->serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 48
    .line 49
    .line 50
    add-int/lit8 v0, v0, 0x1

    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->sharedFeature:Lcom/intsig/office/fc/hssf/record/common/SharedFeature;

    .line 54
    .line 55
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/hssf/record/common/SharedFeature;->serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setCbFeatData(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->cbFeatData:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCellRefs([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->cellRefs:[Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSharedFeature(Lcom/intsig/office/fc/hssf/record/common/SharedFeature;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->sharedFeature:Lcom/intsig/office/fc/hssf/record/common/SharedFeature;

    .line 2
    .line 3
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/record/common/FeatProtection;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x2

    .line 8
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->isf_sharedFeatureType:I

    .line 9
    .line 10
    :cond_0
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/record/common/FeatFormulaErr2;

    .line 11
    .line 12
    const/4 v1, 0x3

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    iput v1, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->isf_sharedFeatureType:I

    .line 16
    .line 17
    :cond_1
    instance-of v0, p1, Lcom/intsig/office/fc/hssf/record/common/FeatSmartTag;

    .line 18
    .line 19
    if-eqz v0, :cond_2

    .line 20
    .line 21
    const/4 v0, 0x4

    .line 22
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->isf_sharedFeatureType:I

    .line 23
    .line 24
    :cond_2
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->isf_sharedFeatureType:I

    .line 25
    .line 26
    if-ne v0, v1, :cond_3

    .line 27
    .line 28
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/record/common/SharedFeature;->getDataSize()I

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    int-to-long v0, p1

    .line 33
    iput-wide v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->cbFeatData:J

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_3
    const-wide/16 v0, 0x0

    .line 37
    .line 38
    iput-wide v0, p0, Lcom/intsig/office/fc/hssf/record/FeatRecord;->cbFeatData:J

    .line 39
    .line 40
    :goto_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "[SHARED FEATURE]\n"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 9
    .line 10
    .line 11
    const-string v1, "[/SHARED FEATURE]\n"

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    return-object v0
    .line 21
.end method

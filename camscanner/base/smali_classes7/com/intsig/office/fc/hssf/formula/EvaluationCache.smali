.class final Lcom/intsig/office/fc/hssf/formula/EvaluationCache;
.super Ljava/lang/Object;
.source "EvaluationCache.java"


# instance fields
.field private final 〇080:Lcom/intsig/office/fc/hssf/formula/PlainCellCache;

.field private final 〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/hssf/formula/IEvaluationListener;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/PlainCellCache;

    .line 5
    .line 6
    invoke-direct {p1}, Lcom/intsig/office/fc/hssf/formula/PlainCellCache;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇080:Lcom/intsig/office/fc/hssf/formula/PlainCellCache;

    .line 10
    .line 11
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;

    .line 12
    .line 13
    invoke-direct {p1}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private 〇080(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Z
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    if-eq v1, v2, :cond_1

    .line 14
    .line 15
    return v0

    .line 16
    :cond_1
    sget-object v2, Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;->instance:Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;

    .line 17
    .line 18
    const/4 v3, 0x1

    .line 19
    if-ne p1, v2, :cond_3

    .line 20
    .line 21
    if-ne p2, p1, :cond_2

    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    :cond_2
    return v0

    .line 25
    :cond_3
    const-class v2, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 26
    .line 27
    if-ne v1, v2, :cond_5

    .line 28
    .line 29
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 30
    .line 31
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;->getNumberValue()D

    .line 32
    .line 33
    .line 34
    move-result-wide v1

    .line 35
    check-cast p2, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 36
    .line 37
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;->getNumberValue()D

    .line 38
    .line 39
    .line 40
    move-result-wide p1

    .line 41
    cmpl-double v4, v1, p1

    .line 42
    .line 43
    if-nez v4, :cond_4

    .line 44
    .line 45
    const/4 v0, 0x1

    .line 46
    :cond_4
    return v0

    .line 47
    :cond_5
    const-class v2, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;

    .line 48
    .line 49
    if-ne v1, v2, :cond_6

    .line 50
    .line 51
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;

    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;->getStringValue()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    check-cast p2, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;

    .line 58
    .line 59
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;->getStringValue()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object p2

    .line 63
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    move-result p1

    .line 67
    return p1

    .line 68
    :cond_6
    const-class v2, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 69
    .line 70
    if-ne v1, v2, :cond_8

    .line 71
    .line 72
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 73
    .line 74
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;->getBooleanValue()Z

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    check-cast p2, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 79
    .line 80
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;->getBooleanValue()Z

    .line 81
    .line 82
    .line 83
    move-result p2

    .line 84
    if-ne p1, p2, :cond_7

    .line 85
    .line 86
    const/4 v0, 0x1

    .line 87
    :cond_7
    return v0

    .line 88
    :cond_8
    const-class v2, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 89
    .line 90
    if-ne v1, v2, :cond_a

    .line 91
    .line 92
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 93
    .line 94
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->getErrorCode()I

    .line 95
    .line 96
    .line 97
    move-result p1

    .line 98
    check-cast p2, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 99
    .line 100
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->getErrorCode()I

    .line 101
    .line 102
    .line 103
    move-result p2

    .line 104
    if-ne p1, p2, :cond_9

    .line 105
    .line 106
    const/4 v0, 0x1

    .line 107
    :cond_9
    return v0

    .line 108
    :cond_a
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 109
    .line 110
    new-instance p2, Ljava/lang/StringBuilder;

    .line 111
    .line 112
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 113
    .line 114
    .line 115
    const-string v0, "Unexpected value class ("

    .line 116
    .line 117
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    const-string v0, ")"

    .line 128
    .line 129
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 133
    .line 134
    .line 135
    move-result-object p2

    .line 136
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    throw p1
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private 〇〇888(IIII)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BookSheetKey;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BookSheetKey;-><init>(II)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;

    .line 7
    .line 8
    new-instance p2, Lcom/intsig/office/fc/hssf/formula/EvaluationCache$1;

    .line 9
    .line 10
    invoke-direct {p2, p0, v0, p3, p4}, Lcom/intsig/office/fc/hssf/formula/EvaluationCache$1;-><init>(Lcom/intsig/office/fc/hssf/formula/EvaluationCache;Lcom/intsig/office/fc/hssf/formula/FormulaUsedBlankCellSet$BookSheetKey;II)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;->〇080(Lcom/intsig/office/fc/hssf/formula/FormulaCellCache$IEntryOperation;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method


# virtual methods
.method public O8(IIIILcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/PlainValueCellCacheEntry;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/PlainCellCache$Loc;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2, p3, p4}, Lcom/intsig/office/fc/hssf/formula/PlainCellCache$Loc;-><init>(IIII)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇080:Lcom/intsig/office/fc/hssf/formula/PlainCellCache;

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/formula/PlainCellCache;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/PlainCellCache$Loc;)Lcom/intsig/office/fc/hssf/formula/PlainValueCellCacheEntry;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    if-nez p1, :cond_0

    .line 13
    .line 14
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/PlainValueCellCacheEntry;

    .line 15
    .line 16
    invoke-direct {p1, p5}, Lcom/intsig/office/fc/hssf/formula/PlainValueCellCacheEntry;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)V

    .line 17
    .line 18
    .line 19
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇080:Lcom/intsig/office/fc/hssf/formula/PlainCellCache;

    .line 20
    .line 21
    invoke-virtual {p2, v0, p1}, Lcom/intsig/office/fc/hssf/formula/PlainCellCache;->〇o〇(Lcom/intsig/office/fc/hssf/formula/PlainCellCache$Loc;Lcom/intsig/office/fc/hssf/formula/PlainValueCellCacheEntry;)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->getValue()Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 26
    .line 27
    .line 28
    move-result-object p2

    .line 29
    invoke-direct {p0, p2, p5}, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇080(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Z

    .line 30
    .line 31
    .line 32
    move-result p2

    .line 33
    if-eqz p2, :cond_1

    .line 34
    .line 35
    :goto_0
    return-object p1

    .line 36
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 37
    .line 38
    const-string p2, "value changed"

    .line 39
    .line 40
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    throw p1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method public Oo08(IILcom/intsig/office/fc/hssf/formula/EvaluationCell;)V
    .locals 3

    .line 1
    invoke-interface {p3}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getCellType()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x2

    .line 6
    const/4 v2, 0x0

    .line 7
    if-ne v0, v1, :cond_1

    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;

    .line 10
    .line 11
    invoke-virtual {p1, p3}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;->Oo08(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    if-nez p1, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;->Oooo8o0〇([Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇〇888(Lcom/intsig/office/fc/hssf/formula/IEvaluationListener;)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/PlainCellCache$Loc;

    .line 26
    .line 27
    invoke-interface {p3}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getRowIndex()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    invoke-interface {p3}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getColumnIndex()I

    .line 32
    .line 33
    .line 34
    move-result p3

    .line 35
    invoke-direct {v0, p1, p2, v1, p3}, Lcom/intsig/office/fc/hssf/formula/PlainCellCache$Loc;-><init>(IIII)V

    .line 36
    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇080:Lcom/intsig/office/fc/hssf/formula/PlainCellCache;

    .line 39
    .line 40
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/formula/PlainCellCache;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/PlainCellCache$Loc;)Lcom/intsig/office/fc/hssf/formula/PlainValueCellCacheEntry;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    if-nez p1, :cond_2

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_2
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇〇888(Lcom/intsig/office/fc/hssf/formula/IEvaluationListener;)V

    .line 48
    .line 49
    .line 50
    :goto_0
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public o〇0(IILcom/intsig/office/fc/hssf/formula/EvaluationCell;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;

    .line 2
    .line 3
    invoke-virtual {v0, p3}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;->〇o〇(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {p3}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getRowIndex()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-interface {p3}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getColumnIndex()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    new-instance v3, Lcom/intsig/office/fc/hssf/formula/PlainCellCache$Loc;

    .line 16
    .line 17
    invoke-direct {v3, p1, p2, v1, v2}, Lcom/intsig/office/fc/hssf/formula/PlainCellCache$Loc;-><init>(IIII)V

    .line 18
    .line 19
    .line 20
    iget-object v4, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇080:Lcom/intsig/office/fc/hssf/formula/PlainCellCache;

    .line 21
    .line 22
    invoke-virtual {v4, v3}, Lcom/intsig/office/fc/hssf/formula/PlainCellCache;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/PlainCellCache$Loc;)Lcom/intsig/office/fc/hssf/formula/PlainValueCellCacheEntry;

    .line 23
    .line 24
    .line 25
    move-result-object v4

    .line 26
    invoke-interface {p3}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getCellType()I

    .line 27
    .line 28
    .line 29
    move-result v5

    .line 30
    const/4 v6, 0x2

    .line 31
    const/4 v7, 0x0

    .line 32
    if-ne v5, v6, :cond_3

    .line 33
    .line 34
    if-nez v0, :cond_1

    .line 35
    .line 36
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 37
    .line 38
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;-><init>()V

    .line 39
    .line 40
    .line 41
    if-nez v4, :cond_0

    .line 42
    .line 43
    invoke-direct {p0, p1, p2, v1, v2}, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇〇888(IIII)V

    .line 44
    .line 45
    .line 46
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;

    .line 47
    .line 48
    invoke-virtual {p1, p3, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;->O8(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;)V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_1
    invoke-virtual {v0, v7}, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇〇888(Lcom/intsig/office/fc/hssf/formula/IEvaluationListener;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;->〇8o8o〇()V

    .line 56
    .line 57
    .line 58
    :goto_0
    if-nez v4, :cond_2

    .line 59
    .line 60
    goto :goto_2

    .line 61
    :cond_2
    invoke-virtual {v4, v7}, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇〇888(Lcom/intsig/office/fc/hssf/formula/IEvaluationListener;)V

    .line 62
    .line 63
    .line 64
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇080:Lcom/intsig/office/fc/hssf/formula/PlainCellCache;

    .line 65
    .line 66
    invoke-virtual {p1, v3}, Lcom/intsig/office/fc/hssf/formula/PlainCellCache;->O8(Lcom/intsig/office/fc/hssf/formula/PlainCellCache$Loc;)V

    .line 67
    .line 68
    .line 69
    goto :goto_2

    .line 70
    :cond_3
    invoke-static {p3}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->getValueFromNonFormulaCell(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 71
    .line 72
    .line 73
    move-result-object v5

    .line 74
    if-nez v4, :cond_5

    .line 75
    .line 76
    sget-object v4, Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;->instance:Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;

    .line 77
    .line 78
    if-eq v5, v4, :cond_7

    .line 79
    .line 80
    new-instance v4, Lcom/intsig/office/fc/hssf/formula/PlainValueCellCacheEntry;

    .line 81
    .line 82
    invoke-direct {v4, v5}, Lcom/intsig/office/fc/hssf/formula/PlainValueCellCacheEntry;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)V

    .line 83
    .line 84
    .line 85
    if-nez v0, :cond_4

    .line 86
    .line 87
    invoke-direct {p0, p1, p2, v1, v2}, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇〇888(IIII)V

    .line 88
    .line 89
    .line 90
    :cond_4
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇080:Lcom/intsig/office/fc/hssf/formula/PlainCellCache;

    .line 91
    .line 92
    invoke-virtual {p1, v3, v4}, Lcom/intsig/office/fc/hssf/formula/PlainCellCache;->〇o〇(Lcom/intsig/office/fc/hssf/formula/PlainCellCache$Loc;Lcom/intsig/office/fc/hssf/formula/PlainValueCellCacheEntry;)V

    .line 93
    .line 94
    .line 95
    goto :goto_1

    .line 96
    :cond_5
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇80〇808〇O(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Z

    .line 97
    .line 98
    .line 99
    move-result p1

    .line 100
    if-eqz p1, :cond_6

    .line 101
    .line 102
    invoke-virtual {v4, v7}, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇〇888(Lcom/intsig/office/fc/hssf/formula/IEvaluationListener;)V

    .line 103
    .line 104
    .line 105
    :cond_6
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;->instance:Lcom/intsig/office/fc/hssf/formula/eval/BlankEval;

    .line 106
    .line 107
    if-ne v5, p1, :cond_7

    .line 108
    .line 109
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇080:Lcom/intsig/office/fc/hssf/formula/PlainCellCache;

    .line 110
    .line 111
    invoke-virtual {p1, v3}, Lcom/intsig/office/fc/hssf/formula/PlainCellCache;->O8(Lcom/intsig/office/fc/hssf/formula/PlainCellCache$Loc;)V

    .line 112
    .line 113
    .line 114
    :cond_7
    :goto_1
    if-nez v0, :cond_8

    .line 115
    .line 116
    goto :goto_2

    .line 117
    :cond_8
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;

    .line 118
    .line 119
    invoke-virtual {p1, p3}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;->Oo08(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 120
    .line 121
    .line 122
    invoke-virtual {v0, v7}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;->Oooo8o0〇([Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;)V

    .line 123
    .line 124
    .line 125
    invoke-virtual {v0, v7}, Lcom/intsig/office/fc/hssf/formula/CellCacheEntry;->〇〇888(Lcom/intsig/office/fc/hssf/formula/IEvaluationListener;)V

    .line 126
    .line 127
    .line 128
    :goto_2
    return-void
    .line 129
.end method

.method public 〇o00〇〇Oo()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇080:Lcom/intsig/office/fc/hssf/formula/PlainCellCache;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/PlainCellCache;->〇080()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;->〇o00〇〇Oo()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;->〇o〇(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;-><init>()V

    .line 12
    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/formula/EvaluationCache;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;

    .line 15
    .line 16
    invoke-virtual {v1, p1, v0}, Lcom/intsig/office/fc/hssf/formula/FormulaCellCache;->O8(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;Lcom/intsig/office/fc/hssf/formula/FormulaCellCacheEntry;)V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-object v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

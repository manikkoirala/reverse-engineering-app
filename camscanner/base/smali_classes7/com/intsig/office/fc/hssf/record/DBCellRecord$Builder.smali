.class public final Lcom/intsig/office/fc/hssf/record/DBCellRecord$Builder;
.super Ljava/lang/Object;
.source "DBCellRecord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/record/DBCellRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private _cellOffsets:[S

.field private _nCellOffsets:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x4

    .line 5
    new-array v0, v0, [S

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/DBCellRecord$Builder;->_cellOffsets:[S

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public addCellOffset(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/DBCellRecord$Builder;->_cellOffsets:[S

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    iget v2, p0, Lcom/intsig/office/fc/hssf/record/DBCellRecord$Builder;->_nCellOffsets:I

    .line 5
    .line 6
    if-gt v1, v2, :cond_0

    .line 7
    .line 8
    mul-int/lit8 v1, v2, 0x2

    .line 9
    .line 10
    new-array v1, v1, [S

    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 14
    .line 15
    .line 16
    iput-object v1, p0, Lcom/intsig/office/fc/hssf/record/DBCellRecord$Builder;->_cellOffsets:[S

    .line 17
    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/DBCellRecord$Builder;->_cellOffsets:[S

    .line 19
    .line 20
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/DBCellRecord$Builder;->_nCellOffsets:I

    .line 21
    .line 22
    int-to-short p1, p1

    .line 23
    aput-short p1, v0, v1

    .line 24
    .line 25
    add-int/lit8 v1, v1, 0x1

    .line 26
    .line 27
    iput v1, p0, Lcom/intsig/office/fc/hssf/record/DBCellRecord$Builder;->_nCellOffsets:I

    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public build(I)Lcom/intsig/office/fc/hssf/record/DBCellRecord;
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/DBCellRecord$Builder;->_nCellOffsets:I

    .line 2
    .line 3
    new-array v1, v0, [S

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/record/DBCellRecord$Builder;->_cellOffsets:[S

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Lcom/intsig/office/fc/hssf/record/DBCellRecord;

    .line 12
    .line 13
    invoke-direct {v0, p1, v1}, Lcom/intsig/office/fc/hssf/record/DBCellRecord;-><init>(I[S)V

    .line 14
    .line 15
    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

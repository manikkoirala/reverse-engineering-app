.class public final Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;
.super Ljava/lang/Object;
.source "HSSFConditionalFormatting.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/ConditionalFormatting;


# instance fields
.field private final _workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

.field private final cfAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_1

    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;->_workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    .line 9
    .line 10
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;->cfAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 14
    .line 15
    const-string p2, "cfAggregate must not be null"

    .line 16
    .line 17
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    throw p1

    .line 21
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 22
    .line 23
    const-string p2, "workbook must not be null"

    .line 24
    .line 25
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    throw p1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public addRule(Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;->cfAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->getCfRuleRecord()Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;->addRule(Lcom/intsig/office/fc/hssf/record/CFRuleRecord;)V

    return-void
.end method

.method public addRule(Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;)V
    .locals 0

    .line 2
    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;->addRule(Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;)V

    return-void
.end method

.method getCFRecordsAggregate()Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;->cfAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFormattingRanges()[Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;->cfAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;->getHeader()Lcom/intsig/office/fc/hssf/record/CFHeaderRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/CFHeaderRecord;->getCellRanges()[Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFormattingRegions()[Lcom/intsig/office/fc/ss/util/Region;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;->getFormattingRanges()[Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/ss/util/Region;->convertCellRangesToRegions([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;)[Lcom/intsig/office/fc/ss/util/Region;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNumberOfRules()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;->cfAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;->getNumberOfRules()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRule(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;->cfAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;->getRule(I)Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    move-result-object p1

    .line 3
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;

    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;->_workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;Lcom/intsig/office/fc/hssf/record/CFRuleRecord;)V

    return-object v0
.end method

.method public bridge synthetic getRule(I)Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;->getRule(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;

    move-result-object p1

    return-object p1
.end method

.method public setRule(ILcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;->cfAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;

    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;->getCfRuleRecord()Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;->setRule(ILcom/intsig/office/fc/hssf/record/CFRuleRecord;)V

    return-void
.end method

.method public setRule(ILcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;)V
    .locals 0

    .line 2
    check-cast p2, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;->setRule(ILcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormattingRule;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFConditionalFormatting;->cfAggregate:Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/CFRecordsAggregate;->toString()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

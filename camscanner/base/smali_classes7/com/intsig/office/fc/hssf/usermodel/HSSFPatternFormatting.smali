.class public Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;
.super Ljava/lang/Object;
.source "HSSFPatternFormatting.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/PatternFormatting;


# instance fields
.field private final cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

.field private final patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;


# direct methods
.method protected constructor <init>(Lcom/intsig/office/fc/hssf/record/CFRuleRecord;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->getPatternFormatting()Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;->patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public getFillBackgroundColor()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;->patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;->getFillBackgroundColor()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFillForegroundColor()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;->patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;->getFillForegroundColor()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFillPattern()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;->patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;->getFillPattern()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getPatternFormattingBlock()Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;->patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setFillBackgroundColor(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;->patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;->setFillBackgroundColor(I)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setPatternBackgroundColorModified(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFillForegroundColor(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;->patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;->setFillForegroundColor(I)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setPatternColorModified(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFillPattern(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;->patternFormatting:Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/cf/PatternFormatting;->setFillPattern(I)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatternFormatting;->cfRuleRecord:Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;->setPatternStyleModified(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public abstract Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation;
.super Lcom/intsig/office/fc/hssf/formula/function/Fixed2ArgFunction;
.source "TwoOperandNumericOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation$SubtractEvalClass;
    }
.end annotation


# static fields
.field public static final AddEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final DivideEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final MultiplyEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final PowerEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

.field public static final SubtractEval:Lcom/intsig/office/fc/hssf/formula/function/Function;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation;->AddEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation$2;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation$2;-><init>()V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation;->DivideEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 14
    .line 15
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation$3;

    .line 16
    .line 17
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation$3;-><init>()V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation;->MultiplyEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 21
    .line 22
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation$4;

    .line 23
    .line 24
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation$4;-><init>()V

    .line 25
    .line 26
    .line 27
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation;->PowerEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 28
    .line 29
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation$SubtractEvalClass;

    .line 30
    .line 31
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation$SubtractEvalClass;-><init>()V

    .line 32
    .line 33
    .line 34
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation;->SubtractEval:Lcom/intsig/office/fc/hssf/formula/function/Function;

    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/function/Fixed2ArgFunction;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method protected abstract evaluate(DD)D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation
.end method

.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 2

    .line 1
    :try_start_0
    invoke-virtual {p0, p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-virtual {p0, p4, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation;->singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D

    .line 6
    .line 7
    .line 8
    move-result-wide p1

    .line 9
    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation;->evaluate(DD)D

    .line 10
    .line 11
    .line 12
    move-result-wide p1

    .line 13
    const-wide/16 p3, 0x0

    .line 14
    .line 15
    cmpl-double v0, p1, p3

    .line 16
    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    instance-of p3, p0, Lcom/intsig/office/fc/hssf/formula/eval/TwoOperandNumericOperation$SubtractEvalClass;

    .line 20
    .line 21
    if-nez p3, :cond_0

    .line 22
    .line 23
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;->ZERO:Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 24
    .line 25
    return-object p1

    .line 26
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    .line 27
    .line 28
    .line 29
    move-result p3

    .line 30
    if-nez p3, :cond_2

    .line 31
    .line 32
    invoke-static {p1, p2}, Ljava/lang/Double;->isInfinite(D)Z

    .line 33
    .line 34
    .line 35
    move-result p3
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    if-eqz p3, :cond_1

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    new-instance p3, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 40
    .line 41
    invoke-direct {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    .line 42
    .line 43
    .line 44
    return-object p3

    .line 45
    :cond_2
    :goto_0
    :try_start_1
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->NUM_ERROR:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;
    :try_end_1
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_1 .. :try_end_1} :catch_0

    .line 46
    .line 47
    return-object p1

    .line 48
    :catch_0
    move-exception p1

    .line 49
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    return-object p1
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method protected final singleOperandEvaluate(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)D
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;
        }
    .end annotation

    .line 1
    invoke-static {p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->getSingleValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->coerceValueToDouble(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)D

    .line 6
    .line 7
    .line 8
    move-result-wide p1

    .line 9
    return-wide p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

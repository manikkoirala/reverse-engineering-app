.class public final Lcom/intsig/office/fc/hssf/formula/function/Lookup;
.super Lcom/intsig/office/fc/hssf/formula/function/Var2or3ArgFunction;
.source "Lookup.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/function/Var2or3ArgFunction;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static createVector(Lcom/intsig/office/fc/hssf/formula/TwoDEval;)Lcom/intsig/office/fc/hssf/formula/function/LookupUtils$ValueVector;
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/function/LookupUtils;->Oo08(Lcom/intsig/office/fc/hssf/formula/TwoDEval;)Lcom/intsig/office/fc/hssf/formula/function/LookupUtils$ValueVector;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    if-eqz p0, :cond_0

    .line 6
    .line 7
    return-object p0

    .line 8
    :cond_0
    new-instance p0, Ljava/lang/RuntimeException;

    .line 9
    .line 10
    const-string v0, "non-vector lookup or result areas not supported yet"

    .line 11
    .line 12
    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    throw p0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 0

    .line 1
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Two arg version of LOOKUP not supported yet"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 0

    .line 2
    :try_start_0
    invoke-static {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->getSingleValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    .line 3
    invoke-static {p4}, Lcom/intsig/office/fc/hssf/formula/function/LookupUtils;->Oooo8o0〇(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/TwoDEval;

    move-result-object p2

    .line 4
    invoke-static {p5}, Lcom/intsig/office/fc/hssf/formula/function/LookupUtils;->Oooo8o0〇(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/TwoDEval;

    move-result-object p3

    .line 5
    invoke-static {p2}, Lcom/intsig/office/fc/hssf/formula/function/Lookup;->createVector(Lcom/intsig/office/fc/hssf/formula/TwoDEval;)Lcom/intsig/office/fc/hssf/formula/function/LookupUtils$ValueVector;

    move-result-object p2

    .line 6
    invoke-static {p3}, Lcom/intsig/office/fc/hssf/formula/function/Lookup;->createVector(Lcom/intsig/office/fc/hssf/formula/TwoDEval;)Lcom/intsig/office/fc/hssf/formula/function/LookupUtils$ValueVector;

    move-result-object p3

    .line 7
    invoke-interface {p2}, Lcom/intsig/office/fc/hssf/formula/function/LookupUtils$ValueVector;->getSize()I

    move-result p4

    invoke-interface {p3}, Lcom/intsig/office/fc/hssf/formula/function/LookupUtils$ValueVector;->getSize()I

    move-result p5

    if-gt p4, p5, :cond_1

    const/4 p4, 0x1

    .line 8
    invoke-static {p1, p2, p4}, Lcom/intsig/office/fc/hssf/formula/function/LookupUtils;->OO0o〇〇〇〇0(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/function/LookupUtils$ValueVector;Z)I

    move-result p1

    if-ltz p1, :cond_0

    .line 9
    invoke-interface {p3, p1}, Lcom/intsig/office/fc/hssf/formula/function/LookupUtils$ValueVector;->getItem(I)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1

    .line 10
    :cond_1
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Lookup vector and result vector of differing sizes not supported yet"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object p1

    return-object p1
.end method

.class public final Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;
.super Ljava/lang/Object;
.source "ForkedEvaluator.java"


# instance fields
.field private _evaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

.field private _sewb:Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;


# direct methods
.method private constructor <init>(Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;Lcom/intsig/office/fc/hssf/formula/IStabilityClassifier;Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;

    .line 5
    .line 6
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;-><init>(Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;->_sewb:Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;

    .line 10
    .line 11
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 12
    .line 13
    invoke-direct {p1, v0, p2, p3}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;-><init>(Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;Lcom/intsig/office/fc/hssf/formula/IStabilityClassifier;Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;)V

    .line 14
    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;->_evaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public static create(Lcom/intsig/office/fc/ss/usermodel/Workbook;Lcom/intsig/office/fc/hssf/formula/IStabilityClassifier;)Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    .line 1
    invoke-static {p0, p1, v0}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;->create(Lcom/intsig/office/fc/ss/usermodel/Workbook;Lcom/intsig/office/fc/hssf/formula/IStabilityClassifier;Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;)Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;

    move-result-object p0

    return-object p0
.end method

.method public static create(Lcom/intsig/office/fc/ss/usermodel/Workbook;Lcom/intsig/office/fc/hssf/formula/IStabilityClassifier;Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;)Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;
    .locals 1

    .line 2
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;

    invoke-static {p0}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;->createEvaluationWorkbook(Lcom/intsig/office/fc/ss/usermodel/Workbook;)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    move-result-object p0

    invoke-direct {v0, p0, p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;-><init>(Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;Lcom/intsig/office/fc/hssf/formula/IStabilityClassifier;Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;)V

    return-object v0
.end method

.method private static createEvaluationWorkbook(Lcom/intsig/office/fc/ss/usermodel/Workbook;)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;
    .locals 3

    .line 1
    instance-of v0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 6
    .line 7
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;->create(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)Lcom/intsig/office/fc/hssf/usermodel/HSSFEvaluationWorkbook;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    return-object p0

    .line 12
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 13
    .line 14
    new-instance v1, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string v2, "Unexpected workbook type ("

    .line 20
    .line 21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 25
    .line 26
    .line 27
    move-result-object p0

    .line 28
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object p0

    .line 32
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    const-string p0, ")"

    .line 36
    .line 37
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object p0

    .line 44
    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    throw v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static setupEnvironment([Ljava/lang/String;[Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;)V
    .locals 4

    .line 1
    array-length v0, p1

    .line 2
    new-array v1, v0, [Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 3
    .line 4
    const/4 v2, 0x0

    .line 5
    :goto_0
    if-ge v2, v0, :cond_0

    .line 6
    .line 7
    aget-object v3, p1, v2

    .line 8
    .line 9
    iget-object v3, v3, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;->_evaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 10
    .line 11
    aput-object v3, v1, v2

    .line 12
    .line 13
    add-int/lit8 v2, v2, 0x1

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-static {p0, v1}, Lcom/intsig/office/fc/hssf/formula/CollaboratingWorkbooksEnvironment;->setup([Ljava/lang/String;[Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public copyUpdatedCells(Lcom/intsig/office/fc/ss/usermodel/Workbook;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;->_sewb:Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇080(Lcom/intsig/office/fc/ss/usermodel/Workbook;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public evaluate(Ljava/lang/String;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;->_sewb:Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇o00〇〇Oo(Ljava/lang/String;II)Lcom/intsig/office/fc/hssf/formula/EvaluationCell;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getCellType()I

    .line 8
    .line 9
    .line 10
    move-result p2

    .line 11
    if-eqz p2, :cond_5

    .line 12
    .line 13
    const/4 p3, 0x1

    .line 14
    if-eq p2, p3, :cond_4

    .line 15
    .line 16
    const/4 p3, 0x2

    .line 17
    if-eq p2, p3, :cond_3

    .line 18
    .line 19
    const/4 p3, 0x3

    .line 20
    if-eq p2, p3, :cond_2

    .line 21
    .line 22
    const/4 p3, 0x4

    .line 23
    if-eq p2, p3, :cond_1

    .line 24
    .line 25
    const/4 p3, 0x5

    .line 26
    if-ne p2, p3, :cond_0

    .line 27
    .line 28
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getErrorCellValue()I

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->valueOf(I)Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    return-object p1

    .line 37
    :cond_0
    new-instance p2, Ljava/lang/IllegalStateException;

    .line 38
    .line 39
    new-instance p3, Ljava/lang/StringBuilder;

    .line 40
    .line 41
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .line 43
    .line 44
    const-string v0, "Bad cell type ("

    .line 45
    .line 46
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getCellType()I

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string p1, ")"

    .line 57
    .line 58
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    throw p2

    .line 69
    :cond_1
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getBooleanCellValue()Z

    .line 70
    .line 71
    .line 72
    move-result p1

    .line 73
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;->valueOf(Z)Lcom/intsig/office/fc/hssf/formula/eval/BoolEval;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    return-object p1

    .line 78
    :cond_2
    const/4 p1, 0x0

    .line 79
    return-object p1

    .line 80
    :cond_3
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;->_evaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 81
    .line 82
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->evaluate(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 83
    .line 84
    .line 85
    move-result-object p1

    .line 86
    return-object p1

    .line 87
    :cond_4
    new-instance p2, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;

    .line 88
    .line 89
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getStringCellValue()Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    invoke-direct {p2, p1}, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;-><init>(Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    return-object p2

    .line 97
    :cond_5
    new-instance p2, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 98
    .line 99
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getNumericCellValue()D

    .line 100
    .line 101
    .line 102
    move-result-wide v0

    .line 103
    invoke-direct {p2, v0, v1}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    .line 104
    .line 105
    .line 106
    return-object p2
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public updateCell(Ljava/lang/String;IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;->_sewb:Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationWorkbook;->〇o〇(Ljava/lang/String;II)Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p1, p4}, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluationCell;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)V

    .line 8
    .line 9
    .line 10
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/formula/eval/ForkedEvaluator;->_evaluator:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 11
    .line 12
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->notifyUpdateCell(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

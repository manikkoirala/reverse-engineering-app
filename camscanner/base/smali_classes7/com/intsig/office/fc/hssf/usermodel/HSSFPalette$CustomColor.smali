.class final Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;
.super Lcom/intsig/office/fc/hssf/util/HSSFColor;
.source "HSSFPalette.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CustomColor"
.end annotation


# instance fields
.field private O8:B

.field private 〇080:S

.field private 〇o00〇〇Oo:B

.field private 〇o〇:B


# direct methods
.method private constructor <init>(SBBB)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/util/HSSFColor;-><init>()V

    .line 3
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;->〇080:S

    .line 4
    iput-byte p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;->〇o00〇〇Oo:B

    .line 5
    iput-byte p3, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;->〇o〇:B

    .line 6
    iput-byte p4, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;->O8:B

    return-void
.end method

.method public constructor <init>(S[B)V
    .locals 3

    const/4 v0, 0x0

    .line 1
    aget-byte v0, p2, v0

    const/4 v1, 0x1

    aget-byte v1, p2, v1

    const/4 v2, 0x2

    aget-byte p2, p2, v2

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;-><init>(SBBB)V

    return-void
.end method

.method private 〇080(B)Ljava/lang/String;
    .locals 3

    .line 1
    const-string v0, "0"

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    goto :goto_1

    .line 6
    :cond_0
    and-int/lit16 p1, p1, 0xff

    .line 7
    .line 8
    shl-int/lit8 v1, p1, 0x8

    .line 9
    .line 10
    or-int/2addr p1, v1

    .line 11
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    const/4 v2, 0x4

    .line 24
    if-ge v1, v2, :cond_1

    .line 25
    .line 26
    new-instance v1, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    goto :goto_0

    .line 42
    :cond_1
    move-object v0, p1

    .line 43
    :goto_1
    return-object v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-byte v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;->〇o00〇〇Oo:B

    .line 7
    .line 8
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;->〇080(B)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 13
    .line 14
    .line 15
    const/16 v1, 0x3a

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 18
    .line 19
    .line 20
    iget-byte v2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;->〇o〇:B

    .line 21
    .line 22
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;->〇080(B)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 30
    .line 31
    .line 32
    iget-byte v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;->O8:B

    .line 33
    .line 34
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;->〇080(B)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getIndex()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;->〇080:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTriplet()[S
    .locals 3

    .line 1
    const/4 v0, 0x3

    .line 2
    new-array v0, v0, [S

    .line 3
    .line 4
    iget-byte v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;->〇o00〇〇Oo:B

    .line 5
    .line 6
    and-int/lit16 v1, v1, 0xff

    .line 7
    .line 8
    int-to-short v1, v1

    .line 9
    const/4 v2, 0x0

    .line 10
    aput-short v1, v0, v2

    .line 11
    .line 12
    iget-byte v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;->〇o〇:B

    .line 13
    .line 14
    and-int/lit16 v1, v1, 0xff

    .line 15
    .line 16
    int-to-short v1, v1

    .line 17
    const/4 v2, 0x1

    .line 18
    aput-short v1, v0, v2

    .line 19
    .line 20
    iget-byte v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPalette$CustomColor;->O8:B

    .line 21
    .line 22
    and-int/lit16 v1, v1, 0xff

    .line 23
    .line 24
    int-to-short v1, v1

    .line 25
    const/4 v2, 0x2

    .line 26
    aput-short v1, v0, v2

    .line 27
    .line 28
    return-object v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.class public final Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;
.super Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;
.source "DataValidityTable.java"


# instance fields
.field private final _headerRec:Lcom/intsig/office/fc/hssf/record/DVALRecord;

.field private final _validationList:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 7
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;-><init>()V

    .line 8
    new-instance v0, Lcom/intsig/office/fc/hssf/record/DVALRecord;

    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/DVALRecord;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;->_headerRec:Lcom/intsig/office/fc/hssf/record/DVALRecord;

    .line 9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;->_validationList:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/model/RecordStream;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate;-><init>()V

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->getNext()Lcom/intsig/office/fc/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/hssf/record/DVALRecord;

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;->_headerRec:Lcom/intsig/office/fc/hssf/record/DVALRecord;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->peekNextClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/intsig/office/fc/hssf/record/DVRecord;

    if-ne v1, v2, :cond_0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/RecordStream;->getNext()Lcom/intsig/office/fc/hssf/record/Record;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 6
    :cond_0
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;->_validationList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addDataValidation(Lcom/intsig/office/fc/hssf/record/DVRecord;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;->_validationList:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;->_headerRec:Lcom/intsig/office/fc/hssf/record/DVALRecord;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;->_validationList:Ljava/util/List;

    .line 9
    .line 10
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/DVALRecord;->setDVRecNo(I)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public visitContainedRecords(Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;->_validationList:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;->_headerRec:Lcom/intsig/office/fc/hssf/record/DVALRecord;

    .line 11
    .line 12
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 13
    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;->_validationList:Ljava/util/List;

    .line 17
    .line 18
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-ge v0, v1, :cond_1

    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;->_validationList:Ljava/util/List;

    .line 25
    .line 26
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    check-cast v1, Lcom/intsig/office/fc/hssf/record/Record;

    .line 31
    .line 32
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lcom/intsig/office/fc/hssf/record/Record;)V

    .line 33
    .line 34
    .line 35
    add-int/lit8 v0, v0, 0x1

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.class final Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;
.super Ljava/lang/Object;
.source "SheetRefEvaluator.java"


# instance fields
.field private O8:Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;

.field private final 〇080:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

.field private final 〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/EvaluationTracker;

.field private final 〇o〇:I


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;Lcom/intsig/office/fc/hssf/formula/EvaluationTracker;I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    if-ltz p3, :cond_0

    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->〇080:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 7
    .line 8
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/EvaluationTracker;

    .line 9
    .line 10
    iput p3, p0, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->〇o〇:I

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 14
    .line 15
    new-instance p2, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v0, "Invalid sheetIndex: "

    .line 21
    .line 22
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string p3, "."

    .line 29
    .line 30
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p2

    .line 37
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    throw p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private 〇o00〇〇Oo()Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->O8:Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->〇080:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 6
    .line 7
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->〇o〇:I

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->getSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->O8:Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;

    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->O8:Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;

    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public O8(II)Z
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->〇o00〇〇Oo()Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0, p1, p2}, Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;->getCell(II)Lcom/intsig/office/fc/hssf/formula/EvaluationCell;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    const/4 p2, 0x0

    .line 10
    if-eqz p1, :cond_1

    .line 11
    .line 12
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationCell;->getCellType()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/4 v1, 0x2

    .line 17
    if-ne v0, v1, :cond_1

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->〇080:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->getWorkbook()Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook;->getFormulaTokens(Lcom/intsig/office/fc/hssf/formula/EvaluationCell;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    array-length v0, p1

    .line 30
    const/4 v1, 0x0

    .line 31
    :goto_0
    if-ge v1, v0, :cond_1

    .line 32
    .line 33
    aget-object v2, p1, v1

    .line 34
    .line 35
    instance-of v3, v2, Lcom/intsig/office/fc/hssf/formula/ptg/FuncVarPtg;

    .line 36
    .line 37
    if-eqz v3, :cond_0

    .line 38
    .line 39
    check-cast v2, Lcom/intsig/office/fc/hssf/formula/ptg/FuncVarPtg;

    .line 40
    .line 41
    const-string v3, "SUBTOTAL"

    .line 42
    .line 43
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/ptg/AbstractFunctionPtg;->getName()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 48
    .line 49
    .line 50
    move-result v2

    .line 51
    if-eqz v2, :cond_0

    .line 52
    .line 53
    const/4 p2, 0x1

    .line 54
    goto :goto_1

    .line 55
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_1
    :goto_1
    return p2
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public 〇080(II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->〇080:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->〇o00〇〇Oo()Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget v2, p0, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->〇o〇:I

    .line 8
    .line 9
    iget-object v5, p0, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/EvaluationTracker;

    .line 10
    .line 11
    move v3, p1

    .line 12
    move v4, p2

    .line 13
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->evaluateReference(Lcom/intsig/office/fc/hssf/formula/EvaluationSheet;IIILcom/intsig/office/fc/hssf/formula/EvaluationTracker;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    return-object p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public 〇o〇()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->〇080:Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/SheetRefEvaluator;->〇o〇:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/formula/WorkbookEvaluator;->getSheetName(I)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

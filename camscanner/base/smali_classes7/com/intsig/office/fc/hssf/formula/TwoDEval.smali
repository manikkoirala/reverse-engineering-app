.class public interface abstract Lcom/intsig/office/fc/hssf/formula/TwoDEval;
.super Ljava/lang/Object;
.source "TwoDEval.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;


# virtual methods
.method public abstract getColumn(I)Lcom/intsig/office/fc/hssf/formula/TwoDEval;
.end method

.method public abstract getHeight()I
.end method

.method public abstract getRow(I)Lcom/intsig/office/fc/hssf/formula/TwoDEval;
.end method

.method public abstract getValue(II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
.end method

.method public abstract getWidth()I
.end method

.method public abstract isColumn()Z
.end method

.method public abstract isRow()Z
.end method

.method public abstract isSubTotal(II)Z
.end method

.class final Lcom/intsig/office/fc/hssf/formula/function/TextFunction$LeftRight;
.super Lcom/intsig/office/fc/hssf/formula/function/Var1or2ArgFunction;
.source "TextFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/formula/function/TextFunction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "LeftRight"
.end annotation


# static fields
.field private static final 〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;


# instance fields
.field private final 〇080:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 2
    .line 3
    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    .line 4
    .line 5
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/function/TextFunction$LeftRight;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected constructor <init>(Z)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/function/Var1or2ArgFunction;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-boolean p1, p0, Lcom/intsig/office/fc/hssf/formula/function/TextFunction$LeftRight;->〇080:Z

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/function/TextFunction$LeftRight;->〇o00〇〇Oo:Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/intsig/office/fc/hssf/formula/function/TextFunction$LeftRight;->evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    move-result-object p1

    return-object p1
.end method

.method public evaluate(IILcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 0

    .line 2
    :try_start_0
    invoke-static {p3, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/TextFunction;->evaluateStringArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Ljava/lang/String;

    move-result-object p3

    .line 3
    invoke-static {p4, p1, p2}, Lcom/intsig/office/fc/hssf/formula/function/TextFunction;->evaluateIntArg(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)I

    move-result p1
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    if-gez p1, :cond_0

    .line 4
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    return-object p1

    .line 5
    :cond_0
    iget-boolean p2, p0, Lcom/intsig/office/fc/hssf/formula/function/TextFunction$LeftRight;->〇080:Z

    const/4 p4, 0x0

    if-eqz p2, :cond_1

    .line 6
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p2

    invoke-static {p2, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-virtual {p3, p4, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 7
    :cond_1
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p2

    sub-int/2addr p2, p1

    invoke-static {p4, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-virtual {p3, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 8
    :goto_0
    new-instance p2, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;

    invoke-direct {p2, p1}, Lcom/intsig/office/fc/hssf/formula/eval/StringEval;-><init>(Ljava/lang/String;)V

    return-object p2

    :catch_0
    move-exception p1

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    move-result-object p1

    return-object p1
.end method

.class public final Lcom/intsig/office/fc/hssf/formula/ptg/DeletedArea3DPtg;
.super Lcom/intsig/office/fc/hssf/formula/ptg/OperandPtg;
.source "DeletedArea3DPtg.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/WorkbookDependentFormula;


# static fields
.field public static final sid:B = 0x3dt


# instance fields
.field private final field_1_index_extern_sheet:I

.field private final unused1:I

.field private final unused2:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/OperandPtg;-><init>()V

    .line 2
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedArea3DPtg;->field_1_index_extern_sheet:I

    const/4 p1, 0x0

    .line 3
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedArea3DPtg;->unused1:I

    .line 4
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedArea3DPtg;->unused2:I

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V
    .locals 1

    .line 5
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/OperandPtg;-><init>()V

    .line 6
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedArea3DPtg;->field_1_index_extern_sheet:I

    .line 7
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedArea3DPtg;->unused1:I

    .line 8
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readInt()I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedArea3DPtg;->unused2:I

    return-void
.end method


# virtual methods
.method public getDefaultOperandClass()B
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSize()I
    .locals 1

    .line 1
    const/16 v0, 0xb

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public toFormulaString()Ljava/lang/String;
    .locals 2

    .line 4
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "3D references need a workbook to determine formula text"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toFormulaString(Lcom/intsig/office/fc/hssf/formula/FormulaRenderingWorkbook;)Ljava/lang/String;
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedArea3DPtg;->field_1_index_extern_sheet:I

    const/16 v1, 0x17

    .line 2
    invoke-static {v1}, Lcom/intsig/office/fc/ss/usermodel/ErrorConstants;->getText(I)Ljava/lang/String;

    move-result-object v1

    .line 3
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/ExternSheetNameResolver;->〇080(Lcom/intsig/office/fc/hssf/formula/FormulaRenderingWorkbook;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getPtgClass()B

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    add-int/lit8 v0, v0, 0x3d

    .line 6
    .line 7
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeByte(I)V

    .line 8
    .line 9
    .line 10
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedArea3DPtg;->field_1_index_extern_sheet:I

    .line 11
    .line 12
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 13
    .line 14
    .line 15
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedArea3DPtg;->unused1:I

    .line 16
    .line 17
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeInt(I)V

    .line 18
    .line 19
    .line 20
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedArea3DPtg;->unused2:I

    .line 21
    .line 22
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeInt(I)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

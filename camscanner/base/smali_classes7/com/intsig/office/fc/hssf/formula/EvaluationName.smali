.class public interface abstract Lcom/intsig/office/fc/hssf/formula/EvaluationName;
.super Ljava/lang/Object;
.source "EvaluationName.java"


# virtual methods
.method public abstract createPtg()Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;
.end method

.method public abstract getNameDefinition()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
.end method

.method public abstract getNameText()Ljava/lang/String;
.end method

.method public abstract hasFormula()Z
.end method

.method public abstract isFunctionName()Z
.end method

.method public abstract isRange()Z
.end method

.class public abstract Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
.super Ljava/lang/Object;
.source "Ptg.java"


# static fields
.field public static final CLASS_ARRAY:B = 0x40t

.field public static final CLASS_REF:B = 0x0t

.field public static final CLASS_VALUE:B = 0x20t

.field public static final EMPTY_PTG_ARRAY:[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;


# instance fields
.field private ptgClass:B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    new-array v0, v0, [Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 3
    .line 4
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->EMPTY_PTG_ARRAY:[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-byte v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->ptgClass:B

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static createBasePtg(BLcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 2

    .line 1
    packed-switch p0, :pswitch_data_0

    .line 2
    .line 3
    .line 4
    :pswitch_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 5
    .line 6
    new-instance v0, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v1, "Unexpected base token id ("

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    const-string p0, ")"

    .line 20
    .line 21
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p0

    .line 28
    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    throw p1

    .line 32
    :pswitch_1
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;

    .line 33
    .line 34
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/NumberPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 35
    .line 36
    .line 37
    return-object p0

    .line 38
    :pswitch_2
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/IntPtg;

    .line 39
    .line 40
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/IntPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 41
    .line 42
    .line 43
    return-object p0

    .line 44
    :pswitch_3
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/ptg/BoolPtg;->read(Lcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/ptg/BoolPtg;

    .line 45
    .line 46
    .line 47
    move-result-object p0

    .line 48
    return-object p0

    .line 49
    :pswitch_4
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/ptg/ErrPtg;->read(Lcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/ptg/ErrPtg;

    .line 50
    .line 51
    .line 52
    move-result-object p0

    .line 53
    return-object p0

    .line 54
    :pswitch_5
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/AttrPtg;

    .line 55
    .line 56
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/AttrPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 57
    .line 58
    .line 59
    return-object p0

    .line 60
    :pswitch_6
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/StringPtg;

    .line 61
    .line 62
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/StringPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 63
    .line 64
    .line 65
    return-object p0

    .line 66
    :pswitch_7
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/MissingArgPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 67
    .line 68
    return-object p0

    .line 69
    :pswitch_8
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/ParenthesisPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ControlPtg;

    .line 70
    .line 71
    return-object p0

    .line 72
    :pswitch_9
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/PercentPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 73
    .line 74
    return-object p0

    .line 75
    :pswitch_a
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/UnaryMinusPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 76
    .line 77
    return-object p0

    .line 78
    :pswitch_b
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/UnaryPlusPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 79
    .line 80
    return-object p0

    .line 81
    :pswitch_c
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/RangePtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;

    .line 82
    .line 83
    return-object p0

    .line 84
    :pswitch_d
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/UnionPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;

    .line 85
    .line 86
    return-object p0

    .line 87
    :pswitch_e
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/IntersectionPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/OperationPtg;

    .line 88
    .line 89
    return-object p0

    .line 90
    :pswitch_f
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/NotEqualPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 91
    .line 92
    return-object p0

    .line 93
    :pswitch_10
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/GreaterThanPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 94
    .line 95
    return-object p0

    .line 96
    :pswitch_11
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/GreaterEqualPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 97
    .line 98
    return-object p0

    .line 99
    :pswitch_12
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/EqualPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 100
    .line 101
    return-object p0

    .line 102
    :pswitch_13
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/LessEqualPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 103
    .line 104
    return-object p0

    .line 105
    :pswitch_14
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/LessThanPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 106
    .line 107
    return-object p0

    .line 108
    :pswitch_15
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/ConcatPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 109
    .line 110
    return-object p0

    .line 111
    :pswitch_16
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/PowerPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 112
    .line 113
    return-object p0

    .line 114
    :pswitch_17
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/DividePtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 115
    .line 116
    return-object p0

    .line 117
    :pswitch_18
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/MultiplyPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 118
    .line 119
    return-object p0

    .line 120
    :pswitch_19
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/SubtractPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 121
    .line 122
    return-object p0

    .line 123
    :pswitch_1a
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/AddPtg;->instance:Lcom/intsig/office/fc/hssf/formula/ptg/ValueOperatorPtg;

    .line 124
    .line 125
    return-object p0

    .line 126
    :pswitch_1b
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/TblPtg;

    .line 127
    .line 128
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/TblPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 129
    .line 130
    .line 131
    return-object p0

    .line 132
    :pswitch_1c
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/ExpPtg;

    .line 133
    .line 134
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/ExpPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 135
    .line 136
    .line 137
    return-object p0

    .line 138
    :pswitch_1d
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/ptg/UnknownPtg;

    .line 139
    .line 140
    invoke-direct {p1, p0}, Lcom/intsig/office/fc/hssf/formula/ptg/UnknownPtg;-><init>(I)V

    .line 141
    .line 142
    .line 143
    return-object p1

    .line 144
    nop

    .line 145
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static createClassifiedPtg(BLcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 2

    .line 1
    and-int/lit8 v0, p0, 0x1f

    .line 2
    .line 3
    or-int/lit8 v0, v0, 0x20

    .line 4
    .line 5
    packed-switch v0, :pswitch_data_0

    .line 6
    .line 7
    .line 8
    packed-switch v0, :pswitch_data_1

    .line 9
    .line 10
    .line 11
    packed-switch v0, :pswitch_data_2

    .line 12
    .line 13
    .line 14
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    .line 15
    .line 16
    new-instance v0, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v1, " Unknown Ptg in Formula: 0x"

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string v1, " ("

    .line 34
    .line 35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    const-string p0, ")"

    .line 42
    .line 43
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object p0

    .line 50
    invoke-direct {p1, p0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    throw p1

    .line 54
    :pswitch_0
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedArea3DPtg;

    .line 55
    .line 56
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedArea3DPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 57
    .line 58
    .line 59
    return-object p0

    .line 60
    :pswitch_1
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedRef3DPtg;

    .line 61
    .line 62
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedRef3DPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 63
    .line 64
    .line 65
    return-object p0

    .line 66
    :pswitch_2
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 67
    .line 68
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 69
    .line 70
    .line 71
    return-object p0

    .line 72
    :pswitch_3
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;

    .line 73
    .line 74
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 75
    .line 76
    .line 77
    return-object p0

    .line 78
    :pswitch_4
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;

    .line 79
    .line 80
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 81
    .line 82
    .line 83
    return-object p0

    .line 84
    :pswitch_5
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaNPtg;

    .line 85
    .line 86
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaNPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 87
    .line 88
    .line 89
    return-object p0

    .line 90
    :pswitch_6
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/RefNPtg;

    .line 91
    .line 92
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/RefNPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 93
    .line 94
    .line 95
    return-object p0

    .line 96
    :pswitch_7
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaErrPtg;

    .line 97
    .line 98
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaErrPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 99
    .line 100
    .line 101
    return-object p0

    .line 102
    :pswitch_8
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/RefErrorPtg;

    .line 103
    .line 104
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/RefErrorPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 105
    .line 106
    .line 107
    return-object p0

    .line 108
    :pswitch_9
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/MemFuncPtg;

    .line 109
    .line 110
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/MemFuncPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 111
    .line 112
    .line 113
    return-object p0

    .line 114
    :pswitch_a
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/MemErrPtg;

    .line 115
    .line 116
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/MemErrPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 117
    .line 118
    .line 119
    return-object p0

    .line 120
    :pswitch_b
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/MemAreaPtg;

    .line 121
    .line 122
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/MemAreaPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 123
    .line 124
    .line 125
    return-object p0

    .line 126
    :pswitch_c
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtg;

    .line 127
    .line 128
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 129
    .line 130
    .line 131
    return-object p0

    .line 132
    :pswitch_d
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtg;

    .line 133
    .line 134
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 135
    .line 136
    .line 137
    return-object p0

    .line 138
    :pswitch_e
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;

    .line 139
    .line 140
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/NamePtg;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 141
    .line 142
    .line 143
    return-object p0

    .line 144
    :pswitch_f
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/ptg/FuncVarPtg;->create(Lcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/ptg/FuncVarPtg;

    .line 145
    .line 146
    .line 147
    move-result-object p0

    .line 148
    return-object p0

    .line 149
    :pswitch_10
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/ptg/FuncPtg;->create(Lcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/ptg/FuncPtg;

    .line 150
    .line 151
    .line 152
    move-result-object p0

    .line 153
    return-object p0

    .line 154
    :pswitch_11
    new-instance p0, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;

    .line 155
    .line 156
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;-><init>(Lcom/intsig/office/fc/util/LittleEndianInput;)V

    .line 157
    .line 158
    .line 159
    return-object p0

    .line 160
    nop

    .line 161
    :pswitch_data_0
    .packed-switch 0x20
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
    .end packed-switch

    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    :pswitch_data_1
    .packed-switch 0x29
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch

    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    :pswitch_data_2
    .packed-switch 0x39
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static createPtg(Lcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 4

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/util/LittleEndianInput;->readByte()B

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/16 v1, 0x20

    .line 6
    .line 7
    if-ge v0, v1, :cond_0

    .line 8
    .line 9
    invoke-static {v0, p0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->createBasePtg(BLcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 10
    .line 11
    .line 12
    move-result-object p0

    .line 13
    return-object p0

    .line 14
    :cond_0
    invoke-static {v0, p0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->createClassifiedPtg(BLcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 15
    .line 16
    .line 17
    move-result-object p0

    .line 18
    const/16 v2, 0x60

    .line 19
    .line 20
    const/16 v3, 0x40

    .line 21
    .line 22
    if-lt v0, v2, :cond_1

    .line 23
    .line 24
    invoke-virtual {p0, v3}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->setClass(B)V

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    if-lt v0, v3, :cond_2

    .line 29
    .line 30
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->setClass(B)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_2
    const/4 v0, 0x0

    .line 35
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->setClass(B)V

    .line 36
    .line 37
    .line 38
    :goto_0
    return-object p0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static doesFormulaReferToDeletedCell([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Z
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    array-length v2, p0

    .line 4
    if-ge v1, v2, :cond_1

    .line 5
    .line 6
    aget-object v2, p0, v1

    .line 7
    .line 8
    invoke-static {v2}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->isDeletedCellRef(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    const/4 p0, 0x1

    .line 15
    return p0

    .line 16
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_1
    return v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static getEncodedSize([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)I
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    array-length v2, p0

    .line 4
    if-ge v0, v2, :cond_0

    .line 5
    .line 6
    aget-object v2, p0, v0

    .line 7
    .line 8
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getSize()I

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    add-int/2addr v1, v2

    .line 13
    add-int/lit8 v0, v0, 0x1

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    return v1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static getEncodedSizeWithoutArrayData([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)I
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    array-length v2, p0

    .line 4
    if-ge v0, v2, :cond_1

    .line 5
    .line 6
    aget-object v2, p0, v0

    .line 7
    .line 8
    instance-of v3, v2, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg;

    .line 9
    .line 10
    if-eqz v3, :cond_0

    .line 11
    .line 12
    add-int/lit8 v1, v1, 0x8

    .line 13
    .line 14
    goto :goto_1

    .line 15
    :cond_0
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getSize()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    add-int/2addr v1, v2

    .line 20
    :goto_1
    add-int/lit8 v0, v0, 0x1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    return v1
    .line 24
.end method

.method private static isDeletedCellRef(Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/ErrPtg;->REF_INVALID:Lcom/intsig/office/fc/hssf/formula/ptg/ErrPtg;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne p0, v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedArea3DPtg;

    .line 8
    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    return v1

    .line 12
    :cond_1
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/DeletedRef3DPtg;

    .line 13
    .line 14
    if-eqz v0, :cond_2

    .line 15
    .line 16
    return v1

    .line 17
    :cond_2
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaErrPtg;

    .line 18
    .line 19
    if-eqz v0, :cond_3

    .line 20
    .line 21
    return v1

    .line 22
    :cond_3
    instance-of p0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/RefErrorPtg;

    .line 23
    .line 24
    if-eqz p0, :cond_4

    .line 25
    .line 26
    return v1

    .line 27
    :cond_4
    const/4 p0, 0x0

    .line 28
    return p0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static readTokens(ILcom/intsig/office/fc/util/LittleEndianInput;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 6

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    div-int/lit8 v1, p0, 0x2

    .line 4
    .line 5
    add-int/lit8 v1, v1, 0x4

    .line 6
    .line 7
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 8
    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    const/4 v2, 0x0

    .line 12
    const/4 v3, 0x0

    .line 13
    :goto_0
    if-ge v2, p0, :cond_1

    .line 14
    .line 15
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->createPtg(Lcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 16
    .line 17
    .line 18
    move-result-object v4

    .line 19
    instance-of v5, v4, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;

    .line 20
    .line 21
    if-eqz v5, :cond_0

    .line 22
    .line 23
    const/4 v3, 0x1

    .line 24
    :cond_0
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->getSize()I

    .line 25
    .line 26
    .line 27
    move-result v5

    .line 28
    add-int/2addr v2, v5

    .line 29
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    if-ne v2, p0, :cond_5

    .line 34
    .line 35
    if-eqz v3, :cond_4

    .line 36
    .line 37
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->toPtgArray(Ljava/util/List;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 38
    .line 39
    .line 40
    move-result-object p0

    .line 41
    :goto_1
    array-length v0, p0

    .line 42
    if-ge v1, v0, :cond_3

    .line 43
    .line 44
    aget-object v0, p0, v1

    .line 45
    .line 46
    instance-of v2, v0, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;

    .line 47
    .line 48
    if-eqz v2, :cond_2

    .line 49
    .line 50
    check-cast v0, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;

    .line 51
    .line 52
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg$Initial;->〇080(Lcom/intsig/office/fc/util/LittleEndianInput;)Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    aput-object v0, p0, v1

    .line 57
    .line 58
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 59
    .line 60
    goto :goto_1

    .line 61
    :cond_3
    return-object p0

    .line 62
    :cond_4
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->toPtgArray(Ljava/util/List;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 63
    .line 64
    .line 65
    move-result-object p0

    .line 66
    return-object p0

    .line 67
    :cond_5
    new-instance p0, Ljava/lang/RuntimeException;

    .line 68
    .line 69
    const-string p1, "Ptg array size mismatch"

    .line 70
    .line 71
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    throw p0
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static serializePtgs([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[BI)I
    .locals 6

    .line 1
    array-length v0, p0

    .line 2
    new-instance v1, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;

    .line 3
    .line 4
    invoke-direct {v1, p1, p2}, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;-><init>([BI)V

    .line 5
    .line 6
    .line 7
    const/4 p1, 0x0

    .line 8
    const/4 v2, 0x0

    .line 9
    const/4 v3, 0x0

    .line 10
    :goto_0
    if-ge v3, v0, :cond_2

    .line 11
    .line 12
    aget-object v4, p0, v3

    .line 13
    .line 14
    invoke-virtual {v4, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->write(Lcom/intsig/office/fc/util/LittleEndianOutput;)V

    .line 15
    .line 16
    .line 17
    instance-of v5, v4, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg;

    .line 18
    .line 19
    if-eqz v5, :cond_1

    .line 20
    .line 21
    if-nez p1, :cond_0

    .line 22
    .line 23
    new-instance p1, Ljava/util/ArrayList;

    .line 24
    .line 25
    const/4 v5, 0x5

    .line 26
    invoke-direct {p1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 27
    .line 28
    .line 29
    :cond_0
    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_2
    if-eqz p1, :cond_3

    .line 36
    .line 37
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 38
    .line 39
    .line 40
    move-result p0

    .line 41
    if-ge v2, p0, :cond_3

    .line 42
    .line 43
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    move-result-object p0

    .line 47
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg;

    .line 48
    .line 49
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/ArrayPtg;->writeTokenValueBytes(Lcom/intsig/office/fc/util/LittleEndianOutput;)I

    .line 50
    .line 51
    .line 52
    add-int/lit8 v2, v2, 0x1

    .line 53
    .line 54
    goto :goto_1

    .line 55
    :cond_3
    invoke-virtual {v1}, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->getWriteIndex()I

    .line 56
    .line 57
    .line 58
    move-result p0

    .line 59
    sub-int/2addr p0, p2

    .line 60
    return p0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method private static toPtgArray(Ljava/util/List;)[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;",
            ">;)[",
            "Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;"
        }
    .end annotation

    .line 1
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object p0, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->EMPTY_PTG_ARRAY:[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 8
    .line 9
    return-object p0

    .line 10
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    new-array v0, v0, [Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 15
    .line 16
    invoke-interface {p0, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    return-object v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public abstract getDefaultOperandClass()B
.end method

.method public final getPtgClass()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->ptgClass:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getRVAType()C
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->isBaseToken()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/16 v0, 0x2e

    .line 8
    .line 9
    return v0

    .line 10
    :cond_0
    iget-byte v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->ptgClass:B

    .line 11
    .line 12
    if-eqz v0, :cond_3

    .line 13
    .line 14
    const/16 v1, 0x20

    .line 15
    .line 16
    if-eq v0, v1, :cond_2

    .line 17
    .line 18
    const/16 v1, 0x40

    .line 19
    .line 20
    if-ne v0, v1, :cond_1

    .line 21
    .line 22
    const/16 v0, 0x41

    .line 23
    .line 24
    return v0

    .line 25
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    .line 26
    .line 27
    new-instance v1, Ljava/lang/StringBuilder;

    .line 28
    .line 29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 30
    .line 31
    .line 32
    const-string v2, "Unknown operand class ("

    .line 33
    .line 34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    iget-byte v2, p0, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->ptgClass:B

    .line 38
    .line 39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    const-string v2, ")"

    .line 43
    .line 44
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    throw v0

    .line 55
    :cond_2
    const/16 v0, 0x56

    .line 56
    .line 57
    return v0

    .line 58
    :cond_3
    const/16 v0, 0x52

    .line 59
    .line 60
    return v0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public abstract getSize()I
.end method

.method public abstract isBaseToken()Z
.end method

.method public final setClass(B)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->isBaseToken()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    iput-byte p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;->ptgClass:B

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 11
    .line 12
    const-string v0, "setClass should not be called on a base token"

    .line 13
    .line 14
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    throw p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public abstract toFormulaString()Ljava/lang/String;
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public abstract write(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
.end method

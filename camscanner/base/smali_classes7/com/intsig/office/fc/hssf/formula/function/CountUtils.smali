.class final Lcom/intsig/office/fc/hssf/formula/function/CountUtils;
.super Ljava/lang/Object;
.source "CountUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchAreaPredicate;,
        Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;
    }
.end annotation


# direct methods
.method public static 〇080(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;)I
    .locals 1

    .line 1
    if-eqz p0, :cond_2

    .line 2
    .line 3
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/TwoDEval;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/TwoDEval;

    .line 8
    .line 9
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/formula/function/CountUtils;->〇o〇(Lcom/intsig/office/fc/hssf/formula/TwoDEval;Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;)I

    .line 10
    .line 11
    .line 12
    move-result p0

    .line 13
    return p0

    .line 14
    :cond_0
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/formula/eval/RefEval;

    .line 15
    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    check-cast p0, Lcom/intsig/office/fc/hssf/formula/eval/RefEval;

    .line 19
    .line 20
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hssf/formula/function/CountUtils;->〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/eval/RefEval;Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;)I

    .line 21
    .line 22
    .line 23
    move-result p0

    .line 24
    return p0

    .line 25
    :cond_1
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;->matches(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Z

    .line 26
    .line 27
    .line 28
    move-result p0

    .line 29
    return p0

    .line 30
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 31
    .line 32
    const-string p1, "eval must not be null"

    .line 33
    .line 34
    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    throw p0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static 〇o00〇〇Oo(Lcom/intsig/office/fc/hssf/formula/eval/RefEval;Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;)I
    .locals 0

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/hssf/formula/eval/RefEval;->getInnerValueEval()Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-interface {p1, p0}, Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;->matches(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Z

    .line 6
    .line 7
    .line 8
    move-result p0

    .line 9
    if-eqz p0, :cond_0

    .line 10
    .line 11
    const/4 p0, 0x1

    .line 12
    return p0

    .line 13
    :cond_0
    const/4 p0, 0x0

    .line 14
    return p0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static 〇o〇(Lcom/intsig/office/fc/hssf/formula/TwoDEval;Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;)I
    .locals 8

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/hssf/formula/TwoDEval;->getHeight()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-interface {p0}, Lcom/intsig/office/fc/hssf/formula/TwoDEval;->getWidth()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x0

    .line 10
    const/4 v3, 0x0

    .line 11
    const/4 v4, 0x0

    .line 12
    :goto_0
    if-ge v3, v0, :cond_3

    .line 13
    .line 14
    const/4 v5, 0x0

    .line 15
    :goto_1
    if-ge v5, v1, :cond_2

    .line 16
    .line 17
    invoke-interface {p0, v3, v5}, Lcom/intsig/office/fc/hssf/formula/TwoDEval;->getValue(II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 18
    .line 19
    .line 20
    move-result-object v6

    .line 21
    instance-of v7, p1, Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchAreaPredicate;

    .line 22
    .line 23
    if-eqz v7, :cond_0

    .line 24
    .line 25
    move-object v7, p1

    .line 26
    check-cast v7, Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchAreaPredicate;

    .line 27
    .line 28
    invoke-interface {v7, p0, v3, v5}, Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchAreaPredicate;->matches(Lcom/intsig/office/fc/hssf/formula/TwoDEval;II)Z

    .line 29
    .line 30
    .line 31
    move-result v7

    .line 32
    if-nez v7, :cond_0

    .line 33
    .line 34
    goto :goto_2

    .line 35
    :cond_0
    invoke-interface {p1, v6}, Lcom/intsig/office/fc/hssf/formula/function/CountUtils$I_MatchPredicate;->matches(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)Z

    .line 36
    .line 37
    .line 38
    move-result v6

    .line 39
    if-eqz v6, :cond_1

    .line 40
    .line 41
    add-int/lit8 v4, v4, 0x1

    .line 42
    .line 43
    :cond_1
    :goto_2
    add-int/lit8 v5, v5, 0x1

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_3
    return v4
    .line 50
    .line 51
    .line 52
    .line 53
.end method

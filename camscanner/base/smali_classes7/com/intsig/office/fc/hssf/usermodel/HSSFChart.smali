.class public final Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;
.super Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;
.source "HSSFChart.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;,
        Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;
    }
.end annotation


# static fields
.field public static final OBJECT_TYPE_CHART:S = 0x5s


# instance fields
.field private chartRecord:Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

.field private chartSeriesText:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;"
        }
    .end annotation
.end field

.field private chartTitleFormat:Lcom/intsig/office/fc/hssf/record/chart/ChartTitleFormatRecord;

.field private legendRecord:Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;

.field private marginColorFormat:Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

.field private series:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;",
            ">;"
        }
    .end annotation
.end field

.field private seriesBackgroundColorFormat:Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

.field private sheet:Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

.field private type:Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

.field private valueRanges:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p2, p3, p4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)V

    .line 2
    .line 3
    .line 4
    new-instance p3, Ljava/util/HashMap;

    .line 5
    .line 6
    invoke-direct {p3}, Ljava/util/HashMap;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p3, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartSeriesText:Ljava/util/Map;

    .line 10
    .line 11
    new-instance p3, Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object p3, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->valueRanges:Ljava/util/List;

    .line 17
    .line 18
    sget-object p3, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;->Unknown:Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

    .line 19
    .line 20
    iput-object p3, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->type:Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

    .line 21
    .line 22
    new-instance p3, Ljava/util/ArrayList;

    .line 23
    .line 24
    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 25
    .line 26
    .line 27
    iput-object p3, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 28
    .line 29
    if-eqz p2, :cond_0

    .line 30
    .line 31
    if-eqz p1, :cond_0

    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->processLineWidth()V

    .line 34
    .line 35
    .line 36
    invoke-virtual {p0, p2, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;->processLine(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0, p2, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->processSimpleBackground(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;->processRotationAndFlip(Lcom/intsig/office/fc/ddf/EscherContainerRecord;)V

    .line 43
    .line 44
    .line 45
    :cond_0
    const/4 p1, 0x5

    .line 46
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->setShapeType(I)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public static convertRecordsToChart(Ljava/util/List;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;",
            ")V"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_e

    .line 2
    .line 3
    if-nez p0, :cond_0

    .line 4
    .line 5
    goto/16 :goto_4

    .line 6
    .line 7
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x0

    .line 12
    const/4 v2, 0x0

    .line 13
    :goto_0
    if-ge v2, v0, :cond_e

    .line 14
    .line 15
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    check-cast v3, Lcom/intsig/office/fc/hssf/record/Record;

    .line 20
    .line 21
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 22
    .line 23
    if-eqz v4, :cond_1

    .line 24
    .line 25
    check-cast v3, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 26
    .line 27
    invoke-virtual {p1, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->setChartRecord(Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;)V

    .line 28
    .line 29
    .line 30
    goto/16 :goto_3

    .line 31
    .line 32
    :cond_1
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;

    .line 33
    .line 34
    if-eqz v4, :cond_2

    .line 35
    .line 36
    check-cast v3, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;

    .line 37
    .line 38
    iput-object v3, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->legendRecord:Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;

    .line 39
    .line 40
    goto/16 :goto_3

    .line 41
    .line 42
    :cond_2
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 43
    .line 44
    .line 45
    move-result v4

    .line 46
    const/16 v5, 0x100a

    .line 47
    .line 48
    if-ne v4, v5, :cond_4

    .line 49
    .line 50
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->getSeries()[Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    array-length v4, v4

    .line 55
    if-nez v4, :cond_3

    .line 56
    .line 57
    check-cast v3, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 58
    .line 59
    iput-object v3, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->marginColorFormat:Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 60
    .line 61
    goto/16 :goto_3

    .line 62
    .line 63
    :cond_3
    check-cast v3, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 64
    .line 65
    iput-object v3, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->seriesBackgroundColorFormat:Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 66
    .line 67
    goto/16 :goto_3

    .line 68
    .line 69
    :cond_4
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 70
    .line 71
    if-eqz v4, :cond_5

    .line 72
    .line 73
    invoke-static {p0, p1, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->convetRecordsToSeriesByPostion(Ljava/util/List;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;I)I

    .line 74
    .line 75
    .line 76
    move-result v2

    .line 77
    goto :goto_3

    .line 78
    :cond_5
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;

    .line 79
    .line 80
    if-eqz v4, :cond_6

    .line 81
    .line 82
    invoke-static {p0, p1, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->convetRecordsToText(Ljava/util/List;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;I)I

    .line 83
    .line 84
    .line 85
    move-result v2

    .line 86
    goto :goto_3

    .line 87
    :cond_6
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/record/chart/DataLabelExtensionRecord;

    .line 88
    .line 89
    if-eqz v4, :cond_7

    .line 90
    .line 91
    iget-object v4, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 92
    .line 93
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 94
    .line 95
    .line 96
    move-result v5

    .line 97
    add-int/lit8 v5, v5, -0x1

    .line 98
    .line 99
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 100
    .line 101
    .line 102
    move-result-object v4

    .line 103
    check-cast v4, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 104
    .line 105
    check-cast v3, Lcom/intsig/office/fc/hssf/record/chart/DataLabelExtensionRecord;

    .line 106
    .line 107
    invoke-virtual {v4, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->setDataLabelExtensionRecord(Lcom/intsig/office/fc/hssf/record/chart/DataLabelExtensionRecord;)V

    .line 108
    .line 109
    .line 110
    goto :goto_3

    .line 111
    :cond_7
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/record/chart/ChartTitleFormatRecord;

    .line 112
    .line 113
    if-eqz v4, :cond_8

    .line 114
    .line 115
    check-cast v3, Lcom/intsig/office/fc/hssf/record/chart/ChartTitleFormatRecord;

    .line 116
    .line 117
    iput-object v3, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartTitleFormat:Lcom/intsig/office/fc/hssf/record/chart/ChartTitleFormatRecord;

    .line 118
    .line 119
    goto :goto_3

    .line 120
    :cond_8
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;

    .line 121
    .line 122
    if-eqz v4, :cond_9

    .line 123
    .line 124
    iget-object v4, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->valueRanges:Ljava/util/List;

    .line 125
    .line 126
    check-cast v3, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;

    .line 127
    .line 128
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    .line 130
    .line 131
    goto :goto_3

    .line 132
    :cond_9
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 133
    .line 134
    .line 135
    move-result v4

    .line 136
    const/16 v5, 0x1041

    .line 137
    .line 138
    if-ne v4, v5, :cond_a

    .line 139
    .line 140
    goto :goto_3

    .line 141
    :cond_a
    invoke-static {}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;->values()[Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

    .line 142
    .line 143
    .line 144
    move-result-object v4

    .line 145
    array-length v5, v4

    .line 146
    const/4 v6, 0x0

    .line 147
    :goto_1
    if-ge v6, v5, :cond_d

    .line 148
    .line 149
    aget-object v7, v4, v6

    .line 150
    .line 151
    sget-object v8, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;->Unknown:Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

    .line 152
    .line 153
    if-ne v7, v8, :cond_b

    .line 154
    .line 155
    goto :goto_2

    .line 156
    :cond_b
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 157
    .line 158
    .line 159
    move-result v8

    .line 160
    invoke-virtual {v7}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;->getSid()S

    .line 161
    .line 162
    .line 163
    move-result v9

    .line 164
    if-ne v8, v9, :cond_c

    .line 165
    .line 166
    iput-object v7, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->type:Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

    .line 167
    .line 168
    goto :goto_3

    .line 169
    :cond_c
    :goto_2
    add-int/lit8 v6, v6, 0x1

    .line 170
    .line 171
    goto :goto_1

    .line 172
    :cond_d
    :goto_3
    add-int/lit8 v2, v2, 0x1

    .line 173
    .line 174
    goto/16 :goto_0

    .line 175
    .line 176
    :cond_e
    :goto_4
    return-void
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private static convetRecordsToSeriesByPostion(Ljava/util/List;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;I)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;",
            "I)I"
        }
    .end annotation

    .line 1
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-ge p2, v0, :cond_7

    .line 6
    .line 7
    invoke-interface {p0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Lcom/intsig/office/fc/hssf/record/Record;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/16 v1, 0x1003

    .line 18
    .line 19
    if-eq v0, v1, :cond_0

    .line 20
    .line 21
    goto/16 :goto_2

    .line 22
    .line 23
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 24
    .line 25
    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    invoke-interface {p0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    check-cast v1, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 33
    .line 34
    invoke-direct {v0, p1, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;)V

    .line 35
    .line 36
    .line 37
    iget-object v1, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 38
    .line 39
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    .line 41
    .line 42
    const/4 v0, 0x1

    .line 43
    add-int/2addr p2, v0

    .line 44
    invoke-interface {p0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    instance-of v1, v1, Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 49
    .line 50
    if-eqz v1, :cond_6

    .line 51
    .line 52
    add-int/lit8 p2, p2, 0x1

    .line 53
    .line 54
    const/4 v1, 0x1

    .line 55
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 56
    .line 57
    .line 58
    move-result v2

    .line 59
    if-gt p2, v2, :cond_6

    .line 60
    .line 61
    if-lez v1, :cond_6

    .line 62
    .line 63
    invoke-interface {p0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    check-cast v2, Lcom/intsig/office/fc/hssf/record/Record;

    .line 68
    .line 69
    instance-of v3, v2, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 70
    .line 71
    if-eqz v3, :cond_1

    .line 72
    .line 73
    check-cast v2, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 74
    .line 75
    iget-object v3, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 76
    .line 77
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 78
    .line 79
    .line 80
    move-result v3

    .line 81
    if-lez v3, :cond_5

    .line 82
    .line 83
    iget-object v3, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 84
    .line 85
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 86
    .line 87
    .line 88
    move-result v4

    .line 89
    sub-int/2addr v4, v0

    .line 90
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 91
    .line 92
    .line 93
    move-result-object v3

    .line 94
    check-cast v3, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 95
    .line 96
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->insertData(Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;)V

    .line 97
    .line 98
    .line 99
    goto :goto_1

    .line 100
    :cond_1
    instance-of v3, v2, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 101
    .line 102
    if-eqz v3, :cond_2

    .line 103
    .line 104
    check-cast v2, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 105
    .line 106
    iget-object v3, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 107
    .line 108
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 109
    .line 110
    .line 111
    move-result v3

    .line 112
    if-lez v3, :cond_5

    .line 113
    .line 114
    iget-object v3, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 115
    .line 116
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 117
    .line 118
    .line 119
    move-result v4

    .line 120
    sub-int/2addr v4, v0

    .line 121
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 122
    .line 123
    .line 124
    move-result-object v3

    .line 125
    check-cast v3, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 126
    .line 127
    invoke-static {v3, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->〇080(Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;)V

    .line 128
    .line 129
    .line 130
    goto :goto_1

    .line 131
    :cond_2
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 132
    .line 133
    .line 134
    move-result v3

    .line 135
    const/16 v4, 0x100a

    .line 136
    .line 137
    if-ne v3, v4, :cond_3

    .line 138
    .line 139
    check-cast v2, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 140
    .line 141
    iget-object v3, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 142
    .line 143
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 144
    .line 145
    .line 146
    move-result v4

    .line 147
    sub-int/2addr v4, v0

    .line 148
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 149
    .line 150
    .line 151
    move-result-object v3

    .line 152
    check-cast v3, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 153
    .line 154
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->setAreaFormat(Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;)V

    .line 155
    .line 156
    .line 157
    goto :goto_1

    .line 158
    :cond_3
    instance-of v3, v2, Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 159
    .line 160
    if-eqz v3, :cond_4

    .line 161
    .line 162
    add-int/lit8 v1, v1, 0x1

    .line 163
    .line 164
    goto :goto_1

    .line 165
    :cond_4
    instance-of v2, v2, Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 166
    .line 167
    if-eqz v2, :cond_5

    .line 168
    .line 169
    add-int/lit8 v1, v1, -0x1

    .line 170
    .line 171
    :cond_5
    :goto_1
    add-int/lit8 p2, p2, 0x1

    .line 172
    .line 173
    goto :goto_0

    .line 174
    :cond_6
    sub-int/2addr p2, v0

    .line 175
    return p2

    .line 176
    :cond_7
    :goto_2
    const/4 p0, -0x1

    .line 177
    return p0
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private static convetRecordsToText(Ljava/util/List;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;I)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;",
            "I)I"
        }
    .end annotation

    .line 1
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-ge p2, v0, :cond_9

    .line 6
    .line 7
    invoke-interface {p0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Lcom/intsig/office/fc/hssf/record/Record;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/16 v1, 0x1025

    .line 18
    .line 19
    if-eq v0, v1, :cond_0

    .line 20
    .line 21
    goto/16 :goto_3

    .line 22
    .line 23
    :cond_0
    invoke-interface {p0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    check-cast v0, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;

    .line 28
    .line 29
    const/4 v1, 0x1

    .line 30
    add-int/2addr p2, v1

    .line 31
    invoke-interface {p0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    instance-of v2, v2, Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 36
    .line 37
    const/4 v3, 0x0

    .line 38
    if-eqz v2, :cond_5

    .line 39
    .line 40
    add-int/lit8 p2, p2, 0x1

    .line 41
    .line 42
    move-object v2, v3

    .line 43
    const/4 v4, 0x1

    .line 44
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 45
    .line 46
    .line 47
    move-result v5

    .line 48
    if-gt p2, v5, :cond_6

    .line 49
    .line 50
    if-lez v4, :cond_6

    .line 51
    .line 52
    invoke-interface {p0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    move-result-object v5

    .line 56
    check-cast v5, Lcom/intsig/office/fc/hssf/record/Record;

    .line 57
    .line 58
    instance-of v6, v5, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 59
    .line 60
    if-eqz v6, :cond_1

    .line 61
    .line 62
    invoke-interface {p0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    move-result-object v2

    .line 66
    check-cast v2, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 67
    .line 68
    goto :goto_1

    .line 69
    :cond_1
    instance-of v6, v5, Lcom/intsig/office/fc/hssf/record/chart/ObjectLinkRecord;

    .line 70
    .line 71
    if-eqz v6, :cond_2

    .line 72
    .line 73
    move-object v3, v5

    .line 74
    check-cast v3, Lcom/intsig/office/fc/hssf/record/chart/ObjectLinkRecord;

    .line 75
    .line 76
    goto :goto_1

    .line 77
    :cond_2
    instance-of v6, v5, Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 78
    .line 79
    if-eqz v6, :cond_3

    .line 80
    .line 81
    add-int/lit8 v4, v4, 0x1

    .line 82
    .line 83
    goto :goto_1

    .line 84
    :cond_3
    instance-of v5, v5, Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 85
    .line 86
    if-eqz v5, :cond_4

    .line 87
    .line 88
    add-int/lit8 v4, v4, -0x1

    .line 89
    .line 90
    :cond_4
    :goto_1
    add-int/lit8 p2, p2, 0x1

    .line 91
    .line 92
    goto :goto_0

    .line 93
    :cond_5
    move-object v2, v3

    .line 94
    :cond_6
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->getWidth()I

    .line 95
    .line 96
    .line 97
    move-result p0

    .line 98
    if-lez p0, :cond_8

    .line 99
    .line 100
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->getHeight()I

    .line 101
    .line 102
    .line 103
    move-result p0

    .line 104
    if-lez p0, :cond_8

    .line 105
    .line 106
    if-eqz v3, :cond_8

    .line 107
    .line 108
    iget-object p0, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 109
    .line 110
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 111
    .line 112
    .line 113
    move-result p0

    .line 114
    if-lez p0, :cond_8

    .line 115
    .line 116
    if-eqz v2, :cond_7

    .line 117
    .line 118
    iget-object p0, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartSeriesText:Ljava/util/Map;

    .line 119
    .line 120
    invoke-interface {p0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    .line 122
    .line 123
    goto :goto_2

    .line 124
    :cond_7
    iget-object p0, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 125
    .line 126
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 127
    .line 128
    .line 129
    move-result p0

    .line 130
    iget-object v0, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartSeriesText:Ljava/util/Map;

    .line 131
    .line 132
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 133
    .line 134
    .line 135
    move-result v0

    .line 136
    if-le p0, v0, :cond_8

    .line 137
    .line 138
    iget-object p0, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartSeriesText:Ljava/util/Map;

    .line 139
    .line 140
    iget-object p1, p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 141
    .line 142
    invoke-interface {p0}, Ljava/util/Map;->size()I

    .line 143
    .line 144
    .line 145
    move-result v0

    .line 146
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 147
    .line 148
    .line 149
    move-result-object p1

    .line 150
    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 151
    .line 152
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->getSeriesTextRecord()Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 153
    .line 154
    .line 155
    move-result-object p1

    .line 156
    invoke-interface {p0, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    .line 158
    .line 159
    :cond_8
    :goto_2
    sub-int/2addr p2, v1

    .line 160
    return p2

    .line 161
    :cond_9
    :goto_3
    const/4 p0, -0x1

    .line 162
    return p0
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method private createAllTextRecord()Lcom/intsig/office/fc/hssf/record/chart/TextRecord;
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x2

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setHorizontalAlignment(B)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setVerticalAlignment(B)V

    .line 11
    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setDisplayMode(S)V

    .line 15
    .line 16
    .line 17
    const/4 v2, 0x0

    .line 18
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setRgbColor(I)V

    .line 19
    .line 20
    .line 21
    const/16 v3, -0x25

    .line 22
    .line 23
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setX(I)V

    .line 24
    .line 25
    .line 26
    const/16 v3, -0x3c

    .line 27
    .line 28
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setY(I)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setWidth(I)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setHeight(I)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setAutoColor(Z)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowKey(Z)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowValue(Z)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setVertical(Z)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setAutoGeneratedText(Z)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setGenerated(Z)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setAutoLabelDeleted(Z)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setAutoBackground(Z)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setRotation(S)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowCategoryLabelAsPercentage(Z)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowValueAsPercentage(Z)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowBubbleSizes(Z)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowLabel(Z)V

    .line 74
    .line 75
    .line 76
    const/16 v1, 0x4d

    .line 77
    .line 78
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setIndexOfColorValue(S)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setDataLabelPlacement(S)V

    .line 82
    .line 83
    .line 84
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setTextRotation(S)V

    .line 85
    .line 86
    .line 87
    return-object v0
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private createAreaFormatRecord1()Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const v1, 0xffffff

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;->setForegroundColor(I)V

    .line 10
    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;->setBackgroundColor(I)V

    .line 14
    .line 15
    .line 16
    const/4 v2, 0x1

    .line 17
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;->setPattern(S)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;->setAutomatic(Z)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;->setInvert(Z)V

    .line 24
    .line 25
    .line 26
    const/16 v1, 0x4e

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;->setForecolorIndex(S)V

    .line 29
    .line 30
    .line 31
    const/16 v1, 0x4d

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;->setBackcolorIndex(S)V

    .line 34
    .line 35
    .line 36
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createAreaFormatRecord2()Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const v1, 0xc0c0c0

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;->setForegroundColor(I)V

    .line 10
    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;->setBackgroundColor(I)V

    .line 14
    .line 15
    .line 16
    const/4 v2, 0x1

    .line 17
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;->setPattern(S)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;->setAutomatic(Z)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;->setInvert(Z)V

    .line 24
    .line 25
    .line 26
    const/16 v1, 0x16

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;->setForecolorIndex(S)V

    .line 29
    .line 30
    .line 31
    const/16 v1, 0x4f

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;->setBackcolorIndex(S)V

    .line 34
    .line 35
    .line 36
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createAxisLineFormatRecord(S)Lcom/intsig/office/fc/hssf/record/chart/AxisLineFormatRecord;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/AxisLineFormatRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/AxisLineFormatRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/AxisLineFormatRecord;->setAxisType(S)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private createAxisOptionsRecord()Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, -0x6fe4

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setMinimumCategory(S)V

    .line 9
    .line 10
    .line 11
    const/16 v2, -0x702b

    .line 12
    .line 13
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setMaximumCategory(S)V

    .line 14
    .line 15
    .line 16
    const/4 v2, 0x2

    .line 17
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setMajorUnitValue(S)V

    .line 18
    .line 19
    .line 20
    const/4 v2, 0x0

    .line 21
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setMajorUnit(S)V

    .line 22
    .line 23
    .line 24
    const/4 v3, 0x1

    .line 25
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setMinorUnitValue(S)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setMinorUnit(S)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setBaseUnit(S)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setCrossingPoint(S)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setDefaultMinimum(Z)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setDefaultMaximum(Z)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setDefaultMajor(Z)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setDefaultMinorUnit(Z)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setIsDate(Z)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setDefaultBase(Z)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setDefaultCross(Z)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;->setDefaultDateSettings(Z)V

    .line 59
    .line 60
    .line 61
    return-object v0
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createAxisParentRecord()Lcom/intsig/office/fc/hssf/record/chart/AxisParentRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/AxisParentRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/AxisParentRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AxisParentRecord;->setAxisType(S)V

    .line 8
    .line 9
    .line 10
    const/16 v1, 0x1df

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AxisParentRecord;->setX(I)V

    .line 13
    .line 14
    .line 15
    const/16 v1, 0xdd

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AxisParentRecord;->setY(I)V

    .line 18
    .line 19
    .line 20
    const/16 v1, 0xbb3

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AxisParentRecord;->setWidth(I)V

    .line 23
    .line 24
    .line 25
    const/16 v1, 0xb56

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/AxisParentRecord;->setHeight(I)V

    .line 28
    .line 29
    .line 30
    return-object v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createAxisRecord(S)Lcom/intsig/office/fc/hssf/record/chart/AxisRecord;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/AxisRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/AxisRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/AxisRecord;->setAxisType(S)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private createAxisRecords(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createAxisParentRecord()Lcom/intsig/office/fc/hssf/record/chart/AxisParentRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createBeginRecord()Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createAxisRecord(S)Lcom/intsig/office/fc/hssf/record/chart/AxisRecord;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createBeginRecord()Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    .line 29
    .line 30
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createCategorySeriesAxisRecord()Lcom/intsig/office/fc/hssf/record/chart/CategorySeriesAxisRecord;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createAxisOptionsRecord()Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createTickRecord1()Lcom/intsig/office/fc/hssf/record/chart/TickRecord;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    .line 50
    .line 51
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createEndRecord()Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    .line 57
    .line 58
    const/4 v1, 0x1

    .line 59
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createAxisRecord(S)Lcom/intsig/office/fc/hssf/record/chart/AxisRecord;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createBeginRecord()Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    .line 72
    .line 73
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createValueRangeRecord()Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createTickRecord2()Lcom/intsig/office/fc/hssf/record/chart/TickRecord;

    .line 81
    .line 82
    .line 83
    move-result-object v2

    .line 84
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    .line 86
    .line 87
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createAxisLineFormatRecord(S)Lcom/intsig/office/fc/hssf/record/chart/AxisLineFormatRecord;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    .line 93
    .line 94
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createLineFormatRecord(Z)Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    .line 100
    .line 101
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createEndRecord()Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    .line 107
    .line 108
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createPlotAreaRecord()Lcom/intsig/office/fc/hssf/record/chart/PlotAreaRecord;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    .line 114
    .line 115
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createFrameRecord2()Lcom/intsig/office/fc/hssf/record/chart/FrameRecord;

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    .line 121
    .line 122
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createBeginRecord()Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 123
    .line 124
    .line 125
    move-result-object v0

    .line 126
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    .line 128
    .line 129
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createLineFormatRecord2()Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;

    .line 130
    .line 131
    .line 132
    move-result-object v0

    .line 133
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    .line 135
    .line 136
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createAreaFormatRecord2()Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    .line 142
    .line 143
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createEndRecord()Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 144
    .line 145
    .line 146
    move-result-object v0

    .line 147
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    .line 149
    .line 150
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createChartFormatRecord()Lcom/intsig/office/fc/hssf/record/chart/ChartFormatRecord;

    .line 151
    .line 152
    .line 153
    move-result-object v0

    .line 154
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    .line 156
    .line 157
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createBeginRecord()Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 158
    .line 159
    .line 160
    move-result-object v0

    .line 161
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    .line 163
    .line 164
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createBarRecord()Lcom/intsig/office/fc/hssf/record/chart/BarRecord;

    .line 165
    .line 166
    .line 167
    move-result-object v0

    .line 168
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    .line 170
    .line 171
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createLegendRecord()Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;

    .line 172
    .line 173
    .line 174
    move-result-object v0

    .line 175
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    .line 177
    .line 178
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createBeginRecord()Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 179
    .line 180
    .line 181
    move-result-object v0

    .line 182
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    .line 184
    .line 185
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createTextRecord()Lcom/intsig/office/fc/hssf/record/chart/TextRecord;

    .line 186
    .line 187
    .line 188
    move-result-object v0

    .line 189
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    .line 191
    .line 192
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createBeginRecord()Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 193
    .line 194
    .line 195
    move-result-object v0

    .line 196
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    .line 198
    .line 199
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createLinkedDataRecord()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 200
    .line 201
    .line 202
    move-result-object v0

    .line 203
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    .line 205
    .line 206
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createEndRecord()Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 207
    .line 208
    .line 209
    move-result-object v0

    .line 210
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    .line 212
    .line 213
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createEndRecord()Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 214
    .line 215
    .line 216
    move-result-object v0

    .line 217
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    .line 219
    .line 220
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createEndRecord()Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 221
    .line 222
    .line 223
    move-result-object v0

    .line 224
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    .line 226
    .line 227
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createEndRecord()Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 228
    .line 229
    .line 230
    move-result-object v0

    .line 231
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    .line 233
    .line 234
    return-void
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private createAxisUsedRecord(S)Lcom/intsig/office/fc/hssf/record/chart/AxisUsedRecord;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/AxisUsedRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/AxisUsedRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/AxisUsedRecord;->setNumAxis(S)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private createBOFRecord()Lcom/intsig/office/fc/hssf/record/BOFRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/BOFRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/BOFRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0x258

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/BOFRecord;->setVersion(I)V

    .line 9
    .line 10
    .line 11
    const/16 v1, 0x14

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/BOFRecord;->setType(I)V

    .line 14
    .line 15
    .line 16
    const/16 v1, 0x1cfe

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/BOFRecord;->setBuild(I)V

    .line 19
    .line 20
    .line 21
    const/16 v1, 0x7cd

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/BOFRecord;->setBuildYear(I)V

    .line 24
    .line 25
    .line 26
    const/16 v1, 0x40c9

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/BOFRecord;->setHistoryBitMask(I)V

    .line 29
    .line 30
    .line 31
    const/16 v1, 0x6a

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/BOFRecord;->setRequiredVersion(I)V

    .line 34
    .line 35
    .line 36
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createBarRecord()Lcom/intsig/office/fc/hssf/record/chart/BarRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/BarRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/BarRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/BarRecord;->setBarSpace(S)V

    .line 8
    .line 9
    .line 10
    const/16 v2, 0x96

    .line 11
    .line 12
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/BarRecord;->setCategorySpace(S)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/BarRecord;->setHorizontal(Z)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/BarRecord;->setStacked(Z)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/BarRecord;->setDisplayAsPercentage(Z)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/BarRecord;->setShadow(Z)V

    .line 25
    .line 26
    .line 27
    return-object v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createBeginRecord()Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private createCategoriesLinkedDataRecord()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;
    .locals 13

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x2

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setLinkType(B)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setReferenceType(B)V

    .line 11
    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setCustomNumberFormat(Z)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setIndexNumberFmtRecord(S)V

    .line 18
    .line 19
    .line 20
    new-instance v12, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 21
    .line 22
    const/4 v3, 0x0

    .line 23
    const/16 v4, 0x1f

    .line 24
    .line 25
    const/4 v5, 0x1

    .line 26
    const/4 v6, 0x1

    .line 27
    const/4 v7, 0x0

    .line 28
    const/4 v8, 0x0

    .line 29
    const/4 v9, 0x0

    .line 30
    const/4 v10, 0x0

    .line 31
    const/4 v11, 0x0

    .line 32
    move-object v2, v12

    .line 33
    invoke-direct/range {v2 .. v11}, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;-><init>(IIIIZZZZI)V

    .line 34
    .line 35
    .line 36
    const/4 v2, 0x1

    .line 37
    new-array v2, v2, [Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 38
    .line 39
    aput-object v12, v2, v1

    .line 40
    .line 41
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setFormulaOfLink([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 42
    .line 43
    .line 44
    return-object v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createCategorySeriesAxisRecord()Lcom/intsig/office/fc/hssf/record/chart/CategorySeriesAxisRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/CategorySeriesAxisRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/CategorySeriesAxisRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/CategorySeriesAxisRecord;->setCrossingPoint(S)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/CategorySeriesAxisRecord;->setLabelFrequency(S)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/CategorySeriesAxisRecord;->setTickMarkFrequency(S)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/CategorySeriesAxisRecord;->setValueAxisCrossing(Z)V

    .line 17
    .line 18
    .line 19
    const/4 v1, 0x0

    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/CategorySeriesAxisRecord;->setCrossesFarRight(Z)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/CategorySeriesAxisRecord;->setReversed(Z)V

    .line 24
    .line 25
    .line 26
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createChartFormatRecord()Lcom/intsig/office/fc/hssf/record/chart/ChartFormatRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/ChartFormatRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/ChartFormatRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/ChartFormatRecord;->setXPosition(I)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/ChartFormatRecord;->setYPosition(I)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/ChartFormatRecord;->setWidth(I)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/ChartFormatRecord;->setHeight(I)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/ChartFormatRecord;->setVaryDisplayPattern(Z)V

    .line 20
    .line 21
    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createChartRecord(IIII)Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;->setX(I)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;->setY(I)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, p3}, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;->setWidth(I)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, p4}, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;->setHeight(I)V

    .line 16
    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method private createDataFormatRecord()Lcom/intsig/office/fc/hssf/record/chart/DataFormatRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/DataFormatRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/DataFormatRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, -0x1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/DataFormatRecord;->setPointNumber(S)V

    .line 8
    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/DataFormatRecord;->setSeriesIndex(S)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/DataFormatRecord;->setSeriesNumber(S)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/DataFormatRecord;->setUseExcel4Colors(Z)V

    .line 18
    .line 19
    .line 20
    return-object v0
    .line 21
.end method

.method private createDefaultTextRecord(S)Lcom/intsig/office/fc/hssf/record/chart/DefaultDataLabelTextPropertiesRecord;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/DefaultDataLabelTextPropertiesRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/DefaultDataLabelTextPropertiesRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/DefaultDataLabelTextPropertiesRecord;->setCategoryDataType(S)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private createDimensionsRecord()Lcom/intsig/office/fc/hssf/record/DimensionsRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setFirstRow(I)V

    .line 8
    .line 9
    .line 10
    const/16 v2, 0x1f

    .line 11
    .line 12
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setLastRow(I)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setFirstCol(S)V

    .line 16
    .line 17
    .line 18
    const/4 v1, 0x1

    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;->setLastCol(S)V

    .line 20
    .line 21
    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createDirectLinkRecord()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setLinkType(B)V

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setReferenceType(B)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setCustomNumberFormat(Z)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setIndexNumberFmtRecord(S)V

    .line 18
    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setFormulaOfLink([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 22
    .line 23
    .line 24
    return-object v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createEndRecord()Lcom/intsig/office/fc/hssf/record/chart/EndRecord;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/EndRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private createFontBasisRecord1()Lcom/intsig/office/fc/hssf/record/chart/FontBasisRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/FontBasisRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/FontBasisRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0x23a0

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/FontBasisRecord;->setXBasis(S)V

    .line 9
    .line 10
    .line 11
    const/16 v1, 0x1608

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/FontBasisRecord;->setYBasis(S)V

    .line 14
    .line 15
    .line 16
    const/16 v1, 0xc8

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/FontBasisRecord;->setHeightBasis(S)V

    .line 19
    .line 20
    .line 21
    const/4 v1, 0x0

    .line 22
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/FontBasisRecord;->setScale(S)V

    .line 23
    .line 24
    .line 25
    const/4 v1, 0x5

    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/FontBasisRecord;->setIndexToFontTable(S)V

    .line 27
    .line 28
    .line 29
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createFontBasisRecord2()Lcom/intsig/office/fc/hssf/record/chart/FontBasisRecord;
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createFontBasisRecord1()Lcom/intsig/office/fc/hssf/record/chart/FontBasisRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x6

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/FontBasisRecord;->setIndexToFontTable(S)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private createFontIndexRecord(I)Lcom/intsig/office/fc/hssf/record/chart/FontIndexRecord;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/FontIndexRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/FontIndexRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    int-to-short p1, p1

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/FontIndexRecord;->setFontIndex(S)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private createFrameRecord1()Lcom/intsig/office/fc/hssf/record/chart/FrameRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/FrameRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/FrameRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/FrameRecord;->setBorderType(S)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/FrameRecord;->setAutoSize(Z)V

    .line 11
    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/FrameRecord;->setAutoPosition(Z)V

    .line 15
    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private createFrameRecord2()Lcom/intsig/office/fc/hssf/record/chart/FrameRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/FrameRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/FrameRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/FrameRecord;->setBorderType(S)V

    .line 8
    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/FrameRecord;->setAutoSize(Z)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/FrameRecord;->setAutoPosition(Z)V

    .line 15
    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private createHCenterRecord()Lcom/intsig/office/fc/hssf/record/HCenterRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/HCenterRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/HCenterRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/HCenterRecord;->setHCenter(Z)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private createLegendRecord()Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0xdd6

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;->setXAxisUpperLeft(I)V

    .line 9
    .line 10
    .line 11
    const/16 v1, 0x61e

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;->setYAxisUpperLeft(I)V

    .line 14
    .line 15
    .line 16
    const/16 v1, 0x1b5

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;->setXSize(I)V

    .line 19
    .line 20
    .line 21
    const/16 v1, 0xd5

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;->setYSize(I)V

    .line 24
    .line 25
    .line 26
    const/4 v1, 0x3

    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;->setType(B)V

    .line 28
    .line 29
    .line 30
    const/4 v1, 0x1

    .line 31
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;->setSpacing(B)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;->setAutoPosition(Z)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;->setAutoSeries(Z)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;->setAutoXPositioning(Z)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;->setAutoYPositioning(Z)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;->setVertical(Z)V

    .line 47
    .line 48
    .line 49
    const/4 v1, 0x0

    .line 50
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;->setDataTable(Z)V

    .line 51
    .line 52
    .line 53
    return-object v0
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createLineFormatRecord(Z)Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->setLineColor(I)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->setLinePattern(S)V

    .line 11
    .line 12
    .line 13
    const/4 v1, -0x1

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->setWeight(S)V

    .line 15
    .line 16
    .line 17
    const/4 v1, 0x1

    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->setAuto(Z)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->setDrawTicks(Z)V

    .line 22
    .line 23
    .line 24
    const/16 p1, 0x4d

    .line 25
    .line 26
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->setColourPaletteIndex(S)V

    .line 27
    .line 28
    .line 29
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private createLineFormatRecord2()Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const v1, 0x808080

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->setLineColor(I)V

    .line 10
    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->setLinePattern(S)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->setWeight(S)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->setAuto(Z)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->setDrawTicks(Z)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->setUnknown(Z)V

    .line 26
    .line 27
    .line 28
    const/16 v1, 0x17

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->setColourPaletteIndex(S)V

    .line 31
    .line 32
    .line 33
    return-object v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createLinkedDataRecord()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setLinkType(B)V

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setReferenceType(B)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setCustomNumberFormat(Z)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setIndexNumberFmtRecord(S)V

    .line 18
    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setFormulaOfLink([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 22
    .line 23
    .line 24
    return-object v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createMSDrawingObjectRecord()Lcom/intsig/office/fc/hssf/record/UnknownRecord;
    .locals 3

    .line 1
    const/16 v0, 0xc8

    .line 2
    .line 3
    new-array v0, v0, [B

    .line 4
    .line 5
    fill-array-data v0, :array_0

    .line 6
    .line 7
    .line 8
    new-instance v1, Lcom/intsig/office/fc/hssf/record/UnknownRecord;

    .line 9
    .line 10
    const/16 v2, 0xec

    .line 11
    .line 12
    invoke-direct {v1, v2, v0}, Lcom/intsig/office/fc/hssf/record/UnknownRecord;-><init>(I[B)V

    .line 13
    .line 14
    .line 15
    return-object v1

    .line 16
    nop

    .line 17
    :array_0
    .array-data 1
        0xft
        0x0t
        0x2t
        -0x10t
        -0x40t
        0x0t
        0x0t
        0x0t
        0x10t
        0x0t
        0x8t
        -0x10t
        0x8t
        0x0t
        0x0t
        0x0t
        0x2t
        0x0t
        0x0t
        0x0t
        0x2t
        0x4t
        0x0t
        0x0t
        0xft
        0x0t
        0x3t
        -0x10t
        -0x58t
        0x0t
        0x0t
        0x0t
        0xft
        0x0t
        0x4t
        -0x10t
        0x28t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x9t
        -0x10t
        0x10t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x2t
        0x0t
        0xat
        -0x10t
        0x8t
        0x0t
        0x0t
        0x0t
        0x0t
        0x4t
        0x0t
        0x0t
        0x5t
        0x0t
        0x0t
        0x0t
        0xft
        0x0t
        0x4t
        -0x10t
        0x70t
        0x0t
        0x0t
        0x0t
        -0x6et
        0xct
        0xat
        -0x10t
        0x8t
        0x0t
        0x0t
        0x0t
        0x2t
        0x4t
        0x0t
        0x0t
        0x0t
        0xat
        0x0t
        0x0t
        -0x6dt
        0x0t
        0xbt
        -0x10t
        0x36t
        0x0t
        0x0t
        0x0t
        0x7ft
        0x0t
        0x4t
        0x1t
        0x4t
        0x1t
        -0x41t
        0x0t
        0x8t
        0x0t
        0x8t
        0x0t
        -0x7ft
        0x1t
        0x4et
        0x0t
        0x0t
        0x8t
        -0x7dt
        0x1t
        0x4dt
        0x0t
        0x0t
        0x8t
        -0x41t
        0x1t
        0x10t
        0x0t
        0x11t
        0x0t
        -0x40t
        0x1t
        0x4dt
        0x0t
        0x0t
        0x8t
        -0x1t
        0x1t
        0x8t
        0x0t
        0x8t
        0x0t
        0x3ft
        0x2t
        0x0t
        0x0t
        0x2t
        0x0t
        -0x41t
        0x3t
        0x0t
        0x0t
        0x8t
        0x0t
        0x0t
        0x0t
        0x10t
        -0x10t
        0x12t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x4t
        0x0t
        -0x40t
        0x2t
        0xat
        0x0t
        -0xct
        0x0t
        0xet
        0x0t
        0x66t
        0x1t
        0x20t
        0x0t
        -0x17t
        0x0t
        0x0t
        0x0t
        0x11t
        -0x10t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private createOBJRecord()Lcom/intsig/office/fc/hssf/record/UnknownRecord;
    .locals 3

    .line 1
    const/16 v0, 0x1a

    .line 2
    .line 3
    new-array v0, v0, [B

    .line 4
    .line 5
    fill-array-data v0, :array_0

    .line 6
    .line 7
    .line 8
    new-instance v1, Lcom/intsig/office/fc/hssf/record/UnknownRecord;

    .line 9
    .line 10
    const/16 v2, 0x5d

    .line 11
    .line 12
    invoke-direct {v1, v2, v0}, Lcom/intsig/office/fc/hssf/record/UnknownRecord;-><init>(I[B)V

    .line 13
    .line 14
    .line 15
    return-object v1

    .line 16
    nop

    .line 17
    :array_0
    .array-data 1
        0x15t
        0x0t
        0x12t
        0x0t
        0x5t
        0x0t
        0x2t
        0x0t
        0x11t
        0x60t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x48t
        0x3t
        -0x79t
        0x3t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private createPlotAreaRecord()Lcom/intsig/office/fc/hssf/record/chart/PlotAreaRecord;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/PlotAreaRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/PlotAreaRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private createPlotGrowthRecord(II)Lcom/intsig/office/fc/hssf/record/chart/PlotGrowthRecord;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/PlotGrowthRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/PlotGrowthRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/PlotGrowthRecord;->setHorizontalScale(I)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hssf/record/chart/PlotGrowthRecord;->setVerticalScale(I)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private createPrintSetupRecord()Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setPaperSize(S)V

    .line 8
    .line 9
    .line 10
    const/16 v2, 0x12

    .line 11
    .line 12
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setScale(S)V

    .line 13
    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setPageStart(S)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setFitWidth(S)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setFitHeight(S)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setLeftToRight(Z)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setLandscape(Z)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setValidSettings(Z)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setNoColor(Z)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setDraft(Z)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setNotes(Z)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setNoOrientation(Z)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setUsePage(Z)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setHResolution(S)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setVResolution(S)V

    .line 53
    .line 54
    .line 55
    const-wide/high16 v1, 0x3fe0000000000000L    # 0.5

    .line 56
    .line 57
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setHeaderMargin(D)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setFooterMargin(D)V

    .line 61
    .line 62
    .line 63
    const/16 v1, 0xf

    .line 64
    .line 65
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;->setCopies(S)V

    .line 66
    .line 67
    .line 68
    return-object v0
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createSCLRecord(SS)Lcom/intsig/office/fc/hssf/record/SCLRecord;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/SCLRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/SCLRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hssf/record/SCLRecord;->setDenominator(S)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/SCLRecord;->setNumerator(S)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private createSeriesIndexRecord(I)Lcom/intsig/office/fc/hssf/record/chart/SeriesIndexRecord;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/SeriesIndexRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/SeriesIndexRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    int-to-short p1, p1

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/SeriesIndexRecord;->setIndex(S)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private createSeriesRecord()Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;->setCategoryDataType(S)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;->setValuesDataType(S)V

    .line 11
    .line 12
    .line 13
    const/16 v2, 0x20

    .line 14
    .line 15
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;->setNumCategories(S)V

    .line 16
    .line 17
    .line 18
    const/16 v2, 0x1f

    .line 19
    .line 20
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;->setNumValues(S)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;->setBubbleSeriesType(S)V

    .line 24
    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;->setNumBubbleValues(S)V

    .line 28
    .line 29
    .line 30
    return-object v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createSeriesToChartGroupRecord()Lcom/intsig/office/fc/hssf/record/chart/SeriesToChartGroupRecord;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/SeriesToChartGroupRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/SeriesToChartGroupRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private createSheetPropsRecord()Lcom/intsig/office/fc/hssf/record/chart/SheetPropertiesRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/SheetPropertiesRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/SheetPropertiesRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/SheetPropertiesRecord;->setChartTypeManuallyFormatted(Z)V

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/SheetPropertiesRecord;->setPlotVisibleOnly(Z)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/SheetPropertiesRecord;->setDoNotSizeWithWindow(Z)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/SheetPropertiesRecord;->setDefaultPlotDimensions(Z)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/SheetPropertiesRecord;->setAutoPlotArea(Z)V

    .line 21
    .line 22
    .line 23
    return-object v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createTextRecord()Lcom/intsig/office/fc/hssf/record/chart/TextRecord;
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x2

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setHorizontalAlignment(B)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setVerticalAlignment(B)V

    .line 11
    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setDisplayMode(S)V

    .line 15
    .line 16
    .line 17
    const/4 v2, 0x0

    .line 18
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setRgbColor(I)V

    .line 19
    .line 20
    .line 21
    const/16 v3, -0x25

    .line 22
    .line 23
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setX(I)V

    .line 24
    .line 25
    .line 26
    const/16 v3, -0x3c

    .line 27
    .line 28
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setY(I)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setWidth(I)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setHeight(I)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setAutoColor(Z)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowKey(Z)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowValue(Z)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setVertical(Z)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setAutoGeneratedText(Z)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setGenerated(Z)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setAutoLabelDeleted(Z)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setAutoBackground(Z)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setRotation(S)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowCategoryLabelAsPercentage(Z)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowValueAsPercentage(Z)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowBubbleSizes(Z)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowLabel(Z)V

    .line 74
    .line 75
    .line 76
    const/16 v1, 0x4d

    .line 77
    .line 78
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setIndexOfColorValue(S)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setDataLabelPlacement(S)V

    .line 82
    .line 83
    .line 84
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setTextRotation(S)V

    .line 85
    .line 86
    .line 87
    return-object v0
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private createTickRecord1()Lcom/intsig/office/fc/hssf/record/chart/TickRecord;
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x2

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;->setMajorTickType(B)V

    .line 8
    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;->setMinorTickType(B)V

    .line 12
    .line 13
    .line 14
    const/4 v2, 0x3

    .line 15
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;->setLabelPosition(B)V

    .line 16
    .line 17
    .line 18
    const/4 v2, 0x1

    .line 19
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;->setBackground(B)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;->setLabelColorRgb(I)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;->setZero1(I)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;->setZero2(I)V

    .line 29
    .line 30
    .line 31
    const/16 v3, 0x2d

    .line 32
    .line 33
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;->setZero3(S)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;->setAutorotate(Z)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;->setAutoTextBackground(Z)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;->setRotation(S)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;->setAutorotate(Z)V

    .line 46
    .line 47
    .line 48
    const/16 v1, 0x4d

    .line 49
    .line 50
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;->setTickColor(S)V

    .line 51
    .line 52
    .line 53
    return-object v0
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createTickRecord2()Lcom/intsig/office/fc/hssf/record/chart/TickRecord;
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createTickRecord1()Lcom/intsig/office/fc/hssf/record/chart/TickRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;->setZero3(S)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private createTitleLinkedDataRecord()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setLinkType(B)V

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setReferenceType(B)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setCustomNumberFormat(Z)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setIndexNumberFmtRecord(S)V

    .line 18
    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setFormulaOfLink([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 22
    .line 23
    .line 24
    return-object v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createUnitsRecord()Lcom/intsig/office/fc/hssf/record/chart/UnitsRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/UnitsRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/UnitsRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/UnitsRecord;->setUnits(S)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private createUnknownTextRecord()Lcom/intsig/office/fc/hssf/record/chart/TextRecord;
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x2

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setHorizontalAlignment(B)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setVerticalAlignment(B)V

    .line 11
    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setDisplayMode(S)V

    .line 15
    .line 16
    .line 17
    const/4 v2, 0x0

    .line 18
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setRgbColor(I)V

    .line 19
    .line 20
    .line 21
    const/16 v3, -0x25

    .line 22
    .line 23
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setX(I)V

    .line 24
    .line 25
    .line 26
    const/16 v3, -0x3c

    .line 27
    .line 28
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setY(I)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setWidth(I)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setHeight(I)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setAutoColor(Z)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowKey(Z)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowValue(Z)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setVertical(Z)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setAutoGeneratedText(Z)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setGenerated(Z)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setAutoLabelDeleted(Z)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setAutoBackground(Z)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setRotation(S)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowCategoryLabelAsPercentage(Z)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowValueAsPercentage(Z)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowBubbleSizes(Z)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setShowLabel(Z)V

    .line 74
    .line 75
    .line 76
    const/16 v1, 0x4d

    .line 77
    .line 78
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setIndexOfColorValue(S)V

    .line 79
    .line 80
    .line 81
    const/16 v1, 0x2b50

    .line 82
    .line 83
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setDataLabelPlacement(S)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;->setTextRotation(S)V

    .line 87
    .line 88
    .line 89
    return-object v0
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private createVCenterRecord()Lcom/intsig/office/fc/hssf/record/VCenterRecord;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/VCenterRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/VCenterRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/VCenterRecord;->setVCenter(Z)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private createValueRangeRecord()Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const-wide/16 v1, 0x0

    .line 7
    .line 8
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setMinimumAxisValue(D)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setMaximumAxisValue(D)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setMajorIncrement(D)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setMinorIncrement(D)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setCategoryAxisCross(D)V

    .line 21
    .line 22
    .line 23
    const/4 v1, 0x1

    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setAutomaticMinimum(Z)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setAutomaticMaximum(Z)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setAutomaticMajor(Z)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setAutomaticMinor(Z)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setAutomaticCategoryCrossing(Z)V

    .line 37
    .line 38
    .line 39
    const/4 v2, 0x0

    .line 40
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setLogarithmicScale(Z)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setValuesInReverse(Z)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setCrossCategoryAxisAtMaximum(Z)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setReserved(Z)V

    .line 50
    .line 51
    .line 52
    return-object v0
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private createValuesLinkedDataRecord()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;
    .locals 14

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setLinkType(B)V

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x2

    .line 11
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setReferenceType(B)V

    .line 12
    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setCustomNumberFormat(Z)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setIndexNumberFmtRecord(S)V

    .line 19
    .line 20
    .line 21
    new-instance v13, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 22
    .line 23
    const/4 v4, 0x0

    .line 24
    const/16 v5, 0x1f

    .line 25
    .line 26
    const/4 v6, 0x0

    .line 27
    const/4 v7, 0x0

    .line 28
    const/4 v8, 0x0

    .line 29
    const/4 v9, 0x0

    .line 30
    const/4 v10, 0x0

    .line 31
    const/4 v11, 0x0

    .line 32
    const/4 v12, 0x0

    .line 33
    move-object v3, v13

    .line 34
    invoke-direct/range {v3 .. v12}, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;-><init>(IIIIZZZZI)V

    .line 35
    .line 36
    .line 37
    new-array v1, v1, [Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 38
    .line 39
    aput-object v13, v1, v2

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->setFormulaOfLink([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 42
    .line 43
    .line 44
    return-object v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static getSheetCharts(Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;)[Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;
    .locals 11

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 7
    .line 8
    .line 9
    move-result-object p0

    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    .line 11
    .line 12
    .line 13
    move-result-object p0

    .line 14
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object p0

    .line 18
    const/4 v1, 0x0

    .line 19
    move-object v2, v1

    .line 20
    move-object v3, v2

    .line 21
    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 22
    .line 23
    .line 24
    move-result v4

    .line 25
    if-eqz v4, :cond_a

    .line 26
    .line 27
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v4

    .line 31
    check-cast v4, Lcom/intsig/office/fc/hssf/record/RecordBase;

    .line 32
    .line 33
    instance-of v5, v4, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 34
    .line 35
    if-eqz v5, :cond_1

    .line 36
    .line 37
    new-instance v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;

    .line 38
    .line 39
    invoke-direct {v2, v1, v1, v1, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)V

    .line 40
    .line 41
    .line 42
    check-cast v4, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 43
    .line 44
    invoke-virtual {v2, v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->setChartRecord(Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;)V

    .line 45
    .line 46
    .line 47
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    .line 49
    .line 50
    move-object v3, v1

    .line 51
    goto :goto_0

    .line 52
    :cond_1
    instance-of v5, v4, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;

    .line 53
    .line 54
    if-eqz v5, :cond_2

    .line 55
    .line 56
    check-cast v4, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;

    .line 57
    .line 58
    iput-object v4, v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->legendRecord:Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_2
    instance-of v5, v4, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 62
    .line 63
    if-eqz v5, :cond_3

    .line 64
    .line 65
    new-instance v3, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 66
    .line 67
    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    .line 69
    .line 70
    check-cast v4, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 71
    .line 72
    invoke-direct {v3, v2, v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;)V

    .line 73
    .line 74
    .line 75
    iget-object v4, v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 76
    .line 77
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_3
    instance-of v5, v4, Lcom/intsig/office/fc/hssf/record/chart/ChartTitleFormatRecord;

    .line 82
    .line 83
    if-eqz v5, :cond_4

    .line 84
    .line 85
    check-cast v4, Lcom/intsig/office/fc/hssf/record/chart/ChartTitleFormatRecord;

    .line 86
    .line 87
    iput-object v4, v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartTitleFormat:Lcom/intsig/office/fc/hssf/record/chart/ChartTitleFormatRecord;

    .line 88
    .line 89
    goto :goto_0

    .line 90
    :cond_4
    instance-of v5, v4, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 91
    .line 92
    if-eqz v5, :cond_5

    .line 93
    .line 94
    check-cast v4, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 95
    .line 96
    iget-object v5, v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->legendRecord:Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;

    .line 97
    .line 98
    if-nez v5, :cond_0

    .line 99
    .line 100
    iget-object v5, v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 101
    .line 102
    invoke-interface {v5}, Ljava/util/List;->size()I

    .line 103
    .line 104
    .line 105
    move-result v5

    .line 106
    if-lez v5, :cond_0

    .line 107
    .line 108
    iget-object v5, v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 109
    .line 110
    invoke-interface {v5}, Ljava/util/List;->size()I

    .line 111
    .line 112
    .line 113
    move-result v6

    .line 114
    add-int/lit8 v6, v6, -0x1

    .line 115
    .line 116
    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 117
    .line 118
    .line 119
    move-result-object v5

    .line 120
    check-cast v5, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 121
    .line 122
    invoke-static {v5, v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->〇080(Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;)V

    .line 123
    .line 124
    .line 125
    goto :goto_0

    .line 126
    :cond_5
    instance-of v5, v4, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 127
    .line 128
    if-eqz v5, :cond_6

    .line 129
    .line 130
    check-cast v4, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 131
    .line 132
    if-eqz v3, :cond_0

    .line 133
    .line 134
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->insertData(Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;)V

    .line 135
    .line 136
    .line 137
    goto :goto_0

    .line 138
    :cond_6
    instance-of v5, v4, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;

    .line 139
    .line 140
    if-eqz v5, :cond_7

    .line 141
    .line 142
    iget-object v5, v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->valueRanges:Ljava/util/List;

    .line 143
    .line 144
    check-cast v4, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;

    .line 145
    .line 146
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    .line 148
    .line 149
    goto/16 :goto_0

    .line 150
    .line 151
    :cond_7
    instance-of v5, v4, Lcom/intsig/office/fc/hssf/record/Record;

    .line 152
    .line 153
    if-eqz v5, :cond_0

    .line 154
    .line 155
    if-eqz v2, :cond_0

    .line 156
    .line 157
    check-cast v4, Lcom/intsig/office/fc/hssf/record/Record;

    .line 158
    .line 159
    invoke-static {}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;->values()[Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

    .line 160
    .line 161
    .line 162
    move-result-object v5

    .line 163
    array-length v6, v5

    .line 164
    const/4 v7, 0x0

    .line 165
    :goto_1
    if-ge v7, v6, :cond_0

    .line 166
    .line 167
    aget-object v8, v5, v7

    .line 168
    .line 169
    sget-object v9, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;->Unknown:Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

    .line 170
    .line 171
    if-ne v8, v9, :cond_8

    .line 172
    .line 173
    goto :goto_2

    .line 174
    :cond_8
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 175
    .line 176
    .line 177
    move-result v9

    .line 178
    invoke-virtual {v8}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;->getSid()S

    .line 179
    .line 180
    .line 181
    move-result v10

    .line 182
    if-ne v9, v10, :cond_9

    .line 183
    .line 184
    iput-object v8, v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->type:Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

    .line 185
    .line 186
    goto/16 :goto_0

    .line 187
    .line 188
    :cond_9
    :goto_2
    add-int/lit8 v7, v7, 0x1

    .line 189
    .line 190
    goto :goto_1

    .line 191
    :cond_a
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 192
    .line 193
    .line 194
    move-result p0

    .line 195
    new-array p0, p0, [Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;

    .line 196
    .line 197
    invoke-interface {v0, p0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 198
    .line 199
    .line 200
    move-result-object p0

    .line 201
    check-cast p0, [Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;

    .line 202
    .line 203
    return-object p0
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method


# virtual methods
.method public createBarChart(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;)V
    .locals 5

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createMSDrawingObjectRecord()Lcom/intsig/office/fc/hssf/record/UnknownRecord;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createOBJRecord()Lcom/intsig/office/fc/hssf/record/UnknownRecord;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createBOFRecord()Lcom/intsig/office/fc/hssf/record/BOFRecord;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    .line 26
    .line 27
    new-instance v1, Lcom/intsig/office/fc/hssf/record/HeaderRecord;

    .line 28
    .line 29
    const-string v2, ""

    .line 30
    .line 31
    invoke-direct {v1, v2}, Lcom/intsig/office/fc/hssf/record/HeaderRecord;-><init>(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    new-instance v1, Lcom/intsig/office/fc/hssf/record/FooterRecord;

    .line 38
    .line 39
    invoke-direct {v1, v2}, Lcom/intsig/office/fc/hssf/record/FooterRecord;-><init>(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createHCenterRecord()Lcom/intsig/office/fc/hssf/record/HCenterRecord;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    .line 51
    .line 52
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createVCenterRecord()Lcom/intsig/office/fc/hssf/record/VCenterRecord;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createPrintSetupRecord()Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createFontBasisRecord1()Lcom/intsig/office/fc/hssf/record/chart/FontBasisRecord;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    .line 72
    .line 73
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createFontBasisRecord2()Lcom/intsig/office/fc/hssf/record/chart/FontBasisRecord;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    new-instance v1, Lcom/intsig/office/fc/hssf/record/ProtectRecord;

    .line 81
    .line 82
    const/4 v2, 0x0

    .line 83
    invoke-direct {v1, v2}, Lcom/intsig/office/fc/hssf/record/ProtectRecord;-><init>(Z)V

    .line 84
    .line 85
    .line 86
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    .line 88
    .line 89
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createUnitsRecord()Lcom/intsig/office/fc/hssf/record/chart/UnitsRecord;

    .line 90
    .line 91
    .line 92
    move-result-object v1

    .line 93
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    .line 95
    .line 96
    const v1, 0x1d06658

    .line 97
    .line 98
    .line 99
    const v3, 0x1226640

    .line 100
    .line 101
    .line 102
    invoke-direct {p0, v2, v2, v1, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createChartRecord(IIII)Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 103
    .line 104
    .line 105
    move-result-object v1

    .line 106
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    .line 108
    .line 109
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createBeginRecord()Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 110
    .line 111
    .line 112
    move-result-object v1

    .line 113
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    .line 115
    .line 116
    const/4 v1, 0x1

    .line 117
    invoke-direct {p0, v1, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createSCLRecord(SS)Lcom/intsig/office/fc/hssf/record/SCLRecord;

    .line 118
    .line 119
    .line 120
    move-result-object v2

    .line 121
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    .line 123
    .line 124
    const/high16 v2, 0x10000

    .line 125
    .line 126
    invoke-direct {p0, v2, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createPlotGrowthRecord(II)Lcom/intsig/office/fc/hssf/record/chart/PlotGrowthRecord;

    .line 127
    .line 128
    .line 129
    move-result-object v2

    .line 130
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    .line 132
    .line 133
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createFrameRecord1()Lcom/intsig/office/fc/hssf/record/chart/FrameRecord;

    .line 134
    .line 135
    .line 136
    move-result-object v2

    .line 137
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    .line 139
    .line 140
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createBeginRecord()Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 141
    .line 142
    .line 143
    move-result-object v2

    .line 144
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    .line 146
    .line 147
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createLineFormatRecord(Z)Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;

    .line 148
    .line 149
    .line 150
    move-result-object v2

    .line 151
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    .line 153
    .line 154
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createAreaFormatRecord1()Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 155
    .line 156
    .line 157
    move-result-object v2

    .line 158
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    .line 160
    .line 161
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createEndRecord()Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 162
    .line 163
    .line 164
    move-result-object v2

    .line 165
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    .line 167
    .line 168
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createSeriesRecord()Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 169
    .line 170
    .line 171
    move-result-object v2

    .line 172
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    .line 174
    .line 175
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createBeginRecord()Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 176
    .line 177
    .line 178
    move-result-object v2

    .line 179
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    .line 181
    .line 182
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createTitleLinkedDataRecord()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 183
    .line 184
    .line 185
    move-result-object v2

    .line 186
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    .line 188
    .line 189
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createValuesLinkedDataRecord()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 190
    .line 191
    .line 192
    move-result-object v2

    .line 193
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    .line 195
    .line 196
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createCategoriesLinkedDataRecord()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 197
    .line 198
    .line 199
    move-result-object v2

    .line 200
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    .line 202
    .line 203
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createDataFormatRecord()Lcom/intsig/office/fc/hssf/record/chart/DataFormatRecord;

    .line 204
    .line 205
    .line 206
    move-result-object v2

    .line 207
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    .line 209
    .line 210
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createSeriesToChartGroupRecord()Lcom/intsig/office/fc/hssf/record/chart/SeriesToChartGroupRecord;

    .line 211
    .line 212
    .line 213
    move-result-object v2

    .line 214
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    .line 216
    .line 217
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createEndRecord()Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 218
    .line 219
    .line 220
    move-result-object v2

    .line 221
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    .line 223
    .line 224
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createSheetPropsRecord()Lcom/intsig/office/fc/hssf/record/chart/SheetPropertiesRecord;

    .line 225
    .line 226
    .line 227
    move-result-object v2

    .line 228
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    .line 230
    .line 231
    const/4 v2, 0x2

    .line 232
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createDefaultTextRecord(S)Lcom/intsig/office/fc/hssf/record/chart/DefaultDataLabelTextPropertiesRecord;

    .line 233
    .line 234
    .line 235
    move-result-object v3

    .line 236
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    .line 238
    .line 239
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createAllTextRecord()Lcom/intsig/office/fc/hssf/record/chart/TextRecord;

    .line 240
    .line 241
    .line 242
    move-result-object v3

    .line 243
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    .line 245
    .line 246
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createBeginRecord()Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 247
    .line 248
    .line 249
    move-result-object v3

    .line 250
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    .line 252
    .line 253
    const/4 v3, 0x5

    .line 254
    invoke-direct {p0, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createFontIndexRecord(I)Lcom/intsig/office/fc/hssf/record/chart/FontIndexRecord;

    .line 255
    .line 256
    .line 257
    move-result-object v3

    .line 258
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    .line 260
    .line 261
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createDirectLinkRecord()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 262
    .line 263
    .line 264
    move-result-object v3

    .line 265
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    .line 267
    .line 268
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createEndRecord()Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 269
    .line 270
    .line 271
    move-result-object v3

    .line 272
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 273
    .line 274
    .line 275
    const/4 v3, 0x3

    .line 276
    invoke-direct {p0, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createDefaultTextRecord(S)Lcom/intsig/office/fc/hssf/record/chart/DefaultDataLabelTextPropertiesRecord;

    .line 277
    .line 278
    .line 279
    move-result-object v4

    .line 280
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    .line 282
    .line 283
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createUnknownTextRecord()Lcom/intsig/office/fc/hssf/record/chart/TextRecord;

    .line 284
    .line 285
    .line 286
    move-result-object v4

    .line 287
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    .line 289
    .line 290
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createBeginRecord()Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 291
    .line 292
    .line 293
    move-result-object v4

    .line 294
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 295
    .line 296
    .line 297
    const/4 v4, 0x6

    .line 298
    invoke-direct {p0, v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createFontIndexRecord(I)Lcom/intsig/office/fc/hssf/record/chart/FontIndexRecord;

    .line 299
    .line 300
    .line 301
    move-result-object v4

    .line 302
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 303
    .line 304
    .line 305
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createDirectLinkRecord()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 306
    .line 307
    .line 308
    move-result-object v4

    .line 309
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 310
    .line 311
    .line 312
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createEndRecord()Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 313
    .line 314
    .line 315
    move-result-object v4

    .line 316
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 317
    .line 318
    .line 319
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createAxisUsedRecord(S)Lcom/intsig/office/fc/hssf/record/chart/AxisUsedRecord;

    .line 320
    .line 321
    .line 322
    move-result-object v4

    .line 323
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 324
    .line 325
    .line 326
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createAxisRecords(Ljava/util/List;)V

    .line 327
    .line 328
    .line 329
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createEndRecord()Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 330
    .line 331
    .line 332
    move-result-object v4

    .line 333
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    .line 335
    .line 336
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createDimensionsRecord()Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    .line 337
    .line 338
    .line 339
    move-result-object v4

    .line 340
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 341
    .line 342
    .line 343
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createSeriesIndexRecord(I)Lcom/intsig/office/fc/hssf/record/chart/SeriesIndexRecord;

    .line 344
    .line 345
    .line 346
    move-result-object v2

    .line 347
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348
    .line 349
    .line 350
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createSeriesIndexRecord(I)Lcom/intsig/office/fc/hssf/record/chart/SeriesIndexRecord;

    .line 351
    .line 352
    .line 353
    move-result-object v1

    .line 354
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355
    .line 356
    .line 357
    invoke-direct {p0, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->createSeriesIndexRecord(I)Lcom/intsig/office/fc/hssf/record/chart/SeriesIndexRecord;

    .line 358
    .line 359
    .line 360
    move-result-object v1

    .line 361
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 362
    .line 363
    .line 364
    sget-object v1, Lcom/intsig/office/fc/hssf/record/EOFRecord;->instance:Lcom/intsig/office/fc/hssf/record/EOFRecord;

    .line 365
    .line 366
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    .line 368
    .line 369
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->insertChartRecords(Ljava/util/List;)V

    .line 370
    .line 371
    .line 372
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->insertChartRecord()V

    .line 373
    .line 374
    .line 375
    return-void
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
.end method

.method public createSeries()Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->sheet:Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    const/4 v3, 0x0

    .line 21
    const/4 v4, -0x1

    .line 22
    const/4 v5, 0x0

    .line 23
    const/4 v6, 0x0

    .line 24
    const/4 v7, 0x0

    .line 25
    const/4 v8, -0x1

    .line 26
    const/4 v9, -0x1

    .line 27
    const/4 v10, -0x1

    .line 28
    const/4 v11, -0x1

    .line 29
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 30
    .line 31
    .line 32
    move-result v12

    .line 33
    const/4 v13, 0x1

    .line 34
    if-eqz v12, :cond_7

    .line 35
    .line 36
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v12

    .line 40
    check-cast v12, Lcom/intsig/office/fc/hssf/record/RecordBase;

    .line 41
    .line 42
    add-int/2addr v3, v13

    .line 43
    instance-of v14, v12, Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 44
    .line 45
    if-eqz v14, :cond_1

    .line 46
    .line 47
    add-int/lit8 v5, v5, 0x1

    .line 48
    .line 49
    goto :goto_2

    .line 50
    :cond_1
    instance-of v14, v12, Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 51
    .line 52
    if-eqz v14, :cond_4

    .line 53
    .line 54
    add-int/lit8 v5, v5, -0x1

    .line 55
    .line 56
    if-ne v8, v5, :cond_3

    .line 57
    .line 58
    if-nez v6, :cond_2

    .line 59
    .line 60
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    move v10, v3

    .line 64
    const/4 v6, 0x1

    .line 65
    goto :goto_1

    .line 66
    :cond_2
    move v10, v3

    .line 67
    :goto_1
    const/4 v8, -0x1

    .line 68
    :cond_3
    if-ne v11, v5, :cond_4

    .line 69
    .line 70
    goto :goto_4

    .line 71
    :cond_4
    :goto_2
    instance-of v13, v12, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 72
    .line 73
    if-eqz v13, :cond_5

    .line 74
    .line 75
    iget-object v13, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartRecord:Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 76
    .line 77
    if-ne v12, v13, :cond_6

    .line 78
    .line 79
    move v9, v3

    .line 80
    move v11, v5

    .line 81
    goto :goto_3

    .line 82
    :cond_5
    instance-of v13, v12, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 83
    .line 84
    if-eqz v13, :cond_6

    .line 85
    .line 86
    if-eq v9, v4, :cond_6

    .line 87
    .line 88
    add-int/lit8 v7, v7, 0x1

    .line 89
    .line 90
    move v8, v5

    .line 91
    :cond_6
    :goto_3
    if-eq v8, v4, :cond_0

    .line 92
    .line 93
    if-nez v6, :cond_0

    .line 94
    .line 95
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    .line 97
    .line 98
    goto :goto_0

    .line 99
    :cond_7
    :goto_4
    const/4 v2, 0x0

    .line 100
    if-ne v10, v4, :cond_8

    .line 101
    .line 102
    return-object v2

    .line 103
    :cond_8
    add-int/2addr v10, v13

    .line 104
    new-instance v3, Ljava/util/ArrayList;

    .line 105
    .line 106
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 107
    .line 108
    .line 109
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 110
    .line 111
    .line 112
    move-result-object v0

    .line 113
    move-object v4, v2

    .line 114
    :cond_9
    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 115
    .line 116
    .line 117
    move-result v5

    .line 118
    if-eqz v5, :cond_12

    .line 119
    .line 120
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 121
    .line 122
    .line 123
    move-result-object v5

    .line 124
    check-cast v5, Lcom/intsig/office/fc/hssf/record/RecordBase;

    .line 125
    .line 126
    instance-of v6, v5, Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 127
    .line 128
    if-eqz v6, :cond_a

    .line 129
    .line 130
    new-instance v5, Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 131
    .line 132
    invoke-direct {v5}, Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;-><init>()V

    .line 133
    .line 134
    .line 135
    goto :goto_6

    .line 136
    :cond_a
    instance-of v6, v5, Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 137
    .line 138
    if-eqz v6, :cond_b

    .line 139
    .line 140
    new-instance v5, Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 141
    .line 142
    invoke-direct {v5}, Lcom/intsig/office/fc/hssf/record/chart/EndRecord;-><init>()V

    .line 143
    .line 144
    .line 145
    goto :goto_6

    .line 146
    :cond_b
    instance-of v6, v5, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 147
    .line 148
    if-eqz v6, :cond_c

    .line 149
    .line 150
    check-cast v5, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 151
    .line 152
    invoke-virtual {v5}, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;->clone()Ljava/lang/Object;

    .line 153
    .line 154
    .line 155
    move-result-object v4

    .line 156
    move-object v5, v4

    .line 157
    check-cast v5, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 158
    .line 159
    new-instance v4, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 160
    .line 161
    invoke-direct {v4, p0, v5}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;)V

    .line 162
    .line 163
    .line 164
    goto :goto_6

    .line 165
    :cond_c
    instance-of v6, v5, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 166
    .line 167
    if-eqz v6, :cond_d

    .line 168
    .line 169
    check-cast v5, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 170
    .line 171
    invoke-virtual {v5}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->clone()Ljava/lang/Object;

    .line 172
    .line 173
    .line 174
    move-result-object v5

    .line 175
    check-cast v5, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 176
    .line 177
    if-eqz v4, :cond_11

    .line 178
    .line 179
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->insertData(Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;)V

    .line 180
    .line 181
    .line 182
    goto :goto_6

    .line 183
    :cond_d
    instance-of v6, v5, Lcom/intsig/office/fc/hssf/record/chart/DataFormatRecord;

    .line 184
    .line 185
    if-eqz v6, :cond_e

    .line 186
    .line 187
    check-cast v5, Lcom/intsig/office/fc/hssf/record/chart/DataFormatRecord;

    .line 188
    .line 189
    invoke-virtual {v5}, Lcom/intsig/office/fc/hssf/record/chart/DataFormatRecord;->clone()Ljava/lang/Object;

    .line 190
    .line 191
    .line 192
    move-result-object v5

    .line 193
    check-cast v5, Lcom/intsig/office/fc/hssf/record/chart/DataFormatRecord;

    .line 194
    .line 195
    int-to-short v6, v7

    .line 196
    invoke-virtual {v5, v6}, Lcom/intsig/office/fc/hssf/record/chart/DataFormatRecord;->setSeriesIndex(S)V

    .line 197
    .line 198
    .line 199
    invoke-virtual {v5, v6}, Lcom/intsig/office/fc/hssf/record/chart/DataFormatRecord;->setSeriesNumber(S)V

    .line 200
    .line 201
    .line 202
    goto :goto_6

    .line 203
    :cond_e
    instance-of v6, v5, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 204
    .line 205
    if-eqz v6, :cond_f

    .line 206
    .line 207
    check-cast v5, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 208
    .line 209
    invoke-virtual {v5}, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;->clone()Ljava/lang/Object;

    .line 210
    .line 211
    .line 212
    move-result-object v5

    .line 213
    check-cast v5, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 214
    .line 215
    if-eqz v4, :cond_11

    .line 216
    .line 217
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->setSeriesTitleText(Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;)V

    .line 218
    .line 219
    .line 220
    goto :goto_6

    .line 221
    :cond_f
    instance-of v6, v5, Lcom/intsig/office/fc/hssf/record/Record;

    .line 222
    .line 223
    if-eqz v6, :cond_10

    .line 224
    .line 225
    check-cast v5, Lcom/intsig/office/fc/hssf/record/Record;

    .line 226
    .line 227
    invoke-virtual {v5}, Lcom/intsig/office/fc/hssf/record/Record;->clone()Ljava/lang/Object;

    .line 228
    .line 229
    .line 230
    move-result-object v5

    .line 231
    check-cast v5, Lcom/intsig/office/fc/hssf/record/Record;

    .line 232
    .line 233
    goto :goto_6

    .line 234
    :cond_10
    move-object v5, v2

    .line 235
    :cond_11
    :goto_6
    if-eqz v5, :cond_9

    .line 236
    .line 237
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    .line 239
    .line 240
    goto :goto_5

    .line 241
    :cond_12
    if-nez v4, :cond_13

    .line 242
    .line 243
    return-object v2

    .line 244
    :cond_13
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 245
    .line 246
    .line 247
    move-result-object v0

    .line 248
    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 249
    .line 250
    .line 251
    move-result v2

    .line 252
    if-eqz v2, :cond_14

    .line 253
    .line 254
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 255
    .line 256
    .line 257
    move-result-object v2

    .line 258
    check-cast v2, Lcom/intsig/office/fc/hssf/record/RecordBase;

    .line 259
    .line 260
    add-int/lit8 v3, v10, 0x1

    .line 261
    .line 262
    invoke-interface {v1, v10, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 263
    .line 264
    .line 265
    move v10, v3

    .line 266
    goto :goto_7

    .line 267
    :cond_14
    return-object v4
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public getChartHeight()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartRecord:Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;->getHeight()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getChartRecord()Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartRecord:Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getChartTitleFormat()Lcom/intsig/office/fc/hssf/record/chart/ChartTitleFormatRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartTitleFormat:Lcom/intsig/office/fc/hssf/record/chart/ChartTitleFormatRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getChartWidth()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartRecord:Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;->getWidth()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getChartX()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartRecord:Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;->getX()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getChartY()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartRecord:Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;->getY()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLegendRecord()Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->legendRecord:Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMarginColorFormat()Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->marginColorFormat:Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSeries()[Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    new-array v1, v1, [Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, [Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSeriesBackgroundColorFormat()Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->seriesBackgroundColorFormat:Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSeriesText()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartSeriesText:Ljava/util/Map;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getType()Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->type:Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getValueRangeRecord()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->valueRanges:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public removeSeries(Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->series:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setChartHeight(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartRecord:Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;->setHeight(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setChartRecord(Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartRecord:Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setChartWidth(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartRecord:Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;->setWidth(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setChartX(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartRecord:Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;->setX(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setChartY(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->chartRecord:Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;->setY(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setValueRange(ILjava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->valueRanges:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;

    .line 8
    .line 9
    if-nez p1, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    if-eqz p2, :cond_1

    .line 13
    .line 14
    invoke-virtual {p2}, Ljava/lang/Double;->isNaN()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setAutomaticMinimum(Z)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    .line 22
    .line 23
    .line 24
    move-result-wide v0

    .line 25
    invoke-virtual {p1, v0, v1}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setMinimumAxisValue(D)V

    .line 26
    .line 27
    .line 28
    :cond_1
    if-eqz p3, :cond_2

    .line 29
    .line 30
    invoke-virtual {p3}, Ljava/lang/Double;->isNaN()Z

    .line 31
    .line 32
    .line 33
    move-result p2

    .line 34
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setAutomaticMaximum(Z)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p3}, Ljava/lang/Double;->doubleValue()D

    .line 38
    .line 39
    .line 40
    move-result-wide p2

    .line 41
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setMaximumAxisValue(D)V

    .line 42
    .line 43
    .line 44
    :cond_2
    if-eqz p4, :cond_3

    .line 45
    .line 46
    invoke-virtual {p4}, Ljava/lang/Double;->isNaN()Z

    .line 47
    .line 48
    .line 49
    move-result p2

    .line 50
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setAutomaticMajor(Z)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {p4}, Ljava/lang/Double;->doubleValue()D

    .line 54
    .line 55
    .line 56
    move-result-wide p2

    .line 57
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setMajorIncrement(D)V

    .line 58
    .line 59
    .line 60
    :cond_3
    if-eqz p5, :cond_4

    .line 61
    .line 62
    invoke-virtual {p5}, Ljava/lang/Double;->isNaN()Z

    .line 63
    .line 64
    .line 65
    move-result p2

    .line 66
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setAutomaticMinor(Z)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {p5}, Ljava/lang/Double;->doubleValue()D

    .line 70
    .line 71
    .line 72
    move-result-wide p2

    .line 73
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->setMinorIncrement(D)V

    .line 74
    .line 75
    .line 76
    :cond_4
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

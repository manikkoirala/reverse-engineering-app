.class public final Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;
.super Ljava/lang/Object;
.source "HSSFSheet.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/Sheet;


# static fields
.field private static final DEBUG:I

.field public static final INITIAL_CAPACITY:I = 0x14

.field private static final log:Lcom/intsig/office/fc/util/POILogger;


# instance fields
.field protected final _book:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

.field private _firstrow:I

.field private _lastrow:I

.field private _patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

.field private final _rows:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;",
            ">;"
        }
    .end annotation
.end field

.field private final _sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

.field protected final _workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

.field private column_activecell:I

.field private isInitForDraw:Z

.field paneInformation:Lcom/intsig/office/fc/hssf/util/HSSFPaneInformation;

.field private row_activecell:I

.field private scrollX:I

.field private scrollY:I

.field private zoom:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lcom/intsig/office/fc/util/POILogger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 8
    .line 9
    sget v0, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 10
    .line 11
    sput v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->DEBUG:I

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x3f800000    # 1.0f

    .line 2
    iput v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->zoom:F

    const/4 v0, -0x1

    .line 3
    iput v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->row_activecell:I

    .line 4
    iput v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->column_activecell:I

    .line 5
    invoke-static {}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 6
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    .line 7
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_book:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;Lcom/intsig/office/fc/hssf/model/InternalSheet;)V
    .locals 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x3f800000    # 1.0f

    .line 10
    iput v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->zoom:F

    const/4 v0, -0x1

    .line 11
    iput v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->row_activecell:I

    .line 12
    iput v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->column_activecell:I

    .line 13
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 14
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    .line 15
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_book:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 17
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setPropertiesFromSheet(Lcom/intsig/office/fc/hssf/model/InternalSheet;)V

    return-void
.end method

.method private addRow(Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getRowNum()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v0, v1, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    if-eqz p2, :cond_0

    .line 15
    .line 16
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getRowRecord()Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->addRow(Lcom/intsig/office/fc/hssf/record/RowRecord;)V

    .line 23
    .line 24
    .line 25
    :cond_0
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    .line 26
    .line 27
    invoke-virtual {p2}, Ljava/util/TreeMap;->size()I

    .line 28
    .line 29
    .line 30
    move-result p2

    .line 31
    const/4 v0, 0x1

    .line 32
    if-ne p2, v0, :cond_1

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    const/4 v0, 0x0

    .line 36
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getRowNum()I

    .line 37
    .line 38
    .line 39
    move-result p2

    .line 40
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getLastRowNum()I

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-gt p2, v1, :cond_2

    .line 45
    .line 46
    if-eqz v0, :cond_3

    .line 47
    .line 48
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getRowNum()I

    .line 49
    .line 50
    .line 51
    move-result p2

    .line 52
    iput p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_lastrow:I

    .line 53
    .line 54
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getRowNum()I

    .line 55
    .line 56
    .line 57
    move-result p2

    .line 58
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getFirstRowNum()I

    .line 59
    .line 60
    .line 61
    move-result v1

    .line 62
    if-lt p2, v1, :cond_4

    .line 63
    .line 64
    if-eqz v0, :cond_5

    .line 65
    .line 66
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getRowNum()I

    .line 67
    .line 68
    .line 69
    move-result p1

    .line 70
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_firstrow:I

    .line 71
    .line 72
    :cond_5
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private createRowFromRecord(Lcom/intsig/office/fc/hssf/record/RowRecord;)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    .line 4
    .line 5
    invoke-direct {v0, v1, p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;Lcom/intsig/office/fc/hssf/record/RowRecord;)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->addRow(Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;Z)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private findFirstRow(I)I
    .locals 1

    .line 1
    add-int/lit8 p1, p1, 0x1

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :goto_0
    if-nez v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getLastRowNum()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-gt p1, v0, :cond_0

    .line 14
    .line 15
    add-int/lit8 p1, p1, 0x1

    .line 16
    .line 17
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    goto :goto_0

    .line 22
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getLastRowNum()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-le p1, v0, :cond_1

    .line 27
    .line 28
    const/4 p1, 0x0

    .line 29
    :cond_1
    return p1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private findLastRow(I)I
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x1

    .line 3
    if-ge p1, v1, :cond_0

    .line 4
    .line 5
    return v0

    .line 6
    :cond_0
    sub-int/2addr p1, v1

    .line 7
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    :goto_0
    if-nez v1, :cond_1

    .line 12
    .line 13
    if-lez p1, :cond_1

    .line 14
    .line 15
    add-int/lit8 p1, p1, -0x1

    .line 16
    .line 17
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    goto :goto_0

    .line 22
    :cond_1
    if-nez v1, :cond_2

    .line 23
    .line 24
    return v0

    .line 25
    :cond_2
    return p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private getCellRange(Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;)Lcom/intsig/office/fc/ss/usermodel/CellRange;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;",
            ")",
            "Lcom/intsig/office/fc/ss/usermodel/CellRange<",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    sub-int v3, v2, v0

    .line 18
    .line 19
    add-int/lit8 v3, v3, 0x1

    .line 20
    .line 21
    sub-int v4, p1, v1

    .line 22
    .line 23
    add-int/lit8 v4, v4, 0x1

    .line 24
    .line 25
    new-instance v5, Ljava/util/ArrayList;

    .line 26
    .line 27
    mul-int v6, v3, v4

    .line 28
    .line 29
    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 30
    .line 31
    .line 32
    move v6, v0

    .line 33
    :goto_0
    if-gt v6, v2, :cond_3

    .line 34
    .line 35
    move v7, v1

    .line 36
    :goto_1
    if-gt v7, p1, :cond_2

    .line 37
    .line 38
    invoke-virtual {p0, v6}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 39
    .line 40
    .line 41
    move-result-object v8

    .line 42
    if-nez v8, :cond_0

    .line 43
    .line 44
    invoke-virtual {p0, v6}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->createRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 45
    .line 46
    .line 47
    move-result-object v8

    .line 48
    :cond_0
    invoke-virtual {v8, v7}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getCell(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;

    .line 49
    .line 50
    .line 51
    move-result-object v9

    .line 52
    if-nez v9, :cond_1

    .line 53
    .line 54
    invoke-virtual {v8, v7}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->createCell(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;

    .line 55
    .line 56
    .line 57
    move-result-object v9

    .line 58
    :cond_1
    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    .line 60
    .line 61
    add-int/lit8 v7, v7, 0x1

    .line 62
    .line 63
    goto :goto_1

    .line 64
    :cond_2
    add-int/lit8 v6, v6, 0x1

    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_3
    const-class p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;

    .line 68
    .line 69
    move v2, v3

    .line 70
    move v3, v4

    .line 71
    move-object v4, v5

    .line 72
    move-object v5, p1

    .line 73
    invoke-static/range {v0 .. v5}, Lcom/intsig/office/fc/ss/util/SSCellRange;->create(IIIILjava/util/List;Ljava/lang/Class;)Lcom/intsig/office/fc/ss/util/SSCellRange;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    return-object p1
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private getProtectionBlock()Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getProtectionBlock()Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private notifyRowShifting(Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;)V
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "Row[rownum="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getRowNum()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v1, "] contains cell(s) included in a multi-cell array formula. You cannot change part of an array."

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->iterator()Ljava/util/Iterator;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    if-eqz v1, :cond_1

    .line 36
    .line 37
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    check-cast v1, Lcom/intsig/office/fc/ss/usermodel/ICell;

    .line 42
    .line 43
    check-cast v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;

    .line 44
    .line 45
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;->isPartOfArrayFormulaGroup()Z

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    if-eqz v2, :cond_0

    .line 50
    .line 51
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;->notifyArrayFormulaChanging(Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_1
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private setPropertiesFromSheet(Lcom/intsig/office/fc/hssf/model/InternalSheet;)V
    .locals 14

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getNextRow()Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v1, 0x0

    .line 10
    :goto_0
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->createRowFromRecord(Lcom/intsig/office/fc/hssf/record/RowRecord;)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 13
    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getNextRow()Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    goto :goto_0

    .line 20
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getCellValueIterator()Ljava/util/Iterator;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 25
    .line 26
    .line 27
    move-result-wide v2

    .line 28
    sget-object v4, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 29
    .line 30
    sget v5, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 31
    .line 32
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/util/POILogger;->check(I)Z

    .line 33
    .line 34
    .line 35
    move-result v5

    .line 36
    if-eqz v5, :cond_2

    .line 37
    .line 38
    sget v5, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->DEBUG:I

    .line 39
    .line 40
    const-string v6, "Time at start of cell creating in HSSF sheet = "

    .line 41
    .line 42
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 43
    .line 44
    .line 45
    move-result-object v7

    .line 46
    invoke-virtual {v4, v5, v6, v7}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 47
    .line 48
    .line 49
    :cond_2
    const/4 v4, 0x0

    .line 50
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 51
    .line 52
    .line 53
    move-result v5

    .line 54
    if-eqz v5, :cond_8

    .line 55
    .line 56
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 57
    .line 58
    .line 59
    move-result-object v5

    .line 60
    check-cast v5, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 61
    .line 62
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 63
    .line 64
    .line 65
    move-result-wide v6

    .line 66
    if-eqz v4, :cond_3

    .line 67
    .line 68
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getRowNum()I

    .line 69
    .line 70
    .line 71
    move-result v8

    .line 72
    invoke-interface {v5}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getRow()I

    .line 73
    .line 74
    .line 75
    move-result v9

    .line 76
    if-eq v8, v9, :cond_5

    .line 77
    .line 78
    :cond_3
    invoke-interface {v5}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getRow()I

    .line 79
    .line 80
    .line 81
    move-result v4

    .line 82
    invoke-virtual {p0, v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 83
    .line 84
    .line 85
    move-result-object v4

    .line 86
    if-nez v4, :cond_5

    .line 87
    .line 88
    if-nez v1, :cond_4

    .line 89
    .line 90
    new-instance v8, Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 91
    .line 92
    invoke-interface {v5}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getRow()I

    .line 93
    .line 94
    .line 95
    move-result v9

    .line 96
    invoke-direct {v8, v9}, Lcom/intsig/office/fc/hssf/record/RowRecord;-><init>(I)V

    .line 97
    .line 98
    .line 99
    invoke-virtual {p1, v8}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->addRow(Lcom/intsig/office/fc/hssf/record/RowRecord;)V

    .line 100
    .line 101
    .line 102
    invoke-direct {p0, v8}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->createRowFromRecord(Lcom/intsig/office/fc/hssf/record/RowRecord;)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 103
    .line 104
    .line 105
    move-result-object v8

    .line 106
    move-object v13, v8

    .line 107
    move-object v8, v4

    .line 108
    move-object v4, v13

    .line 109
    goto :goto_2

    .line 110
    :cond_4
    new-instance p1, Ljava/lang/RuntimeException;

    .line 111
    .line 112
    const-string v0, "Unexpected missing row when some rows already present"

    .line 113
    .line 114
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    throw p1

    .line 118
    :cond_5
    move-object v8, v4

    .line 119
    :goto_2
    sget-object v9, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 120
    .line 121
    sget v10, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 122
    .line 123
    invoke-virtual {v9, v10}, Lcom/intsig/office/fc/util/POILogger;->check(I)Z

    .line 124
    .line 125
    .line 126
    move-result v10

    .line 127
    if-eqz v10, :cond_6

    .line 128
    .line 129
    sget v10, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->DEBUG:I

    .line 130
    .line 131
    new-instance v11, Ljava/lang/StringBuilder;

    .line 132
    .line 133
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    .line 135
    .line 136
    const-string v12, "record id = "

    .line 137
    .line 138
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    .line 140
    .line 141
    move-object v12, v5

    .line 142
    check-cast v12, Lcom/intsig/office/fc/hssf/record/Record;

    .line 143
    .line 144
    invoke-virtual {v12}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 145
    .line 146
    .line 147
    move-result v12

    .line 148
    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v12

    .line 152
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    .line 154
    .line 155
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 156
    .line 157
    .line 158
    move-result-object v11

    .line 159
    invoke-virtual {v9, v10, v11}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 160
    .line 161
    .line 162
    :cond_6
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->createCellFromRecord(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;

    .line 163
    .line 164
    .line 165
    sget v4, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 166
    .line 167
    invoke-virtual {v9, v4}, Lcom/intsig/office/fc/util/POILogger;->check(I)Z

    .line 168
    .line 169
    .line 170
    move-result v4

    .line 171
    if-eqz v4, :cond_7

    .line 172
    .line 173
    sget v4, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->DEBUG:I

    .line 174
    .line 175
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 176
    .line 177
    .line 178
    move-result-wide v10

    .line 179
    sub-long/2addr v10, v6

    .line 180
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 181
    .line 182
    .line 183
    move-result-object v5

    .line 184
    const-string v6, "record took "

    .line 185
    .line 186
    invoke-virtual {v9, v4, v6, v5}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 187
    .line 188
    .line 189
    :cond_7
    move-object v4, v8

    .line 190
    goto/16 :goto_1

    .line 191
    .line 192
    :cond_8
    sget-object p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 193
    .line 194
    sget v0, Lcom/intsig/office/fc/util/POILogger;->DEBUG:I

    .line 195
    .line 196
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/util/POILogger;->check(I)Z

    .line 197
    .line 198
    .line 199
    move-result v0

    .line 200
    if-eqz v0, :cond_9

    .line 201
    .line 202
    sget v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->DEBUG:I

    .line 203
    .line 204
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 205
    .line 206
    .line 207
    move-result-wide v4

    .line 208
    sub-long/2addr v4, v2

    .line 209
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 210
    .line 211
    .line 212
    move-result-object v1

    .line 213
    const-string v2, "total sheet cell creation took "

    .line 214
    .line 215
    invoke-virtual {p1, v0, v2, v1}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 216
    .line 217
    .line 218
    :cond_9
    return-void
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private validateArrayFormulas(Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;)V
    .locals 8

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    .line 14
    .line 15
    .line 16
    move-result v3

    .line 17
    :goto_0
    if-gt v0, v2, :cond_5

    .line 18
    .line 19
    move v4, v1

    .line 20
    :goto_1
    if-gt v4, v3, :cond_4

    .line 21
    .line 22
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 23
    .line 24
    .line 25
    move-result-object v5

    .line 26
    if-nez v5, :cond_0

    .line 27
    .line 28
    goto :goto_2

    .line 29
    :cond_0
    invoke-virtual {v5, v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getCell(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;

    .line 30
    .line 31
    .line 32
    move-result-object v5

    .line 33
    if-nez v5, :cond_1

    .line 34
    .line 35
    goto :goto_2

    .line 36
    :cond_1
    invoke-virtual {v5}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;->isPartOfArrayFormulaGroup()Z

    .line 37
    .line 38
    .line 39
    move-result v6

    .line 40
    if-eqz v6, :cond_3

    .line 41
    .line 42
    invoke-virtual {v5}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;->getArrayFormulaRange()Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 43
    .line 44
    .line 45
    move-result-object v5

    .line 46
    invoke-virtual {v5}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getNumberOfCells()I

    .line 47
    .line 48
    .line 49
    move-result v6

    .line 50
    const/4 v7, 0x1

    .line 51
    if-le v6, v7, :cond_3

    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 54
    .line 55
    .line 56
    move-result v6

    .line 57
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 58
    .line 59
    .line 60
    move-result v7

    .line 61
    invoke-virtual {v5, v6, v7}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->isInRange(II)Z

    .line 62
    .line 63
    .line 64
    move-result v6

    .line 65
    if-nez v6, :cond_2

    .line 66
    .line 67
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 68
    .line 69
    .line 70
    move-result v6

    .line 71
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 72
    .line 73
    .line 74
    move-result v7

    .line 75
    invoke-virtual {v5, v6, v7}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->isInRange(II)Z

    .line 76
    .line 77
    .line 78
    move-result v5

    .line 79
    if-nez v5, :cond_2

    .line 80
    .line 81
    goto :goto_2

    .line 82
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    .line 83
    .line 84
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 85
    .line 86
    .line 87
    const-string v1, "The range "

    .line 88
    .line 89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;->formatAsString()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object p1

    .line 96
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    const-string p1, " intersects with a multi-cell array formula. You cannot merge cells of an array."

    .line 100
    .line 101
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 109
    .line 110
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    throw v0

    .line 114
    :cond_3
    :goto_2
    add-int/lit8 v4, v4, 0x1

    .line 115
    .line 116
    goto :goto_1

    .line 117
    :cond_4
    add-int/lit8 v0, v0, 0x1

    .line 118
    .line 119
    goto :goto_0

    .line 120
    :cond_5
    return-void
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method


# virtual methods
.method public addMergedRegion(Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;)I
    .locals 4

    .line 4
    sget-object v0, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->EXCEL97:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->validate(Lcom/intsig/office/fc/ss/SpreadsheetVersion;)V

    .line 5
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->validateArrayFormulas(Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;)V

    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    move-result v1

    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    move-result v2

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    move-result v3

    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    move-result p1

    .line 8
    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->addMergedRegion(IIII)I

    move-result p1

    return p1
.end method

.method public addMergedRegion(Lcom/intsig/office/fc/ss/util/Region;)I
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/Region;->getRowFrom()I

    move-result v1

    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/Region;->getColumnFrom()S

    move-result v2

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/Region;->getRowTo()I

    move-result v3

    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/Region;->getColumnTo()S

    move-result p1

    .line 3
    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->addMergedRegion(IIII)I

    move-result p1

    return p1
.end method

.method public addValidationData(Lcom/intsig/office/fc/ss/usermodel/DataValidation;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getOrCreateDataValidityTable()Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->createDVRecord(Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;)Lcom/intsig/office/fc/hssf/record/DVRecord;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/DataValidityTable;->addDataValidation(Lcom/intsig/office/fc/hssf/record/DVRecord;)V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 20
    .line 21
    const-string v0, "objValidation must not be null"

    .line 22
    .line 23
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    throw p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public autoSizeColumn(I)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->autoSizeColumn(IZ)V

    return-void
.end method

.method public autoSizeColumn(IZ)V
    .locals 4

    .line 2
    invoke-static {p0, p1, p2}, Lcom/intsig/office/fc/ss/util/SheetUtil;->getColumnWidth(Lcom/intsig/office/fc/ss/usermodel/Sheet;IZ)D

    move-result-wide v0

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpl-double p2, v0, v2

    if-eqz p2, :cond_1

    const-wide/high16 v2, 0x4070000000000000L    # 256.0

    mul-double v0, v0, v2

    const p2, 0xff00

    int-to-double v2, p2

    cmpl-double p2, v0, v2

    if-lez p2, :cond_0

    move-wide v0, v2

    :cond_0
    double-to-int p2, v0

    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setColumnWidth(II)V

    :cond_1
    return-void
.end method

.method cloneSheet(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->cloneSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-direct {v0, p1, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;Lcom/intsig/office/fc/hssf/model/InternalSheet;)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public createDrawingPatriarch()Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->initDrawings()V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 11
    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_book:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getDrawingManager()Lcom/intsig/office/fc/hssf/model/DrawingManager2;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const/4 v2, 0x1

    .line 23
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->aggregateDrawingRecords(Lcom/intsig/office/fc/hssf/model/DrawingManager2;Z)I

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 27
    .line 28
    const/16 v1, 0x2694

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    check-cast v0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;

    .line 35
    .line 36
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->setPatriarch(Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;)V

    .line 39
    .line 40
    .line 41
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 42
    .line 43
    return-object v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public createFreezePane(II)V
    .locals 0

    .line 6
    invoke-virtual {p0, p1, p2, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->createFreezePane(IIII)V

    return-void
.end method

.method public createFreezePane(IIII)V
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->validateColumn(I)V

    .line 2
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->validateRow(I)V

    if-lt p3, p1, :cond_1

    if-lt p4, p2, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p4, p3}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createFreezePane(IIII)V

    return-void

    .line 4
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "topRow parameter must not be less than leftmostColumn parameter"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 5
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "leftmostColumn parameter must not be less than colSplit parameter"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public createRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    .line 4
    .line 5
    invoke-direct {v0, v1, p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;I)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x1

    .line 9
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->addRow(Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;Z)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public createSplitPane(IIIII)V
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    move v1, p1

    .line 6
    move v2, p2

    .line 7
    move v3, p4

    .line 8
    move v4, p3

    .line 9
    move v5, p5

    .line 10
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createSplitPane(IIIII)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method public dumpDrawingRecords(Z)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_book:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getDrawingManager()Lcom/intsig/office/fc/hssf/model/DrawingManager2;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const/4 v2, 0x0

    .line 10
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->aggregateDrawingRecords(Lcom/intsig/office/fc/hssf/model/DrawingManager2;Z)I

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const/16 v1, 0x2694

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    check-cast v0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/AbstractEscherHolderRecord;->getEscherRecords()Ljava/util/List;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    new-instance v1, Ljava/io/PrintWriter;

    .line 30
    .line 31
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 32
    .line 33
    invoke-direct {v1, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 34
    .line 35
    .line 36
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    if-eqz v3, :cond_1

    .line 45
    .line 46
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    check-cast v3, Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 51
    .line 52
    if-eqz p1, :cond_0

    .line 53
    .line 54
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 55
    .line 56
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v3

    .line 60
    invoke-virtual {v4, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_0
    invoke-virtual {v3, v1, v2}, Lcom/intsig/office/fc/ddf/EscherRecord;->display(Ljava/io/PrintWriter;I)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_1
    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V

    .line 69
    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getActiveCell()Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->row_activecell:I

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->row_activecell:I

    .line 10
    .line 11
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iget v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->column_activecell:I

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getCell(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    return-object v0

    .line 22
    :cond_0
    const/4 v0, 0x0

    .line 23
    return-object v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getActiveCellColumn()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->column_activecell:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getActiveCellRow()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->row_activecell:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAlternateExpression()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->getAlternateExpression()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAlternateFormula()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->getAlternateFormula()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAutobreaks()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->getAutobreaks()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCellComment(II)Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getCell(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;->getCellComment()Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    return-object p1

    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 19
    .line 20
    invoke-static {v0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;->findCellComment(Lcom/intsig/office/fc/hssf/model/InternalSheet;II)Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    return-object p1

    .line 25
    :cond_1
    const/4 p1, 0x0

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public getColumnBreaks()[I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->getColumnBreaks()[I

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getColumnInfo()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/util/ColumnInfo;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getColumnInfo()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getColumnPixelWidth(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getColumnPixelWidth(I)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getColumnStyle(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFCellStyle;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    int-to-short p1, p1

    .line 4
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getXFIndexForColAt(S)S

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    const/16 v0, 0xf

    .line 9
    .line 10
    if-ne p1, v0, :cond_0

    .line 11
    .line 12
    const/4 p1, 0x0

    .line 13
    return-object p1

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_book:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 15
    .line 16
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getExFormatAt(I)Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFCellStyle;

    .line 21
    .line 22
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_book:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 23
    .line 24
    invoke-direct {v1, p1, v0, v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCellStyle;-><init>(SLcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;Lcom/intsig/office/fc/hssf/model/InternalWorkbook;)V

    .line 25
    .line 26
    .line 27
    return-object v1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getColumnWidth(I)I
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getColumnWidth(I)I

    move-result p1

    return p1
.end method

.method public getColumnWidth(S)S
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const v0, 0xffff

    and-int/2addr p1, v0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getColumnWidth(I)I

    move-result p1

    int-to-short p1, p1

    return p1
.end method

.method public getDataValidationHelper()Lcom/intsig/office/fc/ss/usermodel/DataValidationHelper;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidationHelper;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidationHelper;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDefaultColumnWidth()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getDefaultColumnWidth()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDefaultRowHeight()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getDefaultRowHeight()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDefaultRowHeightInPoints()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getDefaultRowHeight()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-float v0, v0

    .line 8
    const/high16 v1, 0x41a00000    # 20.0f

    .line 9
    .line 10
    div-float/2addr v0, v1

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDialog()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->getDialog()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDisplayGuts()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->getDisplayGuts()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDrawingEscherAggregate()Lcom/intsig/office/fc/hssf/record/EscherAggregate;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_book:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->findDrawingGroup()Lcom/intsig/office/fc/hssf/model/DrawingManager2;

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_book:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getDrawingManager()Lcom/intsig/office/fc/hssf/model/DrawingManager2;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v1, 0x0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    return-object v1

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 17
    .line 18
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_book:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 19
    .line 20
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getDrawingManager()Lcom/intsig/office/fc/hssf/model/DrawingManager2;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    const/4 v3, 0x0

    .line 25
    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->aggregateDrawingRecords(Lcom/intsig/office/fc/hssf/model/DrawingManager2;Z)I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    const/4 v2, -0x1

    .line 30
    if-ne v0, v2, :cond_1

    .line 31
    .line 32
    return-object v1

    .line 33
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 34
    .line 35
    const/16 v1, 0x2694

    .line 36
    .line 37
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    check-cast v0, Lcom/intsig/office/fc/hssf/record/EscherAggregate;

    .line 42
    .line 43
    return-object v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getDrawingPatriarch()Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getDrawingEscherAggregate()Lcom/intsig/office/fc/hssf/record/EscherAggregate;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const/4 v1, 0x0

    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    return-object v1

    .line 14
    :cond_1
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 15
    .line 16
    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->setPatriarch(Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->convertRecordsToUserModel(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_patriarch:Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 23
    .line 24
    return-object v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getFirstRowNum()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_firstrow:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFitToPage()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->getFitToPage()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFooter()Lcom/intsig/office/fc/hssf/usermodel/HSSFFooter;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFFooter;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFooter;-><init>(Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getForceFormulaRecalculation()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getUncalced()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHeader()Lcom/intsig/office/fc/hssf/usermodel/HSSFHeader;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFHeader;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFHeader;-><init>(Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHorizontallyCenter()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->getHCenter()Lcom/intsig/office/fc/hssf/record/HCenterRecord;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/HCenterRecord;->getHCenter()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLastRowNum()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_lastrow:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLeftCol()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getLeftCol()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMargin(S)D
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->getMargin(S)D

    .line 8
    .line 9
    .line 10
    move-result-wide v0

    .line 11
    return-wide v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getMergedRegion(I)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getMergedRegionAt(I)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getMergedRegionAt(I)Lcom/intsig/office/fc/hssf/util/Region;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getMergedRegion(I)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/hssf/util/Region;

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    int-to-short v2, v2

    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    int-to-short p1, p1

    .line 25
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/intsig/office/fc/hssf/util/Region;-><init>(ISIS)V

    .line 26
    .line 27
    .line 28
    return-object v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public getNumMergedRegions()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getNumMergedRegions()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getObjectProtect()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getProtectionBlock()Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;->isObjectProtected()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPaneInformation()Lcom/intsig/office/fc/hssf/util/HSSFPaneInformation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->paneInformation:Lcom/intsig/office/fc/hssf/util/HSSFPaneInformation;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPaneInformation()Lcom/intsig/office/fc/hssf/util/HSSFPaneInformation;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->paneInformation:Lcom/intsig/office/fc/hssf/util/HSSFPaneInformation;

    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->paneInformation:Lcom/intsig/office/fc/hssf/util/HSSFPaneInformation;

    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPassword()S
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getProtectionBlock()Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;->getPasswordHash()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-short v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPhysicalNumberOfRows()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPrintSetup()Lcom/intsig/office/fc/hssf/usermodel/HSSFPrintSetup;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPrintSetup;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->getPrintSetup()Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPrintSetup;-><init>(Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;)V

    .line 14
    .line 15
    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getProtect()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getProtectionBlock()Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;->isSheetProtected()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    .line 2
    .line 3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 12
    .line 13
    return-object p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getRowBreaks()[I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->getRowBreaks()[I

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRowSumsBelow()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->getRowSumsBelow()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRowSumsRight()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->getRowSumsRight()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getScenarioProtect()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getProtectionBlock()Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;->isScenarioProtected()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getScrollX()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->scrollX:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getScrollY()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->scrollY:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSheetConditionalFormatting()Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheetConditionalFormatting;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSheetName()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getWorkbook()Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetIndex(Lcom/intsig/office/fc/ss/usermodel/Sheet;)I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetName(I)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTopRow()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getTopRow()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getVerticallyCenter()Z
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->getVCenter()Lcom/intsig/office/fc/hssf/record/VCenterRecord;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/VCenterRecord;->getVCenter()Z

    move-result v0

    return v0
.end method

.method public getVerticallyCenter(Z)Z
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getVerticallyCenter()Z

    move-result p1

    return p1
.end method

.method public getWorkbook()Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getZoom()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->zoom:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public groupColumn(II)V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->groupColumnRange(IIZ)V

    return-void
.end method

.method public groupColumn(SS)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const v0, 0xffff

    and-int/2addr p1, v0

    and-int/2addr p2, v0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->groupColumn(II)V

    return-void
.end method

.method public groupRow(II)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, p1, p2, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->groupRowRange(IIZ)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected insertChartRecords(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x23e

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-interface {v1, v0, p1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public isActive()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getWindowTwo()Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->isActive()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isColumnBroken(I)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->isColumnBroken(I)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public isColumnHidden(I)Z
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->isColumnHidden(I)Z

    move-result p1

    return p1
.end method

.method public isColumnHidden(S)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const v0, 0xffff

    and-int/2addr p1, v0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->isColumnHidden(I)Z

    move-result p1

    return p1
.end method

.method public isDisplayFormulas()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->isDisplayFormulas()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isDisplayGridlines()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->isDisplayGridlines()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isDisplayRowColHeadings()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->isDisplayRowColHeadings()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isDisplayZeros()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getWindowTwo()Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->getDisplayZeros()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isGridsPrinted()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->isGridsPrinted()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isInitForDraw()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->isInitForDraw:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isPrintGridlines()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPrintGridlines()Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;->getPrintGridlines()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isRightToLeft()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getWindowTwo()Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->getArabic()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isRowBroken(I)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->isRowBroken(I)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public isSelected()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getWindowTwo()Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->getSelected()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/intsig/office/fc/ss/usermodel/IRow;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->rowIterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public protectSheet(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getProtectionBlock()Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x1

    .line 6
    invoke-virtual {v0, p1, v1, v1}, Lcom/intsig/office/fc/hssf/record/aggregates/WorksheetProtectionBlock;->protectSheet(Ljava/lang/String;ZZ)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public removeArrayFormula(Lcom/intsig/office/fc/ss/usermodel/ICell;)Lcom/intsig/office/fc/ss/usermodel/CellRange;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/ss/usermodel/ICell;",
            ")",
            "Lcom/intsig/office/fc/ss/usermodel/CellRange<",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getSheet()Lcom/intsig/office/fc/ss/usermodel/Sheet;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-ne v0, p0, :cond_2

    .line 6
    .line 7
    move-object v0, p1

    .line 8
    check-cast v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;->getCellValueRecord()Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 15
    .line 16
    if-eqz v1, :cond_1

    .line 17
    .line 18
    check-cast v0, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 19
    .line 20
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getRowIndex()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getColumnIndex()I

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->removeArrayFormula(II)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getCellRange(Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;)Lcom/intsig/office/fc/ss/usermodel/CellRange;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/CellRange;->iterator()Ljava/util/Iterator;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-eqz v1, :cond_0

    .line 45
    .line 46
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    check-cast v1, Lcom/intsig/office/fc/ss/usermodel/ICell;

    .line 51
    .line 52
    const/4 v2, 0x3

    .line 53
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/ss/usermodel/ICell;->setCellType(I)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_0
    return-object p1

    .line 58
    :cond_1
    new-instance v0, Lcom/intsig/office/fc/ss/util/CellReference;

    .line 59
    .line 60
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/ss/util/CellReference;-><init>(Lcom/intsig/office/fc/ss/usermodel/ICell;)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellReference;->formatAsString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 68
    .line 69
    new-instance v1, Ljava/lang/StringBuilder;

    .line 70
    .line 71
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    .line 73
    .line 74
    const-string v2, "Cell "

    .line 75
    .line 76
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    const-string p1, " is not part of an array formula."

    .line 83
    .line 84
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    throw v0

    .line 95
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 96
    .line 97
    const-string v0, "Specified cell does not belong to this sheet."

    .line 98
    .line 99
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    throw p1
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public removeColumnBreak(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->removeColumnBreak(I)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public removeMergedRegion(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->removeMergedRegion(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public removeRow(Lcom/intsig/office/fc/ss/usermodel/IRow;)V
    .locals 6

    .line 1
    move-object v0, p1

    .line 2
    check-cast v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 3
    .line 4
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/IRow;->getSheet()Lcom/intsig/office/fc/ss/usermodel/Sheet;

    .line 5
    .line 6
    .line 7
    move-result-object v1

    .line 8
    const-string v2, "Specified row does not belong to this sheet"

    .line 9
    .line 10
    if-ne v1, p0, :cond_6

    .line 11
    .line 12
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    if-eqz v3, :cond_1

    .line 21
    .line 22
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    check-cast v3, Lcom/intsig/office/fc/ss/usermodel/ICell;

    .line 27
    .line 28
    check-cast v3, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;

    .line 29
    .line 30
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;->isPartOfArrayFormulaGroup()Z

    .line 31
    .line 32
    .line 33
    move-result v4

    .line 34
    if-eqz v4, :cond_0

    .line 35
    .line 36
    new-instance v4, Ljava/lang/StringBuilder;

    .line 37
    .line 38
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .line 40
    .line 41
    const-string v5, "Row[rownum="

    .line 42
    .line 43
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/IRow;->getRowNum()I

    .line 47
    .line 48
    .line 49
    move-result v5

    .line 50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    const-string v5, "] contains cell(s) included in a multi-cell array formula. You cannot change part of an array."

    .line 54
    .line 55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v4

    .line 62
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;->notifyArrayFormulaChanging(Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    .line 67
    .line 68
    invoke-virtual {v1}, Ljava/util/TreeMap;->size()I

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    if-lez v1, :cond_5

    .line 73
    .line 74
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/IRow;->getRowNum()I

    .line 75
    .line 76
    .line 77
    move-result v1

    .line 78
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 79
    .line 80
    .line 81
    move-result-object v1

    .line 82
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    .line 83
    .line 84
    invoke-virtual {v3, v1}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    check-cast v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    .line 89
    .line 90
    if-ne v1, p1, :cond_4

    .line 91
    .line 92
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getRowNum()I

    .line 93
    .line 94
    .line 95
    move-result p1

    .line 96
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getLastRowNum()I

    .line 97
    .line 98
    .line 99
    move-result v1

    .line 100
    if-ne p1, v1, :cond_2

    .line 101
    .line 102
    iget p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_lastrow:I

    .line 103
    .line 104
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->findLastRow(I)I

    .line 105
    .line 106
    .line 107
    move-result p1

    .line 108
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_lastrow:I

    .line 109
    .line 110
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getRowNum()I

    .line 111
    .line 112
    .line 113
    move-result p1

    .line 114
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getFirstRowNum()I

    .line 115
    .line 116
    .line 117
    move-result v1

    .line 118
    if-ne p1, v1, :cond_3

    .line 119
    .line 120
    iget p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_firstrow:I

    .line 121
    .line 122
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->findFirstRow(I)I

    .line 123
    .line 124
    .line 125
    move-result p1

    .line 126
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_firstrow:I

    .line 127
    .line 128
    :cond_3
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 129
    .line 130
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getRowRecord()Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 131
    .line 132
    .line 133
    move-result-object v0

    .line 134
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->removeRow(Lcom/intsig/office/fc/hssf/record/RowRecord;)V

    .line 135
    .line 136
    .line 137
    goto :goto_1

    .line 138
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 139
    .line 140
    invoke-direct {p1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    throw p1

    .line 144
    :cond_5
    :goto_1
    return-void

    .line 145
    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 146
    .line 147
    invoke-direct {p1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 148
    .line 149
    .line 150
    throw p1
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public removeRowBreak(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->removeRowBreak(I)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public rowIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/intsig/office/fc/ss/usermodel/IRow;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setActive(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getWindowTwo()Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setActive(Z)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setActiveCell(II)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->row_activecell:I

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->column_activecell:I

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setAlternativeExpression(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->setAlternateExpression(Z)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setAlternativeFormula(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->setAlternateFormula(Z)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setArrayFormula(Ljava/lang/String;Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;)Lcom/intsig/office/fc/ss/usermodel/CellRange;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;",
            ")",
            "Lcom/intsig/office/fc/ss/usermodel/CellRange<",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;",
            ">;"
        }
    .end annotation

    .line 1
    const/4 p1, 0x0

    .line 2
    return-object p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setAutoFilter(Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;)Lcom/intsig/office/fc/hssf/usermodel/HSSFAutoFilter;
    .locals 13

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    .line 8
    .line 9
    invoke-virtual {v1, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetIndex(Lcom/intsig/office/fc/ss/usermodel/Sheet;)I

    .line 10
    .line 11
    .line 12
    move-result v11

    .line 13
    add-int/lit8 v1, v11, 0x1

    .line 14
    .line 15
    const/16 v2, 0xd

    .line 16
    .line 17
    invoke-virtual {v0, v2, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSpecificBuiltinRecord(BI)Lcom/intsig/office/fc/hssf/record/NameRecord;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    if-nez v3, :cond_0

    .line 22
    .line 23
    invoke-virtual {v0, v2, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->createBuiltInName(BI)Lcom/intsig/office/fc/hssf/record/NameRecord;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    :cond_0
    move-object v0, v3

    .line 28
    new-instance v1, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 35
    .line 36
    .line 37
    move-result v4

    .line 38
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 39
    .line 40
    .line 41
    move-result v5

    .line 42
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    .line 43
    .line 44
    .line 45
    move-result v6

    .line 46
    const/4 v7, 0x0

    .line 47
    const/4 v8, 0x0

    .line 48
    const/4 v9, 0x0

    .line 49
    const/4 v10, 0x0

    .line 50
    move-object v2, v1

    .line 51
    invoke-direct/range {v2 .. v11}, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;-><init>(IIIIZZZZI)V

    .line 52
    .line 53
    .line 54
    const/4 v2, 0x1

    .line 55
    new-array v3, v2, [Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 56
    .line 57
    const/4 v4, 0x0

    .line 58
    aput-object v1, v3, v4

    .line 59
    .line 60
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/record/NameRecord;->setNameDefinition([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 61
    .line 62
    .line 63
    new-instance v0, Lcom/intsig/office/fc/hssf/record/AutoFilterInfoRecord;

    .line 64
    .line 65
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/AutoFilterInfoRecord;-><init>()V

    .line 66
    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    add-int/2addr v1, v2

    .line 73
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 74
    .line 75
    .line 76
    move-result v3

    .line 77
    sub-int/2addr v1, v3

    .line 78
    int-to-short v1, v1

    .line 79
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/AutoFilterInfoRecord;->setNumEntries(S)V

    .line 80
    .line 81
    .line 82
    iget-object v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 83
    .line 84
    const/16 v3, 0x200

    .line 85
    .line 86
    invoke-virtual {v1, v3}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    .line 87
    .line 88
    .line 89
    move-result v1

    .line 90
    iget-object v3, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 91
    .line 92
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    .line 93
    .line 94
    .line 95
    move-result-object v3

    .line 96
    invoke-interface {v3, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 97
    .line 98
    .line 99
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->createDrawingPatriarch()Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 100
    .line 101
    .line 102
    move-result-object v0

    .line 103
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 104
    .line 105
    .line 106
    move-result v1

    .line 107
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    .line 108
    .line 109
    .line 110
    move-result v3

    .line 111
    if-gt v1, v3, :cond_1

    .line 112
    .line 113
    new-instance v3, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;

    .line 114
    .line 115
    const/4 v5, 0x0

    .line 116
    const/4 v6, 0x0

    .line 117
    const/4 v7, 0x0

    .line 118
    const/4 v8, 0x0

    .line 119
    int-to-short v9, v1

    .line 120
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 121
    .line 122
    .line 123
    move-result v10

    .line 124
    add-int/lit8 v1, v1, 0x1

    .line 125
    .line 126
    int-to-short v11, v1

    .line 127
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 128
    .line 129
    .line 130
    move-result v4

    .line 131
    add-int/lit8 v12, v4, 0x1

    .line 132
    .line 133
    move-object v4, v3

    .line 134
    invoke-direct/range {v4 .. v12}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;-><init>(IIIISISI)V

    .line 135
    .line 136
    .line 137
    invoke-virtual {v0, v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;->createComboBox(Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;)Lcom/intsig/office/fc/hssf/usermodel/HSSFSimpleShape;

    .line 138
    .line 139
    .line 140
    goto :goto_0

    .line 141
    :cond_1
    new-instance p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFAutoFilter;

    .line 142
    .line 143
    invoke-direct {p1, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAutoFilter;-><init>(Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;)V

    .line 144
    .line 145
    .line 146
    return-object p1
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public setAutobreaks(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->setAutobreaks(Z)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setColumnBreak(I)V
    .locals 3

    .line 1
    int-to-short p1, p1

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->validateColumn(I)V

    .line 3
    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    sget-object v1, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->EXCEL97:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    .line 12
    .line 13
    invoke-virtual {v1}, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->getLastRowIndex()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    int-to-short v1, v1

    .line 18
    const/4 v2, 0x0

    .line 19
    invoke-virtual {v0, p1, v2, v1}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->setColumnBreak(SSS)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
.end method

.method public setColumnGroupCollapsed(IZ)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setColumnGroupCollapsed(IZ)V

    return-void
.end method

.method public setColumnGroupCollapsed(SZ)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const v0, 0xffff

    and-int/2addr p1, v0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setColumnGroupCollapsed(IZ)V

    return-void
.end method

.method public setColumnHidden(IZ)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setColumnHidden(IZ)V

    return-void
.end method

.method public setColumnHidden(SZ)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const v0, 0xffff

    and-int/2addr p1, v0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setColumnHidden(IZ)V

    return-void
.end method

.method public setColumnPixelWidth(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setColumnPixelWidth(II)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setColumnWidth(II)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setColumnWidth(II)V

    return-void
.end method

.method public setColumnWidth(SS)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const v0, 0xffff

    and-int/2addr p1, v0

    and-int/2addr p2, v0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setColumnWidth(II)V

    return-void
.end method

.method public setDefaultColumnStyle(ILcom/intsig/office/fc/ss/usermodel/ICellStyle;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    check-cast p2, Lcom/intsig/office/fc/hssf/usermodel/HSSFCellStyle;

    .line 4
    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCellStyle;->getIndex()S

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setDefaultColumnStyle(II)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setDefaultColumnWidth(I)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setDefaultColumnWidth(I)V

    return-void
.end method

.method public setDefaultColumnWidth(S)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const v0, 0xffff

    and-int/2addr p1, v0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->setDefaultColumnWidth(I)V

    return-void
.end method

.method public setDefaultRowHeight(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setDefaultRowHeight(S)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDefaultRowHeightInPoints(F)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/high16 v1, 0x41a00000    # 20.0f

    .line 4
    .line 5
    mul-float p1, p1, v1

    .line 6
    .line 7
    float-to-int p1, p1

    .line 8
    int-to-short p1, p1

    .line 9
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setDefaultRowHeight(S)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDialog(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->setDialog(Z)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDisplayFormulas(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setDisplayFormulas(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDisplayGridlines(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setDisplayGridlines(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDisplayGuts(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->setDisplayGuts(Z)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDisplayRowColHeadings(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setDisplayRowColHeadings(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDisplayZeros(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getWindowTwo()Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setDisplayZeros(Z)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFitToPage(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->setFitToPage(Z)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setForceFormulaRecalculation(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setUncalced(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setGridsPrinted(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setGridsPrinted(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setHorizontallyCenter(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->getHCenter()Lcom/intsig/office/fc/hssf/record/HCenterRecord;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/HCenterRecord;->setHCenter(Z)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setInitForDraw(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->isInitForDraw:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setMargin(SD)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->setMargin(SD)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setPrintGridlines(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPrintGridlines()Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;->setPrintGridlines(Z)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRightToLeft(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getWindowTwo()Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setArabic(Z)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRowBreak(I)V
    .locals 3

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->validateRow(I)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const/4 v1, 0x0

    .line 11
    const/16 v2, 0xff

    .line 12
    .line 13
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->setRowBreak(ISS)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRowGroupCollapsed(IZ)V
    .locals 0

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 4
    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getRowsAggregate()Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 6
    .line 7
    .line 8
    move-result-object p2

    .line 9
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->collapseRow(I)V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    iget-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 14
    .line 15
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getRowsAggregate()Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->expandRow(I)V

    .line 20
    .line 21
    .line 22
    :goto_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setRowSumsBelow(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->setRowSumsBelow(Z)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->setAlternateExpression(Z)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRowSumsRight(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/16 v1, 0x81

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;->setRowSumsRight(Z)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setScroll(II)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->scrollX:I

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->scrollY:I

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setSelected(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getWindowTwo()Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;->setSelected(Z)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setVerticallyCenter(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->getVCenter()Lcom/intsig/office/fc/hssf/record/VCenterRecord;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/VCenterRecord;->setVCenter(Z)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setZoom(F)V
    .locals 0

    .line 7
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->zoom:F

    return-void
.end method

.method public setZoom(II)V
    .locals 2

    const/4 v0, 0x1

    if-lt p1, v0, :cond_1

    const v1, 0xffff

    if-gt p1, v1, :cond_1

    if-lt p2, v0, :cond_0

    if-gt p2, v1, :cond_0

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/SCLRecord;

    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/SCLRecord;-><init>()V

    int-to-short p1, p1

    .line 2
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/SCLRecord;->setNumerator(S)V

    int-to-short p1, p2

    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/SCLRecord;->setDenominator(S)V

    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setSCLRecord(Lcom/intsig/office/fc/hssf/record/SCLRecord;)V

    return-void

    .line 5
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Denominator must be greater than 1 and less than 65536"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 6
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Numerator must be greater than 1 and less than 65536"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected shiftMerged(IIIZ)V
    .locals 6

    .line 1
    new-instance p4, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getNumMergedRegions()I

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-ge v1, v2, :cond_6

    .line 13
    .line 14
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getMergedRegion(I)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    invoke-virtual {v2}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    const/4 v4, 0x1

    .line 23
    if-ge v3, p1, :cond_1

    .line 24
    .line 25
    invoke-virtual {v2}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    if-lt v3, p1, :cond_0

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_0
    const/4 v3, 0x0

    .line 33
    goto :goto_2

    .line 34
    :cond_1
    :goto_1
    const/4 v3, 0x1

    .line 35
    :goto_2
    invoke-virtual {v2}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 36
    .line 37
    .line 38
    move-result v5

    .line 39
    if-le v5, p2, :cond_3

    .line 40
    .line 41
    invoke-virtual {v2}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 42
    .line 43
    .line 44
    move-result v5

    .line 45
    if-gt v5, p2, :cond_2

    .line 46
    .line 47
    goto :goto_3

    .line 48
    :cond_2
    const/4 v5, 0x0

    .line 49
    goto :goto_4

    .line 50
    :cond_3
    :goto_3
    const/4 v5, 0x1

    .line 51
    :goto_4
    if-eqz v3, :cond_5

    .line 52
    .line 53
    if-nez v5, :cond_4

    .line 54
    .line 55
    goto :goto_5

    .line 56
    :cond_4
    add-int/lit8 v3, p1, -0x1

    .line 57
    .line 58
    invoke-static {v2, v3, v0}, Lcom/intsig/office/fc/ss/util/SheetUtil;->containsCell(Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;II)Z

    .line 59
    .line 60
    .line 61
    move-result v3

    .line 62
    if-nez v3, :cond_5

    .line 63
    .line 64
    add-int/lit8 v3, p2, 0x1

    .line 65
    .line 66
    invoke-static {v2, v3, v0}, Lcom/intsig/office/fc/ss/util/SheetUtil;->containsCell(Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;II)Z

    .line 67
    .line 68
    .line 69
    move-result v3

    .line 70
    if-nez v3, :cond_5

    .line 71
    .line 72
    invoke-virtual {v2}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 73
    .line 74
    .line 75
    move-result v3

    .line 76
    add-int/2addr v3, p3

    .line 77
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->setFirstRow(I)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {v2}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 81
    .line 82
    .line 83
    move-result v3

    .line 84
    add-int/2addr v3, p3

    .line 85
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->setLastRow(I)V

    .line 86
    .line 87
    .line 88
    invoke-interface {p4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    .line 90
    .line 91
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->removeMergedRegion(I)V

    .line 92
    .line 93
    .line 94
    add-int/lit8 v1, v1, -0x1

    .line 95
    .line 96
    :cond_5
    :goto_5
    add-int/2addr v1, v4

    .line 97
    goto :goto_0

    .line 98
    :cond_6
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 99
    .line 100
    .line 101
    move-result-object p1

    .line 102
    :goto_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 103
    .line 104
    .line 105
    move-result p2

    .line 106
    if-eqz p2, :cond_7

    .line 107
    .line 108
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 109
    .line 110
    .line 111
    move-result-object p2

    .line 112
    check-cast p2, Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 113
    .line 114
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->addMergedRegion(Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;)I

    .line 115
    .line 116
    .line 117
    goto :goto_6

    .line 118
    :cond_7
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public shiftRows(III)V
    .locals 6

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    .line 1
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->shiftRows(IIIZZ)V

    return-void
.end method

.method public shiftRows(IIIZZ)V
    .locals 7

    const/4 v6, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    .line 2
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->shiftRows(IIIZZZ)V

    return-void
.end method

.method public shiftRows(IIIZZZ)V
    .locals 15

    move-object v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    const/4 v4, 0x1

    if-gez v3, :cond_0

    move v6, v1

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    if-lez v3, :cond_15

    const/4 v5, -0x1

    move v6, v2

    :goto_0
    if-eqz p6, :cond_1

    .line 3
    iget-object v7, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    invoke-virtual {v7}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getNoteRecords()[Lcom/intsig/office/fc/hssf/record/NoteRecord;

    move-result-object v7

    goto :goto_1

    .line 4
    :cond_1
    sget-object v7, Lcom/intsig/office/fc/hssf/record/NoteRecord;->EMPTY_ARRAY:[Lcom/intsig/office/fc/hssf/record/NoteRecord;

    .line 5
    :goto_1
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->shiftMerged(IIIZ)V

    .line 6
    iget-object v8, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    invoke-virtual {v8}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPageSettings()Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v8

    invoke-virtual {v8, v1, v2, v3}, Lcom/intsig/office/fc/hssf/record/aggregates/PageSettingsBlock;->shiftRowBreaks(III)V

    :goto_2
    if-lt v6, v1, :cond_c

    if-gt v6, v2, :cond_c

    if-ltz v6, :cond_c

    const/high16 v8, 0x10000

    if-ge v6, v8, :cond_c

    .line 7
    invoke-virtual {p0, v6}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 8
    invoke-direct {p0, v8}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->notifyRowShifting(Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;)V

    :cond_2
    add-int v9, v6, v3

    .line 9
    invoke-virtual {p0, v9}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    move-result-object v10

    if-nez v10, :cond_3

    .line 10
    invoke-virtual {p0, v9}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->createRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    move-result-object v10

    .line 11
    :cond_3
    invoke-virtual {v10}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->removeAllCells()V

    if-nez v8, :cond_4

    goto :goto_6

    :cond_4
    if-eqz p4, :cond_5

    .line 12
    invoke-virtual {v8}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->getHeight()S

    move-result v11

    invoke-virtual {v10, v11}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->setHeight(S)V

    :cond_5
    if-eqz p5, :cond_6

    const/16 v11, 0xff

    .line 13
    invoke-virtual {v8, v11}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->setHeight(S)V

    .line 14
    :cond_6
    invoke-virtual {v8}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->cellIterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_7
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_8

    .line 15
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;

    .line 16
    invoke-virtual {v8, v12}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->removeCell(Lcom/intsig/office/fc/ss/usermodel/ICell;)V

    .line 17
    invoke-virtual {v12}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;->getCellValueRecord()Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    move-result-object v13

    .line 18
    invoke-interface {v13, v9}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->setRow(I)V

    .line 19
    invoke-virtual {v10, v13}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->createCellFromRecord(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;

    .line 20
    iget-object v14, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    invoke-virtual {v14, v9, v13}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->addValueRecord(ILcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V

    .line 21
    invoke-virtual {v12}, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;->getHyperlink()Lcom/intsig/office/fc/hssf/usermodel/HSSFHyperlink;

    move-result-object v12

    if-eqz v12, :cond_7

    .line 22
    invoke-virtual {v12}, Lcom/intsig/office/fc/hssf/usermodel/HSSFHyperlink;->getFirstRow()I

    move-result v13

    add-int/2addr v13, v3

    invoke-virtual {v12, v13}, Lcom/intsig/office/fc/hssf/usermodel/HSSFHyperlink;->setFirstRow(I)V

    .line 23
    invoke-virtual {v12}, Lcom/intsig/office/fc/hssf/usermodel/HSSFHyperlink;->getLastRow()I

    move-result v13

    add-int/2addr v13, v3

    invoke-virtual {v12, v13}, Lcom/intsig/office/fc/hssf/usermodel/HSSFHyperlink;->setLastRow(I)V

    goto :goto_3

    .line 24
    :cond_8
    invoke-virtual {v8}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;->removeAllCells()V

    if-eqz p6, :cond_b

    .line 25
    array-length v8, v7

    sub-int/2addr v8, v4

    :goto_4
    if-ltz v8, :cond_b

    .line 26
    aget-object v10, v7, v8

    .line 27
    invoke-virtual {v10}, Lcom/intsig/office/fc/hssf/record/NoteRecord;->getRow()I

    move-result v11

    if-eq v11, v6, :cond_9

    goto :goto_5

    .line 28
    :cond_9
    invoke-virtual {v10}, Lcom/intsig/office/fc/hssf/record/NoteRecord;->getColumn()I

    move-result v10

    invoke-virtual {p0, v6, v10}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getCellComment(II)Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;

    move-result-object v10

    if-eqz v10, :cond_a

    .line 29
    invoke-virtual {v10, v9}, Lcom/intsig/office/fc/hssf/usermodel/HSSFComment;->setRow(I)V

    :cond_a
    :goto_5
    add-int/lit8 v8, v8, -0x1

    goto :goto_4

    :cond_b
    :goto_6
    add-int/2addr v6, v5

    goto/16 :goto_2

    :cond_c
    const/4 v4, 0x0

    if-lez v3, :cond_f

    .line 30
    iget v5, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_firstrow:I

    if-ne v1, v5, :cond_e

    add-int v5, v1, v3

    .line 31
    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v6

    iput v6, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_firstrow:I

    add-int/lit8 v6, v1, 0x1

    :goto_7
    if-ge v6, v5, :cond_e

    .line 32
    invoke-virtual {p0, v6}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    move-result-object v7

    if-eqz v7, :cond_d

    .line 33
    iput v6, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_firstrow:I

    goto :goto_8

    :cond_d
    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    :cond_e
    :goto_8
    add-int v5, v2, v3

    .line 34
    iget v6, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_lastrow:I

    if-le v5, v6, :cond_12

    .line 35
    sget-object v6, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->EXCEL97:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    invoke-virtual {v6}, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->getLastRowIndex()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_lastrow:I

    goto :goto_a

    :cond_f
    add-int v5, v1, v3

    .line 36
    iget v6, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_firstrow:I

    if-ge v5, v6, :cond_10

    .line 37
    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_firstrow:I

    .line 38
    :cond_10
    iget v5, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_lastrow:I

    if-ne v2, v5, :cond_12

    add-int v5, v2, v3

    .line 39
    sget-object v6, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->EXCEL97:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    invoke-virtual {v6}, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->getLastRowIndex()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    iput v6, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_lastrow:I

    add-int/lit8 v6, v2, -0x1

    :goto_9
    if-le v6, v5, :cond_12

    .line 40
    invoke-virtual {p0, v6}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getRow(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFRow;

    move-result-object v7

    if-eqz v7, :cond_11

    .line 41
    iput v6, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_lastrow:I

    goto :goto_a

    :cond_11
    add-int/lit8 v6, v6, 0x1

    goto :goto_9

    .line 42
    :cond_12
    :goto_a
    iget-object v5, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    invoke-virtual {v5, p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetIndex(Lcom/intsig/office/fc/ss/usermodel/Sheet;)I

    move-result v5

    .line 43
    iget-object v6, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_book:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {v6, v5}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->checkExternSheet(I)S

    move-result v5

    .line 44
    invoke-static {v5, v1, v2, v3}, Lcom/intsig/office/fc/hssf/formula/FormulaShifter;->createForRowShift(IIII)Lcom/intsig/office/fc/hssf/formula/FormulaShifter;

    move-result-object v1

    .line 45
    iget-object v2, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    invoke-virtual {v2, v1, v5}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->updateFormulasAfterCellShift(Lcom/intsig/office/fc/hssf/formula/FormulaShifter;I)V

    .line 46
    iget-object v2, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getNumberOfSheets()I

    move-result v2

    :goto_b
    if-ge v4, v2, :cond_14

    .line 47
    iget-object v3, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;

    move-result-object v3

    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->getSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    move-result-object v3

    .line 48
    iget-object v5, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    if-ne v3, v5, :cond_13

    goto :goto_c

    .line 49
    :cond_13
    iget-object v5, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_book:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {v5, v4}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->checkExternSheet(I)S

    move-result v5

    .line 50
    invoke-virtual {v3, v1, v5}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->updateFormulasAfterCellShift(Lcom/intsig/office/fc/hssf/formula/FormulaShifter;I)V

    :goto_c
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 51
    :cond_14
    iget-object v2, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_workbook:Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;

    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->updateNamesAfterCellShift(Lcom/intsig/office/fc/hssf/formula/FormulaShifter;)V

    :cond_15
    return-void
.end method

.method public showInPane(SS)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setTopRow(S)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 7
    .line 8
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->setLeftCol(S)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public ungroupColumn(II)V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->groupColumnRange(IIZ)V

    return-void
.end method

.method public ungroupColumn(SS)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const v0, 0xffff

    and-int/2addr p1, v0

    and-int/2addr p2, v0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->ungroupColumn(II)V

    return-void
.end method

.method public ungroupRow(II)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;->_sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, p1, p2, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->groupRowRange(IIZ)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method protected validateColumn(I)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->EXCEL97:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->getLastColumnIndex()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-gt p1, v0, :cond_1

    .line 8
    .line 9
    if-ltz p1, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 13
    .line 14
    const-string v0, "Minimum column number is 0"

    .line 15
    .line 16
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    throw p1

    .line 20
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 21
    .line 22
    new-instance v1, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string v2, "Maximum column number is "

    .line 28
    .line 29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    throw p1
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected validateRow(I)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->EXCEL97:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->getLastRowIndex()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-gt p1, v0, :cond_1

    .line 8
    .line 9
    if-ltz p1, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 13
    .line 14
    const-string v0, "Minumum row number is 0"

    .line 15
    .line 16
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    throw p1

    .line 20
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 21
    .line 22
    new-instance v1, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string v2, "Maximum row number is "

    .line 28
    .line 29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    throw p1
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

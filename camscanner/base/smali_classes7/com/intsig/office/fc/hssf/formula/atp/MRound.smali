.class final Lcom/intsig/office/fc/hssf/formula/atp/MRound;
.super Ljava/lang/Object;
.source "MRound.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/function/FreeRefFunction;


# static fields
.field public static final 〇080:Lcom/intsig/office/fc/hssf/formula/function/FreeRefFunction;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/formula/atp/MRound;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/formula/atp/MRound;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/atp/MRound;->〇080:Lcom/intsig/office/fc/hssf/formula/function/FreeRefFunction;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public evaluate([Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;Lcom/intsig/office/fc/hssf/formula/OperationEvaluationContext;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 7

    .line 1
    array-length v0, p1

    .line 2
    const/4 v1, 0x2

    .line 3
    if-eq v0, v1, :cond_0

    .line 4
    .line 5
    sget-object p1, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->VALUE_INVALID:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 6
    .line 7
    return-object p1

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :try_start_0
    aget-object v0, p1, v0

    .line 10
    .line 11
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluationContext;->getRowIndex()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluationContext;->getColumnIndex()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->getSingleValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-static {v0}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->coerceValueToDouble(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)D

    .line 24
    .line 25
    .line 26
    move-result-wide v0

    .line 27
    const/4 v2, 0x1

    .line 28
    aget-object p1, p1, v2

    .line 29
    .line 30
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluationContext;->getRowIndex()I

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/OperationEvaluationContext;->getColumnIndex()I

    .line 35
    .line 36
    .line 37
    move-result p2

    .line 38
    invoke-static {p1, v2, p2}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->getSingleValue(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-static {p1}, Lcom/intsig/office/fc/hssf/formula/eval/OperandResolver;->coerceValueToDouble(Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;)D

    .line 43
    .line 44
    .line 45
    move-result-wide p1

    .line 46
    const-wide/16 v2, 0x0

    .line 47
    .line 48
    cmpl-double v4, p1, v2

    .line 49
    .line 50
    if-nez v4, :cond_1

    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_1
    mul-double v4, v0, p1

    .line 54
    .line 55
    cmpg-double v6, v4, v2

    .line 56
    .line 57
    if-ltz v6, :cond_2

    .line 58
    .line 59
    div-double/2addr v0, p1

    .line 60
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    .line 61
    .line 62
    .line 63
    move-result-wide v0

    .line 64
    long-to-double v0, v0

    .line 65
    mul-double v2, p1, v0

    .line 66
    .line 67
    :goto_0
    invoke-static {v2, v3}, Lcom/intsig/office/fc/hssf/formula/function/NumericFunction;->checkValue(D)V

    .line 68
    .line 69
    .line 70
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 71
    .line 72
    invoke-direct {p1, v2, v3}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;-><init>(D)V

    .line 73
    .line 74
    .line 75
    return-object p1

    .line 76
    :cond_2
    new-instance p1, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;

    .line 77
    .line 78
    sget-object p2, Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;->NUM_ERROR:Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 79
    .line 80
    invoke-direct {p1, p2}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;-><init>(Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;)V

    .line 81
    .line 82
    .line 83
    throw p1
    :try_end_0
    .catch Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :catch_0
    move-exception p1

    .line 85
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/formula/eval/EvaluationException;->getErrorEval()Lcom/intsig/office/fc/hssf/formula/eval/ErrorEval;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    return-object p1
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

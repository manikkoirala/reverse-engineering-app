.class public abstract Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;
.super Lcom/intsig/office/fc/hssf/formula/ptg/OperandPtg;
.source "AreaPtgBase.java"

# interfaces
.implements Lcom/intsig/office/fc/hssf/formula/ptg/AreaI;


# static fields
.field private static final colRelative:Lcom/intsig/office/fc/util/BitField;

.field private static final columnMask:Lcom/intsig/office/fc/util/BitField;

.field private static final rowRelative:Lcom/intsig/office/fc/util/BitField;


# instance fields
.field private field_1_first_row:I

.field private field_2_last_row:I

.field private field_3_first_column:I

.field private field_4_last_column:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const v0, 0x8000

    .line 2
    .line 3
    .line 4
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->rowRelative:Lcom/intsig/office/fc/util/BitField;

    .line 9
    .line 10
    const/16 v0, 0x4000

    .line 11
    .line 12
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->colRelative:Lcom/intsig/office/fc/util/BitField;

    .line 17
    .line 18
    const/16 v0, 0x3fff

    .line 19
    .line 20
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    sput-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->columnMask:Lcom/intsig/office/fc/util/BitField;

    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method protected constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/OperandPtg;-><init>()V

    return-void
.end method

.method protected constructor <init>(IIIIZZZZ)V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/OperandPtg;-><init>()V

    if-le p2, p1, :cond_0

    .line 14
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setFirstRow(I)V

    .line 15
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setLastRow(I)V

    .line 16
    invoke-virtual {p0, p5}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setFirstRowRelative(Z)V

    .line 17
    invoke-virtual {p0, p6}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setLastRowRelative(Z)V

    goto :goto_0

    .line 18
    :cond_0
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setFirstRow(I)V

    .line 19
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setLastRow(I)V

    .line 20
    invoke-virtual {p0, p6}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setFirstRowRelative(Z)V

    .line 21
    invoke-virtual {p0, p5}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setLastRowRelative(Z)V

    :goto_0
    if-le p4, p3, :cond_1

    .line 22
    invoke-virtual {p0, p3}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setFirstColumn(I)V

    .line 23
    invoke-virtual {p0, p4}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setLastColumn(I)V

    .line 24
    invoke-virtual {p0, p7}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setFirstColRelative(Z)V

    .line 25
    invoke-virtual {p0, p8}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setLastColRelative(Z)V

    goto :goto_1

    .line 26
    :cond_1
    invoke-virtual {p0, p4}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setFirstColumn(I)V

    .line 27
    invoke-virtual {p0, p3}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setLastColumn(I)V

    .line 28
    invoke-virtual {p0, p8}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setFirstColRelative(Z)V

    .line 29
    invoke-virtual {p0, p7}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setLastColRelative(Z)V

    :goto_1
    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/ss/util/AreaReference;)V
    .locals 3

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/OperandPtg;-><init>()V

    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/AreaReference;->getFirstCell()Lcom/intsig/office/fc/ss/util/CellReference;

    move-result-object v0

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/AreaReference;->getLastCell()Lcom/intsig/office/fc/ss/util/CellReference;

    move-result-object p1

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellReference;->getRow()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setFirstRow(I)V

    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellReference;->getCol()S

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellReference;->getCol()S

    move-result v1

    :goto_0
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setFirstColumn(I)V

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellReference;->getRow()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setLastRow(I)V

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellReference;->getCol()S

    move-result v1

    if-ne v1, v2, :cond_1

    const/16 v1, 0xff

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellReference;->getCol()S

    move-result v1

    :goto_1
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setLastColumn(I)V

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellReference;->isColAbsolute()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setFirstColRelative(Z)V

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellReference;->isColAbsolute()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setLastColRelative(Z)V

    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellReference;->isRowAbsolute()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setFirstRowRelative(Z)V

    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellReference;->isRowAbsolute()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->setLastRowRelative(Z)V

    return-void
.end method


# virtual methods
.method protected final formatReferenceAsString()Ljava/lang/String;
    .locals 6

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ss/util/CellReference;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->isFirstRowRelative()Z

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    xor-int/lit8 v3, v3, 0x1

    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->isFirstColRelative()Z

    .line 18
    .line 19
    .line 20
    move-result v4

    .line 21
    xor-int/lit8 v4, v4, 0x1

    .line 22
    .line 23
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/intsig/office/fc/ss/util/CellReference;-><init>(IIZZ)V

    .line 24
    .line 25
    .line 26
    new-instance v1, Lcom/intsig/office/fc/ss/util/CellReference;

    .line 27
    .line 28
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastRow()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastColumn()I

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->isLastRowRelative()Z

    .line 37
    .line 38
    .line 39
    move-result v4

    .line 40
    xor-int/lit8 v4, v4, 0x1

    .line 41
    .line 42
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->isLastColRelative()Z

    .line 43
    .line 44
    .line 45
    move-result v5

    .line 46
    xor-int/lit8 v5, v5, 0x1

    .line 47
    .line 48
    invoke-direct {v1, v2, v3, v4, v5}, Lcom/intsig/office/fc/ss/util/CellReference;-><init>(IIZZ)V

    .line 49
    .line 50
    .line 51
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ss/util/AreaReference;->isWholeColumnReference(Lcom/intsig/office/fc/ss/util/CellReference;Lcom/intsig/office/fc/ss/util/CellReference;)Z

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    if-eqz v2, :cond_0

    .line 56
    .line 57
    new-instance v2, Lcom/intsig/office/fc/ss/util/AreaReference;

    .line 58
    .line 59
    invoke-direct {v2, v0, v1}, Lcom/intsig/office/fc/ss/util/AreaReference;-><init>(Lcom/intsig/office/fc/ss/util/CellReference;Lcom/intsig/office/fc/ss/util/CellReference;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v2}, Lcom/intsig/office/fc/ss/util/AreaReference;->formatAsString()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    return-object v0

    .line 67
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    .line 68
    .line 69
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellReference;->formatAsString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    const-string v0, ":"

    .line 80
    .line 81
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v1}, Lcom/intsig/office/fc/ss/util/CellReference;->formatAsString()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    return-object v0
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public getDefaultOperandClass()B
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getFirstColumn()I
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->columnMask:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_3_first_column:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getFirstColumnRaw()S
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_3_first_column:I

    .line 2
    .line 3
    int-to-short v0, v0

    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getFirstRow()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_1_first_row:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getLastColumn()I
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->columnMask:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_4_last_column:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getLastColumnRaw()S
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_4_last_column:I

    .line 2
    .line 3
    int-to-short v0, v0

    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getLastRow()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_2_last_row:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final isFirstColRelative()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->colRelative:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_3_first_column:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final isFirstRowRelative()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->rowRelative:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_3_first_column:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final isLastColRelative()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->colRelative:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_4_last_column:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final isLastRowRelative()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->rowRelative:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_4_last_column:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected final notImplemented()Ljava/lang/RuntimeException;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/RuntimeException;

    .line 2
    .line 3
    const-string v1, "Coding Error: This method should never be called. This ptg should be converted"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected final readCoordinates(Lcom/intsig/office/fc/util/LittleEndianInput;)V
    .locals 1

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_1_first_row:I

    .line 6
    .line 7
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_2_last_row:I

    .line 12
    .line 13
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_3_first_column:I

    .line 18
    .line 19
    invoke-interface {p1}, Lcom/intsig/office/fc/util/LittleEndianInput;->readUShort()I

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_4_last_column:I

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public final setFirstColRelative(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->colRelative:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_3_first_column:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_3_first_column:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setFirstColumn(I)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->columnMask:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_3_first_column:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_3_first_column:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setFirstColumnRaw(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_3_first_column:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setFirstRow(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_1_first_row:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setFirstRowRelative(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->rowRelative:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_3_first_column:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_3_first_column:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setLastColRelative(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->colRelative:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_4_last_column:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_4_last_column:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setLastColumn(I)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->columnMask:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_4_last_column:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_4_last_column:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setLastColumnRaw(S)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_4_last_column:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setLastRow(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_2_last_row:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final setLastRowRelative(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->rowRelative:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_4_last_column:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_4_last_column:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toFormulaString()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->formatReferenceAsString()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected final writeCoordinates(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_1_first_row:I

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 4
    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_2_last_row:I

    .line 7
    .line 8
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 9
    .line 10
    .line 11
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_3_first_column:I

    .line 12
    .line 13
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 14
    .line 15
    .line 16
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->field_4_last_column:I

    .line 17
    .line 18
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
.end method

.class public final Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;
.super Lcom/intsig/office/fc/hssf/record/StandardRecord;
.source "ColumnInfoRecord.java"


# static fields
.field private static final collapsed:Lcom/intsig/office/fc/util/BitField;

.field private static final hidden:Lcom/intsig/office/fc/util/BitField;

.field private static final outlevel:Lcom/intsig/office/fc/util/BitField;

.field public static final sid:S = 0x7ds


# instance fields
.field private _colWidth:I

.field private _firstCol:I

.field private _lastCol:I

.field private _options:I

.field private _xfIndex:I

.field private colPixelWidth:I

.field private field_6_reserved:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    sput-object v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->hidden:Lcom/intsig/office/fc/util/BitField;

    .line 7
    .line 8
    const/16 v0, 0x700

    .line 9
    .line 10
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    sput-object v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->outlevel:Lcom/intsig/office/fc/util/BitField;

    .line 15
    .line 16
    const/16 v0, 0x1000

    .line 17
    .line 18
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    sput-object v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->collapsed:Lcom/intsig/office/fc/util/BitField;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    const/16 v0, 0x4a

    .line 2
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->colPixelWidth:I

    const/16 v0, 0x8e3

    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->setColumnWidth(I)V

    const/4 v0, 0x2

    .line 4
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    const/16 v1, 0xf

    .line 5
    iput v1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_xfIndex:I

    .line 6
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->field_6_reserved:I

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V
    .locals 3

    .line 7
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    const/16 v0, 0x4a

    .line 8
    iput v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->colPixelWidth:I

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUShort()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_firstCol:I

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUShort()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_lastCol:I

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUShort()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_colWidth:I

    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUShort()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_xfIndex:I

    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUShort()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->remaining()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 15
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readUShort()I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->field_6_reserved:I

    goto :goto_0

    .line 16
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unusual record size remaining=("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->remaining()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readByte()B

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->field_6_reserved:I

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    .line 18
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->field_6_reserved:I

    :goto_0
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_firstCol:I

    .line 7
    .line 8
    iput v1, v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_firstCol:I

    .line 9
    .line 10
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_lastCol:I

    .line 11
    .line 12
    iput v1, v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_lastCol:I

    .line 13
    .line 14
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_colWidth:I

    .line 15
    .line 16
    iput v1, v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_colWidth:I

    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_xfIndex:I

    .line 19
    .line 20
    iput v1, v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_xfIndex:I

    .line 21
    .line 22
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 23
    .line 24
    iput v1, v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 25
    .line 26
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->field_6_reserved:I

    .line 27
    .line 28
    iput v1, v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->field_6_reserved:I

    .line 29
    .line 30
    return-object v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public containsColumn(I)Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_firstCol:I

    .line 2
    .line 3
    if-gt v0, p1, :cond_0

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_lastCol:I

    .line 6
    .line 7
    if-gt p1, v0, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 p1, 0x0

    .line 12
    :goto_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public formatMatches(Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;)Z
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_xfIndex:I

    .line 2
    .line 3
    iget v1, p1, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_xfIndex:I

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-eq v0, v1, :cond_0

    .line 7
    .line 8
    return v2

    .line 9
    :cond_0
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 10
    .line 11
    iget v1, p1, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 12
    .line 13
    if-eq v0, v1, :cond_1

    .line 14
    .line 15
    return v2

    .line 16
    :cond_1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_colWidth:I

    .line 17
    .line 18
    iget p1, p1, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_colWidth:I

    .line 19
    .line 20
    if-eq v0, p1, :cond_2

    .line 21
    .line 22
    return v2

    .line 23
    :cond_2
    const/4 p1, 0x1

    .line 24
    return p1
.end method

.method public getColPixelWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->colPixelWidth:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCollapsed()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->collapsed:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getColumnWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_colWidth:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getDataSize()I
    .locals 1

    .line 1
    const/16 v0, 0xc

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFirstColumn()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_firstCol:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHidden()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->hidden:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLastColumn()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_lastCol:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOutlineLevel()I
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->outlevel:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSid()S
    .locals 1

    .line 1
    const/16 v0, 0x7d

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getXFIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_xfIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isAdjacentBefore(Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;)Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_lastCol:I

    .line 2
    .line 3
    iget p1, p1, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_firstCol:I

    .line 4
    .line 5
    const/4 v1, 0x1

    .line 6
    sub-int/2addr p1, v1

    .line 7
    if-ne v0, p1, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v1, 0x0

    .line 11
    :goto_0
    return v1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getFirstColumn()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getLastColumn()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getColumnWidth()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getXFIndex()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 27
    .line 28
    .line 29
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 30
    .line 31
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 32
    .line 33
    .line 34
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->field_6_reserved:I

    .line 35
    .line 36
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setColPixelWidth(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->colPixelWidth:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCollapsed(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->collapsed:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setColumnWidth(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_colWidth:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFirstColumn(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_firstCol:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setHidden(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->hidden:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLastColumn(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_lastCol:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setOutlineLevel(I)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->outlevel:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setXFIndex(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_xfIndex:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "[COLINFO]\n"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    const-string v1, "  colfirst = "

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getFirstColumn()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-string v1, "\n"

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string v2, "  collast  = "

    .line 29
    .line 30
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getLastColumn()I

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    const-string v2, "  colwidth = "

    .line 44
    .line 45
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getColumnWidth()I

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    const-string v2, "  xfindex  = "

    .line 59
    .line 60
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getXFIndex()I

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    const-string v2, "  options  = "

    .line 74
    .line 75
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    iget v2, p0, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->_options:I

    .line 79
    .line 80
    invoke-static {v2}, Lcom/intsig/office/fc/util/HexDump;->shortToHex(I)[C

    .line 81
    .line 82
    .line 83
    move-result-object v2

    .line 84
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    const-string v2, "    hidden   = "

    .line 91
    .line 92
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getHidden()Z

    .line 96
    .line 97
    .line 98
    move-result v2

    .line 99
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    const-string v2, "    olevel   = "

    .line 106
    .line 107
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getOutlineLevel()I

    .line 111
    .line 112
    .line 113
    move-result v2

    .line 114
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    const-string v2, "    collapsed= "

    .line 121
    .line 122
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;->getCollapsed()Z

    .line 126
    .line 127
    .line 128
    move-result v2

    .line 129
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    .line 134
    .line 135
    const-string v1, "[/COLINFO]\n"

    .line 136
    .line 137
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    return-object v0
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

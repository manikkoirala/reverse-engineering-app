.class Lcom/intsig/office/fc/hssf/formula/function/Averagea$DoubleList;
.super Ljava/lang/Object;
.source "Averagea.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hssf/formula/function/Averagea;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DoubleList"
.end annotation


# instance fields
.field private 〇080:[D

.field private 〇o00〇〇Oo:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x8

    .line 5
    .line 6
    new-array v0, v0, [D

    .line 7
    .line 8
    iput-object v0, p0, Lcom/intsig/office/fc/hssf/formula/function/Averagea$DoubleList;->〇080:[D

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    iput v0, p0, Lcom/intsig/office/fc/hssf/formula/function/Averagea$DoubleList;->〇o00〇〇Oo:I

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇o00〇〇Oo(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/function/Averagea$DoubleList;->〇080:[D

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    if-le p1, v1, :cond_0

    .line 5
    .line 6
    mul-int/lit8 p1, p1, 0x3

    .line 7
    .line 8
    div-int/lit8 p1, p1, 0x2

    .line 9
    .line 10
    new-array p1, p1, [D

    .line 11
    .line 12
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/function/Averagea$DoubleList;->〇o00〇〇Oo:I

    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-static {v0, v2, p1, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 16
    .line 17
    .line 18
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/formula/function/Averagea$DoubleList;->〇080:[D

    .line 19
    .line 20
    :cond_0
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public 〇080(D)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/function/Averagea$DoubleList;->〇o00〇〇Oo:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/formula/function/Averagea$DoubleList;->〇o00〇〇Oo(I)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/formula/function/Averagea$DoubleList;->〇080:[D

    .line 9
    .line 10
    iget v1, p0, Lcom/intsig/office/fc/hssf/formula/function/Averagea$DoubleList;->〇o00〇〇Oo:I

    .line 11
    .line 12
    aput-wide p1, v0, v1

    .line 13
    .line 14
    add-int/lit8 v1, v1, 0x1

    .line 15
    .line 16
    iput v1, p0, Lcom/intsig/office/fc/hssf/formula/function/Averagea$DoubleList;->〇o00〇〇Oo:I

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇o〇()[D
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/formula/function/Averagea$DoubleList;->〇o00〇〇Oo:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ge v0, v1, :cond_0

    .line 5
    .line 6
    sget-object v0, Lcom/intsig/office/fc/hssf/formula/function/Averagea;->EMPTY_DOUBLE_ARRAY:[D

    .line 7
    .line 8
    return-object v0

    .line 9
    :cond_0
    new-array v1, v0, [D

    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/office/fc/hssf/formula/function/Averagea$DoubleList;->〇080:[D

    .line 12
    .line 13
    const/4 v3, 0x0

    .line 14
    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 15
    .line 16
    .line 17
    return-object v1
    .line 18
    .line 19
    .line 20
    .line 21
.end method

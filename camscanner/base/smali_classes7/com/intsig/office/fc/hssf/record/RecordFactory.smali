.class public final Lcom/intsig/office/fc/hssf/record/RecordFactory;
.super Ljava/lang/Object;
.source "RecordFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hssf/record/RecordFactory$ReflectionMethodRecordCreator;,
        Lcom/intsig/office/fc/hssf/record/RecordFactory$ReflectionConstructorRecordCreator;,
        Lcom/intsig/office/fc/hssf/record/RecordFactory$I_RecordCreator;
    }
.end annotation


# static fields
.field private static final CONSTRUCTOR_ARGS:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private static final NUM_RECORDS:I = 0x200

.field private static _allKnownRecordSIDs:[S

.field private static final _recordCreatorsById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/office/fc/hssf/record/RecordFactory$I_RecordCreator;",
            ">;"
        }
    .end annotation
.end field

.field private static final recordClasses:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class<",
            "+",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v1, v0, [Ljava/lang/Class;

    .line 3
    .line 4
    const-class v2, Lcom/intsig/office/fc/hssf/record/RecordInputStream;

    .line 5
    .line 6
    const/4 v3, 0x0

    .line 7
    aput-object v2, v1, v3

    .line 8
    .line 9
    sput-object v1, Lcom/intsig/office/fc/hssf/record/RecordFactory;->CONSTRUCTOR_ARGS:[Ljava/lang/Class;

    .line 10
    .line 11
    const/16 v1, 0xa1

    .line 12
    .line 13
    new-array v1, v1, [Ljava/lang/Class;

    .line 14
    .line 15
    const-class v2, Lcom/intsig/office/fc/hssf/record/ArrayRecord;

    .line 16
    .line 17
    aput-object v2, v1, v3

    .line 18
    .line 19
    const-class v2, Lcom/intsig/office/fc/hssf/record/AutoFilterInfoRecord;

    .line 20
    .line 21
    aput-object v2, v1, v0

    .line 22
    .line 23
    const/4 v0, 0x2

    .line 24
    const-class v2, Lcom/intsig/office/fc/hssf/record/BackupRecord;

    .line 25
    .line 26
    aput-object v2, v1, v0

    .line 27
    .line 28
    const/4 v0, 0x3

    .line 29
    const-class v2, Lcom/intsig/office/fc/hssf/record/BlankRecord;

    .line 30
    .line 31
    aput-object v2, v1, v0

    .line 32
    .line 33
    const/4 v0, 0x4

    .line 34
    const-class v2, Lcom/intsig/office/fc/hssf/record/BOFRecord;

    .line 35
    .line 36
    aput-object v2, v1, v0

    .line 37
    .line 38
    const/4 v0, 0x5

    .line 39
    const-class v2, Lcom/intsig/office/fc/hssf/record/BookBoolRecord;

    .line 40
    .line 41
    aput-object v2, v1, v0

    .line 42
    .line 43
    const/4 v0, 0x6

    .line 44
    const-class v2, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;

    .line 45
    .line 46
    aput-object v2, v1, v0

    .line 47
    .line 48
    const/4 v0, 0x7

    .line 49
    const-class v2, Lcom/intsig/office/fc/hssf/record/BottomMarginRecord;

    .line 50
    .line 51
    aput-object v2, v1, v0

    .line 52
    .line 53
    const/16 v0, 0x8

    .line 54
    .line 55
    const-class v2, Lcom/intsig/office/fc/hssf/record/BoundSheetRecord;

    .line 56
    .line 57
    aput-object v2, v1, v0

    .line 58
    .line 59
    const/16 v0, 0x9

    .line 60
    .line 61
    const-class v2, Lcom/intsig/office/fc/hssf/record/CalcCountRecord;

    .line 62
    .line 63
    aput-object v2, v1, v0

    .line 64
    .line 65
    const/16 v0, 0xa

    .line 66
    .line 67
    const-class v2, Lcom/intsig/office/fc/hssf/record/CalcModeRecord;

    .line 68
    .line 69
    aput-object v2, v1, v0

    .line 70
    .line 71
    const/16 v0, 0xb

    .line 72
    .line 73
    const-class v2, Lcom/intsig/office/fc/hssf/record/CFHeaderRecord;

    .line 74
    .line 75
    aput-object v2, v1, v0

    .line 76
    .line 77
    const/16 v0, 0xc

    .line 78
    .line 79
    const-class v2, Lcom/intsig/office/fc/hssf/record/CFRuleRecord;

    .line 80
    .line 81
    aput-object v2, v1, v0

    .line 82
    .line 83
    const/16 v0, 0xd

    .line 84
    .line 85
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/ChartRecord;

    .line 86
    .line 87
    aput-object v2, v1, v0

    .line 88
    .line 89
    const/16 v0, 0xe

    .line 90
    .line 91
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/ChartTitleFormatRecord;

    .line 92
    .line 93
    aput-object v2, v1, v0

    .line 94
    .line 95
    const/16 v0, 0xf

    .line 96
    .line 97
    const-class v2, Lcom/intsig/office/fc/hssf/record/CodepageRecord;

    .line 98
    .line 99
    aput-object v2, v1, v0

    .line 100
    .line 101
    const/16 v0, 0x10

    .line 102
    .line 103
    const-class v2, Lcom/intsig/office/fc/hssf/record/ColumnInfoRecord;

    .line 104
    .line 105
    aput-object v2, v1, v0

    .line 106
    .line 107
    const/16 v0, 0x11

    .line 108
    .line 109
    const-class v2, Lcom/intsig/office/fc/hssf/record/ContinueRecord;

    .line 110
    .line 111
    aput-object v2, v1, v0

    .line 112
    .line 113
    const/16 v0, 0x12

    .line 114
    .line 115
    const-class v2, Lcom/intsig/office/fc/hssf/record/CountryRecord;

    .line 116
    .line 117
    aput-object v2, v1, v0

    .line 118
    .line 119
    const/16 v0, 0x13

    .line 120
    .line 121
    const-class v2, Lcom/intsig/office/fc/hssf/record/CRNCountRecord;

    .line 122
    .line 123
    aput-object v2, v1, v0

    .line 124
    .line 125
    const/16 v0, 0x14

    .line 126
    .line 127
    const-class v2, Lcom/intsig/office/fc/hssf/record/CRNRecord;

    .line 128
    .line 129
    aput-object v2, v1, v0

    .line 130
    .line 131
    const/16 v0, 0x15

    .line 132
    .line 133
    const-class v2, Lcom/intsig/office/fc/hssf/record/DateWindow1904Record;

    .line 134
    .line 135
    aput-object v2, v1, v0

    .line 136
    .line 137
    const/16 v0, 0x16

    .line 138
    .line 139
    const-class v2, Lcom/intsig/office/fc/hssf/record/DBCellRecord;

    .line 140
    .line 141
    aput-object v2, v1, v0

    .line 142
    .line 143
    const/16 v0, 0x17

    .line 144
    .line 145
    const-class v2, Lcom/intsig/office/fc/hssf/record/DefaultColWidthRecord;

    .line 146
    .line 147
    aput-object v2, v1, v0

    .line 148
    .line 149
    const/16 v0, 0x18

    .line 150
    .line 151
    const-class v2, Lcom/intsig/office/fc/hssf/record/DefaultRowHeightRecord;

    .line 152
    .line 153
    aput-object v2, v1, v0

    .line 154
    .line 155
    const/16 v0, 0x19

    .line 156
    .line 157
    const-class v2, Lcom/intsig/office/fc/hssf/record/DeltaRecord;

    .line 158
    .line 159
    aput-object v2, v1, v0

    .line 160
    .line 161
    const/16 v0, 0x1a

    .line 162
    .line 163
    const-class v2, Lcom/intsig/office/fc/hssf/record/DimensionsRecord;

    .line 164
    .line 165
    aput-object v2, v1, v0

    .line 166
    .line 167
    const/16 v0, 0x1b

    .line 168
    .line 169
    const-class v2, Lcom/intsig/office/fc/hssf/record/DrawingGroupRecord;

    .line 170
    .line 171
    aput-object v2, v1, v0

    .line 172
    .line 173
    const/16 v0, 0x1c

    .line 174
    .line 175
    const-class v2, Lcom/intsig/office/fc/hssf/record/DrawingRecord;

    .line 176
    .line 177
    aput-object v2, v1, v0

    .line 178
    .line 179
    const/16 v0, 0x1d

    .line 180
    .line 181
    const-class v2, Lcom/intsig/office/fc/hssf/record/DrawingSelectionRecord;

    .line 182
    .line 183
    aput-object v2, v1, v0

    .line 184
    .line 185
    const/16 v0, 0x1e

    .line 186
    .line 187
    const-class v2, Lcom/intsig/office/fc/hssf/record/DSFRecord;

    .line 188
    .line 189
    aput-object v2, v1, v0

    .line 190
    .line 191
    const/16 v0, 0x1f

    .line 192
    .line 193
    const-class v2, Lcom/intsig/office/fc/hssf/record/DVALRecord;

    .line 194
    .line 195
    aput-object v2, v1, v0

    .line 196
    .line 197
    const/16 v0, 0x20

    .line 198
    .line 199
    const-class v2, Lcom/intsig/office/fc/hssf/record/DVRecord;

    .line 200
    .line 201
    aput-object v2, v1, v0

    .line 202
    .line 203
    const/16 v0, 0x21

    .line 204
    .line 205
    const-class v2, Lcom/intsig/office/fc/hssf/record/EOFRecord;

    .line 206
    .line 207
    aput-object v2, v1, v0

    .line 208
    .line 209
    const/16 v0, 0x22

    .line 210
    .line 211
    const-class v2, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;

    .line 212
    .line 213
    aput-object v2, v1, v0

    .line 214
    .line 215
    const/16 v0, 0x23

    .line 216
    .line 217
    const-class v2, Lcom/intsig/office/fc/hssf/record/ExternalNameRecord;

    .line 218
    .line 219
    aput-object v2, v1, v0

    .line 220
    .line 221
    const/16 v0, 0x24

    .line 222
    .line 223
    const-class v2, Lcom/intsig/office/fc/hssf/record/ExternSheetRecord;

    .line 224
    .line 225
    aput-object v2, v1, v0

    .line 226
    .line 227
    const/16 v0, 0x25

    .line 228
    .line 229
    const-class v2, Lcom/intsig/office/fc/hssf/record/ExtSSTRecord;

    .line 230
    .line 231
    aput-object v2, v1, v0

    .line 232
    .line 233
    const/16 v0, 0x26

    .line 234
    .line 235
    const-class v2, Lcom/intsig/office/fc/hssf/record/FeatRecord;

    .line 236
    .line 237
    aput-object v2, v1, v0

    .line 238
    .line 239
    const/16 v0, 0x27

    .line 240
    .line 241
    const-class v2, Lcom/intsig/office/fc/hssf/record/FeatHdrRecord;

    .line 242
    .line 243
    aput-object v2, v1, v0

    .line 244
    .line 245
    const/16 v0, 0x28

    .line 246
    .line 247
    const-class v2, Lcom/intsig/office/fc/hssf/record/FilePassRecord;

    .line 248
    .line 249
    aput-object v2, v1, v0

    .line 250
    .line 251
    const/16 v0, 0x29

    .line 252
    .line 253
    const-class v2, Lcom/intsig/office/fc/hssf/record/FileSharingRecord;

    .line 254
    .line 255
    aput-object v2, v1, v0

    .line 256
    .line 257
    const/16 v0, 0x2a

    .line 258
    .line 259
    const-class v2, Lcom/intsig/office/fc/hssf/record/FnGroupCountRecord;

    .line 260
    .line 261
    aput-object v2, v1, v0

    .line 262
    .line 263
    const/16 v0, 0x2b

    .line 264
    .line 265
    const-class v2, Lcom/intsig/office/fc/hssf/record/FontRecord;

    .line 266
    .line 267
    aput-object v2, v1, v0

    .line 268
    .line 269
    const/16 v0, 0x2c

    .line 270
    .line 271
    const-class v2, Lcom/intsig/office/fc/hssf/record/FooterRecord;

    .line 272
    .line 273
    aput-object v2, v1, v0

    .line 274
    .line 275
    const/16 v0, 0x2d

    .line 276
    .line 277
    const-class v2, Lcom/intsig/office/fc/hssf/record/FormatRecord;

    .line 278
    .line 279
    aput-object v2, v1, v0

    .line 280
    .line 281
    const/16 v0, 0x2e

    .line 282
    .line 283
    const-class v2, Lcom/intsig/office/fc/hssf/record/FormulaRecord;

    .line 284
    .line 285
    aput-object v2, v1, v0

    .line 286
    .line 287
    const/16 v0, 0x2f

    .line 288
    .line 289
    const-class v2, Lcom/intsig/office/fc/hssf/record/GridsetRecord;

    .line 290
    .line 291
    aput-object v2, v1, v0

    .line 292
    .line 293
    const/16 v0, 0x30

    .line 294
    .line 295
    const-class v2, Lcom/intsig/office/fc/hssf/record/GutsRecord;

    .line 296
    .line 297
    aput-object v2, v1, v0

    .line 298
    .line 299
    const/16 v0, 0x31

    .line 300
    .line 301
    const-class v2, Lcom/intsig/office/fc/hssf/record/HCenterRecord;

    .line 302
    .line 303
    aput-object v2, v1, v0

    .line 304
    .line 305
    const/16 v0, 0x32

    .line 306
    .line 307
    const-class v2, Lcom/intsig/office/fc/hssf/record/HeaderRecord;

    .line 308
    .line 309
    aput-object v2, v1, v0

    .line 310
    .line 311
    const/16 v0, 0x33

    .line 312
    .line 313
    const-class v2, Lcom/intsig/office/fc/hssf/record/HeaderFooterRecord;

    .line 314
    .line 315
    aput-object v2, v1, v0

    .line 316
    .line 317
    const/16 v0, 0x34

    .line 318
    .line 319
    const-class v2, Lcom/intsig/office/fc/hssf/record/HideObjRecord;

    .line 320
    .line 321
    aput-object v2, v1, v0

    .line 322
    .line 323
    const/16 v0, 0x35

    .line 324
    .line 325
    const-class v2, Lcom/intsig/office/fc/hssf/record/HorizontalPageBreakRecord;

    .line 326
    .line 327
    aput-object v2, v1, v0

    .line 328
    .line 329
    const/16 v0, 0x36

    .line 330
    .line 331
    const-class v2, Lcom/intsig/office/fc/hssf/record/HyperlinkRecord;

    .line 332
    .line 333
    aput-object v2, v1, v0

    .line 334
    .line 335
    const/16 v0, 0x37

    .line 336
    .line 337
    const-class v2, Lcom/intsig/office/fc/hssf/record/IndexRecord;

    .line 338
    .line 339
    aput-object v2, v1, v0

    .line 340
    .line 341
    const/16 v0, 0x38

    .line 342
    .line 343
    const-class v2, Lcom/intsig/office/fc/hssf/record/InterfaceEndRecord;

    .line 344
    .line 345
    aput-object v2, v1, v0

    .line 346
    .line 347
    const/16 v0, 0x39

    .line 348
    .line 349
    const-class v2, Lcom/intsig/office/fc/hssf/record/InterfaceHdrRecord;

    .line 350
    .line 351
    aput-object v2, v1, v0

    .line 352
    .line 353
    const/16 v0, 0x3a

    .line 354
    .line 355
    const-class v2, Lcom/intsig/office/fc/hssf/record/IterationRecord;

    .line 356
    .line 357
    aput-object v2, v1, v0

    .line 358
    .line 359
    const/16 v0, 0x3b

    .line 360
    .line 361
    const-class v2, Lcom/intsig/office/fc/hssf/record/LabelRecord;

    .line 362
    .line 363
    aput-object v2, v1, v0

    .line 364
    .line 365
    const/16 v0, 0x3c

    .line 366
    .line 367
    const-class v2, Lcom/intsig/office/fc/hssf/record/LabelSSTRecord;

    .line 368
    .line 369
    aput-object v2, v1, v0

    .line 370
    .line 371
    const/16 v0, 0x3d

    .line 372
    .line 373
    const-class v2, Lcom/intsig/office/fc/hssf/record/LeftMarginRecord;

    .line 374
    .line 375
    aput-object v2, v1, v0

    .line 376
    .line 377
    const/16 v0, 0x3e

    .line 378
    .line 379
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/LegendRecord;

    .line 380
    .line 381
    aput-object v2, v1, v0

    .line 382
    .line 383
    const/16 v0, 0x3f

    .line 384
    .line 385
    const-class v2, Lcom/intsig/office/fc/hssf/record/MergeCellsRecord;

    .line 386
    .line 387
    aput-object v2, v1, v0

    .line 388
    .line 389
    const/16 v0, 0x40

    .line 390
    .line 391
    const-class v2, Lcom/intsig/office/fc/hssf/record/MMSRecord;

    .line 392
    .line 393
    aput-object v2, v1, v0

    .line 394
    .line 395
    const/16 v0, 0x41

    .line 396
    .line 397
    const-class v2, Lcom/intsig/office/fc/hssf/record/MulBlankRecord;

    .line 398
    .line 399
    aput-object v2, v1, v0

    .line 400
    .line 401
    const/16 v0, 0x42

    .line 402
    .line 403
    const-class v2, Lcom/intsig/office/fc/hssf/record/MulRKRecord;

    .line 404
    .line 405
    aput-object v2, v1, v0

    .line 406
    .line 407
    const/16 v0, 0x43

    .line 408
    .line 409
    const-class v2, Lcom/intsig/office/fc/hssf/record/NameRecord;

    .line 410
    .line 411
    aput-object v2, v1, v0

    .line 412
    .line 413
    const/16 v0, 0x44

    .line 414
    .line 415
    const-class v2, Lcom/intsig/office/fc/hssf/record/NameCommentRecord;

    .line 416
    .line 417
    aput-object v2, v1, v0

    .line 418
    .line 419
    const/16 v0, 0x45

    .line 420
    .line 421
    const-class v2, Lcom/intsig/office/fc/hssf/record/NoteRecord;

    .line 422
    .line 423
    aput-object v2, v1, v0

    .line 424
    .line 425
    const/16 v0, 0x46

    .line 426
    .line 427
    const-class v2, Lcom/intsig/office/fc/hssf/record/NumberRecord;

    .line 428
    .line 429
    aput-object v2, v1, v0

    .line 430
    .line 431
    const/16 v0, 0x47

    .line 432
    .line 433
    const-class v2, Lcom/intsig/office/fc/hssf/record/ObjectProtectRecord;

    .line 434
    .line 435
    aput-object v2, v1, v0

    .line 436
    .line 437
    const/16 v0, 0x48

    .line 438
    .line 439
    const-class v2, Lcom/intsig/office/fc/hssf/record/ObjRecord;

    .line 440
    .line 441
    aput-object v2, v1, v0

    .line 442
    .line 443
    const/16 v0, 0x49

    .line 444
    .line 445
    const-class v2, Lcom/intsig/office/fc/hssf/record/PaletteRecord;

    .line 446
    .line 447
    aput-object v2, v1, v0

    .line 448
    .line 449
    const/16 v0, 0x4a

    .line 450
    .line 451
    const-class v2, Lcom/intsig/office/fc/hssf/record/PaneRecord;

    .line 452
    .line 453
    aput-object v2, v1, v0

    .line 454
    .line 455
    const/16 v0, 0x4b

    .line 456
    .line 457
    const-class v2, Lcom/intsig/office/fc/hssf/record/PasswordRecord;

    .line 458
    .line 459
    aput-object v2, v1, v0

    .line 460
    .line 461
    const/16 v0, 0x4c

    .line 462
    .line 463
    const-class v2, Lcom/intsig/office/fc/hssf/record/PasswordRev4Record;

    .line 464
    .line 465
    aput-object v2, v1, v0

    .line 466
    .line 467
    const/16 v0, 0x4d

    .line 468
    .line 469
    const-class v2, Lcom/intsig/office/fc/hssf/record/PrecisionRecord;

    .line 470
    .line 471
    aput-object v2, v1, v0

    .line 472
    .line 473
    const/16 v0, 0x4e

    .line 474
    .line 475
    const-class v2, Lcom/intsig/office/fc/hssf/record/PrintGridlinesRecord;

    .line 476
    .line 477
    aput-object v2, v1, v0

    .line 478
    .line 479
    const/16 v0, 0x4f

    .line 480
    .line 481
    const-class v2, Lcom/intsig/office/fc/hssf/record/PrintHeadersRecord;

    .line 482
    .line 483
    aput-object v2, v1, v0

    .line 484
    .line 485
    const/16 v0, 0x50

    .line 486
    .line 487
    const-class v2, Lcom/intsig/office/fc/hssf/record/PrintSetupRecord;

    .line 488
    .line 489
    aput-object v2, v1, v0

    .line 490
    .line 491
    const/16 v0, 0x51

    .line 492
    .line 493
    const-class v2, Lcom/intsig/office/fc/hssf/record/ProtectionRev4Record;

    .line 494
    .line 495
    aput-object v2, v1, v0

    .line 496
    .line 497
    const/16 v0, 0x52

    .line 498
    .line 499
    const-class v2, Lcom/intsig/office/fc/hssf/record/ProtectRecord;

    .line 500
    .line 501
    aput-object v2, v1, v0

    .line 502
    .line 503
    const/16 v0, 0x53

    .line 504
    .line 505
    const-class v2, Lcom/intsig/office/fc/hssf/record/RecalcIdRecord;

    .line 506
    .line 507
    aput-object v2, v1, v0

    .line 508
    .line 509
    const/16 v0, 0x54

    .line 510
    .line 511
    const-class v2, Lcom/intsig/office/fc/hssf/record/RefModeRecord;

    .line 512
    .line 513
    aput-object v2, v1, v0

    .line 514
    .line 515
    const/16 v0, 0x55

    .line 516
    .line 517
    const-class v2, Lcom/intsig/office/fc/hssf/record/RefreshAllRecord;

    .line 518
    .line 519
    aput-object v2, v1, v0

    .line 520
    .line 521
    const/16 v0, 0x56

    .line 522
    .line 523
    const-class v2, Lcom/intsig/office/fc/hssf/record/RightMarginRecord;

    .line 524
    .line 525
    aput-object v2, v1, v0

    .line 526
    .line 527
    const/16 v0, 0x57

    .line 528
    .line 529
    const-class v2, Lcom/intsig/office/fc/hssf/record/RKRecord;

    .line 530
    .line 531
    aput-object v2, v1, v0

    .line 532
    .line 533
    const/16 v0, 0x58

    .line 534
    .line 535
    const-class v2, Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 536
    .line 537
    aput-object v2, v1, v0

    .line 538
    .line 539
    const/16 v0, 0x59

    .line 540
    .line 541
    const-class v2, Lcom/intsig/office/fc/hssf/record/SaveRecalcRecord;

    .line 542
    .line 543
    aput-object v2, v1, v0

    .line 544
    .line 545
    const/16 v0, 0x5a

    .line 546
    .line 547
    const-class v2, Lcom/intsig/office/fc/hssf/record/ScenarioProtectRecord;

    .line 548
    .line 549
    aput-object v2, v1, v0

    .line 550
    .line 551
    const/16 v0, 0x5b

    .line 552
    .line 553
    const-class v2, Lcom/intsig/office/fc/hssf/record/SelectionRecord;

    .line 554
    .line 555
    aput-object v2, v1, v0

    .line 556
    .line 557
    const/16 v0, 0x5c

    .line 558
    .line 559
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/SeriesRecord;

    .line 560
    .line 561
    aput-object v2, v1, v0

    .line 562
    .line 563
    const/16 v0, 0x5d

    .line 564
    .line 565
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 566
    .line 567
    aput-object v2, v1, v0

    .line 568
    .line 569
    const/16 v0, 0x5e

    .line 570
    .line 571
    const-class v2, Lcom/intsig/office/fc/hssf/record/SharedFormulaRecord;

    .line 572
    .line 573
    aput-object v2, v1, v0

    .line 574
    .line 575
    const/16 v0, 0x5f

    .line 576
    .line 577
    const-class v2, Lcom/intsig/office/fc/hssf/record/SSTRecord;

    .line 578
    .line 579
    aput-object v2, v1, v0

    .line 580
    .line 581
    const/16 v0, 0x60

    .line 582
    .line 583
    const-class v2, Lcom/intsig/office/fc/hssf/record/StringRecord;

    .line 584
    .line 585
    aput-object v2, v1, v0

    .line 586
    .line 587
    const/16 v0, 0x61

    .line 588
    .line 589
    const-class v2, Lcom/intsig/office/fc/hssf/record/StyleRecord;

    .line 590
    .line 591
    aput-object v2, v1, v0

    .line 592
    .line 593
    const/16 v0, 0x62

    .line 594
    .line 595
    const-class v2, Lcom/intsig/office/fc/hssf/record/SupBookRecord;

    .line 596
    .line 597
    aput-object v2, v1, v0

    .line 598
    .line 599
    const/16 v0, 0x63

    .line 600
    .line 601
    const-class v2, Lcom/intsig/office/fc/hssf/record/TabIdRecord;

    .line 602
    .line 603
    aput-object v2, v1, v0

    .line 604
    .line 605
    const/16 v0, 0x64

    .line 606
    .line 607
    const-class v2, Lcom/intsig/office/fc/hssf/record/TableRecord;

    .line 608
    .line 609
    aput-object v2, v1, v0

    .line 610
    .line 611
    const/16 v0, 0x65

    .line 612
    .line 613
    const-class v2, Lcom/intsig/office/fc/hssf/record/TableStylesRecord;

    .line 614
    .line 615
    aput-object v2, v1, v0

    .line 616
    .line 617
    const/16 v0, 0x66

    .line 618
    .line 619
    const-class v2, Lcom/intsig/office/fc/hssf/record/TextObjectRecord;

    .line 620
    .line 621
    aput-object v2, v1, v0

    .line 622
    .line 623
    const/16 v0, 0x67

    .line 624
    .line 625
    const-class v2, Lcom/intsig/office/fc/hssf/record/TopMarginRecord;

    .line 626
    .line 627
    aput-object v2, v1, v0

    .line 628
    .line 629
    const/16 v0, 0x68

    .line 630
    .line 631
    const-class v2, Lcom/intsig/office/fc/hssf/record/UncalcedRecord;

    .line 632
    .line 633
    aput-object v2, v1, v0

    .line 634
    .line 635
    const/16 v0, 0x69

    .line 636
    .line 637
    const-class v2, Lcom/intsig/office/fc/hssf/record/UseSelFSRecord;

    .line 638
    .line 639
    aput-object v2, v1, v0

    .line 640
    .line 641
    const/16 v0, 0x6a

    .line 642
    .line 643
    const-class v2, Lcom/intsig/office/fc/hssf/record/UserSViewBegin;

    .line 644
    .line 645
    aput-object v2, v1, v0

    .line 646
    .line 647
    const/16 v0, 0x6b

    .line 648
    .line 649
    const-class v2, Lcom/intsig/office/fc/hssf/record/UserSViewEnd;

    .line 650
    .line 651
    aput-object v2, v1, v0

    .line 652
    .line 653
    const/16 v0, 0x6c

    .line 654
    .line 655
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;

    .line 656
    .line 657
    aput-object v2, v1, v0

    .line 658
    .line 659
    const/16 v0, 0x6d

    .line 660
    .line 661
    const-class v2, Lcom/intsig/office/fc/hssf/record/VCenterRecord;

    .line 662
    .line 663
    aput-object v2, v1, v0

    .line 664
    .line 665
    const/16 v0, 0x6e

    .line 666
    .line 667
    const-class v2, Lcom/intsig/office/fc/hssf/record/VerticalPageBreakRecord;

    .line 668
    .line 669
    aput-object v2, v1, v0

    .line 670
    .line 671
    const/16 v0, 0x6f

    .line 672
    .line 673
    const-class v2, Lcom/intsig/office/fc/hssf/record/WindowOneRecord;

    .line 674
    .line 675
    aput-object v2, v1, v0

    .line 676
    .line 677
    const/16 v0, 0x70

    .line 678
    .line 679
    const-class v2, Lcom/intsig/office/fc/hssf/record/WindowProtectRecord;

    .line 680
    .line 681
    aput-object v2, v1, v0

    .line 682
    .line 683
    const/16 v0, 0x71

    .line 684
    .line 685
    const-class v2, Lcom/intsig/office/fc/hssf/record/WindowTwoRecord;

    .line 686
    .line 687
    aput-object v2, v1, v0

    .line 688
    .line 689
    const/16 v0, 0x72

    .line 690
    .line 691
    const-class v2, Lcom/intsig/office/fc/hssf/record/WriteAccessRecord;

    .line 692
    .line 693
    aput-object v2, v1, v0

    .line 694
    .line 695
    const/16 v0, 0x73

    .line 696
    .line 697
    const-class v2, Lcom/intsig/office/fc/hssf/record/WriteProtectRecord;

    .line 698
    .line 699
    aput-object v2, v1, v0

    .line 700
    .line 701
    const/16 v0, 0x74

    .line 702
    .line 703
    const-class v2, Lcom/intsig/office/fc/hssf/record/WSBoolRecord;

    .line 704
    .line 705
    aput-object v2, v1, v0

    .line 706
    .line 707
    const/16 v0, 0x75

    .line 708
    .line 709
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/BeginRecord;

    .line 710
    .line 711
    aput-object v2, v1, v0

    .line 712
    .line 713
    const/16 v0, 0x76

    .line 714
    .line 715
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/ChartFRTInfoRecord;

    .line 716
    .line 717
    aput-object v2, v1, v0

    .line 718
    .line 719
    const/16 v0, 0x77

    .line 720
    .line 721
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/ChartStartBlockRecord;

    .line 722
    .line 723
    aput-object v2, v1, v0

    .line 724
    .line 725
    const/16 v0, 0x78

    .line 726
    .line 727
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/ChartEndBlockRecord;

    .line 728
    .line 729
    aput-object v2, v1, v0

    .line 730
    .line 731
    const/16 v0, 0x79

    .line 732
    .line 733
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/ChartStartObjectRecord;

    .line 734
    .line 735
    aput-object v2, v1, v0

    .line 736
    .line 737
    const/16 v0, 0x7a

    .line 738
    .line 739
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/ChartEndObjectRecord;

    .line 740
    .line 741
    aput-object v2, v1, v0

    .line 742
    .line 743
    const/16 v0, 0x7b

    .line 744
    .line 745
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/CatLabRecord;

    .line 746
    .line 747
    aput-object v2, v1, v0

    .line 748
    .line 749
    const/16 v0, 0x7c

    .line 750
    .line 751
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/DataFormatRecord;

    .line 752
    .line 753
    aput-object v2, v1, v0

    .line 754
    .line 755
    const/16 v0, 0x7d

    .line 756
    .line 757
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/EndRecord;

    .line 758
    .line 759
    aput-object v2, v1, v0

    .line 760
    .line 761
    const/16 v0, 0x7e

    .line 762
    .line 763
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 764
    .line 765
    aput-object v2, v1, v0

    .line 766
    .line 767
    const/16 v0, 0x7f

    .line 768
    .line 769
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/SeriesToChartGroupRecord;

    .line 770
    .line 771
    aput-object v2, v1, v0

    .line 772
    .line 773
    const/16 v0, 0x80

    .line 774
    .line 775
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 776
    .line 777
    aput-object v2, v1, v0

    .line 778
    .line 779
    const/16 v0, 0x81

    .line 780
    .line 781
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/AreaRecord;

    .line 782
    .line 783
    aput-object v2, v1, v0

    .line 784
    .line 785
    const/16 v0, 0x82

    .line 786
    .line 787
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/AxisLineFormatRecord;

    .line 788
    .line 789
    aput-object v2, v1, v0

    .line 790
    .line 791
    const/16 v0, 0x83

    .line 792
    .line 793
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/AxisOptionsRecord;

    .line 794
    .line 795
    aput-object v2, v1, v0

    .line 796
    .line 797
    const/16 v0, 0x84

    .line 798
    .line 799
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/AxisParentRecord;

    .line 800
    .line 801
    aput-object v2, v1, v0

    .line 802
    .line 803
    const/16 v0, 0x85

    .line 804
    .line 805
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/AxisRecord;

    .line 806
    .line 807
    aput-object v2, v1, v0

    .line 808
    .line 809
    const/16 v0, 0x86

    .line 810
    .line 811
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/AxisUsedRecord;

    .line 812
    .line 813
    aput-object v2, v1, v0

    .line 814
    .line 815
    const/16 v0, 0x87

    .line 816
    .line 817
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/BarRecord;

    .line 818
    .line 819
    aput-object v2, v1, v0

    .line 820
    .line 821
    const/16 v0, 0x88

    .line 822
    .line 823
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/CategorySeriesAxisRecord;

    .line 824
    .line 825
    aput-object v2, v1, v0

    .line 826
    .line 827
    const/16 v0, 0x89

    .line 828
    .line 829
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/DatRecord;

    .line 830
    .line 831
    aput-object v2, v1, v0

    .line 832
    .line 833
    const/16 v0, 0x8a

    .line 834
    .line 835
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/DefaultDataLabelTextPropertiesRecord;

    .line 836
    .line 837
    aput-object v2, v1, v0

    .line 838
    .line 839
    const/16 v0, 0x8b

    .line 840
    .line 841
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/FontBasisRecord;

    .line 842
    .line 843
    aput-object v2, v1, v0

    .line 844
    .line 845
    const/16 v0, 0x8c

    .line 846
    .line 847
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/FontIndexRecord;

    .line 848
    .line 849
    aput-object v2, v1, v0

    .line 850
    .line 851
    const/16 v0, 0x8d

    .line 852
    .line 853
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/FrameRecord;

    .line 854
    .line 855
    aput-object v2, v1, v0

    .line 856
    .line 857
    const/16 v0, 0x8e

    .line 858
    .line 859
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;

    .line 860
    .line 861
    aput-object v2, v1, v0

    .line 862
    .line 863
    const/16 v0, 0x8f

    .line 864
    .line 865
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/NumberFormatIndexRecord;

    .line 866
    .line 867
    aput-object v2, v1, v0

    .line 868
    .line 869
    const/16 v0, 0x90

    .line 870
    .line 871
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/PlotAreaRecord;

    .line 872
    .line 873
    aput-object v2, v1, v0

    .line 874
    .line 875
    const/16 v0, 0x91

    .line 876
    .line 877
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/PlotGrowthRecord;

    .line 878
    .line 879
    aput-object v2, v1, v0

    .line 880
    .line 881
    const/16 v0, 0x92

    .line 882
    .line 883
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/SeriesLabelsRecord;

    .line 884
    .line 885
    aput-object v2, v1, v0

    .line 886
    .line 887
    const/16 v0, 0x93

    .line 888
    .line 889
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/SeriesListRecord;

    .line 890
    .line 891
    aput-object v2, v1, v0

    .line 892
    .line 893
    const/16 v0, 0x94

    .line 894
    .line 895
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/SheetPropertiesRecord;

    .line 896
    .line 897
    aput-object v2, v1, v0

    .line 898
    .line 899
    const/16 v0, 0x95

    .line 900
    .line 901
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/TickRecord;

    .line 902
    .line 903
    aput-object v2, v1, v0

    .line 904
    .line 905
    const/16 v0, 0x96

    .line 906
    .line 907
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/UnitsRecord;

    .line 908
    .line 909
    aput-object v2, v1, v0

    .line 910
    .line 911
    const/16 v0, 0x97

    .line 912
    .line 913
    const-class v2, Lcom/intsig/office/fc/hssf/record/pivottable/DataItemRecord;

    .line 914
    .line 915
    aput-object v2, v1, v0

    .line 916
    .line 917
    const/16 v0, 0x98

    .line 918
    .line 919
    const-class v2, Lcom/intsig/office/fc/hssf/record/pivottable/ExtendedPivotTableViewFieldsRecord;

    .line 920
    .line 921
    aput-object v2, v1, v0

    .line 922
    .line 923
    const/16 v0, 0x99

    .line 924
    .line 925
    const-class v2, Lcom/intsig/office/fc/hssf/record/pivottable/PageItemRecord;

    .line 926
    .line 927
    aput-object v2, v1, v0

    .line 928
    .line 929
    const/16 v0, 0x9a

    .line 930
    .line 931
    const-class v2, Lcom/intsig/office/fc/hssf/record/pivottable/StreamIDRecord;

    .line 932
    .line 933
    aput-object v2, v1, v0

    .line 934
    .line 935
    const/16 v0, 0x9b

    .line 936
    .line 937
    const-class v2, Lcom/intsig/office/fc/hssf/record/pivottable/ViewDefinitionRecord;

    .line 938
    .line 939
    aput-object v2, v1, v0

    .line 940
    .line 941
    const/16 v0, 0x9c

    .line 942
    .line 943
    const-class v2, Lcom/intsig/office/fc/hssf/record/pivottable/ViewFieldsRecord;

    .line 944
    .line 945
    aput-object v2, v1, v0

    .line 946
    .line 947
    const/16 v0, 0x9d

    .line 948
    .line 949
    const-class v2, Lcom/intsig/office/fc/hssf/record/pivottable/ViewSourceRecord;

    .line 950
    .line 951
    aput-object v2, v1, v0

    .line 952
    .line 953
    const/16 v0, 0x9e

    .line 954
    .line 955
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/DataLabelExtensionRecord;

    .line 956
    .line 957
    aput-object v2, v1, v0

    .line 958
    .line 959
    const/16 v0, 0x9f

    .line 960
    .line 961
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/TextRecord;

    .line 962
    .line 963
    aput-object v2, v1, v0

    .line 964
    .line 965
    const/16 v0, 0xa0

    .line 966
    .line 967
    const-class v2, Lcom/intsig/office/fc/hssf/record/chart/ObjectLinkRecord;

    .line 968
    .line 969
    aput-object v2, v1, v0

    .line 970
    .line 971
    sput-object v1, Lcom/intsig/office/fc/hssf/record/RecordFactory;->recordClasses:[Ljava/lang/Class;

    .line 972
    .line 973
    invoke-static {v1}, Lcom/intsig/office/fc/hssf/record/RecordFactory;->recordsToMap([Ljava/lang/Class;)Ljava/util/Map;

    .line 974
    .line 975
    .line 976
    move-result-object v0

    .line 977
    sput-object v0, Lcom/intsig/office/fc/hssf/record/RecordFactory;->_recordCreatorsById:Ljava/util/Map;

    .line 978
    .line 979
    return-void
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
    .line 1825
    .line 1826
    .line 1827
    .line 1828
    .line 1829
    .line 1830
    .line 1831
    .line 1832
    .line 1833
    .line 1834
    .line 1835
    .line 1836
    .line 1837
    .line 1838
    .line 1839
    .line 1840
    .line 1841
    .line 1842
    .line 1843
    .line 1844
    .line 1845
    .line 1846
    .line 1847
    .line 1848
    .line 1849
    .line 1850
    .line 1851
    .line 1852
    .line 1853
    .line 1854
    .line 1855
    .line 1856
    .line 1857
    .line 1858
    .line 1859
    .line 1860
    .line 1861
    .line 1862
    .line 1863
    .line 1864
    .line 1865
    .line 1866
    .line 1867
    .line 1868
    .line 1869
    .line 1870
    .line 1871
    .line 1872
    .line 1873
    .line 1874
    .line 1875
    .line 1876
    .line 1877
    .line 1878
    .line 1879
    .line 1880
    .line 1881
    .line 1882
    .line 1883
    .line 1884
    .line 1885
    .line 1886
    .line 1887
    .line 1888
    .line 1889
    .line 1890
    .line 1891
    .line 1892
    .line 1893
    .line 1894
    .line 1895
    .line 1896
    .line 1897
    .line 1898
    .line 1899
    .line 1900
    .line 1901
    .line 1902
    .line 1903
    .line 1904
    .line 1905
    .line 1906
    .line 1907
    .line 1908
    .line 1909
    .line 1910
    .line 1911
    .line 1912
    .line 1913
    .line 1914
    .line 1915
    .line 1916
    .line 1917
    .line 1918
    .line 1919
    .line 1920
    .line 1921
    .line 1922
    .line 1923
    .line 1924
    .line 1925
    .line 1926
    .line 1927
    .line 1928
    .line 1929
    .line 1930
    .line 1931
    .line 1932
    .line 1933
    .line 1934
    .line 1935
    .line 1936
    .line 1937
    .line 1938
    .line 1939
    .line 1940
    .line 1941
    .line 1942
    .line 1943
    .line 1944
    .line 1945
    .line 1946
    .line 1947
    .line 1948
    .line 1949
    .line 1950
    .line 1951
    .line 1952
    .line 1953
    .line 1954
    .line 1955
    .line 1956
    .line 1957
    .line 1958
    .line 1959
    .line 1960
    .line 1961
    .line 1962
    .line 1963
    .line 1964
    .line 1965
    .line 1966
    .line 1967
    .line 1968
    .line 1969
    .line 1970
    .line 1971
    .line 1972
    .line 1973
    .line 1974
    .line 1975
    .line 1976
    .line 1977
    .line 1978
    .line 1979
    .line 1980
    .line 1981
    .line 1982
    .line 1983
    .line 1984
    .line 1985
    .line 1986
    .line 1987
    .line 1988
    .line 1989
    .line 1990
    .line 1991
    .line 1992
    .line 1993
    .line 1994
    .line 1995
    .line 1996
    .line 1997
    .line 1998
    .line 1999
    .line 2000
    .line 2001
    .line 2002
    .line 2003
    .line 2004
    .line 2005
    .line 2006
    .line 2007
    .line 2008
    .line 2009
    .line 2010
    .line 2011
    .line 2012
    .line 2013
    .line 2014
    .line 2015
    .line 2016
    .line 2017
    .line 2018
    .line 2019
    .line 2020
    .line 2021
    .line 2022
    .line 2023
    .line 2024
    .line 2025
    .line 2026
    .line 2027
    .line 2028
    .line 2029
    .line 2030
    .line 2031
    .line 2032
    .line 2033
    .line 2034
    .line 2035
    .line 2036
    .line 2037
    .line 2038
    .line 2039
    .line 2040
    .line 2041
    .line 2042
    .line 2043
    .line 2044
    .line 2045
    .line 2046
    .line 2047
    .line 2048
    .line 2049
    .line 2050
    .line 2051
    .line 2052
    .line 2053
    .line 2054
    .line 2055
    .line 2056
    .line 2057
    .line 2058
    .line 2059
    .line 2060
    .line 2061
    .line 2062
    .line 2063
    .line 2064
    .line 2065
    .line 2066
    .line 2067
    .line 2068
    .line 2069
    .line 2070
    .line 2071
    .line 2072
    .line 2073
    .line 2074
    .line 2075
    .line 2076
    .line 2077
    .line 2078
    .line 2079
    .line 2080
    .line 2081
    .line 2082
    .line 2083
    .line 2084
    .line 2085
    .line 2086
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static convertBlankRecords(Lcom/intsig/office/fc/hssf/record/MulBlankRecord;)[Lcom/intsig/office/fc/hssf/record/BlankRecord;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/MulBlankRecord;->getNumColumns()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-array v0, v0, [Lcom/intsig/office/fc/hssf/record/BlankRecord;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/MulBlankRecord;->getNumColumns()I

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-ge v1, v2, :cond_0

    .line 13
    .line 14
    new-instance v2, Lcom/intsig/office/fc/hssf/record/BlankRecord;

    .line 15
    .line 16
    invoke-direct {v2}, Lcom/intsig/office/fc/hssf/record/BlankRecord;-><init>()V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/MulBlankRecord;->getFirstColumn()I

    .line 20
    .line 21
    .line 22
    move-result v3

    .line 23
    add-int/2addr v3, v1

    .line 24
    int-to-short v3, v3

    .line 25
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->setColumn(S)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/MulBlankRecord;->getRow()I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->setRow(I)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/record/MulBlankRecord;->getXFAt(I)S

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->setXFIndex(S)V

    .line 40
    .line 41
    .line 42
    aput-object v2, v0, v1

    .line 43
    .line 44
    add-int/lit8 v1, v1, 0x1

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    return-object v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static convertRKRecords(Lcom/intsig/office/fc/hssf/record/MulRKRecord;)[Lcom/intsig/office/fc/hssf/record/NumberRecord;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/MulRKRecord;->getNumColumns()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-array v0, v0, [Lcom/intsig/office/fc/hssf/record/NumberRecord;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/MulRKRecord;->getNumColumns()I

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-ge v1, v2, :cond_0

    .line 13
    .line 14
    new-instance v2, Lcom/intsig/office/fc/hssf/record/NumberRecord;

    .line 15
    .line 16
    invoke-direct {v2}, Lcom/intsig/office/fc/hssf/record/NumberRecord;-><init>()V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/MulRKRecord;->getFirstColumn()S

    .line 20
    .line 21
    .line 22
    move-result v3

    .line 23
    add-int/2addr v3, v1

    .line 24
    int-to-short v3, v3

    .line 25
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setColumn(S)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/MulRKRecord;->getRow()I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setRow(I)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/record/MulRKRecord;->getXFAt(I)S

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setXFIndex(S)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hssf/record/MulRKRecord;->getRKNumberAt(I)D

    .line 43
    .line 44
    .line 45
    move-result-wide v3

    .line 46
    invoke-virtual {v2, v3, v4}, Lcom/intsig/office/fc/hssf/record/NumberRecord;->setValue(D)V

    .line 47
    .line 48
    .line 49
    aput-object v2, v0, v1

    .line 50
    .line 51
    add-int/lit8 v1, v1, 0x1

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_0
    return-object v0
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static convertToNumberRecord(Lcom/intsig/office/fc/hssf/record/RKRecord;)Lcom/intsig/office/fc/hssf/record/NumberRecord;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/NumberRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/NumberRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CellRecord;->getColumn()S

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setColumn(S)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CellRecord;->getRow()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setRow(I)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/CellRecord;->getXFIndex()S

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setXFIndex(S)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/RKRecord;->getRKNumber()D

    .line 28
    .line 29
    .line 30
    move-result-wide v1

    .line 31
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hssf/record/NumberRecord;->setValue(D)V

    .line 32
    .line 33
    .line 34
    return-object v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static createRecord(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)[Lcom/intsig/office/fc/hssf/record/Record;
    .locals 3

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/record/RecordFactory;->createSingleRecord(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)Lcom/intsig/office/fc/hssf/record/Record;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/record/DBCellRecord;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    const/4 v2, 0x1

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-array p0, v2, [Lcom/intsig/office/fc/hssf/record/Record;

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    aput-object v0, p0, v1

    .line 15
    .line 16
    return-object p0

    .line 17
    :cond_0
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/record/RKRecord;

    .line 18
    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    new-array v0, v2, [Lcom/intsig/office/fc/hssf/record/Record;

    .line 22
    .line 23
    check-cast p0, Lcom/intsig/office/fc/hssf/record/RKRecord;

    .line 24
    .line 25
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/record/RecordFactory;->convertToNumberRecord(Lcom/intsig/office/fc/hssf/record/RKRecord;)Lcom/intsig/office/fc/hssf/record/NumberRecord;

    .line 26
    .line 27
    .line 28
    move-result-object p0

    .line 29
    aput-object p0, v0, v1

    .line 30
    .line 31
    return-object v0

    .line 32
    :cond_1
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/record/MulRKRecord;

    .line 33
    .line 34
    if-eqz v0, :cond_2

    .line 35
    .line 36
    check-cast p0, Lcom/intsig/office/fc/hssf/record/MulRKRecord;

    .line 37
    .line 38
    invoke-static {p0}, Lcom/intsig/office/fc/hssf/record/RecordFactory;->convertRKRecords(Lcom/intsig/office/fc/hssf/record/MulRKRecord;)[Lcom/intsig/office/fc/hssf/record/NumberRecord;

    .line 39
    .line 40
    .line 41
    move-result-object p0

    .line 42
    return-object p0

    .line 43
    :cond_2
    new-array v0, v2, [Lcom/intsig/office/fc/hssf/record/Record;

    .line 44
    .line 45
    aput-object p0, v0, v1

    .line 46
    .line 47
    return-object v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static createRecords(Ljava/io/InputStream;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/record/RecordFormatException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    invoke-static {p0, v0}, Lcom/intsig/office/fc/hssf/record/RecordFactory;->createRecords(Ljava/io/InputStream;Lcom/intsig/office/system/AbstractReader;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static createRecords(Ljava/io/InputStream;Lcom/intsig/office/system/AbstractReader;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Lcom/intsig/office/system/AbstractReader;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/record/RecordFormatException;
        }
    .end annotation

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x200

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 3
    new-instance v1, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;-><init>(Ljava/io/InputStream;Z)V

    .line 4
    :goto_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->nextRecord()Lcom/intsig/office/fc/hssf/record/Record;

    move-result-object p0

    if-eqz p0, :cond_2

    if-eqz p1, :cond_1

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/system/AbstractReader;->isAborted()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 6
    :cond_0
    new-instance p0, Lcom/intsig/office/system/AbortReaderError;

    const-string p1, "abort Reader"

    invoke-direct {p0, p1}, Lcom/intsig/office/system/AbortReaderError;-><init>(Ljava/lang/String;)V

    throw p0

    .line 7
    :cond_1
    :goto_1
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 8
    :cond_2
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/RecordFactoryInputStream;->dispose()V

    return-object v0
.end method

.method public static createSingleRecord(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)Lcom/intsig/office/fc/hssf/record/Record;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/RecordFactory;->_recordCreatorsById:Ljava/util/Map;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->getSid()S

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/office/fc/hssf/record/RecordFactory$I_RecordCreator;

    .line 16
    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    new-instance v0, Lcom/intsig/office/fc/hssf/record/UnknownRecord;

    .line 20
    .line 21
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hssf/record/UnknownRecord;-><init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V

    .line 22
    .line 23
    .line 24
    return-object v0

    .line 25
    :cond_0
    invoke-interface {v0, p0}, Lcom/intsig/office/fc/hssf/record/RecordFactory$I_RecordCreator;->〇080(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)Lcom/intsig/office/fc/hssf/record/Record;

    .line 26
    .line 27
    .line 28
    move-result-object p0

    .line 29
    return-object p0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static getAllKnownRecordSIDs()[S
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/RecordFactory;->_allKnownRecordSIDs:[S

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/office/fc/hssf/record/RecordFactory;->_recordCreatorsById:Ljava/util/Map;

    .line 6
    .line 7
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    new-array v1, v1, [S

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const/4 v2, 0x0

    .line 22
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 23
    .line 24
    .line 25
    move-result v3

    .line 26
    if-eqz v3, :cond_0

    .line 27
    .line 28
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v3

    .line 32
    check-cast v3, Ljava/lang/Integer;

    .line 33
    .line 34
    add-int/lit8 v4, v2, 0x1

    .line 35
    .line 36
    invoke-virtual {v3}, Ljava/lang/Integer;->shortValue()S

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    aput-short v3, v1, v2

    .line 41
    .line 42
    move v2, v4

    .line 43
    goto :goto_0

    .line 44
    :cond_0
    invoke-static {v1}, Ljava/util/Arrays;->sort([S)V

    .line 45
    .line 46
    .line 47
    sput-object v1, Lcom/intsig/office/fc/hssf/record/RecordFactory;->_allKnownRecordSIDs:[S

    .line 48
    .line 49
    :cond_1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/RecordFactory;->_allKnownRecordSIDs:[S

    .line 50
    .line 51
    invoke-virtual {v0}, [S->clone()Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    check-cast v0, [S

    .line 56
    .line 57
    return-object v0
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static getRecordClass(I)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/lang/Class<",
            "+",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/RecordFactory;->_recordCreatorsById:Ljava/util/Map;

    .line 2
    .line 3
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    check-cast p0, Lcom/intsig/office/fc/hssf/record/RecordFactory$I_RecordCreator;

    .line 12
    .line 13
    if-nez p0, :cond_0

    .line 14
    .line 15
    const/4 p0, 0x0

    .line 16
    return-object p0

    .line 17
    :cond_0
    invoke-interface {p0}, Lcom/intsig/office/fc/hssf/record/RecordFactory$I_RecordCreator;->〇o00〇〇Oo()Ljava/lang/Class;

    .line 18
    .line 19
    .line 20
    move-result-object p0

    .line 21
    return-object p0
    .line 22
    .line 23
    .line 24
.end method

.method private static getRecordCreator(Ljava/lang/Class;)Lcom/intsig/office/fc/hssf/record/RecordFactory$I_RecordCreator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;)",
            "Lcom/intsig/office/fc/hssf/record/RecordFactory$I_RecordCreator;"
        }
    .end annotation

    .line 1
    :try_start_0
    sget-object v0, Lcom/intsig/office/fc/hssf/record/RecordFactory;->CONSTRUCTOR_ARGS:[Ljava/lang/Class;

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    new-instance v1, Lcom/intsig/office/fc/hssf/record/RecordFactory$ReflectionConstructorRecordCreator;

    .line 8
    .line 9
    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hssf/record/RecordFactory$ReflectionConstructorRecordCreator;-><init>(Ljava/lang/reflect/Constructor;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    .line 11
    .line 12
    return-object v1

    .line 13
    :catch_0
    :try_start_1
    const-string v0, "create"

    .line 14
    .line 15
    sget-object v1, Lcom/intsig/office/fc/hssf/record/RecordFactory;->CONSTRUCTOR_ARGS:[Ljava/lang/Class;

    .line 16
    .line 17
    invoke-virtual {p0, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    new-instance v1, Lcom/intsig/office/fc/hssf/record/RecordFactory$ReflectionMethodRecordCreator;

    .line 22
    .line 23
    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hssf/record/RecordFactory$ReflectionMethodRecordCreator;-><init>(Ljava/lang/reflect/Method;)V
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    .line 24
    .line 25
    .line 26
    return-object v1

    .line 27
    :catch_1
    new-instance v0, Ljava/lang/RuntimeException;

    .line 28
    .line 29
    new-instance v1, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v2, "Failed to find constructor or create method for ("

    .line 35
    .line 36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object p0

    .line 43
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string p0, ")."

    .line 47
    .line 48
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object p0

    .line 55
    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    throw v0
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method private static recordsToMap([Ljava/lang/Class;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class<",
            "+",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/office/fc/hssf/record/RecordFactory$I_RecordCreator;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Ljava/util/HashSet;

    .line 7
    .line 8
    array-length v2, p0

    .line 9
    mul-int/lit8 v2, v2, 0x3

    .line 10
    .line 11
    div-int/lit8 v2, v2, 0x2

    .line 12
    .line 13
    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 14
    .line 15
    .line 16
    const/4 v2, 0x0

    .line 17
    :goto_0
    array-length v3, p0

    .line 18
    if-ge v2, v3, :cond_4

    .line 19
    .line 20
    aget-object v3, p0, v2

    .line 21
    .line 22
    const-class v4, Lcom/intsig/office/fc/hssf/record/Record;

    .line 23
    .line 24
    invoke-virtual {v4, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    .line 25
    .line 26
    .line 27
    move-result v4

    .line 28
    const-string v5, ")"

    .line 29
    .line 30
    if-eqz v4, :cond_3

    .line 31
    .line 32
    invoke-virtual {v3}, Ljava/lang/Class;->getModifiers()I

    .line 33
    .line 34
    .line 35
    move-result v4

    .line 36
    invoke-static {v4}, Ljava/lang/reflect/Modifier;->isAbstract(I)Z

    .line 37
    .line 38
    .line 39
    move-result v4

    .line 40
    if-nez v4, :cond_2

    .line 41
    .line 42
    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    .line 44
    .line 45
    move-result v4

    .line 46
    if-eqz v4, :cond_1

    .line 47
    .line 48
    :try_start_0
    const-string v4, "sid"

    .line 49
    .line 50
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    const/4 v6, 0x0

    .line 55
    invoke-virtual {v4, v6}, Ljava/lang/reflect/Field;->getShort(Ljava/lang/Object;)S

    .line 56
    .line 57
    .line 58
    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 60
    .line 61
    .line 62
    move-result-object v6

    .line 63
    invoke-interface {v0, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    move-result v7

    .line 67
    if-nez v7, :cond_0

    .line 68
    .line 69
    invoke-static {v3}, Lcom/intsig/office/fc/hssf/record/RecordFactory;->getRecordCreator(Ljava/lang/Class;)Lcom/intsig/office/fc/hssf/record/RecordFactory$I_RecordCreator;

    .line 70
    .line 71
    .line 72
    move-result-object v3

    .line 73
    invoke-interface {v0, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    .line 75
    .line 76
    add-int/lit8 v2, v2, 0x1

    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_0
    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    .line 81
    .line 82
    move-result-object p0

    .line 83
    check-cast p0, Lcom/intsig/office/fc/hssf/record/RecordFactory$I_RecordCreator;

    .line 84
    .line 85
    invoke-interface {p0}, Lcom/intsig/office/fc/hssf/record/RecordFactory$I_RecordCreator;->〇o00〇〇Oo()Ljava/lang/Class;

    .line 86
    .line 87
    .line 88
    move-result-object p0

    .line 89
    new-instance v0, Ljava/lang/RuntimeException;

    .line 90
    .line 91
    new-instance v1, Ljava/lang/StringBuilder;

    .line 92
    .line 93
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 94
    .line 95
    .line 96
    const-string v2, "duplicate record sid 0x"

    .line 97
    .line 98
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v2

    .line 105
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v2

    .line 109
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    const-string v2, " for classes ("

    .line 113
    .line 114
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object v2

    .line 121
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    const-string v2, ") and ("

    .line 125
    .line 126
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object p0

    .line 133
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    .line 135
    .line 136
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    .line 138
    .line 139
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 140
    .line 141
    .line 142
    move-result-object p0

    .line 143
    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 144
    .line 145
    .line 146
    throw v0

    .line 147
    :catch_0
    new-instance p0, Lcom/intsig/office/fc/hssf/record/RecordFormatException;

    .line 148
    .line 149
    const-string v0, "Unable to determine record types"

    .line 150
    .line 151
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    .line 152
    .line 153
    .line 154
    throw p0

    .line 155
    :cond_1
    new-instance p0, Ljava/lang/RuntimeException;

    .line 156
    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    .line 158
    .line 159
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 160
    .line 161
    .line 162
    const-string v1, "duplicate record class ("

    .line 163
    .line 164
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    .line 166
    .line 167
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 168
    .line 169
    .line 170
    move-result-object v1

    .line 171
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    .line 173
    .line 174
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    .line 176
    .line 177
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 178
    .line 179
    .line 180
    move-result-object v0

    .line 181
    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 182
    .line 183
    .line 184
    throw p0

    .line 185
    :cond_2
    new-instance p0, Ljava/lang/RuntimeException;

    .line 186
    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    .line 188
    .line 189
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 190
    .line 191
    .line 192
    const-string v1, "Invalid record class ("

    .line 193
    .line 194
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    .line 196
    .line 197
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 198
    .line 199
    .line 200
    move-result-object v1

    .line 201
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    .line 203
    .line 204
    const-string v1, ") - must not be abstract"

    .line 205
    .line 206
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    .line 208
    .line 209
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 210
    .line 211
    .line 212
    move-result-object v0

    .line 213
    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 214
    .line 215
    .line 216
    throw p0

    .line 217
    :cond_3
    new-instance p0, Ljava/lang/RuntimeException;

    .line 218
    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    .line 220
    .line 221
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 222
    .line 223
    .line 224
    const-string v1, "Invalid record sub-class ("

    .line 225
    .line 226
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    .line 228
    .line 229
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 230
    .line 231
    .line 232
    move-result-object v1

    .line 233
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    .line 235
    .line 236
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    .line 238
    .line 239
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 240
    .line 241
    .line 242
    move-result-object v0

    .line 243
    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 244
    .line 245
    .line 246
    throw p0

    .line 247
    :cond_4
    return-object v0
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.class public final Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;
.super Lcom/intsig/office/fc/hssf/record/StandardRecord;
.source "LineFormatRecord.java"


# static fields
.field public static final LINE_PATTERN_DARK_GRAY_PATTERN:S = 0x6s

.field public static final LINE_PATTERN_DASH:S = 0x1s

.field public static final LINE_PATTERN_DASH_DOT:S = 0x3s

.field public static final LINE_PATTERN_DASH_DOT_DOT:S = 0x4s

.field public static final LINE_PATTERN_DOT:S = 0x2s

.field public static final LINE_PATTERN_LIGHT_GRAY_PATTERN:S = 0x8s

.field public static final LINE_PATTERN_MEDIUM_GRAY_PATTERN:S = 0x7s

.field public static final LINE_PATTERN_NONE:S = 0x5s

.field public static final LINE_PATTERN_SOLID:S = 0x0s

.field public static final WEIGHT_HAIRLINE:S = -0x1s

.field public static final WEIGHT_MEDIUM:S = 0x1s

.field public static final WEIGHT_NARROW:S = 0x0s

.field public static final WEIGHT_WIDE:S = 0x2s

.field private static final auto:Lcom/intsig/office/fc/util/BitField;

.field private static final drawTicks:Lcom/intsig/office/fc/util/BitField;

.field public static final sid:S = 0x1007s

.field private static final unknown:Lcom/intsig/office/fc/util/BitField;


# instance fields
.field private field_1_lineColor:I

.field private field_2_linePattern:S

.field private field_3_weight:S

.field private field_4_format:S

.field private field_5_colourPaletteIndex:S


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    sput-object v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->auto:Lcom/intsig/office/fc/util/BitField;

    .line 7
    .line 8
    const/4 v0, 0x4

    .line 9
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    sput-object v1, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->drawTicks:Lcom/intsig/office/fc/util/BitField;

    .line 14
    .line 15
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    sput-object v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->unknown:Lcom/intsig/office/fc/util/BitField;

    .line 20
    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hssf/record/RecordInputStream;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/fc/hssf/record/StandardRecord;-><init>()V

    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_1_lineColor:I

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_2_linePattern:S

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_3_weight:S

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_4_format:S

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RecordInputStream;->readShort()S

    move-result p1

    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_5_colourPaletteIndex:S

    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;-><init>()V

    .line 4
    .line 5
    .line 6
    iget v1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_1_lineColor:I

    .line 7
    .line 8
    iput v1, v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_1_lineColor:I

    .line 9
    .line 10
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_2_linePattern:S

    .line 11
    .line 12
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_2_linePattern:S

    .line 13
    .line 14
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_3_weight:S

    .line 15
    .line 16
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_3_weight:S

    .line 17
    .line 18
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_4_format:S

    .line 19
    .line 20
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_4_format:S

    .line 21
    .line 22
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_5_colourPaletteIndex:S

    .line 23
    .line 24
    iput-short v1, v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_5_colourPaletteIndex:S

    .line 25
    .line 26
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getColourPaletteIndex()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_5_colourPaletteIndex:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected getDataSize()I
    .locals 1

    .line 1
    const/16 v0, 0xc

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFormat()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_4_format:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLineColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_1_lineColor:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLinePattern()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_2_linePattern:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSid()S
    .locals 1

    .line 1
    const/16 v0, 0x1007

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getWeight()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_3_weight:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isAuto()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->auto:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_4_format:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isDrawTicks()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->drawTicks:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_4_format:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isUnknown()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->unknown:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_4_format:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public serialize(Lcom/intsig/office/fc/util/LittleEndianOutput;)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_1_lineColor:I

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeInt(I)V

    .line 4
    .line 5
    .line 6
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_2_linePattern:S

    .line 7
    .line 8
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 9
    .line 10
    .line 11
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_3_weight:S

    .line 12
    .line 13
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 14
    .line 15
    .line 16
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_4_format:S

    .line 17
    .line 18
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 19
    .line 20
    .line 21
    iget-short v0, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_5_colourPaletteIndex:S

    .line 22
    .line 23
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/util/LittleEndianOutput;->writeShort(I)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public setAuto(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->auto:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_4_format:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setShortBoolean(SZ)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_4_format:S

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setColourPaletteIndex(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_5_colourPaletteIndex:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setDrawTicks(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->drawTicks:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_4_format:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setShortBoolean(SZ)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_4_format:S

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFormat(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_4_format:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLineColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_1_lineColor:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setLinePattern(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_2_linePattern:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setUnknown(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->unknown:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_4_format:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setShortBoolean(SZ)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_4_format:S

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setWeight(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->field_3_weight:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "[LINEFORMAT]\n"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 9
    .line 10
    .line 11
    const-string v1, "    .lineColor            = "

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 14
    .line 15
    .line 16
    const-string v1, "0x"

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->getLineColor()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    invoke-static {v2}, Lcom/intsig/office/fc/util/HexDump;->toHex(I)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 30
    .line 31
    .line 32
    const-string v2, " ("

    .line 33
    .line 34
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->getLineColor()I

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 42
    .line 43
    .line 44
    const-string v3, " )"

    .line 45
    .line 46
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 47
    .line 48
    .line 49
    const-string v4, "line.separator"

    .line 50
    .line 51
    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v5

    .line 55
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 56
    .line 57
    .line 58
    const-string v5, "    .linePattern          = "

    .line 59
    .line 60
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 64
    .line 65
    .line 66
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->getLinePattern()S

    .line 67
    .line 68
    .line 69
    move-result v5

    .line 70
    invoke-static {v5}, Lcom/intsig/office/fc/util/HexDump;->toHex(S)Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v5

    .line 74
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 75
    .line 76
    .line 77
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 78
    .line 79
    .line 80
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->getLinePattern()S

    .line 81
    .line 82
    .line 83
    move-result v5

    .line 84
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 88
    .line 89
    .line 90
    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v5

    .line 94
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 95
    .line 96
    .line 97
    const-string v5, "    .weight               = "

    .line 98
    .line 99
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 103
    .line 104
    .line 105
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->getWeight()S

    .line 106
    .line 107
    .line 108
    move-result v5

    .line 109
    invoke-static {v5}, Lcom/intsig/office/fc/util/HexDump;->toHex(S)Ljava/lang/String;

    .line 110
    .line 111
    .line 112
    move-result-object v5

    .line 113
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 117
    .line 118
    .line 119
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->getWeight()S

    .line 120
    .line 121
    .line 122
    move-result v5

    .line 123
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 124
    .line 125
    .line 126
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 127
    .line 128
    .line 129
    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object v5

    .line 133
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 134
    .line 135
    .line 136
    const-string v5, "    .format               = "

    .line 137
    .line 138
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 139
    .line 140
    .line 141
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 142
    .line 143
    .line 144
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->getFormat()S

    .line 145
    .line 146
    .line 147
    move-result v5

    .line 148
    invoke-static {v5}, Lcom/intsig/office/fc/util/HexDump;->toHex(S)Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v5

    .line 152
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 153
    .line 154
    .line 155
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 156
    .line 157
    .line 158
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->getFormat()S

    .line 159
    .line 160
    .line 161
    move-result v5

    .line 162
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 163
    .line 164
    .line 165
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 166
    .line 167
    .line 168
    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object v5

    .line 172
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 173
    .line 174
    .line 175
    const-string v5, "         .auto                     = "

    .line 176
    .line 177
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 178
    .line 179
    .line 180
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->isAuto()Z

    .line 181
    .line 182
    .line 183
    move-result v5

    .line 184
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 185
    .line 186
    .line 187
    const/16 v5, 0xa

    .line 188
    .line 189
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 190
    .line 191
    .line 192
    const-string v6, "         .drawTicks                = "

    .line 193
    .line 194
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 195
    .line 196
    .line 197
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->isDrawTicks()Z

    .line 198
    .line 199
    .line 200
    move-result v6

    .line 201
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 202
    .line 203
    .line 204
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 205
    .line 206
    .line 207
    const-string v6, "         .unknown                  = "

    .line 208
    .line 209
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 210
    .line 211
    .line 212
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->isUnknown()Z

    .line 213
    .line 214
    .line 215
    move-result v6

    .line 216
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 217
    .line 218
    .line 219
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 220
    .line 221
    .line 222
    const-string v5, "    .colourPaletteIndex   = "

    .line 223
    .line 224
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 225
    .line 226
    .line 227
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 228
    .line 229
    .line 230
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->getColourPaletteIndex()S

    .line 231
    .line 232
    .line 233
    move-result v1

    .line 234
    invoke-static {v1}, Lcom/intsig/office/fc/util/HexDump;->toHex(S)Ljava/lang/String;

    .line 235
    .line 236
    .line 237
    move-result-object v1

    .line 238
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 239
    .line 240
    .line 241
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 242
    .line 243
    .line 244
    invoke-virtual {p0}, Lcom/intsig/office/fc/hssf/record/chart/LineFormatRecord;->getColourPaletteIndex()S

    .line 245
    .line 246
    .line 247
    move-result v1

    .line 248
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 249
    .line 250
    .line 251
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 252
    .line 253
    .line 254
    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    .line 255
    .line 256
    .line 257
    move-result-object v1

    .line 258
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 259
    .line 260
    .line 261
    const-string v1, "[/LINEFORMAT]\n"

    .line 262
    .line 263
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 264
    .line 265
    .line 266
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 267
    .line 268
    .line 269
    move-result-object v0

    .line 270
    return-object v0
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

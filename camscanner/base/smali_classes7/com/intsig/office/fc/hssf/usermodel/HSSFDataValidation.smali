.class public final Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;
.super Ljava/lang/Object;
.source "HSSFDataValidation.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/DataValidation;


# instance fields
.field private _constraint:Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;

.field private _emptyCellAllowed:Z

.field private _errorStyle:I

.field private _error_text:Ljava/lang/String;

.field private _error_title:Ljava/lang/String;

.field private _prompt_text:Ljava/lang/String;

.field private _prompt_title:Ljava/lang/String;

.field private _regions:Lcom/intsig/office/fc/ss/util/CellRangeAddressList;

.field private _showErrorBox:Z

.field private _showPromptBox:Z

.field private _suppress_dropdown_arrow:Z


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/ss/util/CellRangeAddressList;Lcom/intsig/office/fc/ss/usermodel/DataValidationConstraint;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_errorStyle:I

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    iput-boolean v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_emptyCellAllowed:Z

    .line 9
    .line 10
    iput-boolean v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_suppress_dropdown_arrow:Z

    .line 11
    .line 12
    iput-boolean v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_showPromptBox:Z

    .line 13
    .line 14
    iput-boolean v1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_showErrorBox:Z

    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_regions:Lcom/intsig/office/fc/ss/util/CellRangeAddressList;

    .line 17
    .line 18
    check-cast p2, Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;

    .line 19
    .line 20
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_constraint:Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public createDVRecord(Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;)Lcom/intsig/office/fc/hssf/record/DVRecord;
    .locals 19

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget-object v1, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_constraint:Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;

    .line 4
    .line 5
    move-object/from16 v2, p1

    .line 6
    .line 7
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;->createFormulas(Lcom/intsig/office/fc/hssf/usermodel/HSSFSheet;)Lcom/intsig/office/fc/hssf/usermodel/DVConstraint$FormulaPair;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    new-instance v18, Lcom/intsig/office/fc/hssf/record/DVRecord;

    .line 12
    .line 13
    iget-object v2, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_constraint:Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;

    .line 14
    .line 15
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;->getValidationType()I

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    iget-object v2, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_constraint:Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;

    .line 20
    .line 21
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;->getOperator()I

    .line 22
    .line 23
    .line 24
    move-result v4

    .line 25
    iget v5, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_errorStyle:I

    .line 26
    .line 27
    iget-boolean v6, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_emptyCellAllowed:Z

    .line 28
    .line 29
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->getSuppressDropDownArrow()Z

    .line 30
    .line 31
    .line 32
    move-result v7

    .line 33
    iget-object v2, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_constraint:Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;

    .line 34
    .line 35
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;->getValidationType()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    const/4 v8, 0x3

    .line 40
    if-ne v2, v8, :cond_0

    .line 41
    .line 42
    iget-object v2, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_constraint:Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;

    .line 43
    .line 44
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;->getExplicitListValues()[Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    if-eqz v2, :cond_0

    .line 49
    .line 50
    const/4 v2, 0x1

    .line 51
    const/4 v8, 0x1

    .line 52
    goto :goto_0

    .line 53
    :cond_0
    const/4 v2, 0x0

    .line 54
    const/4 v8, 0x0

    .line 55
    :goto_0
    iget-boolean v9, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_showPromptBox:Z

    .line 56
    .line 57
    iget-object v10, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_prompt_title:Ljava/lang/String;

    .line 58
    .line 59
    iget-object v11, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_prompt_text:Ljava/lang/String;

    .line 60
    .line 61
    iget-boolean v12, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_showErrorBox:Z

    .line 62
    .line 63
    iget-object v13, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_error_title:Ljava/lang/String;

    .line 64
    .line 65
    iget-object v14, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_error_text:Ljava/lang/String;

    .line 66
    .line 67
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/DVConstraint$FormulaPair;->getFormula1()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 68
    .line 69
    .line 70
    move-result-object v15

    .line 71
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/DVConstraint$FormulaPair;->getFormula2()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 72
    .line 73
    .line 74
    move-result-object v16

    .line 75
    iget-object v1, v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_regions:Lcom/intsig/office/fc/ss/util/CellRangeAddressList;

    .line 76
    .line 77
    move-object/from16 v2, v18

    .line 78
    .line 79
    move-object/from16 v17, v1

    .line 80
    .line 81
    invoke-direct/range {v2 .. v17}, Lcom/intsig/office/fc/hssf/record/DVRecord;-><init>(IIIZZZZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;Lcom/intsig/office/fc/ss/util/CellRangeAddressList;)V

    .line 82
    .line 83
    .line 84
    return-object v18
    .line 85
    .line 86
    .line 87
.end method

.method public createErrorBox(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_error_title:Ljava/lang/String;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_error_text:Ljava/lang/String;

    .line 4
    .line 5
    const/4 p1, 0x1

    .line 6
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->setShowErrorBox(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public createPromptBox(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_prompt_title:Ljava/lang/String;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_prompt_text:Ljava/lang/String;

    .line 4
    .line 5
    const/4 p1, 0x1

    .line 6
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->setShowPromptBox(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public getConstraint()Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_constraint:Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getEmptyCellAllowed()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_emptyCellAllowed:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getErrorBoxText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_error_text:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getErrorBoxTitle()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_error_title:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getErrorStyle()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_errorStyle:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPromptBoxText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_prompt_text:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPromptBoxTitle()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_prompt_title:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRegions()Lcom/intsig/office/fc/ss/util/CellRangeAddressList;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_regions:Lcom/intsig/office/fc/ss/util/CellRangeAddressList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShowErrorBox()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_showErrorBox:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShowPromptBox()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_showPromptBox:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSuppressDropDownArrow()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_constraint:Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;->getValidationType()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x3

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    iget-boolean v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_suppress_dropdown_arrow:Z

    .line 11
    .line 12
    return v0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getValidationConstraint()Lcom/intsig/office/fc/ss/usermodel/DataValidationConstraint;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_constraint:Lcom/intsig/office/fc/hssf/usermodel/DVConstraint;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setEmptyCellAllowed(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_emptyCellAllowed:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setErrorStyle(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_errorStyle:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setShowErrorBox(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_showErrorBox:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setShowPromptBox(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_showPromptBox:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setSuppressDropDownArrow(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataValidation;->_suppress_dropdown_arrow:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public abstract Lcom/intsig/office/fc/POITextExtractor;
.super Ljava/lang/Object;
.source "POITextExtractor.java"


# instance fields
.field protected document:Lcom/intsig/office/fc/POIDocument;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/POIDocument;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/office/fc/POITextExtractor;->document:Lcom/intsig/office/fc/POIDocument;

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/POITextExtractor;)V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iget-object p1, p1, Lcom/intsig/office/fc/POITextExtractor;->document:Lcom/intsig/office/fc/POIDocument;

    iput-object p1, p0, Lcom/intsig/office/fc/POITextExtractor;->document:Lcom/intsig/office/fc/POIDocument;

    return-void
.end method


# virtual methods
.method public abstract getMetadataTextExtractor()Lcom/intsig/office/fc/POITextExtractor;
.end method

.method public abstract getText()Ljava/lang/String;
.end method

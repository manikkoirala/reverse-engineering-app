.class public interface abstract Lcom/intsig/login/WXNetCallBack;
.super Ljava/lang/Object;
.source "WXNetCallBack.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/login/WXNetCallBack$WXSuccessData;
    }
.end annotation


# virtual methods
.method public abstract onFail(ILjava/lang/String;)V
.end method

.method public abstract onSendAuth()V
.end method

.method public abstract onStart()V
.end method

.method public abstract onSuccess(Lcom/intsig/login/WXNetCallBack$WXSuccessData;)V
    .param p1    # Lcom/intsig/login/WXNetCallBack$WXSuccessData;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.class public interface abstract Lcom/intsig/document/DocumentLoader;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/document/DocumentLoader$b;,
        Lcom/intsig/document/DocumentLoader$c;,
        Lcom/intsig/document/DocumentLoader$a;,
        Lcom/intsig/document/DocumentLoader$d;
    }
.end annotation


# static fields
.field public static final ANNOT_CARET:I = 0xe

.field public static final ANNOT_CIRCLE:I = 0x6

.field public static final ANNOT_FILEATTACHMENT:I = 0x11

.field public static final ANNOT_FREETEXT:I = 0x3

.field public static final ANNOT_HIGHLIGHT:I = 0x9

.field public static final ANNOT_INK:I = 0xf

.field public static final ANNOT_LINE:I = 0x4

.field public static final ANNOT_LINK:I = 0x2

.field public static final ANNOT_MOVIE:I = 0x13

.field public static final ANNOT_POLYGON:I = 0x7

.field public static final ANNOT_POLYLINE:I = 0x8

.field public static final ANNOT_POPUP:I = 0x10

.field public static final ANNOT_PRINTERMARK:I = 0x16

.field public static final ANNOT_REDACT:I = 0x1c

.field public static final ANNOT_RICHMEDIA:I = 0x1a

.field public static final ANNOT_SCREEN:I = 0x15

.field public static final ANNOT_SOUND:I = 0x12

.field public static final ANNOT_SQUARE:I = 0x5

.field public static final ANNOT_SQUIGGLY:I = 0xb

.field public static final ANNOT_STAMP:I = 0xd

.field public static final ANNOT_STRIKEOUT:I = 0xc

.field public static final ANNOT_TEXT:I = 0x1

.field public static final ANNOT_THREED:I = 0x19

.field public static final ANNOT_TRAPNET:I = 0x17

.field public static final ANNOT_UNDERLINE:I = 0xa

.field public static final ANNOT_UNKNOWN:I = 0x0

.field public static final ANNOT_WATERMARK:I = 0x18

.field public static final ANNOT_WIDGET:I = 0x14

.field public static final ANNOT_XFAWIDGET:I = 0x1b

.field public static final FILE_FORMAT_ERROR:I = -0x1

.field public static final LIB_NO_AUTH:I = -0x64

.field public static final PASSWORD_ERROR:I = -0x2

.field public static final SUCCESS:I


# virtual methods
.method public abstract AddAnnotation(II[FFI)I
.end method

.method public abstract AddShapeAnnotation(Lcom/intsig/document/DocumentLoader$a;)I
.end method

.method public abstract AddStampAnnotation(Lcom/intsig/document/DocumentLoader$b;)I
.end method

.method public abstract AddTextAnnotation(ILandroid/graphics/Rect;Ljava/lang/String;FIZ)I
.end method

.method public abstract AddTextAnnotation(Lcom/intsig/document/DocumentLoader$c;)I
.end method

.method public abstract AddWatermark(Ljava/lang/String;Ljava/lang/String;II)I
.end method

.method public abstract Close()V
.end method

.method public abstract DeleteAnnot(La/b;)Z
.end method

.method public abstract GetAnnotations(I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList<",
            "La/b;",
            ">;"
        }
    .end annotation
.end method

.method public abstract GetDocumentHandler()J
.end method

.method public abstract GetMetaInfo()Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract GetOutlines()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "La/c;",
            ">;"
        }
    .end annotation
.end method

.method public abstract GetPageCount()I
.end method

.method public abstract GetPageLinks(I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList<",
            "La/d;",
            ">;"
        }
    .end annotation
.end method

.method public abstract GetPageSize(I)Landroid/util/Size;
.end method

.method public abstract GetSignatures()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "La/e;",
            ">;"
        }
    .end annotation
.end method

.method public abstract InsertImage(ILandroid/graphics/Bitmap;Landroid/graphics/RectF;I)I
.end method

.method public abstract InsertImageWatermark(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/graphics/RectF;I)I
.end method

.method public abstract OpenDocument(Ljava/io/InputStream;Ljava/lang/String;)I
.end method

.method public abstract OpenDocument(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract RenderPage(IIILandroid/graphics/RectF;ZI)Landroid/graphics/Bitmap;
.end method

.method public abstract RenderPage(IZFI)Landroid/graphics/Picture;
.end method

.method public abstract SaveAs(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract SaveTo(Ljava/io/OutputStream;Ljava/lang/String;)I
.end method

.method public abstract SearchText(Ljava/lang/String;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/document/DocumentLoader$d;",
            ">;"
        }
    .end annotation
.end method

.method public abstract SelectText(III)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/document/DocumentLoader$d;",
            ">;"
        }
    .end annotation
.end method

.method public abstract TextAtPos(IFFZ)Lcom/intsig/document/DocumentLoader$d;
.end method

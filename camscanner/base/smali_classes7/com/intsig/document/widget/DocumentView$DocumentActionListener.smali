.class public interface abstract Lcom/intsig/document/widget/DocumentView$DocumentActionListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/document/widget/DocumentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DocumentActionListener"
.end annotation


# virtual methods
.method public abstract onAnnoPlace()V
.end method

.method public abstract onAnnotDelete(II)V
.end method

.method public abstract onElementDataDelete(Lcom/intsig/document/widget/PagesView$ElementData;)V
.end method

.method public abstract onElementDataFocuse(Lcom/intsig/document/widget/PagesView$ElementData;Lcom/intsig/document/widget/PagesView$AnnotFocusState;)V
.end method

.method public abstract onFreeTextAnnotClick()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onInkPaint(I)V
.end method

.method public abstract onLinkClick(Ljava/lang/String;)Z
.end method

.method public abstract onOpen(ZI)V
.end method

.method public abstract onPageScroll(II)V
.end method

.method public abstract onSaveComplete(ZZ)V
.end method

.method public abstract onSelectTextAnnot(ILjava/lang/String;I)V
.end method

.method public abstract onTap()V
.end method

.method public abstract onTextAnnnotClick(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract requestPassword()Z
.end method

.method public abstract requestSaveDialog()Z
.end method

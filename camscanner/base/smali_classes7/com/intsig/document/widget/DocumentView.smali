.class public Lcom/intsig/document/widget/DocumentView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/intsig/document/widget/PagesView$z;
.implements Lcom/intsig/document/widget/PagesView$k;
.implements Lcom/google/android/material/tabs/TabLayout$OnTabSelectedListener;
.implements Lcom/intsig/document/widget/PagesView$w;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/document/widget/DocumentView$RenderCallback;,
        Lcom/intsig/document/widget/DocumentView$FloatingActionType;,
        Lcom/intsig/document/widget/DocumentView$FloatingActionEventCallback;,
        Lcom/intsig/document/widget/DocumentView$DocumentActionListener;,
        Lcom/intsig/document/widget/DocumentView$n;
    }
.end annotation


# static fields
.field public static final SHAPE_ANNOT_CIRCLE:I = 0x6

.field public static final SHAPE_ANNOT_SQUARE:I = 0x5


# instance fields
.field public a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/intsig/document/widget/PagesView;

.field public c:Landroid/widget/LinearLayout;

.field public d:Landroid/widget/LinearLayout;

.field public e:I

.field public f:I

.field public g:Landroid/os/Handler;

.field public h:La/b;

.field public i:Lb/a;

.field public j:Ljava/lang/String;

.field public k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

.field public l:Lcom/intsig/document/widget/DocumentView$FloatingActionEventCallback;

.field public outlines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "La/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    sget v0, Lcom/intsig/document/R$layout;->floating_bar:I

    iput v0, p0, Lcom/intsig/document/widget/DocumentView;->e:I

    sget v0, Lcom/intsig/document/R$layout;->floating_annot_bar:I

    iput v0, p0, Lcom/intsig/document/widget/DocumentView;->f:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/DocumentView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 2
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget p2, Lcom/intsig/document/R$layout;->floating_bar:I

    iput p2, p0, Lcom/intsig/document/widget/DocumentView;->e:I

    sget p2, Lcom/intsig/document/R$layout;->floating_annot_bar:I

    iput p2, p0, Lcom/intsig/document/widget/DocumentView;->f:I

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/DocumentView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget p2, Lcom/intsig/document/R$layout;->floating_bar:I

    iput p2, p0, Lcom/intsig/document/widget/DocumentView;->e:I

    sget p2, Lcom/intsig/document/R$layout;->floating_annot_bar:I

    iput p2, p0, Lcom/intsig/document/widget/DocumentView;->f:I

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/DocumentView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 4
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    sget p2, Lcom/intsig/document/R$layout;->floating_bar:I

    iput p2, p0, Lcom/intsig/document/widget/DocumentView;->e:I

    sget p2, Lcom/intsig/document/R$layout;->floating_annot_bar:I

    iput p2, p0, Lcom/intsig/document/widget/DocumentView;->f:I

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/DocumentView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public static a(Lcom/intsig/document/widget/DocumentView;Ljava/lang/String;Ljava/io/InputStream;)V
    .locals 4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 4
    new-instance v0, Landroid/widget/EditText;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    const/16 v1, 0x81

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setInputType(I)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/intsig/document/R$string;->title_input_pwd:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/intsig/document/widget/d;

    invoke-direct {v2}, Lcom/intsig/document/widget/d;-><init>()V

    const/high16 v3, 0x1040000

    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/intsig/document/widget/c;

    invoke-direct {v2, p0, v0, p1, p2}, Lcom/intsig/document/widget/c;-><init>(Lcom/intsig/document/widget/DocumentView;Landroid/widget/EditText;Ljava/lang/String;Ljava/io/InputStream;)V

    const p0, 0x104000a

    invoke-virtual {v1, p0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method


# virtual methods
.method public Close()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView;->Close()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Destroy()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView;->Destroy()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public GetMetaInfo()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->a:Ljava/util/HashMap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public InsertFreeTextAnnot(Lcom/intsig/document/widget/PagesView$TextArgs;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->InsertFreeTextAnnot(Lcom/intsig/document/widget/PagesView$TextArgs;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public InsertTextAnnot(Ljava/lang/String;Landroid/graphics/Rect;FI)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/intsig/document/widget/PagesView;->InsertTextAnnot(Ljava/lang/String;Landroid/graphics/Rect;FI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public Open(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    new-instance v1, Lcom/intsig/document/widget/DocumentView$i;

    invoke-direct {v1, p0, p1}, Lcom/intsig/document/widget/DocumentView$i;-><init>(Lcom/intsig/document/widget/DocumentView;Ljava/io/InputStream;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2, p1, p2, v1}, Lcom/intsig/document/widget/PagesView;->a(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Lcom/intsig/document/widget/PagesView$r;)Lcom/intsig/document/DocumentLoader;

    move-result-object p1

    check-cast p1, Lb/a;

    iput-object p1, p0, Lcom/intsig/document/widget/DocumentView;->i:Lb/a;

    invoke-virtual {p0}, Lcom/intsig/document/widget/DocumentView;->c()V

    return-void
.end method

.method public Open(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    new-instance v1, Lcom/intsig/document/widget/DocumentView$k;

    invoke-direct {v1, p0, p1}, Lcom/intsig/document/widget/DocumentView$k;-><init>(Lcom/intsig/document/widget/DocumentView;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2, p2, v1}, Lcom/intsig/document/widget/PagesView;->a(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Lcom/intsig/document/widget/PagesView$r;)Lcom/intsig/document/DocumentLoader;

    move-result-object p1

    check-cast p1, Lb/a;

    iput-object p1, p0, Lcom/intsig/document/widget/DocumentView;->i:Lb/a;

    invoke-virtual {p0}, Lcom/intsig/document/widget/DocumentView;->c()V

    return-void
.end method

.method public SaveToFile(Ljava/io/File;Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    :goto_0
    invoke-virtual {v0, p1, p2, p0}, Lcom/intsig/document/widget/PagesView;->Save(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/document/widget/PagesView$w;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public SaveToStream(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p0}, Lcom/intsig/document/widget/PagesView;->SaveToStream(Ljava/io/OutputStream;Ljava/lang/String;Lcom/intsig/document/widget/PagesView$w;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public ScrollTo(ILandroid/graphics/RectF;Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/document/widget/PagesView;->ScrollTo(ILandroid/graphics/RectF;Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public Search(Ljava/lang/String;Lcom/intsig/document/widget/DocumentView$n;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/document/widget/DocumentView$l;

    .line 4
    .line 5
    invoke-direct {v1, p0, p2}, Lcom/intsig/document/widget/DocumentView$l;-><init>(Lcom/intsig/document/widget/DocumentView;Lcom/intsig/document/widget/DocumentView$n;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, p1, v1}, Lcom/intsig/document/widget/PagesView;->Search(Ljava/lang/String;Lcom/intsig/document/widget/PagesView$x;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public SetActionListener(Lcom/intsig/document/widget/DocumentView$DocumentActionListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final a()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->d:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 2

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/intsig/document/R$dimen;->one_dp:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->g:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/intsig/document/widget/DocumentView;->g:Landroid/os/Handler;

    :cond_0
    new-instance v0, Lcom/intsig/document/widget/PagesView;

    invoke-direct {v0, p1}, Lcom/intsig/document/widget/PagesView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/DocumentView;->build(Landroid/content/Context;)V

    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    invoke-virtual {p1, p0}, Lcom/intsig/document/widget/PagesView;->setTextSelectListener(Lcom/intsig/document/widget/PagesView$z;)V

    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    invoke-virtual {p1, p0}, Lcom/intsig/document/widget/PagesView;->setAnnotListener(Lcom/intsig/document/widget/PagesView$k;)V

    return-void
.end method

.method public final a(Landroid/view/View;ILandroid/view/View$OnClickListener;)V
    .locals 0

    .line 3
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    const/16 v1, 0x8

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public beginImageWatermark(Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->beginImageWatermark(Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public beginInkAnnot(Lcom/intsig/document/widget/PagesView$InkArgs;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->beginInkAnnot(Lcom/intsig/document/widget/PagesView$InkArgs;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public beginInsertImage(Lcom/intsig/document/widget/PagesView$ImageArgs;)Lcom/intsig/document/widget/PagesView$ElementData;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/intsig/document/widget/PagesView;->beginInsertImage(Lcom/intsig/document/widget/PagesView$ImageArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;

    move-result-object p1

    return-object p1
.end method

.method public beginInsertImage(Lcom/intsig/document/widget/PagesView$ImageArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/document/widget/PagesView;->beginInsertImage(Lcom/intsig/document/widget/PagesView$ImageArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;

    move-result-object p1

    return-object p1
.end method

.method public beginShapeAnnot(Lcom/intsig/document/widget/PagesView$ShapeArgs;)Lcom/intsig/document/widget/PagesView$ElementData;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/intsig/document/widget/PagesView;->beginShapeAnnot(Lcom/intsig/document/widget/PagesView$ShapeArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;

    move-result-object p1

    return-object p1
.end method

.method public beginShapeAnnot(Lcom/intsig/document/widget/PagesView$ShapeArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/document/widget/PagesView;->beginShapeAnnot(Lcom/intsig/document/widget/PagesView$ShapeArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;

    move-result-object p1

    return-object p1
.end method

.method public beginStampAnnot(Lcom/intsig/document/widget/PagesView$StampArgs;)Lcom/intsig/document/widget/PagesView$ElementData;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/intsig/document/widget/PagesView;->beginStampAnnot(Lcom/intsig/document/widget/PagesView$StampArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;

    move-result-object p1

    return-object p1
.end method

.method public beginStampAnnot(Lcom/intsig/document/widget/PagesView$StampArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/document/widget/PagesView;->beginStampAnnot(Lcom/intsig/document/widget/PagesView$StampArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;

    move-result-object p1

    return-object p1
.end method

.method public beginTextStampAnnot(Lcom/intsig/document/widget/PagesView$TextArgs;)Lcom/intsig/document/widget/PagesView$ElementData;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/intsig/document/widget/PagesView;->beginTextStampAnnot(Lcom/intsig/document/widget/PagesView$TextArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;

    move-result-object p1

    return-object p1
.end method

.method public beginTextStampAnnot(Lcom/intsig/document/widget/PagesView$TextArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/document/widget/PagesView;->beginTextStampAnnot(Lcom/intsig/document/widget/PagesView$TextArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;

    move-result-object p1

    return-object p1
.end method

.method public beginWatermark(Lcom/intsig/document/widget/PagesView$WatermarkArgs;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->beginWatermark(Lcom/intsig/document/widget/PagesView$WatermarkArgs;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public build(Landroid/content/Context;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iget v1, p0, Lcom/intsig/document/widget/DocumentView;->e:I

    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    check-cast v0, Landroid/widget/LinearLayout;

    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 22
    .line 23
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 24
    .line 25
    const/4 v1, -0x2

    .line 26
    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 27
    .line 28
    .line 29
    iget-object v3, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 30
    .line 31
    const/4 v4, 0x4

    .line 32
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 33
    .line 34
    .line 35
    iget-object v3, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 36
    .line 37
    sget v5, Lcom/intsig/document/R$id;->btn_copy:I

    .line 38
    .line 39
    invoke-virtual {p0, v3, v5, p0}, Lcom/intsig/document/widget/DocumentView;->a(Landroid/view/View;ILandroid/view/View$OnClickListener;)V

    .line 40
    .line 41
    .line 42
    iget-object v3, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 43
    .line 44
    sget v5, Lcom/intsig/document/R$id;->btn_web_search:I

    .line 45
    .line 46
    invoke-virtual {p0, v3, v5, p0}, Lcom/intsig/document/widget/DocumentView;->a(Landroid/view/View;ILandroid/view/View$OnClickListener;)V

    .line 47
    .line 48
    .line 49
    iget-object v3, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 50
    .line 51
    sget v5, Lcom/intsig/document/R$id;->btn_deleteline:I

    .line 52
    .line 53
    invoke-virtual {p0, v3, v5, p0}, Lcom/intsig/document/widget/DocumentView;->a(Landroid/view/View;ILandroid/view/View$OnClickListener;)V

    .line 54
    .line 55
    .line 56
    iget-object v3, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 57
    .line 58
    sget v5, Lcom/intsig/document/R$id;->btn_highline:I

    .line 59
    .line 60
    invoke-virtual {p0, v3, v5, p0}, Lcom/intsig/document/widget/DocumentView;->a(Landroid/view/View;ILandroid/view/View$OnClickListener;)V

    .line 61
    .line 62
    .line 63
    iget-object v3, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 64
    .line 65
    sget v5, Lcom/intsig/document/R$id;->btn_underline:I

    .line 66
    .line 67
    invoke-virtual {p0, v3, v5, p0}, Lcom/intsig/document/widget/DocumentView;->a(Landroid/view/View;ILandroid/view/View$OnClickListener;)V

    .line 68
    .line 69
    .line 70
    iget-object v3, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 71
    .line 72
    sget v5, Lcom/intsig/document/R$id;->btn_squiggly:I

    .line 73
    .line 74
    invoke-virtual {p0, v3, v5, p0}, Lcom/intsig/document/widget/DocumentView;->a(Landroid/view/View;ILandroid/view/View$OnClickListener;)V

    .line 75
    .line 76
    .line 77
    iget-object v3, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 78
    .line 79
    invoke-virtual {p0, v3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->d:Landroid/widget/LinearLayout;

    .line 83
    .line 84
    if-eqz v0, :cond_1

    .line 85
    .line 86
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 87
    .line 88
    .line 89
    :cond_1
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    iget v0, p0, Lcom/intsig/document/widget/DocumentView;->f:I

    .line 94
    .line 95
    invoke-virtual {p1, v0, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    check-cast p1, Landroid/widget/LinearLayout;

    .line 100
    .line 101
    iput-object p1, p0, Lcom/intsig/document/widget/DocumentView;->d:Landroid/widget/LinearLayout;

    .line 102
    .line 103
    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 104
    .line 105
    invoke-direct {p1, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 106
    .line 107
    .line 108
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->d:Landroid/widget/LinearLayout;

    .line 109
    .line 110
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 111
    .line 112
    .line 113
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->d:Landroid/widget/LinearLayout;

    .line 114
    .line 115
    sget v1, Lcom/intsig/document/R$id;->btn_annot_del:I

    .line 116
    .line 117
    invoke-virtual {p0, v0, v1, p0}, Lcom/intsig/document/widget/DocumentView;->a(Landroid/view/View;ILandroid/view/View$OnClickListener;)V

    .line 118
    .line 119
    .line 120
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->d:Landroid/widget/LinearLayout;

    .line 121
    .line 122
    sget v1, Lcom/intsig/document/R$id;->btn_annot_style:I

    .line 123
    .line 124
    invoke-virtual {p0, v0, v1, p0}, Lcom/intsig/document/widget/DocumentView;->a(Landroid/view/View;ILandroid/view/View$OnClickListener;)V

    .line 125
    .line 126
    .line 127
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->d:Landroid/widget/LinearLayout;

    .line 128
    .line 129
    invoke-virtual {p0, v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 130
    .line 131
    .line 132
    return-void
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public final c()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->i:Lb/a;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lb/a;->GetMetaInfo()Ljava/util/HashMap;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/document/widget/DocumentView;->a:Ljava/util/HashMap;

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->i:Lb/a;

    .line 12
    .line 13
    invoke-virtual {v0}, Lb/a;->GetOutlines()Ljava/util/ArrayList;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iput-object v0, p0, Lcom/intsig/document/widget/DocumentView;->outlines:Ljava/util/ArrayList;

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView;->GetPageCount()I

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 25
    .line 26
    new-instance v1, Lcom/intsig/document/widget/DocumentView$j;

    .line 27
    .line 28
    invoke-direct {v1}, Lcom/intsig/document/widget/DocumentView$j;-><init>()V

    .line 29
    .line 30
    .line 31
    const/16 v2, 0x80

    .line 32
    .line 33
    invoke-virtual {v0, v2, v1}, Lcom/intsig/document/widget/PagesView;->GetThumbnails(ILcom/intsig/document/widget/PagesView$b0;)I

    .line 34
    .line 35
    .line 36
    :cond_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public cancleAnnotClick()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/document/widget/DocumentView;->h:La/b;

    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/document/widget/DocumentView;->a()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public closeSearch()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView;->CloseSearch()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public doneAllElements(Z)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->doneAllElements(Z)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public doneElement(Lcom/intsig/document/widget/PagesView$ElementData;Z)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/document/widget/PagesView;->doneElement(Lcom/intsig/document/widget/PagesView$ElementData;Z)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public doneImageWatermark(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->doneImageWatermark(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public doneInkAnnot(Z)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->doneInkAnnot(Z)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public doneWatermark(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->doneWatermark(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public enableAnnotOperate(Z)Lcom/intsig/document/widget/DocumentView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->enableAnnotOperate(Z)V

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public enableEdit(Z)Lcom/intsig/document/widget/DocumentView;
    .locals 0

    .line 1
    return-object p0
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public enableTextSelection(Z)Lcom/intsig/document/widget/DocumentView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->enableTextSelection(Z)V

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getAnnotBox()Lcom/intsig/document/widget/PagesView$AnnotBox;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView;->getAnnotBox()Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getInkCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView;->getInkCount()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onAnnoBoxPlace(Lcom/intsig/document/widget/PagesView$AnnotBox;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    invoke-interface {p1}, Lcom/intsig/document/widget/DocumentView$DocumentActionListener;->onAnnoPlace()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onAnnotBoxClick()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/document/widget/DocumentView$DocumentActionListener;->onFreeTextAnnotClick()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onAnnotClick(La/b;Landroid/graphics/PointF;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/intsig/document/widget/DocumentView;->h:La/b;

    .line 2
    .line 3
    const-string v0, "onAnnotClick "

    .line 4
    .line 5
    invoke-static {v0}, La/a;->〇080(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget v1, p1, La/b;->id:I

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string v1, ","

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    iget v1, p1, La/b;->type:I

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string v1, " "

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    iget-object p1, p1, La/b;->text:Ljava/lang/String;

    .line 30
    .line 31
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    iget p1, p2, Landroid/graphics/PointF;->x:F

    .line 35
    .line 36
    iget p2, p2, Landroid/graphics/PointF;->y:F

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->d:Landroid/widget/LinearLayout;

    .line 39
    .line 40
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/document/widget/DocumentView;->d:Landroid/widget/LinearLayout;

    .line 47
    .line 48
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    iget-object v2, p0, Lcom/intsig/document/widget/DocumentView;->d:Landroid/widget/LinearLayout;

    .line 53
    .line 54
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    div-int/lit8 v1, v1, 0x2

    .line 59
    .line 60
    int-to-float v1, v1

    .line 61
    sub-float/2addr p1, v1

    .line 62
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    iput p1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 67
    .line 68
    int-to-float p1, v2

    .line 69
    sub-float/2addr p2, p1

    .line 70
    const/high16 p1, 0x41100000    # 9.0f

    .line 71
    .line 72
    sub-float/2addr p2, p1

    .line 73
    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    .line 74
    .line 75
    .line 76
    move-result p1

    .line 77
    iput p1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 78
    .line 79
    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->d:Landroid/widget/LinearLayout;

    .line 80
    .line 81
    const/16 p2, 0x8

    .line 82
    .line 83
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 84
    .line 85
    .line 86
    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->d:Landroid/widget/LinearLayout;

    .line 87
    .line 88
    const/4 p2, 0x0

    .line 89
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 90
    .line 91
    .line 92
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public onBackPressed()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView;->isChanged()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_2

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-interface {v0}, Lcom/intsig/document/widget/DocumentView$DocumentActionListener;->requestSaveDialog()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v0, 0x0

    .line 20
    :goto_0
    if-nez v0, :cond_1

    .line 21
    .line 22
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .line 23
    .line 24
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    sget v1, Lcom/intsig/document/R$string;->dlg_save_title:I

    .line 36
    .line 37
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    sget v1, Lcom/intsig/document/R$string;->dlg_save_message:I

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    sget v1, Lcom/intsig/document/R$string;->dlg_save_btn_ignore:I

    .line 48
    .line 49
    new-instance v2, Lcom/intsig/document/widget/DocumentView$g;

    .line 50
    .line 51
    invoke-direct {v2, p0}, Lcom/intsig/document/widget/DocumentView$g;-><init>(Lcom/intsig/document/widget/DocumentView;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    sget v1, Lcom/intsig/document/R$string;->dlg_save_btn_save:I

    .line 59
    .line 60
    new-instance v2, Lcom/intsig/document/widget/DocumentView$f;

    .line 61
    .line 62
    invoke-direct {v2, p0}, Lcom/intsig/document/widget/DocumentView$f;-><init>(Lcom/intsig/document/widget/DocumentView;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 70
    .line 71
    .line 72
    :cond_1
    const/4 v0, 0x1

    .line 73
    return v0

    .line 74
    :cond_2
    return v1
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    sget v0, Lcom/intsig/document/R$id;->btn_copy:I

    .line 6
    .line 7
    if-ne p1, v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    const-string v0, "clipboard"

    .line 14
    .line 15
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    check-cast p1, Landroid/content/ClipboardManager;

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->j:Ljava/lang/String;

    .line 22
    .line 23
    const/4 v1, 0x0

    .line 24
    invoke-static {v1, v0}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {p1, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/document/widget/PagesView;->a()V

    .line 34
    .line 35
    .line 36
    invoke-virtual {p0}, Lcom/intsig/document/widget/DocumentView;->b()V

    .line 37
    .line 38
    .line 39
    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->l:Lcom/intsig/document/widget/DocumentView$FloatingActionEventCallback;

    .line 40
    .line 41
    if-eqz p1, :cond_9

    .line 42
    .line 43
    sget-object v0, Lcom/intsig/document/widget/DocumentView$FloatingActionType;->Copy:Lcom/intsig/document/widget/DocumentView$FloatingActionType;

    .line 44
    .line 45
    iget-object v1, p0, Lcom/intsig/document/widget/DocumentView;->j:Ljava/lang/String;

    .line 46
    .line 47
    invoke-interface {p1, v0, v1}, Lcom/intsig/document/widget/DocumentView$FloatingActionEventCallback;->OnFloatingAction(Lcom/intsig/document/widget/DocumentView$FloatingActionType;Ljava/lang/String;)Z

    .line 48
    .line 49
    .line 50
    goto/16 :goto_3

    .line 51
    .line 52
    :cond_0
    sget v0, Lcom/intsig/document/R$id;->btn_highline:I

    .line 53
    .line 54
    if-ne p1, v0, :cond_1

    .line 55
    .line 56
    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 57
    .line 58
    const/16 v0, 0x9

    .line 59
    .line 60
    invoke-virtual {p1, v0}, Lcom/intsig/document/widget/PagesView;->CreateAnnotation(I)I

    .line 61
    .line 62
    .line 63
    move-result p1

    .line 64
    iget-object v1, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 65
    .line 66
    invoke-virtual {v1}, Lcom/intsig/document/widget/PagesView;->a()V

    .line 67
    .line 68
    .line 69
    invoke-virtual {p0}, Lcom/intsig/document/widget/DocumentView;->b()V

    .line 70
    .line 71
    .line 72
    iget-object v1, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 73
    .line 74
    if-eqz v1, :cond_9

    .line 75
    .line 76
    :goto_0
    iget-object v2, p0, Lcom/intsig/document/widget/DocumentView;->j:Ljava/lang/String;

    .line 77
    .line 78
    invoke-interface {v1, p1, v2, v0}, Lcom/intsig/document/widget/DocumentView$DocumentActionListener;->onSelectTextAnnot(ILjava/lang/String;I)V

    .line 79
    .line 80
    .line 81
    goto/16 :goto_3

    .line 82
    .line 83
    :cond_1
    sget v0, Lcom/intsig/document/R$id;->btn_underline:I

    .line 84
    .line 85
    if-ne p1, v0, :cond_2

    .line 86
    .line 87
    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 88
    .line 89
    const/16 v0, 0xa

    .line 90
    .line 91
    invoke-virtual {p1, v0}, Lcom/intsig/document/widget/PagesView;->CreateAnnotation(I)I

    .line 92
    .line 93
    .line 94
    move-result p1

    .line 95
    iget-object v1, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 96
    .line 97
    invoke-virtual {v1}, Lcom/intsig/document/widget/PagesView;->a()V

    .line 98
    .line 99
    .line 100
    invoke-virtual {p0}, Lcom/intsig/document/widget/DocumentView;->b()V

    .line 101
    .line 102
    .line 103
    iget-object v1, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 104
    .line 105
    if-eqz v1, :cond_9

    .line 106
    .line 107
    goto :goto_0

    .line 108
    :cond_2
    sget v0, Lcom/intsig/document/R$id;->btn_squiggly:I

    .line 109
    .line 110
    if-ne p1, v0, :cond_3

    .line 111
    .line 112
    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 113
    .line 114
    const/16 v0, 0xb

    .line 115
    .line 116
    invoke-virtual {p1, v0}, Lcom/intsig/document/widget/PagesView;->CreateAnnotation(I)I

    .line 117
    .line 118
    .line 119
    move-result p1

    .line 120
    iget-object v1, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 121
    .line 122
    invoke-virtual {v1}, Lcom/intsig/document/widget/PagesView;->a()V

    .line 123
    .line 124
    .line 125
    invoke-virtual {p0}, Lcom/intsig/document/widget/DocumentView;->b()V

    .line 126
    .line 127
    .line 128
    iget-object v1, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 129
    .line 130
    if-eqz v1, :cond_9

    .line 131
    .line 132
    goto :goto_0

    .line 133
    :cond_3
    sget v0, Lcom/intsig/document/R$id;->btn_deleteline:I

    .line 134
    .line 135
    if-ne p1, v0, :cond_4

    .line 136
    .line 137
    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 138
    .line 139
    const/16 v0, 0xc

    .line 140
    .line 141
    invoke-virtual {p1, v0}, Lcom/intsig/document/widget/PagesView;->CreateAnnotation(I)I

    .line 142
    .line 143
    .line 144
    move-result p1

    .line 145
    iget-object v1, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 146
    .line 147
    invoke-virtual {v1}, Lcom/intsig/document/widget/PagesView;->a()V

    .line 148
    .line 149
    .line 150
    invoke-virtual {p0}, Lcom/intsig/document/widget/DocumentView;->b()V

    .line 151
    .line 152
    .line 153
    iget-object v1, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 154
    .line 155
    if-eqz v1, :cond_9

    .line 156
    .line 157
    goto :goto_0

    .line 158
    :cond_4
    sget v0, Lcom/intsig/document/R$id;->btn_web_search:I

    .line 159
    .line 160
    if-ne p1, v0, :cond_6

    .line 161
    .line 162
    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 163
    .line 164
    invoke-virtual {p1}, Lcom/intsig/document/widget/PagesView;->a()V

    .line 165
    .line 166
    .line 167
    invoke-virtual {p0}, Lcom/intsig/document/widget/DocumentView;->b()V

    .line 168
    .line 169
    .line 170
    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->l:Lcom/intsig/document/widget/DocumentView$FloatingActionEventCallback;

    .line 171
    .line 172
    if-eqz p1, :cond_5

    .line 173
    .line 174
    sget-object v0, Lcom/intsig/document/widget/DocumentView$FloatingActionType;->WebSearch:Lcom/intsig/document/widget/DocumentView$FloatingActionType;

    .line 175
    .line 176
    iget-object v1, p0, Lcom/intsig/document/widget/DocumentView;->j:Ljava/lang/String;

    .line 177
    .line 178
    invoke-interface {p1, v0, v1}, Lcom/intsig/document/widget/DocumentView$FloatingActionEventCallback;->OnFloatingAction(Lcom/intsig/document/widget/DocumentView$FloatingActionType;Ljava/lang/String;)Z

    .line 179
    .line 180
    .line 181
    move-result p1

    .line 182
    goto :goto_1

    .line 183
    :cond_5
    const/4 p1, 0x0

    .line 184
    :goto_1
    if-nez p1, :cond_9

    .line 185
    .line 186
    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->j:Ljava/lang/String;

    .line 187
    .line 188
    new-instance v0, Landroid/content/Intent;

    .line 189
    .line 190
    const-string v1, "android.intent.action.WEB_SEARCH"

    .line 191
    .line 192
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 193
    .line 194
    .line 195
    const-string v1, "query"

    .line 196
    .line 197
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    .line 199
    .line 200
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 201
    .line 202
    .line 203
    move-result-object p1

    .line 204
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 205
    .line 206
    .line 207
    goto :goto_3

    .line 208
    :cond_6
    sget v0, Lcom/intsig/document/R$id;->btn_annot_style:I

    .line 209
    .line 210
    if-ne p1, v0, :cond_7

    .line 211
    .line 212
    goto :goto_2

    .line 213
    :cond_7
    sget v0, Lcom/intsig/document/R$id;->btn_annot_del:I

    .line 214
    .line 215
    if-ne p1, v0, :cond_9

    .line 216
    .line 217
    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->h:La/b;

    .line 218
    .line 219
    if-eqz p1, :cond_8

    .line 220
    .line 221
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 222
    .line 223
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->DeleteAnnot(La/b;)V

    .line 224
    .line 225
    .line 226
    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 227
    .line 228
    if-eqz p1, :cond_8

    .line 229
    .line 230
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->h:La/b;

    .line 231
    .line 232
    iget v1, v0, La/b;->pageNum:I

    .line 233
    .line 234
    iget v0, v0, La/b;->type:I

    .line 235
    .line 236
    invoke-interface {p1, v1, v0}, Lcom/intsig/document/widget/DocumentView$DocumentActionListener;->onAnnotDelete(II)V

    .line 237
    .line 238
    .line 239
    :cond_8
    :goto_2
    invoke-virtual {p0}, Lcom/intsig/document/widget/DocumentView;->a()V

    .line 240
    .line 241
    .line 242
    :cond_9
    :goto_3
    return-void
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public onElementDataDelete(Lcom/intsig/document/widget/PagesView$ElementData;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/document/widget/DocumentView$DocumentActionListener;->onElementDataDelete(Lcom/intsig/document/widget/PagesView$ElementData;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onElementDataFocus(Lcom/intsig/document/widget/PagesView$ElementData;Lcom/intsig/document/widget/PagesView$AnnotFocusState;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->Down:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 6
    .line 7
    if-eq p2, v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v1, "XXXXXXXXXXXXXXXXXXXXXElementDat "

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 23
    .line 24
    invoke-interface {v0, p1, p2}, Lcom/intsig/document/widget/DocumentView$DocumentActionListener;->onElementDataFocuse(Lcom/intsig/document/widget/PagesView$ElementData;Lcom/intsig/document/widget/PagesView$AnnotFocusState;)V

    .line 25
    .line 26
    .line 27
    :cond_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public onInkPaint(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/document/widget/DocumentView$DocumentActionListener;->onInkPaint(I)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onLinkClick(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/document/widget/DocumentView$DocumentActionListener;->onLinkClick(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    if-nez v0, :cond_1

    .line 12
    .line 13
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .line 14
    .line 15
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 20
    .line 21
    .line 22
    sget v1, Lcom/intsig/document/R$string;->dlg_title_open_link:I

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    new-instance v1, Lcom/intsig/document/widget/DocumentView$b;

    .line 33
    .line 34
    invoke-direct {v1, p0, p1}, Lcom/intsig/document/widget/DocumentView$b;-><init>(Lcom/intsig/document/widget/DocumentView;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    const v2, 0x104000a

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    new-instance v1, Lcom/intsig/document/widget/DocumentView$a;

    .line 45
    .line 46
    invoke-direct {v1, p0, p2, p1}, Lcom/intsig/document/widget/DocumentView$a;-><init>(Lcom/intsig/document/widget/DocumentView;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    const p1, 0x1040002

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, p1, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    new-instance p2, Lcom/intsig/document/widget/DocumentView$m;

    .line 57
    .line 58
    invoke-direct {p2}, Lcom/intsig/document/widget/DocumentView$m;-><init>()V

    .line 59
    .line 60
    .line 61
    const/high16 v0, 0x1040000

    .line 62
    .line 63
    invoke-virtual {p1, v0, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 68
    .line 69
    .line 70
    :cond_1
    const/4 p1, 0x1

    .line 71
    return p1
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public onSaveComplete(I)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/document/widget/DocumentView$h;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lcom/intsig/document/widget/DocumentView$h;-><init>(Lcom/intsig/document/widget/DocumentView;I)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/document/widget/DocumentView;->g:Landroid/os/Handler;

    .line 7
    .line 8
    new-instance v1, Lcom/intsig/document/widget/b;

    .line 9
    .line 10
    invoke-direct {v1, v0}, Lcom/intsig/document/widget/b;-><init>(Ljava/lang/Runnable;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onSelectionClose()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/document/widget/DocumentView;->b()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/document/widget/DocumentView;->j:Ljava/lang/String;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onTabReselected(Lcom/google/android/material/tabs/TabLayout$Tab;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onTabSelected(Lcom/google/android/material/tabs/TabLayout$Tab;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onTabUnselected(Lcom/google/android/material/tabs/TabLayout$Tab;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onTap()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/document/widget/DocumentView$DocumentActionListener;->onTap()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onTextAnnotClick(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->k:Lcom/intsig/document/widget/DocumentView$DocumentActionListener;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-interface {v0, p1, p2}, Lcom/intsig/document/widget/DocumentView$DocumentActionListener;->onTextAnnnotClick(Ljava/lang/String;Ljava/lang/String;)Z

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 p1, 0x0

    .line 12
    :goto_0
    if-nez p1, :cond_1

    .line 13
    .line 14
    new-instance p1, Landroid/app/AlertDialog$Builder;

    .line 15
    .line 16
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 21
    .line 22
    .line 23
    sget v0, Lcom/intsig/document/R$string;->dlg_annotation_title:I

    .line 24
    .line 25
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-virtual {p1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    new-instance p2, Lcom/intsig/document/widget/DocumentView$e;

    .line 34
    .line 35
    invoke-direct {p2}, Lcom/intsig/document/widget/DocumentView$e;-><init>()V

    .line 36
    .line 37
    .line 38
    const-string v0, "Close"

    .line 39
    .line 40
    invoke-virtual {p1, v0, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    new-instance p2, Lcom/intsig/document/widget/DocumentView$d;

    .line 45
    .line 46
    invoke-direct {p2}, Lcom/intsig/document/widget/DocumentView$d;-><init>()V

    .line 47
    .line 48
    .line 49
    const-string v0, "Save"

    .line 50
    .line 51
    invoke-virtual {p1, v0, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    new-instance p2, Lcom/intsig/document/widget/DocumentView$c;

    .line 56
    .line 57
    invoke-direct {p2}, Lcom/intsig/document/widget/DocumentView$c;-><init>()V

    .line 58
    .line 59
    .line 60
    const-string v0, "Delete"

    .line 61
    .line 62
    invoke-virtual {p1, v0, p2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 71
    .line 72
    .line 73
    :cond_1
    return v1
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public onTextSelected(Ljava/lang/String;Landroid/graphics/PointF;)V
    .locals 4

    .line 1
    iget v0, p2, Landroid/graphics/PointF;->x:F

    .line 2
    .line 3
    iget p2, p2, Landroid/graphics/PointF;->y:F

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    int-to-float v0, v0

    .line 12
    const/4 v1, 0x0

    .line 13
    cmpg-float v1, v0, v1

    .line 14
    .line 15
    if-gtz v1, :cond_0

    .line 16
    .line 17
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    sget v1, Lcom/intsig/document/R$dimen;->floating_bar_height:I

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    :cond_0
    float-to-double v0, v0

    .line 32
    const-wide v2, 0x3ff3333333333333L    # 1.2

    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    mul-double v0, v0, v2

    .line 38
    .line 39
    double-to-float v0, v0

    .line 40
    iget-object v1, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 41
    .line 42
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 47
    .line 48
    const/16 v2, 0xe

    .line 49
    .line 50
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 51
    .line 52
    .line 53
    sub-float v2, p2, v0

    .line 54
    .line 55
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 56
    .line 57
    .line 58
    move-result v2

    .line 59
    const/high16 v3, 0x40000000    # 2.0f

    .line 60
    .line 61
    div-float/2addr v0, v3

    .line 62
    add-float/2addr v0, p2

    .line 63
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 64
    .line 65
    .line 66
    move-result p2

    .line 67
    if-gez v2, :cond_1

    .line 68
    .line 69
    if-gez p2, :cond_1

    .line 70
    .line 71
    iget-object p2, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 72
    .line 73
    const/16 v0, 0x8

    .line 74
    .line 75
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 76
    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_1
    if-lez v2, :cond_2

    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_2
    move v2, p2

    .line 83
    :goto_0
    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 84
    .line 85
    iget-object p2, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 86
    .line 87
    const/4 v0, 0x0

    .line 88
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 89
    .line 90
    .line 91
    iget-object p2, p0, Lcom/intsig/document/widget/DocumentView;->c:Landroid/widget/LinearLayout;

    .line 92
    .line 93
    invoke-virtual {p0, p2, v1}, Landroid/view/ViewGroup;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 94
    .line 95
    .line 96
    :goto_1
    iput-object p1, p0, Lcom/intsig/document/widget/DocumentView;->j:Ljava/lang/String;

    .line 97
    .line 98
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public redoInk()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView;->redoInk()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public renderPage(IIZLcom/intsig/document/widget/DocumentView$RenderCallback;)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/intsig/document/widget/PagesView;->renderPage(IIZLcom/intsig/document/widget/DocumentView$RenderCallback;)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x0

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public setAnnotActionBar(I)Lcom/intsig/document/widget/DocumentView;
    .locals 0
    .param p1    # I
        .annotation build Landroidx/annotation/LayoutRes;
        .end annotation
    .end param

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iput p1, p0, Lcom/intsig/document/widget/DocumentView;->f:I

    .line 4
    .line 5
    :cond_0
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setAnnotBoxStyle(IF[FLandroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    move v1, p1

    .line 4
    move v2, p2

    .line 5
    move-object v3, p3

    .line 6
    move-object v4, p4

    .line 7
    move-object v5, p5

    .line 8
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/document/widget/PagesView;->setAnnotBoxStyle(IF[FLandroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method public setCanvasBackgroundColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->setCanvasBackgroundColor(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFastScrollBar(Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/document/widget/PagesView;->setFastScrollBar(Landroid/graphics/drawable/Drawable;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setFloatingActionEventCallback(Lcom/intsig/document/widget/DocumentView$FloatingActionEventCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/document/widget/DocumentView;->l:Lcom/intsig/document/widget/DocumentView$FloatingActionEventCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setInkErase(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->setInkErase(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPageBackground(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->setPageBackground(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPageBorderSize(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->setPageBorderSize(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPageGap(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->setPageGap(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setScaleRange(FF)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/document/widget/PagesView;->setScaleRange(FF)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setSideBarStyle(III)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/document/widget/PagesView;->setSideBarStyle(III)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public setTextSelectionBar(I)Lcom/intsig/document/widget/DocumentView;
    .locals 0
    .param p1    # I
        .annotation build Landroidx/annotation/LayoutRes;
        .end annotation
    .end param

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iput p1, p0, Lcom/intsig/document/widget/DocumentView;->e:I

    .line 4
    .line 5
    :cond_0
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public showAnnot(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->showAnnot(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public showSearchResult(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->showSearchResult(I)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public undoInk()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView;->undoInk()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public updateElement(Lcom/intsig/document/widget/PagesView$ElementData;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/DocumentView;->b:Lcom/intsig/document/widget/PagesView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView;->updateElement(Lcom/intsig/document/widget/PagesView$ElementData;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.class public Lcom/intsig/document/widget/PagesView$ImageArgs;
.super Lcom/intsig/document/widget/PagesView$BaseArgs;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/document/widget/PagesView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ImageArgs"
.end annotation


# instance fields
.field public angle:I

.field public bmp:Landroid/graphics/Bitmap;

.field public fixPosition:Z

.field public initialSize:F

.field public left:I

.field public rotatable:Z

.field public top:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/document/widget/PagesView$BaseArgs;-><init>()V

    const/high16 v0, 0x42f00000    # 120.0f

    iput v0, p0, Lcom/intsig/document/widget/PagesView$ImageArgs;->initialSize:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/intsig/document/widget/PagesView$ImageArgs;->left:I

    iput v0, p0, Lcom/intsig/document/widget/PagesView$ImageArgs;->top:I

    iput-object p1, p0, Lcom/intsig/document/widget/PagesView$ImageArgs;->bmp:Landroid/graphics/Bitmap;

    const/4 p1, 0x0

    iput p1, p0, Lcom/intsig/document/widget/PagesView$ImageArgs;->angle:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView$BaseArgs;->fixedRatio:Z

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;Z)V
    .locals 1

    .line 2
    invoke-direct {p0}, Lcom/intsig/document/widget/PagesView$BaseArgs;-><init>()V

    const/high16 v0, 0x42f00000    # 120.0f

    iput v0, p0, Lcom/intsig/document/widget/PagesView$ImageArgs;->initialSize:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/intsig/document/widget/PagesView$ImageArgs;->left:I

    iput v0, p0, Lcom/intsig/document/widget/PagesView$ImageArgs;->top:I

    iput-object p1, p0, Lcom/intsig/document/widget/PagesView$ImageArgs;->bmp:Landroid/graphics/Bitmap;

    const/4 p1, 0x0

    iput p1, p0, Lcom/intsig/document/widget/PagesView$ImageArgs;->angle:I

    iput-boolean p2, p0, Lcom/intsig/document/widget/PagesView$BaseArgs;->fixedRatio:Z

    return-void
.end method
